.class final Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createPromoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
        ">;"
    }
.end annotation


# instance fields
.field private currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V
    .locals 1

    .prologue
    .line 186
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;)V

    return-void
.end method


# virtual methods
.method protected createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData<Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;>;"
    const/4 v9, 0x4

    .line 191
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;->currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    if-eq v7, p1, :cond_0

    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;->currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 194
    const v7, 0x7f0e06d8

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 196
    .local v4, "root":Landroid/view/View;
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;

    .line 198
    .local v3, "li":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
    const v7, 0x7f0e06df

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 199
    .local v5, "title":Landroid/widget/TextView;
    iget-object v7, v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;->welcomeText:Ljava/lang/String;

    invoke-static {v5, v7, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 201
    const v7, 0x7f0e06dc

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 203
    .local v6, "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getPlaceholderRid()I

    move-result v1

    .line 204
    .local v1, "defaultRid":I
    iget-object v7, v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;->imageUri:Ljava/lang/String;

    invoke-virtual {v6, v7, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 205
    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$300(Landroid/widget/ImageView;)V

    .line 207
    const v7, 0x7f0e06e0

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 208
    .local v0, "companion":Landroid/widget/TextView;
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$400(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "hasString":Ljava/lang/String;
    invoke-static {v0, v2, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 211
    .end local v0    # "companion":Landroid/widget/TextView;
    .end local v1    # "defaultRid":I
    .end local v2    # "hasString":Ljava/lang/String;
    .end local v3    # "li":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;
    .end local v4    # "root":Landroid/view/View;
    .end local v5    # "title":Landroid/widget/TextView;
    .end local v6    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_0
    return-void
.end method
