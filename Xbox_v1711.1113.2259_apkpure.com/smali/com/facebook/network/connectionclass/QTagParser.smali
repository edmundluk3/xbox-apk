.class Lcom/facebook/network/connectionclass/QTagParser;
.super Ljava/lang/Object;
.source "QTagParser.java"


# static fields
.field private static final QTAGUID_UID_STATS:Ljava/lang/String; = "/proc/net/xt_qtaguid/stats"

.field private static final TAG:Ljava/lang/String; = "QTagParser"

.field public static sInstance:Lcom/facebook/network/connectionclass/QTagParser;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static final sLineBuffer:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[B>;"
        }
    .end annotation
.end field

.field private static sPreviousBytes:J

.field private static sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

.field private static sStatsReader:Lcom/facebook/network/connectionclass/LineBufferReader;


# instance fields
.field private mPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/facebook/network/connectionclass/QTagParser$1;

    invoke-direct {v0}, Lcom/facebook/network/connectionclass/QTagParser$1;-><init>()V

    sput-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sLineBuffer:Ljava/lang/ThreadLocal;

    .line 38
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/facebook/network/connectionclass/QTagParser;->sPreviousBytes:J

    .line 39
    new-instance v0, Lcom/facebook/network/connectionclass/LineBufferReader;

    invoke-direct {v0}, Lcom/facebook/network/connectionclass/LineBufferReader;-><init>()V

    sput-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sStatsReader:Lcom/facebook/network/connectionclass/LineBufferReader;

    .line 40
    new-instance v0, Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-direct {v0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;-><init>()V

    sput-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/facebook/network/connectionclass/QTagParser;->mPath:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/facebook/network/connectionclass/QTagParser;
    .locals 3
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 47
    const-class v1, Lcom/facebook/network/connectionclass/QTagParser;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sInstance:Lcom/facebook/network/connectionclass/QTagParser;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/facebook/network/connectionclass/QTagParser;

    const-string v2, "/proc/net/xt_qtaguid/stats"

    invoke-direct {v0, v2}, Lcom/facebook/network/connectionclass/QTagParser;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sInstance:Lcom/facebook/network/connectionclass/QTagParser;

    .line 50
    :cond_0
    sget-object v0, Lcom/facebook/network/connectionclass/QTagParser;->sInstance:Lcom/facebook/network/connectionclass/QTagParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public parseDataUsageForUidAndTag(I)J
    .locals 18
    .param p1, "uid"    # I

    .prologue
    .line 75
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v10

    .line 77
    .local v10, "savedPolicy":Landroid/os/StrictMode$ThreadPolicy;
    const-wide/16 v12, 0x0

    .line 79
    .local v12, "tagRxBytes":J
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/network/connectionclass/QTagParser;->mPath:Ljava/lang/String;

    invoke-direct {v6, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 80
    .local v6, "fis":Ljava/io/FileInputStream;
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sStatsReader:Lcom/facebook/network/connectionclass/LineBufferReader;

    invoke-virtual {v11, v6}, Lcom/facebook/network/connectionclass/LineBufferReader;->setFileStream(Ljava/io/FileInputStream;)V

    .line 81
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sLineBuffer:Ljava/lang/ThreadLocal;

    invoke-virtual {v11}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85
    .local v2, "buffer":[B
    :try_start_1
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sStatsReader:Lcom/facebook/network/connectionclass/LineBufferReader;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/LineBufferReader;->skipLine()V

    .line 87
    const/4 v8, 0x2

    .line 88
    .local v8, "line":I
    :cond_0
    :goto_0
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sStatsReader:Lcom/facebook/network/connectionclass/LineBufferReader;

    invoke-virtual {v11, v2}, Lcom/facebook/network/connectionclass/LineBufferReader;->readLine([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .local v7, "length":I
    const/4 v11, -0x1

    if-eq v7, v11, :cond_1

    .line 104
    :try_start_2
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11, v2, v7}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->reset([BI)Lcom/facebook/network/connectionclass/ByteArrayScanner;

    .line 105
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    const/16 v14, 0x20

    invoke-virtual {v11, v14}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->useDelimiter(C)Lcom/facebook/network/connectionclass/ByteArrayScanner;

    .line 107
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->skip()V

    .line 108
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    const-string v14, "lo"

    invoke-virtual {v11, v14}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->nextStringEquals(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 111
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->skip()V

    .line 112
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->nextInt()I

    move-result v11

    move/from16 v0, p1

    if-ne v11, v0, :cond_0

    .line 115
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->skip()V

    .line 116
    sget-object v11, Lcom/facebook/network/connectionclass/QTagParser;->sScanner:Lcom/facebook/network/connectionclass/ByteArrayScanner;

    invoke-virtual {v11}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->nextInt()I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 117
    .local v9, "rxBytes":I
    int-to-long v14, v9

    add-long/2addr v12, v14

    .line 118
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 120
    .end local v9    # "rxBytes":I
    :catch_0
    move-exception v3

    .line 121
    .local v3, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    const-string v11, "QTagParser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Cannot parse byte count at line"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v11, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 129
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .end local v7    # "length":I
    .end local v8    # "line":I
    :catchall_0
    move-exception v11

    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    throw v11
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 140
    .end local v2    # "buffer":[B
    .end local v6    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 141
    .local v3, "e":Ljava/io/IOException;
    :try_start_5
    const-string v11, "QTagParser"

    const-string v14, "Error reading from /proc/net/xt_qtaguid/stats. Please check if this file exists."

    invoke-static {v11, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 143
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 147
    const-wide/16 v4, -0x1

    .end local v3    # "e":Ljava/io/IOException;
    :goto_1
    return-wide v4

    .line 123
    .restart local v2    # "buffer":[B
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "length":I
    .restart local v8    # "line":I
    :catch_2
    move-exception v3

    .line 124
    .local v3, "e":Ljava/util/NoSuchElementException;
    :try_start_6
    const-string v11, "QTagParser"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid number of tokens on line "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v11, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 129
    .end local v3    # "e":Ljava/util/NoSuchElementException;
    :cond_1
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 132
    sget-wide v14, Lcom/facebook/network/connectionclass/QTagParser;->sPreviousBytes:J

    const-wide/16 v16, -0x1

    cmp-long v11, v14, v16

    if-nez v11, :cond_2

    .line 133
    sput-wide v12, Lcom/facebook/network/connectionclass/QTagParser;->sPreviousBytes:J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 134
    const-wide/16 v4, -0x1

    .line 143
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    .line 136
    :cond_2
    :try_start_8
    sget-wide v14, Lcom/facebook/network/connectionclass/QTagParser;->sPreviousBytes:J

    sub-long v4, v12, v14

    .line 137
    .local v4, "diff":J
    sput-wide v12, Lcom/facebook/network/connectionclass/QTagParser;->sPreviousBytes:J
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 143
    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    .end local v2    # "buffer":[B
    .end local v4    # "diff":J
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "length":I
    .end local v8    # "line":I
    :catchall_1
    move-exception v11

    invoke-static {v10}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v11
.end method
