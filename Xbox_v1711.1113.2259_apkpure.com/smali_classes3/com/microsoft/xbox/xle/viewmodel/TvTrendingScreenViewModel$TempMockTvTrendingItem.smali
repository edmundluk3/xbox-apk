.class public Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;
.super Ljava/lang/Object;
.source "TvTrendingScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TempMockTvTrendingItem"
.end annotation


# instance fields
.field public final description:Ljava/lang/String;

.field public final imageURI:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "imageURI"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->imageURI:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->title:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->description:Ljava/lang/String;

    .line 37
    return-void
.end method
