.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_SearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final continuationTokenAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContinuationToken:Ljava/lang/String;

.field private defaultMaxPageSize:Ljava/lang/Integer;

.field private defaultQuery:Ljava/lang/String;

.field private defaultSort:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;",
            ">;"
        }
    .end annotation
.end field

.field private final maxPageSizeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final queryAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sortAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultQuery:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultContinuationToken:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultSort:Lcom/google/common/collect/ImmutableList;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultMaxPageSize:Ljava/lang/Integer;

    .line 31
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->queryAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->continuationTokenAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->sortAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->maxPageSizeAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 74
    const/4 v5, 0x0

    .line 110
    :goto_0
    return-object v5

    .line 76
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 77
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultQuery:Ljava/lang/String;

    .line 78
    .local v3, "query":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultContinuationToken:Ljava/lang/String;

    .line 79
    .local v1, "continuationToken":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultSort:Lcom/google/common/collect/ImmutableList;

    .line 80
    .local v4, "sort":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultMaxPageSize:Ljava/lang/Integer;

    .line 81
    .local v2, "maxPageSize":Ljava/lang/Integer;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 84
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 87
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 105
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 87
    :sswitch_0
    const-string v6, "query"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "continuationToken"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "sort"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "maxPageSize"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 89
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->queryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "query":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 90
    .restart local v3    # "query":Ljava/lang/String;
    goto :goto_1

    .line 93
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->continuationTokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "continuationToken":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 94
    .restart local v1    # "continuationToken":Ljava/lang/String;
    goto :goto_1

    .line 97
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->sortAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "sort":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;>;"
    check-cast v4, Lcom/google/common/collect/ImmutableList;

    .line 98
    .restart local v4    # "sort":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;>;"
    goto :goto_1

    .line 101
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->maxPageSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "maxPageSize":Ljava/lang/Integer;
    check-cast v2, Ljava/lang/Integer;

    .line 102
    .restart local v2    # "maxPageSize":Ljava/lang/Integer;
    goto :goto_1

    .line 109
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 110
    new-instance v5, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest;

    invoke-direct {v5, v3, v1, v4, v2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 87
    nop

    :sswitch_data_0
    .sparse-switch
        -0x53a3c34c -> :sswitch_3
        0x35f59e -> :sswitch_2
        0x66f18c8 -> :sswitch_0
        0x2ddd9e02 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultContinuationToken(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContinuationToken"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultContinuationToken:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public setDefaultMaxPageSize(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMaxPageSize"    # Ljava/lang/Integer;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultMaxPageSize:Ljava/lang/Integer;

    .line 50
    return-object p0
.end method

.method public setDefaultQuery(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultQuery"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultQuery:Ljava/lang/String;

    .line 38
    return-object p0
.end method

.method public setDefaultSort(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;",
            ">;)",
            "Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "defaultSort":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->defaultSort:Lcom/google/common/collect/ImmutableList;

    .line 46
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    if-nez p2, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 69
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 60
    const-string v0, "query"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->queryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->query()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 62
    const-string v0, "continuationToken"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->continuationTokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->continuationToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 64
    const-string v0, "sort"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->sortAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->sort()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    const-string v0, "maxPageSize"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->maxPageSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->maxPageSize()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)V

    return-void
.end method
