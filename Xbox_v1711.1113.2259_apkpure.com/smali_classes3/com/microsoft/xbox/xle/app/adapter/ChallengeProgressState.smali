.class public final enum Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
.super Ljava/lang/Enum;
.source "ChallengeProgressState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

.field public static final enum Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

.field public static final enum InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

.field public static final enum Invalid:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

.field public static final enum NotStarted:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    const-string v1, "Invalid"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Invalid:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    const-string v1, "Achieved"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    const-string v1, "InProgress"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    const-string v1, "NotStarted"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->NotStarted:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Invalid:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->NotStarted:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    return-object v0
.end method
