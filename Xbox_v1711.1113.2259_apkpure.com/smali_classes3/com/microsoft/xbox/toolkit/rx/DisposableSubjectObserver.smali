.class public final Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;
.super Lio/reactivex/observers/DisposableObserver;
.source "DisposableSubjectObserver.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/observers/DisposableObserver",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final subject:Lio/reactivex/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/Subject",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/subjects/Subject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/subjects/Subject",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver<TT;>;"
    .local p1, "subject":Lio/reactivex/subjects/Subject;, "Lio/reactivex/subjects/Subject<TT;>;"
    invoke-direct {p0}, Lio/reactivex/observers/DisposableObserver;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;->subject:Lio/reactivex/subjects/Subject;

    .line 16
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 1

    .prologue
    .line 30
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0}, Lio/reactivex/subjects/Subject;->onComplete()V

    .line 31
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 25
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->onError(Ljava/lang/Throwable;)V

    .line 26
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->onNext(Ljava/lang/Object;)V

    .line 21
    return-void
.end method
