.class public final enum Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
.super Ljava/lang/Enum;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlatformType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

.field public static final enum Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

.field public static final enum Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

.field private static final enumNameToTypeLookUp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ">;"
        }
    .end annotation
.end field

.field private static final platformNameResourcesOnPDP:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final platformNameToTypeLookUp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final displayNameResource:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 39
    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    const-string v3, "Xbox"

    const-string v4, "windows.xbox"

    const v5, 0x7f071086

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    const-string v3, "Desktop"

    const-string v4, "windows.desktop"

    const v5, 0x7f071085

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .line 38
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    aput-object v3, v2, v6

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .line 56
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameToTypeLookUp:Ljava/util/Map;

    .line 57
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->enumNameToTypeLookUp:Ljava/util/Map;

    .line 58
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameResourcesOnPDP:Ljava/util/List;

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 60
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameToTypeLookUp:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->enumNameToTypeLookUp:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameResourcesOnPDP:Ljava/util/List;

    iget v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->displayNameResource:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "displayNameResource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->text:Ljava/lang/String;

    .line 48
    iput p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->displayNameResource:I

    .line 49
    return-void
.end method

.method public static fromPlatformName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 2
    .param p0, "platformName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameToTypeLookUp:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method

.method public static fromPlatformTypeEnumName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 2
    .param p0, "platformTypeEnumName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->enumNameToTypeLookUp:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method

.method public static getPlatformNameResources()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->platformNameResourcesOnPDP:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->text:Ljava/lang/String;

    return-object v0
.end method
