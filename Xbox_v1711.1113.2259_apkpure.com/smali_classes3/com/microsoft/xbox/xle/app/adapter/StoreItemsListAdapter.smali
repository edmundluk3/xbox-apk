.class public Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "StoreItemsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private filterInfo:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;

.field private hideReleaseDate:Z

.field private listViewRowLayoutId:I

.field private storeSearchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/service/model/StoreBrowseType;Ljava/util/List;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "storeSearchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/microsoft/xbox/service/model/StoreBrowseType;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/service/model/StoreBrowseType;Ljava/util/List;Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/service/model/StoreBrowseType;Ljava/util/List;Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "storeSearchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p5, "filterInfo"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/microsoft/xbox/service/model/StoreBrowseType;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 32
    const v0, 0x7f030216

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->listViewRowLayoutId:I

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->hideReleaseDate:Z

    .line 43
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->storeSearchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    .line 44
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->listViewRowLayoutId:I

    .line 45
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->filterInfo:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;

    .line 46
    invoke-virtual {p0, p4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 47
    return-void
.end method

.method private populateDefaultScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 10
    .param p1, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 144
    if-eqz p2, :cond_3

    .line 145
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v1

    .line 146
    .local v1, "defaultRid":I
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "imageUri":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 148
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v5

    invoke-virtual {v5, v2, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 151
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->storeSearchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    sget-object v6, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v5, v6, :cond_1

    .line 152
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v5

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtBackgroundColor()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setBackgroundColor(I)V

    .line 155
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v3

    .line 157
    .local v3, "releaseDate":Ljava/util/Date;
    if-eqz v3, :cond_2

    .line 158
    invoke-virtual {v3}, Ljava/util/Date;->getYear()I

    move-result v5

    add-int/lit16 v4, v5, 0x76c

    .line 160
    .local v4, "year":I
    const/16 v5, 0xaef

    if-lt v4, v5, :cond_4

    .line 161
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    const v6, 0x7f0703e9

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 170
    .end local v4    # "year":I
    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    move-result-object v5

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAverageUserRating()F

    move-result v6

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAllTimeRatingCount()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v5, v6, v8, v9}, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;->setAverageUserRatingAndUserCount(FJ)V

    .line 172
    .end local v1    # "defaultRid":I
    .end local v2    # "imageUri":Ljava/lang/String;
    .end local v3    # "releaseDate":Ljava/util/Date;
    :cond_3
    return-void

    .line 163
    .restart local v1    # "defaultRid":I
    .restart local v2    # "imageUri":Ljava/lang/String;
    .restart local v3    # "releaseDate":Ljava/util/Date;
    .restart local v4    # "year":I
    :cond_4
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "dateText":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->storeSearchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    sget-object v6, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->filterInfo:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->filterInfo:Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;

    invoke-interface {v5}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$StoreItemsListFilterInfo;->getCurrentSearchFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ComingSoon:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    if-ne v5, v6, :cond_5

    .line 165
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 167
    :cond_5
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private populateGamepassScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 5
    .param p1, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 105
    if-eqz p2, :cond_1

    .line 106
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 107
    .local v0, "defaultRid":I
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "imageUri":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 109
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v2

    invoke-virtual {v2, v1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 111
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v2

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 114
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v2

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCurrencyCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    .end local v0    # "defaultRid":I
    .end local v1    # "imageUri":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private populateGoldScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 6
    .param p1, "viewHolder"    # Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 121
    if-eqz p2, :cond_1

    .line 122
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 123
    .local v0, "defaultRid":I
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "imageUri":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 125
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v4

    invoke-virtual {v4, v1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 127
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v2

    .line 129
    .local v2, "releaseDate":Ljava/util/Date;
    if-eqz v2, :cond_1

    .line 130
    invoke-virtual {v2}, Ljava/util/Date;->getYear()I

    move-result v4

    add-int/lit16 v3, v4, 0x76c

    .line 132
    .local v3, "year":I
    const/16 v4, 0xaef

    if-lt v3, v4, :cond_2

    .line 133
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    const v5, 0x7f0703e9

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 140
    .end local v0    # "defaultRid":I
    .end local v1    # "imageUri":Ljava/lang/String;
    .end local v2    # "releaseDate":Ljava/util/Date;
    .end local v3    # "year":I
    :cond_1
    :goto_0
    return-void

    .line 135
    .restart local v0    # "defaultRid":I
    .restart local v1    # "imageUri":Ljava/lang/String;
    .restart local v2    # "releaseDate":Ljava/util/Date;
    .restart local v3    # "year":I
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 56
    move-object v1, p2

    .line 57
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_2

    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "layout_inflater"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 59
    .local v2, "vi":Landroid/view/LayoutInflater;
    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->listViewRowLayoutId:I

    invoke-virtual {v2, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 60
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    invoke-direct {v3, v8}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$1;)V

    .line 62
    .local v3, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    const v4, 0x7f0e0a73

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 63
    const v4, 0x7f0e0a74

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 64
    const v4, 0x7f0e0a75

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 65
    const v4, 0x7f0e0a76

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$402(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;)Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    .line 66
    const v4, 0x7f0e0a78

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$502(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 67
    const v4, 0x7f0e0a79

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$602(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 69
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 70
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v4

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x10

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPaintFlags(I)V

    .line 73
    :cond_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 78
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 80
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    sget-object v4, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$StoreBrowseType:[I

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->storeSearchType:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/StoreBrowseType;->ordinal()I

    move-result v7

    aget v4, v4, v7

    packed-switch v4, :pswitch_data_0

    .line 90
    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->populateDefaultScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 94
    :goto_1
    if-eqz v3, :cond_1

    .line 95
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v7

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->hideReleaseDate:Z

    if-nez v4, :cond_3

    move v4, v5

    :goto_2
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 96
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v7, "%1$s, %2$s, %3$s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    .line 97
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v8, v6

    .line 98
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNullAndVisible(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v8, v5

    const/4 v5, 0x2

    .line 99
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;->access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;)Lcom/microsoft/xbox/xle/ui/StarRatingWithUserCountView;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveContentDescriptionIfNotNullAndVisible(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v8, v5

    .line 96
    invoke-static {v4, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    :cond_1
    return-object v1

    .line 75
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;

    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;
    goto :goto_0

    .line 82
    .restart local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :pswitch_0
    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->populateGoldScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_1

    .line 86
    :pswitch_1
    invoke-direct {p0, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->populateGamepassScreen(Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter$ViewHolder;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_1

    :cond_3
    move v4, v6

    .line 95
    goto :goto_2

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setHideReleaseDate(Z)V
    .locals 0
    .param p1, "shouldHide"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsListAdapter;->hideReleaseDate:Z

    .line 51
    return-void
.end method
