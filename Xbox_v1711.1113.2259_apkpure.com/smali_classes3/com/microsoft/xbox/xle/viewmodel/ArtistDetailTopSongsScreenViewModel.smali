.class public Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "ArtistDetailTopSongsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;",
        ">;"
    }
.end annotation


# instance fields
.field private isLoadingTracks:Z

.field private loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->isLoadingTracks:Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    .line 24
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getArtistDetailTopSongsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 25
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->onLoadTracksCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->isLoadingTracks:Z

    return p1
.end method

.method private onLoadTracksCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 83
    const-string v0, "ArtistDetailTopSongViewModel"

    const-string v1, "onLoadTracksCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->isLoadingTracks:Z

    .line 86
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->updateAdapter()V

    .line 101
    return-void

    .line 90
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getTopTracks()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 94
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 95
    const-string v0, "MediaItemListViewModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error state because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f070683

    return v0
.end method

.method public getTopTracks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getTopTracks()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->isLoadingTracks:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->cancel()V

    .line 52
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->execute()V

    .line 55
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getArtistDetailTopSongsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 30
    return-void
.end method

.method public onStopOverride()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStopOverride()V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->loadTrackTask:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->cancel()V

    .line 63
    :cond_0
    return-void
.end method

.method public playSmartDJ()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const v5, 0x18ffc9f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    const-string v5, "MusicArtist"

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&PlaySmartDJ=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "uri":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v1

    invoke-static {v0, v1, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    .line 73
    return-void
.end method

.method public playTopSongs()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 66
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v4, "MusicArtist"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "uri":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v1

    invoke-static {v0, v1, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    .line 68
    return-void
.end method

.method public playTrack(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getTopTracks()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 77
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x18ffc9f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ID:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "Track"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "uri":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v2

    invoke-static {v1, v2, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    .line 80
    return-void
.end method
