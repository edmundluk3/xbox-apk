.class public Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "BeamListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final channelItemAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final itemClickRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "channelItemAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 28
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->itemClickRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->channelItemAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->channelItemAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemClickObservable()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->itemClickRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 51
    const v0, 0x7f030236

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    .line 44
    .local v0, "item":Lcom/microsoft/xbox/domain/beam/BeamChannel;
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 47
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;-><init>(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;Landroid/view/View;)V

    return-object v1
.end method
