.class final Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "AuthInfoProviderRnModule.java"


# static fields
.field private static final AGE_GROUP:Ljava/lang/String; = "ageGroup"

.field private static final NAME:Ljava/lang/String; = "AuthInfoProvider"

.field private static final PRIVILEGES:Ljava/lang/String; = "privileges"

.field private static final SIGNATURE:Ljava/lang/String; = "signature"

.field private static final TOKEN:Ljava/lang/String; = "token"


# direct methods
.method constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 31
    return-void
.end method

.method static synthetic lambda$get$0(ZLjava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
    .locals 3
    .param p0, "forceRefresh"    # Z
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    if-eqz p0, :cond_0

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    const-string v1, "GET"

    const-string v2, ""

    invoke-virtual {v0, v1, p1, v2}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getNewTokenAndSignatureSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    .line 43
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    const-string v1, "GET"

    const-string v2, ""

    invoke-virtual {v0, v1, p1, v2}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getTokenAndSignatureSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$get$1(Lcom/facebook/react/bridge/Promise;Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;)V
    .locals 9
    .param p0, "promise"    # Lcom/facebook/react/bridge/Promise;
    .param p1, "tokenAndSignatureResult"    # Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getToken()Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v4

    .line 51
    .local v4, "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-eqz v4, :cond_1

    .line 52
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 53
    .local v0, "authInfoMap":Lcom/facebook/react/bridge/WritableMap;
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 55
    .local v2, "privileges":Lcom/facebook/react/bridge/WritableArray;
    const-string/jumbo v5, "token"

    invoke-virtual {v4}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getToken()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v5, "ageGroup"

    invoke-virtual {v4}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getAgeGroup()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v5, "signature"

    invoke-virtual {v4}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getSignature()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Lcom/facebook/react/bridge/WritableMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v4}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getPriviliges()Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "rawPriv":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 62
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v1, v6, v5

    .line 63
    .local v1, "priv":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {v2, v8}, Lcom/facebook/react/bridge/WritableArray;->pushInt(I)V

    .line 62
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 67
    .end local v1    # "priv":Ljava/lang/String;
    :cond_0
    const-string v5, "privileges"

    invoke-interface {v0, v5, v2}, Lcom/facebook/react/bridge/WritableMap;->putArray(Ljava/lang/String;Lcom/facebook/react/bridge/WritableArray;)V

    .line 69
    invoke-interface {p0, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    .line 76
    .end local v0    # "authInfoMap":Lcom/facebook/react/bridge/WritableMap;
    .end local v2    # "privileges":Lcom/facebook/react/bridge/WritableArray;
    .end local v3    # "rawPriv":Ljava/lang/String;
    :goto_1
    return-void

    .line 73
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getErrorCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v6

    .line 72
    invoke-interface {p0, v5, v6}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$get$2(Ljava/lang/String;ZLcom/facebook/react/bridge/Promise;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "forceRefresh"    # Z
    .param p2, "promise"    # Lcom/facebook/react/bridge/Promise;
    .param p3, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    const-string v0, "AuthInfoProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get auth info: url"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    invoke-interface {p2, p3}, Lcom/facebook/react/bridge/Promise;->reject(Ljava/lang/Throwable;)V

    .line 81
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;ZLcom/facebook/react/bridge/Promise;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "forceRefresh"    # Z
    .param p3, "promise"    # Lcom/facebook/react/bridge/Promise;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 40
    invoke-static {p2, p1}, Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule$$Lambda$1;->lambdaFactory$(ZLjava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    .line 45
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 46
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule$$Lambda$2;->lambdaFactory$(Lcom/facebook/react/bridge/Promise;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/toolkit/rn/AuthInfoProviderRnModule$$Lambda$3;->lambdaFactory$(Ljava/lang/String;ZLcom/facebook/react/bridge/Promise;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 47
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 83
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "AuthInfoProvider"

    return-object v0
.end method
