.class public interface abstract Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
.super Ljava/lang/Object;
.source "IncrementalLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+TT;>;>;)V"
        }
    .end annotation
.end method

.method public abstract hasMoreItemsToLoad()Z
.end method

.method public abstract loadMoreItemsAsync()V
.end method

.method public abstract removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+TT;>;>;)V"
        }
    .end annotation
.end method
