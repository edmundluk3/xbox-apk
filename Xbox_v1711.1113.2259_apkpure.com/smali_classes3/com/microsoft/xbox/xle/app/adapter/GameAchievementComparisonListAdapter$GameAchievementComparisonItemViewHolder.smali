.class public Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;
.super Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;
.source "GameAchievementComparisonListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameAchievementComparisonItemViewHolder"
.end annotation


# instance fields
.field private achievementDescriptionTextView:Landroid/widget/TextView;

.field private achievementNameTextView:Landroid/widget/TextView;

.field private achievementRarityTextView:Landroid/widget/TextView;

.field private gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private gamerscoreIconView:Landroid/widget/TextView;

.field private gamerscoreTextView:Landroid/widget/TextView;

.field private imageContainer:Landroid/view/View;

.field private meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private meCompletedOnView:Landroid/widget/TextView;

.field private meLockedIconTextView:Landroid/widget/TextView;

.field private meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private narratorContent:Ljava/lang/String;

.field private overlay:Landroid/view/View;

.field private rareIconView:Landroid/widget/TextView;

.field private rewardIconView:Landroid/widget/TextView;

.field private youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private youCompletedOnView:Landroid/widget/TextView;

.field private youLockedIconTextView:Landroid/widget/TextView;

.field private youProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;-><init>()V

    return-void
.end method

.method public static inflateView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 247
    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 248
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f03010d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 250
    .local v2, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;-><init>()V

    .line 251
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;
    const v4, 0x7f0e05ee

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    .line 252
    const v4, 0x7f0e05ef

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rareIconView:Landroid/widget/TextView;

    .line 253
    const v4, 0x7f0e05f0

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    .line 254
    const v4, 0x7f0e05f1

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    .line 255
    const v4, 0x7f0e05ed

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    .line 256
    const v4, 0x7f0e05f2

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementDescriptionTextView:Landroid/widget/TextView;

    .line 257
    const v4, 0x7f0e05f3

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementRarityTextView:Landroid/widget/TextView;

    .line 258
    const v4, 0x7f0e05f5

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meCompletedOnView:Landroid/widget/TextView;

    .line 259
    const v4, 0x7f0e05f7

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    .line 260
    const v4, 0x7f0e05f8

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 261
    const v4, 0x7f0e05f9

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 262
    const v4, 0x7f0e05f4

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meLockedIconTextView:Landroid/widget/TextView;

    .line 263
    const v4, 0x7f0e05f6

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youLockedIconTextView:Landroid/widget/TextView;

    .line 264
    const v4, 0x7f0e05eb

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 265
    const v4, 0x7f0e05ec

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->overlay:Landroid/view/View;

    .line 266
    const v4, 0x7f0e05ea

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->imageContainer:Landroid/view/View;

    .line 268
    const v4, 0x7f0e05e9

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 269
    .local v0, "compareAchievementItemContainer":Landroid/widget/LinearLayout;
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 271
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 273
    return-object v2
.end method

.method private isAchievementLocked(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z
    .locals 6
    .param p1, "compareItem"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 441
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 442
    .local v0, "meAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 444
    .local v1, "youAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 456
    :cond_0
    :goto_0
    return v2

    .line 448
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v4, v5, :cond_2

    move v2, v3

    .line 449
    goto :goto_0

    .line 452
    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v4, v5, :cond_0

    move v2, v3

    .line 453
    goto :goto_0
.end method

.method private isCompareAchievementSecret(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z
    .locals 4
    .param p1, "compareItem"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 460
    if-nez p1, :cond_1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 463
    :cond_1
    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v2, :cond_0

    .line 467
    :cond_2
    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_3

    move v0, v1

    .line 468
    goto :goto_0

    .line 471
    :cond_3
    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 472
    goto :goto_0
.end method


# virtual methods
.method public getNarratorContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->narratorContent:Ljava/lang/String;

    return-object v0
.end method

.method public updateUI(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 14
    .param p1, "compareItem"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 277
    if-eqz p1, :cond_c

    .line 278
    iget-object v6, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 279
    .local v6, "youItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v3, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 281
    .local v3, "meItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v2

    .line 282
    .local v2, "meColor":I
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouPreferredColor()I

    move-result v5

    .line 285
    .local v5, "youColor":I
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_d

    .line 286
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->access$000()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 287
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->access$000()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f020125

    const v10, 0x7f020125

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 288
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youLockedIconTextView:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 289
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 297
    :cond_0
    :goto_0
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_e

    .line 298
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->access$100()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 299
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->access$100()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f020125

    const v10, 0x7f020125

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 300
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meLockedIconTextView:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 301
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 308
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->imageContainer:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v7

    if-eqz v7, :cond_f

    const/4 v7, 0x0

    :goto_2
    invoke-static {v8, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 312
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->isCompareAchievementSecret(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z

    move-result v7

    if-nez v7, :cond_12

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v7, :cond_12

    .line 313
    const/4 v0, 0x0

    .line 315
    .local v0, "achievementIconUri":Ljava/lang/String;
    if-eqz v3, :cond_10

    .line 316
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    .line 321
    :cond_2
    :goto_3
    if-eqz v0, :cond_4

    .line 322
    instance-of v7, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-nez v7, :cond_3

    instance-of v7, v6, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v7, :cond_11

    .line 323
    :cond_3
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v8, 0x7f020053

    const v9, 0x7f020055

    invoke-virtual {v7, v0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 333
    .end local v0    # "achievementIconUri":Ljava/lang/String;
    :cond_4
    :goto_4
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->overlay:Landroid/view/View;

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v9, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v7, v9, :cond_13

    :cond_5
    const/4 v7, 0x1

    :goto_5
    invoke-static {v8, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 336
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->isCompareAchievementSecret(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 337
    if-eqz v3, :cond_14

    .line 338
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->name:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 346
    :goto_6
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 348
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->isCompareAchievementSecret(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 349
    if-eqz v3, :cond_16

    .line 350
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementDescriptionTextView:Landroid/widget/TextView;

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->description:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 355
    :goto_7
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementDescriptionTextView:Landroid/widget/TextView;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 358
    :cond_6
    if-eqz v3, :cond_17

    .line 359
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementRarityTextView:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 361
    invoke-virtual {v8}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0705e1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementRarity()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 359
    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 369
    :goto_8
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    if-eqz v7, :cond_7

    .line 370
    if-eqz v6, :cond_18

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_18

    .line 371
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "dateString":Ljava/lang/String;
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v7}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070348

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 373
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youLockedIconTextView:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 384
    .end local v1    # "dateString":Ljava/lang/String;
    :cond_7
    :goto_9
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 385
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 386
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rareIconView:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 388
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->isCompareAchievementSecret(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 389
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 390
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 391
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 392
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 393
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rareIconView:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getIsRare()Z

    move-result v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 394
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->hasRewards()Z

    move-result v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 398
    :cond_8
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meCompletedOnView:Landroid/widget/TextView;

    if-eqz v7, :cond_9

    .line 399
    if-eqz v3, :cond_1a

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_1a

    .line 400
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 402
    .restart local v1    # "dateString":Ljava/lang/String;
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v7}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070348

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 403
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meCompletedOnView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meLockedIconTextView:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    .end local v1    # "dateString":Ljava/lang/String;
    :cond_9
    :goto_a
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v7, :cond_a

    if-eqz v3, :cond_a

    .line 413
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v10

    rsub-int/lit8 v10, v10, 0x64

    int-to-long v10, v10

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 414
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 415
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 416
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 419
    :cond_a
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v7, :cond_b

    if-eqz v6, :cond_b

    .line 420
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v10

    rsub-int/lit8 v10, v10, 0x64

    int-to-long v10, v10

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 421
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7, v5}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 422
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7, v5}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 423
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 427
    :cond_b
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    const-string v9, "%1$s %2$s, %3$s, %4$s %5$s, %6$s %7$s"

    const/4 v7, 0x7

    new-array v10, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    .line 428
    invoke-static {v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v11

    aput-object v11, v10, v7

    const/4 v7, 0x1

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementDescriptionTextView:Landroid/widget/TextView;

    .line 429
    invoke-static {v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v11

    aput-object v11, v10, v7

    const/4 v11, 0x2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    .line 430
    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v12, 0x8

    if-eq v7, v12, :cond_1b

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f070d57

    .line 431
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v12, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    invoke-static {v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 432
    :goto_b
    aput-object v7, v10, v11

    const/4 v7, 0x3

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f07034d

    .line 433
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    const/4 v11, 0x4

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meLockedIconTextView:Landroid/widget/TextView;

    .line 434
    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v12, 0x8

    if-ne v7, v12, :cond_1c

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meCompletedOnView:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v7

    :goto_c
    aput-object v7, v10, v11

    const/4 v7, 0x5

    .line 435
    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouGamerTag()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    const/4 v11, 0x6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youLockedIconTextView:Landroid/widget/TextView;

    .line 436
    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v12, 0x8

    if-ne v7, v12, :cond_1d

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v7

    :goto_d
    aput-object v7, v10, v11

    .line 427
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->narratorContent:Ljava/lang/String;

    .line 438
    .end local v2    # "meColor":I
    .end local v3    # "meItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v5    # "youColor":I
    .end local v6    # "youItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_c
    return-void

    .line 292
    .restart local v2    # "meColor":I
    .restart local v3    # "meItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .restart local v5    # "youColor":I
    .restart local v6    # "youItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_d
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youLockedIconTextView:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 293
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 304
    :cond_e
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meLockedIconTextView:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 305
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 308
    :cond_f
    const/16 v7, 0x8

    goto/16 :goto_2

    .line 317
    .restart local v0    # "achievementIconUri":Ljava/lang/String;
    :cond_10
    if-eqz v6, :cond_2

    .line 318
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 325
    :cond_11
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v8, 0x7f020053

    const v9, 0x7f020055

    invoke-virtual {v7, v0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_4

    .line 329
    .end local v0    # "achievementIconUri":Ljava/lang/String;
    :cond_12
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->gameImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v8, 0x7f020054

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 333
    :cond_13
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 340
    :cond_14
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    iget-object v8, v6, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->name:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 343
    :cond_15
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementNameTextView:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070083

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 352
    :cond_16
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementDescriptionTextView:Landroid/widget/TextView;

    iget-object v8, v6, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->description:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 363
    :cond_17
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->achievementRarityTextView:Landroid/widget/TextView;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 365
    invoke-virtual {v8}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0705e1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementRarity()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 363
    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 375
    :cond_18
    if-eqz v6, :cond_19

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_19

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v7

    if-lez v7, :cond_19

    .line 376
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 377
    .local v4, "percentage":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 379
    .end local v4    # "percentage":Ljava/lang/String;
    :cond_19
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->youCompletedOnView:Landroid/widget/TextView;

    const-string v8, "--"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 405
    :cond_1a
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->InProgress:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v7, v8, :cond_9

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v7

    if-lez v7, :cond_9

    .line 406
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 407
    .restart local v4    # "percentage":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->meCompletedOnView:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 431
    .end local v4    # "percentage":Ljava/lang/String;
    :cond_1b
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070d78

    .line 432
    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_b

    .line 434
    :cond_1c
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070d60

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_c

    .line 436
    :cond_1d
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070d60

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_d
.end method
