.class final Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;
.super Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;
.source "AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final key:Ljava/io/Serializable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final userTriggered:Z


# direct methods
.method constructor <init>(Ljava/io/Serializable;Z)V
    .locals 2
    .param p2, "userTriggered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null key"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->key:Ljava/io/Serializable;

    .line 21
    iput-boolean p2, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->userTriggered:Z

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 49
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;

    .line 50
    .local v0, "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent<*>;"
    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->key:Ljava/io/Serializable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;->key()Ljava/io/Serializable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->userTriggered:Z

    .line 51
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;->userTriggered()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent<*>;"
    :cond_3
    move v1, v2

    .line 53
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    const v2, 0xf4243

    .line 58
    const/4 v0, 0x1

    .line 59
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->key:Ljava/io/Serializable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 61
    mul-int/2addr v0, v2

    .line 62
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->userTriggered:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 63
    return v0

    .line 62
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public key()Ljava/io/Serializable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->key:Ljava/io/Serializable;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatHeadRemovedEvent{key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->key:Ljava/io/Serializable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userTriggered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->userTriggered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userTriggered()Z
    .locals 1

    .prologue
    .line 32
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;->userTriggered:Z

    return v0
.end method
