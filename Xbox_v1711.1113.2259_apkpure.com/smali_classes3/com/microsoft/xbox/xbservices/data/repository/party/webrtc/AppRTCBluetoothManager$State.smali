.class public final enum Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
.super Ljava/lang/Enum;
.source "AppRTCBluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum ERROR:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum SCO_DISCONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

.field public static final enum UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->ERROR:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "HEADSET_UNAVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "HEADSET_AVAILABLE"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "SCO_DISCONNECTING"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_DISCONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "SCO_CONNECTING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    const-string v1, "SCO_CONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 45
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->ERROR:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_DISCONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    return-object v0
.end method
