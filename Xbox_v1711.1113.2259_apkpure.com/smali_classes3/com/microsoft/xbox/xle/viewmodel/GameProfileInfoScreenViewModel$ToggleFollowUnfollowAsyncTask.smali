.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleFollowUnfollowAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 344
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 345
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 372
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getUserTitleFollowingModel()Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->toggle(J)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 367
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 350
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onToggleFollowUnfollowCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 352
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onToggleFollowUnfollowCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 363
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 340
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 356
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 358
    return-void
.end method
