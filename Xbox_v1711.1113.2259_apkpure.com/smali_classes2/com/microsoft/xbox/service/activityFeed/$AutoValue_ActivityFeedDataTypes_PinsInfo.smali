.class abstract Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;
.super Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
.source "$AutoValue_ActivityFeedDataTypes_PinsInfo.java"


# instance fields
.field private final maxPins:I

.field private final numItems:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1, "numItems"    # I
    .param p2, "maxPins"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;-><init>()V

    .line 15
    iput p1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->numItems:I

    .line 16
    iput p2, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->maxPins:I

    .line 17
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    if-ne p1, p0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 43
    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    .line 44
    .local v0, "that":Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    iget v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->numItems:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->numItems()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->maxPins:I

    .line 45
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->maxPins()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    :cond_3
    move v1, v2

    .line 47
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 52
    const/4 v0, 0x1

    .line 53
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 54
    iget v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->numItems:I

    xor-int/2addr v0, v1

    .line 55
    mul-int/2addr v0, v2

    .line 56
    iget v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->maxPins:I

    xor-int/2addr v0, v1

    .line 57
    return v0
.end method

.method public maxPins()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->maxPins:I

    return v0
.end method

.method public numItems()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->numItems:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PinsInfo{numItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->numItems:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxPins="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedDataTypes_PinsInfo;->maxPins:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
