.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_Locator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultExpiration:Ljava/util/Date;

.field private defaultFileSize:Ljava/lang/Integer;

.field private defaultLocatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

.field private defaultUri:Ljava/lang/String;

.field private final expirationAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSizeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final locatorTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;",
            ">;"
        }
    .end annotation
.end field

.field private final uriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultExpiration:Ljava/util/Date;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultFileSize:Ljava/lang/Integer;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultLocatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultUri:Ljava/lang/String;

    .line 30
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->expirationAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->locatorTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->uriAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 72
    const/4 v5, 0x0

    .line 108
    :goto_0
    return-object v5

    .line 74
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultExpiration:Ljava/util/Date;

    .line 76
    .local v1, "expiration":Ljava/util/Date;
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultFileSize:Ljava/lang/Integer;

    .line 77
    .local v2, "fileSize":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultLocatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 78
    .local v3, "locatorType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultUri:Ljava/lang/String;

    .line 79
    .local v4, "uri":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 80
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 82
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 85
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 103
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 85
    :sswitch_0
    const-string v6, "expiration"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "fileSize"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "locatorType"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "uri"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 87
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->expirationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "expiration":Ljava/util/Date;
    check-cast v1, Ljava/util/Date;

    .line 88
    .restart local v1    # "expiration":Ljava/util/Date;
    goto :goto_1

    .line 91
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "fileSize":Ljava/lang/Integer;
    check-cast v2, Ljava/lang/Integer;

    .line 92
    .restart local v2    # "fileSize":Ljava/lang/Integer;
    goto :goto_1

    .line 95
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->locatorTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "locatorType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    check-cast v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 96
    .restart local v3    # "locatorType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    goto :goto_1

    .line 99
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->uriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "uri":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 100
    .restart local v4    # "uri":Ljava/lang/String;
    goto :goto_1

    .line 107
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 108
    new-instance v5, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator;-><init>(Ljava/util/Date;Ljava/lang/Integer;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        -0x31eab551 -> :sswitch_0
        -0x2bd7d463 -> :sswitch_1
        0x1c56c -> :sswitch_3
        0x2b47a4f0 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultExpiration(Ljava/util/Date;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultExpiration"    # Ljava/util/Date;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultExpiration:Ljava/util/Date;

    .line 37
    return-object p0
.end method

.method public setDefaultFileSize(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFileSize"    # Ljava/lang/Integer;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultFileSize:Ljava/lang/Integer;

    .line 41
    return-object p0
.end method

.method public setDefaultLocatorType(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLocatorType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultLocatorType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 45
    return-object p0
.end method

.method public setDefaultUri(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUri"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->defaultUri:Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    if-nez p2, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 67
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 58
    const-string v0, "expiration"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->expirationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->expiration()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 60
    const-string v0, "fileSize"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->fileSize()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 62
    const-string v0, "locatorType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->locatorTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 64
    const-string v0, "uri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->uriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Locator$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;)V

    return-void
.end method
