.class Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PageUserInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadPageInfoAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 222
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 223
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 228
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 230
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 251
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 219
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Z)Z

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateAdapter()V

    .line 246
    return-void
.end method
