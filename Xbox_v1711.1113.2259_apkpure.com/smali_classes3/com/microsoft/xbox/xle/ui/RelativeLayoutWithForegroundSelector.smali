.class public Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;
.super Landroid/widget/RelativeLayout;
.source "RelativeLayoutWithForegroundSelector.java"


# instance fields
.field protected foreground:Landroid/graphics/drawable/Drawable;

.field public foregroundBoundsChanged:Z

.field public foregroundPadding:Z

.field protected final foregroundPaddingRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPaddingRect:Landroid/graphics/Rect;

    .line 20
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundBoundsChanged:Z

    .line 21
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPadding:Z

    .line 37
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 124
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundBoundsChanged:Z

    if-eqz v2, :cond_0

    .line 125
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundBoundsChanged:Z

    .line 127
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getLeft()I

    move-result v3

    sub-int v1, v2, v3

    .line 128
    .local v1, "w":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getTop()I

    move-result v3

    sub-int v0, v2, v3

    .line 130
    .local v0, "h":I
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPadding:Z

    if-nez v2, :cond_2

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v4, v4, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 137
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 139
    :cond_1
    return-void

    .line 133
    .restart local v0    # "h":I
    .restart local v1    # "w":I
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getPaddingRight()I

    move-result v5

    sub-int v5, v1, v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getPaddingBottom()I

    move-result v6

    sub-int v6, v0, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0}, Landroid/widget/RelativeLayout;->drawableStateChanged()V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 99
    :cond_0
    return-void
.end method

.method public getForeground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    .line 41
    sget-object v7, Lcom/microsoft/xboxone/smartglass/R$styleable;->RelativeLayoutWithForegroundSelector:[I

    invoke-virtual {p1, p2, v7, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 42
    .local v1, "array":Landroid/content/res/TypedArray;
    if-eqz v1, :cond_3

    .line 44
    const/high16 v6, -0x80000000

    .line 45
    .local v6, "undefinedResource":I
    const/4 v7, 0x0

    const/high16 v8, -0x80000000

    :try_start_0
    invoke-virtual {v1, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 46
    .local v4, "foregroundResourceId":I
    if-ne v4, v9, :cond_0

    .line 47
    const v4, 0x7f0201f5

    .line 49
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 50
    .local v3, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_1

    .line 51
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->setForegroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 54
    :cond_1
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPadding:Z

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 57
    .local v2, "background":Landroid/graphics/drawable/Drawable;
    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPadding:Z

    if-nez v7, :cond_2

    instance-of v7, v2, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v7, :cond_2

    .line 58
    move-object v0, v2

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    move-object v5, v0

    .line 59
    .local v5, "ninePatchDrawable":Landroid/graphics/drawable/NinePatchDrawable;
    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPaddingRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v7}, Landroid/graphics/drawable/NinePatchDrawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 60
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundPadding:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    .end local v5    # "ninePatchDrawable":Landroid/graphics/drawable/NinePatchDrawable;
    :cond_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    .end local v2    # "background":Landroid/graphics/drawable/Drawable;
    .end local v3    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v4    # "foregroundResourceId":I
    .end local v6    # "undefinedResource":I
    :cond_3
    return-void

    .line 65
    .restart local v6    # "undefinedResource":I
    :catchall_0
    move-exception v7

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v7
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Landroid/widget/RelativeLayout;->jumpDrawablesToCurrentState()V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 112
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 116
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foregroundBoundsChanged:Z

    .line 118
    return-void
.end method

.method protected setForegroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    .line 79
    if-eqz p1, :cond_3

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->setWillNotDraw(Z)V

    .line 81
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 82
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 88
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->requestLayout()V

    .line 89
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->invalidate()V

    .line 91
    :cond_2
    return-void

    .line 86
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RelativeLayoutWithForegroundSelector;->foreground:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
