.class final synthetic Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;

.field private final arg$2:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;

.field private final arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$2:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;

    iput-object p3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;-><init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$2:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->lambda$new$2(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
