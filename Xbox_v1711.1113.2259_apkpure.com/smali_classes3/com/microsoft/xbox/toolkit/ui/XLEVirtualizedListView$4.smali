.class Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "XLEVirtualizedListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p2, "executorService"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->doInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0xa

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->load(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 215
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getFirstVisiblePosition()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->visibleIndexToRealIndex(I)I

    move-result v0

    .line 230
    .local v0, "desiredFirstIndex":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getY()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-int v2, v5

    .line 233
    .local v2, "desiredYOffset":I
    const/4 v4, 0x0

    .line 234
    .local v4, "min":I
    const/16 v5, 0xa

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getSize()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 235
    .local v3, "max":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 237
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$308(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    .line 240
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 242
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onLoadNextDone()V

    .line 244
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onDataUpdated()V

    .line 245
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->realIndexToVisibleIndex(I)I

    move-result v1

    .line 246
    .local v1, "desiredFirstVisible":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/widget/ListView;II)V

    .line 247
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 220
    return-void
.end method
