.class abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;
.source "$AutoValue_MultiplayerDataTypes_CloudComputeProperties.java"


# instance fields
.field private final custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;)V
    .locals 0
    .param p1, "custom"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    .line 15
    return-void
.end method


# virtual methods
.method public custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    if-ne p1, p0, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v1

    .line 35
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;

    .line 37
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;->custom()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;
    :cond_3
    move v1, v2

    .line 39
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x1

    .line 45
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 47
    return v0

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudComputeProperties{custom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputeProperties;->custom:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
