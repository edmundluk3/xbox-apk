.class public Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT:Ljava/lang/String; = "googAutoGainControl"

.field private static final AUDIO_CODEC_ISAC:Ljava/lang/String; = "ISAC"

.field private static final AUDIO_CODEC_OPUS:Ljava/lang/String; = "opus"

.field private static final AUDIO_CODEC_PARAM_BITRATE:Ljava/lang/String; = "maxaveragebitrate"

.field private static final AUDIO_ECHO_CANCELLATION_CONSTRAINT:Ljava/lang/String; = "googEchoCancellation"

.field private static final AUDIO_HIGH_PASS_FILTER_CONSTRAINT:Ljava/lang/String; = "googHighpassFilter"

.field private static final AUDIO_LEVEL_CONTROL_CONSTRAINT:Ljava/lang/String; = "levelControl"

.field private static final AUDIO_NOISE_SUPPRESSION_CONSTRAINT:Ljava/lang/String; = "googNoiseSuppression"

.field public static final AUDIO_TRACK_ID:Ljava/lang/String; = "ARDAMSa0"

.field private static final BPS_IN_KBPS:I = 0x3e8

.field private static final DTLS_SRTP_KEY_AGREEMENT_CONSTRAINT:Ljava/lang/String; = "DtlsSrtpKeyAgreement"

.field private static final TAG:Ljava/lang/String; = "PCRTCClient"

.field private static final VIDEO_CODEC_PARAM_START_BITRATE:Ljava/lang/String; = "x-google-start-bitrate"

.field private static final VIDEO_VP8_INTEL_HW_ENCODER_FIELDTRIAL:Ljava/lang/String; = "WebRTC-IntelVP8/Enabled/"


# instance fields
.field private aecDumpFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private audioConstraints:Lorg/webrtc/MediaConstraints;

.field private audioSource:Lorg/webrtc/AudioSource;

.field private context:Landroid/content/Context;

.field private dataChannel:Lorg/webrtc/DataChannel;

.field private dataChannelEnabled:Z

.field private enableAudio:Z

.field private enableRemoteAudio:Z

.field private events:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

.field private final executor:Ljava/util/concurrent/ScheduledExecutorService;

.field private factory:Lorg/webrtc/PeerConnectionFactory;

.field private isError:Z

.field private isInitiator:Z

.field private localAudioTrack:Lorg/webrtc/AudioTrack;

.field private localSdp:Lorg/webrtc/SessionDescription;

.field private logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private mediaStream:Lorg/webrtc/MediaStream;

.field options:Lorg/webrtc/PeerConnectionFactory$Options;

.field private pcConstraints:Lorg/webrtc/MediaConstraints;

.field private final pcObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

.field private peerConnection:Lorg/webrtc/PeerConnection;

.field private peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

.field private preferIsac:Z

.field private queuedRemoteCandidates:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/webrtc/IceCandidate;",
            ">;"
        }
    .end annotation
.end field

.field private refreshingLocal:Z

.field private remoteStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/webrtc/AudioTrack;",
            ">;"
        }
    .end annotation
.end field

.field private sdpMediaConstraints:Lorg/webrtc/MediaConstraints;

.field private final sdpObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

.field private statsTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .param p2, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    .line 88
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    .line 92
    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    .line 817
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->remoteStreams:Ljava/util/List;

    .line 928
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->refreshingLocal:Z

    .line 227
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 229
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->context:Landroid/content/Context;

    .line 230
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 231
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/AudioTrack;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localAudioTrack:Lorg/webrtc/AudioTrack;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/MediaStreamTrack;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Lorg/webrtc/MediaStreamTrack;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->getStats(Lorg/webrtc/MediaStreamTrack;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->remoteStreams:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableAudio:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableAudio:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/PeerConnection;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isInitiator:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isInitiator:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/MediaConstraints;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpMediaConstraints:Lorg/webrtc/MediaConstraints;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->drainCandidates()V

    return-void
.end method

.method static synthetic access$2200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->preferIsac:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->preferCodec(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;ZLjava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->setStartBitrate(Ljava/lang/String;ZLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannelEnabled:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lorg/webrtc/SessionDescription;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localSdp:Lorg/webrtc/SessionDescription;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/SessionDescription;)Lorg/webrtc/SessionDescription;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Lorg/webrtc/SessionDescription;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localSdp:Lorg/webrtc/SessionDescription;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->refreshingLocal:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->refreshingLocal:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createPeerConnectionFactoryInternal(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createMediaConstraintsInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createPeerConnectionInternal()V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->reportError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->closeInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->events:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    return-object v0
.end method

.method private closeInternal()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 466
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->aecDump:Z

    if-eqz v1, :cond_0

    .line 467
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    invoke-virtual {v1}, Lorg/webrtc/PeerConnectionFactory;->stopAecDump()V

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "Closing peer connection."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->statsTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 471
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->remoteStreams:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 473
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    if-eqz v1, :cond_1

    .line 474
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    invoke-virtual {v1}, Lorg/webrtc/DataChannel;->unregisterObserver()V

    .line 475
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    invoke-virtual {v1}, Lorg/webrtc/DataChannel;->dispose()V

    .line 476
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    .line 478
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    if-eqz v1, :cond_2

    .line 479
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    invoke-virtual {v1}, Lorg/webrtc/PeerConnection;->dispose()V

    .line 480
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    .line 483
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "Closing audio source."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioSource:Lorg/webrtc/AudioSource;

    if-eqz v1, :cond_3

    .line 485
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioSource:Lorg/webrtc/AudioSource;

    invoke-virtual {v1}, Lorg/webrtc/AudioSource;->dispose()V

    .line 486
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioSource:Lorg/webrtc/AudioSource;

    .line 489
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "Closing peer connection factory."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    if-eqz v1, :cond_4

    .line 491
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    invoke-virtual {v1}, Lorg/webrtc/PeerConnectionFactory;->dispose()V

    .line 492
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    .line 495
    :cond_4
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    .line 497
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "Closing peer connection done."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->events:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    .line 500
    .local v0, "pcEvents":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->events:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    .line 501
    invoke-interface {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;->onPeerConnectionClosed()V

    .line 503
    invoke-static {}, Lorg/webrtc/PeerConnectionFactory;->stopInternalTracingCapture()V

    .line 504
    invoke-static {}, Lorg/webrtc/PeerConnectionFactory;->shutdownInternalTracer()V

    .line 505
    return-void
.end method

.method private createAudioTrack()Lorg/webrtc/AudioTrack;
    .locals 3

    .prologue
    .line 662
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    invoke-virtual {v0, v1}, Lorg/webrtc/PeerConnectionFactory;->createAudioSource(Lorg/webrtc/MediaConstraints;)Lorg/webrtc/AudioSource;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioSource:Lorg/webrtc/AudioSource;

    .line 663
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    const-string v1, "ARDAMSa0"

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioSource:Lorg/webrtc/AudioSource;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/PeerConnectionFactory;->createAudioTrack(Ljava/lang/String;Lorg/webrtc/AudioSource;)Lorg/webrtc/AudioTrack;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localAudioTrack:Lorg/webrtc/AudioTrack;

    .line 664
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localAudioTrack:Lorg/webrtc/AudioTrack;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableAudio:Z

    invoke-virtual {v0, v1}, Lorg/webrtc/AudioTrack;->setEnabled(Z)Z

    .line 665
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localAudioTrack:Lorg/webrtc/AudioTrack;

    return-object v0
.end method

.method private createMediaConstraintsInternal()V
    .locals 4

    .prologue
    .line 371
    new-instance v0, Lorg/webrtc/MediaConstraints;

    invoke-direct {v0}, Lorg/webrtc/MediaConstraints;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcConstraints:Lorg/webrtc/MediaConstraints;

    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->loopback:Z

    if-eqz v0, :cond_2

    .line 374
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->optional:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "DtlsSrtpKeyAgreement"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    :goto_0
    new-instance v0, Lorg/webrtc/MediaConstraints;

    invoke-direct {v0}, Lorg/webrtc/MediaConstraints;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->noAudioProcessing:Z

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v1, "PCRTCClient"

    const-string v2, "Disabling audio processing"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "googEchoCancellation"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "googAutoGainControl"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "googHighpassFilter"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "googNoiseSuppression"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->enableLevelControl:Z

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v1, "PCRTCClient"

    const-string v2, "Enabling level control."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->audioConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "levelControl"

    const-string/jumbo v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    :cond_1
    new-instance v0, Lorg/webrtc/MediaConstraints;

    invoke-direct {v0}, Lorg/webrtc/MediaConstraints;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpMediaConstraints:Lorg/webrtc/MediaConstraints;

    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpMediaConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "OfferToReceiveAudio"

    const-string/jumbo v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->sdpMediaConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->mandatory:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "OfferToReceiveVideo"

    const-string v3, "false"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    return-void

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v0, v0, Lorg/webrtc/MediaConstraints;->optional:Ljava/util/List;

    new-instance v1, Lorg/webrtc/MediaConstraints$KeyValuePair;

    const-string v2, "DtlsSrtpKeyAgreement"

    const-string/jumbo v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/webrtc/MediaConstraints$KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private createPeerConnectionFactoryInternal(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 291
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "createPeerConnectionFactoryInternal"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-static {}, Lorg/webrtc/PeerConnectionFactory;->initializeInternalTracer()V

    .line 294
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->tracing:Z

    if-eqz v1, :cond_0

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "webrtc-trace.txt"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 295
    invoke-static {v1}, Lorg/webrtc/PeerConnectionFactory;->startInternalTracingCapture(Ljava/lang/String;)Z

    .line 300
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Create peer connection factory"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    .line 303
    const-string v0, ""

    .line 304
    .local v0, "fieldTrials":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "WebRTC-IntelVP8/Enabled/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 306
    invoke-static {v0}, Lorg/webrtc/PeerConnectionFactory;->initializeFieldTrials(Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Field trials: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->audioCodec:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-object v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->audioCodec:Ljava/lang/String;

    const-string v4, "ISAC"

    .line 311
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->preferIsac:Z

    .line 313
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->useOpenSLES:Z

    if-nez v1, :cond_3

    .line 314
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Disable OpenSL ES audio even if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-static {v2}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    .line 320
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInAEC:Z

    if-eqz v1, :cond_4

    .line 321
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Disable built-in AEC even if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-static {v2}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedAcousticEchoCanceler(Z)V

    .line 327
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInAGC:Z

    if-eqz v1, :cond_5

    .line 328
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Disable built-in AGC even if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-static {v2}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedAutomaticGainControl(Z)V

    .line 334
    :goto_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInNS:Z

    if-eqz v1, :cond_6

    .line 335
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v3, "PCRTCClient"

    const-string v4, "Disable built-in NS even if device supports it"

    invoke-interface {v1, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-static {v2}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedNoiseSuppressor(Z)V

    .line 342
    :goto_4
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-static {v1}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->setErrorCallback(Lorg/webrtc/voiceengine/WebRtcAudioRecord$WebRtcAudioRecordErrorCallback;)V

    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->videoCodecHwAcceleration:Z

    invoke-static {p1, v2, v2, v1}, Lorg/webrtc/PeerConnectionFactory;->initializeAndroidGlobals(Ljava/lang/Object;ZZZ)Z

    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    if-eqz v1, :cond_1

    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Factory networkIgnoreMask option: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    iget v4, v4, Lorg/webrtc/PeerConnectionFactory$Options;->networkIgnoreMask:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_1
    new-instance v1, Lorg/webrtc/PeerConnectionFactory;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    invoke-direct {v1, v2}, Lorg/webrtc/PeerConnectionFactory;-><init>(Lorg/webrtc/PeerConnectionFactory$Options;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "Peer connection factory created."

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    return-void

    :cond_2
    move v1, v3

    .line 311
    goto/16 :goto_0

    .line 317
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Allow OpenSL ES audio if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-static {v3}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->setBlacklistDeviceForOpenSLESUsage(Z)V

    goto/16 :goto_1

    .line 324
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Enable built-in AEC if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-static {v3}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedAcousticEchoCanceler(Z)V

    goto/16 :goto_2

    .line 331
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Enable built-in AGC if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    invoke-static {v3}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedAutomaticGainControl(Z)V

    goto/16 :goto_3

    .line 338
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Enable built-in NS if device supports it"

    invoke-interface {v1, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-static {v3}, Lorg/webrtc/voiceengine/WebRtcAudioUtils;->setWebRtcBasedNoiseSuppressor(Z)V

    goto :goto_4
.end method

.method private createPeerConnectionInternal()V
    .locals 7

    .prologue
    .line 409
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    if-eqz v3, :cond_1

    .line 410
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Peerconnection factory is not created"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Create peer connection."

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PCConstraints: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcConstraints:Lorg/webrtc/MediaConstraints;

    invoke-virtual {v6}, Lorg/webrtc/MediaConstraints;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    .line 417
    new-instance v2, Lorg/webrtc/PeerConnection$RTCConfiguration;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3}, Lorg/webrtc/PeerConnection$RTCConfiguration;-><init>(Ljava/util/List;)V

    .line 422
    .local v2, "rtcConfig":Lorg/webrtc/PeerConnection$RTCConfiguration;
    sget-object v3, Lorg/webrtc/PeerConnection$TcpCandidatePolicy;->DISABLED:Lorg/webrtc/PeerConnection$TcpCandidatePolicy;

    iput-object v3, v2, Lorg/webrtc/PeerConnection$RTCConfiguration;->tcpCandidatePolicy:Lorg/webrtc/PeerConnection$TcpCandidatePolicy;

    .line 423
    sget-object v3, Lorg/webrtc/PeerConnection$BundlePolicy;->MAXBUNDLE:Lorg/webrtc/PeerConnection$BundlePolicy;

    iput-object v3, v2, Lorg/webrtc/PeerConnection$RTCConfiguration;->bundlePolicy:Lorg/webrtc/PeerConnection$BundlePolicy;

    .line 424
    sget-object v3, Lorg/webrtc/PeerConnection$RtcpMuxPolicy;->NEGOTIATE:Lorg/webrtc/PeerConnection$RtcpMuxPolicy;

    iput-object v3, v2, Lorg/webrtc/PeerConnection$RTCConfiguration;->rtcpMuxPolicy:Lorg/webrtc/PeerConnection$RtcpMuxPolicy;

    .line 426
    sget-object v3, Lorg/webrtc/PeerConnection$ContinualGatheringPolicy;->GATHER_CONTINUALLY:Lorg/webrtc/PeerConnection$ContinualGatheringPolicy;

    iput-object v3, v2, Lorg/webrtc/PeerConnection$RTCConfiguration;->continualGatheringPolicy:Lorg/webrtc/PeerConnection$ContinualGatheringPolicy;

    .line 428
    sget-object v3, Lorg/webrtc/PeerConnection$KeyType;->ECDSA:Lorg/webrtc/PeerConnection$KeyType;

    iput-object v3, v2, Lorg/webrtc/PeerConnection$RTCConfiguration;->keyType:Lorg/webrtc/PeerConnection$KeyType;

    .line 429
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcConstraints:Lorg/webrtc/MediaConstraints;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->pcObserver:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    invoke-virtual {v3, v2, v4, v5}, Lorg/webrtc/PeerConnectionFactory;->createPeerConnection(Lorg/webrtc/PeerConnection$RTCConfiguration;Lorg/webrtc/MediaConstraints;Lorg/webrtc/PeerConnection$Observer;)Lorg/webrtc/PeerConnection;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    .line 430
    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannelEnabled:Z

    if-eqz v3, :cond_2

    .line 431
    new-instance v1, Lorg/webrtc/DataChannel$Init;

    invoke-direct {v1}, Lorg/webrtc/DataChannel$Init;-><init>()V

    .line 432
    .local v1, "init":Lorg/webrtc/DataChannel$Init;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget-boolean v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->ordered:Z

    iput-boolean v3, v1, Lorg/webrtc/DataChannel$Init;->ordered:Z

    .line 433
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget-boolean v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->negotiated:Z

    iput-boolean v3, v1, Lorg/webrtc/DataChannel$Init;->negotiated:Z

    .line 434
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->maxRetransmits:I

    iput v3, v1, Lorg/webrtc/DataChannel$Init;->maxRetransmits:I

    .line 435
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->maxRetransmitTimeMs:I

    iput v3, v1, Lorg/webrtc/DataChannel$Init;->maxRetransmitTimeMs:I

    .line 436
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->id:I

    iput v3, v1, Lorg/webrtc/DataChannel$Init;->id:I

    .line 437
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->protocol:Ljava/lang/String;

    iput-object v3, v1, Lorg/webrtc/DataChannel$Init;->protocol:Ljava/lang/String;

    .line 438
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    const-string v4, "PartyChatDataChannel"

    invoke-virtual {v3, v4, v1}, Lorg/webrtc/PeerConnection;->createDataChannel(Ljava/lang/String;Lorg/webrtc/DataChannel$Init;)Lorg/webrtc/DataChannel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    .line 440
    .end local v1    # "init":Lorg/webrtc/DataChannel$Init;
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isInitiator:Z

    .line 443
    const-string v3, "logcat:"

    sget-object v4, Lorg/webrtc/Logging$TraceLevel;->TRACE_DEFAULT:Lorg/webrtc/Logging$TraceLevel;

    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->enableTracing(Ljava/lang/String;Ljava/util/EnumSet;)V

    .line 444
    sget-object v3, Lorg/webrtc/Logging$Severity;->LS_INFO:Lorg/webrtc/Logging$Severity;

    invoke-static {v3}, Lorg/webrtc/Logging;->enableLogToDebugOutput(Lorg/webrtc/Logging$Severity;)V

    .line 446
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    const-string v4, "ARDAMS"

    invoke-virtual {v3, v4}, Lorg/webrtc/PeerConnectionFactory;->createLocalMediaStream(Ljava/lang/String;)Lorg/webrtc/MediaStream;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->mediaStream:Lorg/webrtc/MediaStream;

    .line 447
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->mediaStream:Lorg/webrtc/MediaStream;

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createAudioTrack()Lorg/webrtc/AudioTrack;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/webrtc/MediaStream;->addTrack(Lorg/webrtc/AudioTrack;)Z

    .line 448
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->mediaStream:Lorg/webrtc/MediaStream;

    invoke-virtual {v3, v4}, Lorg/webrtc/PeerConnection;->addStream(Lorg/webrtc/MediaStream;)Z

    .line 450
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    iget-boolean v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->aecDump:Z

    if-eqz v3, :cond_3

    .line 452
    :try_start_0
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 453
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Download/audio.aecdump"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x3c000000    # 0.0078125f

    invoke-static {v3, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->aecDumpFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 457
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->aecDumpFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v4

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Lorg/webrtc/PeerConnectionFactory;->startAecDump(II)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 462
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Peer connection created."

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 458
    :catch_0
    move-exception v0

    .line 459
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v4, "PCRTCClient"

    const-string v5, "Can not open aecdump file"

    invoke-interface {v3, v4, v5, v0}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private drainCandidates()V
    .locals 5

    .prologue
    .line 808
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    if-eqz v1, :cond_1

    .line 809
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Add "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " remote candidates"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/webrtc/IceCandidate;

    .line 811
    .local v0, "candidate":Lorg/webrtc/IceCandidate;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    invoke-virtual {v2, v0}, Lorg/webrtc/PeerConnection;->addIceCandidate(Lorg/webrtc/IceCandidate;)Z

    goto :goto_0

    .line 813
    .end local v0    # "candidate":Lorg/webrtc/IceCandidate;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    .line 815
    :cond_1
    return-void
.end method

.method private static findMediaDescriptionLine(Z[Ljava/lang/String;)I
    .locals 3
    .param p0, "isAudio"    # Z
    .param p1, "sdpLines"    # [Ljava/lang/String;

    .prologue
    .line 731
    if-eqz p0, :cond_0

    const-string v1, "m=audio "

    .line 732
    .local v1, "mediaDescription":Ljava/lang/String;
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 733
    aget-object v2, p1, v0

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 737
    .end local v0    # "i":I
    :goto_2
    return v0

    .line 731
    .end local v1    # "mediaDescription":Ljava/lang/String;
    :cond_0
    const-string v1, "m=video "

    goto :goto_0

    .line 732
    .restart local v0    # "i":I
    .restart local v1    # "mediaDescription":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 737
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private getStats(Lorg/webrtc/MediaStreamTrack;)V
    .locals 4
    .param p1, "track"    # Lorg/webrtc/MediaStreamTrack;

    .prologue
    .line 508
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    if-eqz v1, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$5;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-virtual {v1, v2, p1}, Lorg/webrtc/PeerConnection;->getStats(Lorg/webrtc/StatsObserver;Lorg/webrtc/MediaStreamTrack;)Z

    move-result v0

    .line 517
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 518
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v2, "PCRTCClient"

    const-string v3, "getStats() returns false!"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static joinString(Ljava/lang/Iterable;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "delimiter"    # Ljava/lang/String;
    .param p2, "delimiterAtEnd"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 741
    .local p0, "s":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/CharSequence;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 742
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<+Ljava/lang/CharSequence;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 743
    const-string v2, ""

    .line 752
    :goto_0
    return-object v2

    .line 745
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 746
    .local v0, "buffer":Ljava/lang/StringBuilder;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 747
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 749
    :cond_1
    if-eqz p2, :cond_2

    .line 750
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method static synthetic lambda$setRemoteAudioEnabled$0(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p1, "enable"    # Z

    .prologue
    .line 563
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableRemoteAudio:Z

    .line 564
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->remoteStreams:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/webrtc/AudioTrack;

    .line 565
    .local v0, "remoteTrack":Lorg/webrtc/AudioTrack;
    invoke-virtual {v0, p1}, Lorg/webrtc/AudioTrack;->setEnabled(Z)Z

    goto :goto_0

    .line 567
    .end local v0    # "remoteTrack":Lorg/webrtc/AudioTrack;
    :cond_0
    return-void
.end method

.method private movePayloadTypesToFront(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p2, "mLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "preferredPayloadTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 757
    const-string v4, " "

    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 758
    .local v2, "origLineParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-gt v4, v5, :cond_0

    .line 759
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v5, "PCRTCClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wrong SDP media description format: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const/4 v4, 0x0

    .line 772
    :goto_0
    return-object v4

    .line 762
    :cond_0
    invoke-interface {v2, v6, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 763
    .local v0, "header":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    .line 764
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v5, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 765
    .local v3, "unpreferredPayloadTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 768
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 769
    .local v1, "newLineParts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 770
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 771
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 772
    const-string v4, " "

    invoke-static {v1, v4, v6}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->joinString(Ljava/lang/Iterable;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private preferCodec(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p1, "sdpDescription"    # Ljava/lang/String;
    .param p2, "codec"    # Ljava/lang/String;
    .param p3, "isAudio"    # Z

    .prologue
    const/4 v11, 0x1

    .line 776
    const-string v7, "\r\n"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 777
    .local v4, "lines":[Ljava/lang/String;
    invoke-static {p3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->findMediaDescriptionLine(Z[Ljava/lang/String;)I

    move-result v5

    .line 778
    .local v5, "mLineIndex":I
    const/4 v7, -0x1

    if-ne v5, v7, :cond_1

    .line 779
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v8, "PCRTCClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No mediaDescription line, so can\'t prefer "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    .end local p1    # "sdpDescription":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 784
    .restart local p1    # "sdpDescription":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 786
    .local v2, "codecPayloadTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "^a=rtpmap:(\\d+) "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "(/\\d+)+[\r]?$"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 787
    .local v1, "codecPattern":Ljava/util/regex/Pattern;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v7, v4

    if-ge v3, v7, :cond_3

    .line 788
    aget-object v7, v4, v3

    invoke-virtual {v1, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 789
    .local v0, "codecMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 790
    invoke-virtual {v0, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 787
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 793
    .end local v0    # "codecMatcher":Ljava/util/regex/Matcher;
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 794
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v8, "PCRTCClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No payload types with name "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 797
    :cond_4
    aget-object v7, v4, v5

    invoke-direct {p0, v2, v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->movePayloadTypesToFront(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 798
    .local v6, "newMLine":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 802
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v8, "PCRTCClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Change media description from: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v4, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    aput-object v6, v4, v5

    .line 804
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-static {v7, v8, v11}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->joinString(Ljava/lang/Iterable;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method

.method private reportError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Peerconnection error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$13;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$13;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 659
    return-void
.end method

.method private setStartBitrate(Ljava/lang/String;ZLjava/lang/String;I)Ljava/lang/String;
    .locals 15
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "isVideoCodec"    # Z
    .param p3, "sdpDescription"    # Ljava/lang/String;
    .param p4, "bitrateKbps"    # I

    .prologue
    .line 670
    const-string v11, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 671
    .local v6, "lines":[Ljava/lang/String;
    const/4 v9, -0x1

    .line 672
    .local v9, "rtpmapLineIndex":I
    const/4 v10, 0x0

    .line 673
    .local v10, "sdpFormatUpdated":Z
    const/4 v4, 0x0

    .line 676
    .local v4, "codecRtpMap":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "^a=rtpmap:(\\d+) "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "(/\\d+)+[\r]?$"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 677
    .local v8, "regex":Ljava/lang/String;
    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 678
    .local v3, "codecPattern":Ljava/util/regex/Pattern;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v11, v6

    if-ge v5, v11, :cond_0

    .line 679
    aget-object v11, v6, v5

    invoke-virtual {v3, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 680
    .local v2, "codecMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 681
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 682
    move v9, v5

    .line 686
    .end local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    :cond_0
    if-nez v4, :cond_2

    .line 687
    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v12, "PCRTCClient"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "No rtpmap for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " codec"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    .end local p3    # "sdpDescription":Ljava/lang/String;
    :goto_1
    return-object p3

    .line 678
    .restart local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    .restart local p3    # "sdpDescription":Ljava/lang/String;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 691
    .end local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    :cond_2
    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v12, "PCRTCClient"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Found "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " rtpmap "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " at "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v6, v9

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "^a=fmtp:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " \\w+=\\d+.*[\r]?$"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 695
    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 696
    const/4 v5, 0x0

    :goto_2
    array-length v11, v6

    if-ge v5, v11, :cond_3

    .line 697
    aget-object v11, v6, v5

    invoke-virtual {v3, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 698
    .restart local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 699
    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v12, "PCRTCClient"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Found "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v6, v5

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    if-eqz p2, :cond_5

    .line 701
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v12, v6, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "; x-google-start-bitrate="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v6, v5

    .line 705
    :goto_3
    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v12, "PCRTCClient"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Update remote SDP line: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v6, v5

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const/4 v10, 0x1

    .line 710
    .end local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 711
    .local v7, "newSdpDescription":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    :goto_4
    array-length v11, v6

    if-ge v5, v11, :cond_8

    .line 712
    aget-object v11, v6, v5

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\r\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    if-nez v10, :cond_4

    if-ne v5, v9, :cond_4

    .line 716
    if-eqz p2, :cond_7

    .line 717
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "a=fmtp:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "x-google-start-bitrate"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 723
    .local v1, "bitrateSet":Ljava/lang/String;
    :goto_5
    iget-object v11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v12, "PCRTCClient"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Add remote SDP line: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\r\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    .end local v1    # "bitrateSet":Ljava/lang/String;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 703
    .end local v7    # "newSdpDescription":Ljava/lang/StringBuilder;
    .restart local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    :cond_5
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v12, v6, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "; maxaveragebitrate="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    mul-int/lit16 v12, v0, 0x3e8

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v6, v5

    goto/16 :goto_3

    .line 696
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 720
    .end local v2    # "codecMatcher":Ljava/util/regex/Matcher;
    .restart local v7    # "newSdpDescription":Ljava/lang/StringBuilder;
    :cond_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "a=fmtp:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "maxaveragebitrate"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    mul-int/lit16 v12, v0, 0x3e8

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "bitrateSet":Ljava/lang/String;
    goto :goto_5

    .line 727
    .end local v1    # "bitrateSet":Ljava/lang/String;
    :cond_8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_1
.end method


# virtual methods
.method public addRemoteIceCandidate(Lorg/webrtc/IceCandidate;)V
    .locals 2
    .param p1, "candidate"    # Lorg/webrtc/IceCandidate;

    .prologue
    .line 595
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$10;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$10;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/IceCandidate;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 608
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$3;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 288
    return-void
.end method

.method public createAnswer()V
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$9;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$9;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 593
    return-void
.end method

.method public createOffer()V
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$8;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 581
    return-void
.end method

.method public createPeerConnection()V
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v1, "PCRTCClient"

    const-string v2, "Creating peer connection without initializing factory."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$2;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public createPeerConnectionFactory(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "peerConnectionParameters"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;
    .param p3, "events"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 239
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnectionParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    .line 240
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->events:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    .line 241
    invoke-static {p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannelEnabled:Z

    .line 243
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->factory:Lorg/webrtc/PeerConnectionFactory;

    .line 244
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->peerConnection:Lorg/webrtc/PeerConnection;

    .line 245
    iput-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->preferIsac:Z

    .line 246
    iput-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->isError:Z

    .line 247
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->queuedRemoteCandidates:Ljava/util/LinkedList;

    .line 248
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localSdp:Lorg/webrtc/SessionDescription;

    .line 249
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->mediaStream:Lorg/webrtc/MediaStream;

    .line 250
    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableAudio:Z

    .line 251
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->localAudioTrack:Lorg/webrtc/AudioTrack;

    .line 252
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->statsTimer:Ljava/util/Timer;

    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 259
    return-void

    :cond_0
    move v0, v2

    .line 241
    goto :goto_0
.end method

.method public enableStatsEvents(ZI)V
    .locals 7
    .param p1, "enable"    # Z
    .param p2, "periodMs"    # I

    .prologue
    .line 523
    if-eqz p1, :cond_0

    .line 525
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->statsTimer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$6;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    :goto_0
    return-void

    .line 540
    :catch_0
    move-exception v6

    .line 541
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    const-string v1, "PCRTCClient"

    const-string v2, "Can not schedule statistics timer"

    invoke-interface {v0, v1, v2, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 544
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->statsTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    goto :goto_0
.end method

.method public getDataChannel()Lorg/webrtc/DataChannel;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->dataChannel:Lorg/webrtc/DataChannel;

    return-object v0
.end method

.method public removeRemoteIceCandidates([Lorg/webrtc/IceCandidate;)V
    .locals 2
    .param p1, "candidates"    # [Lorg/webrtc/IceCandidate;

    .prologue
    .line 610
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$11;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$11;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;[Lorg/webrtc/IceCandidate;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 624
    return-void
.end method

.method public setAudioEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 549
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$7;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$7;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 559
    return-void
.end method

.method public setPeerConnectionFactoryOptions(Lorg/webrtc/PeerConnectionFactory$Options;)V
    .locals 0
    .param p1, "options"    # Lorg/webrtc/PeerConnectionFactory$Options;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->options:Lorg/webrtc/PeerConnectionFactory$Options;

    .line 235
    return-void
.end method

.method public setRemoteAudioEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 562
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Z)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 568
    return-void
.end method

.method public setRemoteDescription(Lorg/webrtc/SessionDescription;)V
    .locals 2
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    .line 626
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$12;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$12;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/SessionDescription;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 646
    return-void
.end method
