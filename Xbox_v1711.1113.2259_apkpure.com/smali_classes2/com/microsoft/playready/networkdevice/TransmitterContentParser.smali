.class Lcom/microsoft/playready/networkdevice/TransmitterContentParser;
.super Ljava/lang/Object;
.source "TransmitterContentParser.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ParseContents(Ljava/lang/String;Lcom/microsoft/playready/networkdevice/TransmitterContent;)Ljava/util/List;
    .locals 22
    .param p1, "xml"    # Ljava/lang/String;
    .param p2, "parent"    # Lcom/microsoft/playready/networkdevice/TransmitterContent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 33
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v18, "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;"
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v15

    .line 37
    .local v15, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 38
    invoke-virtual {v15}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v19

    .line 40
    .local v19, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/io/StringReader;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 42
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v14

    .line 43
    .local v14, "eventType":I
    const/4 v12, 0x0

    .line 44
    .local v12, "elementName":Ljava/lang/String;
    const-string v13, ""

    .line 45
    .local v13, "elementValue":Ljava/lang/String;
    new-instance v8, Ljava/util/Hashtable;

    invoke-direct {v8}, Ljava/util/Hashtable;-><init>()V

    .line 46
    .local v8, "props":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 47
    .local v4, "id":Ljava/lang/String;
    const/4 v5, 0x0

    .line 48
    .local v5, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 49
    .local v6, "url":Ljava/lang/String;
    const/4 v7, 0x0

    .line 51
    .local v7, "isContainer":Z
    :goto_0
    const/4 v3, 0x1

    if-eq v14, v3, :cond_d

    .line 53
    const/4 v3, 0x2

    if-ne v14, v3, :cond_4

    .line 55
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 56
    const/16 v17, 0x0

    .line 58
    .local v17, "parseAttribs":Z
    const-string v3, "item"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "videoitem"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "audioitem"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 62
    :cond_0
    const/4 v7, 0x0

    .line 63
    const/16 v17, 0x1

    .line 71
    :cond_1
    :goto_1
    if-eqz v17, :cond_5

    .line 73
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v3

    move/from16 v0, v16

    if-ge v0, v3, :cond_5

    .line 75
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "attrName":Ljava/lang/String;
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    .line 78
    .local v10, "attrValue":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 80
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    .line 83
    :cond_2
    invoke-virtual {v8, v9, v10}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 65
    .end local v9    # "attrName":Ljava/lang/String;
    .end local v10    # "attrValue":Ljava/lang/String;
    .end local v16    # "i":I
    :cond_3
    const-string v3, "container"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 67
    const/4 v7, 0x1

    .line 68
    const/16 v17, 0x1

    goto :goto_1

    .line 87
    .end local v17    # "parseAttribs":Z
    :cond_4
    const/4 v3, 0x4

    if-ne v14, v3, :cond_6

    .line 89
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v13

    .line 133
    :cond_5
    :goto_3
    :try_start_1
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v14

    goto/16 :goto_0

    .line 91
    :cond_6
    const/4 v3, 0x3

    if-ne v14, v3, :cond_5

    .line 93
    :try_start_2
    invoke-interface/range {v19 .. v19}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 95
    const-string v3, "dc:title"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 97
    move-object v5, v13

    .line 128
    :cond_7
    :goto_4
    const-string v13, ""

    goto :goto_3

    .line 99
    :cond_8
    const-string v3, "res"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 101
    move-object v6, v13

    goto :goto_4

    .line 103
    :cond_9
    const-string v3, "item"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "videoitem"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "audioitem"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "container"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_a
    if-eqz v4, :cond_7

    .line 109
    if-nez v5, :cond_b

    move-object v5, v4

    .line 110
    :cond_b
    if-nez v6, :cond_c

    move-object v6, v4

    .line 112
    :cond_c
    new-instance v2, Lcom/microsoft/playready/networkdevice/TransmitterContent;

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/playready/networkdevice/TransmitterContent;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterContent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;)V

    .line 120
    .local v2, "content":Lcom/microsoft/playready/networkdevice/TransmitterContent;
    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    const/4 v4, 0x0

    .line 123
    const/4 v5, 0x0

    .line 124
    const/4 v6, 0x0

    goto :goto_4

    .line 135
    .end local v2    # "content":Lcom/microsoft/playready/networkdevice/TransmitterContent;
    :catch_0
    move-exception v11

    .line 137
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    .line 139
    new-instance v3, Ljava/text/ParseException;

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    const/16 v21, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v3, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1

    .line 143
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "url":Ljava/lang/String;
    .end local v7    # "isContainer":Z
    .end local v8    # "props":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "e":Ljava/io/IOException;
    .end local v12    # "elementName":Ljava/lang/String;
    .end local v13    # "elementValue":Ljava/lang/String;
    .end local v14    # "eventType":I
    .end local v15    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v19    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v11

    .line 145
    .local v11, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v11}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 147
    new-instance v3, Ljava/text/ParseException;

    invoke-virtual {v11}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v11}, Lorg/xmlpull/v1/XmlPullParserException;->getLineNumber()I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v3, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3

    .line 150
    .end local v11    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v6    # "url":Ljava/lang/String;
    .restart local v7    # "isContainer":Z
    .restart local v8    # "props":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v12    # "elementName":Ljava/lang/String;
    .restart local v13    # "elementValue":Ljava/lang/String;
    .restart local v14    # "eventType":I
    .restart local v15    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v19    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :cond_d
    return-object v18
.end method
