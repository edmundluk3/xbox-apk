.class abstract Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;
.super Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
.source "$AutoValue_OOBESessionDataType_OOBESessionResponse.java"


# instance fields
.field private final consoleId:Ljava/lang/String;

.field private final consoleOSVersion:Ljava/lang/String;

.field private final consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

.field private final consoleUpdateProgress:Ljava/lang/Integer;

.field private final consoleUpdateStep:Ljava/lang/Integer;

.field private final encryptionKey:Ljava/lang/String;

.field private final sessionExpiration:Ljava/util/Date;

.field private final sessionId:Ljava/lang/String;

.field private final sessionState:I

.field private final transferToken:Ljava/lang/String;

.field private final userName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;Ljava/lang/String;)V
    .locals 2
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "consoleId"    # Ljava/lang/String;
    .param p3, "sessionState"    # I
    .param p4, "sessionExpiration"    # Ljava/util/Date;
    .param p5, "encryptionKey"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "transferToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "consoleUpdateProgress"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "consoleUpdateStep"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "consoleOSVersion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "consoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "userName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;-><init>()V

    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null sessionId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionId:Ljava/lang/String;

    .line 41
    if-nez p2, :cond_1

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null consoleId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleId:Ljava/lang/String;

    .line 45
    iput p3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionState:I

    .line 46
    if-nez p4, :cond_2

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null sessionExpiration"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_2
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionExpiration:Ljava/util/Date;

    .line 50
    iput-object p5, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    .line 51
    iput-object p6, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    .line 52
    iput-object p7, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    .line 53
    iput-object p8, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    .line 54
    iput-object p9, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    .line 55
    iput-object p10, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 56
    iput-object p11, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    .line 57
    return-void
.end method


# virtual methods
.method public consoleId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleId"
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleId:Ljava/lang/String;

    return-object v0
.end method

.method public consoleOSVersion()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleOSVersion"
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    return-object v0
.end method

.method public consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleSettings"
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    return-object v0
.end method

.method public consoleUpdateProgress()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleUpdateProgress"
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    return-object v0
.end method

.method public consoleUpdateStep()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleUpdateStep"
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    return-object v0
.end method

.method public encryptionKey()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "EncryptionKey"
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    if-ne p1, p0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v1

    .line 157
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 158
    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    .line 159
    .local v0, "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleId:Ljava/lang/String;

    .line 160
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionState:I

    .line 161
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionState()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionExpiration:Ljava/util/Date;

    .line 162
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionExpiration()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 163
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->encryptionKey()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 164
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->transferToken()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    if-nez v3, :cond_5

    .line 165
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateProgress()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    if-nez v3, :cond_6

    .line 166
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateStep()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 167
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleOSVersion()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    if-nez v3, :cond_8

    .line 168
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 169
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->userName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 163
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->encryptionKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 164
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->transferToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 165
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateProgress()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 166
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateStep()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 167
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleOSVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 168
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 169
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->userName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    :cond_a
    move v1, v2

    .line 171
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 176
    const/4 v0, 0x1

    .line 177
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 178
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 179
    mul-int/2addr v0, v3

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 181
    mul-int/2addr v0, v3

    .line 182
    iget v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionState:I

    xor-int/2addr v0, v1

    .line 183
    mul-int/2addr v0, v3

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionExpiration:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 185
    mul-int/2addr v0, v3

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 187
    mul-int/2addr v0, v3

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 189
    mul-int/2addr v0, v3

    .line 190
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 191
    mul-int/2addr v0, v3

    .line 192
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 193
    mul-int/2addr v0, v3

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 195
    mul-int/2addr v0, v3

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 197
    mul-int/2addr v0, v3

    .line 198
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v2

    .line 199
    return v0

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    .line 192
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    .line 194
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 196
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    .line 198
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public sessionExpiration()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionExpiration"
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionExpiration:Ljava/util/Date;

    return-object v0
.end method

.method public sessionId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionId"
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public sessionState()I
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionState"
    .end annotation

    .prologue
    .line 76
    iget v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionState:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBESessionResponse{sessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consoleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionExpiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->sessionExpiration:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", encryptionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->encryptionKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transferToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consoleUpdateProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateProgress:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consoleUpdateStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleUpdateStep:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consoleOSVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleOSVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consoleSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->consoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transferToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TransferToken"
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->transferToken:Ljava/lang/String;

    return-object v0
.end method

.method public userName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserName"
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;->userName:Ljava/lang/String;

    return-object v0
.end method
