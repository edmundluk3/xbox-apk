.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ActivityOverviewActivityAdapter.java"


# instance fields
.field private descriptionsView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

.field private detailSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private deviceRequirementsView:Landroid/widget/TextView;

.field private esrbRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

.field private pegiRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

.field private providersLayout:Landroid/widget/LinearLayout;

.field private providersView:Landroid/widget/TextView;

.field private publisherLayout:Landroid/widget/LinearLayout;

.field private publishersTextView:Landroid/widget/TextView;

.field private releaseYearTextView:Landroid/widget/TextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    .line 41
    const v0, 0x7f0e0175

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->screenBody:Landroid/view/View;

    .line 42
    const v0, 0x7f0e0176

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->content:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->content:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->detailSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 45
    const v0, 0x7f0e017a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->descriptionsView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .line 47
    const v0, 0x7f0e017d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->releaseYearTextView:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0e0181

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->providersView:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0e017f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->publishersTextView:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0e0180

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->providersLayout:Landroid/widget/LinearLayout;

    .line 53
    const v0, 0x7f0e017e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->publisherLayout:Landroid/widget/LinearLayout;

    .line 55
    const v0, 0x7f0e0182

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->deviceRequirementsView:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0e017b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->esrbRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    .line 58
    const v0, 0x7f0e017c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->pegiRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    .line 60
    const v0, 0x7f0e0177

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->launchActivity()V

    return-void
.end method


# virtual methods
.method public updateViewOverride()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->updateLoadingIndicator(Z)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->detailSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->releaseYearTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->descriptionsView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setText(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->getUseAllImage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->pegiRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->pegiRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->pegiRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsViewPEGI;->setVisibility(I)V

    .line 93
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getProviderText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->providersLayout:Landroid/widget/LinearLayout;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->providersView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getProviderText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 101
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getPublisherText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->publisherLayout:Landroid/widget/LinearLayout;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->publishersTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getPublisherText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 109
    :goto_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDeviceRequirementString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->deviceRequirementsView:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->deviceRequirementsView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDeviceRequirementString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 116
    :goto_3
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->esrbRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->esrbRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->esrbRatingView:Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->setVisibility(I)V

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->providersLayout:Landroid/widget/LinearLayout;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->publisherLayout:Landroid/widget/LinearLayout;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;->deviceRequirementsView:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_3
.end method
