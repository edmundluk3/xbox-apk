.class public Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;
.super Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;
.source "TwoLineHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Behavior"
.end annotation


# instance fields
.field private movingSiblingId:I

.field private subTitle:Landroid/view/View;

.field private subTitleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

.field private title:Landroid/view/View;

.field private titleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>()V

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->movingSiblingId:I

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, -0x1

    .line 144
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 136
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->movingSiblingId:I

    .line 145
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoLineHeaderView_Behavior_Params:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 147
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->movingSiblingId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 152
    return-void

    .line 149
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private ensureInterpolators(Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;)V
    .locals 10
    .param p1, "titleContainer"    # Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;
    .param p2, "movingSibling"    # Landroid/view/View;
    .param p3, "b"    # Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    .prologue
    const-wide/16 v2, 0x0

    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->title:Landroid/view/View;

    if-nez v1, :cond_0

    .line 182
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getHeaderView()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->title:Landroid/view/View;

    .line 183
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getPaddingBottom()I

    move-result v4

    sub-int v0, v1, v4

    .line 184
    .local v0, "hTitleContainer":I
    new-instance v1, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getViewHeight(Landroid/view/View;)I

    move-result v4

    int-to-double v4, v4

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->title:Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getViewHeight(Landroid/view/View;)I

    move-result v6

    sub-int v6, v0, v6

    div-int/lit8 v6, v6, 0x2

    int-to-double v6, v6

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;-><init>(DDDD)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->titleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->title:Landroid/view/View;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->titleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getOffset()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y(D)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 188
    .end local v0    # "hTitleContainer":I
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitle:Landroid/view/View;

    if-nez v1, :cond_1

    .line 189
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getSubHeaderView()Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitle:Landroid/view/View;

    .line 190
    new-instance v1, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getViewHeight(Landroid/view/View;)I

    move-result v4

    int-to-double v4, v4

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitle:Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getViewHeight(Landroid/view/View;)I

    move-result v6

    int-to-double v6, v6

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;-><init>(DDDD)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    .line 191
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitle:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getOffset()I

    move-result v3

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 193
    :cond_1
    return-void
.end method

.method private getMovingBehavior(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 197
    .local v1, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    .line 198
    .local v0, "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    instance-of v2, v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    if-eqz v2, :cond_0

    .line 199
    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    .line 201
    .end local v0    # "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    :goto_0
    return-object v0

    .restart local v0    # "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getViewHeight(Landroid/view/View;)I
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 176
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 177
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    return v1
.end method

.method private isMovingSibling(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 205
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 206
    .local v0, "vId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->movingSiblingId:I

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDependencyMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 6
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 166
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->isMovingSibling(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getMovingBehavior(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    move-result-object v0

    .line 168
    .local v0, "b":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    if-eqz v0, :cond_0

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->title:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->titleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getOffset()I

    move-result v3

    neg-int v3, v3

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitle:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->subTitleInterpolator:Lcom/microsoft/xbox/toolkit/functions/LinearFunction;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getOffset()I

    move-result v3

    neg-int v3, v3

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/functions/LinearFunction;->y(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 173
    .end local v0    # "b":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    :cond_0
    return-void
.end method

.method protected onDepenencyLaidOut(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 156
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->isMovingSibling(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    if-eqz v1, :cond_0

    .line 157
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->getMovingBehavior(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    move-result-object v0

    .line 158
    .local v0, "b":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    if-eqz v0, :cond_0

    .line 159
    check-cast p2, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    .end local p2    # "child":Landroid/view/View;
    invoke-direct {p0, p2, p3, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;->ensureInterpolators(Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;)V

    .line 162
    .end local v0    # "b":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    :cond_0
    return-void
.end method
