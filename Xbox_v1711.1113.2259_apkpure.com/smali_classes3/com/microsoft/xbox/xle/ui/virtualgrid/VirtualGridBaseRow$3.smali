.class Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;
.super Ljava/lang/Object;
.source "VirtualGridBaseRow.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->startAnimateBottomFrame(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$3;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 246
    return-void
.end method
