.class public Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;
.super Ljava/lang/Object;
.source "SkypeContentAttachmentFormat.java"


# instance fields
.field public locator:Ljava/lang/String;

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "locator"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;->message:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;->locator:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static getSkypeContentAttachmentFormatJson(Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;)Ljava/lang/String;
    .locals 4
    .param p0, "skypeContentAttachmentFormatJson"    # Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;

    .prologue
    .line 21
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 25
    :goto_0
    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SkypeContentAttachmentFormat"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialization"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const/4 v1, 0x0

    goto :goto_0
.end method
