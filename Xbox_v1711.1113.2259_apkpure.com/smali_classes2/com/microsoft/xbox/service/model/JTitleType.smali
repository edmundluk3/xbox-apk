.class public final enum Lcom/microsoft/xbox/service/model/JTitleType;
.super Ljava/lang/Enum;
.source "JTitleType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/JTitleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum Application:Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum Arcade:Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum Demo:Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum Standard:Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum System:Lcom/microsoft/xbox/service/model/JTitleType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/model/JTitleType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "System"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->System:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "Standard"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Standard:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 7
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "Demo"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Demo:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "Arcade"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Arcade:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "Application"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v7}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/JTitleType;

    const-string v1, "Unknown"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/model/JTitleType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Unknown:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 4
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/JTitleType;

    sget-object v1, Lcom/microsoft/xbox/service/model/JTitleType;->System:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/JTitleType;->Standard:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/JTitleType;->Demo:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/JTitleType;->Arcade:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/JTitleType;->Unknown:Lcom/microsoft/xbox/service/model/JTitleType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->$VALUES:[Lcom/microsoft/xbox/service/model/JTitleType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lcom/microsoft/xbox/service/model/JTitleType;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/microsoft/xbox/service/model/JTitleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->$VALUES:[Lcom/microsoft/xbox/service/model/JTitleType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/JTitleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/microsoft/xbox/service/model/JTitleType;->value:I

    return v0
.end method
