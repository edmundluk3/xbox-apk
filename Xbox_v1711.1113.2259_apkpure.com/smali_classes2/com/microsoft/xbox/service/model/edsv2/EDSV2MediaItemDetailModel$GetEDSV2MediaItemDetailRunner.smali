.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MediaItemDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetEDSV2MediaItemDetailRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V
    .locals 0

    .prologue
    .line 690
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;

    .prologue
    .line 690
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 697
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImpressionGuid()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v1 .. v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->getMediaItemDetail(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v9

    .line 698
    .local v9, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->shouldLoadParent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentItems()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 701
    :try_start_0
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentIDs()Ljava/util/ArrayList;

    move-result-object v8

    .line 702
    .local v8, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 703
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v1

    invoke-interface {v1, v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->getTitleImageFromId(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v10

    .line 704
    .local v10, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setParentItems(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    .end local v8    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_0
    :goto_0
    return-object v9

    .line 706
    :catch_0
    move-exception v0

    .line 707
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "EDSV2MediaItem details"

    const-string v2, "failed to load parent items"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 690
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 723
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    const-wide/16 v0, 0xfae

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 718
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 719
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 693
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
    return-void
.end method
