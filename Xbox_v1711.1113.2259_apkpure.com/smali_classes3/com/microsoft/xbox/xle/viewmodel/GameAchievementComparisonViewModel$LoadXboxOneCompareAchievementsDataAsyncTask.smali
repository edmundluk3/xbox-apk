.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameAchievementComparisonViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadXboxOneCompareAchievementsDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private orderedCompareAchievementList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 759
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 760
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    .line 761
    return-void
.end method

.method private deleteExistingAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 892
    .local p1, "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .local p2, "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 913
    :cond_0
    return-void

    .line 896
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .line 897
    .local v0, "compareAchievement":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    iget-object v5, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v5, :cond_6

    iget-object v5, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v1, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    .line 899
    .local v1, "itemId":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 900
    .local v3, "youItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 901
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 906
    .end local v3    # "youItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 907
    .local v2, "meItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 908
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 897
    .end local v1    # "itemId":Ljava/lang/String;
    .end local v2    # "meItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_6
    iget-object v5, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v1, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    goto :goto_1
.end method

.method private existsInOrderedCompareAchievementList(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Z
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .prologue
    const/4 v1, 0x1

    .line 985
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .line 986
    .local v0, "compareAchievement":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 995
    .end local v0    # "compareAchievement":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    :goto_0
    return v1

    .line 990
    .restart local v0    # "compareAchievement":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    :cond_1
    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 995
    .end local v0    # "compareAchievement":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAchievements(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 999
    .local p1, "rawAchievementList":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    if-eqz p1, :cond_3

    .line 1000
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1001
    .local v1, "achievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 1002
    .local v0, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    instance-of v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v4, :cond_2

    move-object v2, v0

    .line 1003
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 1004
    .local v2, "xboxOneAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->achievementType:Ljava/lang/String;

    const-string v5, "challenge"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v4, v5, :cond_0

    .line 1005
    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1008
    .end local v2    # "xboxOneAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1015
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v1    # "achievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    :cond_3
    const/4 v1, 0x0

    :cond_4
    return-object v1
.end method

.method private orderAchievementsByEarnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 9
    .param p3, "orderByMeFirst"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "x":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .local p2, "y":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    const/4 v4, 0x0

    .line 947
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move-object v1, v4

    .line 981
    :cond_1
    return-object v1

    .line 951
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 952
    .local v1, "orderedCompareItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 953
    .local v2, "xItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->existsInOrderedCompareAchievementList(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 957
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v6, v7, :cond_3

    .line 958
    const/4 v0, 0x0

    .line 959
    .local v0, "itemExists":Z
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 960
    .local v3, "yItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v7, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    iget-object v8, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 961
    const/4 v0, 0x1

    .line 962
    if-eqz p3, :cond_6

    .line 963
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v6, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    .end local v3    # "yItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_5
    :goto_1
    if-nez v0, :cond_3

    .line 972
    if-eqz p3, :cond_7

    .line 973
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v6, v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 965
    .restart local v3    # "yItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_6
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 975
    .end local v3    # "yItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_7
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v6, v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private orderAchievementsByUnearnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
    .locals 7
    .param p3, "orderByMeFirst"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 918
    .local p1, "x":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .local p2, "y":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 919
    :cond_0
    const/4 v0, 0x0

    .line 941
    :cond_1
    return-object v0

    .line 922
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 923
    .local v0, "orderedCompareItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 924
    .local v1, "xItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v4

    if-eqz v4, :cond_3

    .line 928
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 929
    .local v2, "yItem":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 930
    if-eqz p3, :cond_5

    .line 931
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v4, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 933
    :cond_5
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-direct {v4, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private sortAndMergeAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .local p2, "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 868
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 889
    :cond_0
    return-void

    .line 871
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderAchievementsByEarnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 872
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2, v5}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderAchievementsByEarnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 873
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->deleteExistingAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 874
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    invoke-static {p2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 875
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 877
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderAchievementsByUnearnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 878
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->deleteExistingAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 880
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2, v5}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderAchievementsByUnearnedAchievement(Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 881
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->deleteExistingAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 882
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    invoke-static {p2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 883
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 885
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 886
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-direct {v4, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 885
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 765
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 767
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 768
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCompareAchievements(Ljava/lang/String;)Z

    move-result v0

    .line 770
    :goto_0
    return v0

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCompare360Achievements(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 770
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 15

    .prologue
    .line 799
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    if-eqz v11, :cond_4

    .line 800
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1702(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 801
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 802
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1100()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Loading game progress for modern title"

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->forceLoad:Z

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 804
    .local v1, "meAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->forceLoad:Z

    iget-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneCompareAchievements(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v9

    .line 806
    .local v9, "youAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v11

    if-nez v11, :cond_4

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 807
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->getAchievements(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v4

    .line 808
    .local v4, "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getCompareAchievementData(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->getAchievements(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v7

    .line 810
    .local v7, "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    .line 811
    invoke-direct {p0, v4, v7}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->sortAndMergeAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 812
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    iget-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-virtual {v11, v12, v13}, Lcom/microsoft/xbox/service/model/TitleModel;->setOrderedCompareAchievementList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 813
    sget-object v11, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 864
    .end local v1    # "meAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .end local v4    # "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v7    # "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v9    # "youAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :goto_0
    return-object v11

    .line 816
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1100()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Loading game progress for legacy title"

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->forceLoad:Z

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360EarnedAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 818
    .local v2, "meEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->forceLoad:Z

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 819
    .local v3, "meUnearnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->forceLoad:Z

    iget-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360CompareAchievements(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v10

    .line 821
    .local v10, "youEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v11

    if-nez v11, :cond_4

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v11

    if-nez v11, :cond_4

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 822
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->getAchievements(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    .line 823
    .local v5, "rawMeEarnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->getAchievements(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v6

    .line 824
    .local v6, "rawMeUnearnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getCompare360AchievementData(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->getAchievements(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v8

    .line 825
    .local v8, "rawYouEarnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 826
    .restart local v4    # "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 828
    .restart local v7    # "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    if-eqz v5, :cond_1

    .line 829
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 833
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2002(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    .line 834
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2102(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    .line 835
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 836
    .local v0, "earnedAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2000(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)I

    move-result v13

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    add-int/2addr v13, v14

    invoke-static {v12, v13}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2002(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    goto :goto_1

    .line 840
    .end local v0    # "earnedAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_1
    if-eqz v6, :cond_2

    .line 841
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 844
    :cond_2
    if-eqz v8, :cond_3

    .line 845
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 847
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2202(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    .line 848
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2302(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    .line 850
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 851
    .restart local v0    # "earnedAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v12}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)I

    move-result v13

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    add-int/2addr v13, v14

    invoke-static {v12, v13}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$2202(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I

    goto :goto_2

    .line 855
    .end local v0    # "earnedAchievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_3
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    .line 856
    invoke-direct {p0, v4, v7}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->sortAndMergeAchievements(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 857
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    iget-object v13, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->orderedCompareAchievementList:Ljava/util/ArrayList;

    invoke-virtual {v11, v12, v13}, Lcom/microsoft/xbox/service/model/TitleModel;->setOrderedCompareAchievementList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 858
    sget-object v11, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    .line 864
    .end local v2    # "meEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .end local v3    # "meUnearnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .end local v4    # "rawMeAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v5    # "rawMeEarnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v6    # "rawMeUnearnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v7    # "rawYouAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v8    # "rawYouEarnedAchievements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v10    # "youEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_4
    sget-object v11, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 794
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 776
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 777
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadCompareAchievementsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 778
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 789
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadCompareAchievementsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 790
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 754
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 782
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 783
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1902(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 784
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 785
    return-void
.end method
