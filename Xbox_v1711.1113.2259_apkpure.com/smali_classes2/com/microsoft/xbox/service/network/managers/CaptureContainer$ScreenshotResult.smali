.class public Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
.super Ljava/lang/Object;
.source "CaptureContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/CaptureContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenshotResult"
.end annotation


# instance fields
.field public screenshot:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;

    return-object v0
.end method
