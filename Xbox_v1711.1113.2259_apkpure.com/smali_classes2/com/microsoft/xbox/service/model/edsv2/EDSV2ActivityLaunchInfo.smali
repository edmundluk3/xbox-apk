.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
.super Ljava/lang/Object;
.source "EDSV2ActivityLaunchInfo.java"


# instance fields
.field private activityUrl:Ljava/net/URI;

.field private deepLinkUrl:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private requiresCapabilities:I

.field private usesCapabilities:I

.field private whitelistUrls:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActivityUrl()Ljava/net/URI;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->activityUrl:Ljava/net/URI;

    return-object v0
.end method

.method public getActivityUrlString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getActivityUrl()Ljava/net/URI;

    move-result-object v0

    .line 49
    .local v0, "activityUri":Ljava/net/URI;
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 52
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeepLinkUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->deepLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getRequiresCapabilities()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->requiresCapabilities:I

    return v0
.end method

.method public getUsesCapabilities()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->usesCapabilities:I

    return v0
.end method

.method public getWhitelistUrls()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->whitelistUrls:[Ljava/lang/String;

    return-object v0
.end method

.method public setActivityUrl(Ljava/net/URI;)V
    .locals 0
    .param p1, "url"    # Ljava/net/URI;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->activityUrl:Ljava/net/URI;

    .line 17
    return-void
.end method

.method public setDeepLinkUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->deepLinkUrl:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->packageName:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setRequiresCapabilities(I)V
    .locals 0
    .param p1, "capabilities"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->requiresCapabilities:I

    .line 33
    return-void
.end method

.method public setUsesCapabilities(I)V
    .locals 0
    .param p1, "capabilities"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->usesCapabilities:I

    .line 41
    return-void
.end method

.method public setWhitelistUrls([Ljava/lang/String;)V
    .locals 0
    .param p1, "urls"    # [Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->whitelistUrls:[Ljava/lang/String;

    .line 25
    return-void
.end method
