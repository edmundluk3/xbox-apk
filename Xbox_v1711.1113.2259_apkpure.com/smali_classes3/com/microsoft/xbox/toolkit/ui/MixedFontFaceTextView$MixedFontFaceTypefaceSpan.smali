.class public Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;
.super Landroid/text/style/TypefaceSpan;
.source "MixedFontFaceTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MixedFontFaceTypefaceSpan"
.end annotation


# static fields
.field private static final color_override:I = 0xffffff


# instance fields
.field private family:Ljava/lang/String;

.field private textColorOverride:I

.field private textSize:F

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;

.field private typeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "typeface"    # Landroid/graphics/Typeface;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->this$0:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;

    .line 54
    invoke-direct {p0, p2}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    .line 50
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    .line 51
    const v0, 0xffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    .line 55
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->typeface:Landroid/graphics/Typeface;

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;F)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "typeface"    # Landroid/graphics/Typeface;
    .param p4, "textSize"    # F

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->this$0:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;

    .line 59
    invoke-direct {p0, p2}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    .line 50
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    .line 51
    const v0, 0xffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    .line 60
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->typeface:Landroid/graphics/Typeface;

    .line 61
    iput p4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;Ljava/lang/String;Landroid/graphics/Typeface;FI)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "typeface"    # Landroid/graphics/Typeface;
    .param p4, "textSize"    # F
    .param p5, "color"    # I

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->this$0:Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView;

    .line 65
    invoke-direct {p0, p2}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    .line 50
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    .line 51
    const v0, 0xffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    .line 66
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->typeface:Landroid/graphics/Typeface;

    .line 67
    iput p4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    .line 68
    iput p5, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    .line 69
    return-void
.end method

.method private my_apply(Landroid/graphics/Paint;Ljava/lang/String;)V
    .locals 6
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "family"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    .line 89
    .local v1, "old":Landroid/graphics/Typeface;
    if-nez v1, :cond_3

    .line 90
    const/4 v2, 0x0

    .line 95
    .local v2, "oldStyle":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->typeface:Landroid/graphics/Typeface;

    .line 96
    .local v3, "tf":Landroid/graphics/Typeface;
    invoke-virtual {v3}, Landroid/graphics/Typeface;->getStyle()I

    move-result v4

    xor-int/lit8 v4, v4, -0x1

    and-int v0, v2, v4

    .line 98
    .local v0, "fake":I
    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_0

    .line 99
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 102
    :cond_0
    and-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_1

    .line 103
    const/high16 v4, -0x41800000    # -0.25f

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 106
    :cond_1
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_4

    .line 107
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 112
    :goto_1
    invoke-virtual {p1, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 114
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    const v5, 0xffffff

    if-eq v4, v5, :cond_2

    .line 115
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 117
    :cond_2
    return-void

    .line 92
    .end local v0    # "fake":I
    .end local v2    # "oldStyle":I
    .end local v3    # "tf":Landroid/graphics/Typeface;
    :cond_3
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v2

    .restart local v2    # "oldStyle":I
    goto :goto_0

    .line 109
    .restart local v0    # "fake":I
    .restart local v3    # "tf":Landroid/graphics/Typeface;
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textSize:F

    goto :goto_1
.end method


# virtual methods
.method public setOverrideColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->textColorOverride:I

    .line 73
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1, "ds"    # Landroid/text/TextPaint;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->family:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->my_apply(Landroid/graphics/Paint;Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1, "paint"    # Landroid/text/TextPaint;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->family:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/MixedFontFaceTextView$MixedFontFaceTypefaceSpan;->my_apply(Landroid/graphics/Paint;Ljava/lang/String;)V

    .line 83
    return-void
.end method
