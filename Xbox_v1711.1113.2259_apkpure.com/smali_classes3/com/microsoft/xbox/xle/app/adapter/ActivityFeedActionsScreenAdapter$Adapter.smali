.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;
.super Landroid/widget/BaseAdapter;
.source "ActivityFeedActionsScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Adapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$1;

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    return-void
.end method

.method private createSpinner()Landroid/view/View;
    .locals 6

    .prologue
    .line 496
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030025

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 497
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f0e012f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 498
    .local v0, "spinner":Landroid/widget/Spinner;
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 518
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 529
    return-object v1
.end method

.method private getViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 533
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 534
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 535
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->DATA:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    .line 545
    :goto_0
    return-object v0

    .line 537
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->INFO:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    goto :goto_0

    .line 539
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1300(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 540
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->isShared()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->FEED_ITEM_SHARED:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    goto :goto_0

    .line 543
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->FEED_ITEM_NORMAL:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    goto :goto_0

    .line 545
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->SPINNER:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 386
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-object v0

    .line 389
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_0

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1000(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 364
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->getItem(I)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 397
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 376
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->getViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 402
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->getViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    move-result-object v13

    .line 405
    .local v13, "viewType":Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;
    move-object/from16 v12, p2

    .line 406
    .local v12, "v":Landroid/view/View;
    if-nez v12, :cond_0

    .line 407
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$7;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$ActivityFeedActionsScreenAdapter$ViewType:[I

    invoke-virtual {v13}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 445
    :cond_0
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$7;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$ActivityFeedActionsScreenAdapter$ViewType:[I

    invoke-virtual {v13}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 492
    :cond_1
    :goto_1
    return-object v12

    .line 409
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030027

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 410
    const v0, 0x7f0e0134

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 411
    .local v7, "container":Landroid/view/ViewGroup;
    const v0, 0x7f0301b0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createView(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Landroid/view/View;

    move-result-object v8

    .line 412
    .local v8, "feedItem":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 413
    const v0, 0x7f0e08ba

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 414
    .local v10, "shared":Landroid/view/View;
    if-eqz v10, :cond_2

    .line 415
    const v0, 0x7f0c0019

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 417
    :cond_2
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 421
    .end local v7    # "container":Landroid/view/ViewGroup;
    .end local v8    # "feedItem":Landroid/view/View;
    .end local v10    # "shared":Landroid/view/View;
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030027

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 422
    const v0, 0x7f0e0134

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 423
    .restart local v7    # "container":Landroid/view/ViewGroup;
    const v0, 0x7f0301ab

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createView(ILandroid/view/ViewGroup;Landroid/view/LayoutInflater;Z)Landroid/view/View;

    move-result-object v8

    .line 424
    .restart local v8    # "feedItem":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 425
    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 429
    .end local v7    # "container":Landroid/view/ViewGroup;
    .end local v8    # "feedItem":Landroid/view/View;
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->createSpinner()Landroid/view/View;

    move-result-object v12

    .line 430
    goto/16 :goto_0

    .line 432
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03013c

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 433
    goto/16 :goto_0

    .line 435
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030024

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 436
    new-instance v9, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;

    invoke-direct {v9, v12}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;-><init>(Landroid/view/View;)V

    .line 437
    .local v9, "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->init()V

    .line 438
    iget-object v0, v9, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 447
    .end local v9    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$SocialActionItemViewHolder;
    :pswitch_5
    const v0, 0x7f0e0133

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 448
    .local v11, "sp":Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 450
    const v0, 0x7f0e0885

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 451
    .local v1, "row":Landroid/view/View;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->adjustFeedItemViewForActionList(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->bindSocialBar(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 454
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 455
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachGameprofileListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 456
    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Landroid/view/View;)V

    .line 457
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_OLD:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    goto/16 :goto_1

    .line 462
    .end local v1    # "row":Landroid/view/View;
    .end local v11    # "sp":Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    :pswitch_6
    const v0, 0x7f0e0133

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 463
    .restart local v11    # "sp":Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 464
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 465
    const v0, 0x7f0e0885

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 466
    .restart local v1    # "row":Landroid/view/View;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 467
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->adjustFeedItemViewForActionList(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 468
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->bindSocialBar(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 469
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachGameprofileListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    .line 471
    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->detachItemListeners(Landroid/view/View;)V

    .line 472
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->NAV_OLD:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V

    goto/16 :goto_1

    .line 477
    .end local v1    # "row":Landroid/view/View;
    .end local v11    # "sp":Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v2

    const v0, 0x7f0e012f

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->bindSpinner(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/widget/Spinner;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    goto/16 :goto_1

    .line 480
    :pswitch_8
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v12}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->bindInfoRow(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V

    goto/16 :goto_1

    .line 483
    :pswitch_9
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$1000(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;

    .line 484
    .local v6, "actionItem":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    if-eqz v6, :cond_1

    .line 485
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v12, v6}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->bindActionItem(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    .line 486
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v12, v6}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->updateCommentsSocialBar(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-static {v0, v12, v6}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachCommentSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    goto/16 :goto_1

    .line 407
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 445
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 381
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
