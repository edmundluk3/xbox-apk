.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;
.super Ljava/lang/Object;
.source "EDSV2MediaItemDeserializer.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "vc":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static getMediaItemClass(I)Ljava/lang/Class;
    .locals 2
    .param p0, "mediaType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 76
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;->getMediaItemClass(Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;)Ljava/lang/Class;

    move-result-object v1

    return-object v1
.end method

.method private static getMediaItemClass(Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;)Ljava/lang/Class;
    .locals 3
    .param p0, "mediaType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer$1;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV3MediaType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 140
    const-string v0, "EDSV2MediaItemDeserialzier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "don\'t know the type for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 85
    :pswitch_0
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    goto :goto_0

    .line 95
    :pswitch_1
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    goto :goto_0

    .line 106
    :pswitch_2
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    goto :goto_0

    .line 114
    :pswitch_3
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    goto :goto_0

    .line 116
    :pswitch_4
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    goto :goto_0

    .line 118
    :pswitch_5
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVShowMediaItem;

    goto :goto_0

    .line 121
    :pswitch_6
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    goto :goto_0

    .line 124
    :pswitch_7
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    goto :goto_0

    .line 127
    :pswitch_8
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    goto :goto_0

    .line 130
    :pswitch_9
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    goto :goto_0

    .line 133
    :pswitch_a
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    goto :goto_0

    .line 135
    :pswitch_b
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicVideoMediaItem;

    goto :goto_0

    .line 137
    :pswitch_c
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static getMediaItemClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 4
    .param p0, "mediaTypeName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 63
    .local v1, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 69
    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;->getMediaItemClass(Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;)Ljava/lang/Class;

    move-result-object v2

    return-object v2

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t parse mediatype "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 9
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeofT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonIOException;
        }
    .end annotation

    .prologue
    .line 25
    const/4 v3, 0x0

    .line 26
    .local v3, "mediaType":I
    const/4 v4, 0x0

    .line 28
    .local v4, "mediaTypeName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v6

    .line 29
    .local v6, "root":Lcom/google/gson/JsonObject;
    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 30
    .local v1, "entrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 31
    .local v0, "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 32
    .local v5, "name":Ljava/lang/String;
    const-string v8, "MediaItemType"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 34
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/gson/JsonElement;

    invoke-virtual {v7}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v4

    .line 42
    .end local v0    # "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    .end local v5    # "name":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v2, 0x0

    .line 43
    .local v2, "itemClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 44
    invoke-static {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;->getMediaItemClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 49
    :goto_1
    if-nez v2, :cond_4

    .line 53
    const/4 v7, 0x0

    .line 56
    :goto_2
    return-object v7

    .line 36
    .end local v2    # "itemClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .restart local v0    # "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    .restart local v5    # "name":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/gson/JsonElement;

    invoke-virtual {v7}, Lcom/google/gson/JsonElement;->getAsInt()I

    move-result v3

    .line 38
    goto :goto_0

    .line 46
    .end local v0    # "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    .end local v5    # "name":Ljava/lang/String;
    .restart local v2    # "itemClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_3
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;->getMediaItemClass(I)Ljava/lang/Class;

    move-result-object v2

    goto :goto_1

    .line 56
    :cond_4
    invoke-virtual {v6}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    goto :goto_2
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDeserializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method
