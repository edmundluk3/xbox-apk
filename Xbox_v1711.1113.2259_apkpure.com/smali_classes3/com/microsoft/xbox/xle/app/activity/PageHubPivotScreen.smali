.class public Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "PageHubPivotScreen.java"


# instance fields
.field private pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "Page Hub"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 20
    const v2, 0x7f03019d

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->setContentView(I)V

    .line 22
    const v2, 0x7f0e082f

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 23
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0c0149

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 25
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 27
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 28
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPageUserAuthorInfo()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-result-object v0

    .line 30
    .local v0, "info":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 31
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PageHubPivotScreen;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 33
    :cond_0
    return-void
.end method
