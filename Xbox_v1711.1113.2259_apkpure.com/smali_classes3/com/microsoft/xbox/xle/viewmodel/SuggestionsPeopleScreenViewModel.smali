.class public Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "SuggestionsPeopleScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$AddUserToFollowingListAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

.field private allPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private currentFilterOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;",
            ">;"
        }
    .end annotation
.end field

.field private filteredList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingRecommendationsData:Z

.field private loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

.field private meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 82
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 84
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    .line 88
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v2

    .line 91
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    if-eqz v2, :cond_0

    .line 92
    const-class v3, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 93
    .local v1, "obj":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v3, v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 94
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 97
    .end local v1    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz v0, :cond_1

    .line 98
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 102
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->updateFilters()V

    .line 103
    return-void

    .line 100
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ALL_PEOPLE:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->isLoadingRecommendationsData:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private buildFilteredList()V
    .locals 4

    .prologue
    .line 256
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->filteredList:Ljava/util/ArrayList;

    .line 258
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->allPeople:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 259
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ALL_PEOPLE:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-ne v2, v3, :cond_1

    .line 260
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->filteredList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->allPeople:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->allPeople:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 264
    .local v1, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    instance-of v2, v1, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    if-eqz v2, :cond_2

    move-object v2, v1

    .line 265
    check-cast v2, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->mapRecommendationTypeToFilter(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;)I

    move-result v0

    .line 266
    .local v0, "index":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ordinal()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 267
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->filteredList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 272
    .end local v0    # "index":I
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-ne v2, v3, :cond_0

    goto :goto_0
.end method

.method private mapRecommendationTypeToFilter(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;)I
    .locals 2
    .param p1, "type"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    .prologue
    .line 291
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$IPeopleHubResult$RecommendationType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 302
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 294
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PYMK:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ordinal()I

    move-result v0

    goto :goto_0

    .line 296
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->VIPS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ordinal()I

    move-result v0

    goto :goto_0

    .line 298
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->FACEBOOK_FRIENDS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ordinal()I

    move-result v0

    goto :goto_0

    .line 300
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ordinal()I

    move-result v0

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 344
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 368
    :goto_0
    :pswitch_0
    return-void

    .line 346
    :pswitch_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->load(Z)V

    goto :goto_0

    .line 353
    :pswitch_2
    const/4 v1, 0x0

    .line 354
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 355
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAddUserToFollowingResult()Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    move-result-object v1

    .line 359
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 360
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->getAddFollowingRequestStatus()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->code:I

    const/16 v3, 0x404

    if-ne v2, v3, :cond_1

    .line 361
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->description:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    goto :goto_0

    .line 363
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    const v3, 0x7f070ac8

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private updateFilters()V
    .locals 7

    .prologue
    .line 241
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 242
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->FACEBOOK_FRIENDS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-ne v0, v5, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->isFacebookFriendFinderOptedIn()Z

    move-result v5

    if-nez v5, :cond_1

    .line 241
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 244
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-ne v0, v5, :cond_2

    .line 245
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v1

    .line 246
    .local v1, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v5, v6, :cond_0

    .line 251
    .end local v1    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 253
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    :cond_3
    return-void
.end method


# virtual methods
.method public addToFriend(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 2
    .param p1, "gamer"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 174
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 175
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$AddUserToFollowingListAsyncTask;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$AddUserToFollowingListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$AddUserToFollowingListAsyncTask;->load(Z)V

    .line 176
    return-void
.end method

.method protected createAdapter()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    if-nez v0, :cond_0

    .line 199
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Not creating adapter because adapter provider is not set"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;->getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    goto :goto_0
.end method

.method public getCurrentFilters()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->filteredList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSuggestionsFilter()I
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public gotoGamerProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 2
    .param p1, "gamer"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 159
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 161
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsDummy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 163
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 166
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 167
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowFriendAction(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 169
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 171
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->isLoadingRecommendationsData:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 137
    if-eqz p1, :cond_0

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubFriendFinderState()V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->cancel()V

    .line 143
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->load(Z)V

    .line 145
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/CallbackManager;->onActivityResult(IILandroid/content/Intent;)Z

    .line 110
    :cond_0
    return-void
.end method

.method public onLoadRecommendationsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 218
    const-string v0, "SuggestionsPeopleScreen ViewModel"

    const-string v1, "onLoadRecommendations Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->isLoadingRecommendationsData:Z

    .line 221
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 237
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->updateAdapter()V

    .line 238
    return-void

    .line 225
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubRecommendationsData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->allPeople:Ljava/util/ArrayList;

    .line 226
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->buildFilteredList()V

    .line 227
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 231
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->allPeople:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 232
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->cancel()V

    .line 152
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->createAdapter()V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 121
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$LoadRecommendationsAsyncTask;->cancel()V

    .line 133
    :cond_0
    return-void
.end method

.method public setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 0
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 195
    return-void
.end method

.method public setSuggestionsPeopleFilter(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 179
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->currentFilterOptions:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 181
    .local v0, "newFilter":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    if-eq v0, v1, :cond_0

    .line 182
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->suggestionsFilter:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 183
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->buildFilteredList()V

    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->updateViewModelState()V

    .line 185
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->updateAdapter()V

    .line 187
    :cond_0
    return-void

    .line 179
    .end local v0    # "newFilter":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected updateViewModelState()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->filteredList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->isLoadingRecommendationsData:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 211
    :goto_1
    return-void

    .line 207
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 209
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method
