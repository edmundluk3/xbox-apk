.class public interface abstract Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
.super Ljava/lang/Object;
.source "PeopleHubService.java"


# virtual methods
.method public abstract getFriends()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "users/me/people/social/decoration/multiplayersummary,preferredcolor"
    .end annotation
.end method

.method public abstract getSummaries(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;)Lio/reactivex/Single;
    .param p1    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "users/me/people/batch/decoration/multiplayersummary,preferredcolor"
    .end annotation
.end method
