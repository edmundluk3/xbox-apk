.class public abstract Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
.super Ljava/lang/Object;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ContinuationUris"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_ContinuationUris$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_ContinuationUris$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_desktop"
    .end annotation
.end method

.method public abstract uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUri;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_xboxone"
    .end annotation
.end method
