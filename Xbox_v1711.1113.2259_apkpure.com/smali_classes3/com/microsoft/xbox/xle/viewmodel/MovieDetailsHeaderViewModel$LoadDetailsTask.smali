.class Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MovieDetailsHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$1;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;Z)Z

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->updateAdapter()V

    .line 117
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;Z)Z

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;->updateAdapter()V

    .line 111
    return-void
.end method
