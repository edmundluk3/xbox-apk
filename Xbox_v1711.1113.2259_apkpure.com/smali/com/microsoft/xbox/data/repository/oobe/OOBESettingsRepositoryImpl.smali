.class public Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;
.super Ljava/lang/Object;
.source "OOBESettingsRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final PREFS_KEY:Ljava/lang/String; = "oobe_settings_prefs"


# instance fields
.field private consoleId:Ljava/lang/String;

.field private currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

.field private final dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

.field private sessionCode:Ljava/lang/String;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)V
    .locals 4
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "oobe_settings_prefs"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 33
    return-void
.end method


# virtual methods
.method public getConsoleId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->consoleId:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionCode()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sessionCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public setAutoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 4
    .param p1, "autoDST"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->autoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oobe_settings_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 95
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->toString(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public setAutoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 4
    .param p1, "autoUpdateApps"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 45
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->autoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oobe_settings_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 50
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->toString(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public setAutoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 4
    .param p1, "autoUpdateSystem"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->autoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oobe_settings_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 65
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->toString(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 66
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public setConsoleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 132
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->consoleId:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setInstantOn(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 4
    .param p1, "instantOn"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 75
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 76
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->instantOn(Ljava/lang/Boolean;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oobe_settings_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 80
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->toString(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public setSessionCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 120
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 121
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sessionCode:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 4
    .param p1, "timeZone"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 104
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 107
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->timeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;->build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "oobe_settings_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 112
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;->toString(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;->currentSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method
