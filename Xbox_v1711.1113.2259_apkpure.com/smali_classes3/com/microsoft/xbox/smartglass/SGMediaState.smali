.class public Lcom/microsoft/xbox/smartglass/SGMediaState;
.super Lcom/microsoft/xbox/smartglass/MediaState;
.source "SGMediaState.java"


# instance fields
.field public timeStamp:J


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V
    .locals 3
    .param p1, "titleId"    # I
    .param p2, "assetId"    # Ljava/lang/String;
    .param p3, "aumId"    # Ljava/lang/String;
    .param p4, "mediaType"    # I
    .param p5, "soundLevel"    # I
    .param p6, "enabledCommands"    # I
    .param p7, "playbackStatus"    # I
    .param p8, "playbackRate"    # F
    .param p9, "position"    # J
    .param p11, "mediaStart"    # J
    .param p13, "mediaEnd"    # J
    .param p15, "minSeekTicks"    # J
    .param p17, "maxSeekTicks"    # J
    .param p19, "metaData"    # [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .prologue
    .line 29
    invoke-direct/range {p0 .. p19}, Lcom/microsoft/xbox/smartglass/MediaState;-><init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->timeStamp:J

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 23
    .param p1, "m"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 34
    move-object/from16 v0, p1

    iget v4, v0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/microsoft/xbox/smartglass/MediaState;->aumId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/MediaType;->getValue()I

    move-result v7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->soundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/SoundLevel;->getValue()I

    move-result v8

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue(Ljava/util/EnumSet;)I

    move-result v9

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->getValue()I

    move-result v10

    move-object/from16 v0, p1

    iget v11, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/microsoft/xbox/smartglass/MediaState;->position:J

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->minSeekTicks:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->maxSeekTicks:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .line 35
    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    move-object/from16 v3, p0

    .line 34
    invoke-direct/range {v3 .. v22}, Lcom/microsoft/xbox/smartglass/SGMediaState;-><init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V

    .line 36
    return-void
.end method

.method public static getEnabledCommandsValue(Ljava/util/EnumSet;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "commands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    const/4 v1, 0x0

    .line 40
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 41
    .local v0, "item":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v3

    or-int/2addr v1, v3

    .line 42
    goto :goto_0

    .line 43
    .end local v0    # "item":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :cond_0
    return v1
.end method


# virtual methods
.method public areEqualExceptPosition(Lcom/microsoft/xbox/smartglass/MediaState;)Z
    .locals 8
    .param p1, "other"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 57
    iget v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->titleId:I

    iget v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->assetId:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    .line 58
    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->aumId:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->aumId:Ljava/lang/String;

    .line 59
    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->enabledCommands:Ljava/util/EnumSet;

    .line 61
    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue(Ljava/util/EnumSet;)I

    move-result v4

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    invoke-static {v5}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue(Ljava/util/EnumSet;)I

    move-result v5

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->playbackRate:F

    iget v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->mediaStart:J

    iget-wide v6, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->mediaEnd:J

    iget-wide v6, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    const/4 v3, 0x1

    .line 68
    .local v3, "result":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 69
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->metaData:Ljava/util/List;

    if-eqz v4, :cond_5

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    if-eqz v4, :cond_5

    .line 70
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->metaData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_4

    .line 71
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->metaData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_1

    .line 72
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->metaData:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .line 73
    .local v0, "a":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .line 74
    .local v1, "b":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->name:Ljava/lang/String;

    iget-object v5, v1, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->name:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->value:Ljava/lang/String;

    iget-object v5, v1, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->value:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 75
    :cond_0
    const/4 v3, 0x0

    .line 87
    .end local v0    # "a":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .end local v1    # "b":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .end local v2    # "i":I
    :cond_1
    :goto_2
    return v3

    .line 61
    .end local v3    # "result":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 71
    .restart local v0    # "a":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .restart local v1    # "b":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .restart local v2    # "i":I
    .restart local v3    # "result":Z
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 80
    .end local v0    # "a":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .end local v1    # "b":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .end local v2    # "i":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 82
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/SGMediaState;->metaData:Ljava/util/List;

    if-nez v4, :cond_6

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 83
    :cond_6
    const/4 v3, 0x0

    goto :goto_2
.end method
