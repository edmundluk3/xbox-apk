.class public final enum Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
.super Ljava/lang/Enum;
.source "TransmitterInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/TransmitterInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BrowseFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

.field public static final enum BrowseDirectChildren:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

.field public static final enum BrowseMetadata:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;


# instance fields
.field private text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    const-string v1, "BrowseMetadata"

    const-string v2, "BrowseMetadata"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->BrowseMetadata:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    .line 43
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    const-string v1, "BrowseDirectChildren"

    const-string v2, "BrowseDirectChildren"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->BrowseDirectChildren:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->BrowseMetadata:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->BrowseDirectChildren:Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->text:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 59
    if-eqz p0, :cond_1

    .line 61
    invoke-static {}, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->values()[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    move-result-object v0

    .local v0, "arr$":[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 63
    .local v1, "b":Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    iget-object v4, v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->text:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    .end local v0    # "arr$":[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .end local v1    # "b":Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :goto_1
    return-object v1

    .line 61
    .restart local v0    # "arr$":[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .restart local v1    # "b":Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "arr$":[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .end local v1    # "b":Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    invoke-virtual {v0}, [Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;

    return-object v0
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->text:Ljava/lang/String;

    return-object v0
.end method
