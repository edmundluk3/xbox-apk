.class public final enum Lcom/microsoft/xbox/service/model/TitleMessage$Type;
.super Ljava/lang/Enum;
.source "TitleMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/TitleMessage$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/TitleMessage$Type;

.field public static final enum inputstyle:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

.field public static final enum titleinfo:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

.field public static final enum url_changed:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

.field public static final enum url_changing:Lcom/microsoft/xbox/service/model/TitleMessage$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    const-string v1, "titleinfo"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/TitleMessage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->titleinfo:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    const-string v1, "url_changing"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/TitleMessage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changing:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    const-string v1, "url_changed"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/TitleMessage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changed:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    const-string v1, "inputstyle"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/TitleMessage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->inputstyle:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    sget-object v1, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->titleinfo:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changing:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changed:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->inputstyle:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->$VALUES:[Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleMessage$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/TitleMessage$Type;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->$VALUES:[Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/TitleMessage$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    return-object v0
.end method
