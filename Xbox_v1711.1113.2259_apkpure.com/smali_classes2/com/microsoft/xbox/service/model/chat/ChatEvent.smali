.class public final enum Lcom/microsoft/xbox/service/model/chat/ChatEvent;
.super Ljava/lang/Enum;
.source "ChatEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/chat/ChatEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum DeleteFailed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum Deleted:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum Error:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum History:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum IsTyping:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum Joined:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum Message:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum MessageOfTheDay:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum NotAllowed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

.field public static final enum UserInfo:Lcom/microsoft/xbox/service/model/chat/ChatEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "Message"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Message:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "IsTyping"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->IsTyping:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "NotAllowed"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->NotAllowed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 7
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "Joined"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Joined:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "History"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->History:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "Deleted"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Deleted:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "MessageOfTheDay"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->MessageOfTheDay:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "DeleteFailed"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->DeleteFailed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "UserInfo"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->UserInfo:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const-string v1, "Error"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Error:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 3
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Message:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->IsTyping:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->NotAllowed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Joined:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->History:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Deleted:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->MessageOfTheDay:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->DeleteFailed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->UserInfo:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Error:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/chat/ChatEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/chat/ChatEvent;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/chat/ChatEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    return-object v0
.end method
