.class public abstract Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ViewModelWithEntity.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

.field private loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->onGameClipLoaded(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private cancelGameClipTask()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->cancel()V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 134
    :cond_0
    return-void
.end method

.method private loadGameClip(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V
    .locals 3
    .param p1, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->cancelGameClipTask()V

    .line 95
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->load(Z)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->isLoadingGameClip()Z

    move-result v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 100
    :cond_0
    return-void
.end method

.method private onGameClipLoaded(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 137
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->isLoadingGameClip()Z

    move-result v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->setBlocking(ZLjava/lang/String;)V

    .line 141
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loaded game clip with status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 159
    :goto_0
    return-void

    .line 146
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    const-string v2, "Loaded game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getGameClip()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v0

    .line 148
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    if-eqz v1, :cond_1

    .line 149
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToGameclip(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Z

    goto :goto_0

    .line 151
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    const-string v2, "Loaded empty game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 156
    .end local v0    # "gameClip":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    const-string v2, "Failed to load game clip"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const v1, 0x7f070752

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->showError(I)V

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected isLoadingGameClip()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClipTask:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadEntity(Lcom/microsoft/xbox/service/model/entity/EntityModel;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8
    .param p1, "entityModel"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .param p2, "messageSenderXuid"    # Ljava/lang/String;
    .param p3, "forceLoad"    # Z

    .prologue
    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p1, p3}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    .line 105
    .local v4, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 106
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v5, v6, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 108
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v5, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    .line 109
    .local v3, "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    const/4 v5, 0x1

    new-array v5, v5, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    const/4 v6, 0x0

    invoke-virtual {v3, p3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPresenceData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    .line 110
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPresenceData()Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    move-result-object v2

    .line 111
    .local v2, "presense":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    if-eqz v2, :cond_0

    .line 112
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V

    .line 121
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v2    # "presense":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    .end local v3    # "profileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    return-object v4
.end method

.method protected navigateToEntity(Lcom/microsoft/xbox/service/model/entity/Entity;)V
    .locals 5
    .param p1, "entity"    # Lcom/microsoft/xbox/service/model/entity/Entity;

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v1, v2, :cond_3

    .line 57
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    .line 58
    .local v0, "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 85
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 91
    .end local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :goto_0
    return-void

    .line 60
    .restart local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :pswitch_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getAuthorInfoForAchievementComparison()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->platform:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateAchievementToComparison(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_1
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastProvider:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->broadcastId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_2
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    const v1, 0x7f0704c7

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->showError(I)V

    goto :goto_0

    .line 74
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getXuidForLoadingGameClip()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipScid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->clipId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->loadGameClip(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V

    goto :goto_0

    .line 78
    :pswitch_3
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->screenshotScid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    const v1, 0x7f0704c6

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->showError(I)V

    goto :goto_0

    .line 81
    :cond_2
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto :goto_0

    .line 89
    .end local v0    # "feedItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    const-string v2, "message attachments should always be activity feed items"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected navigateToEntityAuthorProfile(Lcom/microsoft/xbox/service/model/entity/Entity;)V
    .locals 4
    .param p1, "entity"    # Lcom/microsoft/xbox/service/model/entity/Entity;

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    if-ne v1, v2, :cond_0

    .line 44
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    .line 45
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->navigateToFriendProfile(Ljava/lang/String;)V

    .line 49
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :goto_0
    return-void

    .line 47
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Navigation to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/entity/Entity;->getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not supported"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected navigateToFriendProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;->cancelGameClipTask()V

    .line 127
    return-void
.end method
