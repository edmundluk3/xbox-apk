.class public final enum Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
.super Ljava/lang/Enum;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrendingCategoryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Broadcasts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Events:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Everything:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Gameclips:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Games:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum People:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Posts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Screenshots:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Topics:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Tournaments:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Unknown:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Everything"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Everything:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Gameclips"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Gameclips:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Screenshots"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Screenshots:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Broadcasts"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Broadcasts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Posts"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Posts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Topics"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Topics:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "People"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->People:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Events"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Events:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Tournaments"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Tournaments:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    const-string v1, "Games"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Games:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 26
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Unknown:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Everything:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Gameclips:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Screenshots:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Broadcasts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Posts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Topics:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->People:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Events:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Tournaments:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Games:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    return-object v0
.end method
