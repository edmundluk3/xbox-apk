.class public Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "GameDetailHeaderViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 14
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 15
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 19
    const v0, 0x7f070683

    return v0
.end method

.method public getGameItemReleaseDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getGameReleaseData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetacriticRating()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->getMetacriticRating()I

    move-result v0

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 25
    return-void
.end method
