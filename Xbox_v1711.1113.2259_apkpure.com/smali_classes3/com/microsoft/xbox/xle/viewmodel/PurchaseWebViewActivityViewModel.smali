.class public Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PurchaseWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;,
        Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;,
        Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;
    }
.end annotation


# static fields
.field private static final AudienceUrl:Ljava/lang/String; = "https://purchase.mp.microsoft.com"

.field private static final DesiredLayout:Ljava/lang/String; = "modal"

.field private static final JavascriptForEventListener:Ljava/lang/String; = "javascript:window:addEventListener(\'message\', function(e) {if (/\\.microsoft\\.com$/i.test(e.origin) && typeof e.data === \'string\') {SmartGlass.onCaptureEvent(e.data);}});"

.field private static final JavascriptForReady:Ljava/lang/String; = "javascript:ready();"

.field private static final JavascriptForSettingWebBlendFormAction:Ljava/lang/String; = "javascript:SetFormAction(\'https://www.microsoft.com/uniblends?client=Universal%20Xbox%20App\');"

.field private static final JavascriptForSettingWebBlendFormData:Ljava/lang/String; = "javascript:AddToForm(\'%1$s\', \'%2$s\');"

.field private static final PhoneWebBlendUrl:Ljava/lang/String; = "https://www.microsoft.com/webblend/getClientView?clientType=DeviceStoreClient&deviceFamily=Android.Phone"

.field private static final PurchaseServiceVersion:Ljava/lang/String; = "7"

.field private static final REQUEST_FIELD_AVAILABILITY_ID:Ljava/lang/String; = "AvailabilityId"

.field private static final REQUEST_FIELD_CULTURE:Ljava/lang/String; = "Culture"

.field private static final REQUEST_FIELD_CV:Ljava/lang/String; = "CV"

.field private static final REQUEST_FIELD_LAYOUT:Ljava/lang/String; = "Layout"

.field private static final REQUEST_FIELD_MARKET:Ljava/lang/String; = "Market"

.field private static final REQUEST_FIELD_PRODUCT_ID:Ljava/lang/String; = "ProductId"

.field private static final REQUEST_FIELD_PURCHASE_SERVICE_VERSION:Ljava/lang/String; = "PurchaseServiceVersion"

.field private static final REQUEST_FIELD_SELAPPS:Ljava/lang/String; = "selApps"

.field private static final REQUEST_FIELD_SKU_ID:Ljava/lang/String; = "SkuId"

.field private static final REQUEST_FIELD_TOKEN_STRING:Ljava/lang/String; = "tokenString"

.field private static final REQUEST_FIELD_XTOKEN:Ljava/lang/String; = "XToken"

.field private static final TAG:Ljava/lang/String;

.field private static final TabletWebBlendUrl:Ljava/lang/String; = "https://www.microsoft.com/webblend/getClientView?clientType=DeviceStoreClient&deviceFamily=Android.Tablet"


# instance fields
.field private dataSource:Ljava/lang/String;

.field private isCodeRedemption:Ljava/lang/Boolean;

.field private isWebBlendReady:Z

.field private pageLoaded:Z

.field private final projectSpecificDataProvider:Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

.field private purchaseAuthToken:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseAvailabilityId:Ljava/lang/String;

.field private purchaseForConsumable:Ljava/lang/Boolean;

.field private purchaseProductId:Ljava/lang/String;

.field private purchaseSkuId:Ljava/lang/String;

.field private purchaseTitleText:Ljava/lang/String;

.field private redemptionToken:Ljava/lang/String;

.field private startLoadingTime:J

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private webViewClient:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 72
    new-instance v1, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseAuthToken:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 74
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "https://www.microsoft.com/webblend/getClientView?clientType=DeviceStoreClient&deviceFamily=Android.Tablet"

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->dataSource:Ljava/lang/String;

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 95
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isCodeRedeemable()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isCodeRedemption:Ljava/lang/Boolean;

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isCodeRedemption:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseTitleText:Ljava/lang/String;

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isPurchaseItemConsumable()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseForConsumable:Ljava/lang/Boolean;

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseProductId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseProductId:Ljava/lang/String;

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseAvailabilityId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseAvailabilityId:Ljava/lang/String;

    .line 101
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseSkuId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseSkuId:Ljava/lang/String;

    .line 107
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPurchaseWebviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 108
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->getWebView()Landroid/webkit/WebView;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/webkit/WebView;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->webViewClient:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->projectSpecificDataProvider:Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    .line 111
    return-void

    .line 92
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    const-string v1, "https://www.microsoft.com/webblend/getClientView?clientType=DeviceStoreClient&deviceFamily=Android.Phone"

    goto :goto_0

    .line 104
    .restart local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getRedemptionToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->redemptionToken:Ljava/lang/String;

    goto :goto_1
.end method

.method private static AddToFormHelper(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "method"    # Ljava/lang/String;
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    .line 521
    const-string v0, "javascript:AddToForm(\'%1$s\', \'%2$s\');"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Landroid/webkit/WebView;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onAuthTokenRequestCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseAuthToken:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseAvailabilityId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseSkuId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->redemptionToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onWebBlendReady()V

    return-void
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onWebBlendSuccess()V

    return-void
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onWebBlendCancel()V

    return-void
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onWebBlendError()V

    return-void
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onWebBlendOpenUrl(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->pageLoaded:Z

    return p1
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isWebBlendReady:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->startLoadingTime:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->projectSpecificDataProvider:Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    return-object v0
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->AddToFormHelper(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isCodeRedemption:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseProductId:Ljava/lang/String;

    return-object v0
.end method

.method private onAuthTokenRequestCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "view"    # Landroid/webkit/WebView;
    .param p3, "audienceUri"    # Ljava/lang/String;

    .prologue
    .line 244
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 256
    :goto_0
    const-string v0, "javascript:ready();"

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->updateAdapter()V

    .line 258
    return-void

    .line 248
    :pswitch_0
    const-string v1, "XToken"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseAuthToken:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->AddToFormHelper(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :pswitch_1
    const-string v0, "XToken"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->AddToFormHelper(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onWebBlendCancel()V
    .locals 0

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    .line 499
    return-void
.end method

.method private onWebBlendError()V
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isWebBlendReady:Z

    if-nez v0, :cond_0

    .line 504
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 508
    :goto_0
    return-void

    .line 506
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    goto :goto_0
.end method

.method private onWebBlendOpenUrl(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "closeBlend"    # Z

    .prologue
    .line 511
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 512
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showWebViewDialog(Ljava/lang/String;)V

    .line 515
    :cond_0
    if-eqz p2, :cond_1

    .line 516
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    .line 518
    :cond_1
    return-void
.end method

.method private onWebBlendReady()V
    .locals 1

    .prologue
    .line 480
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 481
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isWebBlendReady:Z

    .line 482
    return-void
.end method

.method private onWebBlendSuccess()V
    .locals 2

    .prologue
    .line 485
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isCodeRedemption:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 487
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 488
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-string v1, "Purchase - Success"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 489
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPurchaseResultDialog(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V

    .line 495
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 493
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    goto :goto_0
.end method

.method private setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 132
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->updateAdapter()V

    .line 133
    return-void
.end method


# virtual methods
.method public cancelLoadingScreen()V
    .locals 8

    .prologue
    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->startLoadingTime:J

    sub-long v2, v4, v6

    .line 155
    .local v2, "totalLoadingTimeBeforeCancel":J
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 157
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-string v0, "Purchase - WebBlend Canceled Short"

    .line 159
    .local v0, "actionName":Ljava/lang/String;
    const-wide/16 v4, 0x1388

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 160
    const-string v0, "Purchase - WebBlend Canceled Long"

    .line 163
    :cond_0
    long-to-int v4, v2

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    invoke-static {v0, v4, v5, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackWebBlend(Ljava/lang/String;JLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->onBackButtonPressed()V

    .line 166
    return-void
.end method

.method public exit()V
    .locals 0

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->goBack()V

    .line 202
    return-void
.end method

.method public getDataSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->dataSource:Ljava/lang/String;

    return-object v0
.end method

.method public getIsCodeRedemption()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isCodeRedemption:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getPurchaseDiaglogBodyText()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseForConsumable:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseForConsumable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070af8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070af9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPurchaseTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->purchaseTitleText:Ljava/lang/String;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->pageLoaded:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->pageLoaded:Z

    if-nez v0, :cond_0

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->startLoadingTime:J

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->webViewClient:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->Start()V

    .line 145
    :cond_0
    return-void
.end method

.method public logBIForMissingHomeConsole()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 0

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    .line 150
    return-void
.end method

.method public onRehydrate()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 115
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->pageLoaded:Z

    .line 116
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->isWebBlendReady:Z

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPurchaseWebviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 118
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/webkit/WebView;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->webViewClient:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;

    .line 119
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    return-void
.end method
