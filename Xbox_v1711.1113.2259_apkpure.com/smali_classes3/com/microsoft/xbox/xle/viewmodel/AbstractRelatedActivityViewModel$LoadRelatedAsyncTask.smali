.class Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AbstractRelatedActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRelatedAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;)V
    .locals 0

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$1;

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 124
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefreshRelated()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 152
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadRelated(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 147
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 130
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->onLoadRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 132
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 142
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->onLoadRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 143
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 136
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel<TT;>.LoadRelatedAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel$LoadRelatedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;Z)Z

    .line 138
    return-void
.end method
