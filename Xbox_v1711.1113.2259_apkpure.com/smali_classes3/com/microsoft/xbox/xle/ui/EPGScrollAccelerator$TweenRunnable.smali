.class Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;
.super Ljava/lang/Object;
.source "EPGScrollAccelerator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TweenRunnable"
.end annotation


# instance fields
.field private mIsActive:Z

.field private mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V
    .locals 1
    .param p1, "scope"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mIsActive:Z

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .line 29
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mIsActive:Z

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .line 37
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mIsActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$000(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V

    .line 44
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenRunnable;->mScope:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .line 45
    return-void
.end method
