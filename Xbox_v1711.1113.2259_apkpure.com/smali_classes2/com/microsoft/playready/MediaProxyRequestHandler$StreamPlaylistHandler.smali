.class Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;
.super Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;
.source "MediaProxyRequestHandler.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxyRequestHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StreamPlaylistHandler"
.end annotation


# instance fields
.field private final mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private mSegmentAndKeyUrls:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mStreamID:I

.field private request_count:I


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;I)V
    .locals 1
    .param p1, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;
    .param p3, "streamID"    # I

    .prologue
    .line 268
    invoke-direct {p0, p2}, Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;-><init>(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    .line 263
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->request_count:I

    .line 265
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mSegmentAndKeyUrls:Ljava/util/Collection;

    .line 269
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 270
    iput p3, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    .line 271
    return-void
.end method


# virtual methods
.method public HandleElementRequest(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 16
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/microsoft/playready/MediaProxy;->getHLSPlaylist(ILjava/lang/String;)Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;

    move-result-object v7

    .line 277
    .local v7, "playlistInfo":Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mSegmentAndKeyUrls:Ljava/util/Collection;

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_0

    .line 285
    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->getSegmentUrls()[Ljava/lang/String;

    move-result-object v11

    .line 286
    .local v11, "segmentUrls":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v12, v11

    if-lt v2, v12, :cond_1

    .line 298
    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->getKeyUrls()[Ljava/lang/String;

    move-result-object v5

    .line 299
    .local v5, "keyUrls":[Ljava/lang/String;
    const/4 v2, 0x0

    :goto_2
    array-length v12, v5

    if-lt v2, v12, :cond_2

    .line 310
    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->getPlaylistBody()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/microsoft/playready/DeviceAccommodations;->adjustPlaylist(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 311
    .local v6, "playlist":Ljava/lang/String;
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    const-string v12, "UTF-8"

    invoke-static {v12}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v12

    invoke-direct {v1, v12}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 313
    .local v1, "body":Lorg/apache/http/entity/AbstractHttpEntity;
    const/16 v12, 0xc8

    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 314
    const-string v12, "text/html"

    invoke-virtual {v1, v12}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 315
    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 316
    return-void

    .line 277
    .end local v1    # "body":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v2    # "i":I
    .end local v5    # "keyUrls":[Ljava/lang/String;
    .end local v6    # "playlist":Ljava/lang/String;
    .end local v11    # "segmentUrls":[Ljava/lang/String;
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 278
    .local v8, "relativeUrl":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v13

    invoke-virtual {v13, v8}, Lcom/microsoft/playready/MediaProxyRequestHandler;->requestUnregister(Ljava/lang/String;)V

    goto :goto_0

    .line 287
    .end local v8    # "relativeUrl":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v11    # "segmentUrls":[Ljava/lang/String;
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    aget-object v13, v11, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 288
    .local v10, "segmentUrl":Ljava/lang/String;
    new-instance v9, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->getFirstSegmentIndex()I

    move-result v15

    add-int/2addr v15, v2

    invoke-direct {v9, v12, v13, v14, v15}, Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;-><init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;II)V

    .line 289
    .local v9, "segmentHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v12

    invoke-virtual {v12, v10, v9}, Lcom/microsoft/playready/MediaProxyRequestHandler;->register(Ljava/lang/String;Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;)V

    .line 290
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mSegmentAndKeyUrls:Ljava/util/Collection;

    invoke-interface {v12, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 286
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 300
    .end local v9    # "segmentHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    .end local v10    # "segmentUrl":Ljava/lang/String;
    .restart local v5    # "keyUrls":[Ljava/lang/String;
    :cond_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getRootUri()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    aget-object v13, v5, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 301
    .local v4, "keyUrl":Ljava/lang/String;
    new-instance v3, Lcom/microsoft/playready/MediaProxyRequestHandler$KeyHandler;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mStreamID:I

    invoke-virtual {v7}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->getFirstSegmentIndex()I

    move-result v15

    add-int/2addr v15, v2

    invoke-direct {v3, v12, v13, v14, v15}, Lcom/microsoft/playready/MediaProxyRequestHandler$KeyHandler;-><init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;II)V

    .line 302
    .local v3, "keyHandler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v12

    invoke-virtual {v12, v4, v3}, Lcom/microsoft/playready/MediaProxyRequestHandler;->register(Ljava/lang/String;Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;)V

    .line 303
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mSegmentAndKeyUrls:Ljava/util/Collection;

    invoke-interface {v12, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 299
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method

.method public Unregester()V
    .locals 3

    .prologue
    .line 320
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->mSegmentAndKeyUrls:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    return-void

    .line 320
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 321
    .local v0, "relativeUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;->getRootHandler()Lcom/microsoft/playready/MediaProxyRequestHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/playready/MediaProxyRequestHandler;->requestUnregister(Ljava/lang/String;)V

    goto :goto_0
.end method
