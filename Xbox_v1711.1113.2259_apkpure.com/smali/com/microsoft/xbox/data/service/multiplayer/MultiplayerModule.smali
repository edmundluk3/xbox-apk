.class public Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
.super Ljava/lang/Object;
.source "MultiplayerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule$Names;
    }
.end annotation


# static fields
.field private static final XBL_CONTRACT_VERSION:Ljava/lang/String; = "107"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideMultiplayerService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;
    .locals 1
    .param p1, "retrofit"    # Lretrofit2/Retrofit;
        .annotation runtime Ljavax/inject/Named;
            value = "PARTY_RETROFIT"
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 94
    const-class v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;

    return-object v0
.end method

.method provideMultiplayerSessionEndpoint()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "PARTY_ENDPOINT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 50
    const-string v0, "https://sessiondirectory.xboxlive.com"

    return-object v0
.end method

.method provideMultiplayerSessionRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;
    .locals 2
    .param p1, "baseUrl"    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "PARTY_ENDPOINT"
        .end annotation
    .end param
    .param p2, "client"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "PARTY_OK_HTTP"
        .end annotation
    .end param
    .param p3, "gsonConverterFactory"    # Lretrofit2/converter/gson/GsonConverterFactory;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "PARTY_RETROFIT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 84
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 86
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method provideXbLog()Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/toolkit/XBLog;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/XBLog;-><init>()V

    return-object v0
.end method

.method providesMultiplayerSessionClient(Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 3
    .param p1, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p2, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p3, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;
    .param p4, "xblCorrelationIdHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;
    .param p5, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p6, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p7, "contentRestrictionsHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "PARTY_OK_HTTP"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 65
    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 67
    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/service/retrofit/XXblContractVersionHeaderInterceptor;

    const-string v2, "107"

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/retrofit/XXblContractVersionHeaderInterceptor;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0, p5}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p6}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0, p7}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 64
    return-object v0
.end method
