.class public Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;
.super Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;
.source "ScrollingBehavior.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private previousOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->previousOffset:I

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->previousOffset:I

    .line 21
    return-void
.end method

.method private getDependencyBehavior(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    .locals 3
    .param p1, "dep"    # Landroid/view/View;

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 55
    .local v1, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    .line 56
    .local v0, "behavior":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    instance-of v2, v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    if-eqz v2, :cond_0

    .line 57
    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    .line 59
    .end local v0    # "behavior":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    :goto_0
    return-object v0

    .restart local v0    # "behavior":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private layoutChild(Landroid/view/View;I)V
    .locals 12
    .param p1, "child"    # Landroid/view/View;
    .param p2, "dy"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v11, -0x1

    const/high16 v9, -0x80000000

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 41
    .local v5, "lpChild":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    iget v10, v5, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->width:I

    if-ne v10, v11, :cond_0

    move v2, v8

    .line 42
    .local v2, "childWMode":I
    :goto_0
    iget v10, v5, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->height:I

    if-ne v10, v11, :cond_1

    move v0, v8

    .line 43
    .local v0, "childHMode":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 44
    .local v4, "l":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v7

    .line 45
    .local v7, "t":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int v6, v8, p2

    .line 46
    .local v6, "newHeight":I
    const/4 v8, 0x0

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 47
    .local v1, "childHeightMeasureSpec":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-static {v8, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 49
    .local v3, "childWidthMeasureSpec":I
    invoke-virtual {p1, v3, v1}, Landroid/view/View;->measure(II)V

    .line 50
    sub-int v8, v7, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v4

    sub-int v10, v7, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {p1, v4, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 51
    return-void

    .end local v0    # "childHMode":I
    .end local v1    # "childHeightMeasureSpec":I
    .end local v2    # "childWMode":I
    .end local v3    # "childWidthMeasureSpec":I
    .end local v4    # "l":I
    .end local v6    # "newHeight":I
    .end local v7    # "t":I
    :cond_0
    move v2, v9

    .line 41
    goto :goto_0

    .restart local v2    # "childWMode":I
    :cond_1
    move v0, v9

    .line 42
    goto :goto_1
.end method


# virtual methods
.method public onDependentViewChanged(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 25
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->isPreviousSibling(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->getDependencyBehavior(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;

    move-result-object v0

    .line 27
    .local v0, "behavior":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getOffset()I

    move-result v1

    .line 29
    .local v1, "offset":I
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->previousOffset:I

    if-eq v2, v1, :cond_0

    .line 30
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->previousOffset:I

    sub-int/2addr v2, v1

    invoke-direct {p0, p2, v2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->layoutChild(Landroid/view/View;I)V

    .line 31
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/ScrollingBehavior;->previousOffset:I

    .line 32
    const/4 v2, 0x1

    .line 36
    .end local v0    # "behavior":Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
    .end local v1    # "offset":I
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
