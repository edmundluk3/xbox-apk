.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;
.super Ljava/lang/Object;
.source "OOBEViewStates.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OOBEPowerSettingsState"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;
    .locals 1
    .param p0, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEPowerSettingsState;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEPowerSettingsState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    return-object v0
.end method


# virtual methods
.method public abstract oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBEPowerSettingsState, settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
