.class public Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;
.super Landroid/view/View$AccessibilityDelegate;
.source "HideKeyboardAccessibilityDelegate.java"


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->instance:Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->instance:Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    return-object v0
.end method


# virtual methods
.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p1, "host"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 21
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard()V

    .line 23
    return-void
.end method
