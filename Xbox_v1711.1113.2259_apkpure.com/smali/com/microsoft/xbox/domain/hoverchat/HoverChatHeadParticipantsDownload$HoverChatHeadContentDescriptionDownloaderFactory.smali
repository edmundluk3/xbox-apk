.class public final Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;
.super Ljava/lang/Object;
.source "HoverChatHeadParticipantsDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HoverChatHeadContentDescriptionDownloaderFactory"
.end annotation


# instance fields
.field private final userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V
    .locals 0
    .param p1, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 51
    return-void
.end method


# virtual methods
.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 57
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    .line 58
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 60
    .restart local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 61
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .end local p1    # "key":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    move-result-object v0

    goto :goto_0
.end method

.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 67
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 68
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$ClubHoverChatHeadParticipantsDownloader;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$ClubHoverChatHeadParticipantsDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$1;)V

    return-object v0
.end method

.method public get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;
    .locals 3
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$GroupHoverChatHeadParticipantsDownloader;

    invoke-direct {v0, p1, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$GroupHoverChatHeadParticipantsDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$1;)V

    .line 78
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$MessageHoverChatHeadParticipantsDownloader;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$MessageHoverChatHeadParticipantsDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$1;)V

    goto :goto_0
.end method
