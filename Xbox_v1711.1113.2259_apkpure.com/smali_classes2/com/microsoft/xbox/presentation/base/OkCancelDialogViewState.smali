.class public Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;
.super Ljava/lang/Object;
.source "OkCancelDialogViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# instance fields
.field public final cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field protected volatile transient hashCode:I


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V
    .locals 0
    .param p1, "confirmedIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "cancelledIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p0, p1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;

    if-nez v3, :cond_2

    move v1, v2

    .line 55
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 57
    check-cast v0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;

    .line 58
    .local v0, "other":Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    iget-object v4, v0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    iget-object v4, v0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 59
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    if-nez v0, :cond_0

    .line 66
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    .line 67
    iget v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    .line 68
    iget v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    .line 71
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->hashCode:I

    return v0
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "{confirmedIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 45
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;->toLogString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cancelledIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 46
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;->toLogString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
