.class public final Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_SkypeTokenResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultExpiresIn:I

.field private defaultSkypetoken:Ljava/lang/String;

.field private final expiresInAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final skypetokenAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultExpiresIn:I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultSkypetoken:Ljava/lang/String;

    .line 24
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->expiresInAdapter:Lcom/google/gson/TypeAdapter;

    .line 25
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->skypetokenAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 52
    const/4 v3, 0x0

    .line 78
    :goto_0
    return-object v3

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 55
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultExpiresIn:I

    .line 56
    .local v1, "expiresIn":I
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultSkypetoken:Ljava/lang/String;

    .line 57
    .local v2, "skypetoken":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 63
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 73
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 63
    :sswitch_0
    const-string v4, "expiresIn"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "skypetoken"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 65
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->expiresInAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 66
    goto :goto_1

    .line 69
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->skypetokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "skypetoken":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 70
    .restart local v2    # "skypetoken":Ljava/lang/String;
    goto :goto_1

    .line 77
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 78
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 63
    :sswitch_data_0
    .sparse-switch
        -0x594f771d -> :sswitch_1
        0xee9b379 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultExpiresIn(I)Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultExpiresIn"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultExpiresIn:I

    .line 29
    return-object p0
.end method

.method public setDefaultSkypetoken(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSkypetoken"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->defaultSkypetoken:Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    if-nez p2, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 47
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 42
    const-string v0, "expiresIn"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->expiresInAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;->expiresIn()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 44
    const-string v0, "skypetoken"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->skypetokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;->skypetoken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;)V

    return-void
.end method
