.class Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "ArtistDetailTopSongsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private songDurationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private songTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$1;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->songTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->songTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->songDurationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter$ViewHolder;->songDurationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method
