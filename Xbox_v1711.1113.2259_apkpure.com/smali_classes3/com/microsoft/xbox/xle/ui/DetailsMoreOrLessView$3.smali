.class Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;
.super Ljava/lang/Object;
.source "DetailsMoreOrLessView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->handleMoreAndLessClickEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 175
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 171
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 167
    return-void
.end method
