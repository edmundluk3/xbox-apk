.class Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AutoConnectModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/AutoConnectModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetryConnectToConsoleTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private console:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;

    .prologue
    .line 220
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 243
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getLastConnectedConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 244
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-nez v2, :cond_0

    .line 245
    const-string v2, "AutoConnect"

    const-string v3, "There is no last connected console, this should not happen!"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 269
    :goto_0
    return-object v2

    .line 249
    :cond_0
    const-string v2, "AutoConnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retry connect to console "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 251
    const-string v2, "AutoConnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retry connect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$400(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 268
    :cond_1
    const-string v2, "AutoConnect"

    const-string v3, "retry connect to console failed "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 256
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$500(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 257
    const-string v2, "RetryConnect"

    const-string v3, "success"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 262
    :cond_3
    add-int/lit8 v2, v1, 0x1

    mul-int/lit16 v2, v2, 0x7d0

    int-to-long v2, v2

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$300(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 233
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$300(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 280
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 220
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$RetryConnectToConsoleTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 274
    return-void
.end method
