.class public final Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubInviteListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static SELECTION_COLOR:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# instance fields
.field private final selectedPositions:Landroid/util/SparseBooleanArray;

.field private final selectionListener:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V
    .locals 1
    .param p1, "selectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 42
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectedPositions:Landroid/util/SparseBooleanArray;

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectionListener:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;

    .line 44
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->SELECTION_COLOR:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Integer;

    .prologue
    .line 28
    sput-object p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->SELECTION_COLOR:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectedPositions:Landroid/util/SparseBooleanArray;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;->clear()V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 77
    return-void
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 70
    const v0, 0x7f03008b

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 65
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectionListener:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V

    .line 66
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 60
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public setSelectedPositions(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "newSelectedPositions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->selectedPositions:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 52
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->notifyItemChanged(I)V

    .line 49
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_1
    return-void
.end method
