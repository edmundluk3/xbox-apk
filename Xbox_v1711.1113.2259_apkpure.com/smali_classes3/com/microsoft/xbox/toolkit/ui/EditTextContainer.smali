.class public Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;
.super Landroid/widget/RelativeLayout;
.source "EditTextContainer.java"


# static fields
.field private static final MOVE_DIPS_DISTANCE_SQUARED:F = 10.0f


# instance fields
.field private children:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private grabBackButton:Z

.field private keyboardDismissedRunnable:Ljava/lang/Runnable;

.field private shouldHideKeyboard:Z

.field private startX:F

.field private startY:F

.field private unfocused:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->keyboardDismissedRunnable:Ljava/lang/Runnable;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->grabBackButton:Z

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->shouldHideKeyboard:Z

    .line 58
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->addViewThatCausesAndroidLeaks(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->keyboardDismissedRunnable:Ljava/lang/Runnable;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->grabBackButton:Z

    .line 48
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->shouldHideKeyboard:Z

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->children:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocused:Landroid/widget/FrameLayout;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocused:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocused:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setFocusableInTouchMode(Z)V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocused:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->addView(Landroid/view/View;)V

    .line 70
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->addViewThatCausesAndroidLeaks(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->hideSoftKeyboard()V

    return-void
.end method

.method private distanceSquare(FFFF)F
    .locals 3
    .param p1, "x"    # F
    .param p2, "x1"    # F
    .param p3, "y"    # F
    .param p4, "y1"    # F

    .prologue
    .line 202
    sub-float v0, p1, p2

    sub-float v1, p1, p2

    mul-float/2addr v0, v1

    sub-float v1, p3, p4

    sub-float v2, p3, p4

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private hideSoftKeyboard()V
    .locals 2

    .prologue
    .line 183
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 186
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->onKeyboardDismissed()V

    .line 188
    const-string v0, "EditTextContainer"

    const-string v1, "keyboard hidden now "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setDismissSoftKeyboard(Lcom/microsoft/xle/test/interop/delegates/Action;)V

    .line 190
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->requestFocus()Z

    .line 191
    return-void
.end method

.method private isPointInChildren(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->children:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 208
    .local v0, "child":Landroid/view/View;
    invoke-static {p1, p2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isTouchPointInsideView(FFLandroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    const/4 v1, 0x1

    .line 213
    .end local v0    # "child":Landroid/view/View;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onKeyboardDismissed()V
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocusText()V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->keyboardDismissedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->keyboardDismissedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 199
    :cond_0
    return-void
.end method

.method private processTouchEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 81
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 83
    :pswitch_0
    const-string v1, "EditTextContainer"

    const-string v2, "key down"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startX:F

    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startY:F

    goto :goto_0

    .line 88
    :pswitch_1
    const-string v1, "EditTextContainer"

    const-string v2, "key up"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->distanceSquare(FFFF)F

    move-result v1

    const/high16 v2, 0x41200000    # 10.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 91
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startX:F

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->startY:F

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->isPointInChildren(FF)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    const-string v1, "EditTextContainer"

    const-string v2, "click on another child don\'t dismiss keyboard"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_1
    const-string v1, "EditTextContainer"

    const-string v2, "click on edittext container, hide keyboard "

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->shouldHideKeyboard:Z

    if-eqz v1, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->hideSoftKeyboard()V

    goto :goto_0

    .line 102
    :cond_2
    const-string v1, "EditTextContainer"

    const-string v2, "moved or keyboard not shown , ignore"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addChild(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_0
    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 157
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->grabBackButton:Z

    if-eqz v2, :cond_1

    .line 158
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    .line 160
    .local v0, "state":Landroid/view/KeyEvent$DispatcherState;
    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 162
    invoke-virtual {v0, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 171
    .end local v0    # "state":Landroid/view/KeyEvent$DispatcherState;
    :goto_0
    return v1

    .line 164
    .restart local v0    # "state":Landroid/view/KeyEvent$DispatcherState;
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->onKeyboardDismissed()V

    goto :goto_0

    .line 171
    .end local v0    # "state":Landroid/view/KeyEvent$DispatcherState;
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->processTouchEvent(Landroid/view/MotionEvent;)V

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 120
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 127
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 123
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setGrabBackButton(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->grabBackButton:Z

    .line 176
    return-void
.end method

.method public setKeyboardDismissedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->keyboardDismissedRunnable:Ljava/lang/Runnable;

    .line 151
    return-void
.end method

.method public setKeyboardShown()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "EditTextContainer"

    const-string v1, "keyboard shown"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V

    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setDismissSoftKeyboard(Lcom/microsoft/xle/test/interop/delegates/Action;)V

    .line 141
    return-void
.end method

.method public setShouldHideKeyboard(Z)V
    .locals 0
    .param p1, "shouldHide"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->shouldHideKeyboard:Z

    .line 75
    return-void
.end method

.method public unfocusText()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->unfocused:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestFocus()Z

    .line 180
    return-void
.end method
