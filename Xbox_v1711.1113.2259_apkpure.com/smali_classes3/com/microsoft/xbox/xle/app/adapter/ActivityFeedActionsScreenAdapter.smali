.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ActivityFeedActionsScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$ViewType;,
        Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;
    }
.end annotation


# static fields
.field private static final LISTVIEW_BOTTOM_START_POINT:I

.field private static shouldSendSpinnerBI:Z


# instance fields
.field private actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

.field private actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private final adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

.field private commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final inflater:Landroid/view/LayoutInflater;

.field private isKeyboardVisible:Z

.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private final layoutListener:Landroid/view/View$OnLayoutChangeListener;

.field private final listView:Landroid/widget/ListView;

.field private listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

.field private textEntry:Landroid/widget/EditText;

.field private final textWatcher:Landroid/text/TextWatcher;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 326
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    .line 347
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    .line 65
    const v1, 0x7f0e0135

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->screenBody:Landroid/view/View;

    .line 66
    const v1, 0x7f0e0136

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 69
    .local v0, "ctx":Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 71
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    const/4 v1, 0x0

    sput-boolean v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->shouldSendSpinnerBI:Z

    .line 75
    const v1, 0x7f0e0131

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    .line 76
    const v1, 0x7f0e0132

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 77
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    return-object v0
.end method

.method static synthetic access$1200()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->shouldSendSpinnerBI:Z

    return v0
.end method

.method static synthetic access$1202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 44
    sput-boolean p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->shouldSendSpinnerBI:Z

    return p0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getItemPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setSendButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->screenBody:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->screenBody:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->isKeyboardVisible:Z

    return v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->isKeyboardVisible:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getFirstDataPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getDataCount()I

    move-result v0

    return v0
.end method

.method public static feedItemActionClickHandler(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;Landroid/view/View;)Z
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
    .param p1, "feedItem"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletable(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 204
    .local v0, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0f0005

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 205
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$4;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 211
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 212
    const/4 v1, 0x1

    .line 214
    .end local v0    # "popup":Landroid/widget/PopupMenu;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getData()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v2

    aget-object v0, v1, v2

    .line 288
    .local v0, "actionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method private getDataCount()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 292
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    if-nez v3, :cond_1

    .line 300
    :cond_0
    :goto_0
    return v2

    .line 295
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v4

    aget-object v0, v3, v4

    .line 296
    .local v0, "actionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-eqz v0, :cond_0

    .line 299
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v1

    .line 300
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_0
.end method

.method private getFirstDataPosition()I
    .locals 1

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getSpinnerPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private getItemPosition()I
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method private getSpinnerPosition()I
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->getItemPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    const/4 v3, 0x0

    .line 80
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 81
    if-nez p1, :cond_1

    .line 82
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    .line 83
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 94
    :cond_0
    return-void

    .line 85
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getAvailableTypes()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v0

    .line 86
    .local v0, "availableTypes":[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    array-length v3, v0

    new-array v3, v3, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    .line 87
    array-length v3, v0

    new-array v3, v3, [Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 88
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 89
    aget-object v2, v0, v1

    .line 90
    .local v2, "t":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getData(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v4

    aput-object v4, v3, v1

    .line 91
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    aput-object v4, v3, v1

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setSendButtonEnabled(Z)V
    .locals 1
    .param p1, "enableSendButton"    # Z

    .prologue
    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 319
    :cond_0
    return-void
.end method

.method private updateCountAndPostButton()V
    .locals 3

    .prologue
    .line 322
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isPostingComment()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 324
    return-void

    .line 323
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAnimateInCompleted()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onAnimateInCompleted()V

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getShouldAutoFocus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->shouldShowKeyboard(Z)V

    .line 163
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->saveAndAdjustSoftInputAdjustMode()V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 116
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setSendButtonEnabled(Z)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 157
    return-void

    .line 118
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setSendButtonEnabled(Z)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->restoreSoftInputAdjustMode()V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 194
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 195
    return-void
.end method

.method public setCommentText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :cond_0
    return-void
.end method

.method public shouldShowKeyboard(Z)V
    .locals 2
    .param p1, "shouldShowKeyboard"    # Z

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->textEntry:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->showKeyboard(Landroid/view/View;I)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->dismissKeyboard()V

    goto :goto_0
.end method

.method public updateBlockingIndicator()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isDeletingComment()Z

    move-result v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 199
    return-void
.end method

.method protected updateViewOverride()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 222
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isBusy()Z

    move-result v7

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 224
    const/4 v6, 0x0

    .line 226
    .local v6, "notifyAdapter":Z
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v4

    .line 227
    .local v4, "newItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v4, :cond_0

    .line 228
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eq v7, v4, :cond_9

    .line 229
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 230
    const/4 v6, 0x1

    .line 238
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isLikeUpdated()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 239
    const/4 v6, 0x1

    .line 240
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7, v10}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setLikeUpdated(Z)V

    .line 243
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItemListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    .line 244
    .local v5, "newItemListState":Lcom/microsoft/xbox/toolkit/network/ListState;
    if-eqz v5, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v5, v7, :cond_2

    .line 245
    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->itemListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 246
    const/4 v6, 0x1

    .line 249
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionType()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v3

    .line 250
    .local v3, "newActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-eq v3, v7, :cond_3

    .line 251
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 252
    const/4 v6, 0x1

    .line 255
    :cond_3
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 256
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionListState(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    .line 257
    .local v2, "newActionState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v8

    aget-object v7, v7, v8

    if-eq v2, v7, :cond_4

    .line 258
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionListStates:[Lcom/microsoft/xbox/toolkit/network/ListState;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v8

    aput-object v2, v7, v8

    .line 259
    const/4 v6, 0x1

    .line 263
    .end local v2    # "newActionState":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isActionTypeInRange(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 264
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getData(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v1

    .line 265
    .local v1, "newActionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v8

    aget-object v0, v7, v8

    .line 266
    .local v0, "actionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    if-eqz v1, :cond_5

    if-eq v0, v1, :cond_5

    .line 267
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->results:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getActionTypePosition(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)I

    move-result v8

    aput-object v1, v7, v8

    .line 268
    const/4 v6, 0x1

    .line 272
    .end local v0    # "actionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .end local v1    # "newActionResult":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isSocialActionInfoUpdated()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 273
    const/4 v6, 0x1

    .line 274
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->resetSocialActionInfoUpdated()V

    .line 277
    :cond_6
    if-eqz v6, :cond_7

    .line 278
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->notifyDataSetChanged()V

    .line 280
    :cond_7
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->isScrollToBottom(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 281
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->listView:Landroid/widget/ListView;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$Adapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 282
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v7, v10, v8}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setScrollToBottom(ZLcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 284
    :cond_8
    return-void

    .line 231
    .end local v3    # "newActionType":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .end local v5    # "newItemListState":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_9
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->equalCounts(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 232
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->setItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 233
    const/4 v6, 0x1

    goto/16 :goto_0
.end method
