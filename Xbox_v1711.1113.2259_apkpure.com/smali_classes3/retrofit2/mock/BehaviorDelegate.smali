.class public final Lretrofit2/mock/BehaviorDelegate;
.super Ljava/lang/Object;
.source "BehaviorDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final behavior:Lretrofit2/mock/NetworkBehavior;

.field private final executor:Ljava/util/concurrent/ExecutorService;

.field final retrofit:Lretrofit2/Retrofit;

.field private final service:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lretrofit2/Retrofit;Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Ljava/lang/Class;)V
    .locals 0
    .param p1, "retrofit"    # Lretrofit2/Retrofit;
    .param p2, "behavior"    # Lretrofit2/mock/NetworkBehavior;
    .param p3, "executor"    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Retrofit;",
            "Lretrofit2/mock/NetworkBehavior;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<TT;>;"
    .local p4, "service":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lretrofit2/mock/BehaviorDelegate;->retrofit:Lretrofit2/Retrofit;

    .line 44
    iput-object p2, p0, Lretrofit2/mock/BehaviorDelegate;->behavior:Lretrofit2/mock/NetworkBehavior;

    .line 45
    iput-object p3, p0, Lretrofit2/mock/BehaviorDelegate;->executor:Ljava/util/concurrent/ExecutorService;

    .line 46
    iput-object p4, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    .line 47
    return-void
.end method


# virtual methods
.method public returning(Lretrofit2/Call;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Call",
            "<TR;>;)TT;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<TT;>;"
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<TR;>;"
    new-instance v0, Lretrofit2/mock/BehaviorCall;

    iget-object v1, p0, Lretrofit2/mock/BehaviorDelegate;->behavior:Lretrofit2/mock/NetworkBehavior;

    iget-object v2, p0, Lretrofit2/mock/BehaviorDelegate;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v1, v2, p1}, Lretrofit2/mock/BehaviorCall;-><init>(Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Lretrofit2/Call;)V

    .line 56
    .local v0, "behaviorCall":Lretrofit2/Call;, "Lretrofit2/Call<TR;>;"
    iget-object v1, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v4, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    aput-object v4, v2, v3

    new-instance v3, Lretrofit2/mock/BehaviorDelegate$1;

    invoke-direct {v3, p0, v0}, Lretrofit2/mock/BehaviorDelegate$1;-><init>(Lretrofit2/mock/BehaviorDelegate;Lretrofit2/Call;)V

    invoke-static {v1, v2, v3}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public returningResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "response"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<TT;>;"
    invoke-static {p1}, Lretrofit2/mock/Calls;->response(Ljava/lang/Object;)Lretrofit2/Call;

    move-result-object v0

    invoke-virtual {p0, v0}, Lretrofit2/mock/BehaviorDelegate;->returning(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
