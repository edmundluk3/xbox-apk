.class public Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
.super Ljava/lang/Object;
.source "PaneConfigData.java"


# instance fields
.field private displayed:Z

.field private header:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private final paneClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final resourceId:I

.field private screenRatio:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 1
    .param p2, "displayed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "paneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/16 v0, 0x64

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    .line 18
    iput-boolean p2, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->displayed:Z

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->paneClass:Ljava/lang/Class;

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->resourceId:I

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;ZILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p2, "displayed"    # Z
    .param p3, "Rid"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "header"    # Ljava/lang/String;
    .param p6, "screenRatio"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;ZI",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "paneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/16 v0, 0x64

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    .line 24
    iput-boolean p2, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->displayed:Z

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->paneClass:Ljava/lang/Class;

    .line 26
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->resourceId:I

    .line 27
    iput-object p4, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->name:Ljava/lang/String;

    .line 28
    iput-object p5, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->header:Ljava/lang/String;

    .line 29
    iput p6, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    .line 30
    return-void
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getIsDisplayed()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->displayed:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPaneClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->paneClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getResourceId()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->resourceId:I

    return v0
.end method

.method public getScreenRatio()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    return v0
.end method

.method public setHeader(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .locals 0
    .param p1, "header"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->header:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public setIsDisplayed(Z)V
    .locals 0
    .param p1, "displayed"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->displayed:Z

    .line 38
    return-void
.end method

.method public setName(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->name:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public setScreenDIPs(F)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .locals 3
    .param p1, "dips"    # F

    .prologue
    .line 76
    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, p1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 77
    .local v0, "screenPixelWidth":F
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    .line 78
    return-object p0
.end method

.method public setScreenRatio(I)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .locals 0
    .param p1, "ratio"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->screenRatio:I

    .line 72
    return-object p0
.end method
