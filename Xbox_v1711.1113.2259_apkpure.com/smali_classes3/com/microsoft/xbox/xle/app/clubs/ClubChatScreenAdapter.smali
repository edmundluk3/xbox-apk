.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "ClubChatScreenAdapter.java"


# static fields
.field public static final CHARACTER_COUNT_TO_SHOW_UI:I = 0xc8

.field private static final CHARACTER_MAX_COUNT:I = 0x400

.field private static final DIRECT_MENTION_INDICATOR:C = '@'

.field private static final SERVICE_SEARCH_DELAY_MS:J = 0x1f4L

.field private static final TAG:Ljava/lang/String;

.field private static final TYPING_MESSAGE_FREQUENCY_MS:J = 0x7d0L


# instance fields
.field private autoCompleteIndex:I

.field private final autoCompleteWatcher:Landroid/text/TextWatcher;

.field private final characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private chatMenuView:Landroid/widget/LinearLayout;

.field private final chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

.field private final directMentionHandler:Landroid/os/Handler;

.field private final directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private final directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

.field private directMentionRunnable:Ljava/lang/Runnable;

.field private final invalidScreenReason:Landroid/widget/TextView;

.field private isShowingDirectMentionList:Z

.field private final isTypingWatcher:Landroid/text/TextWatcher;

.field private lastTypingMessageTimestamp:J

.field private final layoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final loadMoreHistoryScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

.field private final manageNotificationsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private manageNotificationsFrame:Landroid/widget/RelativeLayout;

.field private final messageContainer:Landroid/view/View;

.field private final messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private final messageOfTheDayContainer:Landroid/widget/RelativeLayout;

.field private final messageOfTheDayTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private final sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewMentionsFrame:Landroid/widget/RelativeLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->lastTypingMessageTimestamp:J

    .line 419
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteWatcher:Landroid/text/TextWatcher;

    .line 448
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isTypingWatcher:Landroid/text/TextWatcher;

    .line 475
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/view/View$OnLayoutChangeListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->layoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 482
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->loadMoreHistoryScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    .line 90
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 93
    const v0, 0x7f0e0a95

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 94
    const v0, 0x7f0e03b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 95
    const v0, 0x7f0e02f5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageContainer:Landroid/view/View;

    .line 97
    const v0, 0x7f0e02fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 98
    const v0, 0x7f0e0300

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 100
    const v0, 0x7f0e02fb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 101
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;J)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->layoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->loadMoreHistoryScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 106
    const v0, 0x7f0e02fc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 107
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    const v0, 0x7f0e02fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x400

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isTypingWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    const v0, 0x7f0e02d5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMenuView:Landroid/widget/LinearLayout;

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMenuView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMenuView:Landroid/widget/LinearLayout;

    const v1, 0x7f0e02d8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->manageNotificationsFrame:Landroid/widget/RelativeLayout;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->manageNotificationsFrame:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMenuView:Landroid/widget/LinearLayout;

    const v1, 0x7f0e02d6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewMentionsFrame:Landroid/widget/RelativeLayout;

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewMentionsFrame:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$4;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    const v0, 0x7f0e02fd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->manageNotificationsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->manageNotificationsButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v0, 0x7f0e02ff

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    const v0, 0x7f0e02b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    .line 185
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionHandler:Landroid/os/Handler;

    .line 187
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isShowingDirectMentionList:Z

    .line 189
    const v0, 0x7f0e02f7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayContainer:Landroid/widget/RelativeLayout;

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayContainer:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v0, 0x7f0e02f8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 198
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->setSearchTerm(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->showDirectMentionListAndUpdateView(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMenuView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->lastTypingMessageTimestamp:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 48
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->lastTypingMessageTimestamp:J

    return-wide p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->updateCharacterCountAndVisibility()V

    return-void
.end method

.method private isViewingPastMessages()Z
    .locals 3

    .prologue
    .line 299
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    .line 300
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 301
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v1

    .line 302
    .local v1, "lastVisibleItemPosition":I
    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_0

    .line 303
    const/4 v2, 0x1

    .line 307
    .end local v1    # "lastVisibleItemPosition":I
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .line 111
    .local v0, "selectedItem":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->performAutoComplete(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;)V

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatPickMentionedUser(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 115
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->showDirectMentionListAndUpdateView(Z)V

    .line 116
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->navigateToNotificationSettings()V

    .line 129
    return-void
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/view/View;IIIIIIII)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 476
    if-eq p5, p9, :cond_0

    .line 477
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 480
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$2(Landroid/view/View;)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 134
    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/view/View;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0e0301

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 139
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->navigateToNotificationSettings()V

    .line 165
    :goto_0
    return-void

    .line 141
    :cond_0
    new-instance v1, Landroid/support/design/widget/BottomSheetDialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/support/design/widget/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 142
    .local v1, "dialog":Landroid/support/design/widget/BottomSheetDialog;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030072

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 143
    .local v0, "controls":Landroid/view/View;
    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 144
    invoke-virtual {v1}, Landroid/support/design/widget/BottomSheetDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7d3

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-static {v3}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v2

    .line 146
    .local v2, "mBehavior":Landroid/support/design/widget/BottomSheetBehavior;
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 148
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldShowHoverChatOption()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 150
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/support/design/widget/BottomSheetDialog;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :goto_1
    const v3, 0x7f0e0302

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/support/design/widget/BottomSheetDialog;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    invoke-virtual {v1}, Landroid/support/design/widget/BottomSheetDialog;->show()V

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 170
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 172
    .local v0, "editText":Landroid/text/Editable;
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "messageString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->sendChatMessage(Ljava/lang/String;)V

    .line 178
    :cond_0
    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 179
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->lastTypingMessageTimestamp:J

    .line 181
    .end local v0    # "editText":Landroid/text/Editable;
    .end local v1    # "messageString":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 191
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 192
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->showClubChatEditReportDialog()V

    .line 195
    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/support/design/widget/BottomSheetDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "dialog"    # Landroid/support/design/widget/BottomSheetDialog;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->openAsHoverChat()V

    .line 152
    invoke-virtual {p1}, Landroid/support/design/widget/BottomSheetDialog;->dismiss()V

    .line 153
    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Landroid/support/design/widget/BottomSheetDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "dialog"    # Landroid/support/design/widget/BottomSheetDialog;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->navigateToNotificationSettings()V

    .line 160
    invoke-virtual {p1}, Landroid/support/design/widget/BottomSheetDialog;->dismiss()V

    .line 161
    return-void
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    return-void
.end method

.method static synthetic lambda$setSearchTerm$8(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 387
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->setSearchTerm(Ljava/lang/String;)V

    return-void
.end method

.method private performAutoComplete(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;)V
    .locals 10
    .param p1, "selectedItem"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .prologue
    .line 399
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->clearComposingText()V

    .line 401
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getSearchTerm()Ljava/lang/String;

    move-result-object v2

    .line 402
    .local v2, "original":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 404
    .local v3, "replaced":Ljava/lang/String;
    iget v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteIndex:I

    .line 405
    .local v4, "start":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int v1, v5, v4

    .line 407
    .local v1, "end":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 409
    .local v0, "editable":Landroid/text/Editable;
    invoke-static {v0, v4, v1, v2}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 411
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 412
    invoke-interface {v0, v4, v1, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 413
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 416
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelection(I)V

    .line 417
    return-void
.end method

.method private setSearchTerm(Ljava/lang/String;I)V
    .locals 4
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "termStart"    # I

    .prologue
    .line 381
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSearchTerm - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iput p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->autoCompleteIndex:I

    .line 384
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isShowingDirectMentionList:Z

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 387
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionRunnable:Ljava/lang/Runnable;

    .line 388
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1f4

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 389
    return-void

    .line 388
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private showDirectMentionListAndUpdateView(Z)V
    .locals 1
    .param p1, "isShowingDirectMentionList"    # Z

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isShowingDirectMentionList:Z

    if-eq v0, p1, :cond_0

    .line 393
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isShowingDirectMentionList:Z

    .line 394
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->updateViewOverride()V

    .line 396
    :cond_0
    return-void
.end method

.method private updateCharacterCountAndVisibility()V
    .locals 10

    .prologue
    const/16 v9, 0x400

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 343
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 346
    .local v0, "length":I
    const/16 v1, 0xc8

    if-lt v0, v1, :cond_1

    .line 347
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 348
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d/%d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070d71

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 354
    .end local v0    # "length":I
    :cond_0
    :goto_0
    return-void

    .line 351
    .restart local v0    # "length":I
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->characterCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateMessageEditText(Z)V
    .locals 7
    .param p1, "canViewerChat"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 311
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setEnabled(Z)V

    .line 314
    if-eqz p1, :cond_1

    .line 315
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702f4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getUserIsMemberOf()Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f0702fa

    .line 325
    .local v0, "roleStringId":I
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702f5

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 326
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 325
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 323
    .end local v0    # "roleStringId":I
    :cond_2
    const v0, 0x7f0702fb

    goto :goto_1
.end method

.method private updateMessageOfTheDay()V
    .locals 4

    .prologue
    .line 332
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    move-result-object v0

    .line 333
    .local v0, "messageOfTheDay":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayContainer:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070306

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 340
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayContainer:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageOfTheDayTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    return-object v0
.end method

.method public onSetActive()V
    .locals 2

    .prologue
    .line 358
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onSetActive()V

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBehaviorEnabled(Z)V

    .line 363
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 3

    .prologue
    .line 367
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onSetInactive()V

    .line 369
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBehaviorEnabled(Z)V

    .line 373
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 375
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_1

    .line 376
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 378
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->saveAndAdjustSoftInputAdjustMode()V

    .line 204
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->restoreSoftInputAdjustMode()V

    .line 209
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 210
    return-void
.end method

.method protected updateViewOverride()V
    .locals 12

    .prologue
    const/16 v7, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 224
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isBusy()Z

    move-result v5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->updateLoadingIndicator(Z)V

    .line 225
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 226
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getInvalidReasonText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerChat()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->updateMessageEditText(Z)V

    .line 230
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->updateMessageOfTheDay()V

    .line 232
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getDirectMentionAutoCompleteList()Ljava/util/List;

    move-result-object v0

    .line 233
    .local v0, "directMentionList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isShowingDirectMentionList:Z

    if-eqz v5, :cond_1

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 234
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v5, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 235
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageContainer:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->clear()V

    .line 238
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->addAll(Ljava/util/Collection;)V

    .line 239
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->notifyDataSetChanged()V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->directMentionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 242
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->messageContainer:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 244
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->isViewingPastMessages()Z

    move-result v1

    .line 246
    .local v1, "isViewingPastMessages":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 247
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clearShouldUpdateAllMessages()V

    .line 249
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getChatMessages()Ljava/util/List;

    move-result-object v2

    .line 250
    .local v2, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->clear()V

    .line 251
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->addAll(Ljava/util/Collection;)V

    .line 253
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->notifyDataSetChanged()V

    .line 265
    .end local v2    # "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;>;"
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldScrollToBottom()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 266
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clearShouldScrollToBottom()V

    .line 267
    if-nez v1, :cond_3

    .line 268
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    .line 272
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateTyping()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 273
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clearShouldUpdateTyping()V

    .line 274
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getTypingData()Ljava/util/List;

    move-result-object v3

    .line 275
    .local v3, "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 276
    .local v4, "typingDataSize":I
    packed-switch v4, :pswitch_data_0

    .line 290
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070310

    new-array v8, v11, [Ljava/lang/Object;

    .line 291
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    .line 290
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 255
    .end local v3    # "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "typingDataSize":I
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateHistoryMessages()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 256
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clearShouldUpdateHistoryMessages()V

    .line 258
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getChatMessages()Ljava/util/List;

    move-result-object v2

    .line 259
    .restart local v2    # "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->clear()V

    .line 260
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->addAll(Ljava/util/Collection;)V

    .line 262
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->chatMessageListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getHistoryMessagesSize()I

    move-result v6

    invoke-virtual {v5, v10, v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->notifyItemRangeInserted(II)V

    goto :goto_1

    .line 278
    .end local v2    # "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;>;"
    .restart local v3    # "typingData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4    # "typingDataSize":I
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 281
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070320

    new-array v8, v11, [Ljava/lang/Object;

    .line 282
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v10

    .line 281
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 285
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->typingIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070323

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    .line 286
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v10

    .line 287
    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v11

    .line 285
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
