.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;
    }
.end annotation


# static fields
.field private static final A_DAY:J = 0x5265c00L

.field private static final MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final clubIdSingletonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasLfgsError:Z

.field private hasPartiesError:Z

.field private isLoadingLfgPeople:Z

.field private isLoadingLfgs:Z

.field private isLoadingParties:Z

.field private isLoadingPartyPeople:Z

.field private lfgList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private loadLfgUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private loadPartiesAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

.field private loadPartyUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private partyAndLfgList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;",
            ">;"
        }
    .end annotation
.end field

.field private partyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->lfgList:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    .line 84
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 86
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubPlayScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubIdSingletonList:Ljava/util/List;

    .line 91
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartyUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 92
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadLfgUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 93
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingParties:Z

    return p1
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasPartiesError:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLoadPartiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLoadPartyUserDataCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLoadLfgUserDataCompleted(Ljava/util/List;)V

    return-void
.end method

.method private buildList()V
    .locals 20

    .prologue
    .line 187
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v14, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v13, v14, :cond_8

    .line 188
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v11

    .line 189
    .local v11, "peopleManager":Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 191
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    .line 192
    .local v7, "now":Ljava/util/Date;
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    const-wide/32 v16, 0x5265c00

    sub-long v14, v14, v16

    invoke-direct {v2, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 194
    .local v2, "aDayAgo":Ljava/util/Date;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 195
    .local v8, "nowList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .local v12, "upcomingList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    .line 199
    .local v9, "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->keywords()Lcom/google/common/collect/ImmutableList;

    move-result-object v10

    .line 200
    .local v10, "partyKeywords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 202
    .local v3, "hasLfg":Z
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 204
    const/4 v13, 0x0

    :try_start_0
    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    const/4 v3, 0x1

    .line 212
    :cond_1
    :goto_1
    if-nez v3, :cond_0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->startTime()Ljava/util/Date;

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->startTime()Ljava/util/Date;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v13

    if-lez v13, :cond_0

    .line 213
    new-instance v13, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->firstMemberXuid()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getPersonSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v15

    invoke-direct {v13, v9, v15}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    .end local v3    # "hasLfg":Z
    .end local v9    # "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    .end local v10    # "partyKeywords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->lfgList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 219
    .local v5, "lfg":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 220
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getPersonSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    .line 221
    .local v4, "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :goto_3
    new-instance v6, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    invoke-direct {v6, v5, v4}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 222
    .local v6, "lfgListItem":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v14

    if-eqz v14, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v14

    if-lez v14, :cond_5

    .line 223
    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 220
    .end local v4    # "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v6    # "lfgListItem":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 225
    .restart local v4    # "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .restart local v6    # "lfgListItem":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
    :cond_5
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 230
    .end local v4    # "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v5    # "lfg":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v6    # "lfgListItem":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
    :cond_6
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_7

    .line 231
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    new-instance v14, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    sget-object v15, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v16, 0x7f070258

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-direct/range {v14 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    invoke-interface {v13, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 235
    :cond_7
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_8

    .line 236
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    new-instance v14, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    sget-object v15, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v16, 0x7f07025f

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-direct/range {v14 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    invoke-interface {v13, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 240
    .end local v2    # "aDayAgo":Ljava/util/Date;
    .end local v7    # "now":Ljava/util/Date;
    .end local v8    # "nowList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    .end local v11    # "peopleManager":Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
    .end local v12    # "upcomingList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;>;"
    :cond_8
    return-void

    .line 206
    .restart local v2    # "aDayAgo":Ljava/util/Date;
    .restart local v3    # "hasLfg":Z
    .restart local v7    # "now":Ljava/util/Date;
    .restart local v8    # "nowList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    .restart local v9    # "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    .restart local v10    # "partyKeywords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v11    # "peopleManager":Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
    .restart local v12    # "upcomingList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;>;"
    :catch_0
    move-exception v13

    goto/16 :goto_1
.end method

.method private onLfgSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v5, 0x1

    .line 329
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLfgSessionsLoaded, status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgs:Z

    .line 332
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 358
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateViewModelState()V

    .line 359
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->buildList()V

    .line 360
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    .line 361
    return-void

    .line 336
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v3, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubIdSingletonList:Ljava/util/List;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->lfgList:Ljava/util/List;

    .line 338
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 340
    .local v1, "xuidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->lfgList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 341
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 345
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 346
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgPeople:Z

    .line 347
    new-instance v2, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadLfgUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto :goto_0

    .line 354
    .end local v1    # "xuidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_1
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasLfgsError:Z

    goto :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadLfgUserDataCompleted(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 370
    .local p1, "personList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadLfgUserDataCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgPeople:Z

    .line 372
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLoadUserDataCompleted(Ljava/util/List;)V

    .line 373
    return-void
.end method

.method private onLoadPartiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "partyList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;>;"
    const/4 v6, 0x1

    .line 298
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLoadPartiesCompleted - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 323
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateViewModelState()V

    .line 324
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->buildList()V

    .line 325
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    .line 326
    return-void

    .line 304
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    .line 305
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 306
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 307
    .local v1, "xuidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    .line 308
    .local v0, "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->firstMemberXuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 311
    .end local v0    # "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingPartyPeople:Z

    .line 313
    new-instance v2, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartyUserDataCallback:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto :goto_0

    .line 319
    .end local v1    # "xuidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_1
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasPartiesError:Z

    goto :goto_0

    .line 300
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadPartyUserDataCompleted(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "personList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadPartyUserDataCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingPartyPeople:Z

    .line 366
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLoadUserDataCompleted(Ljava/util/List;)V

    .line 367
    return-void
.end method

.method private onLoadUserDataCompleted(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 376
    .local p1, "personList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateViewModelState()V

    .line 377
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->buildList()V

    .line 378
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    .line 379
    return-void
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingParties:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgs:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingPartyPeople:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgPeople:Z

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 184
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasPartiesError:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasLfgsError:Z

    if-eqz v0, :cond_3

    .line 178
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->lfgList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 180
    :cond_4
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 182
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyAndLfgList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInvalidReasonText()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 99
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702da

    .line 101
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    :goto_0
    return-object v1

    .line 101
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070160

    .line 102
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 105
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public getLfgCreateRestriction()Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;
    .locals 5

    .prologue
    .line 267
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 268
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 269
    .local v1, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 271
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v3, v4, :cond_2

    .line 272
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->ClubAdmin:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    .line 279
    :goto_1
    return-object v3

    .line 268
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .end local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 273
    .restart local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .restart local v2    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v3

    if-nez v3, :cond_4

    .line 274
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->Privacy:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    goto :goto_1

    .line 275
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateOrJoinLFG()Z

    move-result v3

    if-nez v3, :cond_5

    .line 276
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->Enforcement:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    goto :goto_1

    .line 279
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    goto :goto_1
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v3, 0x1

    .line 147
    if-eqz p1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->partyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getLfgCreateRestriction()Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->Privacy:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    if-ne v0, v1, :cond_1

    .line 152
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasLfgsError:Z

    .line 170
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateViewModelState()V

    .line 171
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    .line 172
    return-void

    .line 154
    :cond_1
    if-nez p1, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubIdSingletonList:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 155
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Loading LFGs"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isLoadingLfgs:Z

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->hasLfgsError:Z

    .line 158
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubIdSingletonList:Ljava/util/List;

    invoke-virtual {v0, v3, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)V

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartiesAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

    if-eqz v0, :cond_4

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartiesAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->cancel()V

    .line 165
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartiesAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->loadPartiesAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public navigateToLfgCreate()V
    .locals 4

    .prologue
    .line 382
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 383
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedClubId(Ljava/lang/Long;)V

    .line 385
    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 386
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 244
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClubModelChanged (STATUS: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 249
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 251
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v1, v2, :cond_0

    .line 252
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 253
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 260
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 261
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubPlayScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 111
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 117
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Play:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 119
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStartOverride()V

    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 126
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 132
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 133
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "updateOverride"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 287
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 288
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 289
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgClubSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v1, v2, :cond_0

    .line 290
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->onLfgSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 294
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 295
    return-void
.end method
