.class Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;
.super Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;
.source "UserInfoRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->send()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 38
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$000(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->remove(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 40
    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->getResponse()Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    move-result-object v1

    .line 41
    .local v1, "response":Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;
    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->isSuccess()Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    const-string v2, ""

    invoke-virtual {p0, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V

    .line 54
    :goto_0
    return-void

    .line 48
    :cond_0
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonUserInfo;

    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;->response:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonUserInfo;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonUserInfo;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$200(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v3}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)I

    move-result v3

    invoke-interface {v2, v3, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "jsonResponse":Lcom/microsoft/xbox/smartglass/controls/JsonUserInfo;
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v2

    const-string v3, "Information.GetUserInfo"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)I

    move-result v4

    sget-object v5, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 50
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V
    .locals 5
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$000(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->remove(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get user info. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$200(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)I

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "Information.GetUserInfo"

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    invoke-static {v3}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method
