.class public Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
.super Ljava/lang/Object;
.source "TutorialNavigator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private facebookManager:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

.field private navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

.field private telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

.field private tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;)V
    .locals 0
    .param p1, "navigationManager"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p2, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p3, "tutorialRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
    .param p4, "telemetryService"    # Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .line 32
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->facebookManager:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .line 33
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 34
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    .line 35
    return-void
.end method


# virtual methods
.method public navigate(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "navigationMethod"    # Ljava/lang/Runnable;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->getIsFirstRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v1, "Ending the first run experience because a user has managed to escape it and tap a Welcome card."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->setAppToNormalState()V

    .line 43
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 44
    return-void
.end method

.method public navigateToDiscoverClubs()V
    .locals 3

    .prologue
    .line 93
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Navigating to discover clubs"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v2, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->clubs:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to store from tutorial"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public navigateToLinkToFacebook()V
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v1, "Navigating to link to facebook"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->facebook:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->facebookManager:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->login()V

    .line 83
    return-void
.end method

.method public navigateToRedeemCode()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 58
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Navigating to redeem"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 61
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v2, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->redeem:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/PurchaseWebViewActivity;

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 66
    return-void
.end method

.method public navigateToSetup()V
    .locals 3

    .prologue
    .line 47
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Navigating to OOBE"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v2, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->setupConsole:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to OOBE from tutorial"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public navigateToStore()V
    .locals 3

    .prologue
    .line 69
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Navigating to store"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v2, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->shop:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to store from tutorial"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public navigateToSuggestedFriends()V
    .locals 3

    .prologue
    .line 86
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->TAG:Ljava/lang/String;

    const-string v1, "Navigating to show suggested friends"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    sget-object v1, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;->suggestedFriends:Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;->navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 89
    return-void
.end method
