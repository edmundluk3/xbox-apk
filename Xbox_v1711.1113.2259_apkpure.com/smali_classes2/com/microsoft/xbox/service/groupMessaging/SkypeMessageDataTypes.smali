.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes;
.super Ljava/lang/Object;
.source "SkypeMessageDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeNoEvent;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeEventMessage;,
        Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
