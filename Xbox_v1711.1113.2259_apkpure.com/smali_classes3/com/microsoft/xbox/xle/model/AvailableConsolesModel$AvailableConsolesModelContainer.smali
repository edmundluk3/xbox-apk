.class Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;
.super Ljava/lang/Object;
.source "AvailableConsolesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AvailableConsolesModelContainer"
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->instance:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->instance:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    return-object v0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->reset()V

    return-void
.end method

.method private static reset()V
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->instance:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->access$200(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)V

    .line 75
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->instance:Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .line 76
    return-void
.end method
