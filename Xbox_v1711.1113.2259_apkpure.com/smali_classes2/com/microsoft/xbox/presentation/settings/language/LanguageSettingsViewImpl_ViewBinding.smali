.class public Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_ViewBinding;
.super Ljava/lang/Object;
.source "LanguageSettingsViewImpl_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Landroid/view/View;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    .line 27
    const v0, 0x7f0e09f0

    const-string v1, "field \'languageSpinner\'"

    const-class v2, Landroid/widget/Spinner;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    .line 28
    const v0, 0x7f0e09f3

    const-string v1, "field \'locationSpinner\'"

    const-class v2, Landroid/widget/Spinner;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    .line 29
    const v0, 0x7f0e09ef

    const-string v1, "field \'languageTitleText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e09f1

    const-string v1, "field \'languageDescriptionText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v0, 0x7f0e09f2

    const-string v1, "field \'locationTitleText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e09f4

    const-string v1, "field \'locationDescriptionText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e09f5

    const-string v1, "field \'languageWarningText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    .line 40
    .local v0, "target":Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageSpinner:Landroid/widget/Spinner;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationSpinner:Landroid/widget/Spinner;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationTitleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->locationDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;->languageWarningText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    return-void
.end method
