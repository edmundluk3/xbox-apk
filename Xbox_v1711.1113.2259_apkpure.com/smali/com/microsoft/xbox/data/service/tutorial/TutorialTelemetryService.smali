.class public Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;
.super Ljava/lang/Object;
.source "TutorialTelemetryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public navigateTo(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V
    .locals 1
    .param p1, "tutorialCard"    # Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 45
    new-instance v0, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$1;-><init>(Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService$TutorialCard;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 52
    return-void
.end method
