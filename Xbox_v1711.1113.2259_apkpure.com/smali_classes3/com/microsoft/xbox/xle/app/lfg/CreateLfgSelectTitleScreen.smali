.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "CreateLfgSelectTitleScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 18
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;->onCreateContentView()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 20
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f030234

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;->setContentView(I)V

    .line 25
    return-void
.end method
