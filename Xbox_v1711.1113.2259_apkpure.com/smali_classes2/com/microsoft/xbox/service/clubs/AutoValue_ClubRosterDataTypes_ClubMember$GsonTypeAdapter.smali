.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubRosterDataTypes_ClubMember.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultError:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

.field private defaultErrorCode:Ljava/lang/Integer;

.field private defaultErrorDescription:Ljava/lang/String;

.field private defaultFollowQuotaMax:Ljava/lang/Integer;

.field private defaultFollowQuotaRemaining:Ljava/lang/Integer;

.field private defaultMemberQuotaMax:Ljava/lang/Integer;

.field private defaultMemberQuotaRemaining:Ljava/lang/Integer;

.field private defaultModeratorQuotaMax:Ljava/lang/Integer;

.field private defaultModeratorQuotaRemaining:Ljava/lang/Integer;

.field private defaultRoles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private defaultUserId:Ljava/lang/String;

.field private final errorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;",
            ">;"
        }
    .end annotation
.end field

.field private final errorCodeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final errorDescriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final followQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final followQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final memberQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final memberQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final moderatorQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final moderatorQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final rolesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;>;"
        }
    .end annotation
.end field

.field private final userIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultUserId:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultRoles:Lcom/google/common/collect/ImmutableList;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaMax:Ljava/lang/Integer;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaRemaining:Ljava/lang/Integer;

    .line 40
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaMax:Ljava/lang/Integer;

    .line 41
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaRemaining:Ljava/lang/Integer;

    .line 42
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaMax:Ljava/lang/Integer;

    .line 43
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaRemaining:Ljava/lang/Integer;

    .line 44
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultError:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 45
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorCode:Ljava/lang/Integer;

    .line 46
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorDescription:Ljava/lang/String;

    .line 48
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->userIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->rolesAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    .line 55
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    .line 56
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorAdapter:Lcom/google/gson/TypeAdapter;

    .line 57
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorCodeAdapter:Lcom/google/gson/TypeAdapter;

    .line 58
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorDescriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 59
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .locals 14
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_0

    .line 139
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 140
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    .line 142
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultUserId:Ljava/lang/String;

    .line 144
    .local v1, "userId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultRoles:Lcom/google/common/collect/ImmutableList;

    .line 145
    .local v2, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaMax:Ljava/lang/Integer;

    .line 146
    .local v3, "followQuotaMax":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaRemaining:Ljava/lang/Integer;

    .line 147
    .local v4, "followQuotaRemaining":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaMax:Ljava/lang/Integer;

    .line 148
    .local v5, "memberQuotaMax":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaRemaining:Ljava/lang/Integer;

    .line 149
    .local v6, "memberQuotaRemaining":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaMax:Ljava/lang/Integer;

    .line 150
    .local v7, "moderatorQuotaMax":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaRemaining:Ljava/lang/Integer;

    .line 151
    .local v8, "moderatorQuotaRemaining":Ljava/lang/Integer;
    iget-object v9, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultError:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 152
    .local v9, "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    iget-object v10, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorCode:Ljava/lang/Integer;

    .line 153
    .local v10, "errorCode":Ljava/lang/Integer;
    iget-object v11, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorDescription:Ljava/lang/String;

    .line 154
    .local v11, "errorDescription":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 155
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v12

    .line 156
    .local v12, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_1

    .line 157
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 160
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 206
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 160
    :sswitch_0
    const-string v13, "userId"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v13, "roles"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v13, "followQuotaMax"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v13, "followQuotaRemaining"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v13, "memberQuotaMax"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v13, "memberQuotaRemaining"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v13, "moderatorQuotaMax"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v13, "moderatorQuotaRemaining"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v13, "error"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x8

    goto :goto_2

    :sswitch_9
    const-string v13, "code"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x9

    goto :goto_2

    :sswitch_a
    const-string v13, "description"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0xa

    goto :goto_2

    .line 162
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->userIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "userId":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 163
    .restart local v1    # "userId":Ljava/lang/String;
    goto/16 :goto_1

    .line 166
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->rolesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    check-cast v2, Lcom/google/common/collect/ImmutableList;

    .line 167
    .restart local v2    # "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    goto/16 :goto_1

    .line 170
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "followQuotaMax":Ljava/lang/Integer;
    check-cast v3, Ljava/lang/Integer;

    .line 171
    .restart local v3    # "followQuotaMax":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 174
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "followQuotaRemaining":Ljava/lang/Integer;
    check-cast v4, Ljava/lang/Integer;

    .line 175
    .restart local v4    # "followQuotaRemaining":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 178
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "memberQuotaMax":Ljava/lang/Integer;
    check-cast v5, Ljava/lang/Integer;

    .line 179
    .restart local v5    # "memberQuotaMax":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 182
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "memberQuotaRemaining":Ljava/lang/Integer;
    check-cast v6, Ljava/lang/Integer;

    .line 183
    .restart local v6    # "memberQuotaRemaining":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 186
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "moderatorQuotaMax":Ljava/lang/Integer;
    check-cast v7, Ljava/lang/Integer;

    .line 187
    .restart local v7    # "moderatorQuotaMax":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 190
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "moderatorQuotaRemaining":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 191
    .restart local v8    # "moderatorQuotaRemaining":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 194
    :pswitch_8
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    check-cast v9, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 195
    .restart local v9    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    goto/16 :goto_1

    .line 198
    :pswitch_9
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorCodeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "errorCode":Ljava/lang/Integer;
    check-cast v10, Ljava/lang/Integer;

    .line 199
    .restart local v10    # "errorCode":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 202
    :pswitch_a
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorDescriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "errorDescription":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 203
    .restart local v11    # "errorDescription":Ljava/lang/String;
    goto/16 :goto_1

    .line 210
    .end local v12    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 211
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    :sswitch_data_0
    .sparse-switch
        -0x7e5c6b91 -> :sswitch_3
        -0x66ca7c04 -> :sswitch_a
        -0x492a100f -> :sswitch_6
        -0x3dca8f7a -> :sswitch_4
        -0x31d4d1ba -> :sswitch_0
        -0x24a3e868 -> :sswitch_5
        0x2eaded -> :sswitch_9
        0x5c4d208 -> :sswitch_8
        0x67a8ebd -> :sswitch_1
        0x1fb51a9d -> :sswitch_2
        0x2b9c1ac3 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultError(Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultError"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultError:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 94
    return-object p0
.end method

.method public setDefaultErrorCode(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultErrorCode"    # Ljava/lang/Integer;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorCode:Ljava/lang/Integer;

    .line 98
    return-object p0
.end method

.method public setDefaultErrorDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultErrorDescription"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultErrorDescription:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public setDefaultFollowQuotaMax(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFollowQuotaMax"    # Ljava/lang/Integer;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaMax:Ljava/lang/Integer;

    .line 70
    return-object p0
.end method

.method public setDefaultFollowQuotaRemaining(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFollowQuotaRemaining"    # Ljava/lang/Integer;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultFollowQuotaRemaining:Ljava/lang/Integer;

    .line 74
    return-object p0
.end method

.method public setDefaultMemberQuotaMax(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMemberQuotaMax"    # Ljava/lang/Integer;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaMax:Ljava/lang/Integer;

    .line 78
    return-object p0
.end method

.method public setDefaultMemberQuotaRemaining(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMemberQuotaRemaining"    # Ljava/lang/Integer;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultMemberQuotaRemaining:Ljava/lang/Integer;

    .line 82
    return-object p0
.end method

.method public setDefaultModeratorQuotaMax(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultModeratorQuotaMax"    # Ljava/lang/Integer;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaMax:Ljava/lang/Integer;

    .line 86
    return-object p0
.end method

.method public setDefaultModeratorQuotaRemaining(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultModeratorQuotaRemaining"    # Ljava/lang/Integer;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultModeratorQuotaRemaining:Ljava/lang/Integer;

    .line 90
    return-object p0
.end method

.method public setDefaultRoles(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "defaultRoles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultRoles:Lcom/google/common/collect/ImmutableList;

    .line 66
    return-object p0
.end method

.method public setDefaultUserId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserId"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->defaultUserId:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    if-nez p2, :cond_0

    .line 108
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 135
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 112
    const-string v0, "userId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->userIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 114
    const-string v0, "roles"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->rolesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 116
    const-string v0, "followQuotaMax"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaMax()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 118
    const-string v0, "followQuotaRemaining"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->followQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->followQuotaRemaining()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 120
    const-string v0, "memberQuotaMax"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaMax()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 122
    const-string v0, "memberQuotaRemaining"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->memberQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaRemaining()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 124
    const-string v0, "moderatorQuotaMax"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaMaxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaMax()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 126
    const-string v0, "moderatorQuotaRemaining"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->moderatorQuotaRemainingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->moderatorQuotaRemaining()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 128
    const-string v0, "error"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->error()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 130
    const-string v0, "code"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorCodeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorCode()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 132
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->errorDescriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->errorDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 134
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)V

    return-void
.end method
