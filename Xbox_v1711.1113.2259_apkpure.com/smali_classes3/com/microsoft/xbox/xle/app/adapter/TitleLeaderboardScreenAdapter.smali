.class public Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "TitleLeaderboardScreenAdapter.java"


# instance fields
.field closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private firstPlaceContainer:Landroid/view/View;

.field private firstPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private leaderBoardName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

.field private myEntry:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

.field private myEntryContainer:Landroid/view/View;

.field private secondPlaceContainer:Landroid/view/View;

.field private secondPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private thirdPlaceContainer:Landroid/view/View;

.field private thirdPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private topPlayers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 42
    const v0, 0x7f0e0acb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 43
    const v0, 0x7f0e0aca

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->leaderBoardName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    const v0, 0x7f0e0ad3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 45
    const v0, 0x7f0e0ad1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 46
    const v0, 0x7f0e0ad5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 47
    const v0, 0x7f0e0ad2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceContainer:Landroid/view/View;

    .line 48
    const v0, 0x7f0e0ad0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceContainer:Landroid/view/View;

    .line 49
    const v0, 0x7f0e0ad4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceContainer:Landroid/view/View;

    .line 50
    const v0, 0x7f0e0acd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntryContainer:Landroid/view/View;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntryContainer:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntry:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

    .line 52
    const v0, 0x7f0e022a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .line 55
    const v0, 0x7f0e0acc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030230

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    return-object v0
.end method

.method private setProfilePic(Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V
    .locals 2
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .param p2, "user"    # Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .prologue
    const v1, 0x7f020125

    .line 170
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;->getUserProfilePicUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 171
    return-void
.end method

.method private updateMyEntry()V
    .locals 3

    .prologue
    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->hasSeparateMyEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntryContainer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->getMyRank()Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    move-result-object v0

    .line 188
    .local v0, "myRankInfo":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntry:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isTimeLeaderboard()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->updateContent(Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;Z)V

    .line 192
    .end local v0    # "myRankInfo":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->myEntryContainer:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->trackPageView()V

    .line 121
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    return-void
.end method

.method public updateViewOverride()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->updateLoadingIndicator(Z)V

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->leaderBoardName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->getLeaderBoardName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->updateMyEntry()V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->getTopPlayers()Ljava/util/ArrayList;

    move-result-object v0

    .line 143
    .local v0, "newData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    if-eq v1, v0, :cond_3

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->clear()V

    .line 146
    if-eqz v0, :cond_0

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->addAll(Ljava/util/Collection;)V

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 150
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->firstPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->setProfilePic(Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v4, :cond_2

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->secondPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->setProfilePic(Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V

    .line 161
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v5, :cond_3

    .line 162
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->thirdPlaceProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->topPlayers:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;->setProfilePic(Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;)V

    .line 167
    :cond_3
    return-void
.end method
