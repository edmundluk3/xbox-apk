.class final enum Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;
.super Ljava/lang/Enum;
.source "AuthStateManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AuthAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

.field public static final enum SignInSilentlyXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

.field public static final enum SignInWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

.field public static final enum SignOutWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

.field public static final enum SignOutXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    const-string v1, "SignInSilentlyXsapi"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInSilentlyXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    const-string v1, "SignOutXsapi"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    const-string v1, "SignOutWithUi"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    const-string v1, "SignInWithUi"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInSilentlyXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->$VALUES:[Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->$VALUES:[Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    return-object v0
.end method
