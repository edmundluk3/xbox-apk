.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_ViewBinding;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.source "MyClubsViewImpl_ViewBinding.java"


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;Landroid/view/View;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    .line 28
    const v0, 0x7f0e07dc

    const-string v1, "field \'contentList\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 29
    const v0, 0x7f0e0590

    const-string v1, "field \'refreshButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->refreshButton:Landroid/widget/Button;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    .line 35
    .local v0, "target":Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    .line 38
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->refreshButton:Landroid/widget/Button;

    .line 41
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->unbind()V

    .line 42
    return-void
.end method
