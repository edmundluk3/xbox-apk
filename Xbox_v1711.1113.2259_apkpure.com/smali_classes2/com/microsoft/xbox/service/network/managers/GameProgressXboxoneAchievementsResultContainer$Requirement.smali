.class public Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;
.super Ljava/lang/Object;
.source "GameProgressXboxoneAchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Requirement"
.end annotation


# instance fields
.field public current:Ljava/lang/String;

.field public operationType:Ljava/lang/String;

.field public target:Ljava/lang/String;

.field public valueType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOperationType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->operationType:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;->valueFor(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementOperationType;

    move-result-object v0

    return-object v0
.end method

.method public getValueType()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Requirement;->valueType:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->valueFor(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    move-result-object v0

    return-object v0
.end method
