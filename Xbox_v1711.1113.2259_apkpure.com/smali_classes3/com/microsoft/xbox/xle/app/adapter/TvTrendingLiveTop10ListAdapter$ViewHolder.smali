.class Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "TvTrendingLiveTop10ListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private descTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$1;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->descTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->descTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method
