.class Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;
.super Ljava/lang/Object;
.source "GamertagSearchDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onSearchBarClear()V

    .line 156
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 151
    return-void
.end method

.method public onQueryTextSubmit(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 144
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog$7;->this$0:Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/GamertagSearchDialog;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertag(Ljava/lang/String;)V

    .line 147
    :cond_0
    return-void
.end method
