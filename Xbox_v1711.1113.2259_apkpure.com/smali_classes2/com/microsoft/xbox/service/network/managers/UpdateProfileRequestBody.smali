.class public Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody;
.super Ljava/lang/Object;
.source "UpdateProfileRequestBody.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody$UserSetting;
    }
.end annotation


# instance fields
.field userSetting:Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody$UserSetting;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)V
    .locals 2
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody$UserSetting;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody$UserSetting;-><init>(Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody;->userSetting:Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody$UserSetting;

    .line 20
    return-void
.end method
