.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Launcher;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Launcher"
.end annotation


# static fields
.field public static final Details:Ljava/lang/String; = "Launcher - Show Details"

.field public static final GameHub:Ljava/lang/String; = "Launcher - Show Game Hub"

.field public static final Pin:Ljava/lang/String; = "Launcher - Pin to Home"

.field public static final PlayToXbox:Ljava/lang/String; = "Launcher - Play to Xbox"

.field public static final Unpin:Ljava/lang/String; = "Launcher - Unpin"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
