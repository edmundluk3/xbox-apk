.class Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setIsSuggestion(Z)V

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->toggleSendButton()V

    .line 184
    return-void
.end method
