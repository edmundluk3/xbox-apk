.class public final enum Lcom/microsoft/xbox/service/model/StoreModel;
.super Ljava/lang/Enum;
.source "StoreModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/StoreModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/StoreModel;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;


# instance fields
.field private browseModels:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/microsoft/cll/android/Pair",
            "<",
            "Lcom/microsoft/xbox/service/model/StoreBrowseType;",
            "Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;",
            ">;",
            "Lcom/microsoft/xbox/service/model/StoreBrowseModel;",
            ">;"
        }
    .end annotation
.end field

.field private searchModel:Lcom/microsoft/xbox/service/model/StoreSearchModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreModel;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    .line 9
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/StoreModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->$VALUES:[Lcom/microsoft/xbox/service/model/StoreModel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreModel;->browseModels:Ljava/util/concurrent/ConcurrentHashMap;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/StoreSearchModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreModel;->searchModel:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/StoreModel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/model/StoreModel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/StoreModel;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/StoreModel;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->$VALUES:[Lcom/microsoft/xbox/service/model/StoreModel;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/StoreModel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/StoreModel;

    return-object v0
.end method


# virtual methods
.method public getSearchModel()Lcom/microsoft/xbox/service/model/StoreSearchModel;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreModel;->searchModel:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    return-object v0
.end method

.method public getStoreBrowseModel(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    .locals 4
    .param p1, "browseType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p2, "browseFilter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .prologue
    .line 21
    new-instance v0, Lcom/microsoft/cll/android/Pair;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/cll/android/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    .local v0, "key":Lcom/microsoft/cll/android/Pair;, "Lcom/microsoft/cll/android/Pair<Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreModel;->browseModels:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 25
    .local v1, "model":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    if-nez v1, :cond_0

    .line 27
    new-instance v2, Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-direct {v2, p1, p2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V

    .line 28
    .local v2, "newModel":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreModel;->browseModels:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "model":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    check-cast v1, Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 29
    .restart local v1    # "model":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    if-nez v1, :cond_0

    .line 31
    move-object v1, v2

    .line 35
    .end local v2    # "newModel":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    :cond_0
    return-object v1
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreModel;->browseModels:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 42
    .local v0, "browseModel":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->reset()V

    goto :goto_0

    .line 44
    .end local v0    # "browseModel":Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreModel;->browseModels:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 45
    return-void
.end method
