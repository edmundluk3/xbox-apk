.class Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;
.super Landroid/app/Dialog;
.source "NotificationDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InAppNotification"
.end annotation


# instance fields
.field private final notificationType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "notificationType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    const/16 v3, 0x8

    .line 561
    const v2, 0x7f08010e

    invoke-direct {p0, p1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 562
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->notificationType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 563
    const v2, 0x7f030183

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->setContentView(I)V

    .line 564
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 565
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x31

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 566
    const v2, 0x7f0e07de

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 567
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 569
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 577
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 580
    .local v0, "dialogContext":Landroid/content/Context;
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 581
    check-cast v0, Landroid/app/Activity;

    .end local v0    # "dialogContext":Landroid/content/Context;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 582
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 585
    .restart local v0    # "dialogContext":Landroid/content/Context;
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public getNotificationType()Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay$InAppNotification;->notificationType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    return-object v0
.end method
