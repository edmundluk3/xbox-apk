.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MutePartyResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;
    .locals 2

    .prologue
    .line 528
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_MutePartyResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_MutePartyResult;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 532
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 533
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_MutePartyResult;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_MutePartyResult;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;->isError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MutePartyResult error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;->error()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 545
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "MutePartyResult completed"

    goto :goto_0
.end method
