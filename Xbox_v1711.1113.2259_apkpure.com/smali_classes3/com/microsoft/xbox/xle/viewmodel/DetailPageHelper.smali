.class public Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;
.super Ljava/lang/Object;
.source "DetailPageHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DetailPageHelper"

.field static final THREE_ITEM_LIST_WIDTH:I = 0x153


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDetailPivotHeaderData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;)Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;
    .locals 3
    .param p0, "screenType"    # Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    .prologue
    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper$1;->$SwitchMap$com$microsoft$xbox$service$model$eds$DetailDisplayScreenType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 206
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 187
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;

    const v2, 0x7f030177

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 189
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;

    const v2, 0x7f030249

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 192
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    const v2, 0x7f03024e

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 195
    :pswitch_4
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;

    const v2, 0x7f030111

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 197
    :pswitch_5
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;

    const v2, 0x7f0300e1

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 199
    :pswitch_6
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    const v2, 0x7f03003b

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 201
    :pswitch_7
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    const v2, 0x7f030038

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 203
    :pswitch_8
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;

    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;

    const v2, 0x7f0300a6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/DetailPivotHeaderData;-><init>(Ljava/lang/Class;I)V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method

.method public static getDetailScreenTypeFromMediaType(I)Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    .line 215
    sparse-switch p0, :sswitch_data_0

    .line 264
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->Unknown:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    :goto_0
    return-object v0

    .line 225
    :sswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 227
    :sswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameDemoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 236
    :sswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->GameContentDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 238
    :sswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->VideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 240
    :sswitch_4
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicVideoDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 242
    :sswitch_5
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 244
    :sswitch_6
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AlbumDetailsFromTrack:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 246
    :sswitch_7
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvShowDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 248
    :sswitch_8
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvEpisodeDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 250
    :sswitch_9
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->MusicArtistDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 253
    :sswitch_a
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->AppDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 255
    :sswitch_b
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeriesDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 257
    :sswitch_c
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->TvSeasonDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 262
    :sswitch_d
    sget-object v0, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->ActivityDetails:Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    goto :goto_0

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
        0x12 -> :sswitch_2
        0x13 -> :sswitch_0
        0x14 -> :sswitch_2
        0x15 -> :sswitch_0
        0x16 -> :sswitch_2
        0x17 -> :sswitch_0
        0x18 -> :sswitch_2
        0x1e -> :sswitch_2
        0x22 -> :sswitch_2
        0x25 -> :sswitch_0
        0x39 -> :sswitch_0
        0x3a -> :sswitch_0
        0x3d -> :sswitch_a
        0x41 -> :sswitch_d
        0x3e8 -> :sswitch_3
        0x3ea -> :sswitch_7
        0x3eb -> :sswitch_8
        0x3ec -> :sswitch_b
        0x3ed -> :sswitch_c
        0x3ee -> :sswitch_5
        0x3ef -> :sswitch_6
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_9
        0x2328 -> :sswitch_a
        0x2329 -> :sswitch_0
        0x232a -> :sswitch_1
        0x232b -> :sswitch_2
        0x232c -> :sswitch_2
        0x232d -> :sswitch_d
        0x232e -> :sswitch_d
        0x232f -> :sswitch_d
    .end sparse-switch
.end method

.method public static getPaneConfigData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .locals 10
    .param p0, "screenType"    # Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 50
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper$1;->$SwitchMap$com$microsoft$xbox$service$model$eds$DetailDisplayScreenType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 179
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 52
    :pswitch_0
    const/4 v1, 0x1

    .line 53
    .local v1, "released":Z
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 54
    .local v0, "releaseDate":Ljava/util/Date;
    if-eqz v0, :cond_0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const/4 v1, 0x0

    .line 58
    :cond_0
    const/16 v2, 0x8

    new-array v2, v2, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/IncludedContentScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameDetailsProgressPhoneScreen;

    invoke-direct {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v9

    const/4 v3, 0x5

    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/BundlesScreen;

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/GameGalleryScreen;

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v4, v2, v3

    goto :goto_0

    .line 71
    .end local v0    # "releaseDate":Ljava/util/Date;
    .end local v1    # "released":Z
    :pswitch_1
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/IncludedContentScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/BundlesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameGalleryScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 79
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->shouldShowLiveTVPivots()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 88
    :cond_1
    new-array v2, v8, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    goto/16 :goto_0

    .line 95
    :pswitch_3
    new-array v2, v6, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/AlbumDetailScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    goto/16 :goto_0

    .line 98
    :pswitch_4
    new-array v2, v6, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/AlbumDetailScreenFromTrack;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    goto/16 :goto_0

    .line 101
    :pswitch_5
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->shouldShowLiveTVPivots()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 102
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeSeasonScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v9

    goto/16 :goto_0

    .line 109
    :cond_2
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeSeasonScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 117
    :pswitch_6
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->shouldShowLiveTVPivots()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 118
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v9

    goto/16 :goto_0

    .line 125
    :cond_3
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/CastCrewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 133
    :pswitch_7
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailTopSongsScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailAlbumsScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ArtistDetailBiographyScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 140
    :pswitch_8
    new-array v2, v6, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    goto/16 :goto_0

    .line 143
    :pswitch_9
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/AppDetailOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/IncludedContentScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/BundlesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 151
    :pswitch_a
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->shouldShowLiveTVPivots()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 152
    new-array v2, v9, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVSeriesSeasonsScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    goto/16 :goto_0

    .line 158
    :cond_4
    new-array v2, v8, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVSeriesOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/TVSeriesSeasonsScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    goto/16 :goto_0

    .line 166
    :pswitch_b
    new-array v2, v8, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ActivityGalleryActivity;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ActivityParentScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    goto/16 :goto_0

    .line 171
    :pswitch_c
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/DLCOverviewScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v5

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/IncludedContentScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v6

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ParentItemScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/BundlesScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v8

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameGalleryScreen;

    invoke-direct {v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    aput-object v3, v2, v9

    goto/16 :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static isActivityDetailsPivotPaneData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)Z
    .locals 4
    .param p0, "pivotPaneData"    # [Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 211
    if-eqz p0, :cond_0

    array-length v2, p0

    if-le v2, v0, :cond_0

    aget-object v2, p0, v1

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static shouldShowLiveTVPivots()Z
    .locals 1

    .prologue
    .line 272
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
