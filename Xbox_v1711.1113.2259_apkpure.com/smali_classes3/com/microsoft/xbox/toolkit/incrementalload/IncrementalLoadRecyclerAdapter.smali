.class public abstract Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "IncrementalLoadRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "VH:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<TT;TVH;>;"
    }
.end annotation


# instance fields
.field private incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final onMoreItemsLoadedListener:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter<TT;TVH;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 16
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->onMoreItemsLoadedListener:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->onMoreItemsLoaded(Ljava/util/Collection;)V

    return-void
.end method

.method private onMoreItemsLoaded(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter<TT;TVH;>;"
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<+TT;>;"
    if-eqz p1, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->getItemCount()I

    move-result v1

    .line 37
    .local v1, "previousCount":I
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    add-int v0, v1, v2

    .line 39
    .local v0, "newCount":I
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->addAll(Ljava/util/Collection;)V

    .line 40
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->notifyItemRangeInserted(II)V

    .line 42
    .end local v0    # "newCount":I
    .end local v1    # "previousCount":I
    :cond_0
    return-void
.end method


# virtual methods
.method public setIncrementalLoader(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter<TT;TVH;>;"
    .local p1, "incrementalLoader":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    if-ne v0, p1, :cond_1

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    if-eqz v0, :cond_2

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->onMoreItemsLoadedListener:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;->removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 27
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;->onMoreItemsLoadedListener:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;->addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    goto :goto_0
.end method
