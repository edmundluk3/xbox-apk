.class Lcom/microsoft/xbox/smartglass/controls/MediaV1;
.super Lcom/microsoft/xbox/smartglass/controls/Media;
.source "MediaV1.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 0
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 10
    return-void
.end method


# virtual methods
.method protected getState(ILjava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->getState(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 15
    return-void
.end method

.method protected seek(ILjava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 20
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 21
    .local v2, "seekPosition":J
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v1

    invoke-virtual {p0, p1, v2, v3, v1}, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->seek(IJLcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .end local v2    # "seekPosition":J
    :goto_0
    return-void

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "ex":Ljava/lang/NumberFormatException;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v4, "Invalid seek position."

    invoke-interface {v1, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected send(ILjava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/MediaV1;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v1, "Not implemented in this version of SmartGlass."

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 31
    return-void
.end method
