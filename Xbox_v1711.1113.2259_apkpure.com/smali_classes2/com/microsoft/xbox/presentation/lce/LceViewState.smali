.class public abstract Lcom/microsoft/xbox/presentation/lce/LceViewState;
.super Ljava/lang/Object;
.source "LceViewState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadingInstance()Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 33
    new-instance v0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static loadingMoreInstance(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 4
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "existingContent":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "content":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .line 23
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v2, v2}, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 3
    .param p0, "err"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, v2, v2}, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;ZZ)V

    return-object v0
.end method


# virtual methods
.method public abstract content()Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isContent()Z
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoadingMore()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoadingMore()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isLoading()Z
.end method

.method public abstract isLoadingMore()Z
.end method
