.class public Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ConversationViewAllMembersScreenViewModel.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private conversationId:Ljava/lang/String;

.field private final messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 29
    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->TAG:Ljava/lang/String;

    .line 31
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 36
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 39
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getViewMemberConversationId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->conversationId:Ljava/lang/String;

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->conversationId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 43
    return-void

    .line 40
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public confirm()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->goBack()V

    .line 65
    return-void
.end method

.method public getConversationMembers()Ljava/util/Collection;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getListWithHeader()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 132
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getConversationMembers()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getOfflineMembers(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 133
    .local v2, "offlineList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getConversationMembers()Ljava/util/Collection;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getOnlineMembers(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    .line 134
    .local v4, "onlineList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v0, "aggregateResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 137
    new-instance v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;->DUMMY_HEADER_ONLINE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    invoke-direct {v1, v8, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(ZLcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;)V

    .line 138
    .local v1, "dummyMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 141
    .local v5, "onlineMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v1    # "dummyMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v5    # "onlineMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 146
    new-instance v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;->DUMMY_HEADER_OFFLINE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;

    invoke-direct {v1, v8, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(ZLcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember$DummyType;)V

    .line 147
    .restart local v1    # "dummyMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 149
    .local v3, "offlineMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    .end local v1    # "dummyMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v3    # "offlineMember":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_1
    return-object v0
.end method

.method public getOfflineMembers(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v1, "offline":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 124
    .local v0, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iget-object v3, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v3, v4, :cond_0

    .line 125
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    .end local v0    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_1
    return-object v1
.end method

.method public getOnlineMembers(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v1, "online":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 113
    .local v0, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iget-object v3, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v4, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v3, v4, :cond_0

    .line 114
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    .end local v0    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_1
    return-object v1
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 75
    return-void
.end method

.method public navigateToConversationMemberProfile(Ljava/lang/String;)V
    .locals 2
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Unable to navigate to you profile, invalid xuid!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public navigateToFriendsPickerScreen()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Navigating to Friend Selector"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/MultiSelection;->reset()V

    .line 82
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 83
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putIsGroupConversation(Z)V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->getConversationMembers()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putViewAllMembers(Ljava/util/Collection;)V

    .line 85
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    invoke-virtual {p0, v1, v5, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 94
    .end local v0    # "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "User has no privilege to send message"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f07061d

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f07061c

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0707c7

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 88
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsViewAllMembersScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 48
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method
