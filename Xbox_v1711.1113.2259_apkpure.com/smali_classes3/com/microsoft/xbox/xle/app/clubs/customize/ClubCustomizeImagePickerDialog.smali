.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubCustomizeImagePickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;
    }
.end annotation


# static fields
.field private static final COLUMN_MARGIN:I = 0x14

.field private static final COLUMN_WIDTH:I = 0x64

.field private static final CUSTOM_GAMERPIC_DIMEN:I = 0x438


# instance fields
.field private addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final clubId:J

.field private currentFilter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

.field private final filterSpinnerArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;",
            ">;"
        }
    .end annotation
.end field

.field private getClubpicsCall:Lretrofit2/Call;

.field private getGamerpicsCall:Lretrofit2/Call;

.field private final imageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

.field private final imageMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final imageSelectionListener:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

.field private final pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private final progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p4, "imageSelectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 81
    const v2, 0x7f080238

    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 78
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    .line 82
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 83
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 84
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 86
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->clubId:J

    .line 87
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    .line 89
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageSelectionListener:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

    .line 91
    const v2, 0x7f03007f

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->setContentView(I)V

    .line 92
    const v2, 0x7f0e0348

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->progressBar:Landroid/widget/ProgressBar;

    .line 94
    const v2, 0x7f0e0347

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 95
    .local v1, "filterSpinner":Landroid/widget/Spinner;
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v3, 0x7f030202

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->values()[Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, p1, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->filterSpinnerArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->filterSpinnerArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->filterSpinnerArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v3, 0x7f03020a

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 100
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->calculateNoOfColumns(Landroid/content/Context;)I

    move-result v0

    .line 103
    .local v0, "columnNumbers":I
    const v2, 0x7f0e0349

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 104
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/GridLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v3, v4, v0, v5, v6}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;

    const/16 v4, 0x14

    invoke-direct {v3, v0, v4}, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 106
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v2, p0, v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$1;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    .line 107
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 109
    const v2, 0x7f0e0255

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 110
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->removeLabelLeftMargin()V

    .line 112
    const v2, 0x7f0e0346

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageSelectionListener:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->onLoadClubpicListSuccess(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/lang/Void;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->onLoadClubpicListFailure(Ljava/lang/Void;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->onLoadGamerpicListSuccess(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$3(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/lang/Void;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->onLoadGamerpicListFailure(Ljava/lang/Void;)V

    return-void
.end method

.method static synthetic access$lambda$4(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->onCustomImageSelected(Landroid/net/Uri;)V

    return-void
.end method

.method private calculateNoOfColumns(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 180
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v2, v3, v4

    .line 181
    .local v2, "dpWidth":F
    const/16 v3, 0x14

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertPixelsToDp(ILandroid/content/Context;)I

    move-result v0

    .line 182
    .local v0, "columnMarginInDp":I
    add-int/lit8 v3, v0, 0x64

    int-to-float v3, v3

    div-float v3, v2, v3

    float-to-int v3, v3

    return v3
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->filterSpinnerArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->setFilter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->dismiss()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/view/View;)V
    .locals 9
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x438

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->getPrivResult()Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    move-result-object v0

    .line 124
    .local v0, "customPicPrivResult":Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$1;->$SwitchMap$com$microsoft$xbox$xle$app$uploadCustomPic$CustomPicPrivCheck$PrivResult:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected priv result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-void

    .line 126
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackUseCustomLogo()V

    .line 127
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->clubId:J

    sget-object v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubLogo:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 133
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v8

    .line 130
    invoke-static {v4, v5, v6, v7, v8}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->getRectangleInstance(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/microsoft/xbox/toolkit/generics/Action;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    move-result-object v4

    .line 127
    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 138
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showPrivFailureDialog()V

    goto :goto_0

    .line 142
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showChildFailureDialog()V

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onCustomImageSelected(Landroid/net/Uri;)V
    .locals 2
    .param p1, "imageUri"    # Landroid/net/Uri;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageSelectionListener:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageSelectionListener;->onImageSelected(Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method private onLoadClubpicListFailure(Ljava/lang/Void;)V
    .locals 3
    .param p1, "v"    # Ljava/lang/Void;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->updateDialog()V

    .line 216
    return-void
.end method

.method private onLoadClubpicListSuccess(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 205
    .local p1, "clubpicList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->updateDialog()V

    .line 210
    return-void
.end method

.method private onLoadGamerpicListFailure(Ljava/lang/Void;)V
    .locals 3
    .param p1, "v"    # Ljava/lang/Void;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->Gamerpics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->updateDialog()V

    .line 230
    return-void
.end method

.method private onLoadGamerpicListSuccess(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "gamerpicList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->Gamerpics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->updateDialog()V

    .line 224
    return-void
.end method

.method private setFilter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    .line 187
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->updateDialog()V

    .line 188
    return-void
.end method

.method private updateDialog()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 191
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->currentFilter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 193
    .local v0, "imageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 198
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->pictureRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->imageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->addAll(Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method protected onStart()V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->customPicUploadEnabled()Z

    move-result v0

    .line 120
    .local v0, "showCustomButtom":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->getClubpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getClubpicsCall:Lretrofit2/Call;

    .line 152
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->getGamerpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getGamerpicsCall:Lretrofit2/Call;

    .line 153
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getClubpicsCall:Lretrofit2/Call;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getClubpicsCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->cancel()V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getGamerpicsCall:Lretrofit2/Call;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;->getGamerpicsCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->cancel()V

    .line 166
    :cond_1
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 172
    return-void
.end method
