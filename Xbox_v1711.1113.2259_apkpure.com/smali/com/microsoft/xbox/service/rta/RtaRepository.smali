.class public final Lcom/microsoft/xbox/service/rta/RtaRepository;
.super Ljava/lang/Object;
.source "RtaRepository.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final RTA_SUBSCRIBE_FORMAT:Ljava/lang/String; = "[1, %s, \"%s\"]"

.field private static final RTA_SUB_PROTOCOL:Ljava/lang/String; = "rta.xboxlive.com.V2"

.field private static final RTA_UNSUBSCRIBE_FORMAT:Ljava/lang/String; = "[2,%s,%s]"

.field private static final RTA_WEBSOCKET_ENDPOINT:Ljava/lang/String; = "https://rta.xboxlive.com/connect"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentSequenceNumber:I

.field private knownSubscriptions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rtaConnectionObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation
.end field

.field rtaDataMapper:Lcom/microsoft/xbox/service/rta/RtaDataMapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sequenceToSubscriptionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sequenceToUnsubscribeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private subscriptionsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/service/rta/RtaRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lokhttp3/OkHttpClient;)V
    .locals 3
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "XTOKEN_OK_HTTP"
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    .line 68
    new-instance v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    const-string v1, "https://rta.xboxlive.com/connect"

    const-string/jumbo v2, "rta.xboxlive.com.V2"

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;-><init>(Lokhttp3/OkHttpClient;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    invoke-static {p0}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/rta/RtaRepository;)Lio/reactivex/functions/Action;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->doOnComplete(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/service/rta/RtaRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/service/rta/RtaRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->rtaConnectionObservable:Lio/reactivex/Observable;

    .line 87
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/service/rta/RtaRepository;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/rta/RtaRepository;->clearSubscriptionMaps()V

    return-void
.end method

.method private declared-synchronized clearSubscriptionMaps()V
    .locals 1

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized handleSubscribeResponse(Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;)V
    .locals 4
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->errorType()Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;->Success:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    if-ne v0, v1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :goto_1
    monitor-exit p0

    return-void

    .line 128
    :cond_0
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RTA SUBSCRIBE failed, removing subscription for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :cond_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    const-string v1, "Received RTA SUBSCRIBE for unknown request!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized handleUnsubscribeResponse(Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;)V
    .locals 4
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->errorCode()I

    move-result v1

    if-nez v1, :cond_1

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    .local v0, "unsubscribedTarget":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    .end local v0    # "unsubscribedTarget":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 149
    :cond_1
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received error from RTA UNSUBSCRIBE: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;->errorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 152
    :cond_2
    :try_start_2
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    const-string v2, "Received RTA UNSUBSCRIBE for unknown sequence ID. Ignoring."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    const-class v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;->text()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/service/rta/RtaRepository;Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/rta/RtaRepository;
    .param p1, "event"    # Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->rtaDataMapper:Lcom/microsoft/xbox/service/rta/RtaDataMapper;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/service/rta/RtaRepository;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/rta/RtaRepository;
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    instance-of v0, p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;

    if-eqz v0, :cond_1

    .line 81
    check-cast p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;

    .end local p1    # "rtaResponse":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaRepository;->handleSubscribeResponse(Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaSubscriptionResponse;)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 82
    .restart local p1    # "rtaResponse":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;

    if-eqz v0, :cond_0

    .line 83
    check-cast p1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;

    .end local p1    # "rtaResponse":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaRepository;->handleUnsubscribeResponse(Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaUnsubscribeResponse;)V

    goto :goto_0
.end method

.method static synthetic lambda$subscribeToRtaEvents$4(Lcom/microsoft/xbox/service/rta/RtaRepository;Ljava/lang/String;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/rta/RtaRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {p2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized sendSubscribeMessage(Ljava/lang/String;)V
    .locals 5
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 157
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 158
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendSubscribeMessage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v1, "[1, %s, \"%s\"]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "joinString":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    iget v2, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    .line 166
    iget-object v1, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->sendData(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    monitor-exit p0

    return-void

    .line 157
    .end local v0    # "joinString":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized subscribeToRtaEvents(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaRepository;->sendSubscribeMessage(Ljava/lang/String;)V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->rtaConnectionObservable:Lio/reactivex/Observable;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaRepository$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/service/rta/RtaRepository;Ljava/lang/String;)Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 107
    monitor-exit p0

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unsubscribeFromAllRtaEvents()V
    .locals 7

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 113
    .local v0, "kvPair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    const-string v3, "[2,%s,%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "unsubscribeString":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    iget v4, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget v3, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->currentSequenceNumber:I

    .line 117
    iget-object v3, p0, Lcom/microsoft/xbox/service/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocket;->sendData(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 112
    .end local v0    # "kvPair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v1    # "unsubscribeString":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 119
    :cond_0
    monitor-exit p0

    return-void
.end method
