.class public final enum Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
.super Ljava/lang/Enum;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserPermissionSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

.field public static final enum Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "error"
    .end annotation
.end field

.field public static final enum IsAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowed"
    .end annotation
.end field

.field public static final enum NotAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "NotAllowed"
    .end annotation
.end field

.field public static final enum UserBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BlockListRestrictsTarget"
    .end annotation
.end field

.field public static final enum UserPrivacyBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PrivacySettingRestrictsTarget"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 220
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    const-string v1, "IsAllowed"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->IsAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    .line 222
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    const-string v1, "NotAllowed"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->NotAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    .line 224
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    const-string v1, "UserBlocked"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->UserBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    .line 226
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    const-string v1, "UserPrivacyBlocked"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->UserPrivacyBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    .line 228
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    const-string v1, "Error"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    .line 219
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->IsAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->NotAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->UserBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->UserPrivacyBlocked:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 219
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
