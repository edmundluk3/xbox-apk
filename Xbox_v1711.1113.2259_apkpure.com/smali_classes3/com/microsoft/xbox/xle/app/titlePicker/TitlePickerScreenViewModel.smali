.class public Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
.source "TitlePickerScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;
    }
.end annotation


# instance fields
.field private final maxSelectable:I

.field private final selectedTitles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private titleInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 3
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    .line 42
    .local v0, "params":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;
    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->access$000(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    .line 43
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 44
    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->access$200(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)I

    move-result v1

    :goto_2
    iput v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    .line 45
    return-void

    .line 42
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 43
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 44
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private showTooManySelectedDialog()V
    .locals 7

    .prologue
    .line 159
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0701c7

    .line 160
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0701c6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    .line 161
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 162
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 159
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 164
    return-void
.end method


# virtual methods
.method protected createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTitlePickerScreenAdapter(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method public getSearchResultTitleInfoList()Ljava/util/List;
    .locals 10
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getSearchResults()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    .line 90
    .local v2, "searchResults":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v2, :cond_1

    .line 91
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 92
    .local v3, "title":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    new-instance v4, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v4, v3}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    .line 94
    .local v4, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 95
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 98
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->setSelected(Z)V

    goto :goto_0

    .line 104
    .end local v3    # "title":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v4    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getPcSearchResults()Ljava/util/List;

    move-result-object v0

    .line 106
    .local v0, "pcTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 107
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;

    .line 108
    .local v3, "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    new-instance v6, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v6, v3}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    .end local v3    # "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    :cond_2
    return-object v1
.end method

.method public getTitleInfoList()Ljava/util/List;
    .locals 12
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 60
    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->getRecentlyPlayedTitles()Ljava/util/List;

    move-result-object v2

    .line 64
    .local v2, "recentTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 65
    .local v3, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    new-instance v4, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v4, v3}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    .line 67
    .local v4, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    const/4 v0, 0x0

    .line 68
    .local v0, "contained":Z
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 69
    .local v1, "otherTitleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 70
    const/4 v0, 0x1

    .line 75
    .end local v1    # "otherTitleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_2
    if-nez v0, :cond_0

    .line 76
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v0    # "contained":Z
    .end local v2    # "recentTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    .end local v3    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .end local v4    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    return-object v5
.end method

.method public onDoneButton()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->goBack()V

    .line 173
    return-void
.end method

.method protected onRecentTitleListUpdated()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->updateAdapter()V

    .line 120
    return-void
.end method

.method public onSearchResultSelectionChanged(Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)Z
    .locals 4
    .param p1, "titleInfo"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 123
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 124
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 125
    iget v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    if-ge v2, v3, :cond_1

    .line 126
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-interface {v2, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 138
    :goto_0
    return v0

    .line 131
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->showTooManySelectedDialog()V

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->titleInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 138
    goto :goto_0
.end method

.method public onTitleInfoListSelectionChanged(Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)Z
    .locals 3
    .param p1, "titleInfo"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 143
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 144
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 145
    iget v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->maxSelectable:I

    if-ge v1, v2, :cond_1

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    :goto_0
    return v0

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->showTooManySelectedDialog()V

    .line 150
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public selectTitle(JLjava/lang/String;)V
    .locals 0
    .param p1, "titleId"    # J
    .param p3, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    return-void
.end method
