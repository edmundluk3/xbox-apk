.class public Lcom/microsoft/xbox/smartglass/SGException;
.super Ljava/lang/Exception;
.source "SGException.java"


# static fields
.field private static final serialVersionUID:J = -0x69a46c9c43bbea33L


# instance fields
.field public final result:Lcom/microsoft/xbox/smartglass/SGResult;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "independentError"    # I
    .param p2, "platformError"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGException;->result:Lcom/microsoft/xbox/smartglass/SGResult;

    .line 32
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "independentError"    # I
    .param p2, "platformError"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 42
    invoke-direct {p0, p3, p4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGException;->result:Lcom/microsoft/xbox/smartglass/SGResult;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/smartglass/SGResult;

    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGException;->result:Lcom/microsoft/xbox/smartglass/SGResult;

    .line 21
    return-void
.end method
