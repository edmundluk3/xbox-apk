.class public Lcom/microsoft/xbox/service/groupMessaging/SkypeService;
.super Ljava/lang/Object;
.source "SkypeService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "ex"    # Lcom/microsoft/xbox/toolkit/XLEException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEException;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "request":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;>;"
    const/16 v6, 0x3a5

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    const-wide/16 v4, 0x3fc

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-class v3, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;

    .line 66
    .local v0, "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->errorCode:I

    const/16 v3, 0xce

    if-eq v2, v3, :cond_3

    iget v2, v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->errorCode:I

    if-eq v2, v6, :cond_3

    .line 67
    :cond_0
    if-eqz v0, :cond_1

    .line 68
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->TAG:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "skype error code - %d, skype error message - %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->errorCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->message:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->TAG:Ljava/lang/String;

    const-string v3, "User banned or registration token expired - refreshing auth tokens"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->registerWithSkypeUsingRefreshedAuthTokens()V

    .line 76
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "retry_ex":Ljava/lang/Exception;
    instance-of v2, v1, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v2, :cond_2

    .line 79
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "retry_ex":Ljava/lang/Exception;
    throw v1

    .line 81
    .restart local v1    # "retry_ex":Ljava/lang/Exception;
    :cond_2
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x1

    invoke-direct {v2, v4, v5, v1}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v2

    .line 84
    .end local v1    # "retry_ex":Ljava/lang/Exception;
    :cond_3
    iget v2, v0, Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;->errorCode:I

    if-ne v2, v6, :cond_4

    .line 86
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x26e9

    invoke-direct {v2, v4, v5, p0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v2

    .line 88
    :cond_4
    throw p0

    .line 91
    .end local v0    # "errResponseBody":Lcom/microsoft/xbox/service/network/managers/SkypeConversationMessagesBadRequestResponseBody;
    :cond_5
    throw p0
.end method

.method public static deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "lastRedirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 49
    :goto_0
    return-object v1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    goto :goto_0
.end method

.method public static getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    :try_start_0
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 57
    :goto_0
    return-object v1

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService$$Lambda$5;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    goto :goto_0
.end method

.method public static getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "lastRedirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 25
    :goto_0
    return-object v1

    .line 24
    :catch_0
    move-exception v0

    .line 25
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService$$Lambda$1;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$deleteWithRedirect$3(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getStreamAndStatusWithBadRequestResponseBody$4(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getStreamAndStatusWithLastRedirectUri$0(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$postStreamAndStatusWithRedirect$1(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$putStreamWithRedirect$2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "responseHeaders"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "lastRedirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-static {p0, p1, p2, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 33
    :goto_0
    return-object v1

    .line 32
    :catch_0
    move-exception v0

    .line 33
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService$$Lambda$2;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    goto :goto_0
.end method

.method public static putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p4, "lastRedirectUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .local p3, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, p3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 41
    :goto_0
    return-object v1

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->checkForAuthFailureAndRetry(Lcom/microsoft/xbox/toolkit/XLEException;Ljava/util/concurrent/Callable;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v1

    goto :goto_0
.end method
