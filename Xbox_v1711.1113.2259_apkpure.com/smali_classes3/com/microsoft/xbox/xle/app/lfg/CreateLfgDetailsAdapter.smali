.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "CreateLfgDetailsAdapter.java"


# instance fields
.field private addEditDescriptionButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private addEditDescriptionLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private addEditDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private dateSpinner:Landroid/widget/Spinner;

.field private languageSpinner:Landroid/widget/Spinner;

.field private needCountSpinner:Landroid/widget/Spinner;

.field private selectedTagsHashCode:I

.field private selectedTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private selectedTitleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field private timeSpinner:Landroid/widget/Spinner;

.field private viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

.field private visibilityAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V
    .locals 18
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 53
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 54
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    .line 56
    const v14, 0x7f0e0726

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 57
    const v14, 0x7f0e0727

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 59
    const v14, 0x7f0e073f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 60
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getShareDescription()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 61
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$1;->lambdaFactory$()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 63
    const v14, 0x7f0e0729

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 64
    .local v3, "addTagsButtonFrame":Landroid/widget/RelativeLayout;
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const v14, 0x7f0e072e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 69
    const v14, 0x7f0e072c

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 70
    const v14, 0x7f0e072f

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 71
    const v14, 0x7f0e072d

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 72
    .local v2, "addEditDescriptionButtonFrame":Landroid/widget/RelativeLayout;
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const v14, 0x7f0e0738

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->needCountSpinner:Landroid/widget/Spinner;

    .line 78
    new-instance v9, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    const v15, 0x7f030202

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getNeedCountLabels()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v9, v14, v15, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 79
    .local v9, "needSpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v14, 0x7f03020a

    invoke-virtual {v9, v14}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 80
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->needCountSpinner:Landroid/widget/Spinner;

    invoke-virtual {v14, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 82
    const v14, 0x7f0e073b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Spinner;

    .line 83
    .local v13, "visibilitySpinner":Landroid/widget/Spinner;
    new-instance v14, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v15

    const v16, 0x7f030202

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getVisibilityOptions()Ljava/util/List;

    move-result-object v17

    invoke-direct/range {v14 .. v17}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->visibilityAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 84
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->visibilityAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v15, 0x7f03020a

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 85
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->visibilityAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v13, v14}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 86
    new-instance v14, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)V

    invoke-virtual {v13, v14}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 104
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/Spinner;->setSelection(I)V

    .line 106
    const v14, 0x7f0e0732

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->dateSpinner:Landroid/widget/Spinner;

    .line 107
    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    const v15, 0x7f030202

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDateOptions()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v5, v14, v15, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 108
    .local v5, "dateAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v14, 0x7f03020a

    invoke-virtual {v5, v14}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->dateSpinner:Landroid/widget/Spinner;

    invoke-virtual {v14, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 111
    const v14, 0x7f0e0735

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->timeSpinner:Landroid/widget/Spinner;

    .line 112
    new-instance v12, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    const v15, 0x7f030202

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getTimeOptions()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v12, v14, v15, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 113
    .local v12, "timeAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v14, 0x7f03020a

    invoke-virtual {v12, v14}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 114
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->timeSpinner:Landroid/widget/Spinner;

    invoke-virtual {v14, v12}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 116
    const v14, 0x7f0e073e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->languageSpinner:Landroid/widget/Spinner;

    .line 117
    new-instance v6, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    const v15, 0x7f030202

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getLanguageOptions()Ljava/util/List;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v6, v14, v15, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 118
    .local v6, "languageAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v14, 0x7f03020a

    invoke-virtual {v6, v14}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 119
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->languageSpinner:Landroid/widget/Spinner;

    invoke-virtual {v14, v6}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->languageSpinner:Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v15}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDefaultLanguageOffset()I

    move-result v15

    invoke-virtual {v14, v15}, Landroid/widget/Spinner;->setSelection(I)V

    .line 122
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v10

    .line 124
    .local v10, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    .line 125
    .local v8, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v15, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v16, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    if-eqz v8, :cond_0

    .line 127
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v14

    :goto_0
    move-object/from16 v0, v16

    invoke-direct {v15, v0, v14, v10}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 129
    const v14, 0x7f0e0728

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    .line 130
    .local v11, "selectedTagsList":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v11, v14}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 131
    invoke-interface {v10}, Ljava/util/List;->hashCode()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTagsHashCode:I

    .line 133
    const v14, 0x7f0e0721

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 134
    .local v7, "letsPlayButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v14, 0x7f0e0724

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 145
    .local v4, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v4, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    return-void

    .line 127
    .end local v4    # "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v7    # "letsPlayButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .end local v11    # "selectedTagsList":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;
    :cond_0
    const/4 v14, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->visibilityAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    return-object v0
.end method

.method static synthetic lambda$new$0(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p0, "view"    # Landroid/widget/CompoundButton;
    .param p1, "isChecked"    # Z

    .prologue
    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareNewLFG(Z)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->showTagPickerDialog()V

    .line 66
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 73
    const-string v0, "LFG - Add Description"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->showEditDescriptionDialog()V

    .line 75
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    const-string v1, "LFG - Done"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "share":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->shareCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->isChecked()Z

    move-result v0

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->createLfg(Z)V

    .line 142
    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 146
    const-string v0, "LFG - Close"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->onTapCloseButton()V

    .line 148
    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    add-int/lit8 v1, p3, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setNeedCount(I)V

    return-void
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setDateOffset(I)V

    return-void
.end method

.method static synthetic lambda$onStart$7(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setTimeOffset(I)V

    return-void
.end method

.method static synthetic lambda$onStart$8(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setLanguageOffset(I)V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 3

    .prologue
    .line 153
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStart()V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->needCountSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->dateSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->timeSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->languageSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 158
    return-void
.end method

.method protected updateViewOverride()V
    .locals 7

    .prologue
    const v6, 0x7f020122

    .line 162
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isBlockingBusy()Z

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07071a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 164
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getSelectedTitle()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v1

    .line 166
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v1, :cond_2

    .line 167
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 169
    iget-object v3, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 170
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v4, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-virtual {v3, v4, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 180
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 181
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070e9a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0706ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 189
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v0

    .line 190
    .local v0, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v2

    .line 191
    .local v2, "viewModelSelectedTagsHashCode":I
    iget v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTagsHashCode:I

    if-eq v2, v3, :cond_1

    .line 192
    iput v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTagsHashCode:I

    .line 193
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 194
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 195
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 197
    :cond_1
    return-void

    .line 176
    .end local v0    # "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    .end local v2    # "viewModelSelectedTagsHashCode":I
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 177
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->selectedTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    goto :goto_0

    .line 184
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070f26

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->addEditDescriptionLabel:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0706d1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
