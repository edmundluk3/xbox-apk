.class public final enum Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
.super Ljava/lang/Enum;
.source "ChatHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChatMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum BasicText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum DirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field private static final INT_TO_TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum IsTyping:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum JoinChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum KickUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum LeaveChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum MessageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum ModifyUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum RichText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum TicketRefresh:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum TicketRequest:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum Undefined:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field public static final enum UserInfo:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 24
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "Undefined"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->Undefined:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 25
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "JoinChannel"

    const v4, 0x4a4f494e    # 3396179.5f

    invoke-direct {v2, v3, v6, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->JoinChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "LeaveChannel"

    const v4, 0x4c454156    # 5.1709272E7f

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->LeaveChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 27
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "TicketRequest"

    const v4, 0x5449434b

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->TicketRequest:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "TicketRefresh"

    const v4, 0x54524546

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->TicketRefresh:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "BasicText"

    const/4 v4, 0x5

    const v5, 0x54455854

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->BasicText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "RichText"

    const/4 v4, 0x6

    const v5, 0x52494348

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->RichText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "KickUser"

    const/4 v4, 0x7

    const v5, 0x4b49434b    # 1.3189963E7f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->KickUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "ModifyUser"

    const/16 v4, 0x8

    const v5, 0x4d4f4455    # 2.1733512E8f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ModifyUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 33
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "DirectMention"

    const/16 v4, 0x9

    const v5, 0x4d454e54    # 2.06890304E8f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->DirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "IsTyping"

    const/16 v4, 0xa

    const v5, 0x54595045

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->IsTyping:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 35
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "MessageOfTheDay"

    const/16 v4, 0xb

    const v5, 0x4d4f5444    # 2.17400384E8f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->MessageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-string v3, "UserInfo"

    const/16 v4, 0xc

    const v5, 0x554e464f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->UserInfo:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 23
    const/16 v2, 0xd

    new-array v2, v2, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->Undefined:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->JoinChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->LeaveChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->TicketRequest:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->TicketRefresh:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->BasicText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->RichText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->KickUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ModifyUser:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->DirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->IsTyping:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->MessageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->UserInfo:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 44
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->INT_TO_TYPE_MAP:Ljava/util/Map;

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 48
    sget-object v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->INT_TO_TYPE_MAP:Ljava/util/Map;

    iget v5, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->value:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->value:I

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .prologue
    .line 23
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->value:I

    return v0
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 53
    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->INT_TO_TYPE_MAP:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 54
    .local v0, "messageType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    if-nez v0, :cond_0

    .line 55
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->Undefined:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 57
    .end local v0    # "messageType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    return-object v0
.end method
