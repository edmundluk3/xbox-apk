.class abstract Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;
.source "$AutoValue_PartyDataChannelTypes_ShoulderTapMessage.java"


# instance fields
.field private final changeNumber:I

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "changeNumber"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->type:Ljava/lang/String;

    .line 20
    iput p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->changeNumber:I

    .line 21
    return-void
.end method


# virtual methods
.method public changeNumber()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->changeNumber:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p1, p0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;

    .line 49
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->changeNumber:I

    .line 50
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;->changeNumber()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;
    :cond_3
    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 57
    const/4 v0, 0x1

    .line 58
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 60
    mul-int/2addr v0, v2

    .line 61
    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->changeNumber:I

    xor-int/2addr v0, v1

    .line 62
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShoulderTapMessage{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", changeNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->changeNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_ShoulderTapMessage;->type:Ljava/lang/String;

    return-object v0
.end method
