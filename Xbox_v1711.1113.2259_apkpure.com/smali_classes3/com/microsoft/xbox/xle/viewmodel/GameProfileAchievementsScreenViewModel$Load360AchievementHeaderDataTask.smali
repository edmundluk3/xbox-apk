.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Load360AchievementHeaderDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

.field private titleId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "xuid"    # Ljava/lang/String;

    .prologue
    .line 891
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 892
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->titleId:Ljava/lang/String;

    .line 893
    invoke-static {p3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 894
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 898
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->titleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshXbox360TitleSummary(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 924
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_1

    .line 926
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->titleId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadXbox360TitleSummary(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 928
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 929
    const-string v1, "LoadAchievementHeaderDataTask"

    const-string v2, "Unable to get 360 title details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 919
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 903
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 904
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 914
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 915
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 886
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 908
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    .line 909
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementHeaderDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 910
    return-void
.end method
