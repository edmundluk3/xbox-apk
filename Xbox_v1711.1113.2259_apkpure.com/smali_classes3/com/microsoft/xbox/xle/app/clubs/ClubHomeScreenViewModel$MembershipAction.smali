.class final enum Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
.super Ljava/lang/Enum;
.source "ClubHomeScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MembershipAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field public static final enum CancelRequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field public static final enum InvitationPending:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field public static final enum None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field public static final enum RemoveSelfFromClub:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field public static final enum RequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 127
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    const-string v1, "InvitationPending"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->InvitationPending:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 128
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    const-string v1, "RequestToJoin"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 129
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    const-string v1, "CancelRequestToJoin"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->CancelRequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 130
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    const-string v1, "RemoveSelfFromClub"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RemoveSelfFromClub:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->InvitationPending:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->CancelRequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RemoveSelfFromClub:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 125
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->fromClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    move-result-object v0

    return-object v0
.end method

.method private static fromClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 133
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 136
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 139
    .local v0, "roles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 150
    :goto_0
    return-object v1

    .line 141
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 142
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RemoveSelfFromClub:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_0

    .line 143
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->RequestedToJoin:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 144
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->CancelRequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_0

    .line 145
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Invited:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 146
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->InvitationPending:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_0

    .line 147
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 148
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->RequestToJoin:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_0

    .line 150
    :cond_6
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    return-object v0
.end method
