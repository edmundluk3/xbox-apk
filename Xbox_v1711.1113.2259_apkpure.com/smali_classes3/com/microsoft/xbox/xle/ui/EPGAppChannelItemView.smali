.class public Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
.super Landroid/view/ViewGroup;
.source "EPGAppChannelItemView.java"


# static fields
.field private static mCache:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAspectRatio:F

.field private mBackdropView:Landroid/view/View;

.field private mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

.field private mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

.field private mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mCache:Ljava/util/Stack;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 179
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 180
    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    .locals 4
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 185
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mCache:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 188
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v2, 0x7f0300eb

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .line 195
    .end local v0    # "inflator":Landroid/view/LayoutInflater;
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :goto_0
    return-object v1

    .line 190
    .end local v1    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mCache:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .line 191
    .restart local v1    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setSelected(Z)V

    .line 192
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setPressed(Z)V

    goto :goto_0
.end method

.method private static recycle(Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .prologue
    .line 199
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mCache:Ljava/util/Stack;

    invoke-virtual {v0, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    return-void
.end method

.method private updateImageView(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getImageURL()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "imageURL":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz p1, :cond_2

    sget-wide v2, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->FADE_IN_DURATION:J

    :goto_1
    invoke-virtual {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->setImageURI3(Ljava/lang/String;J)V

    .line 164
    .end local v0    # "imageURL":Ljava/lang/String;
    :cond_0
    return-void

    .line 161
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 162
    .restart local v0    # "imageURL":Ljava/lang/String;
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getData()Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    return-object v0
.end method

.method public loadImageIfNeeded(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 154
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->updateImageView(Z)V

    .line 156
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 51
    const v0, 0x7f0e00df

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    .line 52
    const v0, 0x7f0e00e3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e056f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mBackdropView:Landroid/view/View;

    .line 54
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingTop()I

    move-result v3

    .line 97
    .local v3, "top":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingLeft()I

    move-result v1

    .line 98
    .local v1, "left":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingRight()I

    move-result v5

    sub-int v2, v4, v5

    .line 99
    .local v2, "right":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingBottom()I

    move-result v5

    sub-int v0, v4, v5

    .line 101
    .local v0, "bottom":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v4, :cond_0

    .line 102
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v0, v4

    .line 103
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v4, v1, v0, v2, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->layout(IIII)V

    .line 106
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mBackdropView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 107
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mBackdropView:Landroid/view/View;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 110
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v4, v1, v3, v2, v0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->layout(IIII)V

    .line 113
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 60
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 61
    .local v0, "height":I
    if-nez v0, :cond_0

    .line 62
    const/16 v0, 0xc8

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v2, :cond_1

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0, v2, p1, p2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->measureChild(Landroid/view/View;II)V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    sub-int v2, v0, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mAspectRatio:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 74
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mBackdropView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 78
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mBackdropView:Landroid/view/View;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->measureChild(Landroid/view/View;II)V

    .line 80
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v2, :cond_3

    .line 81
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->measureChild(Landroid/view/View;II)V

    .line 84
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v2, :cond_4

    .line 86
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0, v2, v3, p2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->measureChild(Landroid/view/View;II)V

    .line 87
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_4
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setMeasuredDimension(II)V

    .line 91
    return-void
.end method

.method public recycle()V
    .locals 0

    .prologue
    .line 170
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->recycle(Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;)V

    .line 171
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 1
    .param p1, "ar"    # F

    .prologue
    .line 117
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mAspectRatio:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 120
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mAspectRatio:F

    goto :goto_0
.end method

.method public setData(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;ZZ)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    .param p2, "loadImage"    # Z
    .param p3, "animate"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-ne v0, p1, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_2

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    if-eqz v0, :cond_3

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGImageView;->clear()V

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->mData:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_0

    .line 144
    if-eqz p2, :cond_0

    .line 145
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->updateImageView(Z)V

    goto :goto_0

    .line 135
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
