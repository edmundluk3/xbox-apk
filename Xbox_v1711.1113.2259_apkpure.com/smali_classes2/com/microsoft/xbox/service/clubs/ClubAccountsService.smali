.class public final enum Lcom/microsoft/xbox/service/clubs/ClubAccountsService;
.super Ljava/lang/Enum;
.source "ClubAccountsService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubAccountsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsService;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubAccountsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

.field private static final BASE_URL:Ljava/lang/String; = "https://clubaccounts.xboxlive.com"

.field private static final CONTRACT_VERSION:I = 0x1

.field private static final CREATE_CLUB_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/create"

.field private static final DEFAULT_ACCEPT_LANG:Ljava/lang/String; = "en-US"

.field private static final DELETE_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

.field private static final GET_DETAILS_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

.field private static final KEEP_CLUB_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/clubid(%d)/suspension/%s"

.field private static final OWNED_CLUBS_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/users/xuid(%d)/clubsowned"

.field private static final RESERVE_NAME_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/reserve"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final TRANSFER_ENDPOINT:Ljava/lang/String; = "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    .line 37
    new-array v0, v3, [Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    .line 41
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    .line 59
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 62
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private getHeadersWithAcceptLang()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 189
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "en-US"

    .line 192
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept-Language"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    return-object v0
.end method

.method static synthetic lambda$changeName$6(JLjava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 137
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ChangeNameRequest;

    invoke-direct {v2, p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ChangeNameRequest;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 136
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createClub$3(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method

.method static synthetic lambda$createClub$4(Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "headers"    # Ljava/util/List;
    .param p1, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    const-string v0, "https://clubaccounts.xboxlive.com/clubs/create"

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$deleteClub$7(J)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 152
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    .line 151
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$deleteClub$8(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method

.method static synthetic lambda$getClubDetails$9(J)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 166
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 167
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    .line 166
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getClubsByOwner$0(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$keepClubForActor$10(JLcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "actor"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 180
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubaccounts.xboxlive.com/clubs/clubid(%d)/suspension/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 181
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    .line 180
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getDeleteStreamWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$reserveClubName$1(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 2
    .param p0, "stream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    return-object v0
.end method

.method static synthetic lambda$reserveClubName$2(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    const-string v0, "https://clubaccounts.xboxlive.com/clubs/reserve"

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatusNoStatusException(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$transferOwnership$5(JLjava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 6
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://clubaccounts.xboxlive.com/clubs/clubid(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 120
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$TransferOwnershipRequest;

    invoke-direct {v2, p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$TransferOwnershipRequest;-><init>(Ljava/lang/String;)V

    .line 122
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubAccountsService;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubAccountsService;

    return-object v0
.end method


# virtual methods
.method public changeName(JLjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeName(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 131
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 132
    const-wide/16 v0, 0x4

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 135
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$7;->lambdaFactory$(JLjava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method

.method public createClub(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .param p2, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createClub("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v1

    .line 100
    .local v1, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;>;"
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;->with(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$CreateClubRequest;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "body":Ljava/lang/String;
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->getHeadersWithAcceptLang()Ljava/util/List;

    move-result-object v2

    .line 105
    .local v2, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$5;->lambdaFactory$(Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v3

    const/4 v4, 0x5

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v3

    :array_0
    .array-data 4
        0xc8
        0xc9
        0x190
        0x193
        0x199
    .end array-data
.end method

.method public deleteClub(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 146
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteClub(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 150
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$8;->lambdaFactory$(J)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$9;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0

    nop

    :array_0
    .array-data 4
        0xc8
        0xca
        0xcc
        0x194
        0x19a
    .end array-data
.end method

.method public getClubDetails(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 161
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getClubDetails(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 165
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$10;->lambdaFactory$(J)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method

.method public getClubsByOwner(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 7
    .param p1, "userId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getClubsByOwner("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 69
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 70
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://clubaccounts.xboxlive.com/users/xuid(%d)/clubsowned"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$1;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    return-object v1
.end method

.method public keepClubForActor(JLcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;)Z
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "actor"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 174
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keepClubForActor(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " actor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 176
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 179
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$11;->lambdaFactory$(JLcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->request(Ljava/util/concurrent/Callable;[I)Z

    move-result v0

    return v0

    :array_0
    .array-data 4
        0xcc
        0x19a
    .end array-data
.end method

.method public reserveClubName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reserveClubName("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 83
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ReserveClubNameRequest;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ReserveClubNameRequest;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "body":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;

    move-result-object v1

    .line 87
    .local v1, "deserializeFromStream":Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;, "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v2, v1, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequest(Ljava/util/concurrent/Callable;Lcom/microsoft/xbox/service/network/managers/ServiceCommon$DeserializeFromStream;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    return-object v2

    :array_0
    .array-data 4
        0xc8
        0xc9
        0x190
        0x199
    .end array-data
.end method

.method public transferOwnership(JLjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "transferOwnership(clubId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " xuid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 115
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 118
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/ClubAccountsService$$Lambda$6;->lambdaFactory$(JLjava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    return-object v0
.end method
