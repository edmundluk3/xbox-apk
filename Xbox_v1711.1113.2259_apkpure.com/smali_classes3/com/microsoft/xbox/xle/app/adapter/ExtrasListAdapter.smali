.class public Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ExtrasListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p4, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 25
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->notifyDataSetChanged()V

    .line 27
    return-void
.end method

.method private bindView(Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v4, 0x0

    .line 42
    const v2, 0x7f0e0791

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 43
    .local v0, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v2, 0x7f0e0792

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 45
    .local v1, "label":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-nez p2, :cond_0

    .line 46
    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->getDefaultImage()I

    move-result v3

    invoke-virtual {v0, v4, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 54
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getIcon2x1Url()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->getDefaultImage()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private getDefaultImage()I
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f0201fa

    return v0
.end method

.method static synthetic lambda$bindView$0(Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->gotoDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 31
    if-nez p2, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030167

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 36
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {p0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 38
    return-object p2
.end method
