.class public abstract Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "EDSV2MediaItemViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;"
    }
.end annotation


# instance fields
.field protected detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel",
            "<TT;>.",
            "LoadDetailsTask;"
        }
    .end annotation
.end field

.field protected genreListString:Ljava/lang/String;

.field protected isLoadingDetail:Z

.field protected mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    const/4 v2, 0x1

    .line 41
    invoke-direct {p0, v2, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(ZZ)V

    .line 32
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 36
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->genreListString:Ljava/lang/String;

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 44
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 45
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 46
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 53
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 54
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    .line 55
    return-void
.end method


# virtual methods
.method public getAvailableOnPlatforms()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAvailableOnPlatforms()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBlockingStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    const-string v0, ""

    return-object v0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.end method

.method protected getDefaultUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1

    .prologue
    .line 234
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method

.method protected abstract getErrorStringResourceId()I
.end method

.method public getGenres()Ljava/lang/String;
    .locals 6

    .prologue
    .line 90
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->genreListString:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 91
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->genreListString:Ljava/lang/String;

    .line 110
    :goto_0
    return-object v3

    .line 94
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getGenres()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_1

    .line 95
    const/4 v3, 0x0

    goto :goto_0

    .line 98
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getGenres()Ljava/util/List;

    move-result-object v1

    .line 100
    .local v1, "genres":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;

    .line 101
    .local v0, "genre":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 102
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07108f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_2
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;->Name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 108
    .end local v0    # "genre":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->genreListString:Ljava/lang/String;

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->genreListString:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getModelErrorCode()J
    .locals 2

    .prologue
    .line 230
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    const-wide/16 v0, 0xfae

    return-wide v0
.end method

.method public getParentMediaItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlatformType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 204
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getPlatformType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 63
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->isLoadingDetail:Z

    return v0
.end method

.method protected isScreenDataEmpty()Z
    .locals 1

    .prologue
    .line 124
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXPA()Z
    .locals 1

    .prologue
    .line 199
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isXPA()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 118
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->load(Z)V

    .line 119
    return-void
.end method

.method protected loadDetailsInBackGround(Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1
    .param p1, "forceLoad"    # Z

    .prologue
    .line 240
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 159
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->isLoadingDetail:Z

    .line 161
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 175
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    .line 176
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateAdapter()V

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_2

    .line 190
    :cond_2
    return-void

    .line 165
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateViewModelState()V

    goto :goto_0

    .line 169
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 170
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 130
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 131
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 137
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->detailTask:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->cancel()V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 139
    return-void
.end method

.method public onUpdateFinished()V
    .locals 4

    .prologue
    .line 218
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getDefaultUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getModelErrorCode()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->getErrorStringResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->showError(I)V

    .line 226
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onUpdateFinished()V

    .line 227
    return-void

    .line 222
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 223
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public setPlatformType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)V
    .locals 1
    .param p1, "platformType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 193
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->setPlatformType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)V

    .line 196
    return-void
.end method

.method protected shouldRefresh()Z
    .locals 1

    .prologue
    .line 246
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 145
    .local v0, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateViewModelState()V

    .line 147
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateAdapter()V

    .line 149
    :cond_0
    return-void
.end method

.method public updateViewModelState()V
    .locals 1

    .prologue
    .line 213
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->isScreenDataEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 214
    return-void

    .line 213
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method
