.class abstract Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;
.super Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
.source "$AutoValue_ActivityFeedFilterPrefs.java"


# instance fields
.field private final showClubs:Z

.field private final showFavorites:Z

.field private final showFriends:Z

.field private final showGames:Z

.field private final showPopular:Z


# direct methods
.method constructor <init>(ZZZZZ)V
    .locals 0
    .param p1, "showFriends"    # Z
    .param p2, "showFavorites"    # Z
    .param p3, "showClubs"    # Z
    .param p4, "showGames"    # Z
    .param p5, "showPopular"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;-><init>()V

    .line 21
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFriends:Z

    .line 22
    iput-boolean p2, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFavorites:Z

    .line 23
    iput-boolean p3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showClubs:Z

    .line 24
    iput-boolean p4, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showGames:Z

    .line 25
    iput-boolean p5, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showPopular:Z

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 70
    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 71
    .local v0, "that":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFriends:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFavorites:Z

    .line 72
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showClubs:Z

    .line 73
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showGames:Z

    .line 74
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showPopular:Z

    .line 75
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    :cond_3
    move v1, v2

    .line 77
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const v4, 0xf4243

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 82
    const/4 v0, 0x1

    .line 83
    .local v0, "h":I
    mul-int/2addr v0, v4

    .line 84
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFriends:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 85
    mul-int/2addr v0, v4

    .line 86
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFavorites:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 87
    mul-int/2addr v0, v4

    .line 88
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showClubs:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 89
    mul-int/2addr v0, v4

    .line 90
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showGames:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 91
    mul-int/2addr v0, v4

    .line 92
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showPopular:Z

    if-eqz v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 93
    return v0

    :cond_0
    move v1, v3

    .line 84
    goto :goto_0

    :cond_1
    move v1, v3

    .line 86
    goto :goto_1

    :cond_2
    move v1, v3

    .line 88
    goto :goto_2

    :cond_3
    move v1, v3

    .line 90
    goto :goto_3

    :cond_4
    move v2, v3

    .line 92
    goto :goto_4
.end method

.method public showClubs()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showClubs:Z

    return v0
.end method

.method public showFavorites()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFavorites:Z

    return v0
.end method

.method public showFriends()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFriends:Z

    return v0
.end method

.method public showGames()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showGames:Z

    return v0
.end method

.method public showPopular()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showPopular:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActivityFeedFilterPrefs{showFriends="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFriends:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showFavorites="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showFavorites:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showClubs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showClubs:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showGames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showGames:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showPopular="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/$AutoValue_ActivityFeedFilterPrefs;->showPopular:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
