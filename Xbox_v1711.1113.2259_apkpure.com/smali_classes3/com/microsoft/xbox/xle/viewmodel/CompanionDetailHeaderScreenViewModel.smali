.class public Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
.source "CompanionDetailHeaderScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;-><init>()V

    .line 10
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 11
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getActivityParentMediaItemData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 13
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 15
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCompanionDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 16
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCompanionDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 21
    return-void
.end method
