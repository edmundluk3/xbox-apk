.class Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;
.super Landroid/webkit/WebViewClient;
.source "PurchaseWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PurchaseWebViewActivityClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

.field private webView:Landroid/webkit/WebView;

.field private wvPageLoadtime:J


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/webkit/WebView;)V
    .locals 0
    .param p2, "view"    # Landroid/webkit/WebView;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 319
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    .line 320
    return-void
.end method


# virtual methods
.method public NavigateBack()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 329
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->goBack()V

    goto :goto_0
.end method

.method public Start()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 332
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->wvPageLoadtime:J

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, p0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/content/Context;)V

    const-string v2, "SmartGlass"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    const-string v1, "javascript:window:addEventListener(\'message\', function(e) {if (/\\.microsoft\\.com$/i.test(e.origin) && typeof e.data === \'string\') {SmartGlass.onCaptureEvent(e.data);}});"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->getDataSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 11
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 362
    const-string v4, "WebViewClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Loaded page: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v4, "WebViewClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "At Time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->wvPageLoadtime:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v4, v10}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Z)Z

    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 366
    .local v2, "totalLoadingTime":J
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 367
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-string v4, "Purchase - WebBlend Loaded"

    long-to-int v5, v2

    div-int/lit16 v5, v5, 0x3e8

    int-to-long v6, v5

    invoke-static {v4, v6, v7, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackWebBlend(Ljava/lang/String;JLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 369
    const-string v4, "javascript:SetFormAction(\'https://www.microsoft.com/uniblends?client=Universal%20Xbox%20App\');"

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 370
    const-string v4, "Culture"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 371
    const-string v4, "Market"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 372
    const-string v4, "PurchaseServiceVersion"

    const-string v5, "7"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 373
    const-string v4, "Layout"

    const-string v5, "modal"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 375
    :try_start_0
    const-string v4, "CV"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$800(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    .line 380
    const-string v4, "ProductId"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 381
    const-string v4, "AvailabilityId"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 382
    const-string v4, "SkuId"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 389
    :goto_1
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->webView:Landroid/webkit/WebView;

    const-string v7, "https://purchase.mp.microsoft.com"

    invoke-direct {v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/webkit/WebView;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->load(Z)V

    .line 390
    return-void

    .line 376
    :catch_0
    move-exception v0

    .line 377
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$700()Ljava/lang/String;

    move-result-object v4

    const-string v5, "failed to obtain the correlation vector"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 384
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 385
    const-string v4, "tokenString"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 387
    :cond_1
    const-string v4, "selApps"

    const-string v5, "redeem.enterCode"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$600(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v6, 0x0

    .line 354
    const-string v0, "PurchaseWebViewClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting to load page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v0, "PurchaseWebViewClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "At Time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->wvPageLoadtime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Z)Z

    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Z)Z

    .line 358
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 394
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$PurchaseWebViewActivityClient;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 395
    const-string v1, "WebViewClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 399
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {p2, p3, p4, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackWebBlendLoadError(ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 401
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 402
    return-void
.end method
