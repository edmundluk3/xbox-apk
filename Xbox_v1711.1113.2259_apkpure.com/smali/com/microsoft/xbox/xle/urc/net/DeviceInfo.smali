.class public Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field public buttons:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public device_brand:Ljava/lang/String;

.field public device_id:Ljava/lang/String;

.field public device_model:Ljava/lang/String;

.field public device_name:Ljava/lang/String;

.field public device_type:Ljava/lang/String;

.field public remote_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJSON(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .locals 9
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    const/4 v8, 0x0

    .line 20
    new-instance v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    invoke-direct {v2}, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;-><init>()V

    .line 22
    .local v2, "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    const-string v7, "device_id"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_id:Ljava/lang/String;

    .line 23
    const-string v7, "device_name"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_name:Ljava/lang/String;

    .line 24
    const-string v7, "device_type"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    .line 25
    const-string v7, "device_model"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_model:Ljava/lang/String;

    .line 26
    const-string v7, "device_brand"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_brand:Ljava/lang/String;

    .line 27
    const-string v7, "remote_url"

    invoke-virtual {p0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->remote_url:Ljava/lang/String;

    .line 28
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    .line 30
    const-string v7, "buttons"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 31
    .local v4, "jButtons":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 32
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 33
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "btnName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 35
    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    invoke-interface {v7, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "btnName":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    const-string v7, "buttons"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 41
    .local v6, "oButtons":Lorg/json/JSONObject;
    if-eqz v6, :cond_4

    .line 42
    invoke-virtual {v6}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v5

    .line 44
    .local v5, "jNames":Lorg/json/JSONArray;
    if-eqz v5, :cond_4

    .line 45
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 46
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    .restart local v0    # "btnName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 49
    .local v1, "btnValue":Ljava/lang/String;
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 50
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    :cond_2
    if-eqz v0, :cond_3

    .line 53
    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 61
    .end local v0    # "btnName":Ljava/lang/String;
    .end local v1    # "btnValue":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "jNames":Lorg/json/JSONArray;
    .end local v6    # "oButtons":Lorg/json/JSONObject;
    :cond_4
    return-object v2
.end method
