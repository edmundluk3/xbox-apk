.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;
.super Ljava/lang/Object;
.source "UTCDiagnostic.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$additionalInfo:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

.field final synthetic val$name:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$name:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$additionalInfo:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$name:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$additionalInfo:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    new-instance v0, Lxbox/smartglass/ClientDiagnostic;

    invoke-direct {v0}, Lxbox/smartglass/ClientDiagnostic;-><init>()V

    .line 35
    .local v0, "clientDiagnostic":Lxbox/smartglass/ClientDiagnostic;
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lxbox/smartglass/ClientDiagnostic;->setName(Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$additionalInfo:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/ClientDiagnostic;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 40
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 42
    const-string v2, "clientDiagnostic:%s; additionalInfo:%s"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic$1;->val$name:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {v0}, Lxbox/smartglass/ClientDiagnostic;->getBaseData()Lcom/microsoft/bond/BondSerializable;

    move-result-object v1

    check-cast v1, Lxbox/smartglass/CommonData;

    invoke-virtual {v1}, Lxbox/smartglass/CommonData;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    return-void
.end method
