.class public Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
.super Ljava/lang/Object;
.source "FeedItemActionResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;,
        Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;,
        Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;
    }
.end annotation


# instance fields
.field public commentCount:I

.field public comments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation
.end field

.field public continuationToken:Ljava/lang/String;

.field public likeCount:I

.field public likes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation
.end field

.field public shareCount:I

.field public shares:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private reverseList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    move-object v1, p1

    .line 43
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    .restart local v1    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 46
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 49
    .end local v0    # "i":I
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$1;->$SwitchMap$com$microsoft$xbox$service$model$feeditemactions$FeedItemActionType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 30
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 24
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->comments:Ljava/util/ArrayList;

    goto :goto_0

    .line 26
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->likes:Ljava/util/ArrayList;

    goto :goto_0

    .line 28
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->shares:Ljava/util/ArrayList;

    goto :goto_0

    .line 22
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reverseLists()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->shares:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->reverseList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->shares:Ljava/util/ArrayList;

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->likes:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->reverseList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->likes:Ljava/util/ArrayList;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->comments:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->reverseList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->comments:Ljava/util/ArrayList;

    .line 39
    return-void
.end method
