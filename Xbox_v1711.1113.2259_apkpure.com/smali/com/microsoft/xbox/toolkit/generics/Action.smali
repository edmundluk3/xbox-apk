.class public interface abstract Lcom/microsoft/xbox/toolkit/generics/Action;
.super Ljava/lang/Object;
.source "Action.java"


# annotations
.annotation runtime Lcom/microsoft/xbox/toolkit/java8/FunctionalInterface;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract run(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
