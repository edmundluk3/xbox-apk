.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LfgNotification"
.end annotation


# static fields
.field public static final transient IMMINENT_EXPIRED_TYPE:Ljava/lang/String; = "imminentLfgExpired"

.field public static final transient SCHEDULED_EXPIRED_TYPE:Ljava/lang/String; = "scheduledLfgExpired"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1461
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_LfgNotification$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public getSearchHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1457
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;->lfg_notification()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;->searchHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    return-object v0
.end method

.method public abstract lfg_notification()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
