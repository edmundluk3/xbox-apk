.class public abstract Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.super Ljava/lang/Object;
.source "SessionManagerListener.java"

# interfaces
.implements Ljava/util/EventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 41
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 25
    return-void
.end method

.method public onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 0
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 62
    return-void
.end method

.method public onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 0
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 55
    return-void
.end method

.method public onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 33
    return-void
.end method

.method public onPrimaryDeviceStateChanged(Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    .prologue
    .line 17
    return-void
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 0
    .param p1, "activeTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 48
    return-void
.end method
