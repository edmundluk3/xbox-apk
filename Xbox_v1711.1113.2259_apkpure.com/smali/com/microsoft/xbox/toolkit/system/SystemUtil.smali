.class public Lcom/microsoft/xbox/toolkit/system/SystemUtil;
.super Ljava/lang/Object;
.source "SystemUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;
    }
.end annotation


# static fields
.field private static final AMAZON_STORE_PACKAGE:Ljava/lang/String; = "com.amazon.mShop.android"

.field private static final AMAZON_STORE_URI:Ljava/lang/String; = "amzn://apps/android?p="

.field private static final AMAZON_STORE_WEB_URI:Ljava/lang/String; = "http://www.amazon.com/gp/mas/dl/android?p="

.field private static final GEARVR_STORE_WEB_URI:Ljava/lang/String; = "https://www2.oculus.com/experiences/gear-vr/1046887318709554/"

.field private static final MAX_SD_SCREEN_PIXELS:I = 0x5dc00

.field private static final PACKAGEID_MINECRAFTPE:Ljava/lang/String; = "com.mojang.minecraftpe"

.field private static final PLAY_STORE_PACKAGE:Ljava/lang/String; = "com.android.vending"

.field private static final PLAY_STORE_URI:Ljava/lang/String; = "market://details?id="

.field private static final PLAY_STORE_WEB_URI:Ljava/lang/String; = "https://play.google.com/store/apps/details?id="

.field public static final TITLEID_MINECRAFTPE_AMAZON:J = 0x73e3c5efL

.field public static final TITLEID_MINECRAFTPE_GEARVR:J = 0x71c9b1c0L

.field public static final TITLEID_MINECRAFTPE_GOOGLE:J = 0x67b57dacL

.field public static final TITLEID_MINECRAFTPE_IOS:J = 0x6bf082d7L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DIPtoPixels(F)I
    .locals 2
    .param p0, "dip"    # F

    .prologue
    .line 84
    const/4 v0, 0x1

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static SPtoPixels(F)I
    .locals 2
    .param p0, "sp"    # F

    .prologue
    .line 95
    const/4 v0, 0x2

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static TEST_randomFalseOutOf(I)Z
    .locals 1
    .param p0, "max"    # I

    .prologue
    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public static TEST_randomSleep(I)V
    .locals 0
    .param p0, "maxSeconds"    # I

    .prologue
    .line 247
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 260
    return-void
.end method

.method public static getColorDepth()I
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "info":Landroid/graphics/PixelFormat;
    const/4 v1, 0x1

    invoke-static {v1, v0}, Landroid/graphics/PixelFormat;->getPixelFormatInfo(ILandroid/graphics/PixelFormat;)V

    .line 117
    iget v1, v0, Landroid/graphics/PixelFormat;->bitsPerPixel:I

    return v1
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public static getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    const-string v0, "AndroidPhone"

    return-object v0
.end method

.method private static getDisplay()Landroid/view/Display;
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    return-object v0
.end method

.method public static getMACAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "interfaceName"    # Ljava/lang/String;

    .prologue
    .line 214
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v3

    .line 215
    .local v3, "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 217
    .local v4, "intf":Ljava/net/NetworkInterface;
    if-eqz p0, :cond_1

    .line 218
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 223
    :cond_1
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getHardwareAddress()[B

    move-result-object v5

    .line 224
    .local v5, "mac":[B
    if-nez v5, :cond_2

    .line 225
    const-string v6, ""

    .line 243
    .end local v3    # "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    .end local v5    # "mac":[B
    :goto_0
    return-object v6

    .line 228
    .restart local v3    # "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    .restart local v4    # "intf":Ljava/net/NetworkInterface;
    .restart local v5    # "mac":[B
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "idx":I
    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_3

    .line 231
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%02X:"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aget-byte v10, v5, v2

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 234
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 238
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 240
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .end local v2    # "idx":I
    .end local v3    # "interfaces":Ljava/util/List;, "Ljava/util/List<Ljava/net/NetworkInterface;>;"
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    .end local v5    # "mac":[B
    :catch_0
    move-exception v1

    .line 241
    .local v1, "ex":Ljava/lang/Exception;
    const-string v6, "SystemUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get MAC address with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_5
    const-string v6, ""

    goto :goto_0
.end method

.method private static getMarketplaceLaunchIntentForPackage(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;)Landroid/content/Intent;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageId"    # Ljava/lang/String;
    .param p2, "marketPlace"    # Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    .prologue
    .line 297
    const-string v6, ""

    .line 298
    .local v6, "storeUri":Ljava/lang/String;
    const-string v3, ""

    .line 299
    .local v3, "expectedStorePackage":Ljava/lang/String;
    const/4 v7, 0x0

    .line 301
    .local v7, "storeWebpage":Landroid/net/Uri;
    sget-object v8, Lcom/microsoft/xbox/toolkit/system/SystemUtil$1;->$SwitchMap$com$microsoft$xbox$toolkit$system$SystemUtil$MarketplaceApp:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 318
    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 320
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 321
    .local v5, "storeIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 323
    .local v1, "appsAbleToHandleIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 324
    .local v0, "app":Landroid/content/pm/ResolveInfo;
    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 325
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 327
    .local v4, "otherAppActivity":Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v8, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v9, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .local v2, "componentName":Landroid/content/ComponentName;
    const/high16 v8, 0x10200000

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 333
    invoke-virtual {v5, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 341
    .end local v0    # "app":Landroid/content/pm/ResolveInfo;
    .end local v1    # "appsAbleToHandleIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v4    # "otherAppActivity":Landroid/content/pm/ActivityInfo;
    .end local v5    # "storeIntent":Landroid/content/Intent;
    :goto_1
    return-object v5

    .line 303
    :pswitch_0
    const-string v6, "amzn://apps/android?p="

    .line 304
    const-string v3, "com.amazon.mShop.android"

    .line 305
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "http://www.amazon.com/gp/mas/dl/android?p="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 306
    goto :goto_0

    .line 309
    :pswitch_1
    const-string v6, "market://details?id="

    .line 310
    const-string v3, "com.android.vending"

    .line 311
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 312
    goto/16 :goto_0

    .line 315
    :pswitch_2
    const-string v8, "https://www2.oculus.com/experiences/gear-vr/1046887318709554/"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    goto/16 :goto_0

    .line 341
    :cond_1
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v5, v8, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    .line 301
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getMarketplaceLaunchIntentForTitleId(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleId"    # J

    .prologue
    .line 283
    const/4 v0, 0x0

    .line 285
    .local v0, "externalStoreIntent":Landroid/content/Intent;
    const-wide/32 v2, 0x67b57dac

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 286
    const-string v1, "com.mojang.minecraftpe"

    sget-object v2, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Google:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    invoke-static {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getMarketplaceLaunchIntentForPackage(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;)Landroid/content/Intent;

    move-result-object v0

    .line 293
    :cond_0
    :goto_0
    return-object v0

    .line 287
    :cond_1
    const-wide/32 v2, 0x71c9b1c0

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 288
    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->GearVR:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    invoke-static {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getMarketplaceLaunchIntentForPackage(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 289
    :cond_2
    const-wide/32 v2, 0x73e3c5ef

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 290
    const-string v1, "com.mojang.minecraftpe"

    sget-object v2, Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;->Amazon:Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;

    invoke-static {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getMarketplaceLaunchIntentForPackage(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/system/SystemUtil$MarketplaceApp;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static getOrientation()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getRotation()I

    move-result v0

    .line 143
    .local v0, "rotation":I
    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    .line 144
    :cond_0
    const-string v1, "SystemUrl"

    const-string v2, "Orientation is portait"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v1, 0x1

    .line 148
    :goto_0
    return v1

    .line 147
    :cond_1
    const-string v2, "SystemUrl"

    const-string v3, "Orientation is landscape"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getRotation()I
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public static getScreenHeight()I
    .locals 2

    .prologue
    .line 110
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 111
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v1
.end method

.method public static getScreenHeightInches()F
    .locals 3

    .prologue
    .line 126
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 127
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v1, v2

    return v1
.end method

.method public static getScreenWidth()I
    .locals 2

    .prologue
    .line 102
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 103
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v1
.end method

.method public static getScreenWidthHeightAspectRatio()F
    .locals 4

    .prologue
    .line 182
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v1

    .line 183
    .local v1, "screenWidth":I
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenHeight()I

    move-result v0

    .line 184
    .local v0, "screenHeight":I
    if-lez v1, :cond_0

    if-gtz v0, :cond_1

    .line 185
    :cond_0
    const/4 v2, 0x0

    .line 191
    :goto_0
    return v2

    .line 188
    :cond_1
    if-le v1, v0, :cond_2

    .line 189
    int-to-float v2, v1

    int-to-float v3, v0

    div-float/2addr v2, v3

    goto :goto_0

    .line 191
    :cond_2
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method public static getScreenWidthInches()F
    .locals 3

    .prologue
    .line 121
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 122
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v1, v2

    return v1
.end method

.method public static getSdkInt()I
    .locals 1

    .prologue
    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public static getYDPI()F
    .locals 2

    .prologue
    .line 131
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 132
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->ydpi:F

    return v1
.end method

.method public static isAmazonDevice()Z
    .locals 2

    .prologue
    .line 278
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 279
    .local v0, "manufecturer":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "AMAZON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isHDScreen()Z
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenHeight()I

    move-result v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v1

    mul-int/2addr v0, v1

    const v1, 0x5dc00

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSDCardAvailable()Z
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isSlate()Z
    .locals 2

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 162
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    return v1
.end method
