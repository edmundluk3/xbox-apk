.class public final Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;
.super Ljava/lang/Object;
.source "QoSMeasurementRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final gameServerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;",
            ">;"
        }
    .end annotation
.end field

.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "gameServerServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;>;"
    .local p2, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->gameServerServiceProvider:Ljavax/inject/Provider;

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 24
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "gameServerServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;>;"
    .local p1, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newQoSMeasurementRepository(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;)Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;
    .locals 1
    .param p0, "gameServerService"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;
    .param p1, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .prologue
    .line 41
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;-><init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;
    .locals 3

    .prologue
    .line 28
    new-instance v2, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->gameServerServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    invoke-direct {v2, v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;-><init>(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;)V

    return-object v2
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->get()Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;

    move-result-object v0

    return-object v0
.end method
