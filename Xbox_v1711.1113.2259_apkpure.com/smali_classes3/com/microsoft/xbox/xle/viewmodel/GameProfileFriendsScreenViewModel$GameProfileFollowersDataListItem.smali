.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;
.super Ljava/lang/Object;
.source "GameProfileFriendsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GameProfileFollowersDataListItem"
.end annotation


# instance fields
.field public final followersData:Lcom/microsoft/xbox/service/model/FollowersData;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "followersData"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 194
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 195
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 199
    if-ne p1, p0, :cond_0

    .line 200
    const/4 v1, 0x1

    .line 205
    :goto_0
    return v1

    .line 201
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    if-nez v1, :cond_1

    .line 202
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 204
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    .line 205
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 211
    const/16 v0, 0x11

    .line 212
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 213
    return v0
.end method
