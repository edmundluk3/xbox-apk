.class final Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapterUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->attachSocialBarListeners(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->displayActionList(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V

    .line 194
    return-void
.end method
