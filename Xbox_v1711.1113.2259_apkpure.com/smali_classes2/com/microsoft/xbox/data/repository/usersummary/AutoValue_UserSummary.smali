.class final Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;
.super Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
.source "AutoValue_UserSummary.java"


# instance fields
.field private final colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

.field private final displayPicUri:Ljava/lang/String;

.field private final gamerScore:J

.field private final gamertag:Ljava/lang/String;

.field private final inParty:Z

.field private final presenceText:Ljava/lang/String;

.field private final realName:Ljava/lang/String;

.field private final xuid:J


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 3
    .param p1, "xuid"    # J
    .param p3, "gamertag"    # Ljava/lang/String;
    .param p4, "realName"    # Ljava/lang/String;
    .param p5, "displayPicUri"    # Ljava/lang/String;
    .param p6, "presenceText"    # Ljava/lang/String;
    .param p7, "gamerScore"    # J
    .param p9, "inParty"    # Z
    .param p10, "colors"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;-><init>()V

    .line 30
    iput-wide p1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    .line 31
    if-nez p3, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null gamertag"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamertag:Ljava/lang/String;

    .line 35
    if-nez p4, :cond_1

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null realName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_1
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->realName:Ljava/lang/String;

    .line 39
    if-nez p5, :cond_2

    .line 40
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null displayPicUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_2
    iput-object p5, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->displayPicUri:Ljava/lang/String;

    .line 43
    if-nez p6, :cond_3

    .line 44
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null presenceText"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_3
    iput-object p6, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->presenceText:Ljava/lang/String;

    .line 47
    iput-wide p7, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    .line 48
    iput-boolean p9, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->inParty:Z

    .line 49
    if-nez p10, :cond_4

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null colors"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_4
    iput-object p10, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 53
    return-void
.end method


# virtual methods
.method public colors()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public displayPicUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->displayPicUri:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    if-ne p1, p0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v1

    .line 120
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 121
    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 122
    .local v0, "that":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    iget-wide v4, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamertag:Ljava/lang/String;

    .line 123
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->realName:Ljava/lang/String;

    .line 124
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->displayPicUri:Ljava/lang/String;

    .line 125
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->presenceText:Ljava/lang/String;

    .line 126
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->presenceText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    .line 127
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamerScore()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->inParty:Z

    .line 128
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->inParty()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 129
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->colors()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_3
    move v1, v2

    .line 131
    goto :goto_0
.end method

.method public gamerScore()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    return-wide v0
.end method

.method public gamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/16 v9, 0x20

    const v8, 0xf4243

    .line 136
    const/4 v0, 0x1

    .line 137
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 138
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    ushr-long/2addr v4, v9

    iget-wide v6, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 139
    mul-int/2addr v0, v8

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 141
    mul-int/2addr v0, v8

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->realName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 143
    mul-int/2addr v0, v8

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->displayPicUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 145
    mul-int/2addr v0, v8

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->presenceText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 147
    mul-int/2addr v0, v8

    .line 148
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    ushr-long/2addr v4, v9

    iget-wide v6, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 149
    mul-int/2addr v0, v8

    .line 150
    iget-boolean v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->inParty:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 151
    mul-int/2addr v0, v8

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 153
    return v0

    .line 150
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public inParty()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->inParty:Z

    return v0
.end method

.method public presenceText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->presenceText:Ljava/lang/String;

    return-object v0
.end method

.method public realName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->realName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserSummary{xuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", realName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->realName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayPicUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->displayPicUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presenceText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->presenceText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->gamerScore:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inParty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->inParty:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", colors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public xuid()J
    .locals 2
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x1L
    .end annotation

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;->xuid:J

    return-wide v0
.end method
