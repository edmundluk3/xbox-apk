.class final synthetic Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;->arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;-><init>(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;->arg$1:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    check-cast p1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->lambda$null$2(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;)V

    return-void
.end method
