.class public abstract Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;
.super Ljava/lang/Object;
.source "ActivityFeedSettingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ActivityFeedSetting"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract name()Ljava/lang/String;
.end method

.method public abstract value()Z
.end method
