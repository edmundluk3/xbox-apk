.class Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;
.super Ljava/lang/Object;
.source "ServiceRequestTracker.java"


# instance fields
.field private final _requests:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    .line 14
    return-void
.end method


# virtual methods
.method public add(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 17
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    monitor-enter v1

    .line 18
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 19
    monitor-exit v1

    .line 20
    return-void

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancelAll()V
    .locals 4

    .prologue
    .line 29
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    monitor-enter v2

    .line 30
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .line 31
    .local v0, "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->cancel()V

    goto :goto_0

    .line 36
    .end local v0    # "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 35
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 36
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 37
    return-void
.end method

.method public remove(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 23
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    monitor-enter v1

    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->_requests:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 25
    monitor-exit v1

    .line 26
    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
