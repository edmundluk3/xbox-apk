.class public Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;
.super Landroid/widget/TextView;
.source "CustomStyledTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21
    sget-object v1, Lcom/microsoft/xbox/idp/R$styleable;->CustomStyledTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 22
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;)V

    .line 24
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    sget-object v1, Lcom/microsoft/xbox/idp/R$styleable;->CustomStyledTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 31
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;)V

    .line 33
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method private applyCustomStyleAttributes(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    .line 37
    sget v2, Lcom/microsoft/xbox/idp/R$styleable;->CustomStyledTextView_typefaceSource:I

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "typeface":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 40
    .local v0, "tf":Landroid/graphics/Typeface;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 45
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->setCursorVisible(Z)V

    .line 46
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    return-void
.end method


# virtual methods
.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    sget-object v1, Lcom/microsoft/xbox/idp/R$styleable;->CustomStyledTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    .local v0, "arr":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/toolkit/CustomStyledTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;)V

    .line 57
    .end local v0    # "arr":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method
