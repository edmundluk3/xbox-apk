.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;
.super Ljava/lang/Object;
.source "HomeScreenPopupScreenViewModelContent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->getLaunchPostActionRunnable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    .prologue
    .line 341
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 344
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent$4;"
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    .line 348
    .local v0, "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :goto_0
    return-void

    .line 349
    :catch_0
    move-exception v1

    .line 350
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to navigate to screen \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
