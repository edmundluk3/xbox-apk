.class final Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
.source "AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem.java"


# instance fields
.field private final actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field private final createdDate:Ljava/util/Date;

.field private final requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/Date;)V
    .locals 2
    .param p1, "requesterSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p2, "actorSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p3, "createdDate"    # Ljava/util/Date;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null requesterSummary"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null actorSummary"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 27
    if-nez p3, :cond_2

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null createdDate"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->createdDate:Ljava/util/Date;

    .line 31
    return-void
.end method


# virtual methods
.method protected actorSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method protected createdDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->createdDate:Ljava/util/Date;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 63
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    .line 64
    .local v0, "that":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->requesterSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->actorSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->createdDate:Ljava/util/Date;

    .line 66
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->createdDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    :cond_3
    move v1, v2

    .line 68
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 73
    const/4 v0, 0x1

    .line 74
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 76
    mul-int/2addr v0, v2

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 78
    mul-int/2addr v0, v2

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->createdDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 80
    return v0
.end method

.method protected requesterSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubRequestListItem{requesterSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->requesterSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actorSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->actorSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/AutoValue_ClubAdminRequestsScreenViewModel_ClubRequestListItem;->createdDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
