.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleHubSocialListAdapter$GameViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;

    .line 23
    const v0, 0x7f0e0933

    const-string v1, "field \'image\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->image:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f0e0934

    const-string v1, "field \'name\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->name:Landroid/widget/TextView;

    .line 25
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;

    .line 31
    .local v0, "target":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;

    .line 34
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->image:Landroid/widget/ImageView;

    .line 35
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->name:Landroid/widget/TextView;

    .line 36
    return-void
.end method
