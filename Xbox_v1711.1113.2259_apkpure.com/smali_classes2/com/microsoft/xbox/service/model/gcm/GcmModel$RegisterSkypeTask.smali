.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;
.super Landroid/os/AsyncTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegisterSkypeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

.field private edfNodeId:Ljava/lang/String;

.field private edfRegId:Ljava/lang/String;

.field private final forceSubscriptionRefresh:Z

.field private final isLogin:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZZ)V
    .locals 0
    .param p2, "forceSubscriptionRefresh"    # Z
    .param p3, "isLogin"    # Z

    .prologue
    .line 914
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 915
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->forceSubscriptionRefresh:Z

    .line 916
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->isLogin:Z

    .line 917
    return-void
.end method

.method private registerWithSkypeBackend(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 7
    .param p1, "edfRegId"    # Ljava/lang/String;
    .param p2, "edfNodeId"    # Ljava/lang/String;
    .param p3, "gcmRegId"    # Ljava/lang/String;
    .param p4, "forceSubscriptionRefresh"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 983
    if-eqz p4, :cond_0

    .line 984
    invoke-static {p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;)V

    .line 988
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;

    invoke-direct {v4, p2, p1, p3}, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->getEDFRegistrationBody(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->edfRegistrationForSkypeNotifications(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 999
    :goto_0
    return v1

    .line 993
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;

    invoke-direct {v4, p3}, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->getSkypeEndpointSubscriptionBody(Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->subscribeWithSkypeChatServiceForNotifications(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 994
    .local v0, "status":I
    const/16 v3, 0xc9

    if-eq v0, v3, :cond_2

    .line 995
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%d Error while subscribing with skype for push notification"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-static {v4, v5, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 999
    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 932
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Registering with EDF for skype message push notifications"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfRegId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 935
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfRegId:Ljava/lang/String;

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfNodeId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 939
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfNodeId:Ljava/lang/String;

    .line 943
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfRegId:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfNodeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->forceSubscriptionRefresh:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->registerWithSkypeBackend(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 944
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->endpointId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v3, v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfRegId:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfNodeId:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1600(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    :goto_0
    return-object v0

    .line 946
    :cond_2
    :try_start_1
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x4

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 948
    :catch_0
    move-exception v7

    .line 949
    .local v7, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v0, v1, v2, v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 906
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 957
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 958
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 959
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 960
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isEDFRegistered()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 961
    const-string v1, "edf.reg.id"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 962
    const-string v1, "edf.node.id"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfNodeId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 963
    const-string v1, "last.edf.registration.time.03"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 964
    const-string v1, "skype.reg.token"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->skypeRegToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 965
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Successfully registered with Skype"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 977
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1702(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 978
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 980
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 967
    .restart local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->isLogin:Z

    if-nez v1, :cond_2

    .line 971
    const-string v1, "user.flags"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v3

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 973
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to register with Skype"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 906
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 921
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 922
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    .line 924
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "edf.reg.id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfRegId:Ljava/lang/String;

    .line 925
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "edf.node.id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->edfNodeId:Ljava/lang/String;

    .line 927
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 928
    return-void
.end method
