.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;
.super Ljava/lang/Object;
.source "LocalizedGamePropertyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GenreCategoryTree"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final localizedData:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocalizedData"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;",
            ">;"
        }
    .end annotation
.end field

.field private localizedDataLookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "genreData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    .line 132
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->rebuildLookup()V

    .line 133
    return-void
.end method

.method private rebuildLookup()V
    .locals 5

    .prologue
    .line 136
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedDataLookup:Ljava/util/Map;

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;

    .line 139
    .local v0, "data":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedDataLookup:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;->canonical:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;->localized:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 141
    .end local v0    # "data":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 156
    if-ne p1, p0, :cond_0

    .line 157
    const/4 v1, 0x1

    .line 162
    :goto_0
    return v1

    .line 158
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;

    if-nez v1, :cond_1

    .line 159
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 161
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;

    .line 162
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getLocalizedGenre(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedDataLookup:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->rebuildLookup()V

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedDataLookup:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->hashCode:I

    if-nez v0, :cond_0

    .line 169
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->hashCode:I

    .line 170
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->localizedData:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->hashCode:I

    .line 172
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
