.class public Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FriendsSelectorListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "rowViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p3, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->notifyDataSetChanged()V

    .line 29
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 33
    move-object/from16 v10, p2

    .line 35
    .local v10, "v":Landroid/view/View;
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v13

    instance-of v13, v13, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    if-eqz v13, :cond_f

    .line 36
    if-eqz v10, :cond_0

    instance-of v13, v10, Landroid/widget/LinearLayout;

    if-nez v13, :cond_1

    .line 37
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    .line 38
    .local v12, "vi":Landroid/view/LayoutInflater;
    const v13, 0x7f03010a

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v12, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 41
    .end local v12    # "vi":Landroid/view/LayoutInflater;
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 42
    .local v2, "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    if-eqz v2, :cond_7

    .line 43
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsDummy()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 44
    const/16 v13, 0x8

    invoke-virtual {v10, v13}, Landroid/view/View;->setVisibility(I)V

    move-object v11, v10

    .line 107
    .end local v2    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .end local v10    # "v":Landroid/view/View;
    .local v11, "v":Landroid/view/View;
    :goto_0
    return-object v11

    .line 48
    .end local v11    # "v":Landroid/view/View;
    .restart local v2    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .restart local v10    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {v10, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    const v13, 0x7f0e05d5

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 50
    .local v3, "gamertagView":Landroid/widget/TextView;
    const v13, 0x7f0e05d6

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 52
    .local v9, "realnameView":Landroid/widget/TextView;
    const v13, 0x7f0e05d3

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 53
    .local v6, "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    const v13, 0x7f0e0231

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 54
    .local v7, "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v13, 0x7f0e04b0

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    .local v1, "favoriteIconView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v13, 0x7f0e05d7

    invoke-virtual {v10, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 57
    .local v5, "iconSelect":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    .line 58
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamertag()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_3
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerRealName()Ljava/lang/String;

    move-result-object v8

    .line 62
    .local v8, "realName":Ljava/lang/String;
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_8

    const/16 v13, 0x8

    :goto_1
    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsOnline()Z

    move-result v13

    if-eqz v13, :cond_9

    const/4 v13, 0x0

    :goto_2
    invoke-static {v7, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 67
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v13

    if-eqz v13, :cond_a

    const/4 v13, 0x0

    :goto_3
    invoke-static {v5, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 68
    iget-boolean v13, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->isFavorite:Z

    if-eqz v13, :cond_b

    const/4 v13, 0x0

    :goto_4
    invoke-static {v1, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 70
    iget-boolean v13, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->isFavorite:Z

    if-eqz v13, :cond_4

    .line 71
    const/16 v13, 0x8

    invoke-virtual {v7, v13}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 72
    iget-object v13, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v14, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v13, v14, :cond_c

    .line 73
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c000c

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v1, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 78
    :cond_4
    :goto_5
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v13

    if-eqz v13, :cond_d

    const/4 v13, 0x0

    :goto_6
    invoke-static {v5, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 79
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsEnable()Z

    move-result v13

    if-nez v13, :cond_5

    if-eqz v5, :cond_5

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0151

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 82
    :cond_5
    if-eqz v6, :cond_6

    .line 84
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f020125

    const v15, 0x7f020125

    .line 83
    invoke-virtual {v6, v13, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 89
    :cond_6
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v13

    if-eqz v13, :cond_e

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/view/View;->setBackgroundColor(I)V

    .end local v1    # "favoriteIconView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v2    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .end local v3    # "gamertagView":Landroid/widget/TextView;
    .end local v5    # "iconSelect":Landroid/widget/TextView;
    .end local v6    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .end local v7    # "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .end local v8    # "realName":Ljava/lang/String;
    .end local v9    # "realnameView":Landroid/widget/TextView;
    :cond_7
    :goto_7
    move-object v11, v10

    .line 107
    .end local v10    # "v":Landroid/view/View;
    .restart local v11    # "v":Landroid/view/View;
    goto/16 :goto_0

    .line 63
    .end local v11    # "v":Landroid/view/View;
    .restart local v1    # "favoriteIconView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .restart local v2    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .restart local v3    # "gamertagView":Landroid/widget/TextView;
    .restart local v5    # "iconSelect":Landroid/widget/TextView;
    .restart local v6    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .restart local v7    # "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .restart local v8    # "realName":Ljava/lang/String;
    .restart local v9    # "realnameView":Landroid/widget/TextView;
    .restart local v10    # "v":Landroid/view/View;
    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 66
    :cond_9
    const/16 v13, 0x8

    goto/16 :goto_2

    .line 67
    :cond_a
    const/16 v13, 0x8

    goto :goto_3

    .line 68
    :cond_b
    const/16 v13, 0x8

    goto :goto_4

    .line 74
    :cond_c
    iget-object v13, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v14, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v13, v14, :cond_4

    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0012

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v1, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    goto :goto_5

    .line 78
    :cond_d
    const/16 v13, 0x8

    goto :goto_6

    .line 92
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0145

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_7

    .line 96
    .end local v1    # "favoriteIconView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v2    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    .end local v3    # "gamertagView":Landroid/widget/TextView;
    .end local v5    # "iconSelect":Landroid/widget/TextView;
    .end local v6    # "listTileView":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .end local v7    # "presenceIcon":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .end local v8    # "realName":Ljava/lang/String;
    .end local v9    # "realnameView":Landroid/widget/TextView;
    :cond_f
    if-eqz v10, :cond_10

    instance-of v13, v10, Landroid/widget/TextView;

    if-nez v13, :cond_11

    .line 97
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getContext()Landroid/content/Context;

    move-result-object v13

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    .line 98
    .restart local v12    # "vi":Landroid/view/LayoutInflater;
    const v13, 0x7f030108

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 101
    .end local v12    # "vi":Landroid/view/LayoutInflater;
    :cond_11
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 102
    .local v4, "headerText":Ljava/lang/String;
    if-eqz v4, :cond_7

    instance-of v13, v10, Landroid/widget/TextView;

    if-eqz v13, :cond_7

    .line 103
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v13, v10

    .line 104
    check-cast v13, Landroid/widget/TextView;

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7
.end method
