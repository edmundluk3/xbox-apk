.class public interface abstract Lcom/microsoft/xbox/xle/test/automator/IAutomator;
.super Ljava/lang/Object;
.source "IAutomator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;
    }
.end annotation


# virtual methods
.method public abstract OnActivityStateChange(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;)V
.end method

.method public abstract cleanupListenerHooks()V
.end method

.method public abstract clearFPSTracker()V
.end method

.method public abstract clearLPSTracker()V
.end method

.method public abstract clearLastSentKeys()V
.end method

.method public abstract clearMostRecentlyPlayedSounds()V
.end method

.method public abstract expireToken(Ljava/lang/String;)Z
.end method

.method public abstract getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;)",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;"
        }
    .end annotation
.end method

.method public abstract getAppBarNowPlaying()Z
.end method

.method public abstract getAppBarState()Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarState;
.end method

.method public abstract getApplicationBlockingTouch()Z
.end method

.method public abstract getAverageFPS()I
.end method

.method public abstract getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.end method

.method public abstract getCurrentDialog()Landroid/app/Dialog;
.end method

.method public abstract getCurrentViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;"
        }
    .end annotation
.end method

.method public abstract getDismissKeyboardMethod()Lcom/microsoft/xle/test/interop/delegates/Action;
.end method

.method public abstract getIsTablet()Z
.end method

.method public abstract getLastSentKeys()[Lcom/microsoft/xbox/service/model/LRCControlKey;
.end method

.method public abstract getMaxMemoryLimitKb()I
.end method

.method public abstract getMinFPS()I
.end method

.method public abstract getMostRecentlyPlayedSounds()[Ljava/lang/Integer;
.end method

.method public abstract getPageStates()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResourceText(I)Ljava/lang/String;
.end method

.method public abstract getTestAllowsAutoConnect(Z)Z
.end method

.method public abstract getTotalLeakedAdapterCount()I
.end method

.method public abstract getTotalPages()I
.end method

.method public abstract getUsedKb()I
.end method

.method public abstract getViewScreenRect(Landroid/view/View;)Landroid/graphics/Rect;
.end method

.method public abstract handleThrowable(Ljava/lang/Throwable;)V
.end method

.method public abstract isAnimating()Z
.end method

.method public abstract leaveSession()V
.end method

.method public abstract logOut()V
.end method

.method public abstract onShowDialog(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract onShowError(I)Z
.end method

.method public abstract setAdapter(Ljava/lang/Class;Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;",
            ")V"
        }
    .end annotation
.end method

.method public abstract setAllowAutoConnect(Z)V
.end method

.method public abstract setAllowDismissKeyboardHandler(Z)V
.end method

.method public abstract setAppBarAnimationCallback(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            "Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setAppBarItemHook(Lcom/microsoft/xle/test/interop/delegates/Action2;Lcom/microsoft/xle/test/interop/delegates/Action;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;",
            "Lcom/microsoft/xle/test/interop/delegates/Action;",
            ")V"
        }
    .end annotation
.end method

.method public abstract setAppbarAnimationState(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V
.end method

.method public abstract setAutomaticSignin(ZILcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Landroid/webkit/WebView;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setCompanionSession(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;)V
.end method

.method public abstract setCurrentDialog(Ljava/lang/String;Landroid/app/Dialog;)V
.end method

.method public abstract setEDSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;)V
.end method

.method public abstract setEnvironment(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V
.end method

.method public abstract setIsUsingToken(Z)V
.end method

.method public abstract setListenerHook(ILandroid/view/View$OnClickListener;)V
.end method

.method public abstract setLogOutOnLaunch(Z)V
.end method

.method public abstract setMembershipLevel(ZZ)V
.end method

.method public abstract setMonitorLPS(Z)V
.end method

.method public abstract setNoRedirectTestNetworkHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
.end method

.method public abstract setNotifyOnActivityStateChange(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            "Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setNotifyOnServiceManagerActivityStateChange(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setOnLoginStateChangedCallback(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/XLEException;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setOnShowDialog(Lcom/microsoft/xle/test/interop/delegates/Action2;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setOnShowError(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setSLSServiceManager(Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;)V
.end method

.method public abstract setTakeScreenshot(Lcom/microsoft/xle/test/interop/delegates/Action;)V
.end method

.method public abstract setTestNetworkHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
.end method

.method public abstract setTestSettings(Z)V
.end method

.method public abstract setTestTextureHttpClient(Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;)V
.end method

.method public abstract setThrowableHandler(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setUseStub(Z)V
.end method

.method public abstract setUserIsChild(ZZ)V
.end method

.method public abstract setVersion(III)V
.end method

.method public abstract setVersionToApplicationDefaults()V
.end method

.method public abstract setViewModelForActivity(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/ViewGroup;",
            ">(TT;",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ")V"
        }
    .end annotation
.end method

.method public abstract takeScreenshot()V
.end method

.method public abstract testTearDown()V
.end method
