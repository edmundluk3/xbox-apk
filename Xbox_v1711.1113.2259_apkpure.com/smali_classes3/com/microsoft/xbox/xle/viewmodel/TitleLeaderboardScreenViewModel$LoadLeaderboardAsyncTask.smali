.class Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TitleLeaderboardScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadLeaderboardAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$1;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshLeaderBoard(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 183
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;J)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Lcom/microsoft/xbox/service/model/TitleModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    .line 189
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 189
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->loadLeaderBoard(ZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->onLoadTitleLeaderboardCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 161
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->onLoadTitleLeaderboardCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 173
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 150
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 165
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;Z)Z

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel$LoadLeaderboardAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->updateAdapter()V

    .line 168
    return-void
.end method
