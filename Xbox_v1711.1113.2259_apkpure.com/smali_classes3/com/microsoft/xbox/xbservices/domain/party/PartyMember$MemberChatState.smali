.class public final enum Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
.super Ljava/lang/Enum;
.source "PartyMember.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MemberChatState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Connecting:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Disconnected:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Muted:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum PrivacyBlocked:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum PrivilegeBanned:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum ServiceRequestFailed:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Silent:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Talking:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

.field public static final enum Unknown:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Unknown:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Disconnected"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Disconnected:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Connecting:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "PrivacyBlocked"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->PrivacyBlocked:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Muted"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Muted:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Silent"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Silent:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "Talking"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Talking:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "ServiceRequestFailed"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->ServiceRequestFailed:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    const-string v1, "PrivilegeBanned"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->PrivilegeBanned:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 18
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Unknown:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Disconnected:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Connecting:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->PrivacyBlocked:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Muted:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Silent:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Talking:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->ServiceRequestFailed:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->PrivilegeBanned:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->$VALUES:[Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->$VALUES:[Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    return-object v0
.end method
