.class public abstract Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "ReactModalActivityBase.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/base/ReactScreen;


# instance fields
.field reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate()V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 24
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;)V

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->onCreateContentView()V

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->onCreateFinished()V

    .line 27
    return-void
.end method

.method public final onCreateContentView()V
    .locals 5

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->getLayoutId()Ljava/lang/Integer;

    move-result-object v0

    .line 35
    .local v0, "layoutId":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->setContentView(I)V

    .line 37
    const v2, 0x7f0e0ae5

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/ReactRootView;

    .line 38
    .local v1, "reactRootView":Lcom/facebook/react/ReactRootView;
    const-string v2, "There must be a ReactRootView with id = react_root_view"

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 46
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->getModuleName()Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->getLaunchOptions()Landroid/os/Bundle;

    move-result-object v4

    .line 44
    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/react/ReactRootView;->startReactApplication(Lcom/facebook/react/ReactInstanceManager;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 48
    return-void

    .line 40
    .end local v1    # "reactRootView":Lcom/facebook/react/ReactRootView;
    :cond_0
    new-instance v1, Lcom/facebook/react/ReactRootView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/react/ReactRootView;-><init>(Landroid/content/Context;)V

    .line 41
    .restart local v1    # "reactRootView":Lcom/facebook/react/ReactRootView;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
