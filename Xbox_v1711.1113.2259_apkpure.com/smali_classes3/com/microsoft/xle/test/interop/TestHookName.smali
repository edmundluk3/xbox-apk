.class public final enum Lcom/microsoft/xle/test/interop/TestHookName;
.super Ljava/lang/Enum;
.source "TestHookName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xle/test/interop/TestHookName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum ACHIEVEMENTSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum ACHIEVEMENTSFRIENDSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum ACHIEVEMENTSRECENTFILTERSWITCHGALLERY:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum ACHIEVEMENTSRECENTPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum ACHIEVEMENTSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum CONSOLEBAROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum CONSOLEBARVISIBLE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum CONSOLECONNECT:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum CONSOLEPICKEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum DRAWEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSACHIEVEMENTSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSGAMECLIPSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum GAMEPROGRESSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum PEOPLEHUBACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum PROFILELOADCOMPLETE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum REMOTEOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum SEARCHDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

.field public static final enum VOLUMEDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "ACHIEVEMENTSRECENTPROGRESSDATALOAD"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSRECENTPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 6
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "ACHIEVEMENTSSUMMARYDATALOAD"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 7
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "ACHIEVEMENTSCHALLENGESDATALOAD"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 8
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "ACHIEVEMENTSFRIENDSDATALOAD"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSFRIENDSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 9
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "CONSOLEBAROPENCLOSE"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEBAROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 10
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "CONSOLEBARVISIBLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEBARVISIBLE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 11
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "CONSOLECONNECT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLECONNECT:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 12
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "DRAWEROPENCLOSE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->DRAWEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 13
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSACHIEVEMENTSDATALOAD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSACHIEVEMENTSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 14
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSACHIEVEMENTSFILTERSWITCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 15
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSCHALLENGESDATALOAD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 16
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSDATALOAD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 17
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSGAMECLIPSDATALOAD"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSGAMECLIPSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 18
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "GAMEPROGRESSSUMMARYDATALOAD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 19
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "PROFILELOADCOMPLETE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->PROFILELOADCOMPLETE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 20
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "REMOTEOPENCLOSE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->REMOTEOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 21
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "SEARCHDIALOGOPENCLOSE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->SEARCHDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 22
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "PEOPLEHUBACHIEVEMENTSFILTERSWITCH"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->PEOPLEHUBACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 23
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "ACHIEVEMENTSRECENTFILTERSWITCHGALLERY"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSRECENTFILTERSWITCHGALLERY:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 24
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "VOLUMEDIALOGOPENCLOSE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->VOLUMEDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 25
    new-instance v0, Lcom/microsoft/xle/test/interop/TestHookName;

    const-string v1, "CONSOLEPICKEROPENCLOSE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xle/test/interop/TestHookName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEPICKEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    .line 4
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/microsoft/xle/test/interop/TestHookName;

    sget-object v1, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSRECENTPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSFRIENDSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEBAROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEBARVISIBLE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLECONNECT:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->DRAWEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSACHIEVEMENTSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSCHALLENGESDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSGAMECLIPSDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->GAMEPROGRESSSUMMARYDATALOAD:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->PROFILELOADCOMPLETE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->REMOTEOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->SEARCHDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->PEOPLEHUBACHIEVEMENTSFILTERSWITCH:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->ACHIEVEMENTSRECENTFILTERSWITCHGALLERY:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->VOLUMEDIALOGOPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xle/test/interop/TestHookName;->CONSOLEPICKEROPENCLOSE:Lcom/microsoft/xle/test/interop/TestHookName;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->$VALUES:[Lcom/microsoft/xle/test/interop/TestHookName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xle/test/interop/TestHookName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/microsoft/xle/test/interop/TestHookName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xle/test/interop/TestHookName;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xle/test/interop/TestHookName;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/microsoft/xle/test/interop/TestHookName;->$VALUES:[Lcom/microsoft/xle/test/interop/TestHookName;

    invoke-virtual {v0}, [Lcom/microsoft/xle/test/interop/TestHookName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xle/test/interop/TestHookName;

    return-object v0
.end method
