.class public final Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;
.super Ljava/lang/Object;
.source "PeopleHubServiceModule_ProvidePeopleHubEndpointFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->module:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .line 18
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;",
            ")",
            "Ldagger/internal/Factory",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;-><init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)V

    return-object v0
.end method

.method public static proxyProvidePeopleHubEndpoint(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)Ljava/lang/String;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;->providePeopleHubEndpoint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->module:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .line 23
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;->providePeopleHubEndpoint()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 22
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
