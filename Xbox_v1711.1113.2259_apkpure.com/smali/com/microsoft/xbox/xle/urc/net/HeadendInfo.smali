.class public Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
.super Ljava/lang/Object;
.source "HeadendInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;,
        Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;,
        Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadendInfo"


# instance fields
.field public headend_locale:Ljava/lang/String;

.field public preferred_provider:Ljava/lang/String;

.field public providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

.field public streaming_port:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJSON(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 22
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 121
    new-instance v17, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-direct/range {v17 .. v17}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;-><init>()V

    .line 124
    .local v17, "d":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    :try_start_0
    const-string/jumbo v2, "streamingPort"

    const-string v3, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    .line 125
    const-string v2, "headendLocale"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->normalizeHeadendLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    .line 126
    const-string v2, "preferredProvider"

    const-string v3, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    .line 130
    const-string v2, "providers"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "providers"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 131
    const-string v2, "providers"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v21

    .line 132
    .local v21, "provider_list":Lorg/json/JSONArray;
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 133
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v19

    if-ge v0, v2, :cond_3

    .line 134
    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v20

    .line 135
    .local v20, "json_provider":Lorg/json/JSONObject;
    const-string/jumbo v2, "titleId"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "title_id":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 137
    .local v7, "isPreferred":Z
    const-string/jumbo v2, "source"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v6

    .line 139
    .local v6, "source":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    const-string v2, "filterPreference"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    move-result-object v8

    .line 140
    .local v8, "filter":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;
    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->UNDEFINED:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    if-ne v8, v2, :cond_0

    .line 141
    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v6, v2, :cond_1

    .line 142
    sget-object v8, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->hdsd:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 148
    :cond_0
    :goto_1
    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    new-instance v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    const-string v3, "headendId"

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "providerName"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "canStream"

    const-string v11, "false"

    .line 149
    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v11, "true"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    invoke-direct/range {v2 .. v9}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;ZLcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;Z)V

    aput-object v2, v10, v19

    .line 150
    const-string v2, "Filter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HeadendInfo.fromJSON "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v5, v5, v19

    iget-object v5, v5, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 144
    :cond_1
    sget-object v8, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->all:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    goto :goto_1

    .line 153
    .end local v4    # "title_id":Ljava/lang/String;
    .end local v6    # "source":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .end local v7    # "isPreferred":Z
    .end local v8    # "filter":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;
    .end local v19    # "i":I
    .end local v20    # "json_provider":Lorg/json/JSONObject;
    .end local v21    # "provider_list":Lorg/json/JSONArray;
    :cond_2
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 154
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    const/4 v3, 0x0

    new-instance v9, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    const-string v5, "headendId"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "162615AD"

    const-string v5, "providerName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    const/4 v14, 0x0

    sget-object v15, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->hdsd:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    const/16 v16, 0x0

    invoke-direct/range {v9 .. v16}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;ZLcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;Z)V

    aput-object v9, v2, v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v17    # "d":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    :cond_3
    :goto_2
    return-object v17

    .line 157
    .restart local v17    # "d":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    :catch_0
    move-exception v18

    .line 158
    .local v18, "e":Lorg/json/JSONException;
    const-string v2, "HeadendInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Malformed headend object: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->reportMalformedHeadend()V

    .line 160
    const/16 v17, 0x0

    goto :goto_2
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 4
    .param p0, "headendid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 167
    if-nez p0, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 183
    :goto_0
    return-object v0

    .line 171
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;-><init>()V

    .line 173
    .local v0, "d":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 174
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    .line 175
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    const-string v2, "162615AD"

    iput-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->title_id:Ljava/lang/String;

    .line 176
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    const-string v2, "PROVIDER"

    iput-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    .line 177
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    iput-boolean v3, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->isPreferred:Z

    .line 178
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->hdsd:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    iput-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 179
    iget-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v1, v3

    iput-boolean v3, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->can_stream:Z

    .line 180
    const-string v1, "00000"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    .line 181
    const-string v1, "en-US"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    goto :goto_0
.end method

.method private static normalizeHeadendLocale(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "headendLocale"    # Ljava/lang/String;

    .prologue
    .line 270
    if-eqz p0, :cond_1

    .line 273
    const-string v2, "[ ;]"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 275
    .local v1, "locales":[Ljava/lang/String;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 276
    .local v0, "locale":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v4, "-ploc"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 282
    .end local v0    # "locale":Ljava/lang/String;
    .end local v1    # "locales":[Ljava/lang/String;
    :goto_1
    return-object v0

    .line 275
    .restart local v0    # "locale":Ljava/lang/String;
    .restart local v1    # "locales":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 282
    .end local v0    # "locale":Ljava/lang/String;
    .end local v1    # "locales":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 14
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 199
    if-ne p0, p1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v4

    .line 202
    :cond_1
    if-nez p1, :cond_2

    move v4, v5

    .line 203
    goto :goto_0

    .line 205
    :cond_2
    instance-of v6, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    if-nez v6, :cond_3

    move v4, v5

    .line 206
    goto :goto_0

    :cond_3
    move-object v2, p1

    .line 208
    check-cast v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .line 210
    .local v2, "other":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    move v4, v5

    .line 211
    goto :goto_0

    .line 214
    :cond_4
    iget-object v6, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    move v4, v5

    .line 215
    goto :goto_0

    .line 218
    :cond_5
    iget-object v6, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    move v4, v5

    .line 219
    goto :goto_0

    .line 222
    :cond_6
    iget-object v6, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v6, v6

    iget-object v7, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v7, v7

    if-eq v6, v7, :cond_7

    move v4, v5

    .line 223
    goto :goto_0

    .line 227
    :cond_7
    iget-object v8, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v9, v8

    move v7, v5

    :goto_1
    if-ge v7, v9, :cond_0

    aget-object v3, v8, v7

    .line 228
    .local v3, "t_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    const/4 v0, 0x0

    .line 229
    .local v0, "found":Z
    iget-object v10, v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v11, v10

    move v6, v5

    :goto_2
    if-ge v6, v11, :cond_8

    aget-object v1, v10, v6

    .line 230
    .local v1, "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v12, v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    iget-object v13, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 231
    const/4 v0, 0x1

    .line 235
    .end local v1    # "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_8
    if-nez v0, :cond_a

    move v4, v5

    .line 236
    goto :goto_0

    .line 229
    .restart local v1    # "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 227
    .end local v1    # "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_a
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1
.end method

.method public hasSettingDiff(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)Z
    .locals 11
    .param p1, "other"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    const/4 v2, 0x0

    .line 251
    if-nez p1, :cond_1

    .line 266
    :cond_0
    :goto_0
    return v2

    .line 255
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v6, v5

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v1, v5, v4

    .line 256
    .local v1, "t_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v7, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v8, v7

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_3

    aget-object v0, v7, v3

    .line 257
    .local v0, "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v9, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    iget-object v10, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 258
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->hasSettingDiff(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 259
    const/4 v2, 0x1

    goto :goto_0

    .line 256
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 255
    .end local v0    # "o_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 188
    const/16 v0, 0x1f

    .line 189
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 190
    .local v1, "result":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 191
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 192
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 193
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 194
    return v1

    .line 190
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 192
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->preferred_provider:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_2
.end method
