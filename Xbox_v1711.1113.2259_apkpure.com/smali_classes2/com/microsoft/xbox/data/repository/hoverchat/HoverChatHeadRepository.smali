.class public interface abstract Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
.super Ljava/lang/Object;
.source "HoverChatHeadRepository.java"


# virtual methods
.method public abstract addClubChatKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addMessageKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getChatHeadLifecycleEvents()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getChatHeadManagerEvents()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getClubChatKeys()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLeftMessageGroupEvents()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMessageKeys()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hoverChatIsOpen()Z
.end method

.method public abstract isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract isEnabledGlobally()Z
.end method

.method public abstract isEnabledGloballyObs()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onChatHeadAttached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onChatHeadDetached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onChatHeadRemoved(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onLeaveGroupMessage(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setActiveChatHeadManager(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .param p1    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Z)V
    .param p1    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setEnabledGlobally(Z)V
.end method
