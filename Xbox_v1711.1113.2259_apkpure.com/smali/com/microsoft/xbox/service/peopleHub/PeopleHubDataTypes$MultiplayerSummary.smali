.class public abstract Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
.super Ljava/lang/Object;
.source "PeopleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerSummary"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_MultiplayerSummary$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_MultiplayerSummary$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract inMultiplayerSession()I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "InMultiplayerSession"
    .end annotation
.end method

.method public abstract inParty()I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "InParty"
    .end annotation
.end method
