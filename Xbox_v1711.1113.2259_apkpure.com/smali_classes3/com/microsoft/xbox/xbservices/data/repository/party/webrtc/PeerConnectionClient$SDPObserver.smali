.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Lorg/webrtc/SdpObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SDPObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0

    .prologue
    .line 932
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;

    .prologue
    .line 932
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    return-void
.end method


# virtual methods
.method public onCreateFailure(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createSDP error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    .line 1009
    return-void
.end method

.method public onCreateSuccess(Lorg/webrtc/SessionDescription;)V
    .locals 5
    .param p1, "origSdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    .line 935
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v2

    const-string v3, "PCRTCClient"

    const-string v4, "SdpObserver.onCreateSuccess"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    iget-object v1, p1, Lorg/webrtc/SessionDescription;->description:Ljava/lang/String;

    .line 943
    .local v1, "sdpDescription":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 944
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    const-string v3, "ISAC"

    const/4 v4, 0x1

    invoke-static {v2, v1, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 947
    :cond_0
    new-instance v0, Lorg/webrtc/SessionDescription;

    iget-object v2, p1, Lorg/webrtc/SessionDescription;->type:Lorg/webrtc/SessionDescription$Type;

    invoke-direct {v0, v2, v1}, Lorg/webrtc/SessionDescription;-><init>(Lorg/webrtc/SessionDescription$Type;Ljava/lang/String;)V

    .line 948
    .local v0, "sdp":Lorg/webrtc/SessionDescription;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2702(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lorg/webrtc/SessionDescription;)Lorg/webrtc/SessionDescription;

    .line 949
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;

    invoke-direct {v3, p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;Lorg/webrtc/SessionDescription;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 965
    return-void
.end method

.method public onSetFailure(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSDP error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    .line 1013
    return-void
.end method

.method public onSetSuccess()V
    .locals 2

    .prologue
    .line 968
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver$2;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$SDPObserver;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1005
    return-void
.end method
