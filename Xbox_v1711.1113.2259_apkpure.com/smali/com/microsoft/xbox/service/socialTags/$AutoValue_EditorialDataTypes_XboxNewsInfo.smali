.class abstract Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;
.super Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
.source "$AutoValue_EditorialDataTypes_XboxNewsInfo.java"


# instance fields
.field private final bgColor:Ljava/lang/String;

.field private final bgImage:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final featuredURL:Ljava/lang/String;

.field private final iconImage:Ljava/lang/String;

.field private final profileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "bgColor"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "bgImage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "featuredURL"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "iconImage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "profileName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    .line 30
    iput-object p6, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public bgColor()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BgColor"
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    return-object v0
.end method

.method public bgImage()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BgImage"
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Description"
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    if-eqz v3, :cond_9

    move-object v0, p1

    .line 93
    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    .line 94
    .local v0, "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgColor()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgImage()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->featuredURL()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->iconImage()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->profileName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 94
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 95
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgImage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 96
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 97
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->featuredURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 98
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->iconImage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 99
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->profileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    :cond_9
    move v1, v2

    .line 101
    goto/16 :goto_0
.end method

.method public featuredURL()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "FeaturedURL"
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 106
    const/4 v0, 0x1

    .line 107
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v3

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v3

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 113
    mul-int/2addr v0, v3

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 115
    mul-int/2addr v0, v3

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 117
    mul-int/2addr v0, v3

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    if-nez v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 119
    return v0

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 112
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 114
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 116
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 118
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public iconImage()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IconImage"
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    return-object v0
.end method

.method public profileName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProfileName"
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "XboxNewsInfo{bgColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bgImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->bgImage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", featuredURL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->featuredURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", iconImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->iconImage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profileName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_XboxNewsInfo;->profileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
