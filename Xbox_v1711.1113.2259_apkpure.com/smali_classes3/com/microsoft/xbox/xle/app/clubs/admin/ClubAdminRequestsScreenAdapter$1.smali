.class Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubAdminRequestsScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ignoreAll()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->ignoreAll()V

    .line 54
    return-void
.end method

.method public onAcceptClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->acceptRequest(J)V

    .line 64
    return-void
.end method

.method public onIgnoreClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->ignoreRequest(J)V

    .line 69
    return-void
.end method

.method public onProfileClick(J)V
    .locals 3
    .param p1, "xuid"    # J

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public sendAllInvites()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->sendAllInvites()V

    .line 49
    return-void
.end method
