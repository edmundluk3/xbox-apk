.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;
.super Ljava/lang/Object;
.source "ILikeControl.java"


# virtual methods
.method public abstract deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
.end method

.method public abstract likeClick(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method public abstract navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
.end method

.method public abstract navigateToPostCommentScreen(Ljava/lang/String;)V
.end method

.method public abstract navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
.end method

.method public abstract resetLikeControlUpdate()V
.end method

.method public abstract shouldUpdateLikeControl()Z
.end method
