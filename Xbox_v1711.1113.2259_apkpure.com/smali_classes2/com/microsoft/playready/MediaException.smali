.class public Lcom/microsoft/playready/MediaException;
.super Ljava/lang/Exception;
.source "MediaException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;
    }
.end annotation


# instance fields
.field private mErrorCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 40
    const-string v0, "Media exception"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 41
    iput p1, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;)V
    .locals 1
    .param p1, "err"    # Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    .prologue
    .line 46
    const-string v0, "Media exception"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 47
    invoke-virtual {p1}, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->getErrorCode()I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v1, "Media exception"

    invoke-direct {p0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 32
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 53
    const-string v1, "MediaException"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception message = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    const-string v1, "MediaException"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, -0x1

    iput v1, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    .line 62
    const-string v1, "MediaException"

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    const-string v0, "MediaException: 0x%1$08X"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/microsoft/playready/MediaException;->mErrorCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
