.class public Lcom/rt2zz/reactnativecontacts/ContactsProvider;
.super Ljava/lang/Object;
.source "ContactsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    }
.end annotation


# static fields
.field private static final FULL_PROJECTION:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ID_FOR_PROFILE_CONTACT:I = -0x1

.field private static final JUST_ME_PROJECTION:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTO_PROJECTION:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final contentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$1;

    invoke-direct {v0}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$1;-><init>()V

    sput-object v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->JUST_ME_PROJECTION:Ljava/util/List;

    .line 62
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$2;

    invoke-direct {v0}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$2;-><init>()V

    sput-object v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->FULL_PROJECTION:Ljava/util/List;

    .line 66
    new-instance v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider$3;

    invoke-direct {v0}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$3;-><init>()V

    sput-object v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->PHOTO_PROJECTION:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->contentResolver:Landroid/content/ContentResolver;

    .line 74
    return-void
.end method

.method static synthetic access$000()Ljava/util/List;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->JUST_ME_PROJECTION:Ljava/util/List;

    return-object v0
.end method

.method private loadContactsFrom(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, -0x1

    .line 156
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 158
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :cond_0
    :goto_0
    if-eqz p1, :cond_a

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 160
    const-string v11, "contact_id"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 162
    .local v0, "columnIndex":I
    if-eq v0, v13, :cond_4

    .line 163
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "contactId":Ljava/lang/String;
    :goto_1
    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 170
    new-instance v11, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    invoke-direct {v11, v2}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v2, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    :cond_1
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .line 175
    .local v1, "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    const-string v11, "mimetype"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 177
    .local v6, "mimeType":Ljava/lang/String;
    const-string v11, "display_name"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, "name":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-static {v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$100(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 179
    invoke-static {v1, v7}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$102(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 182
    :cond_2
    invoke-static {v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$200(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 183
    const-string v11, "photo_uri"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 184
    .local v9, "rawPhotoURI":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 185
    invoke-static {v1, v9}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$202(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    const/4 v11, 0x1

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$302(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Z)Z

    .line 190
    .end local v9    # "rawPhotoURI":Ljava/lang/String;
    :cond_3
    const-string v11, "vnd.android.cursor.item/name"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 191
    const-string v11, "data2"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$402(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 192
    const-string v11, "data5"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$502(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 193
    const-string v11, "data3"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$602(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 194
    const-string v11, "data4"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$702(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 195
    const-string v11, "data6"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$802(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 166
    .end local v1    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .end local v2    # "contactId":Ljava/lang/String;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    :cond_4
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "contactId":Ljava/lang/String;
    goto/16 :goto_1

    .line 196
    .restart local v1    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .restart local v6    # "mimeType":Ljava/lang/String;
    .restart local v7    # "name":Ljava/lang/String;
    :cond_5
    const-string v11, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 197
    const-string v11, "data1"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 198
    .local v8, "phoneNumber":Ljava/lang/String;
    const-string v11, "data2"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 200
    .local v10, "type":I
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 202
    packed-switch v10, :pswitch_data_0

    .line 213
    const-string v4, "other"

    .line 215
    .local v4, "label":Ljava/lang/String;
    :goto_2
    invoke-static {v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$900(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;

    move-result-object v11

    new-instance v12, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;

    invoke-direct {v12, v4, v8}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 204
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_0
    const-string v4, "home"

    .line 205
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_2

    .line 207
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_1
    const-string v4, "work"

    .line 208
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_2

    .line 210
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_2
    const-string v4, "mobile"

    .line 211
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_2

    .line 217
    .end local v4    # "label":Ljava/lang/String;
    .end local v8    # "phoneNumber":Ljava/lang/String;
    .end local v10    # "type":I
    :cond_6
    const-string v11, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 218
    const-string v11, "data1"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 219
    .local v3, "email":Ljava/lang/String;
    const-string v11, "data2"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 221
    .restart local v10    # "type":I
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 223
    packed-switch v10, :pswitch_data_1

    .line 241
    :pswitch_3
    const-string v4, "other"

    .line 243
    .restart local v4    # "label":Ljava/lang/String;
    :goto_3
    invoke-static {v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$1000(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;

    move-result-object v11

    new-instance v12, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;

    invoke-direct {v12, v4, v3}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$Item;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 225
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_4
    const-string v4, "home"

    .line 226
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_3

    .line 228
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_5
    const-string v4, "work"

    .line 229
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_3

    .line 231
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_6
    const-string v4, "mobile"

    .line 232
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_3

    .line 234
    .end local v4    # "label":Ljava/lang/String;
    :pswitch_7
    const-string v11, "data3"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_7

    .line 235
    const-string v11, "data3"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_3

    .line 237
    .end local v4    # "label":Ljava/lang/String;
    :cond_7
    const-string v4, ""

    .line 239
    .restart local v4    # "label":Ljava/lang/String;
    goto :goto_3

    .line 245
    .end local v3    # "email":Ljava/lang/String;
    .end local v4    # "label":Ljava/lang/String;
    .end local v10    # "type":I
    :cond_8
    const-string v11, "vnd.android.cursor.item/organization"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 246
    const-string v11, "data1"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$1102(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 247
    const-string v11, "data4"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$1202(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    .line 248
    const-string v11, "data5"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$1302(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 249
    :cond_9
    const-string v11, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 250
    invoke-static {v1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->access$1400(Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;)Ljava/util/List;

    move-result-object v11

    new-instance v12, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;

    invoke-direct {v12, p1}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact$PostalAddressItem;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 254
    .end local v0    # "columnIndex":I
    .end local v1    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .end local v2    # "contactId":Ljava/lang/String;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    :cond_a
    return-object v5

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 223
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public getContacts()Lcom/facebook/react/bridge/WritableArray;
    .locals 15

    .prologue
    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "data"

    .line 107
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->JUST_ME_PROJECTION:Ljava/util/List;

    sget-object v4, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->JUST_ME_PROJECTION:Ljava/util/List;

    .line 108
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    .line 106
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 115
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v12}, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->loadContactsFrom(Landroid/database/Cursor;)Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 117
    .local v14, "justMe":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    if-eqz v12, :cond_0

    .line 118
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 125
    :cond_0
    iget-object v4, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->contentResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->FULL_PROJECTION:Ljava/util/List;

    sget-object v1, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->FULL_PROJECTION:Ljava/util/List;

    .line 127
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    const-string v7, "mimetype=? OR mimetype=? OR mimetype=? OR mimetype=? OR mimetype=?"

    const/4 v0, 0x5

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "vnd.android.cursor.item/email_v2"

    aput-object v1, v8, v0

    const/4 v0, 0x1

    const-string v1, "vnd.android.cursor.item/phone_v2"

    aput-object v1, v8, v0

    const/4 v0, 0x2

    const-string v1, "vnd.android.cursor.item/name"

    aput-object v1, v8, v0

    const/4 v0, 0x3

    const-string v1, "vnd.android.cursor.item/organization"

    aput-object v1, v8, v0

    const/4 v0, 0x4

    const-string v1, "vnd.android.cursor.item/postal-address_v2"

    aput-object v1, v8, v0

    move-object v9, v3

    .line 125
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 134
    :try_start_1
    invoke-direct {p0, v12}, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->loadContactsFrom(Landroid/database/Cursor;)Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v13

    .line 136
    .local v13, "everyoneElse":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    if-eqz v12, :cond_1

    .line 137
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 142
    :cond_1
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v11

    .line 143
    .local v11, "contacts":Lcom/facebook/react/bridge/WritableArray;
    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .line 144
    .local v10, "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    invoke-virtual {v10}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->toMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v11, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 117
    .end local v10    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .end local v11    # "contacts":Lcom/facebook/react/bridge/WritableArray;
    .end local v13    # "everyoneElse":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    .end local v14    # "justMe":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_2

    .line 118
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 136
    .restart local v14    # "justMe":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :catchall_1
    move-exception v0

    if-eqz v12, :cond_3

    .line 137
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 146
    .restart local v11    # "contacts":Lcom/facebook/react/bridge/WritableArray;
    .restart local v13    # "everyoneElse":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :cond_4
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .line 147
    .restart local v10    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    invoke-virtual {v10}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->toMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v11, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_1

    .line 150
    .end local v10    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    :cond_5
    return-object v11
.end method

.method public getContactsMatchingString(Ljava/lang/String;)Lcom/facebook/react/bridge/WritableArray;
    .locals 12
    .param p1, "searchString"    # Ljava/lang/String;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->FULL_PROJECTION:Ljava/util/List;

    sget-object v3, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->FULL_PROJECTION:Ljava/util/List;

    .line 81
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const-string v3, "display_name LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    .line 79
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 88
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v8}, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->loadContactsFrom(Landroid/database/Cursor;)Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 90
    .local v9, "matchingContacts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    if-eqz v8, :cond_0

    .line 91
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_0
    invoke-static {}, Lcom/facebook/react/bridge/Arguments;->createArray()Lcom/facebook/react/bridge/WritableArray;

    move-result-object v7

    .line 97
    .local v7, "contacts":Lcom/facebook/react/bridge/WritableArray;
    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;

    .line 98
    .local v6, "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    invoke-virtual {v6}, Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;->toMap()Lcom/facebook/react/bridge/WritableMap;

    move-result-object v1

    invoke-interface {v7, v1}, Lcom/facebook/react/bridge/WritableArray;->pushMap(Lcom/facebook/react/bridge/WritableMap;)V

    goto :goto_0

    .line 90
    .end local v6    # "contact":Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;
    .end local v7    # "contacts":Lcom/facebook/react/bridge/WritableArray;
    .end local v9    # "matchingContacts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 91
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 100
    .restart local v7    # "contacts":Lcom/facebook/react/bridge/WritableArray;
    .restart local v9    # "matchingContacts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/rt2zz/reactnativecontacts/ContactsProvider$Contact;>;"
    :cond_2
    return-object v7
.end method

.method public getPhotoUriFromContactId(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "contactId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 258
    iget-object v0, p0, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->PHOTO_PROJECTION:Ljava/util/List;

    sget-object v3, Lcom/rt2zz/reactnativecontacts/ContactsProvider;->PHOTO_PROJECTION:Ljava/util/List;

    .line 260
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const-string v3, "contact_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v4, v8

    .line 258
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 266
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    const-string v0, "photo_uri"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 268
    .local v7, "rawPhotoURI":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 273
    if-eqz v6, :cond_0

    .line 274
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 277
    .end local v7    # "rawPhotoURI":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v7

    .line 273
    :cond_1
    if-eqz v6, :cond_2

    .line 274
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v7, v5

    .line 277
    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 274
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method
