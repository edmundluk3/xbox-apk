.class public Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "EnterIPAddressDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IViewUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;
    }
.end annotation


# static fields
.field private static final DIGIT_PATTERN:Ljava/lang/String; = "(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])"

.field private static final FULL_IP_ADDRESS:Ljava/util/regex/Pattern;

.field private static final PARTIAl_IP_ADDRESS:Ljava/util/regex/Pattern;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final autoConnectCheckbox:Landroid/widget/CheckBox;

.field private final autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final closeButton:Landroid/widget/Button;

.field private final connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

.field private final containerView:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

.field private final currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field private displayErrorOnDisconnect:Z

.field private final errMsg:Landroid/widget/TextView;

.field private final faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final ipAddrEditor:Landroid/widget/EditText;

.field private final myFilters:[Landroid/text/InputFilter;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConnectDialog:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    .line 44
    const-string v0, "^((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])\\.){0,3}((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])){0,1}$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->PARTIAl_IP_ADDRESS:Ljava/util/regex/Pattern;

    .line 45
    const-string v0, "^((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])\\.){3}((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])){1}$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->FULL_IP_ADDRESS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/model/ConsoleData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 57
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->displayErrorOnDisconnect:Z

    .line 331
    new-array v0, v5, [Landroid/text/InputFilter;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->myFilters:[Landroid/text/InputFilter;

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 66
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;-><init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    .line 67
    const v0, 0x7f0300ad

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->setContentView(I)V

    .line 68
    const v0, 0x7f0e0425

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->containerView:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    .line 69
    const v0, 0x7f0e0422

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->closeButton:Landroid/widget/Button;

    .line 70
    const v0, 0x7f0e0430

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 71
    const v0, 0x7f0e0429

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    .line 72
    const v0, 0x7f0e042f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    .line 73
    const v0, 0x7f0e0438

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    .line 74
    const v0, 0x7f0e0434

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0e0435

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->processConnectButton()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->isConnectButtonEnabled(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->PARTIAl_IP_ADDRESS:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private bindCurrentConsole()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 191
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v2

    .line 192
    .local v2, "state":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->containerView:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    if-ne v2, v4, :cond_0

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->NOT_LISTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    :goto_0
    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->setConsoleState(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;)V

    .line 194
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getConsoleConnected(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v0

    .line 196
    .local v0, "connected":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 197
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setVisible(Landroid/view/View;Z)V

    .line 198
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setVisible(Landroid/view/View;Z)V

    .line 200
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v1

    .line 201
    .local v1, "ipAddr":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    if-ne v2, v4, :cond_3

    const v4, 0x7f0c006d

    :goto_3
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 205
    .local v3, "textColor":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setTextColor(I)V

    .line 206
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->isConnectButtonEnabled(Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 207
    return-void

    .end local v0    # "connected":Z
    .end local v1    # "ipAddr":Ljava/lang/String;
    .end local v3    # "textColor":I
    :cond_0
    move-object v4, v2

    .line 192
    goto :goto_0

    .restart local v0    # "connected":Z
    :cond_1
    move v4, v6

    .line 197
    goto :goto_1

    :cond_2
    move v5, v6

    .line 198
    goto :goto_2

    .line 203
    .restart local v1    # "ipAddr":Ljava/lang/String;
    :cond_3
    const v4, 0x7f0c006e

    goto :goto_3
.end method

.method private bindResult()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 161
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 162
    .local v1, "lastResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v2

    .line 163
    .local v2, "state":Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    sget-object v3, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Console state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v3, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$9;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ConnectDialogViewModel$ConsoleState:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 166
    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->displayErrorOnDisconnect:Z

    .line 168
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->clearError()V

    goto :goto_0

    .line 171
    :pswitch_2
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    .line 172
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    if-eqz v0, :cond_1

    .line 173
    sget-object v3, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    const-string v4, "Got error"

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 175
    :cond_1
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->displayErrorOnDisconnect:Z

    .line 176
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->setError(Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0

    .line 179
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :pswitch_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->displayErrorOnDisconnect:Z

    if-eqz v3, :cond_0

    .line 180
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->displayErrorOnDisconnect:Z

    .line 181
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->setError(Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private clearError()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 329
    :cond_0
    return-void
.end method

.method private static cloneConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 4
    .param p0, "other"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 251
    new-instance v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;-><init>()V

    .line 252
    .local v0, "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    .line 253
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    .line 254
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    .line 256
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    iput-wide v2, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    .line 257
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    iput-wide v2, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    .line 258
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getSessionState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setSessionState(I)V

    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setAutoConnect(Z)V

    .line 260
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddressManuallyEntered()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setIpAddressManuallyEntered(Z)V

    .line 261
    return-object v0
.end method

.method private closeDelayed()V
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 231
    return-void
.end method

.method public static getConnectionErrorStringFromException(Lcom/microsoft/xbox/toolkit/XLEException;)Ljava/lang/String;
    .locals 8
    .param p0, "e"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    const v7, 0x7f07036c

    const-wide/16 v4, 0x0

    const v6, 0x7f070361

    .line 291
    const/4 v2, 0x0

    .line 293
    .local v2, "result":Ljava/lang/String;
    if-eqz p0, :cond_6

    .line 294
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    move-wide v0, v4

    .line 296
    .local v0, "errorCode":J
    :goto_0
    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    .line 297
    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidCertificate:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v3

    int-to-long v4, v3

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    .line 298
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 320
    .end local v0    # "errorCode":J
    :goto_1
    return-object v2

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 300
    .restart local v0    # "errorCode":J
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 302
    :cond_2
    cmp-long v3, v0, v4

    if-gez v3, :cond_5

    .line 303
    sget-object v3, Lcom/microsoft/xbox/smartglass/SGError;->CryptoInvalidCertificate:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v3

    int-to-long v4, v3

    cmp-long v3, v0, v4

    if-nez v3, :cond_3

    .line 304
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 306
    :cond_3
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    .line 307
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x8

    if-le v3, v4, :cond_4

    .line 308
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x8

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 310
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 313
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 317
    .end local v0    # "errorCode":J
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private isConnectButtonEnabled(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "ipAddr"    # Ljava/lang/CharSequence;

    .prologue
    .line 275
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->isValidIp(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getConsoleConnected(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidIp(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p0, "ipAddr"    # Ljava/lang/CharSequence;

    .prologue
    .line 266
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 271
    .local v0, "ret":Z
    :goto_0
    return v0

    .line 269
    .end local v0    # "ret":Z
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->FULL_IP_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .restart local v0    # "ret":Z
    goto :goto_0
.end method

.method private mergeWithCurrent(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "other"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 237
    if-eqz p1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-wide v2, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    iput-wide v2, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-wide v2, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    iput-wide v2, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getSessionState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setSessionState(I)V

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddressManuallyEntered()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setIpAddressManuallyEntered(Z)V

    .line 248
    :cond_0
    return-void
.end method

.method private processConnectButton()V
    .locals 3

    .prologue
    .line 210
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getConsoleConnected(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v0

    .line 211
    .local v0, "connected":Z
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    const-string v2, "Will be connecting"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->clearError()V

    .line 213
    if-eqz v0, :cond_0

    .line 214
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    const-string v2, "Already connected"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->bindCurrentConsole()V

    .line 216
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->closeDelayed()V

    .line 222
    :goto_0
    return-void

    .line 218
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->TAG:Ljava/lang/String;

    const-string v2, "Connecting"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->cloneConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->connect(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    goto :goto_0
.end method

.method private setError(Lcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 7
    .param p1, "e"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    const/4 v6, 0x0

    .line 279
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->getConnectionErrorStringFromException(Lcom/microsoft/xbox/toolkit/XLEException;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->errMsg:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 282
    const-wide/16 v0, 0x0

    .line 283
    .local v0, "errorCode":J
    if-eqz p1, :cond_0

    .line 284
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    const-wide/16 v0, 0x0

    .line 286
    :cond_0
    :goto_0
    const-string v3, "IP Address Console Connect: errorCode %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 287
    .local v2, "errorDescription":Ljava/lang/String;
    invoke-static {v2, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 288
    return-void

    .line 284
    .end local v2    # "errorDescription":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->close()V

    .line 147
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->onResume()V

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->closeButton:Landroid/widget/Button;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->myFilters:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaddingBottom()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 130
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 134
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->onPause()V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->closeButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->ipAddrEditor:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->autoConnectCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->connectButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->faqButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method public updateView(Ljava/lang/Object;)V
    .locals 2
    .param p1, "caller"    # Ljava/lang/Object;

    .prologue
    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->viewModel:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;->getTryingConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    .line 152
    .local v0, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->mergeWithCurrent(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 156
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->bindCurrentConsole()V

    .line 157
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->bindResult()V

    .line 158
    return-void
.end method
