.class Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;
.super Ljava/lang/Object;
.source "VerticalScrollDockLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->initializeHeaderCloseAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 603
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 607
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$500(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 608
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 621
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 625
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$500(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 615
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$500(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$702(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;Z)Z

    .line 617
    return-void
.end method
