.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PhoneContactChangeRegionDialog.java"


# instance fields
.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private addPhoneNumberDialog:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

.field private allRegions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private currentRegions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private regionListView:Landroid/widget/ListView;

.field private regionSearchEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "addPhoneNumberDialog"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    .prologue
    .line 33
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getCountryNames()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->allRegions:Ljava/util/ArrayList;

    .line 34
    const v0, 0x7f0301cc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->setContentView(I)V

    .line 35
    const v0, 0x7f0e093f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 37
    const v0, 0x7f0e0949

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionListView:Landroid/widget/ListView;

    .line 38
    const v0, 0x7f0e0947

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionSearchEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->addPhoneNumberDialog:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->currentRegions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->currentRegions:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->addPhoneNumberDialog:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionSearchEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->adapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;
    .param p1, "x1"    # Landroid/widget/ArrayAdapter;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->adapter:Landroid/widget/ArrayAdapter;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->allRegions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionListView:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 115
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->dismiss()V

    .line 110
    return-void
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsChangeRegionView()V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->allRegions:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->currentRegions:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090003

    const v3, 0x1020014

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->currentRegions:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->adapter:Landroid/widget/ArrayAdapter;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionListView:Landroid/widget/ListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionSearchEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->regionListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 99
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 104
    return-void
.end method
