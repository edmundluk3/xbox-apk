.class public final Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
.super Lcom/microsoft/xbox/service/model/FollowersData;
.source "FriendSelectorItem.java"


# static fields
.field private static final serialVersionUID:J = 0x507b6988af949efeL


# instance fields
.field private enable:Z

.field private selected:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 1
    .param p1, "friend"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->selected:Z

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 2
    .param p1, "profileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    .line 38
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/UserProfileData;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->xuid:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerScore:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAppDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->appDisplayName:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAccountTier()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->accountTier:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 2
    .param p1, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    .line 24
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/UserProfileData;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->xuid:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 35
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isDummy"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Z)V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 91
    goto :goto_0

    .line 92
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 93
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 94
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 95
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 96
    :cond_4
    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 97
    goto :goto_0

    .line 98
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 99
    goto :goto_0
.end method

.method public getIsEnable()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    return v0
.end method

.method public getIsSelected()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->selected:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 79
    const/16 v0, 0x1f

    .line 80
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 81
    .local v1, "result":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 82
    return v1

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->enable:Z

    .line 63
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->selected:Z

    .line 75
    return-void
.end method

.method public toggleSelection()V
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->selected:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->selected:Z

    .line 59
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
