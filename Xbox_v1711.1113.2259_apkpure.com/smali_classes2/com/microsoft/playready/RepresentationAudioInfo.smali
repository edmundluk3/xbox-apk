.class public final Lcom/microsoft/playready/RepresentationAudioInfo;
.super Ljava/lang/Object;
.source "RepresentationAudioInfo.java"

# interfaces
.implements Lcom/microsoft/playready/AudioInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mRepIndex:I

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;II)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I
    .param p3, "repIndex"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 45
    iput p2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    .line 46
    iput p3, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    .line 47
    return-void
.end method


# virtual methods
.method public getAudioTag()I
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_25(II)I

    move-result v0

    return v0
.end method

.method public getBitPerSample()I
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_23(II)I

    move-result v0

    return v0
.end method

.method public getChannels()I
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_22(II)I

    move-result v0

    return v0
.end method

.method public getPacketSize()I
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_24(II)I

    move-result v0

    return v0
.end method

.method public getProfile()I
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_26(II)I

    move-result v0

    return v0
.end method

.method public getSamplingRate()I
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationAudioInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_21(II)I

    move-result v0

    return v0
.end method
