.class public abstract Lcom/microsoft/xbox/presentation/base/MviPresenter;
.super Ljava/lang/Object;
.source "MviPresenter.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;,
        Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "VI::",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "VS:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final hasBoundViewIntents:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final hasBoundViewState:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final hasCalledInitialSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final viewHasBeenDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private viewIntentsBinder:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder",
            "<TV;TVI;>;"
        }
    .end annotation
.end field

.field private viewIntentsDisposable:Lio/reactivex/disposables/Disposable;

.field private final viewIntentsSubject:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<TVI;>;"
        }
    .end annotation
.end field

.field private viewRelayConsumerDisposable:Lio/reactivex/disposables/Disposable;

.field private viewStateConsumer:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer",
            "<TV;TVS;>;"
        }
    .end annotation
.end field

.field private viewStateDisposable:Lio/reactivex/disposables/Disposable;

.field private final viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<TVS;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->TAG:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewHasBeenDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasCalledInitialSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewState:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewIntents:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 71
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsSubject:Lio/reactivex/subjects/PublishSubject;

    .line 81
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "initialViewState":Ljava/lang/Object;, "TVS;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->TAG:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewHasBeenDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasCalledInitialSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 67
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewState:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewIntents:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 71
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsSubject:Lio/reactivex/subjects/PublishSubject;

    .line 85
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 86
    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 87
    return-void
.end method

.method private disposeSubscriptions(Z)V
    .locals 3
    .param p1, "isDestroy"    # Z

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    const/4 v2, 0x0

    .line 136
    if-eqz p1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewHasBeenDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 141
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateDisposable:Lio/reactivex/disposables/Disposable;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewRelayConsumerDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewRelayConsumerDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 147
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewRelayConsumerDisposable:Lio/reactivex/disposables/Disposable;

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 152
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsDisposable:Lio/reactivex/disposables/Disposable;

    .line 154
    :cond_2
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/presentation/base/MviPresenter;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V
    .locals 1
    .param p1, "viewIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logIntent(Ljava/lang/String;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/presentation/base/MviPresenter;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "view"    # Ljava/lang/Object;
    .param p2, "vs"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateConsumer:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;->accept(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected final bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder",
            "<TV;TVI;>;)",
            "Lio/reactivex/Observable",
            "<TVI;>;"
        }
    .end annotation

    .prologue
    .line 157
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "binder":Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;, "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder<TV;TVI;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewIntents:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsBinder:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsSubject:Lio/reactivex/subjects/PublishSubject;

    return-object v0

    .line 160
    :cond_0
    const-string v0, "Attempted to bind view intents twice"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V
    .locals 3
    .param p1    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable",
            "<TVS;>;",
            "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer",
            "<TV;TVS;>;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "viewStateObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<TVS;>;"
    .local p2, "consumer":Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;, "Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer<TV;TVS;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 169
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasBoundViewState:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 173
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 175
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateConsumer:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    .line 177
    new-instance v0, Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;-><init>(Lcom/jakewharton/rxrelay2/Relay;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribeWith(Lio/reactivex/Observer;)Lio/reactivex/Observer;

    move-result-object v0

    check-cast v0, Lio/reactivex/disposables/Disposable;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateDisposable:Lio/reactivex/disposables/Disposable;

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    const-string v0, "Attempted to bind view state twice"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract initialSetup()V
.end method

.method public onDestroy(Ljava/lang/Object;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "view":Ljava/lang/Object;, "TV;"
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;->disposeSubscriptions(Z)V

    .line 133
    return-void
.end method

.method public onStart(Ljava/lang/Object;)V
    .locals 4
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "view":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewHasBeenDestroyed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot be started after a call to onDestroy"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->hasCalledInitialSetup:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;->initialSetup()V

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsBinder:Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;->bind(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    .local v0, "intent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<+TVI;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/base/MviPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/rx/DisposableSubjectObserver;-><init>(Lio/reactivex/subjects/Subject;)V

    .line 117
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeWith(Lio/reactivex/Observer;)Lio/reactivex/Observer;

    move-result-object v1

    check-cast v1, Lio/reactivex/disposables/Disposable;

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewIntentsDisposable:Lio/reactivex/disposables/Disposable;

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/base/MviPresenter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/base/MviPresenter;Ljava/lang/Object;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 120
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/base/MviPresenter;->viewRelayConsumerDisposable:Lio/reactivex/disposables/Disposable;

    .line 121
    return-void
.end method

.method public onStop(Ljava/lang/Object;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "this":Lcom/microsoft/xbox/presentation/base/MviPresenter;, "Lcom/microsoft/xbox/presentation/base/MviPresenter<TV;TVI;TVS;>;"
    .local p1, "view":Ljava/lang/Object;, "TV;"
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;->disposeSubscriptions(Z)V

    .line 127
    return-void
.end method
