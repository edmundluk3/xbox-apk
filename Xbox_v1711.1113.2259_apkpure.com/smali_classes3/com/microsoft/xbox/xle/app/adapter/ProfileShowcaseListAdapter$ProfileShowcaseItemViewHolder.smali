.class public Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ProfileShowcaseListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfileShowcaseItemViewHolder"
.end annotation


# instance fields
.field public commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public gameNameView:Landroid/widget/TextView;

.field public gamerTag:Landroid/widget/TextView;

.field public likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

.field public likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field private overlay:Landroid/view/View;

.field public playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public realname:Landroid/widget/TextView;

.field public shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public showcaseItemTitleView:Landroid/widget/TextView;

.field public showcasePlayImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public socialBarValuesContainer:Landroid/view/View;

.field public timesViewedCountView:Landroid/widget/TextView;

.field private useLargeIcon:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 214
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->useLargeIcon:Z

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->hookControls()V

    .line 216
    return-void
.end method

.method private hookControls()V
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->itemView:Landroid/view/View;

    .line 320
    .local v0, "parentView":Landroid/view/View;
    const v1, 0x7f0e064a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseItemTitleView:Landroid/widget/TextView;

    .line 321
    const v1, 0x7f0e0983

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 322
    const v1, 0x7f0e064c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->timesViewedCountView:Landroid/widget/TextView;

    .line 323
    const v1, 0x7f0e064b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->gameNameView:Landroid/widget/TextView;

    .line 324
    const v1, 0x7f0e0985

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 326
    const v1, 0x7f0e0192

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/LikeControl;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    .line 327
    const v1, 0x7f0e0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 328
    const v1, 0x7f0e0195

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 329
    const v1, 0x7f0e0197

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 330
    const v1, 0x7f0e0986

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->socialBarValuesContainer:Landroid/view/View;

    .line 331
    const v1, 0x7f0e0194

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 332
    const v1, 0x7f0e0196

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 333
    const v1, 0x7f0e0649

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->realname:Landroid/widget/TextView;

    .line 334
    const v1, 0x7f0e0648

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->gamerTag:Landroid/widget/TextView;

    .line 335
    const v1, 0x7f0e0199

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->deleteButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 337
    const v1, 0x7f0e0984

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->overlay:Landroid/view/View;

    .line 338
    return-void
.end method


# virtual methods
.method public setLikeClickListener(Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 2
    .param p1, "likeControlClick"    # Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;
    .param p2, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 341
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    :cond_0
    return-void
.end method

.method public setUseLargeIcon(Z)V
    .locals 0
    .param p1, "useLarge"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->useLargeIcon:Z

    .line 220
    return-void
.end method

.method public updateWithData(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)V
    .locals 13
    .param p1, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p2, "gameName"    # Ljava/lang/String;

    .prologue
    .line 223
    if-eqz p1, :cond_8

    .line 224
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseItemTitleView:Landroid/widget/TextView;

    if-eqz v8, :cond_1

    .line 225
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->userCaption:Ljava/lang/String;

    .line 226
    .local v0, "clipCaption":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 227
    :cond_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseItemTitleView:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    .end local v0    # "clipCaption":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v8, :cond_2

    .line 235
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->thumbnails:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 236
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->thumbnails:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    iget-object v8, v8, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    const v10, 0x7f020127

    const v11, 0x7f020127

    invoke-virtual {v9, v8, v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 240
    :cond_2
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->timesViewedCountView:Landroid/widget/TextView;

    if-eqz v8, :cond_3

    .line 241
    iget v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->views:I

    if-lez v8, :cond_b

    .line 242
    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->views:I

    const/4 v10, 0x1

    if-ne v8, v10, :cond_a

    const v8, 0x7f0705f3

    :goto_1
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 243
    .local v2, "countString":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->timesViewedCountView:Landroid/widget/TextView;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->views:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v2, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->timesViewedCountView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    .end local v2    # "countString":Ljava/lang/String;
    :cond_3
    :goto_2
    if-eqz p2, :cond_c

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getGamerTag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 250
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->gamerTag:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getGamerTag()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 254
    :goto_3
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->capturerRealName:Ljava/lang/String;

    .line 255
    .local v5, "realName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 256
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->realname:Landroid/widget/TextView;

    invoke-static {v8, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 259
    :cond_4
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v8, :cond_6

    .line 260
    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->useLargeIcon:Z

    if-eqz v8, :cond_d

    const v8, 0x7f0904a9

    :goto_4
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 261
    .local v7, "size":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 262
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iput v7, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 263
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iput v7, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 266
    :cond_5
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 267
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v9, 0x7f070ee5

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 273
    .end local v7    # "size":I
    :cond_6
    :goto_5
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    if-eqz v8, :cond_7

    .line 275
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v8, :cond_f

    .line 276
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/ui/LikeControl;->enable()V

    .line 277
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    iget-object v9, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v9, v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setLikeState(Z)V

    .line 283
    :cond_7
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    .local v4, "narrationContentBuilder":Ljava/lang/StringBuilder;
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->titleName:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x2c

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 286
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v9, 0x7f070d3f

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :goto_7
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->overlay:Landroid/view/View;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 292
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 293
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 295
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v8

    if-eqz v8, :cond_11

    const v8, 0x7f0c0132

    :goto_8
    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 296
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v8

    if-eqz v8, :cond_12

    const v8, 0x7f0c0132

    :goto_9
    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 298
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v8, :cond_13

    const/4 v3, 0x0

    .line 299
    .local v3, "likeCount":I
    :goto_a
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v8, v3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 300
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    const/4 v8, 0x1

    if-le v3, v8, :cond_14

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700d9

    .line 301
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 300
    :goto_b
    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 303
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v8, :cond_15

    const/4 v1, 0x0

    .line 304
    .local v1, "commentCount":I
    :goto_c
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v8, v1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 305
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->commentsValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    const/4 v8, 0x1

    if-le v1, v8, :cond_16

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b8

    .line 306
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 305
    :goto_d
    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 308
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-nez v8, :cond_17

    const/4 v6, 0x0

    .line 309
    .local v6, "shareCount":I
    :goto_e
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    invoke-static {v8, v6}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V

    .line 310
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->sharesValueView:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    const/4 v8, 0x1

    if-le v6, v8, :cond_18

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700e9

    .line 311
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 310
    :goto_f
    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 315
    .end local v1    # "commentCount":I
    .end local v3    # "likeCount":I
    .end local v4    # "narrationContentBuilder":Ljava/lang/StringBuilder;
    .end local v5    # "realName":Ljava/lang/String;
    .end local v6    # "shareCount":I
    :cond_8
    return-void

    .line 229
    .restart local v0    # "clipCaption":Ljava/lang/String;
    :cond_9
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseItemTitleView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->showcaseItemTitleView:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 242
    .end local v0    # "clipCaption":Ljava/lang/String;
    :cond_a
    const v8, 0x7f0705f4

    goto/16 :goto_1

    .line 246
    :cond_b
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->timesViewedCountView:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 252
    :cond_c
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->gameNameView:Landroid/widget/TextView;

    iget-object v9, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->titleName:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 260
    .restart local v5    # "realName":Ljava/lang/String;
    :cond_d
    const v8, 0x7f0904aa

    goto/16 :goto_4

    .line 269
    .restart local v7    # "size":I
    :cond_e
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->playImageView:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v9, 0x7f070fb8

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    goto/16 :goto_5

    .line 279
    .end local v7    # "size":I
    :cond_f
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter$ProfileShowcaseItemViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/ui/LikeControl;->disable()V

    goto/16 :goto_6

    .line 288
    .restart local v4    # "narrationContentBuilder":Ljava/lang/StringBuilder;
    :cond_10
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v9, 0x7f070d3d

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 295
    :cond_11
    const v8, 0x7f0c0131

    goto/16 :goto_8

    .line 296
    :cond_12
    const v8, 0x7f0c0131

    goto/16 :goto_9

    .line 298
    :cond_13
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v3, v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto/16 :goto_a

    .line 301
    .restart local v3    # "likeCount":I
    :cond_14
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b6

    .line 302
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_b

    .line 303
    :cond_15
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v1, v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    goto/16 :goto_c

    .line 306
    .restart local v1    # "commentCount":I
    :cond_16
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b5

    .line 307
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_d

    .line 308
    :cond_17
    iget-object v8, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v6, v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    goto/16 :goto_e

    .line 311
    .restart local v6    # "shareCount":I
    :cond_18
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b7

    .line 312
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_f
.end method
