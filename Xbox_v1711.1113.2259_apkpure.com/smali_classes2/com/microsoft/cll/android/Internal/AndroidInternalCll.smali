.class public Lcom/microsoft/cll/android/Internal/AndroidInternalCll;
.super Lcom/microsoft/cll/android/AndroidCll;
.source "AndroidInternalCll.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final logger:Lcom/microsoft/cll/android/ILogger;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1, "iKey"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/cll/android/AndroidCll;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 20
    const-string v0, "AndroidInternalCll"

    iput-object v0, p0, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->TAG:Ljava/lang/String;

    .line 21
    invoke-static {}, Lcom/microsoft/cll/android/AndroidLogger;->getInstance()Lcom/microsoft/cll/android/ILogger;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 24
    return-void
.end method


# virtual methods
.method protected createPreSerializedEvent(LMicrosoft/Telemetry/Base;)Lcom/microsoft/cll/android/PreSerializedEvent;
    .locals 5
    .param p1, "event"    # LMicrosoft/Telemetry/Base;

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getPartBName(LMicrosoft/Telemetry/Base;)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "partBName":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getPartCName(LMicrosoft/Telemetry/Base;)Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "partCName":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->getAttributes(LMicrosoft/Telemetry/Base;)Ljava/util/Map;

    move-result-object v0

    .line 59
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    invoke-virtual {p1, v2}, LMicrosoft/Telemetry/Base;->setBaseType(Ljava/lang/String;)V

    .line 63
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->serialize(LMicrosoft/Telemetry/Base;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "data":Ljava/lang/String;
    new-instance v4, Lcom/microsoft/cll/android/PreSerializedEvent;

    invoke-direct {v4, v1, v3, v2, v0}, Lcom/microsoft/cll/android/PreSerializedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v4
.end method

.method protected createPreSerializedEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/cll/android/PreSerializedEvent;
    .locals 3
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "eventName"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Lcom/microsoft/cll/android/PreSerializedEvent;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/microsoft/cll/android/PreSerializedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-object v0
.end method

.method protected getAttributes(LMicrosoft/Telemetry/Base;)Ljava/util/Map;
    .locals 3
    .param p1, "event"    # LMicrosoft/Telemetry/Base;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LMicrosoft/Telemetry/Base;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 86
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, LMicrosoft/Telemetry/Base;->getSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/SchemaDef;->getStructs()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/bond/StructDef;

    invoke-virtual {v1}, Lcom/microsoft/bond/StructDef;->getMetadata()Lcom/microsoft/bond/Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Metadata;->getAttributes()Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 87
    return-object v0
.end method

.method protected getPartBName(LMicrosoft/Telemetry/Base;)Ljava/lang/String;
    .locals 5
    .param p1, "event"    # LMicrosoft/Telemetry/Base;

    .prologue
    .line 74
    const-string v1, ""

    .line 76
    .local v1, "partBName":Ljava/lang/String;
    :try_start_0
    check-cast p1, LMicrosoft/Telemetry/Data;

    .end local p1    # "event":LMicrosoft/Telemetry/Base;
    invoke-virtual {p1}, LMicrosoft/Telemetry/Data;->getBaseData()Lcom/microsoft/bond/BondSerializable;

    move-result-object v2

    check-cast v2, LMicrosoft/Telemetry/Domain;

    invoke-virtual {v2}, LMicrosoft/Telemetry/Domain;->getSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/SchemaDef;->getStructs()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/bond/StructDef;

    invoke-virtual {v2}, Lcom/microsoft/bond/StructDef;->getMetadata()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Metadata;->getQualified_name()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 81
    :goto_0
    return-object v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/ClassCastException;
    iget-object v2, p0, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "AndroidInternalCll"

    const-string v4, "This event doesn\'t extend data"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getPartCName(LMicrosoft/Telemetry/Base;)Ljava/lang/String;
    .locals 3
    .param p1, "event"    # LMicrosoft/Telemetry/Base;

    .prologue
    .line 69
    invoke-virtual {p1}, LMicrosoft/Telemetry/Base;->getSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/SchemaDef;->getStructs()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/bond/StructDef;

    invoke-virtual {v1}, Lcom/microsoft/bond/StructDef;->getMetadata()Lcom/microsoft/bond/Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Metadata;->getQualified_name()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "partCName":Ljava/lang/String;
    return-object v0
.end method

.method public varargs log(LMicrosoft/Telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "event"    # LMicrosoft/Telemetry/Base;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LMicrosoft/Telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->createPreSerializedEvent(LMicrosoft/Telemetry/Base;)Lcom/microsoft/cll/android/PreSerializedEvent;

    move-result-object v0

    .line 33
    .local v0, "preSerializedEvent":Lcom/microsoft/cll/android/PreSerializedEvent;
    invoke-super {p0, v0, p2, p3}, Lcom/microsoft/cll/android/AndroidCll;->log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 34
    return-void
.end method

.method public varargs log(LMicrosoft/Telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 2
    .param p1, "event"    # LMicrosoft/Telemetry/Base;
    .param p2, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->createPreSerializedEvent(LMicrosoft/Telemetry/Base;)Lcom/microsoft/cll/android/PreSerializedEvent;

    move-result-object v0

    .line 28
    .local v0, "preSerializedEvent":Lcom/microsoft/cll/android/PreSerializedEvent;
    const/4 v1, 0x0

    invoke-super {p0, v0, v1, p2}, Lcom/microsoft/cll/android/AndroidCll;->log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 29
    return-void
.end method

.method public varargs log(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 4
    .param p1, "serializedEvent"    # Ljava/lang/String;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p4, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p3, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "."

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "AndroidInternalCll"

    const-string v3, "Event Name does not follow a valid format. Your event must have at least one . between two words. E.g. Microsoft.MyEvent"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->createPreSerializedEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/cll/android/PreSerializedEvent;

    move-result-object v0

    .line 47
    .local v0, "preSerializedEvent":Lcom/microsoft/cll/android/PreSerializedEvent;
    invoke-super {p0, v0, p3, p4}, Lcom/microsoft/cll/android/AndroidCll;->log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    goto :goto_0
.end method

.method public varargs log(Ljava/lang/String;Ljava/lang/String;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 1
    .param p1, "serializedEvent"    # Ljava/lang/String;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p3, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->log(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 38
    return-void
.end method

.method protected serialize(LMicrosoft/Telemetry/Base;)Ljava/lang/String;
    .locals 2
    .param p1, "event"    # LMicrosoft/Telemetry/Base;

    .prologue
    .line 91
    new-instance v0, Lcom/microsoft/cll/android/Internal/BondJsonSerializer;

    iget-object v1, p0, Lcom/microsoft/cll/android/Internal/AndroidInternalCll;->logger:Lcom/microsoft/cll/android/ILogger;

    invoke-direct {v0, v1}, Lcom/microsoft/cll/android/Internal/BondJsonSerializer;-><init>(Lcom/microsoft/cll/android/ILogger;)V

    .line 92
    .local v0, "bondJsonSerializer":Lcom/microsoft/cll/android/Internal/BondJsonSerializer;
    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/Internal/BondJsonSerializer;->serialize(Lcom/microsoft/bond/BondSerializable;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
