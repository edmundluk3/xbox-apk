.class Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubPresenceHeartbeat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadClubPresenceAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private final clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field private final delay:J

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;J)V
    .locals 0
    .param p2, "clubModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .param p3, "clubPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .param p4, "delay"    # J

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 72
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 73
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 74
    iput-wide p4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->delay:J

    .line 75
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 94
    :try_start_0
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->delay:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 95
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->delay:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 98
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->cancelled:Z

    if-nez v0, :cond_1

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Heartbeat"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updatePresence(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 108
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v0

    .line 108
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->clubPresenceState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->cancelled:Z

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 65
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method
