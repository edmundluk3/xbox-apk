.class public Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
.super Ljava/lang/Object;
.source "TokenAndSignatureResult.java"


# instance fields
.field private final errorCode:I

.field private final errorMessage:Ljava/lang/String;

.field private final httpStatusCode:I

.field private final token:Lcom/microsoft/xbox/idp/interop/TokenAndSignature;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/idp/interop/TokenAndSignature;)V
    .locals 2
    .param p1, "token"    # Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    .prologue
    const/4 v1, 0x0

    .line 16
    const-string v0, ""

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;-><init>(Lcom/microsoft/xbox/idp/interop/TokenAndSignature;IILjava/lang/String;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/idp/interop/TokenAndSignature;IILjava/lang/String;)V
    .locals 0
    .param p1, "token"    # Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    .param p2, "httpStatusCode"    # I
    .param p3, "errorCode"    # I
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->token:Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    .line 21
    iput p2, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->httpStatusCode:I

    .line 22
    iput p3, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->errorCode:I

    .line 23
    iput-object p4, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->errorMessage:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->errorCode:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpStatusCode()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->httpStatusCode:I

    return v0
.end method

.method public getToken()Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->token:Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    return-object v0
.end method
