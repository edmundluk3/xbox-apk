.class public final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerCustomProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerLatency;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionDetailsRequest;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotification;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgNotificationBody;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponseItem;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstantsSystem;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberConstants;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberRoles;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionPropertiesSystem;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchHandleVisibility;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RemoveMultiplayerMemberRequest;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstantsSystem;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionConstants;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerPropertiesCustom;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10ServerProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$Q10Server;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputeProperties;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServerCloudCompute;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerServers;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MembersInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ConfirmedRole;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerRoles;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$LfgRoleInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionDescription;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleParameters;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerPeople;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionMembers;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleFilters;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleFilterNumberComparison;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerQueryResponse;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMoniker;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleFilterNumberComparisonType;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionRestriction;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandleType;,
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;
    }
.end annotation


# static fields
.field public static final transient PARTY_SCID:Ljava/lang/String; = "7492BACA-C1B4-440D-A391-B7EF364A8D40"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
