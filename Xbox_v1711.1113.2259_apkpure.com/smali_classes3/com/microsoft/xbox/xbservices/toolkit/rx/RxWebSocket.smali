.class public Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;
.super Lio/reactivex/Observable;
.source "RxWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private client:Lokhttp3/OkHttpClient;

.field private final logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private outgoingMessages:Lio/reactivex/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/ReplaySubject",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private socket:Lokhttp3/WebSocket;

.field private final subProtocol:Ljava/lang/String;

.field private final telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

.field private final webSocketEndpoint:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lokhttp3/OkHttpClient;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V
    .locals 1
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "webSocketEndpoint"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "subProtocol"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "telemetryProvider"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    invoke-static {p2}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 51
    invoke-static {p3}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 52
    invoke-static {p4}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    invoke-static {p5}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->client:Lokhttp3/OkHttpClient;

    .line 56
    invoke-static {}, Lio/reactivex/subjects/ReplaySubject;->create()Lio/reactivex/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    .line 57
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->webSocketEndpoint:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->subProtocol:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 60
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    .line 61
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lio/reactivex/subjects/ReplaySubject;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;)Lokhttp3/WebSocket;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    return-object v0
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendData(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 125
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->outgoingMessages:Lio/reactivex/subjects/ReplaySubject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/ReplaySubject;->onNext(Ljava/lang/Object;)V

    .line 127
    return-void
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->client:Lokhttp3/OkHttpClient;

    new-instance v1, Lokhttp3/Request$Builder;

    invoke-direct {v1}, Lokhttp3/Request$Builder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->webSocketEndpoint:Ljava/lang/String;

    .line 71
    invoke-virtual {v1, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    const-string v2, "Sec-WebSocket-Protocol"

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->subProtocol:Ljava/lang/String;

    .line 72
    invoke-virtual {v1, v2, v3}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket$1;-><init>(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;Lio/reactivex/Observer;)V

    .line 69
    invoke-virtual {v0, v1, v2}, Lokhttp3/OkHttpClient;->newWebSocket(Lokhttp3/Request;Lokhttp3/WebSocketListener;)Lokhttp3/WebSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->socket:Lokhttp3/WebSocket;

    .line 115
    return-void
.end method
