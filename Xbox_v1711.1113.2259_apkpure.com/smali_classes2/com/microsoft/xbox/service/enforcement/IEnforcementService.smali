.class public interface abstract Lcom/microsoft/xbox/service/enforcement/IEnforcementService;
.super Ljava/lang/Object;
.source "IEnforcementService.java"


# virtual methods
.method public abstract sendReport(Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)Z
    .param p1    # Lcom/microsoft/xbox/service/model/sls/ReportUserData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
