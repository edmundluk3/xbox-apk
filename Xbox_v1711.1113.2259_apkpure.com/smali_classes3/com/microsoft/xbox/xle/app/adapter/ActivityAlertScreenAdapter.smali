.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ActivityAlertScreenAdapter.java"


# instance fields
.field private activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

.field private items:[Ljava/lang/Object;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

.field private listView:Landroid/widget/ListView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    .line 31
    const v0, 0x7f0e0124

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    const v0, 0x7f0e0125

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030022

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 66
    return-void
.end method

.method protected restoreListPosition()V
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 110
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListOffset()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/widget/ListView;II)V

    .line 113
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->isBusy()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getOrderedActivityAlertList()[Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->items:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getOrderedActivityAlertList()[Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_3

    .line 75
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->activityAlertViewModel:Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->getOrderedActivityAlertList()[Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->items:[Ljava/lang/Object;

    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "notify":Z
    const/4 v1, 0x0

    .line 80
    .local v1, "restore":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->clear()V

    .line 82
    const/4 v0, 0x1

    .line 85
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->items:[Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 86
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->items:[Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->addAll([Ljava/lang/Object;)V

    .line 87
    const/4 v0, 0x1

    .line 88
    const/4 v1, 0x1

    .line 90
    :cond_1
    if-eqz v0, :cond_2

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->notifyDataSetChanged()V

    .line 93
    :cond_2
    if-eqz v1, :cond_3

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->restoreListPosition()V

    .line 98
    .end local v0    # "notify":Z
    .end local v1    # "restore":Z
    :cond_3
    return-void
.end method
