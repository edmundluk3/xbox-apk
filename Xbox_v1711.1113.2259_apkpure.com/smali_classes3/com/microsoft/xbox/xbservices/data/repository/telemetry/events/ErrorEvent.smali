.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;
.super Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
.source "ErrorEvent.java"


# instance fields
.field private error:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field private exception:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V
    .locals 1
    .param p1, "error"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 14
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;-><init>(Ljava/lang/String;)V

    .line 10
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatGeneric:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->error:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->error:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 16
    return-void
.end method


# virtual methods
.method public getError()Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->error:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    return-object v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->exception:Ljava/lang/Exception;

    return-object v0
.end method

.method public setException(Ljava/lang/Exception;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->exception:Ljava/lang/Exception;

    .line 25
    return-object p0
.end method

.method public setException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 30
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->exception:Ljava/lang/Exception;

    .line 31
    return-object p0
.end method
