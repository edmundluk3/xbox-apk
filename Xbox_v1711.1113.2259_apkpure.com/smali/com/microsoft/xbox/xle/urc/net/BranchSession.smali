.class public Lcom/microsoft/xbox/xle/urc/net/BranchSession;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;
.implements Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;,
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;,
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;,
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;,
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;,
        Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    }
.end annotation


# static fields
.field public static final BUTTON_CHANNEL_DOWN:Ljava/lang/String; = "btn.ch_down"

.field public static final BUTTON_CHANNEL_LAST:Ljava/lang/String; = "btn.last"

.field public static final BUTTON_CHANNEL_UP:Ljava/lang/String; = "btn.ch_up"

.field public static final BUTTON_COMMAND_KEY:Ljava/lang/String; = "button_command"

.field public static final BUTTON_FAST_FORWARD:Ljava/lang/String; = "btn.fast_fwd"

.field public static final BUTTON_PAUSE:Ljava/lang/String; = "btn.pause"

.field public static final BUTTON_PLAY:Ljava/lang/String; = "btn.play"

.field public static final BUTTON_POWER_OFF:Ljava/lang/String; = "btn.power_off"

.field public static final BUTTON_POWER_ON:Ljava/lang/String; = "btn.power_on"

.field public static final BUTTON_POWER_TOGGLE:Ljava/lang/String; = "btn.power"

.field public static final BUTTON_REWIND:Ljava/lang/String; = "btn.rewind"

.field public static final BUTTON_SEEK:Ljava/lang/String; = "btn.seek"

.field public static final BUTTON_VOLUME_DOWN:Ljava/lang/String; = "btn.vol_down"

.field public static final BUTTON_VOLUME_MUTE:Ljava/lang/String; = "btn.vol_mute"

.field public static final BUTTON_VOLUME_UP:Ljava/lang/String; = "btn.vol_up"

.field private static instance:Lcom/microsoft/xbox/xle/urc/net/BranchSession;


# instance fields
.field private final appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;",
            ">;"
        }
    .end annotation
.end field

.field private final connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

.field private final context:Landroid/content/Context;

.field private devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

.field private error:Ljava/lang/String;

.field private headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

.field private final integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

.field private isInHdmiMode:Z

.field private isVolumeEnabled:Z

.field private final listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainHandler:Landroid/os/Handler;

.field private final parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

.field private state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field private final streamListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;",
            ">;"
        }
    .end annotation
.end field

.field private final tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;",
            ">;"
        }
    .end annotation
.end field

.field private final tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->instance:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;Landroid/content/Context;)V
    .locals 2
    .param p1, "connection"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;
    .param p2, "integration"    # Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isInHdmiMode:Z

    .line 64
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->error:Ljava/lang/String;

    .line 73
    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled:Z

    .line 76
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 78
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 79
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 80
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 81
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 84
    iput-object p3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->context:Landroid/content/Context;

    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    .line 86
    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;-><init>(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->setListener(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 96
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->open()Z

    .line 98
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .param p1, "x1"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->setState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->instance:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    return-object v0
.end method

.method public static setInstance(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)V
    .locals 0
    .param p0, "session"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 105
    sput-object p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->instance:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .line 106
    return-void
.end method

.method private setState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
    .locals 5
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .prologue
    .line 616
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v2, p1, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 621
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->error:Ljava/lang/String;

    .line 622
    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    if-eqz v2, :cond_2

    .line 623
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->getLastError()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->error:Ljava/lang/String;

    .line 626
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .line 627
    .local v0, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->error:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V

    goto :goto_1

    .line 631
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq p1, v2, :cond_4

    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p1, v2, :cond_0

    .line 632
    :cond_4
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-eqz v2, :cond_6

    :cond_5
    const/4 v1, 0x1

    .line 633
    .local v1, "needToNotify":Z
    :goto_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v3, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;

    invoke-direct {v3, p0, v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$17;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 632
    .end local v1    # "needToNotify":Z
    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 146
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 170
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getConnectionError()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 178
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 154
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 162
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->close()V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->setListener(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 200
    return-void
.end method

.method public getConnectionError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->error:Ljava/lang/String;

    return-object v0
.end method

.method public getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    return-object v0
.end method

.method public getHeadend()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->headend:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    return-object v0
.end method

.method public getIsInHdmiMode()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isInHdmiMode:Z

    return v0
.end method

.method public getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    return-object v0
.end method

.method public isVolumeEnabled()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled:Z

    return v0
.end method

.method public onAppChannelDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "providerID"    # Ljava/lang/String;
    .param p3, "channelID"    # Ljava/lang/String;

    .prologue
    .line 485
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$9;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$9;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 502
    return-void
.end method

.method public onAppChannelLineupsReceived(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 480
    return-void
.end method

.method public onAppChannelProgramDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "providerID"    # Ljava/lang/String;
    .param p3, "programID"    # Ljava/lang/String;

    .prologue
    .line 506
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$10;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 523
    return-void
.end method

.method public onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 2
    .param p1, "_devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 369
    return-void
.end method

.method public onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    .prologue
    .line 551
    if-eqz p1, :cond_0

    .line 552
    iget-boolean v0, p1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->isInHdmiMode:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isInHdmiMode:Z

    .line 555
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$13;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$13;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 563
    return-void
.end method

.method public onMessageReceived(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 607
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$16;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$16;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 613
    return-void
.end method

.method public onNotifyChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;
    .param p3, "canUserViewChannel"    # Ljava/lang/String;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$4;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 419
    return-void
.end method

.method public onNotifyChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
    .locals 2
    .param p1, "channelType"    # Ljava/lang/String;
    .param p2, "blockExplicitContentPerShowInfo"    # Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    .prologue
    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 432
    return-void
.end method

.method public onNotifyConfigChanged()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestConfig()Z

    .line 336
    return-void
.end method

.method public onNotifyDeviceUIChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "uiType"    # Ljava/lang/String;

    .prologue
    .line 584
    return-void
.end method

.method public onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 2
    .param p1, "_headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$2;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 392
    return-void
.end method

.method public onNotifyProgramInfoChanged()V
    .locals 0

    .prologue
    .line 580
    return-void
.end method

.method public onNotifyStreamError(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorType"    # Ljava/lang/String;

    .prologue
    .line 436
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 444
    return-void
.end method

.method public onNotifyStreamFormatChanged()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$3;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 405
    return-void
.end method

.method public onNotifyTunerStateChange(Ljava/lang/String;)V
    .locals 2
    .param p1, "playbackState"    # Ljava/lang/String;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$7;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$7;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 456
    return-void
.end method

.method public onStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .prologue
    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$15;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 603
    return-void
.end method

.method public onStreamingStartedReceived(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;

    .prologue
    .line 567
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$14;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$14;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 576
    return-void
.end method

.method public onTunerChannelsReceived([Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V
    .locals 2
    .param p1, "data"    # [Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$12;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$12;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 547
    return-void
.end method

.method public onTvRecentsReceived(Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->mainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$11;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$11;-><init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 535
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->appChannelsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->liveTvInfoListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->listeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 182
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tvRecentsListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 158
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->tunerListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 166
    return-void
.end method

.method public reportMalformedHeadend()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;->reportMalformedHeadend()V

    .line 206
    :cond_0
    return-void
.end method

.method public requestAppChannelData(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "providerID"    # Ljava/lang/String;
    .param p2, "channelID"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 279
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestAppChannelData(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestAppChannelLineups()Z
    .locals 2

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 270
    const/4 v0, 0x0

    .line 272
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestAppChannelLineups()Z

    move-result v0

    goto :goto_0
.end method

.method public requestAppChannelProgramData(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "providerID"    # Ljava/lang/String;
    .param p2, "showID"    # Ljava/lang/String;

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 284
    const/4 v0, 0x0

    .line 286
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestAppChannelProgramData(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestHeadend()Z
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestHeadend()Z

    move-result v0

    goto :goto_0
.end method

.method public requestLiveTvInfo()Z
    .locals 2

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 298
    const/4 v0, 0x0

    .line 300
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestLiveTvInfo()Z

    move-result v0

    goto :goto_0
.end method

.method public requestRecentChannels(II)Z
    .locals 1
    .param p1, "first"    # I
    .param p2, "count"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 263
    :cond_0
    const/4 v0, 0x0

    .line 265
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestRecentChannels(II)Z

    move-result v0

    goto :goto_0
.end method

.method public requestStreamStarted(Ljava/lang/String;)Z
    .locals 2
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 305
    const/4 v0, 0x0

    .line 307
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestStreamingStarted(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestTunerChannels()Z
    .locals 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 293
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->requestTunerChannels()Z

    move-result v0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 185
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    .line 186
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    .line 187
    return-void
.end method

.method public resolveString(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    .local v0, "ret":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_0
    if-nez v0, :cond_1

    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_1
    return-object v0
.end method

.method public sendButton(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "button"    # Ljava/lang/String;

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 2
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "button"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 226
    .local p3, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 237
    :goto_0
    return v0

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    if-eqz v0, :cond_1

    .line 231
    const-string v0, "btn.power"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "btn.power_on"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "btn.power_off"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->integration:Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;

    invoke-interface {v0, p2}, Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;->reportButton(Ljava/lang/String;)V

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    goto :goto_0
.end method

.method public setChannel(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "lineupId"    # Ljava/lang/String;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 242
    const/4 v0, 0x0

    .line 244
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->setChannel(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public setChannelName(Ljava/lang/String;)Z
    .locals 2
    .param p1, "ch_name"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    .line 249
    const/4 v0, 0x0

    .line 251
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->parser:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->setChannelName(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public streamingCanBeEnabled()Z
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getHeadend()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getHeadend()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->streaming_port:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
