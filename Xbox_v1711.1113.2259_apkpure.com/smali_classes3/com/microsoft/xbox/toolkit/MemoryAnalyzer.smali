.class public Lcom/microsoft/xbox/toolkit/MemoryAnalyzer;
.super Ljava/lang/Object;
.source "MemoryAnalyzer.java"


# static fields
.field private static final HPROF_BASE:Ljava/lang/String; = "/sdcard/bishop/hprof/"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dumpHPROF(Ljava/lang/String;)V
    .locals 7
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s%s.hprof"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "/sdcard/bishop/hprof/"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "filename":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v2, "/sdcard/bishop/hprof/"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, "dirobj":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "couldn\'t build directory"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 59
    :cond_0
    const-string v2, "MemoryAnalyzer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Saving..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {v1}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public static getMemoryInfo()Landroid/os/Debug$MemoryInfo;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 44
    .local v0, "info":Landroid/os/Debug$MemoryInfo;
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 46
    return-object v0
.end method

.method public static getTotalMemoryUsed(Landroid/os/Debug$MemoryInfo;Landroid/os/Debug$MemoryInfo;)I
    .locals 6
    .param p0, "before"    # Landroid/os/Debug$MemoryInfo;
    .param p1, "after"    # Landroid/os/Debug$MemoryInfo;

    .prologue
    .line 32
    iget v4, p1, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    iget v5, p0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    sub-int v0, v4, v5

    .line 34
    .local v0, "dalvikKbUsed":I
    iget v4, p0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    iget v5, p0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    add-int/2addr v4, v5

    iget v5, p0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    add-int v2, v4, v5

    .line 35
    .local v2, "totalBefore":I
    iget v4, p1, Landroid/os/Debug$MemoryInfo;->nativePss:I

    iget v5, p1, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    add-int/2addr v4, v5

    iget v5, p1, Landroid/os/Debug$MemoryInfo;->otherPss:I

    add-int v1, v4, v5

    .line 37
    .local v1, "totalAfter":I
    sub-int v3, v1, v2

    .line 38
    .local v3, "totalKbUsed":I
    return v3
.end method
