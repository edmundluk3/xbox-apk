.class public Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;
.super Lcom/microsoft/xbox/presentation/base/BaseRxDialog;
.source "ActivityFeedFilterDialog.java"


# instance fields
.field private filterView:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;

.field telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->init(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;I)V

    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->init(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->init(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;)V

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->requestWindowFeature(I)Z

    .line 42
    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->filterView:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->filterView:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->setContentView(Landroid/view/View;)V

    .line 44
    return-void
.end method

.method static synthetic lambda$onAttachedToWindow$0(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->dismiss()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;->cancel()V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->trackCancelFeedFilter()V

    .line 69
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 5

    .prologue
    .line 48
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;->onAttachedToWindow()V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->trackActivityFeedFilterPageView()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->filterView:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;

    .line 53
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;->viewIntents()Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;

    .line 54
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x64

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 59
    invoke-virtual {v1, v2, v3, v4}, Lio/reactivex/Observable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 61
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 63
    return-void
.end method
