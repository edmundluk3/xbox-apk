.class final Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;
.super Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
.source "AutoValue_PartyInteractor_InitialLoadResult.java"


# instance fields
.field private final memberStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field

.field private final textStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observable;Lio/reactivex/Observable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    .local p2, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null textStream"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->textStream:Lio/reactivex/Observable;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null memberStream"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->memberStream:Lio/reactivex/Observable;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    .line 56
    .local v0, "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->textStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->memberStream:Lio/reactivex/Observable;

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    :cond_3
    move v1, v2

    .line 59
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 64
    const/4 v0, 0x1

    .line 65
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    mul-int/2addr v0, v2

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 69
    return v0
.end method

.method public memberStream()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->memberStream:Lio/reactivex/Observable;

    return-object v0
.end method

.method public textStream()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->textStream:Lio/reactivex/Observable;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitialLoadResult{textStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_InitialLoadResult;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
