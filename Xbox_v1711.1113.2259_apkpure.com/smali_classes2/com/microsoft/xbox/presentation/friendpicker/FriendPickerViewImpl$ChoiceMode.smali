.class public final enum Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;
.super Ljava/lang/Enum;
.source "FriendPickerViewImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChoiceMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

.field public static final enum MULTIPLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

.field public static final enum SINGLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;


# instance fields
.field private final listMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 40
    new-instance v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    const-string v1, "MULTIPLE"

    invoke-direct {v0, v1, v2, v4}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .line 39
    new-array v0, v4, [Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->$VALUES:[Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "listMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->listMode:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->$VALUES:[Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    return-object v0
.end method


# virtual methods
.method public getListMode()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->listMode:I

    return v0
.end method
