.class public Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
.super Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
.source "IProfileShowcaseResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Screenshot"
.end annotation


# instance fields
.field public dateTaken:Ljava/lang/String;

.field public lastModified:Ljava/lang/String;

.field public rating:I

.field public screenshotId:Ljava/lang/String;

.field public screenshotUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsScreenshot()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getItemRoot()Ljava/lang/String;
    .locals 5

    .prologue
    .line 164
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getScreenshotUriFormat()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->scid:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setGamerTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->gamertag:Ljava/lang/String;

    .line 161
    return-void
.end method
