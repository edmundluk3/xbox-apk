.class public final enum Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
.super Ljava/lang/Enum;
.source "GameProgressGameClipsFilter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum CommunityClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum CommunityScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum MyGameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum MyScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field public static final enum MyScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "MyGameClipsRecent"

    const v2, 0x7f07059f

    const-string v3, "MyDVRRecent"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "MyGameClipsSaved"

    const v2, 0x7f0705a0

    const-string v3, "MyDVRSaved"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "CommunityClipsRecent"

    const v2, 0x7f07059b

    const-string v3, "CommunityDVRRecent"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "CommunityClipsSaved"

    const v2, 0x7f07059c

    const-string v3, "CommunityDVRSaved"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "MyScreenshotsRecent"

    const v2, 0x7f0705a1

    const-string v3, "MyScreenshotRecent"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "MyScreenshotsSaved"

    const/4 v2, 0x5

    const v3, 0x7f0705a2

    const-string v4, "MyScreenshotSaved"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "CommunityScreenshotsRecent"

    const/4 v2, 0x6

    const v3, 0x7f07059d

    const-string v4, "CommunityScreenshotRecent"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    const-string v1, "CommunityScreenshotsSaved"

    const/4 v2, 0x7

    const v3, 0x7f07059e

    const-string v4, "CommunityScreenshotSaved"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 14
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->resId:I

    .line 45
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->telemetryName:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .locals 5
    .param p0, "filter"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 30
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 33
    .local v0, "gameClipsFilter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    .end local v0    # "gameClipsFilter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :goto_1
    return-object v0

    .line 32
    .restart local v0    # "gameClipsFilter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v0    # "gameClipsFilter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
