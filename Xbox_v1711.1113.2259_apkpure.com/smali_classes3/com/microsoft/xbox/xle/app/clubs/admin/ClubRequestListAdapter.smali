.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;
.super Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;
.source "ClubRequestListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final HEADER_TYPE:I = 0x7f030061

.field private static final REQUEST_TYPE:I = 0x7f030062


# instance fields
.field private final clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadRecyclerAdapter;-><init>()V

    .line 43
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;

    .line 77
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

    if-eqz v1, :cond_0

    .line 78
    const v1, 0x7f030061

    .line 83
    :goto_0
    return v1

    .line 79
    :cond_0
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    if-eqz v1, :cond_1

    .line 80
    const v1, 0x7f030062

    goto :goto_0

    .line 82
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t determine view type for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 83
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;

    .line 68
    .local v0, "clubRequestListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;
    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 71
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 51
    .local v0, "v":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown viewType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 60
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 53
    :pswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 56
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x7f030061
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
