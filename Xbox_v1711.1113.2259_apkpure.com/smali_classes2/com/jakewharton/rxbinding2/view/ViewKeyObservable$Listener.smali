.class final Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "ViewKeyObservable.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/view/ViewKeyObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private final handled:Lio/reactivex/functions/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Landroid/view/KeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Landroid/view/KeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Lio/reactivex/functions/Predicate;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Landroid/view/KeyEvent;",
            ">;",
            "Lio/reactivex/Observer",
            "<-",
            "Landroid/view/KeyEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "handled":Lio/reactivex/functions/Predicate;, "Lio/reactivex/functions/Predicate<-Landroid/view/KeyEvent;>;"
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Landroid/view/KeyEvent;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->view:Landroid/view/View;

    .line 39
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    .line 40
    iput-object p3, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 41
    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 60
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    invoke-interface {v1, p3}, Lio/reactivex/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, p3}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    const/4 v1, 0x1

    .line 55
    :goto_0
    return v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, v0}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    .line 52
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/view/ViewKeyObservable$Listener;->dispose()V

    .line 55
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
