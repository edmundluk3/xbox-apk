.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
.source "GameProfileAttainmentDetailScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;-><init>()V

    .line 9
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAttainmentDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 10
    return-void
.end method


# virtual methods
.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    .line 15
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onStartOverride()V

    .line 16
    return-void
.end method
