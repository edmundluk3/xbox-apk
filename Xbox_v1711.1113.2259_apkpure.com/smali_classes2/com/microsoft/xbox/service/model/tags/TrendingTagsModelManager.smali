.class public Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "TrendingTagsModelManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field public static INSTANCE:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager; = null

.field private static final MAX_TRENDING_MODEL_COUNT:I = 0xff

.field private static final MAX_TRENDING_TAGS:I = 0x32


# instance fields
.field private trendingTagModels:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 32
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->trendingTagModels:Landroid/util/LruCache;

    .line 33
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->onGetTrendingTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGetTrendingTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;>;"
    const/4 v3, 0x1

    .line 64
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 65
    return-void
.end method


# virtual methods
.method public getTrendingTags(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->getTrendingTagsModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->getTrendingTags()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;->items()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTrendingTagsModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    .locals 2
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->trendingTagModels:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    .line 45
    .local v0, "model":Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;-><init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;Ljava/lang/String;)V

    .line 47
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->trendingTagModels:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_0
    return-object v0
.end method

.method public loadAsync(ZLjava/lang/String;)V
    .locals 5
    .param p1, "forceRefresh"    # Z
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 54
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 56
    if-nez p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->getTrendingTagsModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->getTrendingTagsModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->loadTrendingTagsAsync(Z)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->getTrendingTagsModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {v3, v4, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
