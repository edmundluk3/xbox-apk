.class public abstract Lcom/microsoft/xbox/smartglass/TextManagerListener;
.super Ljava/lang/Object;
.source "TextManagerListener.java"

# interfaces
.implements Ljava/util/EventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 0
    .param p1, "configuration"    # Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .prologue
    .line 16
    return-void
.end method

.method public onTextChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 24
    return-void
.end method

.method public onTextSelected(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 28
    return-void
.end method
