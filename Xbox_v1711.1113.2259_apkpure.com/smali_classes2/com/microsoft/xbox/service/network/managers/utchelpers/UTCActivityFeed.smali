.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;
.super Ljava/lang/Object;
.source "UTCActivityFeed.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackActivityFeedComment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "targetXuid"    # Ljava/lang/String;
    .param p1, "feedType"    # Ljava/lang/String;

    .prologue
    .line 98
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 99
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    const-string v1, "ActivityType"

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    const-string v1, "Activity Feed Comment"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 103
    return-void
.end method

.method public static trackActivityFeedCommentPost(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "targetXuid"    # Ljava/lang/String;
    .param p1, "feedType"    # Ljava/lang/String;

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 110
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    const-string v1, "ActivityType"

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    const-string v1, "Activity Feed Comment Post"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 114
    return-void
.end method

.method public static trackDetailsFilter(I)V
    .locals 3
    .param p0, "position"    # I

    .prologue
    .line 183
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 184
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    packed-switch p0, :pswitch_data_0

    .line 196
    :goto_0
    const-string v1, "Activity Detail - Filter"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 197
    return-void

    .line 186
    :pswitch_0
    const-string v1, "Filter"

    const-string v2, "Like"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :pswitch_1
    const-string v1, "Filter"

    const-string v2, "Comment"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 192
    :pswitch_2
    const-string v1, "Filter"

    const-string v2, "Share"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static trackHideFromFeed()V
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$13;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 245
    return-void
.end method

.method public static trackLike(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "isLiked"    # Z
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "feedType"    # Ljava/lang/String;

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;

    invoke-direct {v0, p1, p2, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$1;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 28
    return-void
.end method

.method public static trackPinToFeed()V
    .locals 1

    .prologue
    .line 221
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$11;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 227
    return-void
.end method

.method public static trackRemovePinFromFeed()V
    .locals 1

    .prologue
    .line 230
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$12;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$12;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 236
    return-void
.end method

.method public static trackShare(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 122
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$8;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 137
    return-void
.end method

.method public static trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "feedType"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 144
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    const-string v1, "ActivityType"

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 147
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 148
    return-void
.end method

.method public static trackShareFeedPost(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "targetXuid"    # Ljava/lang/String;
    .param p1, "feedType"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 155
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 156
    const-string v1, "ActivityType"

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    const-string v1, "Activity Feed Share Feed Post"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 159
    return-void
.end method

.method public static trackShareMessagePost(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 165
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$9;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 177
    return-void
.end method

.method public static trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V
    .locals 1
    .param p0, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 206
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;-><init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 218
    return-void
.end method

.method public static trackSuggestedFriendAdd(Ljava/lang/String;)V
    .locals 1
    .param p0, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$4;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 61
    return-void
.end method

.method public static trackSuggestedFriendRemove(Ljava/lang/String;)V
    .locals 1
    .param p0, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 64
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$5;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 73
    return-void
.end method

.method public static trackSuggestedFriendSeeAll()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$2;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 37
    return-void
.end method

.method public static trackSuggestedFriendViewProfile(Ljava/lang/String;)V
    .locals 1
    .param p0, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$3;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 49
    return-void
.end method

.method public static trackTrendingSeeAll()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 82
    return-void
.end method

.method public static trackUnhideFromFeed()V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$14;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$14;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 254
    return-void
.end method

.method public static trackWebLinkLaunch()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$7;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 91
    return-void
.end method
