.class public interface abstract Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.super Ljava/lang/Object;
.source "BranchSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ISessionListener"
.end annotation


# virtual methods
.method public abstract onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
.end method

.method public abstract onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
.end method

.method public abstract onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
.end method

.method public abstract onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
.end method
