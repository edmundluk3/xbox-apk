.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
.source "AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest.java"


# instance fields
.field private final reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

.field private final reportId:J


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)V
    .locals 3
    .param p1, "reportId"    # J
    .param p3, "reason"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    .line 17
    if-nez p3, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null reason"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p1, p0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;

    .line 49
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;->reportId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    .line 50
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;->reason()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
    :cond_3
    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 57
    const/4 v0, 0x1

    .line 58
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 59
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 60
    mul-int/2addr v0, v8

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 62
    return v0
.end method

.method public reason()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    return-object v0
.end method

.method public reportId()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubDeleteReportRequest{reportId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reportId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubDeleteReportRequest;->reason:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
