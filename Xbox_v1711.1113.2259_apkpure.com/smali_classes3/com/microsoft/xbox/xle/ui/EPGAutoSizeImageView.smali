.class public Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageView;
.source "EPGAutoSizeImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
    }
.end annotation


# instance fields
.field private mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

.field private mInLayout:Z

.field private mLoadHeight:I

.field private mLoadWidth:I

.field private mMaxAspectRatio:F

.field private mSrc:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;)V

    .line 22
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 23
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mMaxAspectRatio:F

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 23
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mMaxAspectRatio:F

    .line 35
    return-void
.end method

.method private static getAspectRatio(Landroid/graphics/drawable/Drawable;)F
    .locals 4
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v2, 0x0

    .line 142
    if-nez p0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v2

    .line 145
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 146
    .local v1, "width":I
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 147
    .local v0, "height":I
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    .line 150
    int-to-float v2, v1

    int-to-float v3, v0

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method private loadImage(II)Z
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v0, 0x0

    .line 108
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadWidth:I

    if-ne p1, v1, :cond_2

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadHeight:I

    if-eq p2, v1, :cond_0

    .line 114
    :cond_2
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadWidth:I

    .line 115
    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadHeight:I

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mSrc:Ljava/lang/String;

    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 119
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getEffectiveAutoSizeMode()Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-eq v1, v2, :cond_0

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 134
    :goto_0
    return-object v1

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 128
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v1, v3, :cond_2

    .line 129
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    goto :goto_0

    .line 131
    :cond_2
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eqz v1, :cond_3

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, v3, :cond_4

    .line 132
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    goto :goto_0

    .line 134
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 52
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mInLayout:Z

    .line 54
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->onMeasure(II)V

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getMeasuredHeight()I

    move-result v1

    .line 57
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getMeasuredWidth()I

    move-result v3

    .line 60
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getEffectiveAutoSizeMode()Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    move-result-object v2

    .line 61
    .local v2, "mode":Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
    sget-object v4, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-ne v2, v4, :cond_3

    .line 62
    int-to-float v4, v1

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mMaxAspectRatio:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 70
    :cond_0
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->loadImage(II)Z

    .line 73
    sget-object v4, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-eq v2, v4, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getAspectRatio(Landroid/graphics/drawable/Drawable;)F

    move-result v4

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mMaxAspectRatio:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 78
    .local v0, "ar":F
    sget-object v4, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-ne v2, v4, :cond_4

    .line 79
    int-to-float v4, v1

    mul-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 86
    :cond_1
    :goto_1
    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setMeasuredDimension(II)V

    .line 89
    .end local v0    # "ar":F
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mInLayout:Z

    .line 90
    return-void

    .line 64
    :cond_3
    sget-object v4, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-ne v2, v4, :cond_0

    .line 65
    int-to-float v4, v3

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mMaxAspectRatio:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    .line 81
    .restart local v0    # "ar":F
    :cond_4
    sget-object v4, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-ne v2, v4, :cond_1

    .line 82
    int-to-float v4, v3

    div-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_1
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mAutoSizeMode:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mInLayout:Z

    if-nez v0, :cond_0

    .line 98
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->post(Ljava/lang/Runnable;)Z

    .line 101
    :cond_0
    if-eqz p1, :cond_1

    .line 102
    const-string v0, "EPGAutoSizeImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loaded image:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_1
    return-void
.end method

.method public setImageSrc(Ljava/lang/String;)V
    .locals 1
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mSrc:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mSrc:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mSrc:Ljava/lang/String;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadHeight:I

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->mLoadWidth:I

    .line 46
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->requestLayout()V

    goto :goto_0
.end method
