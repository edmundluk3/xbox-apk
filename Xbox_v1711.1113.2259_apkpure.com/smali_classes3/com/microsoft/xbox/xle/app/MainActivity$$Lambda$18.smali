.class final synthetic Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final arg$1:Z

.field private final arg$2:J

.field private final arg$3:Ljava/lang/Class;

.field private final arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;


# direct methods
.method private constructor <init>(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$1:Z

    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$2:J

    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$3:Ljava/lang/Class;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    return-void
.end method

.method public static lambdaFactory$(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;

    move v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;-><init>(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$1:Z

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$2:J

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$3:Ljava/lang/Class;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->arg$4:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-static {v0, v2, v3, v1, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->lambda$navigateToGameProfile$16(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    return-void
.end method
