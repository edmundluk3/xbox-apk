.class abstract Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;
.super Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
.source "$AutoValue_PeopleHubDataTypes_MultiplayerSummary.java"


# instance fields
.field private final inMultiplayerSession:I

.field private final inParty:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1, "inMultiplayerSession"    # I
    .param p2, "inParty"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;-><init>()V

    .line 16
    iput p1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inMultiplayerSession:I

    .line 17
    iput p2, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inParty:I

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    iget v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inMultiplayerSession:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;->inMultiplayerSession()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inParty:I

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;->inParty()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    :cond_3
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 57
    iget v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inMultiplayerSession:I

    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v2

    .line 59
    iget v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inParty:I

    xor-int/2addr v0, v1

    .line 60
    return v0
.end method

.method public inMultiplayerSession()I
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "InMultiplayerSession"
    .end annotation

    .prologue
    .line 23
    iget v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inMultiplayerSession:I

    return v0
.end method

.method public inParty()I
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "InParty"
    .end annotation

    .prologue
    .line 29
    iget v0, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inParty:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MultiplayerSummary{inMultiplayerSession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inMultiplayerSession:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inParty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/peopleHub/$AutoValue_PeopleHubDataTypes_MultiplayerSummary;->inParty:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
