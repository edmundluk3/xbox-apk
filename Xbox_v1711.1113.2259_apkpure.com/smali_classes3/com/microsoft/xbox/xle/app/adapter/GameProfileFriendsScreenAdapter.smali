.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GameProfileFriendsScreenAdapter.java"


# instance fields
.field private listHash:I

.field private peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->listHash:I

    .line 28
    const v0, 0x7f0e0655

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    .line 31
    const v0, 0x7f0e0657

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 33
    const v0, 0x7f0e0656

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 35
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 39
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;
    .param p1, "clubCardCategoryItem"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->criteria:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->launchRecommendations(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onResume()V

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->notifyDataSetChanged()V

    .line 45
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 59
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 61
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 64
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 65
    .local v1, "newHash":I
    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->listHash:I

    if-eq v2, v1, :cond_0

    .line 66
    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->listHash:I

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->clear()V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->peopleListAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->notifyDataSetChanged()V

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getLastSelectedPosition()I

    move-result v2

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getLastSelectedPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    .line 76
    :cond_0
    return-void
.end method
