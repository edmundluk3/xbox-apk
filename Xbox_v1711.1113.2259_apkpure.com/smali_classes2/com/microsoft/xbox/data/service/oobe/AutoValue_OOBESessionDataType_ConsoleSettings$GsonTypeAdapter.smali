.class public final Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_OOBESessionDataType_ConsoleSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final allowAutoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final allowAutoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final allowInstantOnAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final autoDSTEnabledAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final clientVersionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeZoneAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAllowAutoUpdateApps:Ljava/lang/Boolean;

.field private defaultAllowAutoUpdateSystem:Ljava/lang/Boolean;

.field private defaultAllowInstantOn:Ljava/lang/Boolean;

.field private defaultAutoDSTEnabled:Ljava/lang/Boolean;

.field private defaultClientVersion:Ljava/lang/String;

.field private defaultCurrentTimeZone:Ljava/lang/String;

.field private defaultIsNewUser:Ljava/lang/Boolean;

.field private defaultOffConsoleStatus:Ljava/lang/String;

.field private defaultUserEmail:Ljava/lang/String;

.field private defaultUserGamerPicUrl:Ljava/lang/String;

.field private defaultUserGamerTag:Ljava/lang/String;

.field private final isNewUserAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final offConsoleStatusAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userEmailAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userGamerPicUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userGamerTagAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateApps:Ljava/lang/Boolean;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateSystem:Ljava/lang/Boolean;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultCurrentTimeZone:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAutoDSTEnabled:Ljava/lang/Boolean;

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowInstantOn:Ljava/lang/Boolean;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultOffConsoleStatus:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserEmail:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerPicUrl:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerTag:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultIsNewUser:Ljava/lang/Boolean;

    .line 43
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultClientVersion:Ljava/lang/String;

    .line 45
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->currentTimeZoneAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->autoDSTEnabledAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowInstantOnAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->offConsoleStatusAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userEmailAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerPicUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerTagAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->isNewUserAdapter:Lcom/google/gson/TypeAdapter;

    .line 55
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->clientVersionAdapter:Lcom/google/gson/TypeAdapter;

    .line 56
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .locals 14
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_0

    .line 136
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 137
    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    .line 139
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateApps:Ljava/lang/Boolean;

    .line 141
    .local v1, "allowAutoUpdateApps":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateSystem:Ljava/lang/Boolean;

    .line 142
    .local v2, "allowAutoUpdateSystem":Ljava/lang/Boolean;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultCurrentTimeZone:Ljava/lang/String;

    .line 143
    .local v3, "currentTimeZone":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAutoDSTEnabled:Ljava/lang/Boolean;

    .line 144
    .local v4, "autoDSTEnabled":Ljava/lang/Boolean;
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowInstantOn:Ljava/lang/Boolean;

    .line 145
    .local v5, "allowInstantOn":Ljava/lang/Boolean;
    iget-object v6, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultOffConsoleStatus:Ljava/lang/String;

    .line 146
    .local v6, "offConsoleStatus":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserEmail:Ljava/lang/String;

    .line 147
    .local v7, "userEmail":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerPicUrl:Ljava/lang/String;

    .line 148
    .local v8, "userGamerPicUrl":Ljava/lang/String;
    iget-object v9, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerTag:Ljava/lang/String;

    .line 149
    .local v9, "userGamerTag":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultIsNewUser:Ljava/lang/Boolean;

    .line 150
    .local v10, "isNewUser":Ljava/lang/Boolean;
    iget-object v11, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultClientVersion:Ljava/lang/String;

    .line 151
    .local v11, "clientVersion":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v12

    .line 153
    .local v12, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_1

    .line 154
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 157
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 203
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 157
    :sswitch_0
    const-string v13, "AllowAutoUpdateApps"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v13, "AllowAutoUpdateSystem"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v13, "CurrentTimeZone"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v13, "AutoDSTEnabled"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v13, "AllowInstantOn"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v13, "OffConsoleStatus"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v13, "UserEmail"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v13, "UserGamerPicUrl"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v13, "UserGamerTag"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x8

    goto :goto_2

    :sswitch_9
    const-string v13, "IsNewUser"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x9

    goto :goto_2

    :sswitch_a
    const-string v13, "ClientVersion"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0xa

    goto :goto_2

    .line 159
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "allowAutoUpdateApps":Ljava/lang/Boolean;
    check-cast v1, Ljava/lang/Boolean;

    .line 160
    .restart local v1    # "allowAutoUpdateApps":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 163
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "allowAutoUpdateSystem":Ljava/lang/Boolean;
    check-cast v2, Ljava/lang/Boolean;

    .line 164
    .restart local v2    # "allowAutoUpdateSystem":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 167
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->currentTimeZoneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "currentTimeZone":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 168
    .restart local v3    # "currentTimeZone":Ljava/lang/String;
    goto/16 :goto_1

    .line 171
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->autoDSTEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "autoDSTEnabled":Ljava/lang/Boolean;
    check-cast v4, Ljava/lang/Boolean;

    .line 172
    .restart local v4    # "autoDSTEnabled":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 175
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowInstantOnAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "allowInstantOn":Ljava/lang/Boolean;
    check-cast v5, Ljava/lang/Boolean;

    .line 176
    .restart local v5    # "allowInstantOn":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 179
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->offConsoleStatusAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "offConsoleStatus":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 180
    .restart local v6    # "offConsoleStatus":Ljava/lang/String;
    goto/16 :goto_1

    .line 183
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userEmailAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "userEmail":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 184
    .restart local v7    # "userEmail":Ljava/lang/String;
    goto/16 :goto_1

    .line 187
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerPicUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "userGamerPicUrl":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 188
    .restart local v8    # "userGamerPicUrl":Ljava/lang/String;
    goto/16 :goto_1

    .line 191
    :pswitch_8
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerTagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "userGamerTag":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 192
    .restart local v9    # "userGamerTag":Ljava/lang/String;
    goto/16 :goto_1

    .line 195
    :pswitch_9
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->isNewUserAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "isNewUser":Ljava/lang/Boolean;
    check-cast v10, Ljava/lang/Boolean;

    .line 196
    .restart local v10    # "isNewUser":Ljava/lang/Boolean;
    goto/16 :goto_1

    .line 199
    :pswitch_a
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->clientVersionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "clientVersion":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 200
    .restart local v11    # "clientVersion":Ljava/lang/String;
    goto/16 :goto_1

    .line 207
    .end local v12    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 208
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    :sswitch_data_0
    .sparse-switch
        -0x75c2c98f -> :sswitch_6
        -0x734a268d -> :sswitch_0
        -0x3d205f1f -> :sswitch_9
        -0x31fbd989 -> :sswitch_4
        -0x30eaf36e -> :sswitch_2
        -0x177b8286 -> :sswitch_7
        0x55dcc4f0 -> :sswitch_1
        0x5da1266d -> :sswitch_a
        0x7115571a -> :sswitch_5
        0x729fc5c5 -> :sswitch_8
        0x75b780eb -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAllowAutoUpdateApps(Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAllowAutoUpdateApps"    # Ljava/lang/Boolean;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateApps:Ljava/lang/Boolean;

    .line 59
    return-object p0
.end method

.method public setDefaultAllowAutoUpdateSystem(Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAllowAutoUpdateSystem"    # Ljava/lang/Boolean;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowAutoUpdateSystem:Ljava/lang/Boolean;

    .line 63
    return-object p0
.end method

.method public setDefaultAllowInstantOn(Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAllowInstantOn"    # Ljava/lang/Boolean;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAllowInstantOn:Ljava/lang/Boolean;

    .line 75
    return-object p0
.end method

.method public setDefaultAutoDSTEnabled(Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAutoDSTEnabled"    # Ljava/lang/Boolean;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultAutoDSTEnabled:Ljava/lang/Boolean;

    .line 71
    return-object p0
.end method

.method public setDefaultClientVersion(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClientVersion"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultClientVersion:Ljava/lang/String;

    .line 99
    return-object p0
.end method

.method public setDefaultCurrentTimeZone(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCurrentTimeZone"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultCurrentTimeZone:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public setDefaultIsNewUser(Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsNewUser"    # Ljava/lang/Boolean;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultIsNewUser:Ljava/lang/Boolean;

    .line 95
    return-object p0
.end method

.method public setDefaultOffConsoleStatus(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOffConsoleStatus"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultOffConsoleStatus:Ljava/lang/String;

    .line 79
    return-object p0
.end method

.method public setDefaultUserEmail(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserEmail"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserEmail:Ljava/lang/String;

    .line 83
    return-object p0
.end method

.method public setDefaultUserGamerPicUrl(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserGamerPicUrl"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerPicUrl:Ljava/lang/String;

    .line 87
    return-object p0
.end method

.method public setDefaultUserGamerTag(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserGamerTag"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->defaultUserGamerTag:Ljava/lang/String;

    .line 91
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    if-nez p2, :cond_0

    .line 105
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 132
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 109
    const-string v0, "AllowAutoUpdateApps"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateApps()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 111
    const-string v0, "AllowAutoUpdateSystem"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowAutoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateSystem()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 113
    const-string v0, "CurrentTimeZone"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->currentTimeZoneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->currentTimeZone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 115
    const-string v0, "AutoDSTEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->autoDSTEnabledAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->autoDSTEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 117
    const-string v0, "AllowInstantOn"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->allowInstantOnAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowInstantOn()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 119
    const-string v0, "OffConsoleStatus"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->offConsoleStatusAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->offConsoleStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 121
    const-string v0, "UserEmail"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userEmailAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 123
    const-string v0, "UserGamerPicUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerPicUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerPicUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 125
    const-string v0, "UserGamerTag"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->userGamerTagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 127
    const-string v0, "IsNewUser"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->isNewUserAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->isNewUser()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 129
    const-string v0, "ClientVersion"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->clientVersionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->clientVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 131
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)V

    return-void
.end method
