.class public abstract Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "ActivityFeedScreenViewModelBase.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;
.implements Lcom/microsoft/xbox/xle/viewmodel/IAddFollowingUserManager;
.implements Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$DeleteSocialRecommendationItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$CheckPinStatusAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$BlockingIndicatorContainer;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

.field private addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

.field protected currentFilters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

.field private feedDataChangedInternally:Z

.field filterRepository:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected filteringEnabled:Z

.field protected filtersActive:Z

.field private forceRefresh:Z

.field private final fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private isDeletingFeedItem:Z

.field private isHidingUnhidingFeedItem:Z

.field private isLoadingActivityFeedClub:Z

.field private isLoadingMoreFeed:Z

.field protected isLoadingProfileRecentActivityFeedData:Z

.field private isPinningUnpinningFeedItem:Z

.field private lastSelectedPosition:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

.field private likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

.field private loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

.field private loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

.field private loadActivitySummaryData:Z

.field private loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

.field private loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

.field private loadUnsharedActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

.field protected final model:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private positionSettingEnabled:Z

.field private profileRecentActivityFeedData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private reloadFeed:Z

.field private reloadSocialInfo:Z

.field private reloadUnsharedFeed:Z

.field private shouldUpdateHiddenState:Z

.field private shouldUpdateLikeControl:Z

.field private socialRecommendations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private updateSocialRecommendations:Z

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 146
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 103
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 127
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->forceRefresh:Z

    .line 128
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->positionSettingEnabled:Z

    .line 129
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateSocialRecommendations:Z

    .line 130
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivitySummaryData:Z

    .line 131
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    .line 132
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingMoreFeed:Z

    .line 151
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    .line 153
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 159
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 162
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct/range {p0 .. p5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onPinFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onDeleteSocialRecommendationItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onLikeActivityFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    return-void
.end method

.method static synthetic access$1302(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateLikeControl:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onLoadActivityFeedClubComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingMoreFeed:Z

    return v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingMoreFeed:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onLoadProfileRecentActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onLoadActivityAlertSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onLoadUnsharedActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onHideFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Boolean;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onCheckPinStatusCompleted(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$deleteActivityFeedItem$2(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 652
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onDeleteFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic lambda$deleteActivityFeedItem$3(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 653
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onDeleteFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic lambda$hideActivitiesFromTitle$4(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "unfollowableTitleIds"    # Ljava/util/List;
    .param p2, "titleId"    # J
    .param p4, "dialog"    # Landroid/app/AlertDialog;
    .param p5, "v"    # Landroid/view/View;

    .prologue
    .line 766
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 767
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->unfollowTitles(Ljava/util/List;J)V

    .line 768
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 770
    invoke-virtual {p4}, Landroid/app/AlertDialog;->dismiss()V

    .line 771
    return-void
.end method

.method static synthetic lambda$hideActivitiesFromTitle$5(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/app/AlertDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "dialog"    # Landroid/app/AlertDialog;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 773
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPeopleHubSocialScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 774
    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    .line 775
    return-void
.end method

.method static synthetic lambda$hideActivitiesFromTitle$6(Landroid/app/AlertDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 777
    invoke-virtual {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 778
    return-void
.end method

.method static synthetic lambda$null$7(Ljava/lang/Long;)V
    .locals 4
    .param p0, "id"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 786
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFollowingState(JZ)V

    return-void
.end method

.method static synthetic lambda$onCheckPinStatusCompleted$11(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "itemRoot"    # Ljava/lang/String;

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->pinActivityFeedItem(Ljava/lang/String;)V

    .line 875
    return-void
.end method

.method static synthetic lambda$onCheckPinStatusCompleted$12(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 879
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 880
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 881
    return-void
.end method

.method static synthetic lambda$onStartOverride$0(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->currentFilters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isForceRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadFeed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onStartOverride$1(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 280
    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->defaultPrefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filtersActive:Z

    .line 281
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->currentFilters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 282
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setForceRefresh(Z)V

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 288
    :cond_0
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadFeed(Z)V

    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateViewModelState()V

    .line 290
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 291
    return-void

    .line 280
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$unfollowTitles$10(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;Ljava/lang/Throwable;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "unfollowableTitleIds"    # Ljava/util/List;
    .param p2, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 798
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Failed to unfollow titles: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const v0, 0x7f0700d6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    .line 800
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 801
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 802
    return-void
.end method

.method static synthetic lambda$unfollowTitles$8(Ljava/lang/Long;)Lio/reactivex/CompletableSource;
    .locals 1
    .param p0, "id"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 786
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$13;->lambdaFactory$(Ljava/lang/Long;)Lio/reactivex/functions/Action;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$unfollowTitles$9(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;J)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
    .param p1, "primaryTitleId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 790
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->hideTitleActivityFeed(J)V

    .line 791
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    .line 792
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 793
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setShouldUpdateHiddenState(Z)V

    .line 794
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 795
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 796
    return-void
.end method

.method public static launchGameProfileOrOpenLink(Ljava/lang/Object;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 4
    .param p0, "uncastItem"    # Ljava/lang/Object;
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 616
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 618
    if-eqz p0, :cond_0

    instance-of v2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v2, :cond_0

    move-object v0, p0

    .line 619
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 620
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 626
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    .line 627
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    const-string v3, "Game"

    .line 628
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    .line 629
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 630
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 631
    .local v1, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setNowPlayingTitleId(J)V

    .line 632
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->Name:Ljava/lang/String;

    .line 633
    const/16 v2, 0x2329

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 634
    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->navigateToGameProfile(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 639
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    :cond_0
    :goto_0
    return-void

    .line 622
    .restart local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackWebLinkLaunch()V

    .line 623
    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->openWebLink(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto :goto_0

    .line 620
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private loadActivityFeedClubs()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1114
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1115
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1116
    .local v2, "clubIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 1117
    .local v4, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v7, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v8, "Club"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1118
    iget-object v7, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v8, "Club/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1119
    .local v5, "parts":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 1121
    .local v0, "clubId":Ljava/lang/Long;
    array-length v7, v5

    if-le v7, v10, :cond_1

    .line 1122
    iget-object v7, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    const-string v8, "Club/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    aget-object v1, v7, v10

    .line 1124
    .local v1, "clubIdStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1132
    .end local v1    # "clubIdStr":Ljava/lang/String;
    :goto_1
    if-eqz v0, :cond_0

    .line 1133
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1125
    .restart local v1    # "clubIdStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1126
    .local v3, "ex":Ljava/lang/NumberFormatException;
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to parse club id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1129
    .end local v1    # "clubIdStr":Ljava/lang/String;
    .end local v3    # "ex":Ljava/lang/NumberFormatException;
    :cond_1
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Couldn\'t find club id in: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1137
    .end local v0    # "clubId":Ljava/lang/Long;
    .end local v4    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v5    # "parts":[Ljava/lang/String;
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1138
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    if-eqz v6, :cond_3

    .line 1139
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;->cancel()V

    .line 1140
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    .line 1143
    :cond_3
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v6, p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;)V

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    .line 1144
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;->load(Z)V

    .line 1147
    .end local v2    # "clubIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_4
    return-void
.end method

.method private loadActivityFeedSocialInfo(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1627
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadSocialInfo(Z)V

    .line 1629
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

    if-eqz v0, :cond_0

    .line 1630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;->cancel()V

    .line 1633
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

    .line 1634
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadSocialAsyncTask;->load(Z)V

    .line 1635
    return-void
.end method

.method private loadFeed(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;->cancel()V

    .line 455
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    .line 456
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadFeed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;->load(Z)V

    .line 458
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadFeed(Z)V

    .line 459
    return-void

    :cond_2
    move v0, v1

    .line 456
    goto :goto_0
.end method

.method private loadUnsharedFeed(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;->cancel()V

    .line 465
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

    .line 466
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadUnsharedFeed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadUnsharedActivityFeedAsyncTask;->load(Z)V

    .line 468
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadUnsharedFeed(Z)V

    .line 469
    return-void

    :cond_2
    move v0, v1

    .line 466
    goto :goto_0
.end method

.method private missingCommunicatePrivilegeError()V
    .locals 5

    .prologue
    .line 574
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    .line 575
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 576
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f07061c

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 577
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 574
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 579
    return-void
.end method

.method private onCheckPinStatusCompleted(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "hasPin"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 863
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 866
    if-eqz p2, :cond_1

    .line 867
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700df

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 870
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v2, 0x7f0700de

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 871
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v3, 0x7f0707c7

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v4

    .line 876
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v5, 0x7f07060d

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Ljava/lang/Runnable;

    move-result-object v6

    move-object v0, p0

    .line 868
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 892
    :goto_0
    return-void

    .line 884
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->pinActivityFeedItem(Ljava/lang/String;)V

    goto :goto_0

    .line 887
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "CheckPinStatus returned null indicating we failed to fetch pins for this feed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 889
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 890
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070b6d

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onDeleteFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 663
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isDeletingFeedItem:Z

    .line 664
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 666
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 677
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 678
    return-void

    .line 670
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    goto :goto_0

    .line 674
    :pswitch_1
    const v0, 0x7f0703dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    goto :goto_0

    .line 666
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onDeleteSocialRecommendationItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    const/4 v3, 0x1

    .line 931
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 947
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 948
    return-void

    .line 933
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->socialRecommendations:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->socialRecommendations:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 935
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setUpdateSocialRecommendations(Z)V

    goto :goto_0

    .line 943
    :pswitch_2
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    .line 944
    const-string v0, "DeleteSocialRecommendationItemAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Delete recommendation: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 931
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onHideFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "hide"    # Z

    .prologue
    .line 848
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 849
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 851
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 852
    if-eqz p2, :cond_0

    .line 853
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700d6

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 852
    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(Ljava/lang/String;)V

    .line 859
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 860
    return-void

    .line 854
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700ef

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 856
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    .line 857
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setShouldUpdateHiddenState(Z)V

    goto :goto_1
.end method

.method private onLikeActivityFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "newLikeState"    # Z
    .param p3, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 1590
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "onLikeActivityFeedItemCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1611
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1612
    return-void

    .line 1595
    :pswitch_0
    if-eqz p3, :cond_0

    iget-object v0, p3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateItemLikeInfo(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V

    .line 1598
    :cond_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateLikeControl:Z

    .line 1599
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadSocialInfo(Z)V

    goto :goto_0

    .line 1603
    :pswitch_1
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    .line 1604
    if-eqz p2, :cond_1

    .line 1605
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like ActivityFeedItemID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1607
    :cond_1
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike ActivityFeedItem: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1591
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadActivityAlertSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1150
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "onLoadActivityAlertSummaryCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1163
    :goto_0
    return-void

    .line 1156
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    goto :goto_0

    .line 1152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadActivityFeedClubComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1686
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    .line 1687
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1693
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateViewModelState()V

    .line 1694
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1695
    return-void

    .line 1690
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load clubs."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1687
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadProfileRecentActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1070
    const-string v0, "PeopleActivityFeedScreenViewModel"

    const-string v1, "onLoadRecents Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingMoreFeed:Z

    .line 1072
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1098
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showActivityAlertNotificationBar()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivitySummaryData:Z

    if-eqz v0, :cond_2

    .line 1099
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    if-eqz v0, :cond_1

    .line 1100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;->cancel()V

    .line 1103
    :cond_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivitySummaryData:Z

    .line 1105
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    .line 1107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;->load(Z)V

    .line 1110
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1111
    return-void

    .line 1074
    :pswitch_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isForceRefresh()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1075
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setForceRefresh(Z)V

    .line 1076
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    invoke-direct {v0, v2, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setLastSelectedPosition(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;)V

    .line 1078
    :cond_3
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadSocialInfo(Z)V

    .line 1081
    :pswitch_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getActivityFeedData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    .line 1082
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubs()V

    .line 1083
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    .line 1084
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadSocialInfo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1085
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedSocialInfo(Ljava/util/List;)V

    .line 1087
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateViewModelState()V

    goto :goto_0

    .line 1091
    :pswitch_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1092
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    .line 1093
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 1072
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onLoadUnsharedActivityFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1166
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "onLoadUnsharedActivityFeedCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1178
    :goto_0
    return-void

    .line 1171
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    goto :goto_0

    .line 1167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onPinFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "pinning"    # Z
    .param p4, "feedItemIdForDeletion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "locatorForDeletion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 895
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 897
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 898
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 899
    if-eqz p3, :cond_0

    .line 900
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700dc

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 899
    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(Ljava/lang/String;)V

    .line 918
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 919
    return-void

    .line 901
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700f0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 904
    :cond_1
    if-nez p3, :cond_2

    if-eqz p4, :cond_2

    if-eqz p5, :cond_2

    .line 905
    invoke-virtual {p0, p4, p5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 908
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->load(Z)V

    .line 910
    if-eqz p3, :cond_3

    .line 911
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0700dd

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_1

    .line 913
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0700f1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_1
.end method

.method private pinActivityFeedItem(Ljava/lang/String;)V
    .locals 6
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 832
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 833
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 834
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 835
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;

    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 836
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackPinToFeed()V

    .line 837
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->load(Z)V

    .line 838
    return-void
.end method

.method private unfollowTitles(Ljava/util/List;J)V
    .locals 4
    .param p2, "primaryTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 785
    .local p1, "unfollowableTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 786
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object v1

    .line 787
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 788
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;J)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 789
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 785
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 804
    return-void
.end method

.method public static updateItemLikeInfo(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V
    .locals 1
    .param p0, "socialInfo"    # Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1615
    if-eqz p0, :cond_0

    .line 1616
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 1617
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 1619
    :cond_0
    return-void

    .line 1616
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1617
    :cond_2
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public addFollowingUser(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/List;)V
    .locals 2
    .param p1, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 959
    .local p2, "socialRecommendations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 961
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 962
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->cancel()V

    .line 966
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->socialRecommendations:Ljava/util/List;

    .line 967
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IAddFollowingUserManager;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    .line 968
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->load(Z)V

    .line 973
    :goto_0
    return-void

    .line 971
    :cond_1
    const v0, 0x7f07061c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method public addUserToShareIdentityList(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 2
    .param p1, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 976
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 978
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->cancel()V

    .line 981
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;Ljava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    .line 982
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->load(Z)V

    .line 983
    return-void
.end method

.method public canHidePost()Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public canPinPosts()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public checkAndPinActivityFeedItem(Ljava/lang/String;)V
    .locals 2
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 807
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 808
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 809
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 810
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$CheckPinStatusAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$CheckPinStatusAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;)V

    .line 811
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$CheckPinStatusAsyncTask;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$CheckPinStatusAsyncTask;->load(Z)V

    .line 812
    return-void
.end method

.method public deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "feedItemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 642
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 643
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 645
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 646
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isDeletingFeedItem:Z

    .line 647
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 648
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->deleteFeedItem(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v1

    .line 649
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 650
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 651
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 648
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 659
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Activity Feed Delete Item"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 660
    return-void

    .line 656
    :cond_0
    const v0, 0x7f0703dc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    goto :goto_0
.end method

.method public deleteSocialRecommendationItem(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/List;)V
    .locals 2
    .param p1, "itemToRemove"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 922
    .local p2, "suggestedPeople":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 923
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 925
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->socialRecommendations:Ljava/util/List;

    .line 926
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$DeleteSocialRecommendationItemAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$DeleteSocialRecommendationItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 927
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$DeleteSocialRecommendationItemAsyncTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$DeleteSocialRecommendationItemAsyncTask;->load(Z)V

    .line 928
    return-void
.end method

.method public drainFeedDataChangedInternally()Z
    .locals 2

    .prologue
    .line 1064
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->feedDataChangedInternally:Z

    .line 1065
    .local v0, "value":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->feedDataChangedInternally:Z

    .line 1066
    return v0
.end method

.method public filtersAreActive()Z
    .locals 1

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filtersActive:Z

    return v0
.end method

.method protected abstract getActivityFeedData()Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getContinuationToken()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIsDeletingFeedItem()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isDeletingFeedItem:Z

    return v0
.end method

.method public getIsHidingUnhidingFeedItem()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    return v0
.end method

.method public getIsPinningUnpinningFeedItem()Z
    .locals 1

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    return v0
.end method

.method public getLastSelectedPosition()Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 381
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->lastSelectedPosition:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 473
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07056f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPeopleActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    return-object v0
.end method

.method protected getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 166
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleActivityFeedAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method public getShouldUpdateHiddenState()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateHiddenState:Z

    return v0
.end method

.method public getTimelineId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public getUpdateSocialRecommendations()Z
    .locals 1

    .prologue
    .line 951
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateSocialRecommendations:Z

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hideActivitiesFromClub(J)V
    .locals 5
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 701
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 703
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    .line 704
    .local v0, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 705
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 707
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;J)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->unfollowClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 731
    return-void
.end method

.method public hideActivitiesFromTitle(JLjava/util/List;)V
    .locals 19
    .param p1, "titleId"    # J
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 734
    .local p3, "unfollowableTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;>;"
    const-wide/16 v14, 0x0

    cmp-long v14, p1, v14

    if-lez v14, :cond_1

    .line 735
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 736
    .local v11, "unfollowableGames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 737
    .local v12, "unfollowableTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;

    .line 738
    .local v10, "title":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;
    iget-object v15, v10, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;->titleName:Ljava/lang/String;

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 739
    iget-wide v0, v10, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;->titleId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 742
    .end local v10    # "title":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$UnfollowableTitle;
    :cond_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_2

    .line 743
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 744
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v12, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->unfollowTitles(Ljava/util/List;J)V

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 782
    .end local v11    # "unfollowableGames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "unfollowableTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_1
    :goto_1
    return-void

    .line 747
    .restart local v11    # "unfollowableGames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "unfollowableTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_2
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    invoke-direct {v4, v14}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 748
    .local v4, "alert":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v14

    const-string v15, "layout_inflater"

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    .line 749
    .local v8, "inflater":Landroid/view/LayoutInflater;
    const v14, 0x7f03022c

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v14, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 751
    .local v13, "view":Landroid/view/View;
    sget-object v14, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v15, 0x7f0700d3

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v14

    invoke-virtual {v4, v14}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 752
    sget-object v14, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v15, 0x7f0700d1

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, ", "

    move-object/from16 v0, v17

    invoke-static {v0, v11}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v14

    invoke-virtual {v4, v14}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 753
    invoke-virtual {v4, v13}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 754
    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 756
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    .line 757
    .local v6, "dialog":Landroid/app/AlertDialog;
    const v14, 0x7f0e0ac1

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 758
    .local v7, "hideAllButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v14, 0x7f0e0ac2

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 759
    .local v9, "manageButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v14, 0x7f0e0ac3

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 761
    .local v5, "cancelButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v14, 0x7f0700d4

    invoke-virtual {v7, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 762
    const v14, 0x7f0700d5

    invoke-virtual {v9, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 763
    const v14, 0x7f07060d

    invoke-virtual {v5, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 765
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-static {v0, v12, v1, v2, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;JLandroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 772
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Landroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$7;->lambdaFactory$(Landroid/app/AlertDialog;)Landroid/view/View$OnClickListener;

    move-result-object v14

    invoke-virtual {v5, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 779
    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1
.end method

.method public hideActivityFeedItem(Ljava/lang/String;)V
    .locals 2
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 681
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 682
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 684
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackHideFromFeed()V

    .line 685
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;

    invoke-direct {v0, p0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;Z)V

    .line 686
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->load(Z)V

    .line 687
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 688
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFilterEnabled()Z
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filteringEnabled:Z

    return v0
.end method

.method protected isForceRefresh()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->forceRefresh:Z

    return v0
.end method

.method protected isPositionSettingEnabled()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->positionSettingEnabled:Z

    return v0
.end method

.method public isPostButtonSticky()Z
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method protected isReloadFeed()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadFeed:Z

    return v0
.end method

.method protected isReloadSocialInfo()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadSocialInfo:Z

    return v0
.end method

.method protected isReloadUnsharedFeed()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadUnsharedFeed:Z

    return v0
.end method

.method public isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 322
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canPostStatus()Z

    move-result v0

    return v0
.end method

.method public launchFilterSelection()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->trackShowFilterScreen()V

    .line 507
    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x103000a

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;->show()V

    .line 508
    return-void
.end method

.method public launchGameProfileOrOpenLink(Ljava/lang/Object;)V
    .locals 0
    .param p1, "uncastItem"    # Ljava/lang/Object;

    .prologue
    .line 612
    invoke-static {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->launchGameProfileOrOpenLink(Ljava/lang/Object;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 613
    return-void
.end method

.method public launchStatusPost()V
    .locals 3

    .prologue
    .line 487
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 488
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 489
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)V

    .line 490
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    if-eq v1, v2, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineId(Ljava/lang/String;)V

    .line 493
    :cond_0
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 494
    return-void
.end method

.method public launchUnsharedFeed()V
    .locals 3

    .prologue
    .line 497
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->MyFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;-><init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;)V

    .line 498
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->putTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)V

    .line 499
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    if-eq v1, v2, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->putTimelineId(Ljava/lang/String;)V

    .line 502
    :cond_0
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 503
    return-void
.end method

.method public likeClick(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1, "uncastItem"    # Ljava/lang/Object;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 1528
    if-eqz p1, :cond_1

    instance-of v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1529
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 1530
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v1, :cond_1

    .line 1531
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

    if-eqz v1, :cond_0

    .line 1532
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->cancel()V

    .line 1535
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

    invoke-direct {v1, p0, v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

    .line 1536
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LikeClickAsyncTask;->load(Z)V

    .line 1539
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    return-void
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 423
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setForceRefresh(Z)V

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadActivityFeedMeSettingsAsync(Z)V

    .line 429
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filteringEnabled:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->currentFilters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    if-eqz v0, :cond_2

    .line 430
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadFeed(Z)V

    .line 433
    :cond_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedFeed(Z)V

    .line 435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivitySummaryData:Z

    .line 436
    return-void
.end method

.method protected abstract loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public navigateToActionsScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 519
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 520
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 522
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne p2, v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "navigateToActionsScreen() no communication privilege"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->missingCommunicatePrivilegeError()V

    .line 529
    :goto_0
    return-void

    .line 528
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    goto :goto_0
.end method

.method public navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 2
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 550
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 551
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 553
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    if-ne p2, v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 554
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "navigateToActionsScreen() no communication privilege"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->missingCommunicatePrivilegeError()V

    .line 559
    :goto_0
    return-void

    .line 557
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    goto :goto_0
.end method

.method public navigateToEnforcement(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "evidenceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "shouldReportToModerator"    # Z

    .prologue
    .line 536
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 537
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 538
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 539
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 541
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 542
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->fromScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->trySetActivityFeedContextFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 544
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 545
    return-void
.end method

.method public navigateToPostCommentScreen(Ljava/lang/String;)V
    .locals 2
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 563
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 565
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 571
    :goto_0
    return-void

    .line 568
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "navigateToPostCommentScreen() no communication privilege"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->missingCommunicatePrivilegeError()V

    goto :goto_0
.end method

.method public navigateToShareScreen(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 598
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 600
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->shareRoot:Ljava/lang/String;

    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 609
    :goto_0
    return-void

    .line 603
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    .line 604
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 605
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070110

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 606
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 603
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 5
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 583
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 584
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 586
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 595
    :goto_0
    return-void

    .line 589
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    .line 590
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07061d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 591
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070110

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 592
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 589
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToSuggestedPeopleScreen()V
    .locals 3

    .prologue
    .line 483
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 484
    return-void
.end method

.method public navigateToUserProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 477
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 479
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public onAddFollowingUserNoAction(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1024
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1025
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1027
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->postAddFollowingUser(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 1028
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;->onDestroy()V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 191
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 192
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 299
    return-void
.end method

.method protected onStartOverride()V
    .locals 3

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filteringEnabled:Z

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->filterRepository:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;

    .line 275
    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;->getPrefs()Lio/reactivex/Observable;

    move-result-object v1

    .line 276
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 277
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 278
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 279
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 274
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 294
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;->cancel()V

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    if-eqz v0, :cond_1

    .line 405
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityAlertSummaryAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityAlertSummaryAsyncTask;->cancel()V

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    if-eqz v0, :cond_2

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToFollowingListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToFollowingListAsyncTask;->cancel()V

    .line 412
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    if-eqz v0, :cond_3

    .line 413
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->addUserToShareIdentityListAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AddUserToShareIdentityListAsyncTask;->cancel()V

    .line 416
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    if-eqz v0, :cond_4

    .line 417
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedClubAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadActivityFeedClubAsyncTask;->cancel()V

    .line 419
    :cond_4
    return-void
.end method

.method public postAddFollowingUser(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 4
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 992
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 993
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 995
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1017
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown AsyncActionStatus: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1020
    :goto_0
    return-void

    .line 999
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->socialRecommendations:Ljava/util/List;

    invoke-virtual {p0, p2, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->deleteSocialRecommendationItem(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/List;)V

    goto :goto_0

    .line 1003
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v3, "Failed to add user to following list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    const/4 v1, 0x0

    .line 1005
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 1006
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 1007
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAddUserToFollowingResult()Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    move-result-object v1

    .line 1010
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->getAddFollowingRequestStatus()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->code:I

    const/16 v3, 0x404

    if-ne v2, v3, :cond_1

    .line 1011
    const v2, 0x7f070104

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    goto :goto_0

    .line 1013
    :cond_1
    const v2, 0x7f070b1c

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    goto :goto_0

    .line 995
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public postAddUsersToShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1047
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1058
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown AsyncActionStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1061
    :goto_0
    return-void

    .line 1051
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "Successfully added user to Real Name Share List"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1055
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "Failed Adding user to Real Name Sharing List"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1047
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public postRemoveUserFromShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1042
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ActivityFeedScreenViewModelBase.postRemoveUserFromShareList not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public preAddFollowingUser()V
    .locals 0

    .prologue
    .line 987
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 988
    return-void
.end method

.method public preAddUsersToShareList()V
    .locals 0

    .prologue
    .line 1032
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1033
    return-void
.end method

.method public preRemoveUserFromShareList()V
    .locals 2

    .prologue
    .line 1037
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ActivityFeedScreenViewModelBase.preRemoveUserFromShareList not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeActivityFeedItem(Ljava/lang/String;)V
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 841
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeFeedItemFromCache(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    .line 843
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 845
    :cond_0
    return-void
.end method

.method public resetLikeControlUpdate()V
    .locals 1

    .prologue
    .line 1623
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateLikeControl:Z

    .line 1624
    return-void
.end method

.method protected setForceRefresh(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->forceRefresh:Z

    .line 263
    return-void
.end method

.method public setLastSelectedPosition(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;)V
    .locals 1
    .param p1, "lastSelectedPosition"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPositionSettingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->lastSelectedPosition:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    .line 388
    :cond_0
    return-void
.end method

.method public setPositionSettingEnabled(Z)V
    .locals 0
    .param p1, "positionSettingEnabled"    # Z

    .prologue
    .line 391
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->positionSettingEnabled:Z

    .line 392
    return-void
.end method

.method protected setReloadFeed(Z)V
    .locals 0
    .param p1, "reloadFeed"    # Z

    .prologue
    .line 238
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadFeed:Z

    .line 239
    return-void
.end method

.method protected setReloadSocialInfo(Z)V
    .locals 0
    .param p1, "reloadSocialInfo"    # Z

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadSocialInfo:Z

    .line 247
    return-void
.end method

.method protected setReloadUnsharedFeed(Z)V
    .locals 0
    .param p1, "reloadUnsharedFeed"    # Z

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->reloadUnsharedFeed:Z

    .line 255
    return-void
.end method

.method public setShouldUpdateHiddenState(Z)V
    .locals 0
    .param p1, "update"    # Z

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateHiddenState:Z

    .line 363
    return-void
.end method

.method public setUpdateSocialRecommendations(Z)V
    .locals 0
    .param p1, "updateSocialRecommendations"    # Z

    .prologue
    .line 955
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateSocialRecommendations:Z

    .line 956
    return-void
.end method

.method protected abstract shouldReloadActivityFeedData()Z
.end method

.method public shouldUpdateLikeControl()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->shouldUpdateLikeControl:Z

    return v0
.end method

.method public showActivityAlertNotificationBar()Z
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tryLoadMoreFeed()V
    .locals 3

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "continuationToken":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingMoreFeed:Z

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 441
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    if-eqz v1, :cond_0

    .line 442
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;->cancel()V

    .line 445
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    .line 446
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadProfileRecentActivityFeedAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$LoadProfileRecentActivityFeedAsyncTask;->load(Z)V

    .line 448
    :cond_1
    return-void
.end method

.method public unhideActivityFeedItem(Ljava/lang/String;)V
    .locals 3
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 691
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 692
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isHidingUnhidingFeedItem:Z

    .line 694
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackUnhideFromFeed()V

    .line 695
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;Z)V

    .line 696
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->load(Z)V

    .line 697
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 698
    return-void
.end method

.method public unpinActivityFeedItem(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "deleteAfterUnpinning"    # Z
    .param p3, "feedItemIdForDeletion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "locatorForDeletion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    .line 815
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 817
    if-eqz p2, :cond_0

    .line 818
    const-string v1, "Error, attempting to delete after unpinning without feed item"

    invoke-static {p3, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 819
    const-string v1, "Error, attempting to delete after unpinning without item locator"

    invoke-static {p4, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 822
    :cond_0
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isPinningUnpinningFeedItem:Z

    .line 823
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 825
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackRemovePinFromFeed()V

    .line 826
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 827
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;
    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->load(Z)V

    .line 828
    return-void
.end method

.method protected updateBlockingIndicator()V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$BlockingIndicatorContainer;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$BlockingIndicatorContainer;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$BlockingIndicatorContainer;->updateBlockingIndicator()V

    .line 269
    :cond_0
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 196
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 197
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 198
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 220
    :cond_0
    :goto_0
    :pswitch_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isActive:Z

    if-eqz v1, :cond_2

    .line 221
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadFeed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 222
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadFeed(Z)V

    .line 227
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadUnsharedFeed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadUnsharedFeed(Z)V

    .line 231
    :cond_2
    return-void

    .line 207
    :pswitch_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadFeed(Z)V

    .line 208
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadSocialInfo(Z)V

    goto :goto_0

    .line 213
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setReloadSocialInfo(Z)V

    goto :goto_0

    .line 223
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isReloadSocialInfo()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->loadActivityFeedSocialInfo(Ljava/util/List;)V

    goto :goto_1

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected updateViewModelState()V
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->profileRecentActivityFeedData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    if-eqz v0, :cond_3

    .line 1183
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingProfileRecentActivityFeedData:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isLoadingActivityFeedClub:Z

    if-eqz v0, :cond_2

    .line 1184
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 1192
    :goto_0
    return-void

    .line 1186
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 1189
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method protected updateWithoutAdapter()Z
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method
