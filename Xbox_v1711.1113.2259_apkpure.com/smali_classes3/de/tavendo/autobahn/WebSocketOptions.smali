.class public Lde/tavendo/autobahn/WebSocketOptions;
.super Ljava/lang/Object;
.source "WebSocketOptions.java"


# instance fields
.field private mMaskClientFrames:Z

.field private mMaxFramePayloadSize:I

.field private mMaxMessagePayloadSize:I

.field private mReceiveTextMessagesRaw:Z

.field private mReconnectInterval:I

.field private mSocketConnectTimeout:I

.field private mSocketReceiveTimeout:I

.field private mTcpNoDelay:Z

.field private mValidateIncomingUtf8:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v0, 0x20000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    .line 47
    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    .line 48
    iput-boolean v2, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReceiveTextMessagesRaw:Z

    .line 49
    iput-boolean v1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mTcpNoDelay:Z

    .line 50
    const/16 v0, 0xc8

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketReceiveTimeout:I

    .line 51
    const/16 v0, 0x1770

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketConnectTimeout:I

    .line 52
    iput-boolean v1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mValidateIncomingUtf8:Z

    .line 53
    iput-boolean v1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaskClientFrames:Z

    .line 54
    iput v2, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReconnectInterval:I

    .line 55
    return-void
.end method

.method public constructor <init>(Lde/tavendo/autobahn/WebSocketOptions;)V
    .locals 1
    .param p1, "other"    # Lde/tavendo/autobahn/WebSocketOptions;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iget v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    .line 65
    iget v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    .line 66
    iget-boolean v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mReceiveTextMessagesRaw:Z

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReceiveTextMessagesRaw:Z

    .line 67
    iget-boolean v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mTcpNoDelay:Z

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mTcpNoDelay:Z

    .line 68
    iget v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mSocketReceiveTimeout:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketReceiveTimeout:I

    .line 69
    iget v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mSocketConnectTimeout:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketConnectTimeout:I

    .line 70
    iget-boolean v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mValidateIncomingUtf8:Z

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mValidateIncomingUtf8:Z

    .line 71
    iget-boolean v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mMaskClientFrames:Z

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaskClientFrames:Z

    .line 72
    iget v0, p1, Lde/tavendo/autobahn/WebSocketOptions;->mReconnectInterval:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReconnectInterval:I

    .line 73
    return-void
.end method


# virtual methods
.method public getMaskClientFrames()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaskClientFrames:Z

    return v0
.end method

.method public getMaxFramePayloadSize()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    return v0
.end method

.method public getMaxMessagePayloadSize()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    return v0
.end method

.method public getReceiveTextMessagesRaw()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReceiveTextMessagesRaw:Z

    return v0
.end method

.method public getReconnectInterval()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReconnectInterval:I

    return v0
.end method

.method public getSocketConnectTimeout()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketConnectTimeout:I

    return v0
.end method

.method public getSocketReceiveTimeout()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketReceiveTimeout:I

    return v0
.end method

.method public getTcpNoDelay()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mTcpNoDelay:Z

    return v0
.end method

.method public getValidateIncomingUtf8()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mValidateIncomingUtf8:Z

    return v0
.end method

.method public setMaskClientFrames(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 249
    iput-boolean p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaskClientFrames:Z

    .line 250
    return-void
.end method

.method public setMaxFramePayloadSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 106
    if-lez p1, :cond_0

    .line 107
    iput p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    .line 108
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    iget v1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    if-ge v0, v1, :cond_0

    .line 109
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    .line 112
    :cond_0
    return-void
.end method

.method public setMaxMessagePayloadSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 133
    if-lez p1, :cond_0

    .line 134
    iput p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    .line 135
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    iget v1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    if-ge v0, v1, :cond_0

    .line 136
    iget v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxMessagePayloadSize:I

    iput v0, p0, Lde/tavendo/autobahn/WebSocketOptions;->mMaxFramePayloadSize:I

    .line 139
    :cond_0
    return-void
.end method

.method public setReceiveTextMessagesRaw(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReceiveTextMessagesRaw:Z

    .line 85
    return-void
.end method

.method public setReconnectInterval(I)V
    .locals 0
    .param p1, "reconnectInterval"    # I

    .prologue
    .line 267
    iput p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mReconnectInterval:I

    .line 268
    return-void
.end method

.method public setSocketConnectTimeout(I)V
    .locals 0
    .param p1, "timeoutMs"    # I

    .prologue
    .line 204
    if-ltz p1, :cond_0

    .line 205
    iput p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketConnectTimeout:I

    .line 207
    :cond_0
    return-void
.end method

.method public setSocketReceiveTimeout(I)V
    .locals 0
    .param p1, "timeoutMs"    # I

    .prologue
    .line 180
    if-ltz p1, :cond_0

    .line 181
    iput p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mSocketReceiveTimeout:I

    .line 183
    :cond_0
    return-void
.end method

.method public setTcpNoDelay(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 159
    iput-boolean p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mTcpNoDelay:Z

    .line 160
    return-void
.end method

.method public setValidateIncomingUtf8(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 227
    iput-boolean p1, p0, Lde/tavendo/autobahn/WebSocketOptions;->mValidateIncomingUtf8:Z

    .line 228
    return-void
.end method
