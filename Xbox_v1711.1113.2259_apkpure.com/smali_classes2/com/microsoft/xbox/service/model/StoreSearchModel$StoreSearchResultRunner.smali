.class Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "StoreSearchModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/StoreSearchModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StoreSearchResultRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private isCanceled:Z

.field private page:I

.field private searchTerm:Ljava/lang/String;

.field private final storeService:Lcom/microsoft/xbox/service/store/IStoreService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/StoreSearchModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/StoreSearchModel;Ljava/lang/String;I)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/service/model/StoreSearchModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "searchTerm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p3, "page"    # I

    .prologue
    const/4 v4, 0x0

    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 108
    iput v4, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->page:I

    .line 111
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 112
    const-wide/16 v0, 0x0

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 114
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->isCanceled:Z

    .line 115
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->searchTerm:Ljava/lang/String;

    .line 116
    iput p3, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->page:I

    .line 117
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 127
    :try_start_0
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->access$000(Lcom/microsoft/xbox/service/model/StoreSearchModel;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    array-length v6, v6

    const/16 v7, 0xfa

    if-ge v6, v7, :cond_2

    .line 128
    const/4 v4, 0x0

    .line 129
    .local v4, "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->searchTerm:Ljava/lang/String;

    iget v8, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->page:I

    invoke-interface {v6, v7, v8}, Lcom/microsoft/xbox/service/store/IStoreService;->getSearchResults(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v3

    .line 130
    .local v3, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v3, :cond_0

    .line 131
    new-instance v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .end local v4    # "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    invoke-direct {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;-><init>()V

    .line 132
    .restart local v4    # "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v2, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 134
    .local v0, "detail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    .end local v0    # "detail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v2    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v3    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .end local v4    # "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    move-object v4, v5

    .line 145
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v4

    .line 136
    .restart local v2    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .restart local v3    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .restart local v4    # "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_1
    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setItems(Ljava/util/ArrayList;)V

    .line 137
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setTotalCount(I)V

    .line 138
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    iget-boolean v7, v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->hasMorePages:Z

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->access$102(Lcom/microsoft/xbox/service/model/StoreSearchModel;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .end local v2    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v3    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .end local v4    # "searchResult":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_2
    move-object v4, v5

    .line 142
    goto :goto_1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    return-object v0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->isCanceled:Z

    .line 121
    return-void
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 162
    const-wide/16 v0, 0xfb3

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->isCanceled:Z

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreSearchModel$StoreSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreSearchModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/StoreSearchModel;->access$200(Lcom/microsoft/xbox/service/model/StoreSearchModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 158
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method
