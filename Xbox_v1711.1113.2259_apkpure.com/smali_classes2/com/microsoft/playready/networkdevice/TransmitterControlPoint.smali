.class public Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;
.super Ljava/lang/Object;
.source "TransmitterControlPoint.java"


# instance fields
.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/networkdevice/ITransmitterListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mListeners:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)Lcom/microsoft/playready/networkdevice/Native_Class4;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addTransmitterListener(Lcom/microsoft/playready/networkdevice/ITransmitterListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterListener;

    .prologue
    .line 38
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-direct {v0}, Lcom/microsoft/playready/networkdevice/Native_Class4;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    .line 44
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    new-instance v1, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/networkdevice/Native_Class4;->setHandler1(Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_0
    monitor-exit p0

    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized refresh()V
    .locals 1

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/Native_Class4;->method_3()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeTransmitterListener(Lcom/microsoft/playready/networkdevice/ITransmitterListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterListener;

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->mNativeClass:Lcom/microsoft/playready/networkdevice/Native_Class4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
