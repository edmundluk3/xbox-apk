.class public abstract Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
.super Ljava/lang/Object;
.source "PartyMessage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
    .locals 2
    .param p0, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 25
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Ljava/lang/String;Ljava/util/Calendar;)V

    return-object v0
.end method


# virtual methods
.method public abstract member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract message()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract timeStamp()Ljava/util/Calendar;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
