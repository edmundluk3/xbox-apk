.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubPickerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private showInviteSubtitle:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 31
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 32
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->showInviteSubtitle:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;->bindTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    .line 47
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 41
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030091

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter$ClubViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;Landroid/view/View;)V

    return-object v0
.end method

.method public setShowInviteSubtitle(Z)V
    .locals 0
    .param p1, "showInviteSubtitle"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->showInviteSubtitle:Z

    .line 37
    return-void
.end method
