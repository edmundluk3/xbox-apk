.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
.super Ljava/lang/Object;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FinishAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;
    .locals 1
    .param p0, "email"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "gamerpic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "isNewUser"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 414
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_FinishAction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract email()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract gamerpic()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract gamertag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract isNewUser()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 419
    const-string v0, "FinishAction"

    return-object v0
.end method
