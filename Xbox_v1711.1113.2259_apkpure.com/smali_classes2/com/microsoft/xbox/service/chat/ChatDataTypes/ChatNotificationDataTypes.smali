.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes;
.super Ljava/lang/Object;
.source "ChatNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostSuccess;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
    }
.end annotation


# static fields
.field public static final CHAT_PLATFORM:Ljava/lang/String; = "Android"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
