.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;
.super Ljava/lang/Object;
.source "UTCBeam.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam;->trackLaunchBeamStreamInWeb(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$streamId:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;->val$streamId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 51
    const-wide/16 v2, 0x1

    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;->val$streamId:I

    int-to-long v4, v1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 54
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "StreamId"

    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCBeam$3;->val$streamId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    const-string v1, "Beam - Launch Stream In Web"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 57
    return-void
.end method
