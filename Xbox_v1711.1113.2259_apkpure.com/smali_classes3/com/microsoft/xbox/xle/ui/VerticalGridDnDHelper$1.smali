.class Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;
.super Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;
.source "VerticalGridDnDHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper",
        "<TT;>.Anim",
        "Listener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 51
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;)V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 54
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$100(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$200(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$300(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/graphics/PointF;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->firstPosition:I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 57
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->firstPosition:I

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "v":Landroid/view/View;
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->topOffset:I

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->firstHeight:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->topOffset:I

    .line 60
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->firstHeight:I

    .line 62
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 63
    .local v1, "val":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->firstPosition:I

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->topOffset:I

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;->repeatCounter:I

    mul-int/lit16 v5, v5, 0x190

    add-int/2addr v4, v5

    add-int/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->smoothScrollToPositionFromTop(II)V

    .line 68
    .end local v1    # "val":I
    :cond_1
    :goto_0
    return-void

    .line 66
    :cond_2
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0
.end method
