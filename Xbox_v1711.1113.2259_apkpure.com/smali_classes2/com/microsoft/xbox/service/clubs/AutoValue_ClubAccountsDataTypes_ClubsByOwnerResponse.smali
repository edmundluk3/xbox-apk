.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;
.source "AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;IIII)V
    .locals 0
    .param p1, "owner"    # Ljava/lang/String;
    .param p3, "remainingOpenAndClosedClubs"    # I
    .param p4, "remainingSecretClubs"    # I
    .param p5, "maximumOpenAndClosedClubs"    # I
    .param p6, "maximumSecretClubs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;IIII)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "clubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;IIII)V

    .line 20
    return-void
.end method
