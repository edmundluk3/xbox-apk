.class public final enum Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;
.super Ljava/lang/Enum;
.source "LiveTvUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/LiveTvUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Screen"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

.field public static final enum FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

.field public static final enum LAST:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

.field public static final enum RECENT_CHANNELS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

.field public static final enum TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    const-string v1, "TV_LISTINGS"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    new-instance v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    new-instance v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    const-string v1, "RECENT_CHANNELS"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->RECENT_CHANNELS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    new-instance v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->LAST:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->RECENT_CHANNELS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->LAST:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->$VALUES:[Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->$VALUES:[Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    return-object v0
.end method
