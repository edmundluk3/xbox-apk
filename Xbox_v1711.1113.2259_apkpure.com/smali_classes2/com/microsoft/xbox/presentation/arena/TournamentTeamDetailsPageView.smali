.class public Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView;
.super Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;
.source "TournamentTeamDetailsPageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;
    }
.end annotation


# static fields
.field public static final ID_KEY:Ljava/lang/String; = "id"

.field public static final IS_PENDING_SESSION_KEY:Ljava/lang/String; = "isPendingSession"

.field public static final ORGANIZER_KEY:Ljava/lang/String; = "organizer"

.field public static final TEAM_ID_KEY:Ljava/lang/String; = "teamId"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchOptions()Landroid/os/Bundle;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 22
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;

    .line 23
    .local v1, "parameters":Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 25
    .local v0, "launchOptions":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 26
    const-string v2, "organizer"

    iget-object v3, v1, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->organizer:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v2, "id"

    iget-object v3, v1, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->id:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v2, "teamId"

    iget-object v3, v1, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->teamId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v2, "isPendingSession"

    iget-boolean v3, v1, Lcom/microsoft/xbox/presentation/arena/TournamentTeamDetailsPageView$TournamentTeamDetailsPageViewParameters;->isPendingSession:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 32
    :cond_0
    return-object v0
.end method

.method public getLayoutId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public getModuleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    const-string v0, "TournamentTeamDetailsPage"

    return-object v0
.end method

.method public onCreateFinished()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method
