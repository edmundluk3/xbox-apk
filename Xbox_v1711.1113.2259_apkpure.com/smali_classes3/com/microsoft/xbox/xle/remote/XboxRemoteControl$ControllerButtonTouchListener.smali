.class Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;
.super Ljava/lang/Object;
.source "XboxRemoteControl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControllerButtonTouchListener"
.end annotation


# static fields
.field private static final BUTTON_ANIM_DURATION:I = 0x32


# instance fields
.field private gamePadButtons:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field private final initialTranslationX:F

.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Landroid/view/View;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "gamePadButtons"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .prologue
    .line 496
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497
    invoke-virtual {p2}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->initialTranslationX:F

    .line 498
    iput-object p2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->view:Landroid/view/View;

    .line 499
    iput-object p3, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->gamePadButtons:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 500
    return-void
.end method

.method private animate(FF)V
    .locals 4
    .param p1, "startX"    # F
    .param p2, "endX"    # F

    .prologue
    .line 531
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->view:Landroid/view/View;

    const-string v1, "translationX"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    aput p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x32

    .line 532
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 535
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 504
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->view:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 506
    const/4 v1, 0x0

    .line 507
    .local v1, "handled":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 508
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 526
    :goto_0
    :pswitch_0
    return v1

    .line 510
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->gamePadButtons:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    .line 511
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$200(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V

    .line 513
    iget v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->initialTranslationX:F

    invoke-direct {p0, v2, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->animate(FF)V

    .line 515
    const/4 v1, 0x1

    .line 516
    goto :goto_0

    .line 520
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    .line 521
    iget v2, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->initialTranslationX:F

    invoke-direct {p0, v4, v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$ControllerButtonTouchListener;->animate(FF)V

    .line 523
    const/4 v1, 0x1

    goto :goto_0

    .line 508
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
