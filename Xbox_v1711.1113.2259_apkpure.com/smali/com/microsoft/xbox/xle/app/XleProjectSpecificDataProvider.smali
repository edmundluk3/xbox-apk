.class public Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;
.super Ljava/lang/Object;
.source "XleProjectSpecificDataProvider.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$ContentRestrictions;
    }
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;


# instance fields
.field private androidId:Ljava/lang/String;

.field private blockFeaturedChild:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gotSettings:Z

.field private isMeAdult:Z

.field public languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private musicBlocked:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private promotionalRestrictedRegions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseBlocked:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private videoBlocked:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->instance:Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->musicBlocked:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->videoBlocked:Ljava/util/Set;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->purchaseBlocked:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->blockFeaturedChild:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->promotionalRestrictedRegions:Ljava/util/Set;

    .line 65
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;)V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->prefs()Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 71
    return-void
.end method

.method private addRegions(Ljava/lang/String;Ljava/util/Set;)V
    .locals 5
    .param p1, "locales"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "blockSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 89
    const-string v2, "[|]"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "list":[Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 91
    invoke-interface {p2}, Ljava/util/Set;->clear()V

    .line 92
    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 93
    .local v1, "region":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 94
    invoke-interface {p2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "list":[Ljava/lang/String;
    .end local v1    # "region":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->instance:Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->ensureDisplayLocale()V

    return-void
.end method


# virtual methods
.method public ensureDisplayLocale()V
    .locals 4

    .prologue
    .line 78
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->getLanguageInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->getLocale()Ljava/util/Locale;

    move-result-object v1

    .line 81
    .local v1, "displayLocale":Ljava/util/Locale;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 82
    .local v2, "dm":Landroid/util/DisplayMetrics;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 83
    .local v0, "conf":Landroid/content/res/Configuration;
    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 84
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 85
    return-void
.end method

.method public getAllowExplicitContent()Z
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method public getAutoSuggestdDataSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    const-string v0, "bbxall2"

    return-object v0
.end method

.method public getCombinedContentRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, ""

    return-object v0
.end method

.method public getContentRestrictions()Ljava/lang/String;
    .locals 6

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v3

    .line 345
    .local v3, "region":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getMeMaturityLevel()I

    move-result v2

    .line 346
    .local v2, "maturityLevel":I
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xff

    if-eq v2, v4, :cond_0

    if-eqz v2, :cond_0

    .line 347
    new-instance v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$ContentRestrictions;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPromotionalRestricted()Z

    move-result v4

    invoke-direct {v0, p0, v3, v2, v4}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$ContentRestrictions;-><init>(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;Ljava/lang/String;IZ)V

    .line 348
    .local v0, "contentRestriction":Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$ContentRestrictions;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "jsonString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 350
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 354
    .end local v0    # "contentRestriction":Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$ContentRestrictions;
    .end local v1    # "jsonString":Ljava/lang/String;
    :goto_0
    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getCurrentSandboxID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->androidId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 232
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "android_id":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_2

    .line 236
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getGUID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->androidId:Ljava/lang/String;

    .line 242
    .end local v0    # "android_id":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->androidId:Ljava/lang/String;

    return-object v1

    .line 238
    .restart local v0    # "android_id":Ljava/lang/String;
    :cond_2
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->androidId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getInitializeComplete()Z
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsForXboxOne()Z
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x1

    return v0
.end method

.method public getIsFreeAccount()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public getIsXboxMusicSupported()Z
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x1

    return v0
.end method

.method public getLegalLocale()Ljava/lang/String;
    .locals 6

    .prologue
    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->getLanguageInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 169
    .local v0, "currentLocale":Ljava/util/Locale;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s-%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getMeMaturityLevel()I
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 124
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMaturityLevel()I

    move-result v1

    .line 127
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMembershipLevel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAccountTier()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 182
    const-string v0, "ProjectSpecificDataProvider"

    const-string v1, "The profile account tier info is null"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "Gold"

    .line 186
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAccountTier()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRegion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->getLocationInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getMarket()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseEDS31()Z
    .locals 1

    .prologue
    .line 296
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getUseEDS31()Z

    move-result v0

    return v0
.end method

.method public getVersionCheckUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    sget-object v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$2;->$SwitchMap$com$microsoft$xbox$toolkit$network$XboxLiveEnvironment$Environment:[I

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 250
    :pswitch_0
    const-string v0, "http://www.rtm.vint.xbox.com/en-US/Platform/Android/XboxLIVE/sgversion"

    .line 254
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "http://www.xbox.com/en-US/Platform/Android/XboxLIVE/sgversion"

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 320
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v0

    return v0
.end method

.method public getWindowsLiveClientId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$2;->$SwitchMap$com$microsoft$xbox$toolkit$network$XboxLiveEnvironment$Environment:[I

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 216
    :pswitch_0
    const-string v0, "0000000048093EE3"

    .line 221
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "0000000068036303"

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getXuidString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getUserXuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public gotSettings()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->gotSettings:Z

    return v0
.end method

.method public isFeaturedBlocked()Z
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMeAdult()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->blockFeaturedChild:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMeAdult()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMeAdult:Z

    return v0
.end method

.method public isMusicBlocked()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method public isPromotionalRestricted()Z
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMeAdult()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->promotionalRestrictedRegions:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurchaseBlocked()Z
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->purchaseBlocked:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isVideoBlocked()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public isViewingBroadcastsRestricted()Z
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMeAdult()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processContentBlockedList(Lcom/microsoft/xbox/xle/app/SmartglassSettings;)V
    .locals 2
    .param p1, "settings"    # Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    .prologue
    .line 102
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->VIDEO_BLOCKED:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->videoBlocked:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->addRegions(Ljava/lang/String;Ljava/util/Set;)V

    .line 103
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->MUSIC_BLOCKED:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->musicBlocked:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->addRegions(Ljava/lang/String;Ljava/util/Set;)V

    .line 104
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->PURCHASE_BLOCKED:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->purchaseBlocked:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->addRegions(Ljava/lang/String;Ljava/util/Set;)V

    .line 105
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->BLOCK_FEATURED_CHILD:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->blockFeaturedChild:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->addRegions(Ljava/lang/String;Ljava/util/Set;)V

    .line 106
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;->PROMOTIONAL_CONTENT_RESTRICTED_REGIONS:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->promotionalRestrictedRegions:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->addRegions(Ljava/lang/String;Ljava/util/Set;)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->gotSettings:Z

    .line 108
    return-void
.end method

.method public resetModels(Z)V
    .locals 1
    .param p1, "clearEverything"    # Z

    .prologue
    .line 268
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->resetMessagingStorage()V

    .line 269
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->reset()V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->reset()V

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->reset()V

    .line 272
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->reset()V

    .line 273
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->reset()V

    .line 274
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->reset()V

    .line 275
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->reset()V

    .line 276
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->reset()V

    .line 277
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleModel;->reset()V

    .line 278
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->onPause()V

    .line 279
    invoke-static {}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->reset()V

    .line 280
    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->reset()V

    .line 281
    invoke-static {}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->reset()V

    .line 282
    invoke-static {}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->reset()V

    .line 283
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->reset()V

    .line 284
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreModel;->reset()V

    .line 285
    invoke-static {}, Lcom/microsoft/xbox/service/model/ExperimentModel;->getInstance()Lcom/microsoft/xbox/service/model/ExperimentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ExperimentModel;->reset()V

    .line 286
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->reset()V

    .line 287
    return-void
.end method

.method public resetTokenForService()V
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider$1;-><init>(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadSend(Ljava/lang/Runnable;)V

    .line 311
    return-void
.end method

.method public setIsMeAdult(Z)V
    .locals 0
    .param p1, "isAdult"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMeAdult:Z

    .line 116
    return-void
.end method
