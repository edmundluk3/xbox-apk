.class public Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
.super Ljava/lang/Object;
.source "LanguageSettingsInteractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;,
        Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation
.end field

.field private final languageChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation
.end field

.field private final locationChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)V
    .locals 1
    .param p1, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 68
    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->languageChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->locationChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 81
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 61
    invoke-static {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 69
    invoke-static {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 76
    invoke-static {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    .line 63
    invoke-interface {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->getCurrentPrefs()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;->newLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->setUserDefinedLanguage(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 70
    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "languageSettingsRepository"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;->newLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->setUserDefinedLocation(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 77
    return-object v0
.end method


# virtual methods
.method public getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getLanguageChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->languageChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getLocationChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->locationChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
