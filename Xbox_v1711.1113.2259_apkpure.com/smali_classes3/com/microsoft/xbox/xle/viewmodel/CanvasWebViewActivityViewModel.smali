.class public Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "CanvasWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;,
        Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;,
        Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;
    }
.end annotation


# static fields
.field private static final CANVAS_STATE_ERROR:Ljava/lang/String;

.field private static final GRACEPERIOD:I = 0xbb8

.field private static final MEDIA_QUERY_PARAM_FORMAT:Ljava/lang/String; = "canonicalId=%s&mediaGroup=%s"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field private canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

.field private isInitialLoad:Z

.field private isLoading:Z

.field private launchUrl:Ljava/lang/String;

.field private needToLoadActivity:Z

.field private webViewListener:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->TAG:Ljava/lang/String;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0700f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->CANVAS_STATE_ERROR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 73
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Loading:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    invoke-direct {v2, p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$1;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->webViewListener:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    .line 76
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isLoading:Z

    .line 77
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isInitialLoad:Z

    .line 78
    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    .line 79
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->needToLoadActivity:Z

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 84
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 86
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v0

    .line 89
    .local v0, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 91
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Launching canvas activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 94
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isInitialLoad:Z

    return v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isInitialLoad:Z

    return p1
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isLoading:Z

    return p1
.end method

.method private isDirty()Z
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Loaded:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Error:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showOkCancelDailogWithRunnable(Ljava/lang/Runnable;)V
    .locals 7
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 306
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070344

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070343

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707c7

    .line 307
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$2;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Ljava/lang/Runnable;)V

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 312
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    .line 306
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 313
    return-void
.end method


# virtual methods
.method public drainNeedToLoadActivity()Z
    .locals 2

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->needToLoadActivity:Z

    .line 121
    .local v0, "value":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->needToLoadActivity:Z

    .line 122
    return v0
.end method

.method public exit()V
    .locals 0

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->goBack()V

    .line 279
    return-void
.end method

.method public getActivityTitleId()I
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getNowPlayingTitleId()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getCanvasInternalStateIsLoading()Z
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Loading:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCanvasState()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$3;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$CanvasWebViewActivityViewModel$CanvasInternalState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 131
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Splash:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getCompanionDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompanionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompanionIsHelp()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getIsHelp()Z

    move-result v0

    return v0
.end method

.method public getCompanionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsNowPlayingTileVisibleInAppBar()Z
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->getCanvasState()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;->Webview:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasViewState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsRemoteButtonVisibleInAppBar()Z
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Loaded:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLaunchUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getShowError()Z
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Error:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWebViewListener()Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->webViewListener:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;

    return-object v0
.end method

.method public getWhitelistTitleIds()[I
    .locals 4

    .prologue
    .line 188
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getAllowedTitleIds()Ljava/util/ArrayList;

    move-result-object v2

    .line 189
    .local v2, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 190
    .local v1, "list":[I
    if-nez v2, :cond_1

    .line 191
    const/4 v3, 0x0

    new-array v1, v3, [I

    .line 198
    :cond_0
    return-object v1

    .line 193
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [I

    .line 194
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 195
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getWhitelistUrls()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getWhitelistUrls()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected goBack()V
    .locals 0

    .prologue
    .line 283
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->goBack()V

    .line 284
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isLoading:Z

    return v0
.end method

.method public isRemoteButtonEnabled()Z
    .locals 2

    .prologue
    .line 172
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveViewModel(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->showOkCancelDailogWithRunnable(Ljava/lang/Runnable;)V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 238
    if-eqz p1, :cond_0

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    .line 241
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->updateLaunchUrl()V

    .line 242
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->updateAdapter()V

    .line 243
    return-void
.end method

.method public navigateToRemote()V
    .locals 0

    .prologue
    .line 274
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->showRemoteControl()V

    .line 275
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->showOkCancelDailogWithRunnable(Ljava/lang/Runnable;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->goBack()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 221
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 234
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 225
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    .line 226
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 227
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 251
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 252
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 256
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 257
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 260
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->Always:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 261
    return-void

    .line 260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reload()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->needToLoadActivity:Z

    .line 214
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->Loading:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->updateCanvasState(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;)V

    .line 215
    return-void
.end method

.method public setInitialLoad()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->isInitialLoad:Z

    .line 177
    return-void
.end method

.method public updateCanvasState(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;)V
    .locals 3
    .param p1, "newState"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    if-eq p1, v0, :cond_0

    .line 288
    const-string v0, "CanvasWebViewActivityViewModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update canvas state to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->canvasRealState:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;

    .line 290
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->updateAdapter()V

    .line 294
    :goto_0
    return-void

    .line 292
    :cond_0
    const-string v0, "CanvasWebViewActivityViewModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore update canvas state to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$CanvasInternalState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateLaunchUrl()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 203
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getActivityUrlString()Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "newUrl":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 206
    :cond_0
    const-string v1, "Canvas"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Need to update launch url old:%s, new %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    aput-object v6, v4, v5

    aput-object v0, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->launchUrl:Ljava/lang/String;

    .line 208
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->needToLoadActivity:Z

    .line 210
    :cond_1
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->updateAdapter()V

    .line 271
    return-void
.end method
