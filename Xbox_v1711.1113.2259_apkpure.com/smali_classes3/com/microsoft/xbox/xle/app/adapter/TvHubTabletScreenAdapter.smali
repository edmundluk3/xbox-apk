.class public Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TvHubTabletScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ProgressTask;,
        Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    }
.end annotation


# instance fields
.field private mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mPageContainer:Landroid/widget/FrameLayout;

.field private mPlayStatePullTimer:Ljava/util/Timer;

.field private mProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field final mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

.field private mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;)V
    .locals 6
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 52
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    .line 57
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;

    .line 60
    const v1, 0x7f0e0b65

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    .line 61
    const v1, 0x7f0e0b67

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 62
    const v1, 0x7f0e0b6f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v2

    const v3, 0x7f0e0b68

    const v4, 0x7f0e0b69

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/TvListingsScreen;

    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->initButton(IILjava/lang/Class;)Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    move-result-object v3

    aput-object v3, v1, v2

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v2

    const v3, 0x7f0e0b6a

    const v4, 0x7f0e0b6b

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;

    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->initButton(IILjava/lang/Class;)Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    move-result-object v3

    aput-object v3, v1, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    sget-object v2, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->RECENT_CHANNELS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v2

    const v3, 0x7f0e0b6c

    const v4, 0x7f0e0b6d

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/TvRecentChannelsScreen;

    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->initButton(IILjava/lang/Class;)Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    move-result-object v3

    aput-object v3, v1, v2

    .line 68
    const v1, 0x7f0e0b70

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPageContainer:Landroid/widget/FrameLayout;

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getLastScreen()I

    move-result v0

    .line 72
    .local v0, "startScreen":I
    if-ltz v0, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->LAST:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 73
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v0

    .line 75
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 77
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "EPGLoad"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->beginTrackPerformance(Ljava/lang/String;)V

    .line 80
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->switchScreen(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Z)V

    .line 82
    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p2, "x2"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->switchScreen(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->onTimerTick()V

    return-void
.end method

.method private initButton(IILjava/lang/Class;)Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .locals 3
    .param p1, "buttonId"    # I
    .param p2, "buttonUnderlineId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;"
        }
    .end annotation

    .prologue
    .line 85
    .local p3, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;-><init>()V

    .line 86
    .local v0, "ret":Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    invoke-static {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$002(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Ljava/lang/Class;)Ljava/lang/Class;

    .line 88
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$102(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Lcom/microsoft/xbox/toolkit/ui/XLEButton;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 90
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$202(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Landroid/view/View;)Landroid/view/View;

    .line 92
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$1;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    return-object v0
.end method

.method private onTimerTick()V
    .locals 3

    .prologue
    .line 120
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v1

    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->formatTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "strTime":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method

.method private switchScreen(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Z)V
    .locals 9
    .param p1, "cache"    # Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;
    .param p2, "firstTime"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 126
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->updateNavState(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v6, 0x0

    .line 133
    .local v6, "created":Z
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 136
    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_7

    .line 137
    const/4 v8, 0x0

    .line 145
    .local v8, "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_1
    invoke-static {p1, v8}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$402(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 146
    const/4 v6, 0x1

    .line 149
    .end local v8    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPageContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPageContainer:Landroid/widget/FrameLayout;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setIsPivotPane(Z)V

    .line 159
    if-eqz v6, :cond_3

    .line 160
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    .line 163
    :cond_3
    if-nez p2, :cond_5

    .line 164
    if-eqz v6, :cond_4

    .line 165
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 166
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 168
    :cond_4
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 169
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onAnimateInStarted()V

    .line 172
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_6

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContentKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContent()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_6
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    goto/16 :goto_0

    .line 139
    :cond_7
    :try_start_1
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v8    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    goto/16 :goto_1

    .line 141
    .end local v8    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :catch_0
    move-exception v7

    .line 142
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "TvHubTabletScreenAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIXME: Failed to create a screen of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private updateNavState(Landroid/view/View;)V
    .locals 5
    .param p1, "selectedButton"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 184
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    if-ne v3, p1, :cond_0

    const/4 v1, 0x1

    .line 185
    .local v1, "isSelected":Z
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 186
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Landroid/view/View;

    move-result-object v4

    if-eqz v1, :cond_1

    move v3, v2

    :goto_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "isSelected":Z
    :cond_0
    move v1, v2

    .line 184
    goto :goto_1

    .line 186
    .restart local v1    # "isSelected":Z
    :cond_1
    const/16 v3, 0x8

    goto :goto_2

    .line 189
    .end local v1    # "isSelected":Z
    :cond_2
    return-void
.end method


# virtual methods
.method public getCurrentScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 196
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onApplicationBarIsHidden()V
    .locals 5

    .prologue
    .line 315
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 316
    .local v3, "top":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 317
    .local v1, "left":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 318
    .local v2, "right":I
    const/4 v0, 0x0

    .line 319
    .local v0, "bottom":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 320
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 322
    return-void
.end method

.method protected onApplicationBarIsShown()V
    .locals 6

    .prologue
    .line 326
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 327
    .local v3, "top":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 328
    .local v1, "left":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 329
    .local v2, "right":I
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f09057a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    .line 330
    .local v0, "bottom":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 331
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 333
    return-void
.end method

.method public onApplicationBarVisibilityChanged(Z)V
    .locals 0
    .param p1, "isApplicationBarShown"    # Z

    .prologue
    .line 306
    if-eqz p1, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->onApplicationBarIsShown()V

    .line 311
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->onApplicationBarIsHidden()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 289
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onDestroy()V

    .line 290
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 256
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    .line 258
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onPause()V

    .line 260
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 270
    const v0, 0xea60

    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    rem-long/2addr v2, v4

    long-to-int v1, v2

    sub-int v7, v0, v1

    .line 272
    .local v7, "leftToMinute":I
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mPlayStatePullTimer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ProgressTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ProgressTask;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$1;)V

    int-to-long v2, v7

    const-wide/16 v4, 0x7530

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 276
    .end local v7    # "leftToMinute":I
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v6

    .line 277
    .local v6, "applicationBarManager":Lcom/microsoft/xbox/xle/app/ApplicationBarManager;
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getIsApplicationBarShown()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->onApplicationBarVisibilityChanged(Z)V

    .line 279
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onResume()V

    .line 281
    return-void
.end method

.method public onSetActive()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 205
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onSetActive()V

    .line 206
    return-void
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 214
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onSetInactive()V

    .line 215
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 223
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 224
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 233
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mCurrentScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 233
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 239
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mScreens:[Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;->access$402(Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter$ScreenCache;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    goto :goto_1

    .line 243
    :cond_3
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 244
    return-void
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    const v3, 0x7f020125

    .line 106
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->getCurrentScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v0

    .line 107
    .local v0, "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->forceUpdateViewImmediately()V

    .line 114
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mProfilePic:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->getProfilePicUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->onTimerTick()V

    .line 116
    return-void

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->mViewModel:Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->updateLoadingIndicator(Z)V

    goto :goto_0
.end method
