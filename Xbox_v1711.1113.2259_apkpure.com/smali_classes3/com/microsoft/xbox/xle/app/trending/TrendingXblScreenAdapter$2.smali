.class Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;
.super Ljava/lang/Object;
.source "TrendingXblScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->ensureInitialized()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCTrending;->trackTrendingFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->setFilter(I)V

    .line 102
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
