.class Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;
.super Ljava/lang/Object;
.source "PartyWebRtcRepository.java"

# interfaces
.implements Lorg/webrtc/DataChannel$Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->onRemoteDescription(Lorg/webrtc/SessionDescription;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferedAmountChange(J)V
    .locals 5
    .param p1, "l"    # J

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dataChannel.onBufferedAmountChange "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    return-void
.end method

.method public onMessage(Lorg/webrtc/DataChannel$Buffer;)V
    .locals 6
    .param p1, "buffer"    # Lorg/webrtc/DataChannel$Buffer;

    .prologue
    .line 359
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "dataChannel.onMessage"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    sget-object v2, Lcom/microsoft/xbox/xbservices/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    iget-object v3, p1, Lorg/webrtc/DataChannel$Buffer;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 361
    .local v0, "bufferString":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;

    .line 364
    .local v1, "message":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->type()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 365
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$400(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lio/reactivex/subjects/PublishSubject;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->from()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->data()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->type()Ljava/lang/String;

    move-result-object v2

    const-string v3, "join"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->mpsdMemberIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 368
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->mpsdMemberIndex()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->ssrc()Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$602(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Z)Z

    .line 374
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createOffer()V

    goto :goto_0
.end method

.method public onStateChange()V
    .locals 5

    .prologue
    .line 344
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->getDataChannel()Lorg/webrtc/DataChannel;

    move-result-object v0

    .line 346
    .local v0, "dataChannel":Lorg/webrtc/DataChannel;
    if-eqz v0, :cond_1

    .line 347
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dataChannel.onStateChange "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/webrtc/DataChannel;->state()Lorg/webrtc/DataChannel$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-virtual {v0}, Lorg/webrtc/DataChannel;->state()Lorg/webrtc/DataChannel$State;

    move-result-object v1

    sget-object v2, Lorg/webrtc/DataChannel$State;->OPEN:Lorg/webrtc/DataChannel$State;

    if-ne v1, v2, :cond_0

    .line 350
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$400(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lio/reactivex/subjects/PublishSubject;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDataChannelConnected;->INSTANCE:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDataChannelConnected;

    invoke-virtual {v1, v2}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Received dataChannel stateChange from null datachannel, peerConnection is shutting down"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
