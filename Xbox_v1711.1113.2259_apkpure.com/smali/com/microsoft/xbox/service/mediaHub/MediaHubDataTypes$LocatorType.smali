.class public final enum Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
.super Ljava/lang/Enum;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocatorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

.field public static final enum Download:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

.field public static final enum Thumbnail:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Thumbnail"
    .end annotation
.end field

.field public static final enum ThumbnailLarge:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Thumbnail_Large"
    .end annotation
.end field

.field public static final enum ThumbnailSmall:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Thumbnail_Small"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 388
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    const-string v1, "Download"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Download:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 390
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    const-string v1, "ThumbnailSmall"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->ThumbnailSmall:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 393
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    const-string v1, "ThumbnailLarge"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->ThumbnailLarge:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 396
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    const-string v1, "Thumbnail"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Thumbnail:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    .line 387
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Download:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->ThumbnailSmall:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->ThumbnailLarge:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Thumbnail:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 387
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 387
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    .locals 1

    .prologue
    .line 387
    sget-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    return-object v0
.end method


# virtual methods
.method public isThumbnail()Z
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Download:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
