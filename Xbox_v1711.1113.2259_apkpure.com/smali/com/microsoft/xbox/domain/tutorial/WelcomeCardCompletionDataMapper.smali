.class public final Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;
.super Ljava/lang/Object;
.source "WelcomeCardCompletionDataMapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public fromString(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p1, "json"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 19
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    const-class v1, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 22
    .local v0, "states":Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    if-nez v0, :cond_0

    .line 23
    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->defaultStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 22
    .end local v0    # "states":Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    :cond_0
    return-object v0
.end method

.method public toString(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Ljava/lang/String;
    .locals 1
    .param p1, "states"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
