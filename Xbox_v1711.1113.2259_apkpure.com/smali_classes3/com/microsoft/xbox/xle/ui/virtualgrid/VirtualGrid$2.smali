.class Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;
.super Ljava/lang/Object;
.source "VirtualGrid.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "list"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->updateHeaderVisibility()V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->mListener:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;

    invoke-interface {v0, p2, p3, p4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;->onVerticalScroll(III)V

    .line 150
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "list"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 156
    packed-switch p2, :pswitch_data_0

    .line 164
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 166
    .local v0, "state":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->setVerticalScrollState(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    .line 167
    return-void

    .line 158
    .end local v0    # "state":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 159
    .restart local v0    # "state":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    goto :goto_0

    .line 161
    .end local v0    # "state":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->DRAG:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .line 162
    .restart local v0    # "state":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
