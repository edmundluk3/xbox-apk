.class public final enum Lcom/microsoft/xbox/service/model/ActivityAlertActionType;
.super Ljava/lang/Enum;
.source "ActivityAlertActionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/ActivityAlertActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

.field public static final enum Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

.field public static final enum Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

.field public static final enum Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    const-string v1, "Like"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    new-instance v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    const-string v1, "Comment"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    new-instance v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    const-string v1, "Share"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->$VALUES:[Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ActivityAlertActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/ActivityAlertActionType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->$VALUES:[Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    return-object v0
.end method
