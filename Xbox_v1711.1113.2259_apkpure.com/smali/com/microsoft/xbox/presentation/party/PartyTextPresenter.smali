.class public Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "PartyTextPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyTextView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;)V
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 35
    invoke-static {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 48
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$5(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p2, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;)Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;->text()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 39
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;

    .line 43
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 44
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor;->getSendTextTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 38
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p2, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    invoke-static {p2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 37
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .locals 4
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 52
    instance-of v2, p2, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    if-eqz v2, :cond_1

    move-object v0, p2

    .line 53
    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    .line 54
    .local v0, "initialLoadResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->textStream()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->memberStream()Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->withStreams(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    move-result-object p1

    .line 60
    .end local v0    # "initialLoadResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    :cond_0
    :goto_0
    return-object p1

    .line 55
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    :cond_1
    instance-of v2, p2, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    if-eqz v2, :cond_0

    move-object v1, p2

    .line 56
    check-cast v1, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    .line 57
    .local v1, "partyStateResult":Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->withContent(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 66
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 67
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 68
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 69
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 71
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 72
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 74
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/party/PartyTextViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 75
    return-void
.end method
