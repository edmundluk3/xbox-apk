.class public final Lcom/microsoft/onlineid/sdk/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/onlineid/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final Theme_MSA:I = 0x7f080193

.field public static final Theme_MSA_Dialog:I = 0x7f080194

.field public static final Theme_MSA_DialogWhenLarge:I = 0x7f080195

.field public static final Theme_MSA_NoActionBar:I = 0x7f080196

.field public static final Theme_MSA_Transparent:I = 0x7f080197

.field public static final accountPickerListView:I = 0x7f0801f5

.field public static final accountPickerUserTile:I = 0x7f0801f6

.field public static final accountPickerUserTileUserName:I = 0x7f0801f7

.field public static final accountPickerUserTileUserNameBidirectional:I = 0x7f080000

.field public static final accountPickerUserTileUserNameRtl:I = 0x7f0801f8

.field public static final accountTile:I = 0x7f0801f9

.field public static final accountTileOverflowMenu:I = 0x7f0801fa

.field public static final actionBar:I = 0x7f0801fe

.field public static final activity:I = 0x7f0801ff

.field public static final button:I = 0x7f080218

.field public static final dividerButtons:I = 0x7f080243

.field public static final listView:I = 0x7f08027e

.field public static final overflowMenu:I = 0x7f08028a

.field public static final popupMessage:I = 0x7f08029b

.field public static final signOutCheckBox:I = 0x7f0802bf

.field public static final signOutCheckBoxBidirectional:I = 0x7f080001

.field public static final signOutCheckBoxRtl:I = 0x7f0802c0

.field public static final textDefault:I = 0x7f0802c9

.field public static final textHeader:I = 0x7f0802ca

.field public static final textLarge:I = 0x7f0802cb

.field public static final textLargest:I = 0x7f0802cc

.field public static final textLink:I = 0x7f0802cd

.field public static final textPopupMessageBody:I = 0x7f0802ce

.field public static final textPopupMessageHeader:I = 0x7f0802cf

.field public static final textStaticPage:I = 0x7f0802d0

.field public static final userTile:I = 0x7f0802dc

.field public static final userTileDisplayName:I = 0x7f0802dd

.field public static final userTileEmail:I = 0x7f0802de

.field public static final userTileImage:I = 0x7f0802df

.field public static final userTileImageBidirectional:I = 0x7f080002

.field public static final userTileImageRtl:I = 0x7f0802e0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
