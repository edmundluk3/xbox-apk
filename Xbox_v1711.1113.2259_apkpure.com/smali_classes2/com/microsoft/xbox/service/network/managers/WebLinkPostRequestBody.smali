.class public Lcom/microsoft/xbox/service/network/managers/WebLinkPostRequestBody;
.super Ljava/lang/Object;
.source "WebLinkPostRequestBody.java"


# instance fields
.field public final linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

.field public final postText:Ljava/lang/String;

.field public final postType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "webLinkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const-string v0, "link"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostRequestBody;->postType:Ljava/lang/String;

    .line 9
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostRequestBody;->postText:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostRequestBody;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .line 11
    return-void
.end method
