.class Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AutoConnectModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/AutoConnectModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoConnectToConsoleTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private console:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel$1;

    .prologue
    .line 284
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 309
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getLastConnectedConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 310
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-eqz v2, :cond_0

    .line 312
    const-string v2, "AutoConnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "try to connect last connected console "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$500(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v1

    .line 314
    .local v1, "status":Z
    if-eqz v1, :cond_0

    .line 315
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 362
    .end local v1    # "status":Z
    :goto_0
    return-object v2

    .line 319
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$400(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 320
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 324
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 325
    .local v0, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 326
    const-string v2, "AutoConnect"

    const-string v3, "failed to get default console, fail"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 330
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$400(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 331
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 334
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getDefaultConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 337
    :try_start_0
    const-string v2, "https://xboxlive.com"

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getXTokenString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-eqz v2, :cond_5

    .line 342
    const-string v2, "AutoConnect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "try to connect to default console "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$500(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)Z

    move-result v1

    .line 344
    .restart local v1    # "status":Z
    if-eqz v1, :cond_4

    .line 345
    new-instance v2, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask$1;-><init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadSend(Ljava/lang/Runnable;)V

    .line 355
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 357
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 361
    .end local v1    # "status":Z
    :cond_5
    const-string v2, "AutoConnect"

    const-string v3, "could not get default console, fail"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    .line 338
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 302
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 296
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$300(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 298
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$300(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 375
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 284
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/model/AutoConnectModel$AutoConnectToConsoleTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 368
    return-void
.end method
