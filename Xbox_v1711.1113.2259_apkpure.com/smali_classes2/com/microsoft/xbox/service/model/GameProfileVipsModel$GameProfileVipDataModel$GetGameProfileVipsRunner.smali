.class Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GameProfileVipsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProfileVipsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final MAX_NUM_VIPS:I = 0xa


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 89
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->access$000(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->access$100(Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;)J

    move-result-wide v6

    const/16 v8, 0xa

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileVips(Ljava/lang/String;JI)Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 94
    .local v4, "vipsData":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;>;"
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 95
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v3, "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPeopleHubBatchSummaries(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v2

    .line 98
    .local v2, "summaries":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v2, :cond_2

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 99
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 100
    .local v0, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    new-instance v1, Lcom/microsoft/xbox/service/model/GameProfileVipData;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/service/model/GameProfileVipData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 101
    .local v1, "player":Lcom/microsoft/xbox/service/model/GameProfileVipData;
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 102
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/model/GameProfileVipData;->updateVipInfo(Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;)V

    .line 104
    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    .end local v0    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v1    # "player":Lcom/microsoft/xbox/service/model/GameProfileVipData;
    .end local v2    # "summaries":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v3    # "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;"
    :cond_1
    const/4 v3, 0x0

    :cond_2
    return-object v3
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 124
    const-wide/16 v0, 0xb

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/GameProfileVipData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel$GetGameProfileVipsRunner;->this$0:Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel$GameProfileVipDataModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 119
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method
