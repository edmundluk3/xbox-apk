.class final Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable;
.super Lio/reactivex/Observable;
.source "ToolbarNavigationClickObservable.java"


# annotations
.annotation build Landroid/support/annotation/RequiresApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final view:Landroid/widget/Toolbar;


# direct methods
.method constructor <init>(Landroid/widget/Toolbar;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/Toolbar;

    .prologue
    .line 19
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable;->view:Landroid/widget/Toolbar;

    .line 21
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Ljava/lang/Object;>;"
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/internal/Preconditions;->checkMainThread(Lio/reactivex/Observer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    :goto_0
    return-void

    .line 27
    :cond_0
    new-instance v0, Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable$Listener;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable;->view:Landroid/widget/Toolbar;

    invoke-direct {v0, v1, p1}, Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable$Listener;-><init>(Landroid/widget/Toolbar;Lio/reactivex/Observer;)V

    .line 28
    .local v0, "listener":Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable$Listener;
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 29
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/ToolbarNavigationClickObservable;->view:Landroid/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
