.class public Lcom/microsoft/xbox/data/repository/RepositoryModule;
.super Ljava/lang/Object;
.source "RepositoryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideActivityFeedFilterRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;)Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .locals 1
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;)V

    return-object v0
.end method

.method provideClubWatchRepository(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .locals 6
    .param p1, "beamService"    # Lcom/microsoft/xbox/data/service/beam/BeamService;
    .param p2, "mediaHubService"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .param p3, "dataMapper"    # Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;
    .param p4, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p5, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;-><init>(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    return-object v0
.end method

.method provideHomeScreenRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 74
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)V

    return-object v0
.end method

.method provideHoverChatHeadRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    return-object v0
.end method

.method provideLanguageSettingsRespository(Landroid/content/SharedPreferences;)Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method provideOOBESettingsRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;
    .locals 1
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)V

    return-object v0
.end method

.method provideTutorialRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    return-object v0
.end method

.method provideUserSummaryRepository(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .locals 1
    .param p1, "peopleHubService"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
    .param p3, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;-><init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    return-object v0
.end method

.method provideWelcomeCardCompletionRepository(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .locals 1
    .param p1, "dataMapper"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;
    .param p2, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 98
    new-instance v0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;

    invoke-direct {v0, p2, p1, p3}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    return-object v0
.end method
