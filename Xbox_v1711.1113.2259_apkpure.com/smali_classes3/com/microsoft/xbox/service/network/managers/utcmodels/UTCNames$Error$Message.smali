.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error$Message;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Message"
.end annotation


# static fields
.field public static final OOBEInvalidSetupCode:Ljava/lang/String; = "OOBE - Invalid setup code"

.field public static final OOBESaveSettingsFailure:Ljava/lang/String; = "OOBE - Could not save settings"

.field public static final PartyChatBlocked:Ljava/lang/String; = "Party - Party chat is blocked by settings or enforcement actions"

.field public static final PartyChatDoesntExist:Ljava/lang/String; = "Party - Party doesn\'t exist"

.field public static final PartyChatFailedToChangeRestriction:Ljava/lang/String; = "Party - Party chat failed to change party restriction"

.field public static final PartyChatFailedToInvite:Ljava/lang/String; = "Party - Party chat failed to invite"

.field public static final PartyChatFailedToJoin:Ljava/lang/String; = "Party - Party - Party chat failed to join party"

.field public static final PartyChatFailedToKick:Ljava/lang/String; = "Party - Party chat failed to kick user"

.field public static final PartyChatFailedToLeave:Ljava/lang/String; = "Party - Party chat failed to leave party"

.field public static final PartyChatFailedToLoadDetails:Ljava/lang/String; = "Party - Party chat failed to load party details"

.field public static final PartyChatFailedToMute:Ljava/lang/String; = "Party - Party chat failed to mute user"

.field public static final PartyChatFailedToSendTextMessage:Ljava/lang/String; = "Party - Party chat failed to send text message"

.field public static final PartyChatFailedToStart:Ljava/lang/String; = "Party - Party chat failed to start"

.field public static final PartyChatFailedToUploadPingTime:Ljava/lang/String; = "Party - Party chat failed to upload ping time"

.field public static final PartyChatFull:Ljava/lang/String; = "Party - Party chat is full"

.field public static final PartyChatGeneric:Ljava/lang/String; = "Party - Party chat generic error"

.field public static final PartyChatInvalidInvitation:Ljava/lang/String; = "Party - Party chat invitaion is no longer valid"

.field public static final PartyChatInviteExceedsCapacity:Ljava/lang/String; = "Party - Party chat invite exceeds capacity"

.field public static final PartyChatLocalNetwork:Ljava/lang/String; = "Party - Party chat local network error"

.field public static final PartyChatXBLNetworking:Ljava/lang/String; = "Party - Party chat service error"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
