.class public Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameProgressGameclipsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    }
.end annotation


# static fields
.field private static final GAME_PROGRESS_GAME_CLIPS_FILTER:Ljava/lang/String; = "GameProgressGameClipsFilter"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private captures:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;>;"
        }
    .end annotation
.end field

.field private isFilterChanged:Z

.field private isLoadingCaptures:Z

.field private lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

.field private likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

.field private loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

.field private loadingCapturesAsyncTask:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:J

.field private updateLikeControl:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 2
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 52
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 53
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    .line 54
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isFilterChanged:Z

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->titleId:J

    .line 65
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->onLoadCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isLoadingCaptures:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->onLikeCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateLikeControl:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateViewModelState()V

    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$0(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 0
    .param p0, "screenshot1"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    return-void
.end method

.method static synthetic lambda$navigateToScreenshot$1(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to increment screenshot count"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private loadSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 2
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 426
    .local p1, "captureData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->cancel()V

    .line 429
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

    .line 430
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->load(Z)V

    .line 431
    return-void
.end method

.method public static navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z
    .locals 4
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "ret":Z
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getScreenshotUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;

    move-result-object v1

    .line 134
    .local v1, "screenshotUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;
    if-eqz v1, :cond_0

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ScreenshotUri;->uri:Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 136
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->NavigateTo(Ljava/lang/Class;)V

    .line 138
    const/4 v0, 0x1

    .line 140
    :cond_0
    return v0
.end method

.method private onLikeCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "newLikeState"    # Z
    .param p3, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 413
    const-string v0, "GameProgressGameClips ViewModel"

    const-string v1, "onLikeCapturesCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 415
    if-eqz p2, :cond_1

    .line 416
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 423
    return-void

    .line 418
    :cond_1
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 241
    const-string v1, "GameProgressGameClips ViewModel"

    const-string v2, "onLoadCapturesCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 261
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 262
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 263
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-direct {p0, v1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 269
    :goto_1
    return-void

    .line 248
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 249
    .local v0, "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    if-eqz v0, :cond_0

    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 257
    .end local v0    # "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 258
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 265
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isLoadingCaptures:Z

    .line 266
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateViewModelState()V

    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    goto :goto_1

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 3

    .prologue
    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 233
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isLoadingCaptures:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 238
    :goto_1
    return-void

    .line 234
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 236
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method


# virtual methods
.method public deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "feedItemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 201
    const-string v0, "Cannot delete item from here"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .locals 2

    .prologue
    .line 84
    const-string v0, "GameProgressGameClipsFilter"

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->get(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    return-object v0
.end method

.method public getCommunityClips()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->captures:Ljava/util/Hashtable;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getGameName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 208
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsFilterChanged()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isFilterChanged:Z

    return v0
.end method

.method public getLastSelectedCapture()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 79
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 80
    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v0

    .line 108
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-ne v0, v1, :cond_4

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-eq v1, v2, :cond_2

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-ne v1, v2, :cond_3

    :cond_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    :goto_0
    return-object v1

    .line 110
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705ee

    .line 111
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 113
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-eq v1, v2, :cond_6

    .line 114
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyScreenshotsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-ne v1, v2, :cond_7

    :cond_6
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_7
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705f1

    .line 115
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isLoadingCaptures:Z

    return v0
.end method

.method public likeClick(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 5
    .param p1, "uncastItem"    # Ljava/lang/Object;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 346
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 347
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_1

    .line 350
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v4, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 351
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v2, :cond_3

    .line 352
    const/4 v1, 0x1

    .line 353
    .local v1, "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 358
    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateLikeControl:Z

    .line 359
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 361
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

    if-eqz v2, :cond_0

    .line 362
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->cancel()V

    .line 365
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;ZLjava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

    .line 366
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->load(Z)V

    .line 368
    .end local v1    # "newLikeState":Z
    :cond_1
    return-void

    .line 350
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 355
    :cond_3
    const/4 v1, 0x0

    .line 356
    .restart local v1    # "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto :goto_1
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 273
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;

    .line 274
    .local v1, "task":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->cancel()V

    goto :goto_0

    .line 277
    .end local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v0

    .line 278
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;

    .line 279
    .restart local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    if-nez v1, :cond_1

    .line 280
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;

    .end local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 281
    .restart local v1    # "task":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    :cond_1
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->load(Z)V

    .line 285
    return-void
.end method

.method public navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 163
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 164
    return-void
.end method

.method public navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 5
    .param p1, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 120
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getGameClipUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    move-result-object v0

    .line 121
    .local v0, "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    if-eqz v0, :cond_0

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedDataSource(Ljava/lang/String;)V

    .line 123
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 124
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setGameClip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Play Game DVR"

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 129
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public navigateToPostCommentScreen(Ljava/lang/String;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-static {p0, p1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 180
    :goto_0
    return-void

    .line 171
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User has no communication privileges"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 173
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 175
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 176
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 174
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 177
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 172
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V
    .locals 4
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .prologue
    .line 144
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->navigateToScreenshot(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 146
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/IncrementScreenshotViewCount;->execute(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lio/reactivex/Single;

    move-result-object v1

    .line 147
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 148
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 149
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 145
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot View Count"

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Screenshot Display"

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->lastSelectedCapture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 159
    :cond_0
    return-void
.end method

.method public navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 184
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 196
    :goto_0
    return-void

    .line 187
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User has no share privileges"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 189
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 191
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ac

    .line 192
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 190
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 193
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 188
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->titleId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 220
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->loadingCapturesAsyncTask:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 225
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadCaptureAsyncTask;->cancel()V

    goto :goto_0

    .line 228
    :cond_0
    return-void
.end method

.method public resetLikeControlUpdate()V
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateLikeControl:Z

    .line 342
    return-void
.end method

.method public setGameClipsFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Z)V
    .locals 1
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .param p2, "manually"    # Z

    .prologue
    .line 92
    const-string v0, "GameProgressGameClipsFilter"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->set(Ljava/lang/String;Ljava/lang/Enum;)Z

    .line 93
    if-eqz p2, :cond_0

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isFilterChanged:Z

    .line 97
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->load(Z)V

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateViewModelState()V

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 100
    return-void
.end method

.method public shouldUpdateLikeControl()Z
    .locals 1

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateLikeControl:Z

    return v0
.end method
