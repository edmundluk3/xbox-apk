.class Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;
.super Ljava/lang/Object;
.source "HoverChatHeadImageDownload.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageHoverChatHeadImageDownloader"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

.field private final messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final repository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-class v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
    .param p2, "repository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .line 117
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->repository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 119
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;
    .param p2, "x1"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .param p3, "x2"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V

    return-void
.end method

.method static synthetic lambda$getImagesUrls$0(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)Lcom/google/common/collect/ImmutableList;
    .locals 1
    .param p0, "summary"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getImagesUrls()Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v4, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v5, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v5}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    .line 125
    .local v0, "conversationSummary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 126
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-static {v4}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v4

    .line 141
    :goto_0
    return-object v4

    .line 128
    :cond_0
    const-wide/16 v2, 0x0

    .line 131
    .local v2, "xuid":J
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 136
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 137
    iget-object v4, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->repository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->load(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v5

    .line 138
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    .line 139
    invoke-virtual {v4}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v4

    goto :goto_0

    .line 132
    :catch_0
    move-exception v1

    .line 133
    .local v1, "ex":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to parse xuid from: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-virtual {v6}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 141
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :cond_1
    const-string v4, "no_image_available"

    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-static {v4}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v4

    goto :goto_0
.end method
