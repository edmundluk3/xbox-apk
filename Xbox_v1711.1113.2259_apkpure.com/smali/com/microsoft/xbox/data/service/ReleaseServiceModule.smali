.class public Lcom/microsoft/xbox/data/service/ReleaseServiceModule;
.super Ljava/lang/Object;
.source "ReleaseServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/microsoft/xbox/data/service/ServiceModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideHttpLoggingInterceptor$0(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 21
    const-string v0, "OkHttp"

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method provideHttpLoggingInterceptor()Lokhttp3/logging/HttpLoggingInterceptor;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lokhttp3/logging/HttpLoggingInterceptor;

    invoke-static {}, Lcom/microsoft/xbox/data/service/ReleaseServiceModule$$Lambda$1;->lambdaFactory$()Lokhttp3/logging/HttpLoggingInterceptor$Logger;

    move-result-object v1

    invoke-direct {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;-><init>(Lokhttp3/logging/HttpLoggingInterceptor$Logger;)V

    .line 22
    .local v0, "interceptor":Lokhttp3/logging/HttpLoggingInterceptor;
    sget-object v1, Lokhttp3/logging/HttpLoggingInterceptor$Level;->BASIC:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    invoke-virtual {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;->setLevel(Lokhttp3/logging/HttpLoggingInterceptor$Level;)Lokhttp3/logging/HttpLoggingInterceptor;

    .line 23
    return-object v0
.end method
