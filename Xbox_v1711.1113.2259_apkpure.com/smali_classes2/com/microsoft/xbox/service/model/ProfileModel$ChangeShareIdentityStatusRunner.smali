.class Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeShareIdentityStatusRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

.field private shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 2657
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2658
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 2659
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2660
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .line 2661
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "shareIdentityStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .param p4, "setting"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .prologue
    .line 2663
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2664
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 2665
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2666
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    .line 2667
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2671
    new-instance v1, Lcom/microsoft/xbox/service/model/sls/ChangeShareIdentityRequest;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/sls/ChangeShareIdentityRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/sls/ChangeShareIdentityRequest;->getChangeShareIdentityRequestBody(Lcom/microsoft/xbox/service/model/sls/ChangeShareIdentityRequest;)Ljava/lang/String;

    move-result-object v0

    .line 2672
    .local v0, "postBody":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setUserProfileShareIdentitySetting(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2652
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2682
    const-wide/16 v0, 0xf9a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2687
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->setting:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeShareIdentityStatusRunner;->shareIdentityStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$1800(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 2688
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2677
    return-void
.end method
