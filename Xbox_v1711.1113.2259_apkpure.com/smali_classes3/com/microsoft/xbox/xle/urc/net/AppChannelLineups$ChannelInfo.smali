.class public Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;
.super Ljava/lang/Object;
.source "AppChannelLineups.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChannelInfo"
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "provider"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;
    .param p2, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v0, "id"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->id:Ljava/lang/String;

    .line 71
    const-string v0, "name"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->name:Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    .line 73
    return-void
.end method


# virtual methods
.method public isTheSameChannel(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    .prologue
    .line 76
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
