.class public Lcom/microsoft/xbox/toolkit/DataLoaderTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "DataLoaderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<TT;>;>;"
    }
.end annotation


# static fields
.field private static final DATA_WAIT_TIMEOUT_MS:I = 0x3e8


# instance fields
.field private dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
            "<TT;>;"
        }
    .end annotation
.end field

.field private exception:Lcom/microsoft/xbox/toolkit/XLEException;

.field private lastInvalidatedTick:J

.field private requestStartTime:J

.field private shouldCheckTimeStamp:Z

.field private stopwatch:Lcom/microsoft/xbox/toolkit/TimeMonitor;


# direct methods
.method public constructor <init>(JLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V
    .locals 3
    .param p1, "lastInvalidatedTick"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    .local p3, "runnable":Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;, "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 21
    new-instance v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->stopwatch:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 30
    iput-wide p1, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->lastInvalidatedTick:J

    .line 31
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->requestStartTime:J

    .line 33
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->shouldCheckTimeStamp:Z

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->shouldCheckTimeStamp:Z

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    .local p1, "runnable":Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;, "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable<TT;>;"
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(JLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 42
    return-void
.end method


# virtual methods
.method public doInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    const/4 v0, 0x0

    .line 48
    .local v0, "data":Ljava/lang/Object;, "TT;"
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;->buildData()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    .end local v0    # "data":Ljava/lang/Object;, "TT;"
    :goto_0
    const-string v1, "DataLoadTask"

    const-string v2, "async task done"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->waitForReady(I)V

    .line 74
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->exception:Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v1, v0, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    return-object v1

    .line 49
    .restart local v0    # "data":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v5

    .line 50
    .local v5, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 53
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;->getDefaultErrorCode()J

    move-result-wide v2

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;->getUserObject()Ljava/lang/Object;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->exception:Lcom/microsoft/xbox/toolkit/XLEException;

    .line 55
    instance-of v1, v5, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v1, :cond_1

    move-object v1, v5

    .line 56
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v8

    .line 57
    .local v8, "errorCode":J
    const-wide/16 v2, 0x3ed

    cmp-long v1, v8, v2

    if-eqz v1, :cond_0

    const-wide/16 v2, 0x404

    cmp-long v1, v8, v2

    if-nez v1, :cond_2

    :cond_0
    move-object v1, v5

    .line 58
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->exception:Lcom/microsoft/xbox/toolkit/XLEException;

    .line 68
    .end local v8    # "errorCode":J
    :cond_1
    :goto_1
    const-string v1, "DataLoaderTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Caught an exception during background operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    .restart local v8    # "errorCode":J
    :cond_2
    const-wide/16 v2, 0x3ee

    cmp-long v1, v8, v2

    if-eqz v1, :cond_3

    const-wide/16 v2, 0x2

    cmp-long v1, v8, v2

    if-nez v1, :cond_1

    .line 64
    :cond_3
    const-string v1, "DataLoaderTask"

    const-string v2, "INVALID_TOKEN error code received. Returning generic error code for the model."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->doInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 90
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->shouldCheckTimeStamp:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->requestStartTime:J

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->lastInvalidatedTick:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 93
    new-instance p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    .end local p1    # "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    const/4 v0, 0x0

    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xa

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {p1, v0, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    .line 94
    .restart local p1    # "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    const-string v0, "DataLoaderTask"

    const-string v1, "Invalidated an incoming data packet because it was deemed out of date"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 100
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->exception:Lcom/microsoft/xbox/toolkit/XLEException;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->dataLoadRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;->onPreExecute()V

    .line 84
    return-void
.end method
