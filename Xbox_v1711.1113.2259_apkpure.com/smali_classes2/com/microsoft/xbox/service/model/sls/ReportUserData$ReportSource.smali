.class public final enum Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
.super Ljava/lang/Enum;
.source "ReportUserData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/sls/ReportUserData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReportSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum ClubsChat:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum ClubsProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum Comment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum Gameclip:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum LfgHost:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum LfgUser:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum Message:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum Screenshot:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum SocialBar:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public static final enum YouProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "SocialBar"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->SocialBar:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "Message"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Message:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "YouProfile"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->YouProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "Comment"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Comment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "Gameclip"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Gameclip:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "Screenshot"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Screenshot:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "LfgUser"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgUser:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "LfgHost"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgHost:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "Club"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "ClubsProfile"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "ClubsFeed"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "ClubsComment"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    const-string v1, "ClubsChat"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsChat:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 26
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->SocialBar:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Message:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->YouProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Comment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Gameclip:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Screenshot:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgUser:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgHost:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsProfile:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsComment:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsChat:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->$VALUES:[Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->$VALUES:[Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    return-object v0
.end method
