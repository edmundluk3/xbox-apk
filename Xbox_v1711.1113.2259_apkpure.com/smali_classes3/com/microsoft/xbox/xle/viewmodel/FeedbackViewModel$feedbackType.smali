.class public final enum Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;
.super Ljava/lang/Enum;
.source "FeedbackViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "feedbackType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

.field public static final enum crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

.field public static final enum negative:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

.field public static final enum positive:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    const-string v1, "crash"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    const-string v1, "positive"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->positive:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    const-string v1, "negative"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->negative:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->positive:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->negative:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    return-object v0
.end method
