.class public abstract Lcom/microsoft/xbox/idp/compat/BaseActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "BaseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public addFragment(ILcom/microsoft/xbox/idp/compat/BaseFragment;)V
    .locals 1
    .param p1, "fragmentId"    # I
    .param p2, "fragment"    # Lcom/microsoft/xbox/idp/compat/BaseFragment;

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/compat/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 12
    return-void
.end method

.method public hasFragment(I)Z
    .locals 1
    .param p1, "fragmentId"    # I

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/compat/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
