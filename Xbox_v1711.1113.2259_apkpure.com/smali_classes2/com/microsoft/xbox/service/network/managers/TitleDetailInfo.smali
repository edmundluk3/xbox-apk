.class public Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;
.super Ljava/lang/Object;
.source "TitleDetailInfo.java"


# instance fields
.field private id:I

.field private scid:Ljava/lang/String;

.field private titleType:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "scid"    # Ljava/lang/String;

    .prologue
    .line 9
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 10
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "titleType"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->id:I

    .line 14
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->scid:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->titleType:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getAchievementId()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->id:I

    return v0
.end method

.method public getServiceConfigId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->scid:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->titleType:Ljava/lang/String;

    return-object v0
.end method
