.class public Lcom/google/android/exoplayer2/util/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/util/Logger$Module;
    }
.end annotation


# static fields
.field private static final enabledModules:[I


# instance fields
.field private mModule:I

.field private mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->All:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    .line 64
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/util/Logger$Module;Ljava/lang/String;)V
    .locals 2
    .param p1, "module"    # Lcom/google/android/exoplayer2/util/Logger$Module;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Unknown:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    .line 72
    if-nez p2, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    iput-object p2, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    .line 77
    return-void
.end method

.method public static setLogLevel(Lcom/google/android/exoplayer2/util/Logger$Module;I)V
    .locals 2
    .param p0, "module"    # Lcom/google/android/exoplayer2/util/Logger$Module;
    .param p1, "logLevel"    # I

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->All:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_2

    .line 94
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([II)V

    .line 98
    :goto_0
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideo:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 99
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideoCommon:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v1

    aput p1, v0, v1

    .line 101
    :cond_0
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideo:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/Logger$Module;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_1

    .line 102
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v1

    aput p1, v0, v1

    .line 103
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Video:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v1

    aput p1, v0, v1

    .line 105
    :cond_1
    return-void

    .line 96
    :cond_2
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v1

    aput p1, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public allowDebug()Z
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    iget v1, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    aget v0, v0, v1

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public allowVerbose()Z
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    iget v1, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    aget v0, v0, v1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    iget v1, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    aget v0, v0, v1

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 183
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void
.end method

.method public setModule(Lcom/google/android/exoplayer2/util/Logger$Module;)V
    .locals 1
    .param p1, "module"    # Lcom/google/android/exoplayer2/util/Logger$Module;

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/Logger$Module;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    .line 124
    return-void
.end method

.method public setTAG(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Tag"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public v(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger;->enabledModules:[I

    iget v1, p0, Lcom/google/android/exoplayer2/util/Logger;->mModule:I

    aget v0, v0, v1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/util/Logger;->mTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return-void
.end method
