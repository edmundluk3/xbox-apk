.class Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;
.super Ljava/lang/Object;
.source "TvStreamerModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 75
    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 76
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 94
    :cond_0
    return-void

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$000(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 81
    const-string v1, "TvStreamerModel"

    const-string v2, "Video stream timed out. Setting loading state to false"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->closeStream(Z)V

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$200(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;

    .line 90
    .local v0, "listener":Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070c66

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;->onStreamError(Ljava/lang/String;Z)V

    goto :goto_0
.end method
