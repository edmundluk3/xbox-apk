.class Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1;
.super Ljava/lang/Object;
.source "ProviderPickerDialogViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1;->val$mediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 4
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    const/4 v3, 0x1

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    .line 48
    .local v0, "popup":Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$1;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen;-><init>(Lcom/microsoft/xbox/xle/app/activity/HomeScreenPopupScreen$ViewModelProvider;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 54
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 55
    return-void
.end method
