.class public Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
.super Lcom/microsoft/xbox/xle/ui/XLERootView;
.source "FriendPickerViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;
    }
.end annotation


# instance fields
.field cancelButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05cf
    .end annotation
.end field

.field private choiceMode:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

.field listView:Landroid/widget/ListView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05d1
    .end annotation
.end field

.field okButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05ce
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field private selectedItem:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

.field switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05d0
    .end annotation
.end field

.field title:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e05c6
    .end annotation
.end field

.field private titleStringResId:I

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 81
    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "titleStringResId"    # I
    .param p4, "choiceMode"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p5, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;>;"
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLERootView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    iput p3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->titleStringResId:I

    .line 87
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->choiceMode:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .line 88
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 89
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->init(Landroid/content/Context;)V

    .line 90
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030109

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 94
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->titleStringResId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03010a

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->choiceMode:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->getListMode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listView:Landroid/widget/ListView;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->okButton:Landroid/view/View;

    .line 125
    invoke-static {v0}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 126
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 138
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->cancelButton:Landroid/view/View;

    .line 141
    invoke-static {v1}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 142
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 143
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v1

    .line 124
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 146
    return-void
.end method

.method static synthetic lambda$init$0(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v3, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 105
    .local v0, "item":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    if-eqz v0, :cond_0

    .line 106
    sget-object v3, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$1;->$SwitchMap$com$microsoft$xbox$presentation$friendpicker$FriendPickerViewImpl$ChoiceMode:[I

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->choiceMode:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    invoke-virtual {v4}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 121
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    .line 122
    return-void

    .line 108
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->selectedItem:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    if-eqz v3, :cond_1

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->selectedItem:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 112
    :cond_1
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 113
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->selectedItem:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    goto :goto_0

    .line 117
    :pswitch_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic lambda$init$1(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 127
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v2, "xuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 130
    .local v1, "item":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    iget-object v3, v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    .end local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    :cond_1
    invoke-static {v2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    return-object v3
.end method

.method static synthetic lambda$init$2(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$init$3(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;
    .locals 1
    .param p0, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 142
    sget-object v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;

    return-object v0
.end method

.method static synthetic lambda$init$4(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/XLERootView;->onAttachedToWindow()V

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->presenter:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->onStart(Ljava/lang/Object;)V

    .line 152
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/XLERootView;->onDetachedFromWindow()V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->presenter:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;->onDestroy(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "viewState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 164
    .local v1, "userSummaryList":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 174
    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 175
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->clear()V

    .line 176
    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 177
    .local v0, "user":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 166
    .end local v0    # "user":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isError()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isContent()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 169
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 179
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    .line 181
    :cond_4
    return-void
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Lcom/microsoft/xbox/presentation/lce/LceViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
