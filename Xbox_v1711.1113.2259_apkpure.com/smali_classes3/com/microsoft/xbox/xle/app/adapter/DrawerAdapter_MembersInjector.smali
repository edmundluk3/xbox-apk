.class public final Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;
.super Ljava/lang/Object;
.source "DrawerAdapter_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final homeScreenRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final languageSettingsRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final systemSettingsModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "languageSettingsRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;>;"
    .local p2, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p3, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p4, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->languageSettingsRepositoryProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->systemSettingsModelProvider:Ljavax/inject/Provider;

    .line 37
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "languageSettingsRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;>;"
    .local p1, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p2, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p3, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectHomeScreenRepository(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 76
    return-void
.end method

.method public static injectLanguageSettingsRepository(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "languageSettingsRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    .line 66
    return-void
.end method

.method public static injectSystemSettingsModel(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 81
    return-void
.end method

.method public static injectTutorialRepository(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 71
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->languageSettingsRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->systemSettingsModelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 60
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->injectMembers(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)V

    return-void
.end method
