.class public final Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;
.super Ljava/lang/Object;
.source "ClubWatchRepositoryImpl_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final beamServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;"
        }
    .end annotation
.end field

.field private final clubModelManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "beamServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/beam/BeamService;>;"
    .local p2, "mediaHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;>;"
    .local p3, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;>;"
    .local p4, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    .local p5, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->beamServiceProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->mediaHubServiceProvider:Ljavax/inject/Provider;

    .line 37
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 39
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->clubModelManagerProvider:Ljavax/inject/Provider;

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 43
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "beamServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/beam/BeamService;>;"
    .local p1, "mediaHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;>;"
    .local p2, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;>;"
    .local p3, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    .local p4, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;
    .locals 6

    .prologue
    .line 47
    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->beamServiceProvider:Ljavax/inject/Provider;

    .line 48
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/service/beam/BeamService;

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->mediaHubServiceProvider:Ljavax/inject/Provider;

    .line 49
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 50
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    iget-object v4, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->clubModelManagerProvider:Ljavax/inject/Provider;

    .line 51
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iget-object v5, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 52
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;-><init>(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 47
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl_Factory;->get()Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl;

    move-result-object v0

    return-object v0
.end method
