.class public Lcom/microsoft/xbox/xle/epg/LiveTvUtils;
.super Ljava/lang/Object;
.source "LiveTvUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LiveTvUtils"

.field private static sIsSessionActive:Z = false

.field private static sIsSessionNested:Z = false

.field private static final sResumeThreshold:J = 0x493e0L

.field private static sSessionStart:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 211
    sput-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    .line 212
    sput-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionNested:Z

    .line 214
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sSessionStart:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppChannelInformationString(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Ljava/lang/String;
    .locals 5
    .param p0, "channelItem"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    .line 86
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .local v2, "info":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getEpisodeName()Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "episodeName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getDuration()I

    move-result v0

    .line 95
    .local v0, "duration":I
    if-lez v0, :cond_2

    .line 96
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 97
    const-string v4, " | "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->formatDuration(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getParentalRating()Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "parentalRating":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 105
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 106
    const-string v4, " | "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static getShowtimeForProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Landroid/content/Context;)Ljava/lang/String;
    .locals 14
    .param p0, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p1, "currentContext"    # Landroid/content/Context;

    .prologue
    const-wide/16 v12, 0x3e8

    .line 46
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v3

    int-to-long v4, v3

    .line 47
    .local v4, "timeStart":J
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getDuration()I

    move-result v3

    int-to-long v6, v3

    add-long v0, v4, v6

    .line 49
    .local v0, "timeFinish":J
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 51
    .local v2, "timeFormat":Ljava/text/DateFormat;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704b6

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v8, Ljava/util/Date;

    mul-long v10, v4, v12

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/util/Date;

    mul-long v10, v0, v12

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getStreamRelativeId(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "providerId"    # Ljava/lang/String;
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "appId"    # I

    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p0, "nil"

    .end local p0    # "providerId":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "nil"

    .end local p1    # "channelId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTVInformationString(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p1, "isChannelHD"    # Z
    .param p2, "currentContext"    # Landroid/content/Context;

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .local v0, "info":Ljava/lang/StringBuilder;
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getShowtimeForProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "showtime":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 61
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentalRating()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "parentalRating":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 68
    const-string v3, " | "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_2
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    if-eqz p1, :cond_3

    .line 75
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0703d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 77
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0703d4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static isContentAppropriate(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z
    .locals 6
    .param p0, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p1, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanViewAdultTVContent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v3

    .line 119
    :cond_1
    if-nez p1, :cond_2

    move v3, v4

    .line 120
    goto :goto_0

    .line 123
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getShowSpecificContentRatingsEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getNonAdultContentRatings()Ljava/util/Set;

    move-result-object v0

    .line 125
    .local v0, "nonAdultRatings":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentalRatingSystem()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "showRatingSystem":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentalRating()Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "showRating":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 128
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getTreatMissingRatingAsAdult()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0

    .line 130
    :cond_4
    new-instance v3, Landroid/util/Pair;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 133
    .end local v0    # "nonAdultRatings":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v1    # "showRating":Ljava/lang/String;
    .end local v2    # "showRatingSystem":Ljava/lang/String;
    :cond_5
    if-eqz p0, :cond_6

    .line 134
    invoke-interface {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->isAdult()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0

    :cond_6
    move v3, v4

    .line 137
    goto :goto_0
.end method

.method public static resetSettings()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    .line 302
    .local v0, "storage":Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setLastScreen(I)V

    .line 303
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setAppChannelLastChannel(I)V

    .line 304
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setAppChannelSelectedChannel(I)V

    .line 305
    return-void
.end method

.method public static shouldEnableStreaming(Ljava/lang/String;)Z
    .locals 3
    .param p0, "headendId"    # Ljava/lang/String;

    .prologue
    .line 196
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 197
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 198
    .local v0, "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamingCanBeEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->canStream()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static shouldShowURC(Ljava/lang/String;)Z
    .locals 3
    .param p0, "headendId"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 203
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static startTrackingSession()V
    .locals 12

    .prologue
    .line 217
    sget-boolean v8, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    if-eqz v8, :cond_0

    .line 247
    .local v0, "currentTime":J
    .local v3, "previousSessionId":Ljava/lang/String;
    .local v4, "previousSessionDuration":J
    .local v6, "previousSessionEnd":J
    :goto_0
    return-void

    .line 220
    .end local v0    # "currentTime":J
    .end local v3    # "previousSessionId":Ljava/lang/String;
    .end local v4    # "previousSessionDuration":J
    .end local v6    # "previousSessionEnd":J
    :cond_0
    const/4 v8, 0x1

    sput-boolean v8, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    .line 222
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getSessionEndTime()J

    move-result-wide v6

    .line 223
    .restart local v6    # "previousSessionEnd":J
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getSessionID()Ljava/lang/String;

    move-result-object v3

    .line 224
    .restart local v3    # "previousSessionId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getSessionDuration()J

    move-result-wide v4

    .line 225
    .restart local v4    # "previousSessionDuration":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 228
    .restart local v0    # "currentTime":J
    if-eqz v3, :cond_1

    sub-long v8, v0, v6

    const-wide/32 v10, 0x493e0

    cmp-long v8, v8, v10

    if-gez v8, :cond_1

    .line 229
    const-string v8, "EpgSessionTracking"

    const-string v9, "Resuming OneGuide session"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    sput-wide v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sSessionStart:J

    goto :goto_0

    .line 235
    :cond_1
    if-eqz v3, :cond_2

    .line 236
    const-string v8, "EpgSessionTracking"

    const-string v9, "Reporting OneGuide session end"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    invoke-static {v3}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v9

    long-to-int v10, v4

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackVesperSessionEnd(Ljava/util/UUID;I)V

    .line 241
    :cond_2
    const-string v8, "EpgSessionTracking"

    const-string v9, "Starting new OneGuide session"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    .line 243
    .local v2, "newSessionId":Ljava/util/UUID;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackVesperSessionStart(Ljava/util/UUID;)V

    .line 244
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v8

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setSessionID(Ljava/lang/String;)V

    .line 245
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setSessionDuration(J)V

    .line 246
    sput-wide v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sSessionStart:J

    goto :goto_0
.end method

.method public static startTrackingURCSession()V
    .locals 1

    .prologue
    .line 277
    sget-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    if-eqz v0, :cond_0

    .line 278
    const/4 v0, 0x1

    sput-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionNested:Z

    .line 282
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->startTrackingSession()V

    goto :goto_0
.end method

.method public static stopTrackingSession()V
    .locals 8

    .prologue
    .line 250
    sget-boolean v6, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    if-nez v6, :cond_0

    .line 263
    .local v0, "currentTime":J
    .local v2, "duration":J
    .local v4, "previousSessionDuration":J
    :goto_0
    return-void

    .line 253
    .end local v0    # "currentTime":J
    .end local v2    # "duration":J
    .end local v4    # "previousSessionDuration":J
    :cond_0
    const/4 v6, 0x0

    sput-boolean v6, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionActive:Z

    .line 255
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getSessionDuration()J

    move-result-wide v4

    .line 256
    .restart local v4    # "previousSessionDuration":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 259
    .restart local v0    # "currentTime":J
    const-string v6, "EpgSessionTracking"

    const-string v7, "Pausing OneGuide session"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    sget-wide v6, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sSessionStart:J

    sub-long v6, v0, v6

    add-long v2, v4, v6

    .line 261
    .restart local v2    # "duration":J
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setSessionDuration(J)V

    .line 262
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setSessionEndTime(J)V

    goto :goto_0
.end method

.method public static stopTrackingURCSession(Z)V
    .locals 1
    .param p0, "fromURC"    # Z

    .prologue
    .line 286
    sget-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionNested:Z

    if-eqz v0, :cond_1

    .line 287
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->sIsSessionNested:Z

    .line 290
    if-nez p0, :cond_0

    .line 291
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->startTrackingSession()V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->stopTrackingSession()V

    goto :goto_0
.end method

.method public static streamChannel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "channelId"    # Ljava/lang/String;
    .param p1, "lineupId"    # Ljava/lang/String;

    .prologue
    .line 173
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 174
    .local v0, "isConnected":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->streamFullScreen(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Stream FullScreen"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    return-void

    .line 173
    .end local v0    # "isConnected":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "providerId"    # Ljava/lang/String;
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 181
    if-nez p0, :cond_0

    move-object v6, v3

    .line 182
    .local v6, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :goto_0
    if-nez v6, :cond_1

    .line 183
    .local v3, "context":Ljava/lang/String;
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getTitleIdWithFocus()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p2, :cond_3

    .end local p2    # "showId":Ljava/lang/String;
    :goto_2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "appIdShowId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Stream Launch"

    move-object v2, p0

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuidePageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void

    .line 181
    .end local v3    # "context":Ljava/lang/String;
    .end local v5    # "appIdShowId":Ljava/lang/String;
    .end local v6    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .restart local p2    # "showId":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v6

    goto :goto_0

    .line 182
    .restart local v6    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v0, v1, :cond_2

    const-string v3, "HDMI"

    goto :goto_1

    :cond_2
    const-string v3, "USB"

    goto :goto_1

    .line 183
    .restart local v3    # "context":Ljava/lang/String;
    :cond_3
    const-string p2, ""

    goto :goto_2
.end method

.method public static tuneToProgram(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z
    .locals 9
    .param p0, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p1, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_0

    move v0, v4

    .line 150
    .local v0, "isConnected":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v5

    const-string v6, "ShowTuned"

    .line 152
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v8

    if-nez p1, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v5, v6, v7, v8, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 155
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v1, :cond_2

    move v2, v3

    .line 162
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :goto_2
    return v2

    .end local v0    # "isConnected":Z
    :cond_0
    move v0, v3

    .line 148
    goto :goto_0

    .line 152
    .restart local v0    # "isConnected":Z
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getId()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 159
    .restart local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getTuneToUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleLocation()Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    move v2, v4

    .line 160
    goto :goto_2

    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_3
    move v2, v3

    .line 162
    goto :goto_2
.end method
