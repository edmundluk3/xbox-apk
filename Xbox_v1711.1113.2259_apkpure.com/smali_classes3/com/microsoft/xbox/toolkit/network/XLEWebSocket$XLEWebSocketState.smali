.class public final enum Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
.super Ljava/lang/Enum;
.source "XLEWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "XLEWebSocketState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum CloseSent:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum Closed:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum Connecting:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum Error:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum None:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

.field public static final enum Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->None:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Connecting:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "Open"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "CloseSent"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->CloseSent:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "Closed"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Closed:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    const-string v1, "Error"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Error:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->None:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Connecting:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->CloseSent:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Closed:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Error:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->$VALUES:[Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    return-object v0
.end method
