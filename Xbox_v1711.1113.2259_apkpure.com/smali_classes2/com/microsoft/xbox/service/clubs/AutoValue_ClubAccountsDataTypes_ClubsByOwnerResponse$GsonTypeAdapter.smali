.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;>;"
        }
    .end annotation
.end field

.field private defaultClubs:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;"
        }
    .end annotation
.end field

.field private defaultMaximumOpenAndClosedClubs:I

.field private defaultMaximumSecretClubs:I

.field private defaultOwner:Ljava/lang/String;

.field private defaultRemainingOpenAndClosedClubs:I

.field private defaultRemainingSecretClubs:I

.field private final maximumOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final maximumSecretClubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final remainingOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final remainingSecretClubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultClubs:Lcom/google/common/collect/ImmutableList;

    .line 31
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingOpenAndClosedClubs:I

    .line 32
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingSecretClubs:I

    .line 33
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumOpenAndClosedClubs:I

    .line 34
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumSecretClubs:I

    .line 36
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->clubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 9
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_0

    .line 93
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 94
    const/4 v0, 0x0

    .line 140
    :goto_0
    return-object v0

    .line 96
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 98
    .local v1, "owner":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultClubs:Lcom/google/common/collect/ImmutableList;

    .line 99
    .local v2, "clubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    iget v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingOpenAndClosedClubs:I

    .line 100
    .local v3, "remainingOpenAndClosedClubs":I
    iget v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingSecretClubs:I

    .line 101
    .local v4, "remainingSecretClubs":I
    iget v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumOpenAndClosedClubs:I

    .line 102
    .local v5, "maximumOpenAndClosedClubs":I
    iget v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumSecretClubs:I

    .line 103
    .local v6, "maximumSecretClubs":I
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 105
    .local v7, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_1

    .line 106
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 109
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 135
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 109
    :sswitch_0
    const-string v8, "owner"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "clubs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "remainingOpenAndClosedClubs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "remainingSecretClubs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "maximumOpenAndClosedClubs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "maximumSecretClubs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    .line 111
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "owner":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 112
    .restart local v1    # "owner":Ljava/lang/String;
    goto :goto_1

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->clubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "clubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    check-cast v2, Lcom/google/common/collect/ImmutableList;

    .line 116
    .restart local v2    # "clubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    goto :goto_1

    .line 119
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 120
    goto :goto_1

    .line 123
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 124
    goto/16 :goto_1

    .line 127
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 128
    goto/16 :goto_1

    .line 131
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 132
    goto/16 :goto_1

    .line 139
    .end local v7    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 140
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;IIII)V

    goto/16 :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        -0x5f2c2866 -> :sswitch_2
        -0x30dada29 -> :sswitch_3
        0x5a5f27d -> :sswitch_1
        0x653f2b3 -> :sswitch_0
        0x36543f6d -> :sswitch_5
        0x7216e944 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultClubs(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "defaultClubs":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$OwnedClub;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultClubs:Lcom/google/common/collect/ImmutableList;

    .line 49
    return-object p0
.end method

.method public setDefaultMaximumOpenAndClosedClubs(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMaximumOpenAndClosedClubs"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumOpenAndClosedClubs:I

    .line 61
    return-object p0
.end method

.method public setDefaultMaximumSecretClubs(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMaximumSecretClubs"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultMaximumSecretClubs:I

    .line 65
    return-object p0
.end method

.method public setDefaultOwner(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwner"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public setDefaultRemainingOpenAndClosedClubs(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRemainingOpenAndClosedClubs"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingOpenAndClosedClubs:I

    .line 53
    return-object p0
.end method

.method public setDefaultRemainingSecretClubs(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRemainingSecretClubs"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->defaultRemainingSecretClubs:I

    .line 57
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    if-nez p2, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 88
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 75
    const-string v0, "owner"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->owner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 77
    const-string v0, "clubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->clubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 79
    const-string v0, "remainingOpenAndClosedClubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingOpenAndClosedClubs()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 81
    const-string v0, "remainingSecretClubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->remainingSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->remainingSecretClubs()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    const-string v0, "maximumOpenAndClosedClubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumOpenAndClosedClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumOpenAndClosedClubs()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 85
    const-string v0, "maximumSecretClubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->maximumSecretClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;->maximumSecretClubs()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 87
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubsByOwnerResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)V

    return-void
.end method
