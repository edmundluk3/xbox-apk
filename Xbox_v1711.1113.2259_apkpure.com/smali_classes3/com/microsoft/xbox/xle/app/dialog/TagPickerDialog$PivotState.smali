.class public abstract Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
.super Ljava/lang/Object;
.source "TagPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PivotState"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enableAll(J)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
    .locals 6
    .param p0, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 467
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 468
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;

    move v2, v1

    move v3, v1

    move-wide v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;-><init>(ZZZJ)V

    return-object v0
.end method

.method public static systemOnly()Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 472
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;

    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;-><init>(ZZZJ)V

    return-object v0
.end method


# virtual methods
.method public abstract achievementTagsEnabled()Z
.end method

.method public abstract systemTagsEnabled()Z
.end method

.method public abstract titleId()J
.end method

.method public abstract trendingTagsEnabled()Z
.end method
