.class public Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;
.super Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent;
.source "ChatHeadLifecycleEventDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatHeadDetachedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent",
        "<TT;>;"
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/io/Serializable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent<TT;>;"
    .local p1, "key":Ljava/io/Serializable;, "TT;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent;-><init>(Ljava/io/Serializable;)V

    .line 74
    return-void
.end method

.method public static with(Ljava/io/Serializable;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;
    .locals 1
    .param p0    # Ljava/io/Serializable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(TT;)",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "key":Ljava/io/Serializable;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 78
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;-><init>(Ljava/io/Serializable;)V

    return-object v0
.end method
