.class public Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;
.super Ljava/lang/Object;
.source "TVSeriesSeasonsItemViewHolder.java"


# instance fields
.field private final episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final episodeDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const v0, 0x7f0e0b86

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 15
    const v0, 0x7f0e0b87

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 16
    const v0, 0x7f0e0b88

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 17
    return-void
.end method


# virtual methods
.method public getEpisodeAirDateTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeAirDateTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method public getEpisodeDescriptionTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method public getEpisodeTitleTextView()Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsItemViewHolder;->episodeTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method
