.class public Lcom/microsoft/xbox/service/model/epg/EPGProvider;
.super Ljava/lang/Object;
.source "EPGProvider.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;,
        Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;,
        Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;,
        Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/service/model/epg/EPGProvider;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EPGProvider"

.field private static sXuid:J


# instance fields
.field private mChannelsLoaded:Z

.field private mColorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

.field private final mHeadend:Ljava/lang/String;

.field private mIsBusy:Z

.field private final mIsPreferred:Z

.field private final mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

.field private final mLocale:Ljava/lang/String;

.field private final mLogPrefix:Ljava/lang/String;

.field private mLogo:Ljava/lang/String;

.field private final mMetadataListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field private final mTitleId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->sXuid:J

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;Ljava/lang/String;)V
    .locals 9
    .param p1, "provider"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    .param p2, "locale"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 42
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mMetadataListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    .line 58
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLocale:Ljava/lang/String;

    .line 60
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->title_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mTitleId:Ljava/lang/String;

    .line 61
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    .line 63
    iget-boolean v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->isPreferred:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsPreferred:Z

    .line 64
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mTitleId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLocale:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsPreferred:Z

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeAddHeadend(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetBrandingColors(Ljava/lang/String;)[Lcom/microsoft/xbox/service/model/epg/BrandingColor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->storeBrandingColors([Lcom/microsoft/xbox/service/model/epg/BrandingColor;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetProviderLogo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogo:Ljava/lang/String;

    .line 77
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 78
    new-instance v8, Lcom/microsoft/xbox/service/model/epg/EPGProvider$2;

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$1;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    invoke-direct {v8, p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$2;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 88
    .local v8, "channelLoad":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 95
    .end local v8    # "channelLoad":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->loadChannels(Z)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v0, v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->loadMetadata()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsBusy:Z

    return p1
.end method

.method public static getUserId()J
    .locals 6

    .prologue
    .line 613
    sget-wide v2, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->sXuid:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 614
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 615
    .local v0, "xuid":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 616
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sput-wide v2, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->sXuid:J

    .line 619
    :cond_0
    sget-wide v2, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->sXuid:J

    return-wide v2
.end method

.method public static getUserToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 629
    :try_start_0
    const-string v1, "https://xboxlive.com"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getXTokenString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 633
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_0
    return-object v1

    .line 630
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_0
    move-exception v0

    .line 631
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v1, "EPGProvider"

    const-string v2, "Failed to get user authorization token. Stack trace follows."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    .line 633
    const-string v1, ""

    goto :goto_0
.end method

.method private native nativeAddHeadend(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method private native nativeEnsureChannelListLoadedAsync(Ljava/lang/String;JLjava/lang/String;Z)Z
.end method

.method private native nativeFetchFromListServiceIfNeededAsync(Ljava/lang/String;JLjava/lang/String;)Z
.end method

.method private native nativeFetchIfNeededForTimeAsync(Ljava/lang/String;JLjava/lang/String;IIIILcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;Z)V
.end method

.method private native nativeFetchProviderMetadataAsync(Ljava/lang/String;JLjava/lang/String;)Z
.end method

.method private native nativeGetBrandingColors(Ljava/lang/String;)[Lcom/microsoft/xbox/service/model/epg/BrandingColor;
.end method

.method private native nativeGetChannelList(Ljava/lang/String;I)[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
.end method

.method private native nativeGetProviderLogo(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native nativePurgeOldSchedules(Ljava/lang/String;)V
.end method

.method private native nativeRemoveHeadend(Ljava/lang/String;)V
.end method

.method private native nativeSetFavoriteForChannel(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I
.end method

.method private native nativeStoreTunerChannels(Ljava/lang/String;[Ljava/lang/String;[Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z
.end method

.method private native nativeUpdateHeadendFilterType(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private processFavoriteResult(ZI)Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    .locals 4
    .param p1, "favorite"    # Z
    .param p2, "ret"    # I

    .prologue
    .line 430
    packed-switch p2, :pswitch_data_0

    .line 435
    :pswitch_0
    if-eqz p1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotSet:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .line 442
    .local v1, "result":Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .line 443
    .local v0, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;->onSetFavoriteError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    goto :goto_1

    .line 432
    .end local v0    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    .end local v1    # "result":Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .line 445
    :cond_0
    return-object v1

    .line 435
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotRemove:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    goto :goto_0

    .line 438
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorFullList:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .restart local v1    # "result":Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    goto :goto_0

    .line 430
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static resetUserId()V
    .locals 2

    .prologue
    .line 623
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->sXuid:J

    .line 624
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 228
    return-void
.end method

.method public addListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mMetadataListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 243
    return-void
.end method

.method public applyBranding()V
    .locals 10

    .prologue
    .line 314
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getProfileColor()I

    move-result v1

    .line 315
    .local v1, "defaultColor":I
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0c009b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 317
    .local v3, "whiteText":I
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->values()[Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v2, v5, v4

    .line 318
    .local v2, "purpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 319
    .local v0, "color":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 320
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 322
    :cond_0
    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->setColor(I)V

    .line 323
    sget-object v7, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    if-ne v2, v7, :cond_1

    .line 324
    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v3, v8, v9}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->defaultPressedSelectedText(III)Landroid/content/res/ColorStateList;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->setColor(Landroid/content/res/ColorStateList;)V

    .line 317
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 326
    :cond_1
    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->setColor(I)V

    goto :goto_1

    .line 329
    .end local v0    # "color":Ljava/lang/Integer;
    .end local v2    # "purpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    :cond_2
    return-void
.end method

.method public canStream()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 159
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v1

    .line 160
    .local v1, "streamer":Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->canStreamTuner()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v3, v4, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->canStreamHdmi()Z
    :try_end_0
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 162
    .end local v1    # "streamer":Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    :cond_2
    :goto_0
    return v2

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Lcom/microsoft/xbox/xle/epg/TvStreamerException;
    goto :goto_0
.end method

.method public compareTo(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)I
    .locals 4
    .param p1, "another"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 173
    if-nez p1, :cond_0

    .line 174
    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "another is null"

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_2

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-eq v2, v3, :cond_2

    .line 201
    :cond_1
    :goto_0
    return v0

    .line 180
    :cond_2
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 181
    goto :goto_0

    .line 185
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_4

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_1

    .line 187
    :cond_4
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->hdmi:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 188
    goto :goto_0

    .line 192
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_6

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->streaming:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v2, v3, :cond_6

    .line 193
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsPreferred:Z

    if-nez v2, :cond_1

    .line 195
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsPreferred:Z

    if-eqz v0, :cond_6

    move v0, v1

    .line 196
    goto :goto_0

    .line 201
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->compareTo(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)I

    move-result v0

    return v0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 108
    const-string v0, "EPGProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "destroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeRemoveHeadend(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public fetchFromListServiceIfNeededAsync()Z
    .locals 4

    .prologue
    .line 416
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserId()J

    move-result-wide v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserToken()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeFetchFromListServiceIfNeededAsync(Ljava/lang/String;JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public fetchIfNeededForTimeAsync(IZIZLcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;Z)V
    .locals 11
    .param p1, "time"    # I
    .param p2, "fetchForward"    # Z
    .param p3, "channelOrdinal"    # I
    .param p4, "channelForward"    # Z
    .param p5, "scheduleType"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;
    .param p6, "forceRefresh"    # Z

    .prologue
    .line 412
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserId()J

    move-result-wide v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserToken()Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_0

    const/4 v6, 0x1

    :goto_0
    if-eqz p4, :cond_1

    const/4 v8, 0x1

    :goto_1
    move-object v0, p0

    move v5, p1

    move v7, p3

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeFetchIfNeededForTimeAsync(Ljava/lang/String;JLjava/lang/String;IIIILcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;Z)V

    .line 413
    return-void

    .line 412
    :cond_0
    const/4 v6, -0x1

    goto :goto_0

    :cond_1
    const/4 v8, -0x1

    goto :goto_1
.end method

.method public getChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->full:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetChannelList(Ljava/lang/String;I)[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    return-object v0
.end method

.method public getFavoriteChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->favorites:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetChannelList(Ljava/lang/String;I)[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    return-object v0
.end method

.method public getHdAndUnmatchedSdChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdAndUnmatchedSd:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetChannelList(Ljava/lang/String;I)[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    return-object v0
.end method

.method public getHdOnlyChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdOnly:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetChannelList(Ljava/lang/String;I)[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    return-object v0
.end method

.method public getHeadend()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    return-object v0
.end method

.method public getIsPreferred()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsPreferred:Z

    return v0
.end method

.method public getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    return-object v0
.end method

.method public getLogo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogo:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    return-object v0
.end method

.method public getTitleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mTitleId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleLocation()Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mTitleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v0

    return-object v0
.end method

.method public hasLoadedChannels()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mChannelsLoaded:Z

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsBusy:Z

    return v0
.end method

.method public loadChannels(Z)V
    .locals 6
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mChannelsLoaded:Z

    .line 367
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserId()J

    move-result-wide v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserToken()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeEnsureChannelListLoadedAsync(Ljava/lang/String;JLjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsBusy:Z

    .line 370
    :cond_0
    return-void
.end method

.method public loadMetadata()V
    .locals 4

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserId()J

    move-result-wide v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserToken()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeFetchProviderMetadataAsync(Ljava/lang/String;JLjava/lang/String;)Z

    .line 374
    return-void
.end method

.method public onNativeChannelLoadComplete(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 459
    packed-switch p1, :pswitch_data_0

    .line 474
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 477
    .local v0, "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :goto_0
    const-string v1, "EPGProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Channel Load Complete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mSource:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    sget-object v2, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-eq v1, v2, :cond_0

    .line 481
    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 482
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    .line 483
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsBusy:Z

    .line 494
    :cond_0
    :goto_1
    return-void

    .line 461
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 462
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mChannelsLoaded:Z

    goto :goto_0

    .line 465
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 466
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 468
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 469
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 471
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 472
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 485
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$4;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 459
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onNativeFetchComplete(I)V
    .locals 5
    .param p1, "result"    # I

    .prologue
    .line 526
    packed-switch p1, :pswitch_data_0

    .line 540
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 543
    .local v0, "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :goto_0
    const-string v2, "EPGProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Fetch Complete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .line 546
    .local v1, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;->onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_1

    .line 528
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .end local v1    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 529
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 531
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 532
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 534
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 535
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 537
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 538
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 548
    :cond_0
    return-void

    .line 526
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onNativeFetchListServiceComplete(I)V
    .locals 5
    .param p1, "result"    # I

    .prologue
    .line 559
    packed-switch p1, :pswitch_data_0

    .line 570
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 573
    .local v0, "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :goto_0
    const-string v2, "EPGProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Fetch List Service Complete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .line 576
    .local v1, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;->onFetchListServiceComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_1

    .line 561
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .end local v1    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 562
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 564
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 565
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 567
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 568
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 578
    :cond_0
    return-void

    .line 559
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onNativeFetchProgress(I)V
    .locals 4
    .param p1, "percent"    # I

    .prologue
    .line 551
    const-string v1, "EPGProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Fetch Progress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .line 553
    .local v0, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;->onFetchProgress(I)V

    goto :goto_0

    .line 555
    .end local v0    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    :cond_0
    return-void
.end method

.method public onNativeMetadataLoadComplete(I)V
    .locals 5
    .param p1, "result"    # I

    .prologue
    .line 498
    packed-switch p1, :pswitch_data_0

    .line 514
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 517
    .local v0, "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :goto_0
    const-string v2, "EPGProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Metadata Load Complete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mMetadataListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;

    .line 520
    .local v1, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;->onMetadataLoad(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_1

    .line 500
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .end local v1    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 501
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetBrandingColors(Ljava/lang/String;)[Lcom/microsoft/xbox/service/model/epg/BrandingColor;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->storeBrandingColors([Lcom/microsoft/xbox/service/model/epg/BrandingColor;)V

    .line 502
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeGetProviderLogo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogo:Ljava/lang/String;

    goto :goto_0

    .line 505
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 506
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 508
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 509
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 511
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 512
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 522
    :cond_0
    return-void

    .line 498
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onNativeSendComplete(IZ)V
    .locals 5
    .param p1, "result"    # I
    .param p2, "attemptedFavoriteValue"    # Z

    .prologue
    .line 582
    packed-switch p1, :pswitch_data_0

    .line 593
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 596
    .local v0, "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :goto_0
    const-string v2, "EPGProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Send Complete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .line 599
    .local v1, "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    invoke-interface {v1, v0, p2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;->onSendComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;Z)V

    goto :goto_1

    .line 584
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .end local v1    # "l":Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 585
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 587
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 588
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 590
    .end local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorParseAndStore:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .line 591
    .restart local v0    # "fetchResult":Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    goto :goto_0

    .line 601
    :cond_0
    return-void

    .line 582
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public purgeOldSchedules()V
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativePurgeOldSchedules(Ljava/lang/String;)V

    .line 421
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFetchListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$IMetadataListener;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mMetadataListeners:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 247
    return-void
.end method

.method public setActive()V
    .locals 0

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->applyBranding()V

    .line 209
    return-void
.end method

.method public setFavoriteForChannel(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    .locals 8
    .param p1, "channelGuid"    # Ljava/lang/String;
    .param p2, "favorite"    # Z

    .prologue
    .line 424
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserId()J

    move-result-wide v2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getUserToken()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeSetFavoriteForChannel(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    move-result v7

    .line 425
    .local v7, "ret":I
    invoke-direct {p0, p2, v7}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->processFavoriteResult(ZI)Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    move-result-object v0

    return-object v0
.end method

.method public storeBrandingColors([Lcom/microsoft/xbox/service/model/epg/BrandingColor;)V
    .locals 13
    .param p1, "colors"    # [Lcom/microsoft/xbox/service/model/epg/BrandingColor;

    .prologue
    const/4 v12, 0x0

    .line 253
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c009b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 254
    .local v4, "whiteText":I
    const/high16 v0, -0x1000000

    .line 256
    .local v0, "blackText":I
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 259
    if-eqz p1, :cond_8

    .line 260
    array-length v7, p1

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_8

    aget-object v1, p1, v6

    .line 261
    .local v1, "color":Lcom/microsoft/xbox/service/model/epg/BrandingColor;
    const/4 v2, 0x0

    .local v2, "purpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    const/4 v3, 0x0

    .line 262
    .local v3, "textPurpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    const-string v8, "TopHeading"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 263
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 264
    sget-object v3, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 280
    :goto_1
    if-eqz v2, :cond_0

    .line 282
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    const/16 v8, 0xff

    iget v9, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->red:I

    iget v10, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->green:I

    iget v11, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->blue:I

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    :cond_0
    if-eqz v3, :cond_1

    .line 286
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->textColor:Ljava/lang/String;

    const-string v9, "Black"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const/high16 v5, -0x1000000

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 265
    :cond_2
    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    const-string v8, "ChannelHeading"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 266
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 267
    sget-object v3, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    goto :goto_1

    .line 268
    :cond_3
    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    const-string v8, "CellHighlight"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 269
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 270
    sget-object v3, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    goto :goto_1

    .line 271
    :cond_4
    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    const-string v8, "InFocus"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 272
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 273
    sget-object v3, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocusText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    goto :goto_1

    .line 274
    :cond_5
    iget-object v5, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    const-string v8, "CellProgress"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 275
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    goto :goto_1

    .line 277
    :cond_6
    const-string v5, "EPGProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown color purpose "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/microsoft/xbox/service/model/epg/BrandingColor;->purpose:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    move v5, v4

    .line 286
    goto :goto_2

    .line 292
    .end local v1    # "color":Lcom/microsoft/xbox/service/model/epg/BrandingColor;
    .end local v2    # "purpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    .end local v3    # "textPurpose":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    :cond_8
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 293
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    :cond_9
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 297
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    :cond_a
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 301
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    :cond_b
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 305
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocusText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    :cond_c
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 309
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mColorMap:Ljava/util/HashMap;

    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v5, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    :cond_d
    return-void
.end method

.method public storeTunerChannels(Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V
    .locals 6
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .prologue
    .line 377
    if-nez p1, :cond_0

    .line 409
    :goto_0
    return-void

    .line 381
    :cond_0
    const-string v3, "EPGProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Storing tuner channels"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v3, p1, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    array-length v3, v3

    new-array v0, v3, [Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 385
    .local v0, "foundChannels":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p1, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 386
    new-instance v3, Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    aget-object v5, v5, v1

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;)V

    aput-object v3, v0, v1

    .line 385
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 389
    :cond_1
    iget-object v3, p1, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->serviceChannels:[Ljava/lang/String;

    invoke-virtual {v3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 391
    .local v2, "serviceChannels":[Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    invoke-direct {p0, v3, v2, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeStoreTunerChannels(Ljava/lang/String;[Ljava/lang/String;[Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 392
    const-string v3, "Failed to store tuner channels"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 393
    const-string v3, "EPGProvider"

    const-string v4, "Failed to store tuner channels"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v3, v4, :cond_3

    .line 398
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    .line 399
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIsBusy:Z

    goto :goto_0

    .line 401
    :cond_3
    new-instance v3, Lcom/microsoft/xbox/service/model/epg/EPGProvider$3;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$3;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;)V
    .locals 3
    .param p1, "provider"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    if-eq v0, v1, :cond_0

    .line 99
    const-string v0, "EPGProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Updating filter preference to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v0, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 101
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setPreferredFilter(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mHeadend:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mFilter:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->nativeUpdateHeadendFilterType(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->mIteratorFactory:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    .line 105
    :cond_0
    return-void
.end method
