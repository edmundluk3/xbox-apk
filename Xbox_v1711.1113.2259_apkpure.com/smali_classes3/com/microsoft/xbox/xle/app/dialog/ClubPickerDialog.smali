.class public Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
    }
.end annotation


# instance fields
.field private final clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

.field private doneHandler:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    const v2, 0x7f080238

    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 28
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 30
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    const v2, 0x7f030090

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->setContentView(I)V

    .line 34
    const v2, 0x7f0e03ae

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 35
    .local v0, "closeButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const v2, 0x7f0e03b0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 39
    .local v1, "clubRecyclerList":Landroid/support/v7/widget/RecyclerView;
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    .line 44
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 47
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    .line 48
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubPickerDialog()V

    .line 65
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;->onClubsSelected(J)V

    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->dismissSelf()V

    .line 42
    return-void
.end method


# virtual methods
.method public setClubList(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->clear()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->addAll(Ljava/util/Collection;)V

    .line 53
    return-void
.end method

.method public setDoneHandler(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V
    .locals 0
    .param p1, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    .line 57
    return-void
.end method

.method public setShowInviteSubtitle(Z)V
    .locals 1
    .param p1, "showInviteSubtitle"    # Z

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->clubPickerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPickerListAdapter;->setShowInviteSubtitle(Z)V

    .line 61
    return-void
.end method
