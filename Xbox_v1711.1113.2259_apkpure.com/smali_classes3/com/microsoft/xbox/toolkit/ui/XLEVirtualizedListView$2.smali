.class Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "XLEVirtualizedListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->notifyDataSetChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

.field final synthetic val$max:I

.field final synthetic val$min:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p2, "executorService"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->val$min:I

    iput p4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->val$max:I

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->doInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->val$min:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->val$max:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->load(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 110
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->setSelection(I)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onDataUpdated()V

    .line 129
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method
