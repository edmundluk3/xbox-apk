.class final Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;
.super Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
.source "AutoValue_RecyclerViewScrollEvent.java"


# instance fields
.field private final dx:I

.field private final dy:I

.field private final view:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->view:Landroid/support/v7/widget/RecyclerView;

    .line 21
    iput p2, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dx:I

    .line 22
    iput p3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dy:I

    .line 23
    return-void
.end method


# virtual methods
.method public dx()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dx:I

    return v0
.end method

.method public dy()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dy:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;

    .line 57
    .local v0, "that":Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    iget-object v3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dx:I

    .line 58
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->dx()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dy:I

    .line 59
    invoke-virtual {v0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->dy()I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    :cond_3
    move v1, v2

    .line 61
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 66
    const/4 v0, 0x1

    .line 67
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 68
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 69
    mul-int/2addr v0, v2

    .line 70
    iget v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dx:I

    xor-int/2addr v0, v1

    .line 71
    mul-int/2addr v0, v2

    .line 72
    iget v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dy:I

    xor-int/2addr v0, v1

    .line 73
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecyclerViewScrollEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->view:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dx="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->dy:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public view()Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/AutoValue_RecyclerViewScrollEvent;->view:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method
