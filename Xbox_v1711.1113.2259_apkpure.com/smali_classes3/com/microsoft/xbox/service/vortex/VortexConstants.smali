.class public Lcom/microsoft/xbox/service/vortex/VortexConstants;
.super Ljava/lang/Object;
.source "VortexConstants.java"


# static fields
.field public static final Event_App_Launch:Ljava/lang/String; = "AppLaunch"

.field public static final Event_Media_Usage:Ljava/lang/String; = "MediaUsageEvent"

.field public static final MediaControlAction_End:I = 0x9

.field public static final MediaControlAction_FForward:I = 0x5

.field public static final MediaControlAction_MediaDisengaged:I = 0xb

.field public static final MediaControlAction_MediaEngaged:I = 0xa

.field public static final MediaControlAction_Pause:I = 0x2

.field public static final MediaControlAction_Play:I = 0x1

.field public static final MediaControlAction_Resume:I = 0x3

.field public static final MediaControlAction_Rewind:I = 0x4

.field public static final MediaControlAction_Skip:I = 0x7

.field public static final MediaControlAction_Stop:I = 0x8

.field public static final MediaControlAction_Update:I = 0x6

.field public static final MediaStreamQuality_High:I = 0x0

.field public static final MediaStreamQuality_Low:I = 0x2

.field public static final MediaStreamQuality_Medium:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
