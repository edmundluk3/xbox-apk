.class public final enum Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;
.super Ljava/lang/Enum;
.source "MediaException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MEDIA_ERRORS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

.field public static final enum XDRM_E_NOTIMPL:Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;


# instance fields
.field private final mCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    const-string v1, "XDRM_E_NOTIMPL"

    const v2, -0x7fffbfff

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->XDRM_E_NOTIMPL:Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    sget-object v1, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->XDRM_E_NOTIMPL:Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->ENUM$VALUES:[Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "code"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->mCode:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->ENUM$VALUES:[Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    array-length v1, v0

    new-array v2, v1, [Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/playready/MediaException$MEDIA_ERRORS;->mCode:I

    return v0
.end method
