.class Lcom/microsoft/xbox/smartglass/controls/Gyroscope;
.super Lcom/microsoft/xbox/smartglass/controls/Sensor;
.source "Gyroscope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/controls/Sensor",
        "<",
        "Lcom/microsoft/xbox/smartglass/GyrometerListener;",
        ">;"
    }
.end annotation


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Gyroscope"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 19
    const-string v0, "Gyroscope"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/Sensor;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected createListener()Lcom/microsoft/xbox/smartglass/GyrometerListener;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Gyroscope;)V

    return-object v0
.end method

.method protected bridge synthetic createListener()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Gyroscope;->createListener()Lcom/microsoft/xbox/smartglass/GyrometerListener;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentState()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 25
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscopeCurrentState;

    invoke-static {}, Lcom/microsoft/xbox/smartglass/Gyrometer;->isSupported()Z

    move-result v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/smartglass/controls/Gyroscope;->_isActive:Z

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscopeCurrentState;-><init>(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :catch_0
    move-exception v0

    .line 30
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getMetricMethodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "StartGyroscope"

    return-object v0
.end method

.method protected getSensor()Lcom/microsoft/xbox/smartglass/Sensor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/smartglass/Sensor",
            "<",
            "Lcom/microsoft/xbox/smartglass/GyrometerListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SensorManager;->getGyrometer()Lcom/microsoft/xbox/smartglass/Gyrometer;

    move-result-object v0

    return-object v0
.end method

.method protected isSupported()Z
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/smartglass/Gyrometer;->isSupported()Z

    move-result v0

    return v0
.end method
