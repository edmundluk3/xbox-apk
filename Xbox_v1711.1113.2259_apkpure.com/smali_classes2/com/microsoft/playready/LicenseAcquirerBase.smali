.class abstract Lcom/microsoft/playready/LicenseAcquirerBase;
.super Ljava/lang/Object;
.source "LicenseAcquirer.java"

# interfaces
.implements Lcom/microsoft/playready/ILicenseAcquirerListener;


# instance fields
.field private mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

.field private final mLicenseAcquirerListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/ILicenseAcquirerListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 17
    return-void
.end method


# virtual methods
.method public addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 25
    monitor-enter p0

    .line 27
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    monitor-exit p0

    .line 29
    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLicenseAcquisitionPlugin()Lcom/microsoft/playready/ILicenseAcquisitionPlugin;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    return-object v0
.end method

.method public onAcquireLicenseCompleted(Lcom/microsoft/playready/ILicenseAcquisitionTask;)V
    .locals 3
    .param p1, "completedTask"    # Lcom/microsoft/playready/ILicenseAcquisitionTask;

    .prologue
    .line 42
    monitor-enter p0

    .line 44
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    monitor-exit p0

    .line 49
    return-void

    .line 44
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/ILicenseAcquirerListener;

    .line 46
    .local v0, "listener":Lcom/microsoft/playready/ILicenseAcquirerListener;
    invoke-interface {v0, p1}, Lcom/microsoft/playready/ILicenseAcquirerListener;->onAcquireLicenseCompleted(Lcom/microsoft/playready/ILicenseAcquisitionTask;)V

    goto :goto_0

    .line 42
    .end local v0    # "listener":Lcom/microsoft/playready/ILicenseAcquirerListener;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 33
    monitor-enter p0

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLicenseAcquirerListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 33
    monitor-exit p0

    .line 37
    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setLicenseAcquisitionPlugin(Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V
    .locals 2
    .param p1, "laPlugin"    # Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Invalid argument to setLicenseAcquisitionPlugin,laPlugin must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iput-object p1, p0, Lcom/microsoft/playready/LicenseAcquirerBase;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 59
    return-void
.end method
