.class public abstract Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;
.super Ljava/lang/Object;
.source "CommonActionsAndResults.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/common/CommonActionsAndResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LceResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;"
    }
.end annotation


# instance fields
.field public final content:Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final error:Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final inFlight:Z


# direct methods
.method protected constructor <init>(Ljava/lang/Object;Ljava/lang/Throwable;Z)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "inFlight"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Throwable;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    .local p1, "content":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->content:Ljava/lang/Object;

    .line 178
    iput-object p2, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->error:Ljava/lang/Throwable;

    .line 179
    iput-boolean p3, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->inFlight:Z

    .line 180
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    if-ne p0, p1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;

    if-nez v3, :cond_2

    move v1, v2

    .line 195
    goto :goto_0

    .line 196
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 197
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 199
    check-cast v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;

    .line 200
    .local v0, "other":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->content:Ljava/lang/Object;

    iget-object v4, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->content:Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->error:Ljava/lang/Throwable;

    iget-object v4, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->error:Ljava/lang/Throwable;

    .line 201
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->inFlight:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->inFlight:Z

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 208
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    if-nez v0, :cond_0

    .line 209
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    .line 210
    iget v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->content:Ljava/lang/Object;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    .line 211
    iget v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->error:Ljava/lang/Throwable;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    .line 212
    iget v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->inFlight:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    .line 215
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->hashCode:I

    return v0
.end method

.method public isFailure()Z
    .locals 1

    .prologue
    .line 187
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->error:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 183
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->content:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toLogString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 225
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->inFlight:Z

    if-eqz v1, :cond_0

    const-string v0, "In Flight"

    .line 229
    .local v0, "description":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 227
    .end local v0    # "description":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "Success"

    goto :goto_0

    :cond_1
    const-string v0, "Failure"

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    .local p0, "this":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
