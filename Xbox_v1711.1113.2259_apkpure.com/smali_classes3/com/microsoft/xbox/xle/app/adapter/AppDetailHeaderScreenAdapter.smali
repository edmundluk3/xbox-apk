.class public Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AppDetailHeaderScreenAdapter.java"


# instance fields
.field private final imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private final title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final titleSmallTextView:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    .line 24
    const v0, 0x7f0e01b2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->screenBody:Landroid/view/View;

    .line 25
    const v0, 0x7f0e01b3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 26
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 27
    const v0, 0x7f0e01b4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e01b6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->titleSmallTextView:Landroid/widget/TextView;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 60
    .local v0, "defaultRid":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getBoxArtBackgroundColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    .line 63
    .end local v0    # "defaultRid":I
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->titleSmallTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->getAverageUserRating()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 66
    return-void
.end method
