.class public Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_ViewBinding;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.source "TutorialViewImpl_ViewBinding.java"


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;Landroid/view/View;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    .line 27
    const v0, 0x7f0e0af9

    const-string v1, "field \'setupWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->setupWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 28
    const v0, 0x7f0e0afa

    const-string v1, "field \'redeemWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->redeemWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 29
    const v0, 0x7f0e0afb

    const-string v1, "field \'shopWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->shopWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 30
    const v0, 0x7f0e0afc

    const-string v1, "field \'facebookWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 31
    const v0, 0x7f0e0afd

    const-string v1, "field \'suggestedFriendsWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->suggestedFriendsWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 32
    const v0, 0x7f0e0afe

    const-string v1, "field \'clubWelcomeCard\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->clubWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 33
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    .line 38
    .local v0, "target":Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->setupWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->redeemWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->shopWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->facebookWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->suggestedFriendsWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;->clubWelcomeCard:Lcom/microsoft/xbox/xle/ui/WelcomeCard;

    .line 48
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->unbind()V

    .line 49
    return-void
.end method
