.class public final enum Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;
.super Ljava/lang/Enum;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AlternateIdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

.field public static final enum LegacyXboxProductId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

.field public static final enum PackageFamilyName:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

.field public static final enum XboxTitleId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;


# instance fields
.field private final text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    const-string v1, "LegacyXboxProductId"

    const-string v2, "LegacyXboxProductId"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->LegacyXboxProductId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    const-string v1, "XboxTitleId"

    const-string v2, "XboxTitleId"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->XboxTitleId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    .line 94
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    const-string v1, "PackageFamilyName"

    const-string v2, "PackageFamilyName"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->PackageFamilyName:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    .line 91
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->LegacyXboxProductId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->XboxTitleId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->PackageFamilyName:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->text:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->text:Ljava/lang/String;

    return-object v0
.end method
