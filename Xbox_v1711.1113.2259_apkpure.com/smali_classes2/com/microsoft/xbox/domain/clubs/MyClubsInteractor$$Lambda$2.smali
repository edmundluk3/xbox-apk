.class final synthetic Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

.field private final arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;-><init>(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    check-cast p1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->lambda$null$3(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
