.class final synthetic Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/service/beam/BeamService;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/service/beam/BeamService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;->arg$1:Lcom/microsoft/xbox/data/service/beam/BeamService;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/service/beam/BeamService;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;-><init>(Lcom/microsoft/xbox/data/service/beam/BeamService;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$7;->arg$1:Lcom/microsoft/xbox/data/service/beam/BeamService;

    check-cast p1, Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/service/beam/BeamService;->getSpecificChannels(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
