.class public final Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
.super Ljava/lang/Object;
.source "CommentsServiceDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Comment"
.end annotation


# instance fields
.field private final date:Ljava/util/Date;

.field public final gamertag:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final parentPath:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final path:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final rootPath:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final rootType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final xuid:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "date"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "parentPath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "path"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "rootPath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "rootType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p8, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p9, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 200
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 201
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 202
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 203
    invoke-static {p7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 204
    invoke-static {p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 205
    invoke-static {p9}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 207
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->date:Ljava/util/Date;

    .line 208
    iput-object p2, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->gamertag:Ljava/lang/String;

    .line 209
    iput-object p3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->id:Ljava/lang/String;

    .line 210
    iput-object p4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->parentPath:Ljava/lang/String;

    .line 211
    iput-object p5, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->path:Ljava/lang/String;

    .line 212
    iput-object p6, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootPath:Ljava/lang/String;

    .line 213
    iput-object p7, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootType:Ljava/lang/String;

    .line 214
    iput-object p8, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->text:Ljava/lang/String;

    .line 215
    iput-object p9, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->xuid:Ljava/lang/String;

    .line 216
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 220
    if-ne p1, p0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v1

    .line 222
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    if-nez v3, :cond_2

    move v1, v2

    .line 223
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 225
    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    .line 226
    .local v0, "other":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->date:Ljava/util/Date;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->date:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->gamertag:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->gamertag:Ljava/lang/String;

    .line 227
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->id:Ljava/lang/String;

    .line 228
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->parentPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->parentPath:Ljava/lang/String;

    .line 229
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->path:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->path:Ljava/lang/String;

    .line 230
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootPath:Ljava/lang/String;

    .line 231
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootType:Ljava/lang/String;

    .line 232
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->text:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->text:Ljava/lang/String;

    .line 233
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->xuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->xuid:Ljava/lang/String;

    .line 234
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    if-nez v0, :cond_0

    .line 241
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 242
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->date:Ljava/util/Date;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 243
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->gamertag:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 244
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 245
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->parentPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 246
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->path:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 247
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 248
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->rootType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 249
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->text:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 250
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    .line 253
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
