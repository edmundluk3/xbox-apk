.class public final enum Lcom/google/android/exoplayer2/util/Logger$Module;
.super Ljava/lang/Enum;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/util/Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Module"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/exoplayer2/util/Logger$Module;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum All:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum AudioVideo:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum AudioVideoCommon:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Manifest:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Player:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Source:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Text:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Unknown:Lcom/google/android/exoplayer2/util/Logger$Module;

.field public static final enum Video:Lcom/google/android/exoplayer2/util/Logger$Module;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Unknown:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 33
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "AudioVideoCommon"

    invoke-direct {v0, v1, v4}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideoCommon:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 37
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Audio"

    invoke-direct {v0, v1, v5}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 41
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Video"

    invoke-direct {v0, v1, v6}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Video:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 45
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "AudioVideo"

    invoke-direct {v0, v1, v7}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideo:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 47
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Text"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Text:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 48
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Source"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Source:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 49
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Manifest"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Manifest:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 50
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "Player"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->Player:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 54
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    const-string v1, "All"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger$Module;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->All:Lcom/google/android/exoplayer2/util/Logger$Module;

    .line 28
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/exoplayer2/util/Logger$Module;

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Unknown:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideoCommon:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Video:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->AudioVideo:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/exoplayer2/util/Logger$Module;->Text:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/exoplayer2/util/Logger$Module;->Source:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/exoplayer2/util/Logger$Module;->Manifest:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/exoplayer2/util/Logger$Module;->Player:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/exoplayer2/util/Logger$Module;->All:Lcom/google/android/exoplayer2/util/Logger$Module;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->$VALUES:[Lcom/google/android/exoplayer2/util/Logger$Module;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/exoplayer2/util/Logger$Module;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/util/Logger$Module;

    return-object v0
.end method

.method public static values()[Lcom/google/android/exoplayer2/util/Logger$Module;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/exoplayer2/util/Logger$Module;->$VALUES:[Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-virtual {v0}, [Lcom/google/android/exoplayer2/util/Logger$Module;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/util/Logger$Module;

    return-object v0
.end method
