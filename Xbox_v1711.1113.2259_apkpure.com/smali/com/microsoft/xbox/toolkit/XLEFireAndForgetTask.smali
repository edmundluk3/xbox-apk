.class public Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "XLEFireAndForgetTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final backgroundRunnable:Ljava/lang/Runnable;

.field protected lastException:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "executorService"    # Ljava/util/concurrent/ExecutorService;
    .param p2, "backgroundRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 11
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->backgroundRunnable:Ljava/lang/Runnable;

    .line 12
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->lastException:Ljava/lang/Exception;

    .line 23
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->backgroundRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :goto_0
    return-object v2

    .line 24
    :catch_0
    move-exception v0

    .line 25
    .local v0, "ex":Ljava/lang/Exception;
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->lastException:Ljava/lang/Exception;

    goto :goto_0
.end method

.method protected handleException(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 32
    const-string v0, "FireAndForgetTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "completed with exeception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method protected onPostExecute()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->lastException:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->lastException:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->handleException(Ljava/lang/Exception;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->onPostExecute()V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method
