.class public final Lcom/microsoft/playready/RepresentationVideoInfo;
.super Ljava/lang/Object;
.source "RepresentationVideoInfo.java"

# interfaces
.implements Lcom/microsoft/playready/VideoInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mRepIndex:I

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;II)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I
    .param p3, "repIndex"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 51
    iput p2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    .line 52
    iput p3, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    .line 53
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_28(II)I

    move-result v0

    return v0
.end method

.method public getRotation()I
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_27(II)I

    move-result v0

    return v0
.end method

.method public getSARHeight()I
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_31(II)I

    move-result v0

    return v0
.end method

.method public getSARWidth()I
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_30(II)I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationVideoInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_27(II)I

    move-result v0

    return v0
.end method
