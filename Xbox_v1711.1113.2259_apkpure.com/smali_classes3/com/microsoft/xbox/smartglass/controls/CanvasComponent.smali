.class abstract Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.super Ljava/lang/Object;
.source "CanvasComponent.java"

# interfaces
.implements Lcom/microsoft/xbox/smartglass/controls/WebComponent;


# static fields
.field public static Origin:Ljava/lang/String;


# instance fields
.field protected final _canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

.field protected final _componentName:Ljava/lang/String;

.field protected _container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

.field private final _methods:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "{3FFA4698-AF91-4072-A816-190D68D83F00}"

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_methods:Ljava/util/HashSet;

    .line 25
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_componentName:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method protected failRequestUnknownMethod(ILjava/lang/String;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v1, "\"%s\" is not a valid method."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 96
    return-void
.end method

.method protected failRequestValidation(I)V
    .locals 3
    .param p1, "requestId"    # I

    .prologue
    .line 85
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-eq v1, v2, :cond_0

    .line 86
    const-string v0, "Session is disconnected."

    .line 91
    .local v0, "message":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 92
    return-void

    .line 88
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    const-string v0, "Target is not valid or is not associated with this activity."

    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0
.end method

.method public getCurrentState()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 51
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    goto :goto_0
.end method

.method protected getMetricName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 99
    const-string v0, "%s.%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_componentName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;)V
    .locals 0
    .param p1, "container"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    .line 32
    return-void
.end method

.method protected registerMethod(Ljava/lang/String;)V
    .locals 1
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_methods:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method protected validateMethod(Ljava/lang/String;)Z
    .locals 1
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_methods:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected validateSession()Z
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v0

    return v0
.end method

.method protected validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z
    .locals 6
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-eq v3, v4, :cond_2

    :cond_0
    move v1, v2

    .line 76
    :cond_1
    :goto_0
    return v1

    .line 64
    :cond_2
    sget-boolean v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-nez v3, :cond_1

    .line 68
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v3, v3, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v3

    .line 69
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v4, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 70
    .local v0, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/smartglass/MessageTarget;->equals(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 71
    monitor-exit v3

    goto :goto_0

    .line 74
    .end local v0    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_4
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v2

    .line 76
    goto :goto_0
.end method
