.class Lcom/microsoft/xbox/smartglass/controls/MediaV2;
.super Lcom/microsoft/xbox/smartglass/controls/Media;
.source "MediaV2.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 14
    const-string v0, "Send"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->registerMethod(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getState(ILjava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 20
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;-><init>(Ljava/lang/String;)V

    .line 21
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;
    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;->titleId:I

    if-lez v3, :cond_0

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;->titleId:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 22
    .local v2, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_0
    invoke-virtual {p0, p1, p2, v2}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->getState(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 26
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_1
    return-void

    .line 21
    .restart local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 23
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaGetState;
    :catch_0
    move-exception v0

    .line 24
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid JSON. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_1
.end method

.method protected seek(ILjava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 31
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;-><init>(Ljava/lang/String;)V

    .line 32
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;
    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;->titleId:I

    if-lez v3, :cond_0

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;->titleId:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 33
    .local v2, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_0
    iget-wide v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;->seekPosition:J

    invoke-virtual {p0, p1, v4, v5, v2}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->seek(IJLcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 37
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_1
    return-void

    .line 32
    .restart local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 34
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSeek;
    :catch_0
    move-exception v0

    .line 35
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid JSON. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_1
.end method

.method protected send(ILjava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 42
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;-><init>(Ljava/lang/String;)V

    .line 43
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;
    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;->titleId:I

    if-lez v3, :cond_0

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;->titleId:I

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 44
    .local v2, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_0
    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;->command:I

    invoke-virtual {p0, p1, v3, v2}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->send(IILcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 48
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;
    .end local v2    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_1
    return-void

    .line 43
    .restart local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 45
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonMediaSend;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/MediaV2;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid JSON. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_1
.end method
