.class public Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;
.super Landroid/widget/LinearLayout;
.source "EPGHeaderTimeView.java"


# static fields
.field private static mCache:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSeparator:Landroid/widget/FrameLayout;

.field private mTimeTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mCache:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;
    .locals 3
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 75
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 78
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f0300ef

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    .line 80
    .end local v0    # "inflator":Landroid/view/LayoutInflater;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    goto :goto_0
.end method

.method private static reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    .prologue
    .line 85
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 86
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    const v0, 0x7f0e0576

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mTimeTextView:Landroid/widget/TextView;

    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 51
    const v0, 0x7f0e0577

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mSeparator:Landroid/widget/FrameLayout;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mSeparator:Landroid/widget/FrameLayout;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    return-void
.end method

.method public reclaim()V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;)V

    .line 62
    return-void
.end method

.method public setTime(Ljava/util/Date;)V
    .locals 3
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v0

    .line 57
    .local v0, "cfg":Lcom/microsoft/xbox/xle/ui/EPGViewConfig;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->mTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->formatTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method
