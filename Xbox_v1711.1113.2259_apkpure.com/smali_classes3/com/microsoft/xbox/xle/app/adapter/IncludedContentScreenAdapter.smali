.class public Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "IncludedContentScreenAdapter.java"


# instance fields
.field private includedContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 28
    const v0, 0x7f0e0718

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->screenBody:Landroid/view/View;

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    .line 31
    const v0, 0x7f0e071a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 32
    const v0, 0x7f0e0719

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030155

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 46
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    :cond_0
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->getShouldHideScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/IncludedContentScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 75
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->updateLoadingIndicator(Z)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->includedContent:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->getIncludedContent()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->getIncludedContent()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->includedContent:Ljava/util/ArrayList;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->clear()V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->includedContent:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->includedContent:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentListAdapter;->addAll(Ljava/util/Collection;)V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto :goto_0
.end method
