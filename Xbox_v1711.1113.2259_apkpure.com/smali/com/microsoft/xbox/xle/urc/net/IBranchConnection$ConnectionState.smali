.class public final enum Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
.super Ljava/lang/Enum;
.source "IBranchConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field public static final enum CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field public static final enum CONNECTING:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field public static final enum DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field public static final enum ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTING:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTING:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->$VALUES:[Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    return-object v0
.end method
