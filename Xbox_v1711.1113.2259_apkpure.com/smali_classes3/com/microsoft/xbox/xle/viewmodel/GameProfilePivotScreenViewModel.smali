.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GameProfilePivotScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private gameTitle:Ljava/lang/String;

.field private isLoadingTitleData:Z

.field private loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

.field private titleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 6
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const-wide/16 v4, -0x1

    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 20
    iput-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfilePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 30
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 32
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 33
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 34
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    .line 36
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->gameTitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->gameTitle:Ljava/lang/String;

    .line 40
    :cond_1
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 41
    return-void

    .line 34
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0

    .line 37
    :cond_3
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 40
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->isLoadingTitleData:Z

    return p1
.end method


# virtual methods
.method public getGameTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->gameTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->isLoadingTitleData:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->cancel()V

    .line 74
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->load(Z)V

    .line 76
    return-void
.end method

.method protected onLoadGameProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 79
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onLoadGameProfile Completed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->isLoadingTitleData:Z

    .line 82
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->updateAdapter()V

    .line 98
    return-void

    .line 86
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->titleId:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    .line 87
    .local v0, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->gameTitle:Ljava/lang/String;

    goto :goto_0

    .line 93
    .end local v0    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to load game profile"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfilePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 54
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;->cancel()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->loadGameProfileTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel$LoadGameProfileAsyncTask;

    .line 62
    :cond_0
    return-void
.end method
