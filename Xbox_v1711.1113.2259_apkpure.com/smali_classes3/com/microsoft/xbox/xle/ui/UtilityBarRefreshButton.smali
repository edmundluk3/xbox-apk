.class public Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;
.super Landroid/widget/LinearLayout;
.source "UtilityBarRefreshButton.java"


# instance fields
.field rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030270

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 35
    new-instance v1, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton$1;-><init>(Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method


# virtual methods
.method public setRightPaneContentLayout(Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;)V
    .locals 0
    .param p1, "peopleScreen"    # Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    .line 52
    return-void
.end method
