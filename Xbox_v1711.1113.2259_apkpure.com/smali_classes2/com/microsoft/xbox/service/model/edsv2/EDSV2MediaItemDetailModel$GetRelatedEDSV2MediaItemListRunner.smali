.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MediaItemDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRelatedEDSV2MediaItemListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<TU;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V
    .locals 0

    .prologue
    .line 858
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;

    .prologue
    .line 858
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 858
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 865
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 866
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductRelatedItemList(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 867
    .local v0, "relatedItems":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v2, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1

    .line 870
    .end local v0    # "relatedItems":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xfb0

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 880
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    const-wide/16 v0, 0xfb0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 875
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->access$800(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 876
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 861
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetRelatedEDSV2MediaItemListRunner;"
    return-void
.end method
