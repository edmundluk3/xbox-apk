.class public Lcom/flipkart/chatheads/ui/MinimizedArrangement;
.super Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
.source "MinimizedArrangement.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;"
    }
.end annotation


# static fields
.field public static final BUNDLE_HERO_INDEX_KEY:Ljava/lang/String; = "hero_index"

.field public static final BUNDLE_HERO_RELATIVE_X_KEY:Ljava/lang/String; = "hero_relative_x"

.field public static final BUNDLE_HERO_RELATIVE_Y_KEY:Ljava/lang/String; = "hero_relative_y"

.field private static MAX_VELOCITY_FOR_IDLING:I

.field private static MIN_VELOCITY_TO_POSITION_BACK:I


# instance fields
.field private DELTA:F

.field private currentDelta:F

.field private extras:Landroid/os/Bundle;

.field private hasActivated:Z

.field private hero:Lcom/flipkart/chatheads/ui/ChatHead;

.field private horizontalHeroListener:Lcom/facebook/rebound/SpringListener;

.field private horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

.field private idleStateX:I

.field private idleStateY:I

.field private isTransitioning:Z

.field private manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;"
        }
    .end annotation
.end field

.field private maxHeight:I

.field private maxWidth:I

.field private relativeXPosition:D

.field private relativeYPosition:D

.field private verticalHeroListener:Lcom/facebook/rebound/SpringListener;

.field private verticalSpringChain:Lcom/facebook/rebound/SpringChain;


# direct methods
.method public constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 4
    .param p1, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 74
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;-><init>()V

    .line 23
    iput v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->DELTA:F

    .line 24
    iput v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->currentDelta:F

    .line 25
    iput v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    .line 26
    iput v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hasActivated:Z

    .line 34
    iput-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    .line 35
    iput-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    .line 37
    new-instance v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalHeroListener:Lcom/facebook/rebound/SpringListener;

    .line 53
    new-instance v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$2;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalHeroListener:Lcom/facebook/rebound/SpringListener;

    .line 75
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 76
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->DELTA:F

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)F
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->currentDelta:F

    return v0
.end method

.method static synthetic access$002(Lcom/flipkart/chatheads/ui/MinimizedArrangement;F)F
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->currentDelta:F

    return p1
.end method

.method static synthetic access$100(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)F
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->DELTA:F

    return v0
.end method

.method static synthetic access$200(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)I
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    return-object v0
.end method

.method static synthetic access$400(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Z
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->isTransitioning:Z

    return v0
.end method

.method static synthetic access$402(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->isTransitioning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    return-object v0
.end method

.method private deactivate()V
    .locals 3

    .prologue
    .line 331
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v0

    .line 332
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v2, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    invoke-interface {v1, v2, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 333
    return-void
.end method

.method private getBundle(I)Landroid/os/Bundle;
    .locals 8
    .param p1, "heroIndex"    # I

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 341
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    mul-double/2addr v2, v6

    iget v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    .line 343
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    mul-double/2addr v2, v6

    iget v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxHeight:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->extras:Landroid/os/Bundle;

    .line 347
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 348
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 350
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v1, "hero_index"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    const-string v1, "hero_relative_x"

    iget-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 352
    const-string v1, "hero_relative_y"

    iget-wide v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 353
    return-object v0
.end method

.method private getBundleWithHero()Landroid/os/Bundle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 337
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getHeroIndex()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getBundle(I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private getHeroIndex(Lcom/flipkart/chatheads/ui/ChatHead;)Ljava/lang/Integer;
    .locals 6
    .param p1, "hero"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 365
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v2, 0x0

    .line 366
    .local v2, "heroIndex":I
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    .line 367
    .local v1, "chatHeads":Ljava/util/List;, "Ljava/util/List<Lcom/flipkart/chatheads/ui/ChatHead<TT;>;>;"
    const/4 v3, 0x0

    .line 368
    .local v3, "i":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 369
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    if-ne p1, v0, :cond_0

    .line 370
    move v2, v3

    .line 372
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 373
    goto :goto_0

    .line 374
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    return-object v4
.end method

.method private settleToClosest(Lcom/flipkart/chatheads/ui/ChatHead;II)V
    .locals 10
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "xVelocity"    # I
    .param p3, "yVelocity"    # I

    .prologue
    .line 291
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    .line 292
    .local v0, "activeHorizontalSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    .line 293
    .local v1, "activeVerticalSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v3

    sget-object v4, Lcom/flipkart/chatheads/ui/ChatHead$State;->FREE:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v3, v4, :cond_1

    .line 294
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    const/16 v5, 0x32

    invoke-static {v4, v5}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/util/DisplayMetrics;I)I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 295
    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v4

    iget v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    int-to-double v6, v3

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v8

    sub-double/2addr v6, v8

    cmpg-double v3, v4, v6

    if-gez v3, :cond_4

    .line 296
    const/4 p2, -0x1

    .line 301
    :cond_0
    :goto_0
    if-gez p2, :cond_5

    .line 302
    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v4

    neg-double v4, v4

    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    iget-wide v6, v3, Lcom/facebook/rebound/SpringConfig;->friction:D

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 303
    .local v2, "newVelocity":I
    if-le p2, v2, :cond_1

    .line 304
    move p2, v2

    .line 314
    .end local v2    # "newVelocity":I
    :cond_1
    :goto_1
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_2

    .line 317
    if-gez p2, :cond_6

    .line 318
    const/4 p2, -0x1

    .line 323
    :cond_2
    :goto_2
    if-nez p3, :cond_3

    .line 324
    const/4 p3, 0x1

    .line 326
    :cond_3
    int-to-double v4, p2

    invoke-virtual {v0, v4, v5}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 327
    int-to-double v4, p3

    invoke-virtual {v1, v4, v5}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 328
    return-void

    .line 298
    :cond_4
    const/4 p2, 0x1

    goto :goto_0

    .line 306
    :cond_5
    if-lez p2, :cond_1

    .line 307
    iget v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    int-to-double v4, v3

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    sub-double/2addr v4, v6

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v3

    int-to-double v6, v3

    sub-double/2addr v4, v6

    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    iget-wide v6, v3, Lcom/facebook/rebound/SpringConfig;->friction:D

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 308
    .restart local v2    # "newVelocity":I
    if-le v2, p2, :cond_1

    .line 309
    move p2, v2

    goto :goto_1

    .line 320
    .end local v2    # "newVelocity":I
    :cond_6
    const/4 p2, 0x1

    goto :goto_2
.end method

.method private stickToEdgeX(IILcom/flipkart/chatheads/ui/ChatHead;)I
    .locals 1
    .param p1, "currentX"    # I
    .param p2, "maxWidth"    # I
    .param p3, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 210
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    sub-int v0, p2, p1

    if-ge v0, p1, :cond_0

    .line 212
    invoke-virtual {p3}, Lcom/flipkart/chatheads/ui/ChatHead;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p2, v0

    .line 214
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 6
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 493
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getHeroIndex(Lcom/flipkart/chatheads/ui/ChatHead;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getBundle(I)Landroid/os/Bundle;

    move-result-object v2

    .line 494
    .local v2, "b":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxWidth()I

    move-result v3

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getMaxHeight()I

    move-result v4

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 495
    return-void
.end method

.method public canDrag(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 389
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public getHeroIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 361
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getHeroIndex(Lcom/flipkart/chatheads/ui/ChatHead;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getIdleStatePosition()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 88
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    iget v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public getRetainBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 384
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public handleTouchUp(Lcom/flipkart/chatheads/ui/ChatHead;IILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Z)Z
    .locals 2
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "xVelocity"    # I
    .param p3, "yVelocity"    # I
    .param p4, "activeHorizontalSpring"    # Lcom/facebook/rebound/Spring;
    .param p5, "activeVerticalSpring"    # Lcom/facebook/rebound/Spring;
    .param p6, "wasDragging"    # Z

    .prologue
    .line 278
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->settleToClosest(Lcom/flipkart/chatheads/ui/ChatHead;II)V

    .line 280
    if-nez p6, :cond_0

    .line 281
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->onItemSelected(Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v0

    .line 282
    .local v0, "handled":Z
    if-nez v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->deactivate()V

    .line 284
    const/4 v1, 0x0

    .line 287
    .end local v0    # "handled":Z
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V
    .locals 10
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I
    .param p5, "animated"    # Z

    .prologue
    .line 98
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->isTransitioning:Z

    .line 99
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    if-eqz v6, :cond_1

    .line 100
    :cond_0
    invoke-virtual {p0, p3, p4}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->onDeactivate(II)V

    .line 103
    :cond_1
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    const/16 v7, 0x258

    invoke-static {v6, v7}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/util/DisplayMetrics;I)I

    move-result v6

    sput v6, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->MIN_VELOCITY_TO_POSITION_BACK:I

    .line 104
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/util/DisplayMetrics;I)I

    move-result v6

    sput v6, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->MAX_VELOCITY_FOR_IDLING:I

    .line 105
    const/4 v3, 0x0

    .line 106
    .local v3, "heroIndex":I
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->extras:Landroid/os/Bundle;

    .line 107
    if-eqz p2, :cond_2

    .line 108
    const-string v6, "hero_index"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 109
    const-string v6, "hero_relative_x"

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    invoke-virtual {p2, v6, v8, v9}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    .line 110
    const-string v6, "hero_relative_y"

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    invoke-virtual {p2, v6, v8, v9}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    .line 112
    :cond_2
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    .line 113
    .local v1, "chatHeads":Ljava/util/List;, "Ljava/util/List<Lcom/flipkart/chatheads/ui/ChatHead;>;"
    if-ltz v3, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-le v3, v6, :cond_4

    .line 114
    :cond_3
    const/4 v3, 0x0

    .line 116
    :cond_4
    const/4 v5, 0x0

    .line 117
    .local v5, "zIndex":I
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_a

    .line 118
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/flipkart/chatheads/ui/ChatHead;

    iput-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 119
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/flipkart/chatheads/ui/ChatHead;->setHero(Z)V

    .line 120
    invoke-static {}, Lcom/facebook/rebound/SpringChain;->create()Lcom/facebook/rebound/SpringChain;

    move-result-object v6

    iput-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    .line 121
    invoke-static {}, Lcom/facebook/rebound/SpringChain;->create()Lcom/facebook/rebound/SpringChain;

    move-result-object v6

    iput-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    .line 122
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_6

    .line 123
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 124
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eq v0, v6, :cond_5

    .line 125
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/flipkart/chatheads/ui/ChatHead;->setHero(Z)V

    .line 126
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    new-instance v7, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;

    invoke-direct {v7, p0, v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$3;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Lcom/flipkart/chatheads/ui/ChatHead;)V

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->addSpring(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/SpringChain;

    .line 134
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v6}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v7}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/rebound/Spring;

    .line 135
    .local v2, "currentSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 136
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    new-instance v7, Lcom/flipkart/chatheads/ui/MinimizedArrangement$4;

    invoke-direct {v7, p0, v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$4;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Lcom/flipkart/chatheads/ui/ChatHead;)V

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->addSpring(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/SpringChain;

    .line 142
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v6}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v7}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "currentSpring":Lcom/facebook/rebound/Spring;
    check-cast v2, Lcom/facebook/rebound/Spring;

    .line 143
    .restart local v2    # "currentSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 144
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v6}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->bringToFront(Landroid/view/View;)V

    .line 145
    add-int/lit8 v5, v5, 0x1

    .line 122
    .end local v2    # "currentSpring":Lcom/facebook/rebound/Spring;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_6
    iget-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_b

    .line 149
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getInitialPosition()Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->x:I

    iput v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    .line 153
    :goto_1
    iget-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_c

    .line 154
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getInitialPosition()Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    iput v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    .line 159
    :goto_2
    iget v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-direct {p0, v6, p3, v7}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->stickToEdgeX(IILcom/flipkart/chatheads/ui/ChatHead;)I

    move-result v6

    iput v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    .line 161
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 162
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v6}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadContainer()Lcom/flipkart/chatheads/ui/ChatHeadContainer;

    move-result-object v6

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-interface {v6, v7}, Lcom/flipkart/chatheads/ui/ChatHeadContainer;->bringToFront(Landroid/view/View;)V

    .line 163
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    new-instance v7, Lcom/flipkart/chatheads/ui/MinimizedArrangement$5;

    invoke-direct {v7, p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$5;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->addSpring(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/SpringChain;

    .line 165
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    new-instance v7, Lcom/flipkart/chatheads/ui/MinimizedArrangement$6;

    invoke-direct {v7, p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement$6;-><init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->addSpring(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/SpringChain;

    .line 167
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->setControlSpringIndex(I)Lcom/facebook/rebound/SpringChain;

    .line 168
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/SpringChain;->setControlSpringIndex(I)Lcom/facebook/rebound/SpringChain;

    .line 170
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalHeroListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 171
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget-object v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalHeroListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 173
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    sget-object v7, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 174
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    iget v8, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    int-to-double v8, v8

    cmpl-double v6, v6, v8

    if-nez v6, :cond_7

    .line 176
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    add-int/lit8 v7, v7, -0x1

    int-to-double v8, v7

    const/4 v7, 0x1

    invoke-virtual {v6, v8, v9, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 179
    :cond_7
    if-eqz p5, :cond_d

    .line 180
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    int-to-double v8, v7

    invoke-virtual {v6, v8, v9}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 185
    :goto_3
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    sget-object v7, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v6, v7}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 186
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    iget v8, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    int-to-double v8, v8

    cmpl-double v6, v6, v8

    if-nez v6, :cond_8

    .line 188
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    add-int/lit8 v7, v7, -0x1

    int-to-double v8, v7

    const/4 v7, 0x1

    invoke-virtual {v6, v8, v9, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    .line 190
    :cond_8
    if-eqz p5, :cond_e

    .line 191
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    int-to-double v8, v7

    invoke-virtual {v6, v8, v9}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 199
    :cond_9
    :goto_4
    iput p3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    .line 200
    iput p4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxHeight:I

    .line 201
    invoke-interface {p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setEnabled(Z)V

    .line 203
    .end local v4    # "i":I
    :cond_a
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hasActivated:Z

    .line 207
    return-void

    .line 151
    .restart local v4    # "i":I
    :cond_b
    iget-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeXPosition:D

    int-to-double v8, p3

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    goto/16 :goto_1

    .line 156
    :cond_c
    iget-wide v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->relativeYPosition:D

    int-to-double v8, p4

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    goto/16 :goto_2

    .line 182
    :cond_d
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    int-to-double v8, v7

    const/4 v7, 0x1

    invoke-virtual {v6, v8, v9, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    goto :goto_3

    .line 193
    :cond_e
    iget-object v6, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v6}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    iget v7, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    int-to-double v8, v7

    const/4 v7, 0x1

    invoke-virtual {v6, v8, v9, v7}, Lcom/facebook/rebound/Spring;->setCurrentValue(DZ)Lcom/facebook/rebound/Spring;

    goto :goto_4
.end method

.method public onCapture(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 1
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 242
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeAllChatHeads(Z)V

    .line 243
    return-void
.end method

.method public onChatHeadAdded(Lcom/flipkart/chatheads/ui/ChatHead;Z)V
    .locals 6
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "animated"    # Z

    .prologue
    .line 220
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    iget v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->currentDelta:F

    float-to-double v4, v1

    sub-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 222
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->getRetainBundle()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxHeight:I

    move-object v0, p0

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 226
    return-void
.end method

.method public onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 6
    .param p1, "removed"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 231
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 232
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-ne p1, v0, :cond_0

    .line 233
    iput-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->maxHeight:I

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 237
    return-void
.end method

.method public onConfigChanged(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
    .locals 0
    .param p1, "newConfig"    # Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .prologue
    .line 380
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    return-void
.end method

.method public onDeactivate(II)V
    .locals 5
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v4, 0x0

    .line 252
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hasActivated:Z

    .line 253
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v2, :cond_0

    .line 254
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v2

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalHeroListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v2, v3}, Lcom/facebook/rebound/Spring;->removeListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 255
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v2

    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalHeroListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v2, v3}, Lcom/facebook/rebound/Spring;->removeListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 257
    :cond_0
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    if-eqz v2, :cond_1

    .line 258
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v2}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v0

    .line 259
    .local v0, "allSprings":Ljava/util/List;, "Ljava/util/List<Lcom/facebook/rebound/Spring;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/rebound/Spring;

    .line 260
    .local v1, "spring":Lcom/facebook/rebound/Spring;
    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->destroy()V

    goto :goto_0

    .line 263
    .end local v0    # "allSprings":Ljava/util/List;, "Ljava/util/List<Lcom/facebook/rebound/Spring;>;"
    .end local v1    # "spring":Lcom/facebook/rebound/Spring;
    :cond_1
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    if-eqz v2, :cond_2

    .line 264
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    invoke-virtual {v2}, Lcom/facebook/rebound/SpringChain;->getAllSprings()Ljava/util/List;

    move-result-object v0

    .line 265
    .restart local v0    # "allSprings":Ljava/util/List;, "Ljava/util/List<Lcom/facebook/rebound/Spring;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/rebound/Spring;

    .line 266
    .restart local v1    # "spring":Lcom/facebook/rebound/Spring;
    invoke-virtual {v1}, Lcom/facebook/rebound/Spring;->destroy()V

    goto :goto_1

    .line 270
    .end local v0    # "allSprings":Ljava/util/List;, "Ljava/util/List<Lcom/facebook/rebound/Spring;>;"
    .end local v1    # "spring":Lcom/facebook/rebound/Spring;
    :cond_2
    iput-object v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->horizontalSpringChain:Lcom/facebook/rebound/SpringChain;

    .line 271
    iput-object v4, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->verticalSpringChain:Lcom/facebook/rebound/SpringChain;

    .line 272
    return-void
.end method

.method public onReloadFragment(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 0
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 500
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    return-void
.end method

.method public onSpringUpdate(Lcom/flipkart/chatheads/ui/ChatHead;ZIILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;I)V
    .locals 18
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "isDragging"    # Z
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I
    .param p5, "spring"    # Lcom/facebook/rebound/Spring;
    .param p6, "activeHorizontalSpring"    # Lcom/facebook/rebound/Spring;
    .param p7, "activeVerticalSpring"    # Lcom/facebook/rebound/Spring;
    .param p8, "totalVelocity"    # I

    .prologue
    .line 405
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v8

    .line 406
    .local v8, "xVelocity":D
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v12

    .line 407
    .local v12, "yVelocity":D
    if-nez p2, :cond_1

    invoke-static/range {p8 .. p8}, Ljava/lang/Math;->abs(I)I

    move-result v14

    sget v15, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->MIN_VELOCITY_TO_POSITION_BACK:I

    if-ge v14, v15, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_1

    .line 409
    invoke-static/range {p8 .. p8}, Ljava/lang/Math;->abs(I)I

    move-result v14

    sget v15, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->MAX_VELOCITY_FOR_IDLING:I

    if-ge v14, v15, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/ChatHead$State;->FREE:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v14, v15, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hasActivated:Z

    if-eqz v14, :cond_0

    .line 410
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v14

    double-to-int v14, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->setIdleStateX(I)V

    .line 411
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v14

    double-to-int v14, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->setIdleStateY(I)V

    .line 413
    :cond_0
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    if-ne v0, v1, :cond_7

    .line 415
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    .line 416
    .local v6, "xPosition":D
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v14

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v14

    int-to-double v14, v14

    add-double/2addr v14, v6

    move/from16 v0, p3

    int-to-double v0, v0

    move-wide/from16 v16, v0

    cmpl-double v14, v14, v16

    if-lez v14, :cond_6

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmpl-double v14, v14, v16

    if-lez v14, :cond_6

    .line 419
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v14

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v14

    sub-int v3, p3, v14

    .line 420
    .local v3, "newPos":I
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 421
    int-to-double v14, v3

    move-object/from16 v0, p6

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 454
    .end local v3    # "newPos":I
    .end local v6    # "xPosition":D
    :cond_1
    :goto_0
    if-nez p2, :cond_5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->hero:Lcom/flipkart/chatheads/ui/ChatHead;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_5

    .line 459
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadCoordsForCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)[I

    move-result-object v2

    .line 460
    .local v2, "coords":[I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v16

    div-int/lit8 v16, v16, 0x2

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v15, v15, v16

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v17

    div-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    add-float v16, v16, v17

    invoke-interface/range {v14 .. v16}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDistanceCloseButtonFromHead(FF)D

    move-result-wide v4

    .line 462
    .local v4, "distanceCloseButtonFromHead":D
    move-object/from16 v0, p1

    iget v14, v0, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    int-to-double v14, v14

    cmpg-double v14, v4, v14

    if-gez v14, :cond_2

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-ne v14, v15, :cond_2

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-ne v14, v15, :cond_2

    .line 463
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 464
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 465
    sget-object v14, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/flipkart/chatheads/ui/ChatHead;->setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V

    .line 468
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v14, v15, :cond_3

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    if-eq v14, v15, :cond_3

    .line 469
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 470
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 471
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 472
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 473
    const/4 v14, 0x0

    aget v14, v2, v14

    int-to-double v14, v14

    move-object/from16 v0, p6

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 474
    const/4 v14, 0x1

    aget v14, v2, v14

    int-to-double v14, v14

    move-object/from16 v0, p7

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 477
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v14

    sget-object v15, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v14, v15, :cond_4

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 478
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    .line 479
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->captureChatHeads(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 481
    :cond_4
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v14

    if-nez v14, :cond_9

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->isTransitioning:Z

    if-nez v14, :cond_9

    .line 482
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v14

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->appear()V

    .line 489
    .end local v2    # "coords":[I
    .end local v4    # "distanceCloseButtonFromHead":D
    :cond_5
    :goto_1
    return-void

    .line 422
    .restart local v6    # "xPosition":D
    :cond_6
    const-wide/16 v14, 0x0

    cmpg-double v14, v6, v14

    if-gez v14, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmpg-double v14, v14, v16

    if-gez v14, :cond_1

    .line 425
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 426
    const-wide/16 v14, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    goto/16 :goto_0

    .line 433
    .end local v6    # "xPosition":D
    :cond_7
    move-object/from16 v0, p5

    move-object/from16 v1, p7

    if-ne v0, v1, :cond_1

    .line 434
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v10

    .line 435
    .local v10, "yPosition":D
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v14

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v14

    int-to-double v14, v14

    add-double/2addr v14, v10

    move/from16 v0, p4

    int-to-double v0, v0

    move-wide/from16 v16, v0

    cmpl-double v14, v14, v16

    if-lez v14, :cond_8

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmpl-double v14, v14, v16

    if-lez v14, :cond_8

    .line 439
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 440
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v14

    invoke-virtual {v14}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v14

    sub-int v14, p4, v14

    int-to-double v14, v14

    move-object/from16 v0, p7

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    goto/16 :goto_0

    .line 441
    :cond_8
    const-wide/16 v14, 0x0

    cmpg-double v14, v10, v14

    if-gez v14, :cond_1

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getVelocity()D

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmpg-double v14, v14, v16

    if-gez v14, :cond_1

    .line 445
    sget-object v14, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v14}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 446
    const-wide/16 v14, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v14, v15}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    goto/16 :goto_0

    .line 484
    .end local v10    # "yPosition":D
    .restart local v2    # "coords":[I
    .restart local v4    # "distanceCloseButtonFromHead":D
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v14}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v14

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    goto/16 :goto_1
.end method

.method public removeOldestChatHead()V
    .locals 4

    .prologue
    .line 394
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 395
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v2

    if-nez v2, :cond_0

    .line 396
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeChatHead(Ljava/io/Serializable;Z)Z

    .line 400
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    :cond_1
    return-void
.end method

.method public selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 0
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 248
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    return-void
.end method

.method public setContainer(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 0
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    .line 93
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 94
    return-void
.end method

.method public setIdleStateX(I)V
    .locals 0
    .param p1, "idleStateX"    # I

    .prologue
    .line 80
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iput p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateX:I

    .line 81
    return-void
.end method

.method public setIdleStateY(I)V
    .locals 0
    .param p1, "idleStateY"    # I

    .prologue
    .line 84
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    iput p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->idleStateY:I

    .line 85
    return-void
.end method

.method public shouldShowCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 504
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement<TT;>;"
    const/4 v0, 0x1

    return v0
.end method
