.class public Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;
.super Ljava/lang/Object;
.source "UTCAppLaunch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;
    }
.end annotation


# static fields
.field private static final APPLAUNCHVERSION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "UTCLOGGING"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;)V
    .locals 1
    .param p0, "launchType"    # Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V

    .line 40
    return-void
.end method

.method public static track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V
    .locals 1
    .param p0, "launchType"    # Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;
    .param p1, "launchIntent"    # Landroid/content/Intent;

    .prologue
    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;-><init>(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 122
    return-void
.end method

.method public static trackDelegatedSigninComplete(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)V
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .prologue
    .line 125
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$2;-><init>(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 133
    return-void
.end method
