.class public Lcom/microsoft/xbox/service/model/pins/PinsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "PinsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;,
        Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;,
        Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;,
        Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;,
        Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;,
        Lcom/microsoft/xbox/service/model/pins/PinsModel$PinsModelContainer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final KEY_REQUEST_ID:Ljava/lang/String; = "requestId"

.field private static final PIN_RUNNER_TIMEOUT_IN_MS:I = 0x7530

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final fetchedPins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private getRunner:Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

.field private myPins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private final pinMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private final storeService:Lcom/microsoft/xbox/service/store/IStoreService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    .line 57
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lifetime:J

    .line 58
    new-instance v0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/pins/PinsModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getRunner:Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->fetchedPins:Ljava/util/List;

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel$1;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->onRemovePinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/model/pins/PinsModel;JLcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p4, "x3"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p5, "x4"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p6, "x5"    # Z

    .prologue
    .line 44
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->onMovePinCompleted(JLcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/epg/EPListItem;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getStoreProductList(Lcom/microsoft/xbox/service/model/epg/EPListItem;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->fetchedPins:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->onGetPinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getRunner:Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->onAddPinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel$PinsModelContainer;->access$200()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    return-object v0
.end method

.method private getStoreProductList(Lcom/microsoft/xbox/service/model/epg/EPListItem;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .locals 8
    .param p1, "listItem"    # Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/epg/EPItem;->ItemId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 221
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/epg/EPItem;->ItemId:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseUUID(Ljava/lang/String;Ljava/util/UUID;)Ljava/util/UUID;

    move-result-object v2

    .line 223
    .local v2, "uuid":Ljava/util/UUID;
    if-eqz v2, :cond_2

    .line 224
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/epg/EPItem;->ItemId:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductFromLegacyProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    .line 239
    .end local v2    # "uuid":Ljava/util/UUID;
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 240
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 242
    :cond_1
    return-object v3

    .line 228
    .restart local v2    # "uuid":Ljava/util/UUID;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/epg/EPItem;->ItemId:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductFromPackageFamilyName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    goto :goto_0

    .line 231
    .end local v2    # "uuid":Ljava/util/UUID;
    :cond_3
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 232
    iget-object v4, p1, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 233
    .local v1, "titleId":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 234
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductFromTitleId(J)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    goto :goto_0
.end method

.method private onAddPinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 4
    .param p2, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;[",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->isLoading:Z

    .line 139
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 140
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lastRefreshTime:Ljava/util/Date;

    .line 141
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->PinsUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 143
    :cond_0
    return-void
.end method

.method private onGetPinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 118
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 120
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 122
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 123
    .local v0, "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 125
    :cond_0
    const-string v2, "PinsModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pin item does not have bing id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 134
    .end local v0    # "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_2
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->GetPins:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {v1, v2, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 135
    return-void
.end method

.method private onMovePinCompleted(JLcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V
    .locals 9
    .param p1, "requestId"    # J
    .param p4, "srcItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p5, "dstItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p6, "beforeDstItem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p3, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v5, -0x1

    .line 185
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->isLoading:Z

    .line 186
    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    if-nez v4, :cond_0

    .line 189
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-interface {v4, p4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 190
    .local v3, "srcIdx":I
    if-eq v3, v5, :cond_0

    .line 191
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-interface {v4, p5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 192
    .local v0, "dstIdx":I
    if-eq v0, v5, :cond_0

    .line 193
    if-eq v0, v3, :cond_0

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 195
    .local v2, "newPins":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 196
    invoke-virtual {v2, v0, p4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 197
    iput-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    .line 203
    .end local v0    # "dstIdx":I
    .end local v2    # "newPins":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    .end local v3    # "srcIdx":I
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 204
    .local v1, "extras":Landroid/os/Bundle;
    const-string v4, "requestId"

    invoke-virtual {v1, v4, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 205
    new-instance v4, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->PinMoved:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v6

    invoke-direct {v4, v5, p0, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 206
    return-void
.end method

.method private onRemovePinsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 9
    .param p2, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;[",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v6, 0x0

    .line 146
    iput-boolean v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->isLoading:Z

    .line 147
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    sget-object v7, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v5, v7, :cond_9

    .line 148
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lastRefreshTime:Ljava/util/Date;

    .line 149
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 151
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 152
    .local v4, "removedPins":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    array-length v7, p2

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_3

    aget-object v1, p2, v5

    .line 153
    .local v1, "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 155
    :cond_0
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 158
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 159
    .local v2, "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    if-eqz v2, :cond_2

    .line 160
    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 165
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    .end local v2    # "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_3
    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 166
    new-instance v3, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 167
    .local v3, "remainingPins":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 168
    .restart local v1    # "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v0

    .line 169
    .restart local v0    # "key":Ljava/lang/String;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_6

    .line 170
    :cond_5
    iget-object v0, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 173
    :cond_6
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 174
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 177
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_7
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    .line 179
    .end local v3    # "remainingPins":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    :cond_8
    new-instance v5, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v6, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v7, Lcom/microsoft/xbox/service/model/UpdateType;->PinsUpdated:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v8, 0x1

    invoke-direct {v6, v7, v8}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v7, 0x0

    invoke-direct {v5, v6, p0, v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 182
    .end local v4    # "removedPins":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    :cond_9
    return-void
.end method

.method public static reset()V
    .locals 0

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel$PinsModelContainer;->access$300()V

    .line 90
    return-void
.end method


# virtual methods
.method public varargs add([Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "pins"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 102
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method

.method public getPins()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->myPins:Ljava/util/List;

    return-object v0
.end method

.method public hasPin(Ljava/lang/String;)Z
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->pinMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getRunner:Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadAsync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 97
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetPins:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getRunner:Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 98
    return-void
.end method

.method public move(JLcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 13
    .param p1, "requestId"    # J
    .param p3, "srcItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p4, "dstItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p5, "beforeDstItem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 112
    const/4 v7, 0x1

    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lifetime:J

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v11, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;JLcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    move v1, v7

    move-wide v2, v8

    move-object v4, v10

    move-object v5, v11

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public varargs remove([Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "pins"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 107
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method
