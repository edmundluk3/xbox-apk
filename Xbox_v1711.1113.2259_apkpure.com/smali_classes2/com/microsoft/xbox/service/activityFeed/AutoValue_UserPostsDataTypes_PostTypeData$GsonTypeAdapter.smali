.class public final Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_UserPostsDataTypes_PostTypeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final achievementIdsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private defaultAchievementIds:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultDescription:Ljava/lang/String;

.field private defaultHostXuid:Ljava/lang/String;

.field private defaultLocator:Ljava/lang/String;

.field private defaultPostedTime:Ljava/util/Date;

.field private defaultScid:Ljava/lang/String;

.field private defaultSessionId:Ljava/lang/String;

.field private defaultStartTime:Ljava/util/Date;

.field private defaultTags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTitleId:Ljava/lang/String;

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final hostXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final locatorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final postedTimeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final scidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final startTimeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultLocator:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultHostXuid:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultScid:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 40
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultAchievementIds:Lcom/google/common/collect/ImmutableList;

    .line 41
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 42
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultPostedTime:Ljava/util/Date;

    .line 44
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->locatorAdapter:Lcom/google/gson/TypeAdapter;

    .line 45
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->hostXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 46
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->scidAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->achievementIdsAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->postedTimeAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .locals 13
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v12, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v12, :cond_0

    .line 128
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 129
    const/4 v0, 0x0

    .line 195
    :goto_0
    return-object v0

    .line 131
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultLocator:Ljava/lang/String;

    .line 133
    .local v1, "locator":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultHostXuid:Ljava/lang/String;

    .line 134
    .local v2, "hostXuid":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 135
    .local v3, "titleId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultScid:Ljava/lang/String;

    .line 136
    .local v4, "scid":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 137
    .local v5, "sessionId":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 138
    .local v6, "description":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 139
    .local v7, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultAchievementIds:Lcom/google/common/collect/ImmutableList;

    .line 140
    .local v8, "achievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 141
    .local v9, "startTime":Ljava/util/Date;
    iget-object v10, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultPostedTime:Ljava/util/Date;

    .line 142
    .local v10, "postedTime":Ljava/util/Date;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v11

    .line 144
    .local v11, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v12, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v12, :cond_1

    .line 145
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 148
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 190
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 148
    :sswitch_0
    const-string v12, "locator"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v12, "hostXuid"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v12, "titleId"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v12, "scid"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v12, "sessionId"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v12, "description"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v12, "tags"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v12, "achievementIds"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v12, "startTime"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/16 v0, 0x8

    goto :goto_2

    :sswitch_9
    const-string v12, "postedTime"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/16 v0, 0x9

    goto :goto_2

    .line 150
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->locatorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "locator":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 151
    .restart local v1    # "locator":Ljava/lang/String;
    goto/16 :goto_1

    .line 154
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->hostXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "hostXuid":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 155
    .restart local v2    # "hostXuid":Ljava/lang/String;
    goto/16 :goto_1

    .line 158
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "titleId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 159
    .restart local v3    # "titleId":Ljava/lang/String;
    goto/16 :goto_1

    .line 162
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->scidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "scid":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 163
    .restart local v4    # "scid":Ljava/lang/String;
    goto/16 :goto_1

    .line 166
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "sessionId":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 167
    .restart local v5    # "sessionId":Ljava/lang/String;
    goto/16 :goto_1

    .line 170
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "description":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 171
    .restart local v6    # "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 174
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    check-cast v7, Lcom/google/common/collect/ImmutableList;

    .line 175
    .restart local v7    # "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 178
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->achievementIdsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "achievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    check-cast v8, Lcom/google/common/collect/ImmutableList;

    .line 179
    .restart local v8    # "achievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 182
    :pswitch_8
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "startTime":Ljava/util/Date;
    check-cast v9, Ljava/util/Date;

    .line 183
    .restart local v9    # "startTime":Ljava/util/Date;
    goto/16 :goto_1

    .line 186
    :pswitch_9
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->postedTimeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "postedTime":Ljava/util/Date;
    check-cast v10, Ljava/util/Date;

    .line 187
    .restart local v10    # "postedTime":Ljava/util/Date;
    goto/16 :goto_1

    .line 194
    .end local v11    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 195
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData;

    invoke-direct/range {v0 .. v10}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/util/Date;Ljava/util/Date;)V

    goto/16 :goto_0

    .line 148
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7eea75b1 -> :sswitch_8
        -0x66ca7c04 -> :sswitch_5
        -0x4deb0a6d -> :sswitch_2
        -0x11e85960 -> :sswitch_1
        -0xf5272f7 -> :sswitch_7
        0x35c76b -> :sswitch_3
        0x363419 -> :sswitch_6
        0x142bdc96 -> :sswitch_0
        0x243a3e51 -> :sswitch_4
        0x7e4a0cac -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAchievementIds(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "defaultAchievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultAchievementIds:Lcom/google/common/collect/ImmutableList;

    .line 85
    return-object p0
.end method

.method public setDefaultDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDescription"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public setDefaultHostXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultHostXuid"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultHostXuid:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setDefaultLocator(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLocator"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultLocator:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method public setDefaultPostedTime(Ljava/util/Date;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPostedTime"    # Ljava/util/Date;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultPostedTime:Ljava/util/Date;

    .line 93
    return-object p0
.end method

.method public setDefaultScid(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultScid"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultScid:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public setDefaultSessionId(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionId"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public setDefaultStartTime(Ljava/util/Date;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultStartTime"    # Ljava/util/Date;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultStartTime:Ljava/util/Date;

    .line 89
    return-object p0
.end method

.method public setDefaultTags(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "defaultTags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTags:Lcom/google/common/collect/ImmutableList;

    .line 81
    return-object p0
.end method

.method public setDefaultTitleId(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleId"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->defaultTitleId:Ljava/lang/String;

    .line 65
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    if-nez p2, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 124
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 103
    const-string v0, "locator"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->locatorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->locator()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 105
    const-string v0, "hostXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->hostXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->hostXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 107
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 109
    const-string v0, "scid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->scidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->scid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 111
    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->sessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 113
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 115
    const-string v0, "tags"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 117
    const-string v0, "achievementIds"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->achievementIdsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->achievementIds()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 119
    const-string v0, "startTime"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->startTimeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->startTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 121
    const-string v0, "postedTime"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->postedTimeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->postedTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 123
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V

    return-void
.end method
