.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ClubCustomizeImagePickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubImageAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final pictureList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p2, "pictureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 237
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    .line 238
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
    .param p2, "x1"    # Ljava/util/List;
    .param p3, "x2"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$1;

    .prologue
    .line 232
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addAll(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p1, "pictureList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 243
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->notifyDataSetChanged()V

    .line 244
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 232
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->pictureList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->getItemCount()I

    move-result v1

    invoke-virtual {p1, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;->bindTo(Ljava/lang/String;II)V

    .line 259
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 248
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubImageAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ImageViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;Landroid/view/ViewGroup;)V

    return-object v0
.end method
