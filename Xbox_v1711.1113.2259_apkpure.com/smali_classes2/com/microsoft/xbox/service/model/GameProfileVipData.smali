.class public Lcom/microsoft/xbox/service/model/GameProfileVipData;
.super Lcom/microsoft/xbox/service/model/FollowersData;
.source "GameProfileVipData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public titleName:Ljava/lang/String;

.field public vipType:Ljava/lang/String;

.field public vipTypeId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/GameProfileVipData;)V
    .locals 2
    .param p1, "vip"    # Lcom/microsoft/xbox/service/model/GameProfileVipData;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>()V

    .line 22
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->xuid:Ljava/lang/String;

    .line 23
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 24
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 25
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->titleName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->titleName:Ljava/lang/String;

    .line 26
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->titleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->titleId:J

    .line 27
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipType:Ljava/lang/String;

    .line 28
    iget v0, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipTypeId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipTypeId:I

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p1, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 19
    return-void
.end method


# virtual methods
.method public updateVipInfo(Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;)V
    .locals 1
    .param p1, "vip"    # Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;

    .prologue
    .line 32
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;->vipType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipType:Ljava/lang/String;

    .line 33
    iget v0, p1, Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;->vipTypeId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;->vipTypeId:I

    .line 34
    return-void
.end method
