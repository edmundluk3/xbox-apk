.class Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "CustomizeProfileScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGamertagSuggestionsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final seed:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "seed"    # Ljava/lang/String;

    .prologue
    .line 1040
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1041
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->seed:Ljava/lang/String;

    .line 1042
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1046
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1047
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 1072
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    .line 1075
    .local v1, "serviceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->seed:Ljava/lang/String;

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGamertagSuggestions(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$2302(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1080
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v2

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1067
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 1052
    const-string v0, "GetSuggestedGamertagsAsyncTask should always run if requested"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1053
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$2200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 1063
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1036
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$GetGamertagSuggestionsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1057
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1058
    return-void
.end method
