.class public final Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequestSerializer;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"

# interfaces
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubProfileUpdateRequestSerializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonSerializer",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serialize(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 7
    .param p1, "src"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
    .param p2, "typeOfSrc"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonSerializationContext;

    .prologue
    .line 81
    new-instance v1, Lcom/google/gson/JsonObject;

    invoke-direct {v1}, Lcom/google/gson/JsonObject;-><init>()V

    .line 83
    .local v1, "jsonObject":Lcom/google/gson/JsonObject;
    const-string v4, "modifiedFields"

    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->access$000(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Ljava/util/List;

    move-result-object v5

    invoke-interface {p3, v5}, Lcom/google/gson/JsonSerializationContext;->serialize(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 86
    iget-object v4, p1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->requestContract:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    invoke-interface {p3, v4}, Lcom/google/gson/JsonSerializationContext;->serialize(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v2

    .line 87
    .local v2, "requestContract":Lcom/google/gson/JsonObject;
    iget-object v4, p1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->requestContract:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    invoke-interface {p3, v4}, Lcom/google/gson/JsonSerializationContext;->serialize(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v3

    .line 89
    .local v3, "requestContractCopy":Lcom/google/gson/JsonObject;
    invoke-virtual {v3}, Lcom/google/gson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 90
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->access$000(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 91
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/gson/JsonObject;->remove(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    goto :goto_0

    .line 95
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/gson/JsonElement;>;"
    :cond_1
    const-string v4, "requestContract"

    invoke-virtual {v1, v4, v2}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 97
    return-object v1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 1

    .prologue
    .line 78
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequestSerializer;->serialize(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method
