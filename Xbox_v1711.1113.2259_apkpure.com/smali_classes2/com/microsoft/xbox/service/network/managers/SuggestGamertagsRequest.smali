.class public Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
.super Ljava/lang/Object;
.source "SuggestGamertagsRequest.java"


# instance fields
.field public algorithm:I

.field public count:I

.field public locale:Ljava/lang/String;

.field public seed:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "locale"    # Ljava/lang/String;
    .param p3, "seed"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;->algorithm:I

    .line 10
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;->count:I

    .line 11
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;->locale:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;->seed:Ljava/lang/String;

    .line 13
    return-void
.end method
