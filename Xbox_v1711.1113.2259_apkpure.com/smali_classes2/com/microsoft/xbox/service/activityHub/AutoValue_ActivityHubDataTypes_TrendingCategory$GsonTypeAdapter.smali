.class public final Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ActivityHubDataTypes_TrendingCategory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;",
        ">;"
    }
.end annotation


# instance fields
.field private final categoryAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCategory:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

.field private defaultResults:Lcom/google/gson/JsonArray;

.field private final resultsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/gson/JsonArray;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultCategory:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultResults:Lcom/google/gson/JsonArray;

    .line 25
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->categoryAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    const-class v0, Lcom/google/gson/JsonArray;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->resultsAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 54
    const/4 v3, 0x0

    .line 80
    :goto_0
    return-object v3

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultCategory:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 58
    .local v1, "category":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    iget-object v2, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultResults:Lcom/google/gson/JsonArray;

    .line 59
    .local v2, "results":Lcom/google/gson/JsonArray;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 65
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 75
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 65
    :sswitch_0
    const-string v4, "category"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "results"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 67
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->categoryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "category":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 68
    .restart local v1    # "category":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    goto :goto_1

    .line 71
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->resultsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "results":Lcom/google/gson/JsonArray;
    check-cast v2, Lcom/google/gson/JsonArray;

    .line 72
    .restart local v2    # "results":Lcom/google/gson/JsonArray;
    goto :goto_1

    .line 79
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 80
    new-instance v3, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory;-><init>(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;Lcom/google/gson/JsonArray;)V

    goto :goto_0

    .line 65
    :sswitch_data_0
    .sparse-switch
        0x302bcfe -> :sswitch_0
        0x416b3bf6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCategory(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCategory"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultCategory:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    .line 30
    return-object p0
.end method

.method public setDefaultResults(Lcom/google/gson/JsonArray;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultResults"    # Lcom/google/gson/JsonArray;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->defaultResults:Lcom/google/gson/JsonArray;

    .line 34
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    if-nez p2, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 49
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 44
    const-string v0, "category"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->categoryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;->category()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 46
    const-string v0, "results"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->resultsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;->results()Lcom/google/gson/JsonArray;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 48
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingCategory$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;)V

    return-void
.end method
