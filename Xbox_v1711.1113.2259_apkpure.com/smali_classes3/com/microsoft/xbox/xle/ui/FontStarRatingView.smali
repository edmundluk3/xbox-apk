.class public Lcom/microsoft/xbox/xle/ui/FontStarRatingView;
.super Landroid/widget/RelativeLayout;
.source "FontStarRatingView.java"


# instance fields
.field private backgroundStarsView:Landroid/widget/TextView;

.field private foregroundSpace:Landroid/view/View;

.field private foregroundStarsView:Landroid/widget/TextView;

.field private ratingCountTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    const-string v6, "layout_inflater"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 42
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030104

    const/4 v7, 0x1

    invoke-virtual {v5, v6, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 44
    const v6, 0x7f0e05b0

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    .line 45
    const v6, 0x7f0e05b1

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    .line 46
    const v6, 0x7f0e05b3

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    .line 47
    const v6, 0x7f0e05b2

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundSpace:Landroid/view/View;

    .line 49
    if-eqz p2, :cond_5

    .line 50
    sget-object v6, Lcom/microsoft/xboxone/smartglass/R$styleable;->FontStarRatingView:[I

    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 52
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    const/4 v7, 0x1

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 54
    :cond_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 55
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    const/4 v7, 0x0

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0142

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 57
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_3

    .line 58
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 59
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    const/4 v7, 0x2

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0142

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    :cond_2
    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 62
    .local v2, "hideRating":Z
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_8

    const/16 v6, 0x8

    :goto_0
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 64
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/high16 v9, 0x41500000    # 13.0f

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 67
    .end local v2    # "hideRating":Z
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_4

    .line 68
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 69
    const/4 v6, 0x5

    const/high16 v7, 0x41500000    # 13.0f

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    .line 70
    .local v4, "starSize":F
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 71
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 74
    .end local v4    # "starSize":F
    :cond_4
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_5
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070f2f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "starIcon":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "fiveStarString":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_6

    .line 80
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->backgroundStarsView:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_6
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    if-eqz v6, :cond_7

    .line 83
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_7
    return-void

    .line 62
    .end local v1    # "fiveStarString":Ljava/lang/String;
    .end local v3    # "starIcon":Ljava/lang/String;
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v2    # "hideRating":Z
    :cond_8
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setAverageUserRatingAndUserCount(FJ)V
    .locals 4
    .param p1, "averageUserRating"    # F
    .param p2, "userRateCount"    # J

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, -0x2

    .line 89
    cmpl-float v0, p1, v2

    if-lez v0, :cond_0

    .line 90
    const/high16 p1, 0x3f800000    # 1.0f

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    sub-float/2addr v2, p1

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundSpace:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x1

    invoke-direct {v1, v3, v2, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->foregroundStarsView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/FontStarRatingView;->ratingCountTextView:Landroid/widget/TextView;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method
