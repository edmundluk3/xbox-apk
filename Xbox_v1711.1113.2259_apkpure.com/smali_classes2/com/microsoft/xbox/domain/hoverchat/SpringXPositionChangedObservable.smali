.class final Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;
.super Lio/reactivex/Observable;
.source "SpringXPositionChangedObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final spring:Lcom/facebook/rebound/Spring;


# direct methods
.method constructor <init>(Lcom/facebook/rebound/Spring;)V
    .locals 0
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 21
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;->spring:Lcom/facebook/rebound/Spring;

    .line 23
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Ljava/lang/Object;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;->spring:Lcom/facebook/rebound/Spring;

    invoke-direct {v0, p0, v1, p1}, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;-><init>(Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;Lcom/facebook/rebound/Spring;Lio/reactivex/Observer;)V

    .line 28
    .local v0, "listener":Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 29
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;->spring:Lcom/facebook/rebound/Spring;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->access$000(Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;)Lcom/facebook/rebound/SpringListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 30
    return-void
.end method
