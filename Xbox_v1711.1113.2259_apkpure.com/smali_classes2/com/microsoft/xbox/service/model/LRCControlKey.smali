.class public final enum Lcom/microsoft/xbox/service/model/LRCControlKey;
.super Ljava/lang/Enum;
.source "LRCControlKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/LRCControlKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum EMPTY:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_0:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_1:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_2:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_3:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_4:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_5:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_6:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_7:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_8:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_9:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_DISPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_DVDMENU:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_ESCAPE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_FASTFWD:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_INFO:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_MCE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_A:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_B:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_BACK:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_DPAD_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_DPAD_LEFT:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_DPAD_RIGHT:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_DPAD_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_START:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_X:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_XE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAD_Y:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PAUSE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_PLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_POUND:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_RECORD:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_REPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_RETURN:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_REWIND:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_SKIP:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_STAR:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_STOP:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_TITLE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_VOLUME_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_VOLUME_MUTE:Lcom/microsoft/xbox/service/model/LRCControlKey;

.field public static final enum VK_VOLUME_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x1b

    const/16 v6, 0x13

    const/16 v5, 0xd

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_DPAD_UP"

    const/16 v2, 0x5810

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_DPAD_DOWN"

    const/16 v2, 0x5811

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_DPAD_LEFT"

    const/4 v2, 0x2

    const/16 v3, 0x5812

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_LEFT:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_DPAD_RIGHT"

    const/4 v2, 0x3

    const/16 v3, 0x5813

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_RIGHT:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_START"

    const/4 v2, 0x4

    const/16 v3, 0x5814

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_START:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_BACK"

    const/4 v2, 0x5

    const/16 v3, 0x5815

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_BACK:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_A"

    const/4 v2, 0x6

    const/16 v3, 0x5800

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_A:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_B"

    const/4 v2, 0x7

    const/16 v3, 0x5801

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_B:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_X"

    const/16 v2, 0x8

    const/16 v3, 0x5802

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_X:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_Y"

    const/16 v2, 0x9

    const/16 v3, 0x5803

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_Y:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_INFO"

    const/16 v2, 0xa

    const/16 v3, 0x587c

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_INFO:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_TITLE"

    const/16 v2, 0xb

    const/16 v3, 0x5887

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_TITLE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAUSE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v6}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAUSE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PLAY"

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_STOP"

    const/16 v2, 0xe

    const/16 v3, 0x5876

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_STOP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_RECORD"

    const/16 v2, 0xf

    const/16 v3, 0x5877

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_RECORD:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_FASTFWD"

    const/16 v2, 0x10

    const/16 v3, 0x5878

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_FASTFWD:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_REWIND"

    const/16 v2, 0x11

    const/16 v3, 0x5879

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_REWIND:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_SKIP"

    const/16 v2, 0x12

    const/16 v3, 0x587a

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_SKIP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_REPLAY"

    const/16 v2, 0x587b

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_REPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_VOLUME_MUTE"

    const/16 v2, 0x14

    const/16 v3, 0xad

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_MUTE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_VOLUME_DOWN"

    const/16 v2, 0x15

    const/16 v3, 0xae

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_VOLUME_UP"

    const/16 v2, 0x16

    const/16 v3, 0xaf

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_DVDMENU"

    const/16 v2, 0x17

    const/16 v3, 0x5870

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_DVDMENU:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_DISPLAY"

    const/16 v2, 0x18

    const/16 v3, 0x5875

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_DISPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_0"

    const/16 v2, 0x19

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_0:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_1"

    const/16 v2, 0x1a

    const/16 v3, 0x31

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_1:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_2"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_2:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_3"

    const/16 v2, 0x1c

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_3:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_4"

    const/16 v2, 0x1d

    const/16 v3, 0x34

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_4:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_5"

    const/16 v2, 0x1e

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_5:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_6"

    const/16 v2, 0x1f

    const/16 v3, 0x36

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_6:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_7"

    const/16 v2, 0x20

    const/16 v3, 0x37

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_7:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_8"

    const/16 v2, 0x21

    const/16 v3, 0x38

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_8:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_9"

    const/16 v2, 0x22

    const/16 v3, 0x39

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_9:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_STAR"

    const/16 v2, 0x23

    const/16 v3, 0x5885

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_STAR:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_POUND"

    const/16 v2, 0x24

    const/16 v3, 0x5886

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_POUND:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_ESCAPE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2, v7}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_ESCAPE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_RETURN"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2, v5}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_RETURN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_MCE"

    const/16 v2, 0x27

    const/16 v3, 0x5888

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_MCE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "VK_PAD_XE"

    const/16 v2, 0x28

    const/16 v3, 0x5808

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_XE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    const-string v1, "EMPTY"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2, v4}, Lcom/microsoft/xbox/service/model/LRCControlKey;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->EMPTY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    .line 7
    const/16 v0, 0x2a

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/LRCControlKey;

    sget-object v1, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v1, v0, v8

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_LEFT:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_DPAD_RIGHT:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_START:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_BACK:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_A:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_B:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_X:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_Y:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_INFO:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_TITLE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAUSE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v1, v0, v5

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_STOP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_RECORD:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_FASTFWD:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_REWIND:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_SKIP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_REPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v1, v0, v6

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_MUTE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_DOWN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_VOLUME_UP:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_DVDMENU:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_DISPLAY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_0:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_1:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_2:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v1, v0, v7

    const/16 v1, 0x1c

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_3:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_4:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_5:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_6:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_7:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_8:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_9:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_STAR:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_POUND:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_ESCAPE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_RETURN:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_MCE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->VK_PAD_XE:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/microsoft/xbox/service/model/LRCControlKey;->EMPTY:Lcom/microsoft/xbox/service/model/LRCControlKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->$VALUES:[Lcom/microsoft/xbox/service/model/LRCControlKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lcom/microsoft/xbox/service/model/LRCControlKey;->value:I

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/LRCControlKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/LRCControlKey;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/LRCControlKey;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/microsoft/xbox/service/model/LRCControlKey;->$VALUES:[Lcom/microsoft/xbox/service/model/LRCControlKey;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/LRCControlKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/LRCControlKey;

    return-object v0
.end method


# virtual methods
.method public getKeyValue()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/microsoft/xbox/service/model/LRCControlKey;->value:I

    return v0
.end method
