.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$OneGuide;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OneGuide"
.end annotation


# static fields
.field public static final ButtonPressed:Ljava/lang/String; = "ButtonPressed"

.field public static final ChannelFavorited:Ljava/lang/String; = "ChannelFavorited"

.field public static final ChannelStreamed:Ljava/lang/String; = "ChannelStreamed"

.field public static final ChannelUnfavorited:Ljava/lang/String; = "ChannelUnfavorited"

.field public static final EpgLoad:Ljava/lang/String; = "EpgLoad"

.field public static final HeadendCount:Ljava/lang/String; = "Headend Count"

.field public static final NetworkTestStart:Ljava/lang/String; = "Network Test"

.field public static final ShowDetails:Ljava/lang/String; = "ShowDetails"

.field public static final ShowExpanded:Ljava/lang/String; = "ShowExpanded"

.field public static final ShowTuned:Ljava/lang/String; = "ShowTuned"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
