.class public Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ProviderPickerDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 191
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 202
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 181
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;Z)Z

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;)V

    .line 197
    return-void
.end method
