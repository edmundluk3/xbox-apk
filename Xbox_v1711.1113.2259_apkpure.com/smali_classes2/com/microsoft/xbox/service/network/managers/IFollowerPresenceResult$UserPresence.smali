.class public Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
.super Ljava/lang/Object;
.source "IFollowerPresenceResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserPresence"
.end annotation


# instance fields
.field private broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

.field private broadcastRecordSet:Z

.field public devices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;",
            ">;"
        }
    .end annotation
.end field

.field public lastSeen:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$LastSeenRecord;

.field public state:Ljava/lang/String;

.field public xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBroadcastRecord(J)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;
    .locals 7
    .param p1, "titleId"    # J

    .prologue
    .line 69
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->broadcastRecordSet:Z

    if-nez v2, :cond_3

    .line 70
    const-string v2, "Online"

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->devices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;

    .line 73
    .local v0, "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->isXboxOne()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 74
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;

    .line 75
    .local v1, "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    iget-wide v4, v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->id:J

    cmp-long v4, v4, p1

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->isRunningInFullOrFill()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;->broadcast:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    if-eqz v4, :cond_1

    .line 76
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;->broadcast:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    .line 83
    .end local v0    # "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    .end local v1    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->broadcastRecordSet:Z

    .line 85
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->broadcastRecord:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    return-object v2
.end method

.method public getBroadcastingViewerCount(J)I
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 89
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->getBroadcastRecord(J)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    move-result-object v0

    .line 90
    .local v0, "r":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;->viewers:I

    goto :goto_0
.end method

.method public getPresenceString()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 26
    const/4 v2, 0x0

    .line 27
    .local v2, "result":Ljava/lang/String;
    const/4 v1, 0x0

    .line 29
    .local v1, "foundXboxOnePresence":Z
    const-string v4, "Online"

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 30
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->devices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;

    .line 31
    .local v0, "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->isXboxOne()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 32
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;

    .line 33
    .local v3, "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->isRunningInFullOrFill()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 34
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    if-eqz v6, :cond_2

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;->broadcast:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    if-eqz v6, :cond_2

    .line 35
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07057f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->name:Ljava/lang/String;

    aput-object v9, v8, v10

    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->activity:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$ActivityRecord;->broadcast:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;->provider:Ljava/lang/String;

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 40
    :goto_2
    const/4 v1, 0x1

    goto :goto_1

    .line 38
    :cond_2
    iget-object v2, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->name:Ljava/lang/String;

    goto :goto_2

    .line 43
    .end local v3    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    :cond_3
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->isXbox360()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_0

    .line 44
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 46
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;

    .line 47
    .restart local v3    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->isDash()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v2, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->name:Ljava/lang/String;

    :goto_3
    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070580

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->name:Ljava/lang/String;

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 54
    .end local v0    # "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    .end local v3    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->lastSeen:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$LastSeenRecord;

    if-eqz v4, :cond_7

    .line 55
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070581

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->lastSeen:Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$LastSeenRecord;

    iget-object v7, v7, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$LastSeenRecord;->titleName:Ljava/lang/String;

    aput-object v7, v6, v10

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 61
    :cond_6
    :goto_4
    return-object v2

    .line 57
    :cond_7
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070582

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method

.method public getXboxOneNowPlayingDate()Ljava/util/Date;
    .locals 6

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    .local v1, "result":Ljava/util/Date;
    const-string v3, "Online"

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->devices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;

    .line 117
    .local v0, "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->isXboxOne()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;

    .line 119
    .local v2, "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->isRunningInFullOrFill()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 120
    iget-object v1, v2, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->lastModified:Ljava/util/Date;

    .line 121
    goto :goto_0

    .line 128
    .end local v0    # "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    .end local v2    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    :cond_2
    return-object v1
.end method

.method public getXboxOneNowPlayingTitleId()J
    .locals 7

    .prologue
    .line 94
    const-wide/16 v2, -0x1

    .line 96
    .local v2, "result":J
    const-string v4, "Online"

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 97
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->devices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;

    .line 98
    .local v0, "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->isXboxOne()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 99
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;->titles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;

    .line 100
    .local v1, "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->isRunningInFullOrFill()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 101
    iget-wide v2, v1, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;->id:J

    .line 102
    goto :goto_0

    .line 109
    .end local v0    # "device":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$DeviceRecord;
    .end local v1    # "title":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$TitleRecord;
    :cond_2
    return-wide v2
.end method

.method public isBroadcasting(J)Z
    .locals 1
    .param p1, "titleId"    # J

    .prologue
    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->getBroadcastRecord(J)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$BroadcastRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
