.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;
.super Ljava/lang/Object;
.source "PhoneContactVerifyPhoneDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 114
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 118
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 124
    return-void

    .line 123
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
