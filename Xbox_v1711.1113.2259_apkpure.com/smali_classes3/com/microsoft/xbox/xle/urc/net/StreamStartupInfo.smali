.class public Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;
.super Ljava/lang/Object;
.source "StreamStartupInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LiveTvInfo"


# instance fields
.field public blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

.field public currentChannelId:Ljava/lang/String;

.field public currentChannelType:Ljava/lang/String;

.field public source:Ljava/lang/String;

.field public streamingPort:Ljava/lang/String;

.field public userCanViewChannel:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;
    .locals 6
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 22
    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;-><init>()V

    .line 25
    .local v1, "info":Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;
    :try_start_0
    const-string v3, "params"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 26
    .local v2, "params":Lorg/json/JSONObject;
    const-string v3, "source"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->source:Ljava/lang/String;

    .line 27
    const-string v3, "currentChannelId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelId:Ljava/lang/String;

    .line 28
    const-string v3, "tunerChannelType"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->currentChannelType:Ljava/lang/String;

    .line 29
    const-string v3, "userCanViewChannel"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->userCanViewChannel:Ljava/lang/String;

    .line 30
    const-string v3, "streamingPort"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->streamingPort:Ljava/lang/String;

    .line 31
    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    move-result-object v3

    iput-object v3, v1, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;
    .end local v2    # "params":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 33
    .restart local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "LiveTvInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad json blob: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const/4 v1, 0x0

    goto :goto_0
.end method
