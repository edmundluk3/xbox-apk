.class public interface abstract Lcom/flipkart/chatheads/ui/ChatHeadContainer;
.super Ljava/lang/Object;
.source "ChatHeadContainer.java"


# virtual methods
.method public abstract addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public abstract bringToFront(Landroid/view/View;)V
.end method

.method public abstract createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;
.end method

.method public abstract getDisplayMetrics()Landroid/util/DisplayMetrics;
.end method

.method public abstract getViewX(Landroid/view/View;)I
.end method

.method public abstract getViewY(Landroid/view/View;)I
.end method

.method public abstract onArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V
.end method

.method public abstract onInitialized(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
.end method

.method public abstract removeView(Landroid/view/View;)V
.end method

.method public abstract requestLayout()V
.end method

.method public abstract setViewX(Landroid/view/View;I)V
.end method

.method public abstract setViewY(Landroid/view/View;I)V
.end method
