.class Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;
.super Ljava/lang/Object;
.source "ClubTypeSelectionStep.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 25
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 32
    const-string v0, "Clubs - Create Public"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackSelectClubType(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    .line 34
    return-void
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 36
    const-string v0, "Clubs - Create Private"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackSelectClubType(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    .line 38
    return-void
.end method

.method static synthetic lambda$onCreateContentView$2(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 40
    const-string v0, "Clubs - Create Hidden"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackSelectClubType(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    .line 42
    return-void
.end method


# virtual methods
.method public onCreateContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 30
    const v4, 0x7f030077

    invoke-virtual {p1, v4, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 31
    .local v1, "page":Landroid/view/View;
    const v4, 0x7f0e0318

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    const v4, 0x7f0e0319

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v4, 0x7f0e031a

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v4, 0x7f0e0316

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 45
    .local v3, "totalClubs":Landroid/widget/TextView;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070184

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 46
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getMaximumOpenAndClosedClubs()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getMaximumHiddenClubs()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    .line 45
    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const v4, 0x7f0e0317

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 49
    .local v2, "publicClubsRemaining":Landroid/widget/TextView;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070185

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 50
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getRemainingOpenAndClosedClubs()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 51
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getMaximumOpenAndClosedClubs()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    .line 52
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getRemainingPrivateClubs()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 49
    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "clubsPublicPrivateRemaining":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-object v1
.end method

.method public update()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
