.class Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubDiscoveryScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSocialTagsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->loadDataInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/lang/Void;
    .locals 2

    .prologue
    .line 310
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsSync(Z)V

    .line 312
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->onError()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 292
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z

    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V

    .line 324
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$LoadSocialTagsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z

    .line 318
    return-void
.end method
