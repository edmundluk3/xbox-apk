.class public Lcom/microsoft/xbox/smartglass/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field public final body:[B

.field public final headers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final isSuccess:Z

.field public final statusCode:I


# direct methods
.method constructor <init>(IZ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;[B)V
    .locals 6
    .param p1, "statusCode"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "headers"    # [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .param p4, "body"    # [B

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->statusCode:I

    .line 33
    iput-boolean p2, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->isSuccess:Z

    .line 35
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->headers:Ljava/util/HashMap;

    .line 36
    array-length v2, p3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p3, v1

    .line 37
    .local v0, "entry":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->headers:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->name:Ljava/lang/String;

    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "entry":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    :cond_0
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->body:[B

    .line 41
    return-void
.end method


# virtual methods
.method public getBodyAsString()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/HttpResponse;->body:[B

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method
