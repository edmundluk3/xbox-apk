.class public final Lcom/microsoft/xbox/service/rta/RtaDataMapper;
.super Ljava/lang/Object;
.source "RtaDataMapper.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/DataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/data/repository/DataMapper",
        "<",
        "Ljava/lang/String;",
        "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/rta/RtaDataMapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private parseEventResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 4
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 154
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 155
    array-length v1, p1

    const/4 v3, 0x3

    if-lt v1, v3, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 157
    aget-object v1, p1, v2

    instance-of v1, v1, Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 158
    aget-object v1, p1, v2

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v0

    .line 160
    .local v0, "subscriptionId":I
    new-instance v1, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaEventResponse;

    .line 161
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    .line 162
    invoke-static {v3}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaEventResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;)V

    .line 160
    invoke-static {v1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    .line 166
    .end local v0    # "subscriptionId":I
    :goto_1
    return-object v1

    .line 155
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 165
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v2, "Invalid RTA EVENT"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v1

    goto :goto_1
.end method

.method private parseSubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 11
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v8, 0x3

    .line 84
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 85
    array-length v0, p1

    if-lt v0, v8, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 87
    aget-object v0, p1, v2

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_2

    aget-object v0, p1, v9

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 88
    aget-object v0, p1, v2

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v3

    .line 89
    .local v3, "sequenceNumber":I
    aget-object v0, p1, v9

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v6

    .line 90
    .local v6, "errorCode":I
    sget-object v4, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;->Unknown:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    .line 91
    .local v4, "errorType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;
    const/4 v5, 0x0

    .line 92
    .local v5, "errorMessage":Ljava/lang/String;
    const/4 v1, 0x0

    .line 93
    .local v1, "subscriptionId":Ljava/lang/Integer;
    const/4 v7, 0x0

    .line 95
    .local v7, "payload":Ljava/lang/Object;
    packed-switch v6, :pswitch_data_0

    .line 117
    .end local v7    # "payload":Ljava/lang/Object;
    :goto_1
    new-instance v0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;

    .line 119
    invoke-static {v7}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaSubscriptionResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;ILcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;Ljava/lang/String;)V

    .line 117
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 126
    .end local v1    # "subscriptionId":Ljava/lang/Integer;
    .end local v3    # "sequenceNumber":I
    .end local v4    # "errorType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v6    # "errorCode":I
    :goto_2
    return-object v0

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    .restart local v1    # "subscriptionId":Ljava/lang/Integer;
    .restart local v3    # "sequenceNumber":I
    .restart local v4    # "errorType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;
    .restart local v5    # "errorMessage":Ljava/lang/String;
    .restart local v6    # "errorCode":I
    .restart local v7    # "payload":Ljava/lang/Object;
    :pswitch_0
    aget-object v0, p1, v8

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_1

    array-length v0, p1

    if-le v0, v10, :cond_1

    .line 98
    sget-object v4, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;->Success:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    .line 99
    aget-object v0, p1, v8

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 100
    aget-object v7, p1, v10

    goto :goto_1

    .line 102
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v2, "Malformed RTA SUBSCRIBE message!"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_2

    .line 108
    :pswitch_1
    sget-object v4, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;->MaxSubscriptionLimit:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    .line 109
    aget-object v5, p1, v8

    .end local v5    # "errorMessage":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 110
    .restart local v5    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 112
    :pswitch_2
    sget-object v4, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;->NoResourceData:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;

    .line 113
    aget-object v5, p1, v8

    .end local v5    # "errorMessage":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .restart local v5    # "errorMessage":Ljava/lang/String;
    goto :goto_1

    .line 126
    .end local v1    # "subscriptionId":Ljava/lang/Integer;
    .end local v3    # "sequenceNumber":I
    .end local v4    # "errorType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaErrorType;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v6    # "errorCode":I
    .end local v7    # "payload":Ljava/lang/Object;
    :cond_2
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_2

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private parseUnsubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 7
    .param p1, "rtaResponse"    # [Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x3L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v3, 0x1

    .line 134
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 135
    array-length v2, p1

    const/4 v4, 0x3

    if-lt v2, v4, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 137
    aget-object v2, p1, v3

    instance-of v2, v2, Ljava/lang/Double;

    if-eqz v2, :cond_1

    aget-object v2, p1, v5

    instance-of v2, v2, Ljava/lang/Double;

    if-eqz v2, :cond_1

    .line 138
    aget-object v2, p1, v3

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v1

    .line 139
    .local v1, "sequenceNumber":I
    aget-object v2, p1, v5

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v0

    .line 141
    .local v0, "errorCode":I
    new-instance v2, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;

    invoke-direct {v2, v6, v6, v1, v0}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaUnsubscribeResponse;-><init>(Ljava/lang/Integer;Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;II)V

    invoke-static {v2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    .line 148
    .end local v0    # "errorCode":I
    .end local v1    # "sequenceNumber":I
    :goto_1
    return-object v2

    .line 135
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 147
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v3, "Failed to parse RTA UNSUBSCRIBE"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public apply(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 7
    .param p1, "responseText"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 36
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got RTA message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-class v2, [Ljava/lang/Object;

    invoke-static {p1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 40
    .local v0, "rtaResponse":[Ljava/lang/Object;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    aget-object v2, v0, v6

    instance-of v2, v2, Ljava/lang/Double;

    if-eqz v2, :cond_3

    .line 41
    aget-object v2, v0, v6

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->valueOf(I)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    move-result-object v1

    .line 43
    .local v1, "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper$1;->$SwitchMap$com$microsoft$xbox$service$rta$RtaDataTypes$RtaResponseType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 69
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received unknown RTA message type. Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    .line 76
    .end local v1    # "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :goto_0
    return-object v2

    .line 45
    .restart local v1    # "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :pswitch_0
    array-length v2, v0

    if-le v2, v5, :cond_0

    .line 46
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->parseSubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 48
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RTA SUBSCRIBE payload does not have the required number of fields! Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .end local v1    # "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :goto_1
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 52
    .restart local v1    # "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :pswitch_1
    array-length v2, v0

    if-lt v2, v5, :cond_1

    .line 53
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->parseUnsubscribeResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 55
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RTA UNSUBSCRIBE payload does not have the required number of fields! Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 59
    :pswitch_2
    array-length v2, v0

    if-ne v2, v5, :cond_2

    .line 60
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->parseEventResponse([Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 62
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RTA EVENT payload does not have the required number of fields! Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 66
    :pswitch_3
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    const-string v3, "RTA UNSUBSCRIBE ALL is not currently supported."

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v2

    goto :goto_0

    .line 73
    .end local v1    # "rtaResposeTypeCode":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received invalid RTA response payload! Message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
