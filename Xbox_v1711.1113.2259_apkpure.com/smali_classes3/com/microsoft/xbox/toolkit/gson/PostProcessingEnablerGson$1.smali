.class Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$1;
.super Lcom/google/gson/TypeAdapter;
.source "PostProcessingEnablerGson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;->create(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

.field final synthetic val$delegate:Lcom/google/gson/TypeAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;Lcom/google/gson/TypeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$1;->this$0:Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$1;->val$delegate:Lcom/google/gson/TypeAdapter;

    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$1;->val$delegate:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    .line 35
    .local v0, "obj":Ljava/lang/Object;, "TT;"
    instance-of v1, v0, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 36
    check-cast v1, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;->postProcess()V

    .line 38
    :cond_0
    return-object v0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 1
    .param p1, "out"    # Lcom/google/gson/stream/JsonWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "value":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$1;->val$delegate:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 31
    return-void
.end method
