.class public Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
.super Ljava/lang/Object;
.source "AppChannelsModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Channel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;,
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;,
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;
    }
.end annotation


# static fields
.field public static final ATTR_FAVORITES:Ljava/lang/String; = "favorite"

.field public static final ATTR_LOADING:Ljava/lang/String; = "loading"


# instance fields
.field private _taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

.field private _taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

.field private isFavorite:Z

.field private listenersInstalled:Z

.field private final loadingItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

.field private mError:Ljava/lang/Throwable;

.field private mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

.field public final propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    .line 424
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    .line 425
    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    .line 427
    iput-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    .line 429
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadingItems:Ljava/util/HashMap;

    .line 770
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->listenersInstalled:Z

    .line 432
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    .line 433
    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    return-object p1
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p1, "x1"    # Z

    .prologue
    .line 414
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    return p1
.end method

.method private startListeners()V
    .locals 1

    .prologue
    .line 773
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->listenersInstalled:Z

    if-eqz v0, :cond_0

    .line 779
    :goto_0
    return-void

    .line 776
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 777
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V

    .line 778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->listenersInstalled:Z

    goto :goto_0
.end method

.method private stopListeners()V
    .locals 1

    .prologue
    .line 782
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->listenersInstalled:Z

    if-nez v0, :cond_1

    .line 794
    :cond_0
    :goto_0
    return-void

    .line 785
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    if-nez v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadingItems:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 792
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V

    .line 793
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->listenersInstalled:Z

    goto :goto_0
.end method


# virtual methods
.method public copyFavorite(Z)V
    .locals 2
    .param p1, "favorite"    # Z

    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    if-eq v0, p1, :cond_0

    .line 695
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    .line 696
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "favorite"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 698
    :cond_0
    return-void
.end method

.method public getChannelId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getColors()[I
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->primaryColor:I

    if-nez v0, :cond_0

    .line 466
    const/4 v0, 0x0

    .line 468
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget v2, v2, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->primaryColor:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget v2, v2, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->secondaryColor:I

    aput v2, v0, v1

    goto :goto_0
.end method

.method public getError()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mError:Ljava/lang/Throwable;

    return-object v0
.end method

.method public getImageAspectRatio()F
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 485
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    if-nez v1, :cond_0

    .line 499
    :goto_0
    :pswitch_0
    return v0

    .line 488
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$2;->$SwitchMap$com$microsoft$xbox$xle$urc$net$AppChannelData$ImageOrientation:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->imageOrientation:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ImageOrientation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 490
    :pswitch_1
    const v0, 0x3f3a8c15    # 0.7287f

    goto :goto_0

    .line 494
    :pswitch_2
    const v0, 0x3fe38e39

    goto :goto_0

    .line 496
    :pswitch_3
    const v0, 0x3faaaaab

    goto :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getItem(I)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 508
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    if-nez v1, :cond_1

    .line 514
    :cond_0
    :goto_0
    return-object v0

    .line 511
    :cond_1
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getItemCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 514
    new-instance v1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->shows:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;->shows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getProviderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ProviderInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 460
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    return v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTheSameChannel(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .prologue
    .line 436
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mInfo:Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;->isTheSameChannel(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadItemExtra(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Z
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    const/4 v3, 0x1

    .line 541
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 555
    :cond_0
    :goto_0
    return v3

    .line 545
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadingItems:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 549
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadingItems:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->startListeners()V

    .line 553
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Ljava/lang/String;)V

    .line 554
    .local v0, "task":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemExtraAsyncTask;->execute()V

    goto :goto_0
.end method

.method public onAppChannelDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData;
    .param p2, "error"    # Ljava/lang/Throwable;
    .param p3, "reqProviderID"    # Ljava/lang/String;
    .param p4, "reqChannelID"    # Ljava/lang/String;

    .prologue
    .line 560
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 580
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mData:Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    .line 573
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->mError:Ljava/lang/Throwable;

    .line 575
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    .line 577
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->stopListeners()V

    .line 579
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "loading"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAppChannelLineupsReceived(Ljava/util/List;Ljava/lang/Throwable;)V
    .locals 0
    .param p2, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 653
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    return-void
.end method

.method public onAppChannelProgramDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
    .param p2, "error"    # Ljava/lang/Throwable;
    .param p3, "reqProviderID"    # Ljava/lang/String;
    .param p4, "reqShowID"    # Ljava/lang/String;

    .prologue
    .line 608
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 616
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadingItems:Ljava/util/HashMap;

    invoke-virtual {v1, p4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .line 617
    .local v0, "item":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->stopListeners()V

    .line 619
    if-eqz v0, :cond_0

    .line 620
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->setExtra(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;)V

    goto :goto_0
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 667
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 657
    invoke-static {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->access$300(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->onAppChannelDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :cond_0
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 672
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 677
    return-void
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->startListeners()V

    .line 520
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    .line 521
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskGetItems:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$GetChannelItemsAsyncTask;->execute()V

    .line 523
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "loading"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public toggleFavorite()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 680
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    if-nez v0, :cond_3

    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    if-nez v0, :cond_2

    .line 686
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite:Z

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelName()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    .line 687
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->_taskToggleFavorite:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->load(Z)V

    .line 690
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "favorite"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    goto :goto_0

    .line 683
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public tuneToShow(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Z
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    const/4 v0, 0x0

    .line 527
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 537
    :cond_0
    :goto_0
    return v0

    .line 531
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 535
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "ShowTuned"

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideAppPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getTuneToUrl()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 537
    const/4 v0, 0x1

    goto :goto_0
.end method
