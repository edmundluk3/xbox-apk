.class public final enum Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
.super Ljava/lang/Enum;
.source "HomeScreenPreference.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Clubs:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Friends:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Messages:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Trending:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

.field public static final enum Tutorial:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;


# instance fields
.field private telemetryActionName:Ljava/lang/String;

.field private telemetrySettingName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "ActivityFeed"

    const-string v2, "Settings - Default Homescreen ActivityFeed"

    const-string v3, "ActivityFeed"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Friends"

    const-string v2, "Settings - Default Homescreen Friends"

    const-string v3, "Friends"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Friends:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Achievements"

    const-string v2, "Settings - Default Homescreen Achievements"

    const-string v3, "Achievements"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Trending"

    const-string v2, "Settings - Default Homescreen Trending"

    const-string v3, "Trending"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Trending:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Clubs"

    const-string v2, "Settings - Default Homescreen Clubs"

    const-string v3, "Clubs"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Clubs:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Messages"

    const/4 v2, 0x5

    const-string v3, "Settings - Default Homescreen Messages"

    const-string v4, "Messages"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Messages:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    const-string v1, "Tutorial"

    const/4 v2, 0x6

    const-string v3, "Settings - Default Homescreen Tutorial"

    const-string v4, "Tutorial"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Tutorial:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    .line 12
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ActivityFeed:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Friends:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Trending:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Clubs:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Messages:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Tutorial:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->$VALUES:[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3, "actionName"    # Ljava/lang/String;
    .param p4, "settingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetryActionName:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetrySettingName:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetryActionName:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetrySettingName:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 6
    .param p0, "value"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 30
    const-wide/16 v0, 0x0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->values()[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    int-to-long v4, p0

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->values()[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->$VALUES:[Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    return-object v0
.end method


# virtual methods
.method public getTelemetryActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetryActionName:Ljava/lang/String;

    return-object v0
.end method

.method public getTelemetrySettingName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->telemetrySettingName:Ljava/lang/String;

    return-object v0
.end method
