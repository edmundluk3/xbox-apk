.class Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "FriendsWhoPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFriendsWhoPlayAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$1;

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V

    return-void
.end method

.method private processData()V
    .locals 8

    .prologue
    .line 205
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFavorites()Ljava/util/ArrayList;

    move-result-object v1

    .line 206
    .local v1, "favoritesData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 207
    .local v0, "allFavorites":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 208
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 209
    .local v2, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    .end local v2    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getResult(J)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 214
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    .line 216
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 217
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 218
    .restart local v2    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v5, v2, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, v2, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    goto :goto_1

    .line 222
    .end local v2    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_1
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;)V

    .line 252
    .local v3, "gamerComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 254
    .end local v3    # "gamerComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_2
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 144
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 147
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->shouldRefresh(J)Z

    move-result v0

    .line 149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 177
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 179
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 183
    const-string v2, "DGame"

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 185
    .local v0, "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 187
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;J)J

    .line 194
    .end local v0    # "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 195
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 196
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v1, v2, :cond_1

    .line 197
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->processData()V

    .line 201
    :cond_1
    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 155
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->onLoadFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 157
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->onLoadFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 168
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 140
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel$LoadFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->updateAdapter()V

    .line 163
    return-void
.end method
