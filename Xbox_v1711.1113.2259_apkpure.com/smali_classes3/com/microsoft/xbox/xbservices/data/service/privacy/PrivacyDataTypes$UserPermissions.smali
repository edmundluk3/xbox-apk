.class public abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
.super Ljava/lang/Object;
.source "PrivacyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UserPermissions"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserPermissions$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserPermissions$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract permissions()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;",
            ">;"
        }
    .end annotation
.end method

.method public abstract user()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
