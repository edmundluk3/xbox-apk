.class public Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "GameDetailsProgressPhoneScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ILikeControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final maxItems:I = 0x3


# instance fields
.field private gameclipSublist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation
.end field

.field private gameclips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation
.end field

.field private isLoading:Z

.field private likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

.field private loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

.field private loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

.field private mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private updateLikeControl:Z

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 46
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 62
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameDetailsProgressPhoneScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/service/model/TitleModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/TitleModel;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->onLoadProgressCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->isLoading:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->shouldShowGameClips()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->onLikeGameClipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateLikeControl:Z

    return p1
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object p1
.end method

.method private loadSocialInfo(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 425
    .local p1, "gameClipData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->cancel()V

    .line 428
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

    .line 429
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadSocialTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadSocialAsyncTask;->load(Z)V

    .line 430
    return-void
.end method

.method private onLikeGameClipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "newLikeState"    # Z
    .param p3, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 412
    const-string v0, "GameProgressGameClips ViewModel"

    const-string v1, "onLikeGameClipsCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 414
    if-eqz p2, :cond_1

    .line 415
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Like XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 422
    return-void

    .line 417
    :cond_1
    const-string v0, "LikeClickAsyncTask"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Unlike XUID:%s GameClipID: %s Failed"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadProgressCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 225
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadSocialInfo(Ljava/util/ArrayList;)V

    .line 234
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 235
    return-void

    .line 189
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->hasValidData()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 191
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 192
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    .line 199
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v4, :cond_5

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 210
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 211
    :cond_4
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->isLoading:Z

    if-eqz v0, :cond_6

    .line 212
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 204
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclips:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 214
    :cond_6
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 217
    :cond_7
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 228
    :cond_8
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->isLoading:Z

    .line 230
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_1

    .line 185
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private shouldShowGameClips()Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsBundle:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->selectedItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->BundlePrimaryItemId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public deleteActivityFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "feedItemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 171
    const-string v0, "Cannot delete item from here"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public getGameClips()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->isLoading:Z

    return v0
.end method

.method public likeClick(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 5
    .param p1, "uncastItem"    # Ljava/lang/Object;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 345
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 346
    .local v0, "gameClip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v2, :cond_1

    .line 349
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v4, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 350
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v2, :cond_3

    .line 351
    const/4 v1, 0x1

    .line 352
    .local v1, "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 357
    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateLikeControl:Z

    .line 358
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateAdapter()V

    .line 360
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

    if-eqz v2, :cond_0

    .line 361
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->cancel()V

    .line 364
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;ZLjava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

    .line 365
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->likeClickTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->load(Z)V

    .line 367
    .end local v1    # "newLikeState":Z
    :cond_1
    return-void

    .line 349
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 354
    :cond_3
    const/4 v1, 0x0

    .line 355
    .restart local v1    # "newLikeState":Z
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto :goto_1
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->cancel()V

    .line 83
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->load(Z)V

    .line 85
    return-void
.end method

.method public navigateToActionsScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 1
    .param p1, "itemRoot"    # Ljava/lang/String;
    .param p2, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 135
    return-void
.end method

.method public navigateToGameClips()V
    .locals 5

    .prologue
    .line 119
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 120
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-string v3, "Navigate from Game Detail Clips page to Game hub Clips page should have a mediaModel that contains a valid media item"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 122
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfileCapturesScreen;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 125
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/activity/GameDetailsProgressPhoneScreen;

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {v2, v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopTillScreenThenPush(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_1
    return-void

    .line 120
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "GameDetailsProgressPhoneScreenViewModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to navigate to pivot \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    .line 128
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' / pane \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfileCapturesScreen;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 5
    .param p1, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 108
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->getGameClipUriForNavigation(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    move-result-object v0

    .line 109
    .local v0, "clipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    if-eqz v0, :cond_0

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedDataSource(Ljava/lang/String;)V

    .line 111
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 112
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setGameClip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .line 113
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Play Game DVR"

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->titleName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 116
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public navigateToPostCommentScreen(Ljava/lang/String;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canCommentOnItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {p0, p1, v6}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPostCommentScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 151
    :goto_0
    return-void

    .line 142
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User has no communication privileges"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 144
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 146
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704ad

    .line 147
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 145
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 148
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 143
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public navigateToShareScreen(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 7
    .param p1, "itemLocator"    # Ljava/lang/String;
    .param p2, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 155
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canShareItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 166
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 159
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 161
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704ad

    .line 162
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 160
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 163
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 158
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameDetailsProgressPhoneScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 70
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LoadProgressTask;->cancel()V

    .line 97
    :cond_0
    return-void
.end method

.method public resetLikeControlUpdate()V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateLikeControl:Z

    .line 341
    return-void
.end method

.method public shouldUpdateLikeControl()Z
    .locals 1

    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->updateLikeControl:Z

    return v0
.end method

.method public showNoContentTextForGameClips()Z
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->shouldShowGameClips()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->gameclipSublist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
