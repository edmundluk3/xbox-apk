.class public final Lcom/microsoft/xbox/service/model/recents/RecentsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "RecentsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;,
        Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/recents/Recent;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final RECENT_RUNNER_TIMEOUT_IN_MS:I = 0x7530

.field private static final TAG:Ljava/lang/String;

.field private static final esServiceManager:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

.field private static instance:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

.field private static final storeService:Lcom/microsoft/xbox/service/store/IStoreService;


# instance fields
.field private recents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->instance:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->esServiceManager:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 47
    const-wide/32 v0, 0x927c0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->lifetime:J

    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;-><init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/service/model/recents/RecentsModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->runner:Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;

    .line 49
    return-void
.end method

.method static synthetic access$100()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->esServiceManager:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/recents/RecentsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->onLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$400()Lcom/microsoft/xbox/service/store/IStoreService;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->instance:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    return-object v0
.end method

.method private onLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->recents:Ljava/util/ArrayList;

    .line 77
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 78
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 79
    return-void
.end method

.method public static reset()V
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->instance:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    .line 62
    return-void
.end method


# virtual methods
.method public getRecents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->recents:Ljava/util/ArrayList;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceLoad"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->runner:Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadAsync(Z)V
    .locals 3
    .param p1, "forceLoad"    # Z

    .prologue
    .line 65
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->RecentsData:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->runner:Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;Ljava/util/Date;)V

    .line 66
    return-void
.end method
