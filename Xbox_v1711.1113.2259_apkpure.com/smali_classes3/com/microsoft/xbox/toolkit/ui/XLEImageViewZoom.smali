.class public Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
.source "XLEImageViewZoom.java"


# static fields
.field public static final ACTION_DRAG:I = 0x1

.field public static final ACTION_NONE:I


# instance fields
.field private endPoint:Landroid/graphics/PointF;

.field private m:[F

.field private matrix:Landroid/graphics/Matrix;

.field private maxScale:F

.field private minScale:F

.field private mode:I

.field oldMeasuredHeight:I

.field oldMeasuredWidth:I

.field protected origHeight:F

.field protected origWidth:F

.field saveScale:F

.field private startPoint:Landroid/graphics/PointF;

.field viewHeight:I

.field viewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    .line 21
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    .line 22
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->startPoint:Landroid/graphics/PointF;

    .line 23
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->minScale:F

    .line 24
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    .line 28
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->init(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    .line 21
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    .line 22
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->startPoint:Landroid/graphics/PointF;

    .line 23
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->minScale:F

    .line 24
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    .line 28
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->init(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    .line 44
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->m:[F

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 48
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 49
    return-void
.end method


# virtual methods
.method fixTrans()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 135
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->m:[F

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 136
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->m:[F

    const/4 v5, 0x2

    aget v2, v4, v5

    .line 137
    .local v2, "transX":F
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->m:[F

    const/4 v5, 0x5

    aget v3, v4, v5

    .line 139
    .local v3, "transY":F
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origWidth:F

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v5, v6

    invoke-virtual {p0, v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getFixTrans(FFF)F

    move-result v0

    .line 140
    .local v0, "fixTransX":F
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v4, v4

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origHeight:F

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v5, v6

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getFixTrans(FFF)F

    move-result v1

    .line 142
    .local v1, "fixTransY":F
    cmpl-float v4, v0, v7

    if-nez v4, :cond_0

    cmpl-float v4, v1, v7

    if-eqz v4, :cond_1

    .line 143
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 144
    :cond_1
    return-void
.end method

.method getFixDragTrans(FFF)F
    .locals 1
    .param p1, "delta"    # F
    .param p2, "viewSize"    # F
    .param p3, "contentSize"    # F

    .prologue
    .line 165
    cmpg-float v0, p3, p2

    if-gtz v0, :cond_0

    .line 166
    const/4 p1, 0x0

    .line 168
    .end local p1    # "delta":F
    :cond_0
    return p1
.end method

.method getFixTrans(FFF)F
    .locals 3
    .param p1, "trans"    # F
    .param p2, "viewSize"    # F
    .param p3, "contentSize"    # F

    .prologue
    .line 149
    cmpg-float v2, p3, p2

    if-gtz v2, :cond_0

    .line 150
    const/4 v1, 0x0

    .line 151
    .local v1, "minTrans":F
    sub-float v0, p2, p3

    .line 157
    .local v0, "maxTrans":F
    :goto_0
    cmpg-float v2, p1, v1

    if-gez v2, :cond_1

    .line 158
    neg-float v2, p1

    add-float/2addr v2, v1

    .line 161
    :goto_1
    return v2

    .line 153
    .end local v0    # "maxTrans":F
    .end local v1    # "minTrans":F
    :cond_0
    sub-float v1, p2, p3

    .line 154
    .restart local v1    # "minTrans":F
    const/4 v0, 0x0

    .restart local v0    # "maxTrans":F
    goto :goto_0

    .line 159
    :cond_1
    cmpl-float v2, p1, v0

    if-lez v2, :cond_2

    .line 160
    neg-float v2, p1

    add-float/2addr v2, v0

    goto :goto_1

    .line 161
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getScaled()Z
    .locals 2

    .prologue
    .line 131
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleScale(FFF)Z
    .locals 4
    .param p1, "factor"    # F
    .param p2, "focusX"    # F
    .param p3, "focusY"    # F

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 102
    .local v0, "origScale":F
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v1, p1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 103
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 104
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 105
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    div-float p1, v1, v0

    .line 111
    :cond_0
    :goto_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origWidth:F

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-lez v1, :cond_1

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origHeight:F

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, p1, p1, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 116
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->fixTrans()V

    .line 118
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->invalidate()V

    .line 123
    :cond_2
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    return v1

    .line 106
    :cond_3
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->minScale:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 107
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->minScale:F

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 108
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->minScale:F

    div-float p1, v1, v0

    goto :goto_0

    .line 114
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_1

    .line 123
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public handleTouch(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 52
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    .line 53
    .local v5, "oldScale":F
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    cmpl-float v9, v9, v5

    if-eqz v9, :cond_2

    move v6, v7

    .line 54
    .local v6, "processed":Z
    :goto_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-direct {v0, v9, v10}, Landroid/graphics/PointF;-><init>(FF)V

    .line 57
    .local v0, "currPoint":Landroid/graphics/PointF;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 91
    :cond_0
    :goto_1
    :pswitch_0
    if-eqz v6, :cond_1

    .line 92
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 93
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->invalidate()V

    .line 96
    :cond_1
    return v6

    .end local v0    # "currPoint":Landroid/graphics/PointF;
    .end local v6    # "processed":Z
    :cond_2
    move v6, v8

    .line 53
    goto :goto_0

    .line 59
    .restart local v0    # "currPoint":Landroid/graphics/PointF;
    .restart local v6    # "processed":Z
    :pswitch_1
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    cmpl-float v8, v8, v11

    if-lez v8, :cond_0

    .line 60
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    invoke-virtual {v8, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 61
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->startPoint:Landroid/graphics/PointF;

    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    invoke-virtual {v8, v9}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 62
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    .line 63
    const/4 v6, 0x1

    goto :goto_1

    .line 69
    :pswitch_2
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    if-ne v8, v7, :cond_0

    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    cmpl-float v7, v7, v11

    if-lez v7, :cond_0

    .line 70
    iget v7, v0, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float v1, v7, v8

    .line 71
    .local v1, "deltaX":F
    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float v2, v7, v8

    .line 72
    .local v2, "deltaY":F
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v7, v7

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origWidth:F

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v8, v9

    invoke-virtual {p0, v1, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getFixDragTrans(FFF)F

    move-result v3

    .line 73
    .local v3, "fixTransX":F
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origHeight:F

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    mul-float/2addr v8, v9

    invoke-virtual {p0, v2, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getFixDragTrans(FFF)F

    move-result v4

    .line 74
    .local v4, "fixTransY":F
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->fixTrans()V

    .line 76
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->endPoint:Landroid/graphics/PointF;

    iget v8, v0, Landroid/graphics/PointF;->x:F

    iget v9, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->set(FF)V

    .line 77
    const/4 v6, 0x1

    .line 78
    goto :goto_1

    .line 82
    .end local v1    # "deltaX":F
    .end local v2    # "deltaY":F
    .end local v3    # "fixTransX":F
    .end local v4    # "fixTransY":F
    :pswitch_3
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    goto :goto_1

    .line 87
    :pswitch_4
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->mode:I

    goto :goto_1

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    .line 173
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->onMeasure(II)V

    .line 174
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    .line 175
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    .line 180
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->oldMeasuredHeight:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    if-ne v8, v9, :cond_0

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->oldMeasuredHeight:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    if-eq v8, v9, :cond_1

    :cond_0
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    if-eqz v8, :cond_1

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    if-nez v8, :cond_2

    .line 216
    :cond_1
    :goto_0
    return-void

    .line 182
    :cond_2
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->oldMeasuredHeight:I

    .line 183
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->oldMeasuredWidth:I

    .line 185
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->saveScale:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_3

    .line 189
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 190
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    if-eqz v8, :cond_1

    .line 192
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 193
    .local v1, "bmWidth":I
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 195
    .local v0, "bmHeight":I
    const-string v8, "bmSize"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "bmWidth: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " bmHeight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v8, v8

    int-to-float v9, v1

    div-float v6, v8, v9

    .line 198
    .local v6, "scaleX":F
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v8, v8

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 199
    .local v7, "scaleY":F
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 200
    .local v5, "scale":F
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 203
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v8, v8

    int-to-float v9, v0

    mul-float/2addr v9, v5

    sub-float v4, v8, v9

    .line 204
    .local v4, "redundantYSpace":F
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v8, v8

    int-to-float v9, v1

    mul-float/2addr v9, v5

    sub-float v3, v8, v9

    .line 205
    .local v3, "redundantXSpace":F
    div-float/2addr v4, v11

    .line 206
    div-float/2addr v3, v11

    .line 208
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 210
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewWidth:I

    int-to-float v8, v8

    mul-float v9, v11, v3

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origWidth:F

    .line 211
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->viewHeight:I

    int-to-float v8, v8

    mul-float v9, v11, v4

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->origHeight:F

    .line 212
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 215
    .end local v0    # "bmHeight":I
    .end local v1    # "bmWidth":I
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "redundantXSpace":F
    .end local v4    # "redundantYSpace":F
    .end local v5    # "scale":F
    .end local v6    # "scaleX":F
    .end local v7    # "scaleY":F
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->fixTrans()V

    goto/16 :goto_0
.end method

.method public setMaxZoom(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 127
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewZoom;->maxScale:F

    .line 128
    return-void
.end method
