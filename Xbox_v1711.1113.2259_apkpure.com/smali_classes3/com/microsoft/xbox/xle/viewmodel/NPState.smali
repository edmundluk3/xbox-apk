.class public Lcom/microsoft/xbox/xle/viewmodel/NPState;
.super Ljava/lang/Object;
.source "NPState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;,
        Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;,
        Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    }
.end annotation


# static fields
.field private static final MAX_RECENTS:I = 0x4


# instance fields
.field private final currentProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

.field private volatile transient hashCode:I

.field private final liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

.field private final recents:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Ljava/util/List;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)V
    .locals 0
    .param p1, "liBat"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "currentProgressItem"    # Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "recents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .line 45
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->currentProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 47
    return-void
.end method

.method private static appendRecents(Ljava/util/List;Ljava/util/LinkedList;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "recents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    .local p1, "viewModelRecents":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    .local p2, "addedItems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz p1, :cond_1

    .line 240
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 241
    .local v0, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 242
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .line 243
    .local v1, "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    .end local v0    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    .end local v1    # "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    :cond_1
    return-void
.end method

.method public static computeState(Lcom/microsoft/xbox/xle/viewmodel/NPState;Ljava/util/LinkedList;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .locals 19
    .param p0, "oldState"    # Lcom/microsoft/xbox/xle/viewmodel/NPState;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/LinkedList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "currentProgressItem"    # Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;",
            ")",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState;"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "viewModelRecents":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 104
    const/4 v4, 0x0

    .line 105
    .local v4, "npiBat":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    const/4 v8, 0x0

    .line 106
    .local v8, "npiFull":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    const/4 v6, 0x0

    .line 107
    .local v6, "npiFill":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    const/4 v9, 0x0

    .line 108
    .local v9, "npiSnap":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v12, "recents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v13

    .line 111
    .local v13, "sm":Lcom/microsoft/xbox/service/model/SessionModel;
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    .line 112
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v3

    .line 113
    .local v3, "npgm":Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v15

    array-length v0, v15

    move/from16 v16, v0

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_1

    aget-object v10, v15, v14

    .line 114
    .local v10, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    sget-object v17, Lcom/microsoft/xbox/xle/viewmodel/NPState$1;->$SwitchMap$com$microsoft$xbox$smartglass$ActiveTitleLocation:[I

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    .line 113
    :cond_0
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 116
    :pswitch_0
    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isPlaying(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 117
    new-instance v8, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;

    .end local v8    # "npiFull":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-direct {v8, v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .restart local v8    # "npiFull":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    goto :goto_1

    .line 121
    :pswitch_1
    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isPlaying(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 122
    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;

    .end local v6    # "npiFill":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-direct {v6, v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .restart local v6    # "npiFill":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    goto :goto_1

    .line 126
    :pswitch_2
    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isPlaying(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 127
    new-instance v9, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;

    .end local v9    # "npiSnap":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-direct {v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;-><init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .restart local v9    # "npiSnap":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    goto :goto_1

    .line 136
    .end local v3    # "npgm":Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;
    .end local v10    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 138
    .local v2, "addedItems":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const/4 v5, 0x0

    .local v5, "npiBatOld":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    const/4 v7, 0x0

    .line 139
    .local v7, "npiFirstOld":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz p0, :cond_2

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v5

    .line 141
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hasRecents()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 142
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecent(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v7

    .line 146
    :cond_2
    if-eqz v8, :cond_4

    .line 147
    move-object v4, v8

    .line 148
    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 150
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 186
    :cond_3
    :goto_2
    move-object/from16 v0, p1

    invoke-static {v12, v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->appendRecents(Ljava/util/List;Ljava/util/LinkedList;Ljava/util/Set;)V

    .line 188
    new-instance v14, Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-object/from16 v0, p2

    invoke-direct {v14, v4, v12, v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Ljava/util/List;Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;)V

    return-object v14

    .line 152
    :cond_4
    if-eqz v6, :cond_7

    .line 153
    move-object v4, v6

    .line 154
    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    if-eqz v9, :cond_5

    .line 156
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-interface {v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_5
    invoke-static {v7, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 160
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 162
    :cond_6
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 163
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    goto :goto_2

    .line 167
    :cond_7
    if-eqz v9, :cond_8

    .line 168
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-interface {v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    :cond_8
    invoke-static {v7, v9}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 172
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 174
    :cond_9
    invoke-virtual/range {p1 .. p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .line 175
    .local v11, "r":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-interface {v11}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v2, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_a

    .line 176
    move-object v4, v11

    .line 177
    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-interface {v2, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    .end local v11    # "r":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    :cond_b
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 182
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    goto/16 :goto_2

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static isInHiddenMruItems(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z
    .locals 2
    .param p0, "item"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 218
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isInHiddenMruItems(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isPlaying(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z
    .locals 2
    .param p0, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 192
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$1;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 201
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 197
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static prependToModelRecents(Ljava/util/LinkedList;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 8
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 223
    .local p0, "viewModelRecents":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;>;"
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v0

    .line 224
    .local v0, "providerTitleId":J
    const/4 v3, 0x0

    .line 225
    .local v3, "toRemove":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-virtual {p0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .line 226
    .local v2, "r":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v6

    cmp-long v5, v6, v0

    if-nez v5, :cond_0

    .line 227
    move-object v3, v2

    .line 231
    .end local v2    # "r":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    :cond_1
    if-eqz v3, :cond_2

    .line 232
    invoke-virtual {p0, v3}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 235
    :cond_2
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->toRecent()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 236
    return-void
.end method

.method private static shouldAddToRecents(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z
    .locals 6
    .param p0, "npiOld"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .param p1, "npiNew"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 206
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isInHiddenMruItems(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 210
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 211
    goto :goto_0

    .line 214
    :cond_2
    invoke-interface {p0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v2

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getProviderTitleId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isValid()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/viewmodel/NPState;

    if-nez v3, :cond_2

    move v1, v2

    .line 91
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 93
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NPState;

    .line 94
    .local v0, "that":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->currentProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->currentProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 95
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    .line 96
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    return-object v0
.end method

.method public getRecent(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .locals 4
    .param p1, "position"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 56
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecentsCount()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public hasRecents()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    if-nez v0, :cond_0

    .line 77
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    .line 78
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    .line 79
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->currentProgressItem:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    .line 80
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    .line 83
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hashCode:I

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->liBat:Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState;->recents:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
