.class public Lcom/microsoft/xbox/toolkit/XLEAccelerometer;
.super Ljava/lang/Object;
.source "XLEAccelerometer.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final GRAVITY_BLEND_ALPHA:F = 0.9f

.field private static final SHAKE_MS:I = 0x5a

.field private static final X_ACCEL_EPSILON:F = 7.0f


# instance fields
.field private accel:[F

.field private accelSensor:Landroid/hardware/Sensor;

.field private gravity:[F

.field private lastMs:J

.field private lastSignX:I

.field private sensorManager:Landroid/hardware/SensorManager;

.field private shakeCount:I

.field private shakeUpdatedRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/XLEApplication;)V
    .locals 4
    .param p1, "application"    # Lcom/microsoft/xbox/XLEApplication;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v2, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastSignX:I

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastMs:J

    .line 20
    iput v2, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    .line 24
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->gravity:[F

    .line 25
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->accel:[F

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeUpdatedRunnable:Ljava/lang/Runnable;

    .line 29
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->sensorManager:Landroid/hardware/SensorManager;

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->accelSensor:Landroid/hardware/Sensor;

    .line 31
    return-void

    .line 24
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 25
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private callShakeUpdatedRunnable()V
    .locals 2

    .prologue
    .line 113
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeUpdatedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeUpdatedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 118
    :cond_0
    return-void

    .line 113
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearShakes()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 62
    iput v1, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    .line 63
    return-void

    :cond_0
    move v0, v1

    .line 60
    goto :goto_0
.end method

.method public getShakeCount()I
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 56
    iget v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 68
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 42
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->accelSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 45
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 72
    if-eqz p1, :cond_0

    iget-object v7, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    if-eqz v7, :cond_0

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    if-nez v7, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v7, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v7}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 79
    :pswitch_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->gravity:[F

    array-length v7, v7

    if-ge v3, v7, :cond_2

    .line 80
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->gravity:[F

    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->gravity:[F

    aget v8, v8, v3

    const v9, 0x3f666666    # 0.9f

    mul-float/2addr v8, v9

    iget-object v9, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v9, v9, v3

    const v10, 0x3dccccd0    # 0.100000024f

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    aput v8, v7, v3

    .line 81
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->accel:[F

    iget-object v8, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v8, v8, v3

    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->gravity:[F

    aget v9, v9, v3

    sub-float/2addr v8, v9

    aput v8, v7, v3

    .line 79
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 84
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->accel:[F

    aget v5, v7, v2

    .line 86
    .local v5, "xAcc":F
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x40e00000    # 7.0f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_4

    move v4, v2

    .line 88
    .local v4, "signX":I
    :goto_2
    if-eqz v4, :cond_0

    iget v7, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastSignX:I

    if-eq v4, v7, :cond_0

    .line 89
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastMs:J

    sub-long v0, v8, v10

    .line 90
    .local v0, "elapsedMs":J
    const-wide/16 v8, 0x5a

    cmp-long v7, v0, v8

    if-gez v7, :cond_3

    move v2, v6

    .line 92
    .local v2, "happenedInTime":Z
    :cond_3
    if-nez v2, :cond_5

    .line 94
    iput v6, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    .line 102
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastMs:J

    .line 103
    iput v4, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->lastSignX:I

    goto :goto_0

    .line 86
    .end local v0    # "elapsedMs":J
    .end local v2    # "happenedInTime":Z
    .end local v4    # "signX":I
    :cond_4
    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v7

    float-to-int v4, v7

    goto :goto_2

    .line 97
    .restart local v0    # "elapsedMs":J
    .restart local v2    # "happenedInTime":Z
    .restart local v4    # "signX":I
    :cond_5
    iget v6, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeCount:I

    .line 99
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->callShakeUpdatedRunnable()V

    goto :goto_3

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setShakeUpdatedRunnable(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 48
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->shakeUpdatedRunnable:Ljava/lang/Runnable;

    .line 51
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
