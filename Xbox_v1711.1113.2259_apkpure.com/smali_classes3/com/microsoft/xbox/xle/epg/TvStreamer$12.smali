.class Lcom/microsoft/xbox/xle/epg/TvStreamer$12;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->animateView(Landroid/view/View;IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

.field final synthetic val$lp:Landroid/widget/FrameLayout$LayoutParams;

.field final synthetic val$toHeight:I

.field final synthetic val$toWidth:I

.field final synthetic val$toX:I

.field final synthetic val$toY:I

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Landroid/widget/FrameLayout$LayoutParams;IIIILandroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 566
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    iput p3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toWidth:I

    iput p4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toHeight:I

    iput p5, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toX:I

    iput p6, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toY:I

    iput-object p7, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 592
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 582
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toWidth:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 583
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toHeight:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 584
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toX:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 585
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$toY:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$v:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$12;->val$lp:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 587
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 578
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 572
    return-void
.end method
