.class public Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConsoleConnectionScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;

    .line 30
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "Connection - Connection View"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 35
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->onCreateContentView()V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$ConsoleConnectionScreenAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 38
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 46
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->onCreate()V

    .line 48
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f0300b4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConsoleConnectionScreen;->setContentView(I)V

    .line 53
    return-void
.end method
