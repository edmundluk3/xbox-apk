.class Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AttainmentDetailScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAchievementDetailAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

.field private achievementId:I

.field private friendsWhoEarnedRequestFailed:Z

.field private scid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Ljava/lang/String;I)V
    .locals 4
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I

    .prologue
    const/4 v3, 0x0

    .line 506
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 501
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 504
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->friendsWhoEarnedRequestFailed:Z

    .line 507
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    .line 508
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    .line 509
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 3

    .prologue
    .line 513
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 514
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    iget-boolean v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshAchievementDetail(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshGameProgressTitleImages()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 542
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 543
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->forceLoad:Z

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v10, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAchievementDetail(ZLjava/lang/String;I)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    .line 544
    .local v5, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 545
    const-string v7, "AttainmentDetailScreenViewModel"

    const-string v8, "Unable to get achievement details"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_0
    :goto_0
    return-object v5

    .line 549
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v9, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAchievementDetailData(Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    move-result-object v0

    .line 551
    .local v0, "achievementDetail":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-boolean v7, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->isSecret:Z

    if-nez v7, :cond_2

    .line 552
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->forceLoad:Z

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleImages(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 553
    const-string v7, "AttainmentDetailScreenViewModel"

    const-string v8, "Unable to get title image details"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    iget-boolean v7, v7, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    if-eqz v7, :cond_0

    .line 559
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->forceLoad:Z

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v10, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v7, v8, v9, v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFriendsWhoEarnedAchievement(ZLjava/lang/String;I)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 561
    .local v2, "friendsWhoEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 562
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFriendsWhoEarnedAchievement()Ljava/util/ArrayList;

    move-result-object v1

    .line 563
    .local v1, "friendsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 564
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 565
    .local v6, "userXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;

    .line 566
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 567
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;
    :cond_4
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 572
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->forceLoad:Z

    invoke-virtual {v7, v8, v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadProfileFriendsWhoEarnedAchievement(ZLjava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    .line 573
    .local v4, "profileFriendsWhoEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 574
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->friendsWhoEarnedRequestFailed:Z

    goto/16 :goto_0

    .line 580
    .end local v1    # "friendsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;>;"
    .end local v4    # "profileFriendsWhoEarnedAchievementStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .end local v6    # "userXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->friendsWhoEarnedRequestFailed:Z

    .line 581
    const-string v7, "AttainmentDetailScreenViewModel"

    const-string v8, "Unable to get friends who earned this achievement"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 5

    .prologue
    .line 519
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 520
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->friendsWhoEarnedRequestFailed:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;IZ)V

    .line 521
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->achievementId:I

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->friendsWhoEarnedRequestFailed:Z

    invoke-static {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;IZ)V

    .line 533
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 500
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 525
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 526
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->access$702(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Z)Z

    .line 527
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->updateAdapter()V

    .line 528
    return-void
.end method
