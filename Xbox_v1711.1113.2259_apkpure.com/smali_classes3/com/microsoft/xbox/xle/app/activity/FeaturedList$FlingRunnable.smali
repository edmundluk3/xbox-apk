.class Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;
.super Ljava/lang/Object;
.source "FeaturedList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlingRunnable"
.end annotation


# static fields
.field public static final ACCELERATION_THERSHOLD:I = 0x14

.field public static final MAX_FRAME_DELAY:I = 0x32

.field public static final SPEED_THRESHOLD:I = 0xc8

.field public static final WANTED_FRAME_DELAY:I = 0xa


# instance fields
.field private lastTime:J

.field private snapPoint:I

.field private snapping:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

.field private velocity:F


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;F)V
    .locals 2
    .param p2, "velocity"    # F

    .prologue
    .line 789
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 790
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 791
    iput p2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    .line 795
    :goto_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->lastTime:J

    .line 796
    return-void

    .line 793
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    goto :goto_0
.end method

.method private getAcceleration()F
    .locals 4

    .prologue
    .line 856
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1200(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F

    move-result v2

    :goto_0
    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    neg-float v3, v3

    mul-float v0, v2, v3

    .line 858
    .local v0, "acceleration":F
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-eqz v2, :cond_0

    .line 859
    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$900(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v3

    sub-int v1, v2, v3

    .line 861
    .local v1, "distanceToSnapPoint":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F

    move-result v2

    int-to-float v3, v1

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 863
    .end local v1    # "distanceToSnapPoint":I
    :cond_0
    return v0

    .line 856
    .end local v0    # "acceleration":F
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F

    move-result v2

    goto :goto_0
.end method

.method private getDeltaTAndSaveCurrentTime()F
    .locals 6

    .prologue
    .line 889
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    .line 890
    .local v2, "now":J
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->lastTime:J

    sub-long v0, v2, v4

    .line 891
    .local v0, "deltaT":J
    const-wide/16 v4, 0x32

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 892
    const-wide/16 v0, 0x32

    .line 894
    :cond_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->lastTime:J

    .line 895
    long-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    return v4
.end method

.method private scheduleNewFrame()V
    .locals 4

    .prologue
    .line 899
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, p0, v2, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 900
    return-void
.end method

.method private snapIfNeeded(I)V
    .locals 3
    .param p1, "listTop"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 867
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1500(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1600(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    .line 869
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    if-le p1, v0, :cond_0

    .line 873
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    .line 877
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1700(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1800(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    .line 879
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    if-ge p1, v0, :cond_1

    .line 883
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    .line 886
    :cond_1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 805
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$800(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    if-eq v4, v5, :cond_1

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$900(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v3

    .line 812
    .local v3, "listTop":I
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-nez v4, :cond_2

    .line 813
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapIfNeeded(I)V

    .line 816
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->getAcceleration()F

    move-result v0

    .line 817
    .local v0, "acceleration":F
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->getDeltaTAndSaveCurrentTime()F

    move-result v2

    .line 818
    .local v2, "dt":F
    iget v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    mul-float v5, v0, v2

    add-float/2addr v4, v5

    iput v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    .line 819
    iget v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    mul-float/2addr v4, v2

    float-to-int v1, v4

    .line 823
    .local v1, "deltaPos":I
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$900(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v4

    add-int/2addr v4, v1

    iget v5, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    if-eq v4, v5, :cond_3

    .line 824
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$900(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I

    move-result v4

    add-int/2addr v4, v1

    iget v5, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    if-ge v4, v5, :cond_6

    .line 825
    add-int/lit8 v1, v1, 0x1

    .line 831
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1000(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 833
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-nez v4, :cond_4

    .line 834
    add-int v4, v3, v1

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapIfNeeded(I)V

    .line 839
    :cond_4
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapping:Z

    if-eqz v4, :cond_5

    .line 840
    const/4 v4, 0x0

    iput v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->velocity:F

    .line 841
    const/4 v0, 0x0

    .line 842
    iget v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->snapPoint:I

    sub-int v1, v4, v3

    .line 846
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    add-int v5, v3, v1

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->access$1100(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;I)V

    .line 848
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 849
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->scheduleNewFrame()V

    goto :goto_0

    .line 827
    :cond_6
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public start()V
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->this$0:Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 800
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->scheduleNewFrame()V

    .line 801
    return-void
.end method
