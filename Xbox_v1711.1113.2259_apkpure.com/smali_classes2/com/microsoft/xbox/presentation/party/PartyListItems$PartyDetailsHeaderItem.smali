.class public final enum Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;
.super Ljava/lang/Enum;
.source "PartyListItems.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PartyDetailsHeaderItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;",
        ">;",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->$VALUES:[Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->$VALUES:[Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    return-object v0
.end method
