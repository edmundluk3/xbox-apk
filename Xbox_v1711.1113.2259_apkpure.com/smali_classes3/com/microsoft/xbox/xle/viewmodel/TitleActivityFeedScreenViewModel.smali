.class public Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "TitleActivityFeedScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

.field private titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 8
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const-wide/16 v6, 0x0

    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 36
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v2

    .line 37
    .local v2, "titleId":J
    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 38
    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getInstance(J)Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 41
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 42
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    .line 43
    :goto_1
    cmp-long v4, v2, v6

    if-eqz v4, :cond_3

    .line 44
    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getInstance(J)Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_1

    .line 45
    :cond_3
    if-eqz v1, :cond_0

    .line 46
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    goto :goto_0
.end method


# virtual methods
.method public canPinPosts()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method protected getActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getFeedItems()Ljava/util/List;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0705b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimelineId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Title:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 12
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 103
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 105
    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 107
    .local v6, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    if-nez v7, :cond_0

    .line 108
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v7, :cond_0

    .line 109
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v7, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v6

    .line 110
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 111
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->getInstance(J)Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .line 116
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    if-eqz v7, :cond_8

    .line 117
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-virtual {v7, p1, p2}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->loadSync(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v6

    .line 119
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 120
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    const-string v8, "Unable to get my following activity data"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v7, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 127
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v2

    .line 129
    .local v2, "followingData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_8

    .line 131
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    .line 132
    .local v4, "lookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 133
    .local v1, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v8, v1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v8, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 136
    .end local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->getActivityFeedData()Ljava/util/List;

    move-result-object v0

    .line 137
    .local v0, "activityFeedData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_8

    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 139
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 141
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPresenceData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 145
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 146
    .restart local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 147
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 148
    .restart local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v8, v1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V

    .line 149
    iget-object v8, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setRealName(Ljava/lang/String;)V

    .line 150
    iget-object v8, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v8, :cond_5

    .line 151
    iget-object v8, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfilePictureURI(Ljava/lang/String;)V

    goto :goto_1

    .line 153
    .end local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_6
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPresenceData()Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    move-result-object v5

    .line 155
    .local v5, "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    if-eqz v5, :cond_7

    .line 156
    iget-object v8, v5, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V

    .line 158
    :cond_7
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setRealName(Ljava/lang/String;)V

    goto :goto_1

    .line 165
    .end local v0    # "activityFeedData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    .end local v2    # "followingData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v4    # "lookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v5    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    :cond_8
    return-object v6
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->mediaItemDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->titleFeedModel:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 84
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshFollowingProfile()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showActivityAlertNotificationBar()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method
