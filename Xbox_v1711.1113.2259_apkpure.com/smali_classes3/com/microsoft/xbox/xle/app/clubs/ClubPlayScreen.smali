.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreen;
.super Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;
.source "ClubPlayScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f030095

    return v0
.end method

.method protected getErrorScreenId()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f030093

    return v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0702c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getInvalidScreenId()I
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f030067

    return v0
.end method

.method protected getNoContentScreenId()I
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f030094

    return v0
.end method

.method protected getRootViewId()I
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f030096

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->onCreate()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    return-void
.end method
