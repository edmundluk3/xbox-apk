.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;
.super Ljava/lang/Object;
.source "UTCLeaderboard.java"


# static fields
.field private static gameTitleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->gameTitleId:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 11
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->gameTitleId:J

    return-wide v0
.end method

.method public static getGameTitleId()J
    .locals 2

    .prologue
    .line 16
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->gameTitleId:J

    return-wide v0
.end method

.method public static setGameTitleId(J)V
    .locals 0
    .param p0, "gameTitleId"    # J

    .prologue
    .line 20
    sput-wide p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard;->gameTitleId:J

    .line 21
    return-void
.end method

.method public static trackPageView()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 60
    return-void
.end method

.method public static trackShowFriendHeroProfile(Ljava/lang/String;)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$2;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 48
    return-void
.end method

.method public static trackShowFriendProfile(Ljava/lang/String;)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;

    .prologue
    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLeaderboard$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 34
    return-void
.end method
