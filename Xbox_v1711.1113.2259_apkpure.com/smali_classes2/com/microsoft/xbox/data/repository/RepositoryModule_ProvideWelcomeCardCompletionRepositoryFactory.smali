.class public final Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;
.super Ljava/lang/Object;
.source "RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final authStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

.field private final sharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;>;"
    .local p3, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p4, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 37
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 39
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;>;"
    .local p2, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p3, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;-><init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideWelcomeCardCompletionRepository(Lcom/microsoft/xbox/data/repository/RepositoryModule;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .param p1, "dataMapper"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;
    .param p2, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p3, "authStateManager"    # Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .prologue
    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideWelcomeCardCompletionRepository(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .locals 4

    .prologue
    .line 43
    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 45
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 46
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 47
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 44
    invoke-virtual {v3, v0, v1, v2}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideWelcomeCardCompletionRepository(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 43
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->get()Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    move-result-object v0

    return-object v0
.end method
