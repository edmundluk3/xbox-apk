.class public final enum Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;
.super Ljava/lang/Enum;
.source "SkypeConversationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

.field public static final enum Group:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

.field public static final enum OneOnOne:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

.field public static final enum Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    const-string v1, "Group"

    const-string v2, "19"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Group:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    .line 5
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    const-string v1, "OneOnOne"

    const-string v2, "8"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->OneOnOne:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    .line 6
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    const-string v1, "Service"

    const-string v2, "28"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Group:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->OneOnOne:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->type:Ljava/lang/String;

    .line 12
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;

    return-object v0
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeConversationType;->type:Ljava/lang/String;

    return-object v0
.end method
