.class public Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "MovieOverviewScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "MovieOverview"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;->onCreateContentView()V

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    const/4 v2, 0x0

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    const v4, 0x7f0e07bd

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 16
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 20
    const v0, 0x7f030178

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/MovieOverviewScreen;->setContentView(I)V

    .line 21
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method
