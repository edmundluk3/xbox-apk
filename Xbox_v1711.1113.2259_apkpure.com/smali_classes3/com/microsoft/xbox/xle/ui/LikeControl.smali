.class public Lcom/microsoft/xbox/xle/ui/LikeControl;
.super Lcom/microsoft/xbox/toolkit/ui/XLEButton;
.source "LikeControl.java"


# instance fields
.field private filledHeartColor:I

.field private filledLikeHeart:Ljava/lang/String;

.field private isLiked:Z

.field private likeContentDescription:Ljava/lang/String;

.field private likeHeart:Ljava/lang/String;

.field private textColor:I

.field private unlikeContentDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const v1, 0x7f070f5d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->likeHeart:Ljava/lang/String;

    .line 29
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0142

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->textColor:I

    .line 30
    const v1, 0x7f070f5e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->filledLikeHeart:Ljava/lang/String;

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 34
    .local v0, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->filledHeartColor:I

    .line 38
    const v1, 0x7f070d5f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->likeContentDescription:Ljava/lang/String;

    .line 39
    const v1, 0x7f070d87

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->unlikeContentDescription:Ljava/lang/String;

    .line 41
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->isLiked:Z

    .line 46
    return-void

    .line 35
    :cond_0
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_0
.end method

.method private setNarratorContent()V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->isLiked:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->unlikeContentDescription:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->likeContentDescription:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public cleanup()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    return-void
.end method

.method public disable()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->likeHeart:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setEnabled(Z)V

    .line 67
    return-void
.end method

.method public enable()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setEnabled(Z)V

    .line 71
    return-void
.end method

.method public setLikeState(Z)V
    .locals 1
    .param p1, "liked"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->isLiked:Z

    .line 54
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->isLiked:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->filledLikeHeart:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->filledHeartColor:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setTextColor(I)V

    .line 61
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setNarratorContent()V

    .line 62
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->likeHeart:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/LikeControl;->textColor:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/LikeControl;->setTextColor(I)V

    goto :goto_0
.end method
