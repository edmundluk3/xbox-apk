.class Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;
.super Lcom/microsoft/xbox/smartglass/AccelerometerListener;
.source "JavaScriptAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccelListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/AccelerometerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReadingChanged(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/AccelerometerReading;)V
    .locals 11
    .param p1, "accelerometer"    # Lcom/microsoft/xbox/smartglass/Accelerometer;
    .param p2, "reading"    # Lcom/microsoft/xbox/smartglass/AccelerometerReading;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->access$000(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)Lcom/microsoft/xbox/smartglass/RefTPtr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-wide v4, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->x:D

    iget-wide v6, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->y:D

    iget-wide v8, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->z:D

    const-string v10, "accelerometerReading"

    invoke-static/range {v1 .. v10}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->access$100(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;JDDDLjava/lang/String;)V

    .line 104
    return-void
.end method
