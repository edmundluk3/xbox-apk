.class public Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "XLEMultiScreenDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Landroid/view/View;",
        ":",
        "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;",
        ">",
        "Lcom/microsoft/xbox/toolkit/XLEManagedDialog;"
    }
.end annotation


# instance fields
.field private screen:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 24
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 25
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 20
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 21
    return-void
.end method

.method private asScreen(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")TS;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    const-string v0, "View must implement DialogScreen"

    instance-of v1, p1, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 78
    return-object p1
.end method

.method private shutdownScreen()V
    .locals 1

    .prologue
    .line 93
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->onStop()V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->isCreated()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->onDestroy()V

    .line 101
    :cond_1
    return-void
.end method

.method private startupScreen()V
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->isCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->onCreate()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;->onStart()V

    .line 90
    :cond_1
    return-void
.end method


# virtual methods
.method public getAnimateIn()Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1

    .prologue
    .line 116
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAnimateOut()Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1

    .prologue
    .line 108
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScreen()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    return-object v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 65
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->startupScreen()V

    .line 67
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 71
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->shutdownScreen()V

    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 73
    return-void
.end method

.method public final setContentView(I)V
    .locals 2
    .param p1, "layoutResID"    # I

    .prologue
    .line 29
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    const-string v0, "XLEMultiScreenDialog.setContentView(int layoutResID) is disabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 30
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 34
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->asScreen(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->setScreen(Landroid/view/View;)V

    .line 35
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 39
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->asScreen(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->setScreen(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    return-void
.end method

.method public setScreen(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    .local p1, "screen":Landroid/view/View;, "TS;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->setScreen(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    return-void
.end method

.method public setScreen(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Landroid/view/ViewGroup$LayoutParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;, "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog<TS;>;"
    .local p1, "screen":Landroid/view/View;, "TS;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->shutdownScreen()V

    .line 53
    if-nez p2, :cond_1

    .line 54
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->setContentView(Landroid/view/View;)V

    .line 58
    :goto_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->screen:Landroid/view/View;

    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;->startupScreen()V

    .line 61
    :cond_0
    return-void

    .line 56
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
