.class public Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;
.source "UnsharedActivityFeedScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UnsharedActivityFeedListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;Landroid/content/Context;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;

    .line 134
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;-><init>(Landroid/content/Context;I)V

    .line 135
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    .line 136
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 140
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const-string v14, "layout_inflater"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 141
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v13, 0x7f03025a

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v5, v13, v0, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 142
    .local v9, "rowView":Landroid/view/View;
    const v13, 0x7f0e0b9e

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 143
    .local v4, "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v13, 0x7f0e0b9f

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 144
    .local v7, "legacyAchievementIconView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v13, 0x7f0e0ba2

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 145
    .local v8, "primaryTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v13, 0x7f0e0ba3

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 146
    .local v10, "secondaryTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v13, 0x7f0e0ba0

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 147
    .local v3, "iconText":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v13, 0x7f0e0ba1

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 148
    .local v2, "achievementScore":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v13, 0x7f0e0ba4

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 149
    .local v12, "viewCountContainer":Landroid/view/View;
    const v13, 0x7f0e0ba5

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 150
    .local v11, "viewCount":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 152
    .local v6, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v6, :cond_0

    .line 153
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    invoke-virtual {v10, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    sget-object v13, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 199
    :cond_0
    :goto_0
    return-object v9

    .line 157
    :pswitch_0
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->backgroundImage:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 158
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->backgroundImage:Ljava/lang/String;

    const v14, 0x7f020053

    const v15, 0x7f020053

    invoke-virtual {v4, v13, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 160
    :cond_1
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 161
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    invoke-virtual {v7, v13}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 162
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070f51

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070d57

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    iget v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 168
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementDescription:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    const v14, 0x7f020053

    const v15, 0x7f020053

    invoke-virtual {v4, v13, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 173
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070f51

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070d57

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    iget v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->gamerscore:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 177
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->achievementDescription:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 181
    :pswitch_2
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    const v14, 0x7f02017f

    const v15, 0x7f02017f

    invoke-virtual {v4, v13, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 182
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070fb8

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    const/16 v13, 0x8

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 184
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 185
    iget-wide v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-wide v14, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    const-wide/16 v16, 0x1

    cmp-long v13, v14, v16

    if-lez v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f0705f4

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 187
    :goto_1
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-wide v0, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->viewCount:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    .line 186
    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f0705f3

    .line 187
    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    .line 190
    :pswitch_3
    iget-object v13, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemImage:Ljava/lang/String;

    const v14, 0x7f0201fb

    const v15, 0x7f0201fb

    invoke-virtual {v4, v13, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 191
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;->context:Landroid/content/Context;

    const v14, 0x7f070ee5

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const/16 v13, 0x8

    invoke-virtual {v2, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 193
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 210
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 205
    return-void
.end method
