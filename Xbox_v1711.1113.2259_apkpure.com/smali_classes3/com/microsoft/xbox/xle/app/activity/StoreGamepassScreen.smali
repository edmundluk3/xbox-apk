.class public Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "StoreGamepassScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;

    .line 20
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "Store - Game Pass"

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;->onCreateContentView()V

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;

    sget-object v2, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gamepass:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;)V

    .line 29
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->setAsPivotPane()V

    .line 30
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->onCreate()V

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 33
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f030213

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;->setContentView(I)V

    .line 38
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method
