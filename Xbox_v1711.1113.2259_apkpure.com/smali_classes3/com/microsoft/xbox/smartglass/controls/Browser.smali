.class Lcom/microsoft/xbox/smartglass/controls/Browser;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Browser.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Browser"

.field private static final LaunchMethod:Ljava/lang/String; = "Launch"

.field private static final s_allowedScheme:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/Browser;->s_allowedScheme:Ljava/util/HashSet;

    .line 25
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/Browser;->s_allowedScheme:Ljava/util/HashSet;

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 26
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/Browser;->s_allowedScheme:Ljava/util/HashSet;

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;Landroid/content/Context;)V
    .locals 2
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "Browser"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 34
    if-nez p2, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/Browser;->_context:Landroid/content/Context;

    .line 40
    const-string v0, "Launch"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Browser;->registerMethod(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method private launch(ILandroid/net/Uri;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "url"    # Landroid/net/Uri;

    .prologue
    .line 60
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/Browser;->s_allowedScheme:Ljava/util/HashSet;

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Browser;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "Only http or https are supported."

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 73
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Browser;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "Launch"

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/Browser;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 72
    .local v0, "launchBrowser":Landroid/content/Intent;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Browser;->_context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Browser;->validateMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Browser;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 57
    :goto_0
    return-void

    .line 52
    :cond_0
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonBrowserParameters;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonBrowserParameters;-><init>(Ljava/lang/String;)V

    .line 53
    .local v1, "request":Lcom/microsoft/xbox/smartglass/controls/JsonBrowserParameters;
    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/controls/JsonBrowserParameters;->uri:Landroid/net/Uri;

    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/Browser;->launch(ILandroid/net/Uri;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    .end local v1    # "request":Lcom/microsoft/xbox/smartglass/controls/JsonBrowserParameters;
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Browser;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v3, "Invalid URL."

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
