.class public abstract Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;
.super Ljava/lang/Object;
.source "FrameChatHeadContainer.java"

# interfaces
.implements Lcom/flipkart/chatheads/ui/ChatHeadContainer;


# instance fields
.field private final context:Landroid/content/Context;

.field displayMetrics:Landroid/util/DisplayMetrics;

.field private frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

.field private manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 22
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->context:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public abstract addContainer(Landroid/view/View;Z)V
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    invoke-virtual {v0, p1, p2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    :cond_0
    return-void
.end method

.method public bringToFront(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 107
    return-void
.end method

.method public createLayoutParams(IIII)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "height"    # I
    .param p2, "width"    # I
    .param p3, "gravity"    # I
    .param p4, "bottomMargin"    # I

    .prologue
    .line 71
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, p2, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 72
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 73
    iput p4, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 74
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->context:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 90
    .local v0, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 91
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->displayMetrics:Landroid/util/DisplayMetrics;

    return-object v1
.end method

.method public getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    return-object v0
.end method

.method public getManager()Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    return-object v0
.end method

.method public getViewX(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getViewY(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public onInitialized(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    const/4 v2, 0x1

    .line 32
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 33
    new-instance v0, Lcom/flipkart/chatheads/ui/HostFrameLayout;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p1}, Lcom/flipkart/chatheads/ui/HostFrameLayout;-><init>(Landroid/content/Context;Lcom/flipkart/chatheads/ui/ChatHeadContainer;Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    .line 34
    .local v0, "frameLayout":Lcom/flipkart/chatheads/ui/HostFrameLayout;
    invoke-virtual {v0, v2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->setFocusable(Z)V

    .line 35
    invoke-virtual {v0, v2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->setFocusableInTouchMode(Z)V

    .line 36
    iput-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    .line 37
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->addContainer(Landroid/view/View;Z)V

    .line 38
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    invoke-virtual {v0, p1}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->removeView(Landroid/view/View;)V

    .line 67
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->frameLayout:Lcom/flipkart/chatheads/ui/HostFrameLayout;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->requestLayout()V

    .line 60
    :cond_0
    return-void
.end method

.method public setViewX(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "xPosition"    # I

    .prologue
    .line 79
    int-to-float v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 80
    return-void
.end method

.method public setViewY(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "yPosition"    # I

    .prologue
    .line 84
    int-to-float v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 85
    return-void
.end method
