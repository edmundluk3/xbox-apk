.class Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;
.super Ljava/lang/Object;
.source "InternetExplorerControl.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 146
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->access$500(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->access$600(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Ljava/lang/String;)V

    .line 152
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 153
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$7;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->access$000(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->isInRemoteControl()Z

    move-result v2

    if-nez v2, :cond_0

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissAppBar()V

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    .line 158
    :cond_0
    const/4 v2, 0x1

    .line 160
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return v2
.end method
