.class Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewAdapter"
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 575
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 576
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 577
    return-void
.end method

.method private bindNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 6
    .param p1, "cell"    # Landroid/view/View;
    .param p2, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    const/4 v5, 0x0

    .line 646
    const v4, 0x7f0e001b

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 647
    const v4, 0x7f0e06d6

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 648
    .local v2, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    if-nez p2, :cond_0

    .line 649
    invoke-virtual {p1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 650
    const v4, 0x7f0e0069

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 651
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->clearImage()V

    .line 652
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 653
    invoke-virtual {p1, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 654
    invoke-static {p1, v5, v5}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 670
    :goto_0
    return-void

    .line 656
    :cond_0
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 657
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getAppMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    .line 658
    .local v3, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getBoxArtUri()Ljava/lang/String;

    move-result-object v1

    .line 659
    .local v1, "imageUrl":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-static {p2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 660
    :cond_1
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f070fca

    :goto_1
    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 661
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 667
    :goto_2
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 668
    invoke-static {p1, p2, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->bindCellTitle(Landroid/view/View;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 660
    :cond_2
    const v4, 0x7f070fdb

    goto :goto_1

    .line 663
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 664
    .local v0, "defaultRid":I
    :goto_3
    const v4, 0x7f0e001d

    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;

    invoke-direct {v5, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 665
    invoke-virtual {v2, v1, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto :goto_2

    .line 663
    .end local v0    # "defaultRid":I
    :cond_4
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_3
.end method

.method private bindView(ILandroid/view/View;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "cell"    # Landroid/view/View;

    .prologue
    .line 622
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    .line 623
    .local v0, "npi":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v0, :cond_2

    .line 624
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isRecent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 625
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    const-string v3, "Home Recents"

    invoke-virtual {v1, p2, v2, v3, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;->bindRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;Ljava/lang/String;I)V

    .line 632
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizeCellForTwoWayView(Landroid/view/View;)V

    .line 633
    return-void

    .line 626
    :cond_1
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 627
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->bindNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    goto :goto_0

    .line 630
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private createView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 636
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$ViewType:[I

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 641
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030140

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 638
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03013d

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 636
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 610
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    .line 611
    .local v0, "item":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isRecent()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->SNAP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    goto :goto_0
.end method

.method private getRecentCount()I
    .locals 2

    .prologue
    .line 580
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    .line 581
    .local v0, "state":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecentsCount()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$bindNPM$0(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;Lcom/microsoft/xbox/xle/model/NowPlayingModel;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 656
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedOnSnappedNPM(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 586
    const/4 v0, 0x4

    return v0
.end method

.method public getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 591
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getRecentCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecent(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 572
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 596
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 606
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 616
    if-nez p2, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->createView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 617
    .local v0, "cell":Landroid/view/View;
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->bindView(ILandroid/view/View;)V

    .line 618
    return-object v0

    .end local v0    # "cell":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 616
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 601
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
