.class public final Lcom/microsoft/msa/resources/MSAResources/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/msa/resources/MSAResources/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x1a

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1c

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x1b

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x8

.field public static final SearchView_commitIcon:I = 0xd

.field public static final SearchView_goIcon:I = 0x9

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xf

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchIcon:I = 0xa

.field public static final SearchView_submitBackground:I = 0x10

.field public static final SearchView_suggestionRowLayout:I = 0xe

.field public static final SearchView_voiceIcon:I = 0xc

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0xd

.field public static final SwitchCompat_splitTrack:I = 0xc

.field public static final SwitchCompat_switchMinWidth:I = 0xa

.field public static final SwitchCompat_switchPadding:I = 0xb

.field public static final SwitchCompat_switchTextAppearance:I = 0x9

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_track:I = 0x5

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_collapseContentDescription:I = 0x17

.field public static final Toolbar_collapseIcon:I = 0x16

.field public static final Toolbar_contentInsetEnd:I = 0x6

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x5

.field public static final Toolbar_maxButtonHeight:I = 0x14

.field public static final Toolbar_navigationContentDescription:I = 0x19

.field public static final Toolbar_navigationIcon:I = 0x18

.field public static final Toolbar_popupTheme:I = 0xb

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xd

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMarginBottom:I = 0x12

.field public static final Toolbar_titleMarginEnd:I = 0x10

.field public static final Toolbar_titleMarginStart:I = 0xf

.field public static final Toolbar_titleMarginTop:I = 0x11

.field public static final Toolbar_titleMargins:I = 0x13

.field public static final Toolbar_titleTextAppearance:I = 0xc

.field public static final View:[I

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_paddingEnd:I = 0x3

.field public static final View_paddingStart:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 688
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActionBar:[I

    .line 689
    new-array v0, v3, [I

    const v1, 0x10100b3

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActionBarLayout:[I

    .line 718
    new-array v0, v3, [I

    const v1, 0x101013f

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActionMenuItemView:[I

    .line 720
    new-array v0, v2, [I

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActionMenuView:[I

    .line 721
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActionMode:[I

    .line 728
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ActivityChooserView:[I

    .line 731
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->DrawerArrowToggle:[I

    .line 737
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->LinearLayoutCompat:[I

    .line 738
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 752
    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ListPopupWindow:[I

    .line 755
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->MenuGroup:[I

    .line 762
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->MenuItem:[I

    .line 780
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->MenuView:[I

    .line 789
    new-array v0, v5, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->PopupWindow:[I

    .line 790
    new-array v0, v3, [I

    const v1, 0x7f0101c6

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->PopupWindowBackgroundState:[I

    .line 794
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->SearchView:[I

    .line 810
    new-array v0, v6, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->Spinner:[I

    .line 813
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->SwitchCompat:[I

    .line 824
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->Toolbar:[I

    .line 846
    new-array v0, v6, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->View:[I

    .line 847
    new-array v0, v5, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/microsoft/msa/resources/MSAResources/R$styleable;->ViewStubCompat:[I

    return-void

    .line 688
    nop

    :array_0
    .array-data 4
        0x7f01000b
        0x7f010043
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
        0x7f010064
        0x7f0100aa
    .end array-data

    .line 721
    :array_1
    .array-data 4
        0x7f01000b
        0x7f01004e
        0x7f01004f
        0x7f010053
        0x7f010055
        0x7f010065
    .end array-data

    .line 728
    :array_2
    .array-data 4
        0x7f010066
        0x7f010067
    .end array-data

    .line 731
    :array_3
    .array-data 4
        0x7f01014c
        0x7f01014d
        0x7f01014e
        0x7f01014f
        0x7f010150
        0x7f010151
        0x7f010152
        0x7f010153
    .end array-data

    .line 737
    :array_4
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f010052
        0x7f01019b
        0x7f01019c
        0x7f01019d
    .end array-data

    .line 738
    :array_5
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    .line 752
    :array_6
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    .line 755
    :array_7
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    .line 762
    :array_8
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f0101a3
        0x7f0101a4
        0x7f0101a5
        0x7f0101a6
    .end array-data

    .line 780
    :array_9
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f0101a7
        0x7f0101a8
    .end array-data

    .line 789
    :array_a
    .array-data 4
        0x1010176
        0x10102c9
        0x7f0101c5
    .end array-data

    .line 794
    :array_b
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0101d5
        0x7f0101d6
        0x7f0101d7
        0x7f0101d8
        0x7f0101d9
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
    .end array-data

    .line 810
    :array_c
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f010064
    .end array-data

    .line 813
    :array_d
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f0101f7
        0x7f0101f8
        0x7f0101f9
        0x7f0101fa
        0x7f0101fb
        0x7f0101fc
        0x7f0101fd
        0x7f0101fe
        0x7f0101ff
        0x7f010200
        0x7f010201
    .end array-data

    .line 824
    :array_e
    .array-data 4
        0x10100af
        0x1010140
        0x7f010043
        0x7f01004d
        0x7f010051
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010064
        0x7f010227
        0x7f010228
        0x7f010229
        0x7f01022a
        0x7f01022b
        0x7f01022c
        0x7f01022d
        0x7f01022e
        0x7f01022f
        0x7f010230
        0x7f010231
        0x7f010232
        0x7f010233
        0x7f010234
        0x7f010235
        0x7f010236
        0x7f010237
    .end array-data

    .line 846
    :array_f
    .array-data 4
        0x1010000
        0x10100da
        0x7f010249
        0x7f01024a
        0x7f01024b
    .end array-data

    .line 847
    :array_10
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
