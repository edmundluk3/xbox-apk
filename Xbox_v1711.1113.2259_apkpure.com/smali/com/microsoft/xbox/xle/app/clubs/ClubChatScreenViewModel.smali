.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubChatScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;
    }
.end annotation


# static fields
.field private static final DIRECT_MENTION_FORMAT:Ljava/lang/String; = "<at id=\"%1$s\">@%2$s</at>"

.field private static final DIRECT_MENTION_RESOLVING_PATTERN:Ljava/util/regex/Pattern;

.field private static final MAX_RESULTS:I = 0x64

.field private static final REQUEST_OVERLAY_PERM_CODE:I = 0x4d2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private canViewerChat:Ljava/lang/Boolean;

.field private canViewerDeleteMessage:Ljava/lang/Boolean;

.field private canViewerSetChatTopic:Ljava/lang/Boolean;

.field private canViewerViewChat:Ljava/lang/Boolean;

.field private final channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

.field chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

.field private final chatObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/chat/ChatEvent;",
            ">;"
        }
    .end annotation
.end field

.field private clubPresenceHash:I

.field private final directMentionDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final directMentionSearchList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final directMentionSearchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field private directMentionSearchTerm:Ljava/lang/String;

.field private formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private hasCommunicationPrivilege:Z

.field private isLoadingMoreHistory:Z

.field private volatile isSearchingService:Z

.field private oldInputMode:I

.field private final personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

.field private final queryXuids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

.field private shouldScrollToBottom:Z

.field private shouldUpdateAllMessages:Z

.field private shouldUpdateHistoryMessages:Z

.field private shouldUpdateTyping:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    .line 98
    const-string v0, "@([a-zA-Z0-9 ]{0,15})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->DIRECT_MENTION_RESOLVING_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;)V

    .line 145
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;)V
    .locals 5
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "parameters"    # Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;)V

    .line 149
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 151
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 153
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    .line 154
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;-><init>(J)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    .line 155
    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->getClubChatModel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 157
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->queryXuids:Ljava/util/Set;

    .line 159
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 160
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubChatScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 162
    iput v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubPresenceHash:I

    .line 164
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    .line 165
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 166
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchList:Ljava/util/List;

    .line 168
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    .line 171
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/toolkit/XLEObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 250
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 252
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->oldInputMode:I

    .line 258
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 260
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hasCommunicationPrivilege:Z

    .line 261
    return-void

    .line 255
    :cond_0
    iput v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->oldInputMode:I

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->DIRECT_MENTION_RESOLVING_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Z

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->onFormatMessageCompleted(Ljava/lang/String;Ljava/util/List;Z)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method private formatMessageFromLocal(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 768
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopFormatMessageTask()V

    .line 770
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .line 771
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 772
    return-void
.end method

.method private formatMessageFromService(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "clubId"    # Ljava/lang/String;

    .prologue
    .line 775
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopFormatMessageTask()V

    .line 777
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .line 778
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 779
    return-void
.end method

.method public static getErrorRoleStringId(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)I
    .locals 4
    .param p0, "settingsRole"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .prologue
    .line 344
    if-nez p0, :cond_0

    .line 345
    const/4 v0, 0x0

    .line 361
    :goto_0
    return v0

    .line 348
    :cond_0
    const/4 v0, 0x0

    .line 349
    .local v0, "roleStringId":I
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 357
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized settings role: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v1, "getErrorRoleStringId - unrecognized settings role"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 351
    :pswitch_0
    const v0, 0x7f0702fc

    .line 352
    goto :goto_0

    .line 354
    :pswitch_1
    const v0, 0x7f0702fb

    .line 355
    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
    .param p1, "asyncResult"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    const/4 v5, 0x1

    .line 172
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .line 173
    .local v1, "event":Lcom/microsoft/xbox/service/model/chat/ChatEvent;
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "received event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v2, v3, :cond_0

    .line 176
    const-string v2, "chatObserver update is not called in UIThread"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 247
    :goto_0
    return-void

    .line 180
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$chat$ChatEvent:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 243
    const-string/jumbo v2, "unsupported event type"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 246
    :cond_1
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 182
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 185
    :pswitch_2
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    .line 186
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldScrollToBottom:Z

    goto :goto_1

    .line 189
    :pswitch_3
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateTyping:Z

    goto :goto_1

    .line 192
    :pswitch_4
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 193
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    .line 194
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldScrollToBottom:Z

    goto :goto_1

    .line 197
    :pswitch_5
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 200
    :pswitch_6
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isLoadingMoreHistory:Z

    .line 202
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_2

    .line 203
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateHistoryMessages:Z

    goto :goto_1

    .line 205
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chatObserver received History event with exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 214
    :pswitch_7
    const v2, 0x7f070276

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->showError(I)V

    .line 215
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    goto :goto_1

    .line 219
    :pswitch_8
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    goto :goto_1

    .line 223
    :pswitch_9
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getChatTicket()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    move-result-object v0

    .line 224
    .local v0, "currentTicket":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
    if-eqz v0, :cond_3

    .line 226
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->canRead()Z

    move-result v2

    .line 227
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->canWrite()Z

    move-result v3

    .line 228
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->canModerate()Z

    move-result v4

    .line 229
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->canSetMessageOfTheDay()Z

    move-result v5

    .line 225
    invoke-direct {p0, v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updatePrivilegesFromUserInfo(ZZZZ)V

    .line 231
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat()Z

    move-result v2

    if-nez v2, :cond_1

    .line 232
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "chatObserver observe UserInfo - user is not allowed to view chat"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 237
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "chatObserver received UserInfo with a null ChatTicket"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private loadPersonSummaries(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 759
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopGetPersonSummariesTask()V

    .line 761
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 762
    new-instance v0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 763
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 765
    :cond_0
    return-void
.end method

.method private onFormatMessageCompleted(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2
    .param p1, "messageText"    # Ljava/lang/String;
    .param p3, "containsLinks"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 905
    .local p2, "directMentionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 906
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->DirectMention:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-direct {v0, v1, p1, p3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Ljava/lang/String;Z)V

    .line 908
    .local v0, "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v1, v0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->sendDirectMentionMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Ljava/util/List;)V

    .line 915
    :goto_0
    return-void

    .line 911
    .end local v0    # "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    if-eqz p3, :cond_1

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->RichText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    :goto_1
    invoke-direct {v0, v1, p1, p3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Ljava/lang/String;Z)V

    .line 913
    .restart local v0    # "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->sendMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    goto :goto_0

    .line 911
    .end local v0    # "chatMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->BasicText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    goto :goto_1
.end method

.method private onLazyQueryPersonSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 702
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v4, v5, :cond_3

    .line 703
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getContext()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Ljava/util/List;

    if-eqz v4, :cond_2

    .line 705
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getContext()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 706
    .local v0, "completedXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onLazyQueryPersonSummaryCompleted - (competedXuids:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") (queryXuids:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->queryXuids:Ljava/util/Set;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 711
    const/4 v2, 0x0

    .line 713
    .local v2, "shouldUpdateView":Z
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 714
    .local v3, "xuid":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->queryXuids:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 716
    .local v1, "contained":Z
    if-eqz v1, :cond_0

    .line 717
    const/4 v2, 0x1

    goto :goto_0

    .line 721
    .end local v1    # "contained":Z
    .end local v3    # "xuid":Ljava/lang/String;
    :cond_1
    if-eqz v2, :cond_2

    .line 722
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    .line 723
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapterOnUIThread()V

    .line 730
    .end local v0    # "completedXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "shouldUpdateView":Z
    :cond_2
    :goto_1
    return-void

    .line 728
    :cond_3
    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "onLazyQueryPersonSummaryCompleted - PeopleHubPersonSummaryLoaded failed"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    .prologue
    .line 891
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isSearchingService:Z

    .line 892
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 894
    if-eqz p1, :cond_0

    .line 895
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;->getResults()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;

    .line 896
    .local v0, "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchList:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    iget-object v4, v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;-><init>(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 900
    .end local v0    # "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapter()V

    .line 901
    return-void
.end method

.method private onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 882
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUserDataLoadCompleted with result size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 885
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 886
    .local v0, "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 888
    .end local v0    # "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    return-void
.end method

.method private openAsHoverChatInternal()V
    .locals 4

    .prologue
    .line 600
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubChatOpenFromContextMenu(J)V

    .line 601
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addClubInstance(Landroid/content/Context;J)V

    .line 602
    return-void
.end method

.method private stopActiveTasks()V
    .locals 0

    .prologue
    .line 782
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopGetPersonSummariesTask()V

    .line 783
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopSearchTask()V

    .line 784
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopFormatMessageTask()V

    .line 785
    return-void
.end method

.method private stopFormatMessageTask()V
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->cancel()V

    .line 804
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageAsyncTask:Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    .line 806
    :cond_0
    return-void
.end method

.method private stopGetPersonSummariesTask()V
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 792
    :cond_0
    return-void
.end method

.method private stopSearchTask()V
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    if-eqz v0, :cond_0

    .line 796
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->cancel()V

    .line 797
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 799
    :cond_0
    return-void
.end method

.method private updateCanViewerViewChatIsNecessary(Z)V
    .locals 2
    .param p1, "canViewerViewChat"    # Z

    .prologue
    .line 858
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 859
    :cond_0
    if-eqz p1, :cond_2

    .line 860
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->connectAndJoinIfNecessary()V

    .line 867
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    .line 869
    :cond_1
    return-void

    .line 862
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateCanViewerViewChatIsNecessary - not allowed to view chat"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveNotAllowedChannel()V

    goto :goto_0
.end method

.method private updateMemberDataFromPresence(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 744
    .local p1, "presenceList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-interface {p1}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 746
    .local v1, "presenceHash":I
    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubPresenceHash:I

    if-eq v1, v3, :cond_1

    .line 747
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubPresenceHash:I

    .line 749
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 750
    .local v2, "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x64

    if-ge v3, v4, :cond_0

    .line 751
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 754
    :cond_0
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->loadPersonSummaries(Ljava/util/List;)V

    .line 756
    .end local v0    # "i":I
    .end local v2    # "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method private updatePrivilegesFromUserInfo(ZZZZ)V
    .locals 1
    .param p1, "canRead"    # Z
    .param p2, "canWrite"    # Z
    .param p3, "canModerate"    # Z
    .param p4, "canSetMessageOfTheDay"    # Z

    .prologue
    .line 872
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateCanViewerViewChatIsNecessary(Z)V

    .line 873
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerChat:Ljava/lang/Boolean;

    .line 874
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerDeleteMessage:Ljava/lang/Boolean;

    .line 875
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerSetChatTopic:Ljava/lang/Boolean;

    .line 878
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateChatPrivileges(ZZZZ)V

    .line 879
    return-void
.end method


# virtual methods
.method public canDeleteMessage()Z
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerDeleteMessage:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public canViewerChat()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerChat:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hasCommunicationPrivilege:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public canViewerSetChatTopic()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerSetChatTopic:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hasCommunicationPrivilege:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public canViewerViewChat()Z
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public clearShouldScrollToBottom()V
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldScrollToBottom:Z

    .line 380
    return-void
.end method

.method public clearShouldUpdateAllMessages()V
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    .line 388
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clearShouldUpdateHistoryMessages()V

    .line 389
    return-void
.end method

.method public clearShouldUpdateHistoryMessages()V
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateHistoryMessages:Z

    .line 397
    return-void
.end method

.method public clearShouldUpdateTyping()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateTyping:Z

    .line 405
    return-void
.end method

.method public deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 4
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 678
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 680
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubMessageDeleted(J)V

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->deleteMessageAsync(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    .line 685
    return-void
.end method

.method public dismissClubChatEditReportDialog()V
    .locals 1

    .prologue
    .line 485
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubChatEditReportDialog()V

    .line 486
    return-void
.end method

.method public dismissClubChatTopicDialog()V
    .locals 1

    .prologue
    .line 471
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatEditTopicCancel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 473
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubChatTopicDialog()V

    .line 474
    return-void
.end method

.method public getChatMessages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getMessages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getClubId()J
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getClubName()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 295
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 296
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getDirectMentionAutoCompleteList()Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 277
    .local v1, "emptySearchTerm":Z
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->serviceSearchEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchList:Ljava/util/List;

    .line 278
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 280
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    :goto_0
    if-nez v1, :cond_2

    .line 281
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 282
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .line 284
    .local v2, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->matchesSearchTerm(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 285
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 278
    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 290
    .restart local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;>;"
    :cond_2
    return-object v0
.end method

.method public getHistoryMessagesSize()I
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getNumHistoryMessagesLoaded()I

    move-result v0

    return v0
.end method

.method public getInvalidReasonText()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 490
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 492
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v1

    if-nez v1, :cond_0

    .line 493
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a3

    .line 494
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070495

    .line 495
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 493
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 499
    :goto_0
    return-object v1

    .line 496
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v1, v2, :cond_1

    .line 497
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 499
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    move-result-object v0

    return-object v0
.end method

.method public getSearchTerm()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public getTypingData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getTypingData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserIsMemberOf()Z
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 306
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWhoCanChat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 323
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 324
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 325
    .local v1, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    :cond_0
    return-object v2

    .end local v1    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_1
    move-object v1, v2

    .line 324
    goto :goto_0
.end method

.method public getWhoCanSetChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 335
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 336
    .local v1, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    :cond_0
    return-object v2

    .end local v1    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_1
    move-object v1, v2

    .line 335
    goto :goto_0
.end method

.method public hoverChatIsOpen()Z
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isSearchingService:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isLoadingMoreHistory:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lazyQueryPersonSummary(Ljava/lang/String;)V
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 668
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lazyQueryPersonSummary - xuid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 672
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->queryXuids:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 673
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->lazyQuery(Ljava/lang/String;)V

    .line 674
    return-void
.end method

.method public loadMoreHistory()V
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryAsync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isLoadingMoreHistory:Z

    .line 735
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapter()V

    .line 737
    :cond_0
    return-void
.end method

.method public loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 557
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 558
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    .line 559
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 560
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->ClubPresence:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 565
    :cond_1
    return-void
.end method

.method public navigateToEnforcement(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;ZLjava/lang/Long;)V
    .locals 12
    .param p1, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "shouldReportToClubModerator"    # Z
    .param p3, "clubId"    # Ljava/lang/Long;

    .prologue
    .line 630
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 631
    iget-object v1, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 633
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 634
    if-eqz p2, :cond_1

    .line 635
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubMessageReportedToAdmins(J)V

    .line 643
    :cond_0
    :goto_0
    new-instance v7, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 644
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    .line 645
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-wide v4, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    invoke-direct {v7, v2, v1, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 648
    .local v7, "clubChatModerationEvidenceId":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsChat:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 650
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_3

    .line 651
    invoke-virtual {p3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    iget-object v4, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "chatfd.xboxlive.com/channels/%s/%s/messages/%d"

    const/4 v5, 0x3

    new-array v9, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v10, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 657
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->name()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v5

    const/4 v10, 0x1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    .line 658
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;->getId()Ljava/lang/String;

    move-result-object v5

    :goto_3
    aput-object v5, v9, v10

    const/4 v5, 0x2

    iget-wide v10, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    .line 659
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v5

    .line 654
    invoke-static {v6, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 661
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatEditTopicReport(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 663
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 664
    return-void

    .line 637
    .end local v0    # "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    .end local v7    # "clubChatModerationEvidenceId":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubMessageReportedToXbox(J)V

    goto :goto_0

    .line 645
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 651
    .restart local v7    # "clubChatModerationEvidenceId":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;
    :cond_3
    iget-object v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    goto :goto_2

    .line 658
    :cond_4
    const-string v5, ""

    goto :goto_3
.end method

.method public navigateToNotificationSettings()V
    .locals 4

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubSettingsSelected(J)V

    .line 622
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    .line 624
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;J)V

    .line 625
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ClubChatNotificationScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 626
    return-void
.end method

.method public navigateToUserProfile(Ljava/lang/String;)V
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 689
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 691
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->viewProfileFromClubSelected(J)V

    .line 695
    :cond_0
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 696
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 610
    const/16 v0, 0x4d2

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->openAsHoverChatInternal()V

    .line 615
    :goto_0
    return-void

    .line 613
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 8
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 824
    sget-object v5, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onClubModelChanged (STATUS: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    sget-object v5, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 854
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapter()V

    .line 855
    return-void

    .line 830
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 831
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 833
    .local v2, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 834
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v5, v6, :cond_2

    move v1, v3

    .line 835
    .local v1, "clubSuspended":Z
    :goto_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateMemberDataFromPresence(Ljava/util/List;)V

    .line 836
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateCanViewerViewChatIsNecessary(Z)V

    .line 837
    if-nez v1, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v5

    if-eqz v5, :cond_3

    move v5, v3

    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerChat:Ljava/lang/Boolean;

    .line 838
    if-nez v1, :cond_4

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v3

    :goto_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerSetChatTopic:Ljava/lang/Boolean;

    .line 839
    if-nez v1, :cond_5

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v5, v6}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :goto_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerDeleteMessage:Ljava/lang/Boolean;

    .line 841
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerViewChat:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 842
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "onClubModelChanged - user is not allowed to view chat"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 831
    .end local v1    # "clubSuspended":Z
    .end local v2    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_1

    .restart local v2    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_2
    move v1, v4

    .line 834
    goto :goto_2

    .restart local v1    # "clubSuspended":Z
    :cond_3
    move v5, v4

    .line 837
    goto :goto_3

    :cond_4
    move v5, v4

    .line 838
    goto :goto_4

    :cond_5
    move v3, v4

    .line 839
    goto :goto_5

    .line 850
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubSuspended":Z
    .end local v2    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :pswitch_1
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Club model changed to invalid state. Using old club data."

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 826
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 547
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onDestroy()V

    .line 548
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 505
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubChatScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 506
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 512
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Chat:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 513
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->setCurrentDisplayedChannel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 515
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 2

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getIsActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->setCurrentDisplayedChannel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 521
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetInactive()V

    .line 523
    :cond_0
    return-void
.end method

.method public onStartOverride()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 528
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStartOverride()V

    .line 530
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 531
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    .line 532
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateTyping:Z

    .line 533
    return-void
.end method

.method public onStopOverride()V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 538
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    .line 539
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopActiveTasks()V

    .line 541
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 542
    return-void
.end method

.method public openAsHoverChat()V
    .locals 2

    .prologue
    .line 592
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->openAsHoverChatInternal()V

    .line 597
    :goto_0
    return-void

    .line 595
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const/16 v1, 0x4d2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->requestDrawOverlayViewPermission(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method public restoreSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 450
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 452
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 454
    .local v1, "wnd":Landroid/view/Window;
    iget v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->oldInputMode:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 456
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method public saveAndAdjustSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 440
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 442
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 444
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->oldInputMode:I

    .line 445
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 447
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method public sendChatMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 416
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 418
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->clubMessageSent(J)V

    .line 424
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 425
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageFromLocal(Ljava/lang/String;)V

    .line 429
    :goto_1
    return-void

    .line 421
    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatSendMessage(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    goto :goto_0

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->formatMessageFromService(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public sendIsTypingMessage()V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->sendIsTypingMessage()V

    .line 413
    return-void
.end method

.method public serviceSearchEnabled()Z
    .locals 6

    .prologue
    .line 370
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 371
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMessageOfTheDay(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 432
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 434
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatModel:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->setMessageOfTheDay(Ljava/lang/String;)V

    .line 436
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->dismissClubChatTopicDialog()V

    .line 437
    return-void
.end method

.method public setSearchTerm(Ljava/lang/String;)V
    .locals 5
    .param p1, "searchTerm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 568
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 570
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->stopSearchTask()V

    .line 572
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchTerm:Ljava/lang/String;

    .line 573
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isSearchingService:Z

    .line 576
    monitor-enter p0

    .line 577
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 578
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    new-instance v0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->directMentionSearchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 581
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->load(Z)V

    .line 584
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->updateAdapter()V

    .line 585
    return-void

    .line 578
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public shouldLoadMoreHistory()Z
    .locals 1

    .prologue
    .line 740
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->isLoadingMoreHistory:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldScrollToBottom()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldScrollToBottom:Z

    return v0
.end method

.method public shouldShowHoverChatOption()Z
    .locals 1

    .prologue
    .line 605
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canRequestDrawOverlayViews()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldUpdateAllMessages()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateAllMessages:Z

    return v0
.end method

.method public shouldUpdateHistoryMessages()Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateHistoryMessages:Z

    return v0
.end method

.method public shouldUpdateTyping()Z
    .locals 1

    .prologue
    .line 400
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldUpdateTyping:Z

    return v0
.end method

.method public showClubChatEditReportDialog()V
    .locals 4

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->chatHeadTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageOfTheDaySelected(J)V

    .line 481
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubChatEditReportDialog(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 482
    return-void
.end method

.method public showClubChatTopicDialog()V
    .locals 1

    .prologue
    .line 461
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatEditTopic(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 463
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubChatTopicDialog(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 466
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatEditTopicPageView(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 467
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 810
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v1

    .line 811
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    .line 812
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 819
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 820
    return-void

    .line 810
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 814
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->onLazyQueryPersonSummaryCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_1

    .line 812
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
