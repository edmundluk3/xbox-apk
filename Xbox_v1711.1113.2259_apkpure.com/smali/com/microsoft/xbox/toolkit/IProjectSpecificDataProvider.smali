.class public interface abstract Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;
.super Ljava/lang/Object;
.source "IProjectSpecificDataProvider.java"


# virtual methods
.method public abstract getAllowExplicitContent()Z
.end method

.method public abstract getAutoSuggestdDataSource()Ljava/lang/String;
.end method

.method public abstract getCombinedContentRating()Ljava/lang/String;
.end method

.method public abstract getContentRestrictions()Ljava/lang/String;
.end method

.method public abstract getCurrentSandboxID()Ljava/lang/String;
.end method

.method public abstract getDeviceId()Ljava/lang/String;
.end method

.method public abstract getInitializeComplete()Z
.end method

.method public abstract getIsForXboxOne()Z
.end method

.method public abstract getIsFreeAccount()Z
.end method

.method public abstract getIsXboxMusicSupported()Z
.end method

.method public abstract getLegalLocale()Ljava/lang/String;
.end method

.method public abstract getMembershipLevel()Ljava/lang/String;
.end method

.method public abstract getRegion()Ljava/lang/String;
.end method

.method public abstract getUseEDS31()Z
.end method

.method public abstract getVersionCheckUrl()Ljava/lang/String;
.end method

.method public abstract getVersionCode()I
.end method

.method public abstract getWindowsLiveClientId()Ljava/lang/String;
.end method

.method public abstract getXuidString()Ljava/lang/String;
.end method

.method public abstract resetModels(Z)V
.end method

.method public abstract resetTokenForService()V
.end method
