.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "HoverChatMenuScreen.java"


# instance fields
.field telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 25
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V

    .line 31
    return-void
.end method

.method static synthetic lambda$onCreate$0(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->inboxSettingsSelected()V

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;

    const-class v2, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 60
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->onCreateContentView()V

    .line 41
    const v1, 0x7f0e0707

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .local v0, "pivotWithTabs":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 43
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->trackInboxTabPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->MENU_TAB_NAMES:[Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->MENU_TAB_ACTIONS:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetryTabSelectCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, v1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->trackInboxTabResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v2, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->MENU_TAB_NAMES:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetrySetActiveTabCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 46
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 47
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderVisibility(Z)V

    .line 48
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBehaviorEnabled(Z)V

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 52
    const v1, 0x7f0e04a8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->xleFindViewId(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 53
    const v1, 0x7f0e04a7

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->xleFindViewId(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0201d1

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 54
    const v1, 0x7f0201d3

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundResource(I)V

    .line 56
    const v1, 0x7f0e0708

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f03014c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;->setContentView(I)V

    .line 66
    return-void
.end method
