.class public Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;
.super Landroid/view/View$AccessibilityDelegate;
.source "ActivityFeedViewAccessibilityDelegate.java"


# static fields
.field private static focusedView:Landroid/view/View;

.field private static instance:Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;

.field private static isAccessibilityEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->focusedView:Landroid/view/View;

    .line 23
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isAccessibilityEnabled:Z

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->instance:Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method

.method public static attachActivityFeedViewAccessibilityDelegate(Landroid/view/View;)V
    .locals 1
    .param p0, "activityFeedView"    # Landroid/view/View;

    .prologue
    .line 74
    sget-object v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->instance:Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;

    invoke-virtual {p0, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 75
    return-void
.end method

.method private static isAccessibilityEnabled()Z
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isAccessibilityEnabled:Z

    return v0
.end method

.method public static isViewAccessibilityFocused(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 55
    if-nez p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isAccessibilityEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 59
    const/4 v1, 0x0

    sput-object v1, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->focusedView:Landroid/view/View;

    goto :goto_0

    .line 62
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->focusedView:Landroid/view/View;

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setAccessibilityEnabled(Z)V
    .locals 0
    .param p0, "val"    # Z

    .prologue
    .line 70
    sput-boolean p0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isAccessibilityEnabled:Z

    .line 71
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "host"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    .line 31
    .local v0, "eventType":I
    if-nez v0, :cond_0

    .line 38
    :goto_0
    return v1

    .line 33
    :cond_0
    const v2, 0x8000

    if-ne v0, v2, :cond_1

    .line 34
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 35
    sput-object p1, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->focusedView:Landroid/view/View;

    goto :goto_0

    .line 38
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View$AccessibilityDelegate;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "host"    # Landroid/view/ViewGroup;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 47
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const v1, 0x8000

    if-ne v0, v1, :cond_0

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->focusedView:Landroid/view/View;

    .line 50
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View$AccessibilityDelegate;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method
