.class Lorg/simpleframework/xml/core/ModelMap;
.super Ljava/util/LinkedHashMap;
.source "ModelMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lorg/simpleframework/xml/core/ModelList;",
        ">;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/simpleframework/xml/core/ModelList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 47
    return-void
.end method

.method private build(Lorg/simpleframework/xml/stream/Style;)Lorg/simpleframework/xml/core/ModelMap;
    .locals 6
    .param p1, "style"    # Lorg/simpleframework/xml/stream/Style;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v3, Lorg/simpleframework/xml/core/ModelMap;

    invoke-direct {v3}, Lorg/simpleframework/xml/core/ModelMap;-><init>()V

    .line 132
    .local v3, "map":Lorg/simpleframework/xml/core/ModelMap;
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/ModelMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    .local v0, "element":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/simpleframework/xml/core/ModelMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simpleframework/xml/core/ModelList;

    .line 134
    .local v2, "list":Lorg/simpleframework/xml/core/ModelList;
    invoke-interface {p1, v0}, Lorg/simpleframework/xml/stream/Style;->getElement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "name":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 137
    invoke-virtual {v2}, Lorg/simpleframework/xml/core/ModelList;->build()Lorg/simpleframework/xml/core/ModelList;

    move-result-object v2

    .line 139
    :cond_0
    invoke-virtual {v3, v4, v2}, Lorg/simpleframework/xml/core/ModelMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 141
    .end local v0    # "element":Ljava/lang/String;
    .end local v2    # "list":Lorg/simpleframework/xml/core/ModelList;
    .end local v4    # "name":Ljava/lang/String;
    :cond_1
    return-object v3
.end method


# virtual methods
.method public build(Lorg/simpleframework/xml/core/Context;)Lorg/simpleframework/xml/core/ModelMap;
    .locals 1
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 111
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Context;->getStyle()Lorg/simpleframework/xml/stream/Style;

    move-result-object v0

    .line 113
    .local v0, "style":Lorg/simpleframework/xml/stream/Style;
    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0, v0}, Lorg/simpleframework/xml/core/ModelMap;->build(Lorg/simpleframework/xml/stream/Style;)Lorg/simpleframework/xml/core/ModelMap;

    move-result-object p0

    .line 116
    .end local p0    # "this":Lorg/simpleframework/xml/core/ModelMap;
    :cond_0
    return-object p0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/simpleframework/xml/core/ModelList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/ModelMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lookup(Ljava/lang/String;I)Lorg/simpleframework/xml/core/Model;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lorg/simpleframework/xml/core/ModelMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/ModelList;

    .line 63
    .local v0, "list":Lorg/simpleframework/xml/core/ModelList;
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0, p2}, Lorg/simpleframework/xml/core/ModelList;->lookup(I)Lorg/simpleframework/xml/core/Model;

    move-result-object v1

    .line 66
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public register(Ljava/lang/String;Lorg/simpleframework/xml/core/Model;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "model"    # Lorg/simpleframework/xml/core/Model;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lorg/simpleframework/xml/core/ModelMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/ModelList;

    .line 81
    .local v0, "list":Lorg/simpleframework/xml/core/ModelList;
    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lorg/simpleframework/xml/core/ModelList;

    .end local v0    # "list":Lorg/simpleframework/xml/core/ModelList;
    invoke-direct {v0}, Lorg/simpleframework/xml/core/ModelList;-><init>()V

    .line 83
    .restart local v0    # "list":Lorg/simpleframework/xml/core/ModelList;
    invoke-virtual {p0, p1, v0}, Lorg/simpleframework/xml/core/ModelMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    :cond_0
    invoke-virtual {v0, p2}, Lorg/simpleframework/xml/core/ModelList;->register(Lorg/simpleframework/xml/core/Model;)V

    .line 86
    return-void
.end method
