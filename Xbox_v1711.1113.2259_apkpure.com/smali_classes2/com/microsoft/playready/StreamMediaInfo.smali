.class final Lcom/microsoft/playready/StreamMediaInfo;
.super Ljava/lang/Object;
.source "StreamMediaInfo.java"

# interfaces
.implements Lcom/microsoft/playready/MediaInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;I)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 47
    iput p2, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    .line 48
    return-void
.end method


# virtual methods
.method public getAudioInfo()Lcom/microsoft/playready/AudioInfo;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/microsoft/playready/StreamAudioInfo;

    iget-object v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/StreamAudioInfo;-><init>(Lcom/microsoft/playready/Native_Class10;I)V

    return-object v0
.end method

.method public getFourCC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 28
    iget-object v2, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v3, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-virtual {v2, v3}, Lcom/microsoft/playready/Native_Class10;->method_34(I)I

    move-result v1

    .line 29
    .local v1, "fourccInt":I
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 30
    .local v0, "fourccByte":[B
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    return-object v2
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_36(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_37(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSchemeId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_35(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/microsoft/playready/MediaInfo$MediaType;
    .locals 3

    .prologue
    .line 24
    invoke-static {}, Lcom/microsoft/playready/MediaInfo$MediaType;->values()[Lcom/microsoft/playready/MediaInfo$MediaType;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-virtual {v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_33(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getVideoInfo()Lcom/microsoft/playready/VideoInfo;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/microsoft/playready/StreamVideoInfo;

    iget-object v1, p0, Lcom/microsoft/playready/StreamMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/StreamMediaInfo;->mStreamIndex:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/StreamVideoInfo;-><init>(Lcom/microsoft/playready/Native_Class10;I)V

    return-object v0
.end method
