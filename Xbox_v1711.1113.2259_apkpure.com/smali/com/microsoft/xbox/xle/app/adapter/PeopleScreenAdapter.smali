.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "PeopleScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private currentFilteredPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersFilter;",
            ">;"
        }
    .end annotation
.end field

.field languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

.field private listHeaderControlsLayout:Landroid/widget/FrameLayout;

.field private listOrGridView:Landroid/widget/AbsListView;

.field private noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private showFilteredList:Z

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 59
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 69
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V

    .line 71
    const v0, 0x7f0e08ee

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->screenBody:Landroid/view/View;

    .line 72
    const v0, 0x7f0e08f7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 73
    const v0, 0x7f0e08f9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 75
    const v0, 0x7f0e08f1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07058b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 80
    :cond_0
    const v0, 0x7f0e08f0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 82
    const v0, 0x7f0e08f2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 86
    const v0, 0x7f0e08f8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_1

    .line 90
    new-instance v0, Landroid/widget/FrameLayout;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listHeaderControlsLayout:Landroid/widget/FrameLayout;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listHeaderControlsLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->addHeaderView(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->screenBody:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->screenBody:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getTabletBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 97
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 140
    const v0, 0x7f0e08f6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    .line 142
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    invoke-static {}, Lcom/microsoft/xbox/service/model/FollowersFilter;->values()[Lcom/microsoft/xbox/service/model/FollowersFilter;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilter()Lcom/microsoft/xbox/service/model/FollowersFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_4

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 220
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_5

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    .line 226
    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->prefs()Lio/reactivex/Observable;

    move-result-object v1

    .line 227
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 228
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 229
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/widget/AbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->showFilteredList:Z

    return p1
.end method

.method private clear()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onSearchBarClear()V

    .line 267
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->dismissKeyboard()V

    .line 101
    move v1, p3

    .line 102
    .local v1, "itemIndex":I
    instance-of v2, p1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v2, :cond_1

    .line 103
    if-nez v1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 107
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 110
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 112
    .local v0, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$3;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getType()Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 137
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->setLastSelectedPosition(I)V

    goto :goto_0

    .line 114
    :pswitch_0
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackNavigateToProfile(Ljava/lang/String;)V

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    goto :goto_1

    .line 119
    :pswitch_1
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackPeopleNavigateToClub(JI)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    goto :goto_1

    .line 124
    :pswitch_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_FACEBOOK:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_3

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackFindFacebookFriendsClicked()V

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->login()V

    goto :goto_1

    .line 127
    :cond_3
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_PHONE_CONTACT:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_4

    .line 128
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackFindPhoneContactsClicked()V

    .line 129
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->linkUnlink()V

    goto :goto_1

    .line 130
    :cond_4
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_SEARCH_TAG:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_2

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getQueryText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertag(Ljava/lang/String;)V

    goto :goto_1

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "queryText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertag(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 208
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v3

    .line 212
    :cond_1
    const-string v0, "SearchBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "actionid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v0, 0x3

    if-eq p2, v0, :cond_2

    if-nez p2, :cond_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertag(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->clear()V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->relocalizeView()V

    return-void
.end method

.method private relocalizeView()V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->load(Z)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07058b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilter()Lcom/microsoft/xbox/service/model/FollowersFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 281
    :cond_2
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->updateLoadingIndicator(Z)V

    .line 235
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredNoContentText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 238
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->showFilteredList:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredPeopleBasedOnSearchText()Ljava/util/List;

    move-result-object v0

    .line 239
    .local v0, "newFilteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->currentFilteredPeople:Ljava/util/List;

    if-eq v1, v0, :cond_1

    .line 240
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->clear()V

    .line 241
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->addAll(Ljava/util/Collection;)V

    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;->onDataUpdated()V

    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getLastSelectedPosition()I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getLastSelectedPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 247
    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->currentFilteredPeople:Ljava/util/List;

    .line 249
    :cond_1
    return-void

    .line 238
    .end local v0    # "newFilteredData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredPeople()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
