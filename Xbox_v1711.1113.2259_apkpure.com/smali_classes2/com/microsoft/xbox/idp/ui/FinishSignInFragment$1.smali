.class Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;
.super Ljava/lang/Object;
.source "FinishSignInFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Creating loader_finish_sign_in"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v0, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader;

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "ARG_AUTH_STATUS"

    .line 128
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/idp/interop/Interop$AuthFlowScreenStatus;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/Interop$AuthFlowScreenStatus;

    move-result-object v2

    const-string v3, "ARG_CID"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;

    .line 129
    invoke-static {v4}, Lcom/microsoft/xbox/idp/util/CacheUtil;->getResultCache(Ljava/lang/Class;)Lcom/microsoft/xbox/idp/util/ResultCache;

    move-result-object v4

    const-string v5, "KEY_RESULT_KEY"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/idp/interop/Interop$AuthFlowScreenStatus;Ljava/lang/String;Lcom/microsoft/xbox/idp/util/ResultCache;Ljava/lang/Object;)V

    .line 127
    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;)V
    .locals 3
    .param p2, "result"    # Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;",
            ">;",
            "Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;",
            ")V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;>;"
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "loader_finish_sign_in finished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loader_finish_sign_in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;->getError()Lcom/microsoft/xbox/idp/toolkit/HttpError;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;

    invoke-static {v0}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$100(Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;)Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$State;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget-object v1, Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;->CATCHALL:Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->startErrorActivity(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;)V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;

    invoke-static {v0}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$200(Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;)Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$Callbacks;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$Status;->SUCCESS:Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$Status;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$Callbacks;->onComplete(Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$Status;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 123
    check-cast p2, Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment$1;->onLoadFinished(Landroid/support/v4/content/Loader;Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Lcom/microsoft/xbox/idp/toolkit/FinishSignInLoader$Result;>;"
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/FinishSignInFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "loader_finish_sign_in reset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    return-void
.end method
