.class public Lcom/microsoft/xbox/smartglass/CatalogServiceManager;
.super Ljava/lang/Object;
.source "CatalogServiceManager.java"


# instance fields
.field private final _pCatalogServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 1
    .param p1, "pCatalogServiceManager"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->_pCatalogServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 13
    return-void
.end method

.method private native getItemDetails(JILjava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getTitleDetails(JILcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native setCulture(JLjava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public getItemDetails(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .locals 7
    .param p1, "titleId"    # I
    .param p2, "assetId"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/microsoft/xbox/smartglass/ServiceRequestListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->_pCatalogServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->getItemDetails(JILjava/lang/String;Lcom/microsoft/xbox/smartglass/ServiceRequestListener;)V

    .line 40
    return-void
.end method

.method public getTitleDetails(ILcom/microsoft/xbox/smartglass/ServiceRequestListener;)V
    .locals 2
    .param p1, "titleId"    # I
    .param p2, "listener"    # Lcom/microsoft/xbox/smartglass/ServiceRequestListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->_pCatalogServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->getTitleDetails(JILcom/microsoft/xbox/smartglass/ServiceRequestListener;)V

    .line 30
    return-void
.end method

.method public setCulture(Ljava/lang/String;)V
    .locals 2
    .param p1, "culture"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->_pCatalogServiceManager:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/CatalogServiceManager;->setCulture(JLjava/lang/String;)J

    .line 21
    return-void
.end method
