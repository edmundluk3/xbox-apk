.class Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "ClubRequestListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubRequestListItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private inclusionReason:Landroid/widget/TextView;

.field private realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    .line 120
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 122
    const v0, 0x7f0e0292

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 123
    const v0, 0x7f0e0293

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 124
    const v0, 0x7f0e0294

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 125
    const v0, 0x7f0e0295

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->inclusionReason:Landroid/widget/TextView;

    .line 126
    const v0, 0x7f0e0296

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 127
    const v0, 0x7f0e0291

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 128
    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;->onAcceptClick(J)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;->onIgnoreClick(J)V

    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getXuid()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$RequestsClickListener;->onProfileClick(J)V

    return-void
.end method

.method static synthetic lambda$onBind$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 144
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->removeSelfDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$onBind$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 145
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->removeSelfDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method private removeSelfDecorator(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 149
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->getAdapterPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->remove(I)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter;->notifyItemRemoved(I)V

    .line 154
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;)V
    .locals 5
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v4, 0x7f020125

    .line 132
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    move-object v0, p1

    .line 134
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    .line 136
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->itemView:Landroid/view/View;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getGamertag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getReason()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getRealName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->inclusionReason:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 111
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubRequestListAdapter$ClubRequestListItemViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;)V

    return-void
.end method
