.class Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "ChooseProfileColorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChooseProfileColorDialogAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 88
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getProfileColorList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v0

    int-to-long v0, v0

    .line 106
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 112
    move-object v1, p2

    .line 113
    .local v1, "colorView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 115
    .local v2, "itemColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-nez v1, :cond_0

    .line 116
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030054

    invoke-virtual {v7, v8, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 119
    :cond_0
    new-instance v7, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;

    invoke-direct {v7, p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070d71

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    add-int/lit8 v9, p1, 0x1

    .line 128
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    .line 127
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "color":I
    if-eqz v2, :cond_1

    .line 133
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v0

    .line 135
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 138
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getWidth()I

    move-result v7

    int-to-double v8, v7

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    div-double/2addr v8, v10

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    int-to-double v10, v7

    sub-double/2addr v8, v10

    double-to-int v4, v8

    .line 139
    .local v4, "viewSize":I
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 140
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 141
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    if-eqz v2, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v7

    invoke-interface {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v7

    invoke-interface {v7}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_0
    invoke-virtual {v1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 146
    return-object v1

    :cond_2
    move v5, v6

    .line 144
    goto :goto_0
.end method
