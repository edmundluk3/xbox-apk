.class public Lcom/microsoft/xbox/xle/ui/EPGViewConfig;
.super Ljava/lang/Object;
.source "EPGViewConfig.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SimpleDateFormat"
    }
.end annotation


# static fields
.field public static FADE_IN_DURATION:J

.field public static SHOW_CHANNEL_LOGO_ON_VERTICAL_FLING:Z

.field public static SHOW_PROGRAM_TITLE_ON_VERTICAL_FLING:Z

.field private static mSharedInstance:Lcom/microsoft/xbox/xle/ui/EPGViewConfig;


# instance fields
.field private mCalendar:Ljava/util/Calendar;

.field private mContext:Landroid/content/Context;

.field private mDateFormat:Ljava/text/DateFormat;

.field private mTimeFormat:Ljava/text/DateFormat;

.field private mWeekdayFormat:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->SHOW_PROGRAM_TITLE_ON_VERTICAL_FLING:Z

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->SHOW_CHANNEL_LOGO_ON_VERTICAL_FLING:Z

    .line 49
    const-wide/16 v0, 0x96

    sput-wide v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->FADE_IN_DURATION:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mCalendar:Ljava/util/Calendar;

    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mContext:Landroid/content/Context;

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->refreshTimeAndDateFormatting()V

    .line 56
    return-void
.end method

.method public static getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mSharedInstance:Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    return-object v0
.end method

.method public static setSharedInstance(Lcom/microsoft/xbox/xle/ui/EPGViewConfig;)V
    .locals 0
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    .prologue
    .line 138
    sput-object p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mSharedInstance:Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    .line 139
    return-void
.end method


# virtual methods
.method public calendar()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mCalendar:Ljava/util/Calendar;

    return-object v0
.end method

.method public formatDay(Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    const/4 v4, 0x6

    .line 67
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v3, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 68
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 69
    .local v0, "day":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 70
    .local v2, "today":I
    sub-int v1, v0, v2

    .line 71
    .local v1, "diff":I
    if-nez v1, :cond_0

    .line 72
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mContext:Landroid/content/Context;

    const v4, 0x7f07048f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 77
    :goto_0
    return-object v3

    .line 74
    :cond_0
    if-lez v1, :cond_1

    const/4 v3, 0x7

    if-ge v1, v3, :cond_1

    .line 75
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mWeekdayFormat:Ljava/text/DateFormat;

    invoke-virtual {v3, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 77
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mDateFormat:Ljava/text/DateFormat;

    invoke-virtual {v3, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public formatDuration(I)Ljava/lang/String;
    .locals 13
    .param p1, "seconds"    # I

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 85
    if-nez p1, :cond_1

    .line 86
    const/4 v3, 0x0

    .line 118
    :cond_0
    :goto_0
    return-object v3

    .line 89
    :cond_1
    rem-int/lit8 v2, p1, 0x3c

    .line 90
    .local v2, "currentSeconds":I
    div-int/lit8 v6, p1, 0x3c

    rem-int/lit8 v1, v6, 0x3c

    .line 91
    .local v1, "currentMinutes":I
    div-int/lit16 v0, p1, 0xe10

    .line 93
    .local v0, "currentHours":I
    if-ne v2, v10, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070626

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "secondsString":Ljava/lang/String;
    :goto_1
    if-ne v1, v10, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070622

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, "minutesString":Ljava/lang/String;
    :goto_2
    if-ne v0, v10, :cond_4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07061f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "hoursString":Ljava/lang/String;
    :goto_3
    if-lez v0, :cond_5

    .line 103
    if-lez v1, :cond_0

    .line 104
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070620

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v3, v8, v11

    aput-object v4, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 93
    .end local v3    # "hoursString":Ljava/lang/String;
    .end local v4    # "minutesString":Ljava/lang/String;
    .end local v5    # "secondsString":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070627

    .line 94
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    .line 93
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 95
    .restart local v5    # "secondsString":Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070624

    .line 96
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    .line 95
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 97
    .restart local v4    # "minutesString":Ljava/lang/String;
    :cond_4
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070621

    .line 98
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    .line 97
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 108
    .restart local v3    # "hoursString":Ljava/lang/String;
    :cond_5
    if-lez v1, :cond_7

    .line 110
    if-lez v2, :cond_6

    .line 111
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070623

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v4, v8, v11

    aput-object v5, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_6
    move-object v3, v4

    .line 113
    goto/16 :goto_0

    :cond_7
    move-object v3, v5

    .line 118
    goto/16 :goto_0
.end method

.method public formatTime(Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mTimeFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public locale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public refreshTimeAndDateFormatting()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mContext:Landroid/content/Context;

    const v2, 0x7f070e7d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mWeekdayFormat:Ljava/text/DateFormat;

    .line 124
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mContext:Landroid/content/Context;

    const v2, 0x7f070e79

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mDateFormat:Ljava/text/DateFormat;

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->mTimeFormat:Ljava/text/DateFormat;

    .line 126
    return-void
.end method
