.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;
.super Ljava/lang/Object;
.source "UTCImpression.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->trackImpressionEvents(Ljava/lang/String;JLjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$pageName:Ljava/lang/String;

.field final synthetic val$profileRecentItems:Ljava/util/ArrayList;

.field final synthetic val$resolutionHeight:J


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$profileRecentItems:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$pageName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$resolutionHeight:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 12

    .prologue
    .line 38
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 39
    .local v5, "impressionEvents":Lorg/json/JSONArray;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$profileRecentItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 40
    .local v6, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v6, :cond_0

    .line 42
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 43
    .local v4, "impressionEvent":Lorg/json/JSONObject;
    const-string v9, "ContentId"

    iget-object v7, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    :goto_1
    invoke-virtual {v4, v9, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string v9, "ContentSource"

    iget-object v7, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    if-eqz v7, :cond_2

    iget-object v7, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->access$000(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;)Ljava/lang/String;

    move-result-object v7

    :goto_2
    invoke-virtual {v4, v9, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    const-string v7, "ContentType"

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->access$100(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v7, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    const-string v7, "CommentCount"

    iget-wide v10, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numComments:J

    invoke-virtual {v4, v7, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 47
    const-string v7, "LikeCount"

    iget-wide v10, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numLikes:J

    invoke-virtual {v4, v7, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 48
    const-string v7, "ShareCount"

    iget-wide v10, v6, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->numShares:J

    invoke-virtual {v4, v7, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 49
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    .end local v4    # "impressionEvent":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 43
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v4    # "impressionEvent":Lorg/json/JSONObject;
    :cond_1
    :try_start_1
    const-string v7, "UNKNOWN"

    goto :goto_1

    .line 44
    :cond_2
    const-string v7, "UNKNOWN"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 55
    .end local v4    # "impressionEvent":Lorg/json/JSONObject;
    .end local v6    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_3
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 57
    .local v3, "impressionArrayJson":Lorg/json/JSONObject;
    :try_start_2
    const-string v7, "impressionArray"

    invoke-virtual {v3, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 61
    :goto_3
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "impressionArray":Ljava/lang/String;
    new-instance v1, Lxbox/smartglass/Impression;

    invoke-direct {v1}, Lxbox/smartglass/Impression;-><init>()V

    .line 64
    .local v1, "impression":Lxbox/smartglass/Impression;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$pageName:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lxbox/smartglass/Impression;->setPageName(Ljava/lang/String;)V

    .line 65
    iget-wide v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$resolutionHeight:J

    long-to-int v7, v8

    invoke-virtual {v1, v7}, Lxbox/smartglass/Impression;->setResolutionHeight(I)V

    .line 66
    invoke-virtual {v1, v2}, Lxbox/smartglass/Impression;->setImpressionArray(Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->access$200()I

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v7

    invoke-virtual {v1, v7}, Lxbox/smartglass/Impression;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 70
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 72
    const-string v7, "ImpressionEvents - pageName: %s : Data:%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression$1;->val$pageName:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v2, v8, v9

    invoke-static {v7, v8}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    return-void

    .line 58
    .end local v1    # "impression":Lxbox/smartglass/Impression;
    .end local v2    # "impressionArray":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 59
    .restart local v0    # "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method
