.class public Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEListView;
.source "XLEVirtualizedListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;
    }
.end annotation


# static fields
.field private static final BLOCKS_RESIDENT_IN_RAM:I = 0x2

.field private static final ELEMENTS_PER_BLOCK:I = 0xa


# instance fields
.field private basemultiple:I

.field private footer:Landroid/view/View;

.field private header:Landroid/view/View;

.field private isLoadingMore:Z

.field private loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

.field private size:I

.field private stageddata:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->setVerticalScrollBarEnabled(Z)V

    .line 48
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->setHorizontalScrollBarEnabled(Z)V

    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->size:I

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->stageddata:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    .prologue
    .line 21
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    return v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    return p1
.end method

.method static synthetic access$308(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    .prologue
    .line 21
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    return v0
.end method

.method static synthetic access$310(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    .prologue
    .line 21
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    return v0
.end method

.method private inRange(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 294
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getSize()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v9, 0x7f0e077e

    const v8, 0x7f030162

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 54
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 57
    .local v2, "vi":Landroid/view/LayoutInflater;
    invoke-virtual {v2, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "headerLayout":Landroid/view/View;
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->header:Landroid/view/View;

    .line 59
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->header:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 60
    invoke-virtual {p0, v1, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 61
    invoke-virtual {v2, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "footerLayout":Landroid/view/View;
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->footer:Landroid/view/View;

    .line 63
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->footer:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-virtual {p0, v0, v6, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 66
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 68
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    .line 69
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->stageddata:Ljava/util/ArrayList;

    .line 70
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->stageddata:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 71
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->size:I

    .line 72
    return-void
.end method


# virtual methods
.method public ensureMaxLoaded(I)Z
    .locals 1
    .param p1, "postlast"    # I

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMax()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->inRange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ensureMinLoaded(I)Z
    .locals 1
    .param p1, "prefirst"    # I

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMin()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->inRange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    return v0
.end method

.method public getMax()I
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public getMin()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    mul-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->size:I

    return v0
.end method

.method public getStagedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->stageddata:Ljava/util/ArrayList;

    return-object v0
.end method

.method public load(II)Ljava/util/List;
    .locals 3
    .param p1, "base"    # I
    .param p2, "count"    # I

    .prologue
    .line 279
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 281
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    if-nez v1, :cond_0

    .line 282
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 286
    :goto_0
    return-object v1

    .line 285
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getSize()I

    move-result v1

    add-int v2, p1, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 286
    .local v0, "max":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    invoke-interface {v1, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;->load(II)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 6

    .prologue
    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 86
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, p0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V

    .line 107
    .local v3, "setSizeTask":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMin()I

    move-result v2

    .line 108
    .local v2, "min":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMax()I

    move-result v1

    .line 110
    .local v1, "max":I
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p0, v4, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;II)V

    .line 132
    .local v0, "loadInitialTask":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/util/List<Ljava/lang/Object;>;>;"
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/microsoft/xbox/toolkit/XLEAsyncTask;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->executeAll([Lcom/microsoft/xbox/toolkit/XLEAsyncTask;)V

    .line 133
    return-void
.end method

.method public onLoadNextDone()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->footer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    .line 264
    return-void
.end method

.method public onLoadPrevDone()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->header:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    .line 259
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 147
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 6
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 151
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    if-nez v3, :cond_1

    if-nez p2, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getFirstVisiblePosition()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->visibleIndexToRealIndex(I)I

    move-result v1

    .line 156
    .local v1, "prefirst":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getLastVisiblePosition()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->visibleIndexToRealIndex(I)I

    move-result v0

    .line 158
    .local v0, "postlast":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->ensureMinLoaded(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    .line 160
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->header:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_0

    .line 165
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V

    .line 204
    .local v2, "task":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/util/List<Ljava/lang/Object;>;>;"
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 209
    .end local v2    # "task":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/util/List<Ljava/lang/Object;>;>;"
    :cond_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->ensureMaxLoaded(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->isLoadingMore:Z

    .line 211
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->footer:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getSize()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    if-gt v3, v4, :cond_1

    .line 215
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$4;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V

    .line 250
    .restart local v2    # "task":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/util/List<Ljava/lang/Object;>;>;"
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;->execute()V

    .line 254
    .end local v0    # "postlast":I
    .end local v1    # "prefirst":I
    .end local v2    # "task":Lcom/microsoft/xbox/toolkit/XLEAsyncTask;, "Lcom/microsoft/xbox/toolkit/XLEAsyncTask<Ljava/util/List<Ljava/lang/Object;>;>;"
    :cond_1
    return-void
.end method

.method public realIndexToVisibleIndex(I)I
    .locals 1
    .param p1, "realIndex"    # I

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMin()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method public setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;)V
    .locals 1
    .param p1, "loadMoreListener_"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->basemultiple:I

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->stageddata:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->notifyDataSetChanged()V

    .line 142
    return-void
.end method

.method public visibleIndexToRealIndex(I)I
    .locals 1
    .param p1, "visibleIndex"    # I

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getMin()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method
