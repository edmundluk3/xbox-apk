.class public Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ExpandedAppBar.java"


# instance fields
.field private appBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;Landroid/content/Context;)V
    .locals 4
    .param p1, "appBarView"    # Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 28
    const v1, 0x7f08001e

    invoke-direct {p0, p2, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dialogBody:Landroid/view/View;

    .line 32
    const-string v3, "Make sure to remove the ApplicationBarView from its parent before passing it into ExpandedAppBar."

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v3, v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 34
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->setCancelable(Z)V

    .line 35
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 36
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->requestWindowFeature(I)Z

    .line 38
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 39
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 40
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->appBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    .line 43
    return-void

    .line 32
    .end local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .locals 3
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;
    .param p2, "goingBack"    # Z

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->getDialogBody()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v1

    const-string v2, "ExpandedAppbar"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->getDialogBody()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v0

    .line 65
    :cond_0
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->appBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isTouchPointInsideView(FFLandroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 55
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
