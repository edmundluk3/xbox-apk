.class Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "FutureShowtimesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFutureShowtimesDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$1;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefreshFutureShowtimes()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 123
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->forceLoad:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 127
    .local v2, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v0, "headendIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 130
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v4

    if-eq v1, v4, :cond_0

    .line 131
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    iget-object v4, v3, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->forceLoad:Z

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadFutureShowtimes(Z[Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 136
    .end local v0    # "headendIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    return-object v2
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->onLoadFutureShowtimesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 103
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->onLoadFutureShowtimesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 114
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 92
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel$LoadFutureShowtimesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;Z)Z

    .line 109
    return-void
.end method
