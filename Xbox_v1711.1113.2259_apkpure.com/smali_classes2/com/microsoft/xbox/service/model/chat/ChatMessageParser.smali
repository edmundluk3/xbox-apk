.class public final enum Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;
.super Ljava/lang/Enum;
.source "ChatMessageParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

.field private static final DIRECT_MENTION_TAG_NAME:Ljava/lang/String; = "at"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

.field private static final ROOT_END:Ljava/lang/String; = "</root>"

.field private static final ROOT_START:Ljava/lang/String; = "<root>"

.field private static final TAG:Ljava/lang/String;

.field private static final URL_ATTRIBUTE_NAME:Ljava/lang/String; = "href"

.field private static final URL_TAG_NAME:Ljava/lang/String; = "a"


# instance fields
.field private final xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    .line 20
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    .line 23
    const-class v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;

    return-object v0
.end method


# virtual methods
.method public parseMessage(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 14
    .param p1, "messageText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 35
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 38
    .local v5, "spannableStringBuilder":Landroid/text/SpannableStringBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<root>"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "</root>"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 41
    :try_start_0
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    new-instance v11, Ljava/io/StringReader;

    invoke-direct {v11, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 43
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 44
    .local v2, "event":I
    :goto_0
    if-eq v2, v10, :cond_3

    .line 45
    const/4 v8, 0x4

    if-ne v2, v8, :cond_1

    .line 47
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 81
    :cond_0
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 49
    :cond_1
    const/4 v8, 0x2

    if-ne v2, v8, :cond_0

    .line 53
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "tagName":Ljava/lang/String;
    const/4 v8, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v8, :pswitch_data_0

    goto :goto_1

    .line 57
    :pswitch_0
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 59
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "directMentionContent":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 62
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 63
    .local v1, "directMentionGamerTag":Landroid/text/SpannableString;
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0142

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-direct {v8, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v11, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v1, v8, v11, v12, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 64
    new-instance v8, Landroid/text/style/StyleSpan;

    const/4 v11, 0x1

    invoke-direct {v8, v11}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v11, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v1, v8, v11, v12, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 66
    invoke-virtual {v5, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 84
    .end local v0    # "directMentionContent":Ljava/lang/String;
    .end local v1    # "directMentionGamerTag":Landroid/text/SpannableString;
    .end local v2    # "event":I
    .end local v6    # "tagName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 85
    .local v3, "ex":Ljava/lang/Exception;
    sget-object v8, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parseMessage failed on message: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ex: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .end local v3    # "ex":Ljava/lang/Exception;
    :cond_3
    return-object v5

    .line 55
    .restart local v2    # "event":I
    .restart local v6    # "tagName":Ljava/lang/String;
    :sswitch_0
    :try_start_1
    const-string v11, "at"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v8, v9

    goto :goto_2

    :sswitch_1
    const-string v11, "a"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v8, v10

    goto :goto_2

    .line 69
    :pswitch_1
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v11, 0x0

    const-string v12, "href"

    invoke-interface {v8, v11, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "link":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 73
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/chat/ChatMessageParser;->xmlPullParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 74
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 75
    .local v7, "urlString":Landroid/text/SpannableString;
    new-instance v8, Landroid/text/style/URLSpan;

    invoke-direct {v8, v4}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v7, v8, v11, v12, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 76
    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 55
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_1
        0xc33 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
