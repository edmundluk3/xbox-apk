.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlatformDependency"
.end annotation


# instance fields
.field public final minVersion:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MinVersion"
    .end annotation
.end field

.field public final platformName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PlatformName"
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "minVersion"    # J
    .param p3, "platformName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 850
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 852
    iput-wide p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->minVersion:J

    .line 853
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->platformName:Ljava/lang/String;

    .line 854
    return-void
.end method

.method private buildAndRevisionString(II)Ljava/lang/String;
    .locals 2
    .param p1, "mainBuild"    # I
    .param p2, "revision"    # I

    .prologue
    .line 894
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 895
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 899
    :goto_0
    return-object v0

    .line 896
    :cond_0
    if-eqz p1, :cond_1

    .line 897
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 899
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private getPCVersionString(J)Ljava/lang/String;
    .locals 11
    .param p1, "version"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 866
    const-wide/16 v6, 0x0

    invoke-static {v6, v7, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 868
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 869
    .local v3, "numbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/16 v4, 0x30

    .line 870
    .local v4, "shiftBits":I
    const v1, 0xffff

    .line 872
    .local v1, "mask":I
    :goto_0
    if-ltz v4, :cond_0

    .line 873
    shr-long v6, p1, v4

    long-to-int v6, v6

    and-int v2, v6, v1

    .line 874
    .local v2, "number":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 875
    add-int/lit8 v4, v4, -0x10

    .line 876
    goto :goto_0

    .line 879
    .end local v2    # "number":I
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    .line 880
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v7, v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->buildAndRevisionString(II)Ljava/lang/String;

    move-result-object v5

    .line 881
    .local v5, "windowsValue":Ljava/lang/String;
    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v6, 0x3

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v7, v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->buildAndRevisionString(II)Ljava/lang/String;

    move-result-object v0

    .line 883
    .local v0, "buildVersion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 884
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070c50

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v5, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 890
    .end local v0    # "buildVersion":Ljava/lang/String;
    .end local v5    # "windowsValue":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 886
    .restart local v0    # "buildVersion":Ljava/lang/String;
    .restart local v5    # "windowsValue":Ljava/lang/String;
    :cond_1
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070c51

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v9

    aput-object v0, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 890
    .end local v0    # "buildVersion":Ljava/lang/String;
    .end local v5    # "windowsValue":Ljava/lang/String;
    :cond_2
    const-string v6, ""

    goto :goto_1
.end method


# virtual methods
.method public getVersionString()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 858
    iget-wide v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->minVersion:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 859
    iget-wide v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->minVersion:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;->getPCVersionString(J)Ljava/lang/String;

    move-result-object v0

    .line 861
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
