.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileFriendAttainmentDetailScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFriendAchievementDetailAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private achievementId:I

.field private scid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Ljava/lang/String;I)V
    .locals 0
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I

    .prologue
    .line 124
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 125
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    .line 126
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->achievementId:I

    .line 127
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 3

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshAchievementDetail(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->achievementId:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAchievementDetail(ZLjava/lang/String;I)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 162
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-object v0

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "GameProfileFriendAttainmentDetailScreenViewModel"

    const-string v2, "loading profile summary failed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 4

    .prologue
    .line 137
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->achievementId:I

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;I)V

    .line 139
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->scid:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->achievementId:I

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;I)V

    .line 151
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 120
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Z)Z

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->updateAdapter()V

    .line 146
    return-void
.end method
