.class public Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ChooseProfileColorDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;,
        Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;
    }
.end annotation


# static fields
.field private static final COLUMNS:I = 0x4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cellMargin:I

.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private profileColorContainer:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

.field private rootView:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->cellMargin:I

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030053

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e025a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e025c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setNumColumns(I)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->setContentView(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileColorContainer"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 48
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->profileColorContainer:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->profileColorContainer:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->layoutInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .prologue
    .line 20
    iget v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->cellMargin:I

    return v0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method public setProfileColorContainer(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->profileColorContainer:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    .line 70
    return-void
.end method
