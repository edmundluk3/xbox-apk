.class public Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "AccountProvisioningFragment.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;,
        Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;,
        Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;
    }
.end annotation


# static fields
.field private static final KEY_STATE:Ljava/lang/String; = "KEY_STATE"

.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

.field private final getPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/Privacy$Settings;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loaderMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final setPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

.field private userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

.field private final userProfileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/UserAccount;",
            ">;>;"
        }
    .end annotation
.end field

.field private final xtokenCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/XTokenLoader$Result;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    .line 401
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$5;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 42
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    .line 138
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$1;-><init>(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->userProfileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 223
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$2;-><init>(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->xtokenCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 265
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$3;-><init>(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->getPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 314
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$4;-><init>(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->setPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_acc_prov_get_profile:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->userProfileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_post_profile:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->userProfileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_x_token:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ResultLoaderInfo;

    const-class v3, Lcom/microsoft/xbox/idp/toolkit/XTokenLoader$Result;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->xtokenCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/idp/util/ResultLoaderInfo;-><init>(Ljava/lang/Class;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_get_privacy_settings:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->getPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_set_privacy_settings:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->setPrivacySettingsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)Lcom/microsoft/xbox/idp/model/UserAccount;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;Lcom/microsoft/xbox/idp/model/UserAccount;)Lcom/microsoft/xbox/idp/model/UserAccount;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;
    .param p1, "x1"    # Lcom/microsoft/xbox/idp/model/UserAccount;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;)Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getLoaderInfo(I)Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;
    .locals 1
    .param p1, "loaderId"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->loaderMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->getActivityResult(IILandroid/content/Intent;)Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;

    move-result-object v0

    .line 120
    .local v0, "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;->isTryAgain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    sget-object v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    const-string v2, "Trying again"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->deleteLoader()V

    .line 131
    .end local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_0
    :goto_0
    return-void

    .line 125
    .restart local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iput-object v3, v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    .line 126
    sget-object v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    const-string v2, "onActivityResult"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 59
    check-cast p1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    .line 60
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 70
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 73
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 74
    sget-object v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    const-string v2, "No arguments provided"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    const-string v1, "ARG_USER_PTR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 77
    sget-object v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    const-string v2, "No ARG_USER_PTR"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Status;Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;)V

    goto :goto_0

    .line 80
    :cond_2
    if-nez p1, :cond_3

    new-instance v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    invoke-direct {v1}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;-><init>()V

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->setActivityContext(Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;)V

    goto :goto_0

    .line 80
    :cond_3
    const-string v1, "KEY_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_busy:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 65
    sget-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$Callbacks;

    .line 66
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 97
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onResume()V

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 100
    .local v0, "args":Landroid/os/Bundle;
    sget-object v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->TAG:Ljava/lang/String;

    const-string v3, "Initializing loader_acc_prov_get_profile"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 102
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_RESULT_KEY"

    new-instance v3, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;

    const-class v4, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;

    sget v5, Lcom/microsoft/xbox/idp/R$id;->loader_acc_prov_get_profile:I

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 103
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget v3, Lcom/microsoft/xbox/idp/R$id;->loader_acc_prov_get_profile:I

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->initLoader(ILandroid/os/Bundle;)Z

    .line 106
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 111
    const-string v0, "KEY_STATE"

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment;->state:Lcom/microsoft/xbox/idp/ui/AccountProvisioningFragment$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    return-void
.end method
