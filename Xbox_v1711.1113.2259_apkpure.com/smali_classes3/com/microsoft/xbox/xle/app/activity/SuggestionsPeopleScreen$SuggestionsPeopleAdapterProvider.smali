.class public Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;
.super Ljava/lang/Object;
.source "SuggestionsPeopleScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SuggestionsPeopleAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen$SuggestionsPeopleAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 67
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .line 68
    .local v0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSuggestionsPeopleScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
