.class final Lcom/microsoft/xbox/xle/app/XLEUtil$1;
.super Landroid/os/CountDownTimer;
.source "XLEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$date:Ljava/util/Date;

.field final synthetic val$dateTextView:Landroid/widget/TextView;

.field final synthetic val$helper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

.field final synthetic val$milliSecond:J

.field final synthetic val$nextDate:Ljava/util/Date;

.field final synthetic val$nextMilliSecond:J

.field final synthetic val$textColor:I

.field final synthetic val$timerLabel:Landroid/widget/TextView;


# direct methods
.method constructor <init>(JJJLjava/util/Date;Landroid/widget/TextView;Landroid/widget/TextView;JILcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;Ljava/util/Date;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # J

    .prologue
    .line 766
    iput-wide p5, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextMilliSecond:J

    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextDate:Ljava/util/Date;

    iput-object p8, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$timerLabel:Landroid/widget/TextView;

    iput-object p9, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    iput-wide p10, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$milliSecond:J

    iput p12, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$textColor:I

    iput-object p13, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$helper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    iput-object p14, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$date:Ljava/util/Date;

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 12

    .prologue
    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    .line 769
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextMilliSecond:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextDate:Ljava/util/Date;

    if-eqz v0, :cond_2

    .line 771
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$timerLabel:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$timerLabel:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070619

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 778
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0142

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 781
    :cond_1
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextMilliSecond:J

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$milliSecond:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    div-long v10, v0, v2

    .line 782
    .local v10, "timespanDayDiff":J
    const-wide/16 v0, 0x1

    cmp-long v0, v10, v0

    if-ltz v0, :cond_3

    .line 783
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextDate:Ljava/util/Date;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 789
    .end local v10    # "timespanDayDiff":J
    :cond_2
    :goto_0
    return-void

    .line 786
    .restart local v10    # "timespanDayDiff":J
    :cond_3
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextMilliSecond:J

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$nextDate:Ljava/util/Date;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$textColor:I

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$helper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    move-object v8, v7

    invoke-static/range {v0 .. v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 10
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 793
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 794
    .local v0, "currentDate":Ljava/util/Date;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$date:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long v4, v6, v8

    .line 795
    .local v4, "timeDiff":J
    const-wide/32 v6, 0x36ee80

    div-long v2, v4, v6

    .line 796
    .local v2, "hoursDiff":J
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$date:Ljava/util/Date;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 797
    const-wide/16 v6, 0x1

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 798
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$dateTextView:Landroid/widget/TextView;

    iget v6, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$1;->val$textColor:I

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 800
    :cond_0
    return-void
.end method
