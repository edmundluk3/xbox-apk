.class Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubInviteScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFollowerProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$1;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshFollowingProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    .line 329
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 328
    :goto_0
    return-object v0

    .line 329
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 322
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 308
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 318
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 299
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 313
    return-void
.end method
