.class public abstract Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ChatHeadRemovedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/io/Serializable;Z)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent;
    .locals 1
    .param p0    # Ljava/io/Serializable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "userTriggered"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(TT;Z)",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadRemovedEvent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "key":Ljava/io/Serializable;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 40
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadRemovedEvent;-><init>(Ljava/io/Serializable;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract key()Ljava/io/Serializable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract userTriggered()Z
.end method
