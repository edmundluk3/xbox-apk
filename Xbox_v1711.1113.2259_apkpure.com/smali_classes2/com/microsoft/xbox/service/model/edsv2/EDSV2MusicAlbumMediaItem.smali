.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2MusicAlbumMediaItem.java"


# static fields
.field private static final XBOX_MUSIC_TITLE_STRING:Ljava/lang/String;

.field private static final ZUNE_ALBUM_DEEPLINK:Ljava/lang/String; = "Details?ContentType=Album&ContentId=%s"

.field private static final ZUNE_TITLE_ID_STRING:Ljava/lang/String; = "0x5848085B"


# instance fields
.field public Duration:Ljava/lang/String;

.field private DurationInMMSS:Ljava/lang/String;

.field public Genres:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation
.end field

.field public IsExplicit:Z

.field public Label:Ljava/lang/String;

.field public LabelOwner:Ljava/lang/String;

.field public ParentItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public SortName:Ljava/lang/String;

.field public TrackCount:I

.field public ZuneId:Ljava/lang/String;

.field private generatedProvider:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation
.end field

.field private primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0710a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->XBOX_MUSIC_TITLE_STRING:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 40
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Duration:Ljava/lang/String;

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Genres:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Genres:Ljava/util/ArrayList;

    move-object v0, p1

    .line 43
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->IsExplicit:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->IsExplicit:Z

    move-object v0, p1

    .line 44
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Label:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Label:Ljava/lang/String;

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->LabelOwner:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->LabelOwner:Ljava/lang/String;

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->SortName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->SortName:Ljava/lang/String;

    move-object v0, p1

    .line 47
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ZuneId:Ljava/lang/String;

    .line 48
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ParentItems:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ParentItems:Ljava/util/ArrayList;

    .line 51
    :cond_0
    const/16 v0, 0x3ee

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->setMediaItemTypeFromInt(I)V

    .line 52
    return-void
.end method


# virtual methods
.method public getArtistName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    .line 72
    .local v0, "artist":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getArtistZuneId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    .line 77
    .local v0, "artist":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->DurationInMMSS:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Duration:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Duration:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->DurationInMMSS:Ljava/lang/String;

    .line 113
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->DurationInMMSS:Ljava/lang/String;

    return-object v0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Duration:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->durationStringToSeconds(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getTimeStringMMSS(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->DurationInMMSS:Ljava/lang/String;

    goto :goto_0
.end method

.method public getGeneres()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->Genres:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic getGeneres()Ljava/util/List;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getGeneres()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getLabelOwner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->LabelOwner:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfTracks()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->TrackCount:I

    return v0
.end method

.method public getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    .locals 4

    .prologue
    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    if-nez v1, :cond_1

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ParentItems:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ParentItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 58
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    const/16 v3, 0x3f1

    if-ne v2, v3, :cond_0

    .line 59
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->primaryArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    return-object v1
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->generatedProvider:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ZuneId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getMusicProviders(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->generatedProvider:Ljava/util/ArrayList;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->generatedProvider:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method
