.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.source "EDSV2AppDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x2

    return v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->PublisherName:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 5
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->loadBigCatItemDetailFromProductId(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getLegacyId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getLegacyId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->loadBigCatItemDetailFromLegacyId(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getTitleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getTitleId()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->loadBigCatItemDetailFromTitleId(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xfae

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v4, v4, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method protected onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->isLoading:Z

    .line 64
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 65
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method
