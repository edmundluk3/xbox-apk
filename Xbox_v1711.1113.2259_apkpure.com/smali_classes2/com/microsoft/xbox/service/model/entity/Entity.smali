.class public Lcom/microsoft/xbox/service/model/entity/Entity;
.super Ljava/lang/Object;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/entity/Entity$Type;
    }
.end annotation


# instance fields
.field private final entity:Ljava/lang/Object;

.field private final type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V
    .locals 1
    .param p1, "achievement"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "achievement cannot be null"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V
    .locals 1
    .param p1, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "gameClip cannot be null"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 14
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    .line 15
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->GAME_DVR:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 1
    .param p1, "activityFeedItem"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "activityFeedItem cannot be null"

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ACTIVITY_FEED_ITEM:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    .line 28
    return-void
.end method


# virtual methods
.method public getAchievement()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$1;->$SwitchMap$com$microsoft$xbox$service$model$entity$Entity$Type:[I

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 48
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 46
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$1;->$SwitchMap$com$microsoft$xbox$service$model$entity$Entity$Type:[I

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 57
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 55
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public getGameClip()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/model/entity/Entity$1;->$SwitchMap$com$microsoft$xbox$service$model$entity$Entity$Type:[I

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/entity/Entity$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 39
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 37
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->entity:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    goto :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getType()Lcom/microsoft/xbox/service/model/entity/Entity$Type;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/Entity;->type:Lcom/microsoft/xbox/service/model/entity/Entity$Type;

    return-object v0
.end method
