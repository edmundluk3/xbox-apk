.class public Lorg/webrtc/VideoCapturerAndroid;
.super Ljava/lang/Object;
.source "VideoCapturerAndroid.java"

# interfaces
.implements Lorg/webrtc/CameraVideoCapturer;
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Lorg/webrtc/SurfaceTextureHelper$OnTextureFrameAvailableListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final CAMERA_STOP_TIMEOUT_MS:I = 0x1b58

.field private static final MAX_OPEN_CAMERA_ATTEMPTS:I = 0x3

.field private static final NUMBER_OF_CAPTURE_BUFFERS:I = 0x3

.field private static final OPEN_CAMERA_DELAY_MS:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "VideoCapturerAndroid"

.field private static final videoCapturerAndroidResolutionHistogram:Lorg/webrtc/Histogram;

.field private static final videoCapturerAndroidStartTimeMsHistogram:Lorg/webrtc/Histogram;

.field private static final videoCapturerAndroidStopTimeMsHistogram:Lorg/webrtc/Histogram;


# instance fields
.field private applicationContext:Landroid/content/Context;

.field private camera:Landroid/hardware/Camera;

.field private final cameraErrorCallback:Landroid/hardware/Camera$ErrorCallback;

.field private final cameraIdLock:Ljava/lang/Object;

.field private cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

.field private volatile cameraThreadHandler:Landroid/os/Handler;

.field private captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

.field private final eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

.field private firstFrameReported:Z

.field private frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

.field private id:I

.field private info:Landroid/hardware/Camera$CameraInfo;

.field private final isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final isCapturingToTexture:Z

.field private openCameraAttempts:I

.field private volatile pendingCameraSwitch:Z

.field private final pendingCameraSwitchLock:Ljava/lang/Object;

.field private final queuedBuffers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation
.end field

.field private requestedFramerate:I

.field private requestedHeight:I

.field private requestedWidth:I

.field private startStartTimeNs:J

.field private surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x2710

    const/16 v2, 0x32

    const/4 v1, 0x1

    .line 49
    const-string v0, "WebRTC.Android.VideoCapturerAndroid.StartTimeMs"

    .line 50
    invoke-static {v0, v1, v3, v2}, Lorg/webrtc/Histogram;->createCounts(Ljava/lang/String;III)Lorg/webrtc/Histogram;

    move-result-object v0

    sput-object v0, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidStartTimeMsHistogram:Lorg/webrtc/Histogram;

    .line 51
    const-string v0, "WebRTC.Android.VideoCapturerAndroid.StopTimeMs"

    .line 52
    invoke-static {v0, v1, v3, v2}, Lorg/webrtc/Histogram;->createCounts(Ljava/lang/String;III)Lorg/webrtc/Histogram;

    move-result-object v0

    sput-object v0, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidStopTimeMsHistogram:Lorg/webrtc/Histogram;

    .line 53
    const-string v0, "WebRTC.Android.VideoCapturerAndroid.Resolution"

    sget-object v1, Lorg/webrtc/CameraEnumerationAndroid;->COMMON_RESOLUTIONS:Ljava/util/ArrayList;

    .line 55
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 54
    invoke-static {v0, v1}, Lorg/webrtc/Histogram;->createEnumeration(Ljava/lang/String;I)Lorg/webrtc/Histogram;

    move-result-object v0

    sput-object v0, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidResolutionHistogram:Lorg/webrtc/Histogram;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;Z)V
    .locals 3
    .param p1, "cameraName"    # Ljava/lang/String;
    .param p2, "eventsHandler"    # Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;
    .param p3, "captureToTexture"    # Z

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraIdLock:Ljava/lang/Object;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitchLock:Ljava/lang/Object;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    .line 83
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->queuedBuffers:Ljava/util/Set;

    .line 94
    new-instance v0, Lorg/webrtc/VideoCapturerAndroid$1;

    invoke-direct {v0, p0}, Lorg/webrtc/VideoCapturerAndroid$1;-><init>(Lorg/webrtc/VideoCapturerAndroid;)V

    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraErrorCallback:Landroid/hardware/Camera$ErrorCallback;

    .line 224
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No cameras available"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    .line 232
    :goto_0
    iput-object p2, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    .line 233
    iput-boolean p3, p0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    .line 234
    const-string v0, "VideoCapturerAndroid"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VideoCapturerAndroid isCapturingToTexture : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    return-void

    .line 230
    :cond_2
    invoke-static {p1}, Lorg/webrtc/Camera1Enumerator;->getCameraIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    goto :goto_0
.end method

.method static synthetic access$000(Lorg/webrtc/VideoCapturerAndroid;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100(Lorg/webrtc/VideoCapturerAndroid;)Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    return-object v0
.end method

.method static synthetic access$200(Lorg/webrtc/VideoCapturerAndroid;)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->switchCameraOnCameraThread()V

    return-void
.end method

.method static synthetic access$300(Lorg/webrtc/VideoCapturerAndroid;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitchLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lorg/webrtc/VideoCapturerAndroid;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitch:Z

    return p1
.end method

.method static synthetic access$500(Lorg/webrtc/VideoCapturerAndroid;)Landroid/hardware/Camera$CameraInfo;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;

    .prologue
    .line 44
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    return-object v0
.end method

.method static synthetic access$600(Lorg/webrtc/VideoCapturerAndroid;III)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid;->startPreviewOnCameraThread(III)V

    return-void
.end method

.method static synthetic access$702(Lorg/webrtc/VideoCapturerAndroid;I)I
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lorg/webrtc/VideoCapturerAndroid;->openCameraAttempts:I

    return p1
.end method

.method static synthetic access$800(Lorg/webrtc/VideoCapturerAndroid;III)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid;->startCaptureOnCameraThread(III)V

    return-void
.end method

.method static synthetic access$900(Lorg/webrtc/VideoCapturerAndroid;Z)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/VideoCapturerAndroid;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/webrtc/VideoCapturerAndroid;->stopCaptureOnCameraThread(Z)V

    return-void
.end method

.method private checkIsOnCameraThread()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 239
    const-string v0, "VideoCapturerAndroid"

    const-string v1, "Camera is not initialized - can\'t check thread."

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_0
    return-void

    .line 240
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 241
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrong thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static create(Ljava/lang/String;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;)Lorg/webrtc/VideoCapturerAndroid;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "eventsHandler"    # Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/webrtc/VideoCapturerAndroid;->create(Ljava/lang/String;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;Z)Lorg/webrtc/VideoCapturerAndroid;

    move-result-object v0

    return-object v0
.end method

.method public static create(Ljava/lang/String;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;Z)Lorg/webrtc/VideoCapturerAndroid;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "eventsHandler"    # Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;
    .param p2, "captureToTexture"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 129
    :try_start_0
    new-instance v1, Lorg/webrtc/VideoCapturerAndroid;

    invoke-direct {v1, p0, p1, p2}, Lorg/webrtc/VideoCapturerAndroid;-><init>(Ljava/lang/String;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-object v1

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "VideoCapturerAndroid"

    const-string v2, "Couldn\'t create camera."

    invoke-static {v1, v2, v0}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCurrentCameraId()I
    .locals 2

    .prologue
    .line 212
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraIdLock:Ljava/lang/Object;

    monitor-enter v1

    .line 213
    :try_start_0
    iget v0, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    monitor-exit v1

    return v0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getDeviceOrientation()I
    .locals 4

    .prologue
    .line 566
    const/4 v0, 0x0

    .line 568
    .local v0, "orientation":I
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->applicationContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 569
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 581
    const/4 v0, 0x0

    .line 584
    :goto_0
    return v0

    .line 571
    :pswitch_0
    const/16 v0, 0x5a

    .line 572
    goto :goto_0

    .line 574
    :pswitch_1
    const/16 v0, 0xb4

    .line 575
    goto :goto_0

    .line 577
    :pswitch_2
    const/16 v0, 0x10e

    .line 578
    goto :goto_0

    .line 569
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getFrameOrientation()I
    .locals 2

    .prologue
    .line 588
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->getDeviceOrientation()I

    move-result v0

    .line 589
    .local v0, "rotation":I
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v1, :cond_0

    .line 590
    rsub-int v0, v0, 0x168

    .line 592
    :cond_0
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v1, v0

    rem-int/lit16 v1, v1, 0x168

    return v1
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->applicationContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybePostDelayedOnCameraThread(ILjava/lang/Runnable;)Z
    .locals 6
    .param p1, "delayMs"    # I
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 250
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    .line 252
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    int-to-long v4, p1

    add-long/2addr v2, v4

    .line 251
    invoke-virtual {v0, p2, p0, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 250
    :goto_0
    return v0

    .line 251
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybePostOnCameraThread(Ljava/lang/Runnable;)Z
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/webrtc/VideoCapturerAndroid;->maybePostDelayedOnCameraThread(ILjava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method private onFirstFrameAvailable()V
    .locals 6

    .prologue
    .line 646
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v1, :cond_0

    .line 647
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    invoke-interface {v1}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onFirstFrameAvailable()V

    .line 649
    :cond_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 650
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, p0, Lorg/webrtc/VideoCapturerAndroid;->startStartTimeNs:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v0, v2

    .line 651
    .local v0, "startTimeMs":I
    sget-object v1, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidStartTimeMsHistogram:Lorg/webrtc/Histogram;

    invoke-virtual {v1, v0}, Lorg/webrtc/Histogram;->addSample(I)V

    .line 652
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/webrtc/VideoCapturerAndroid;->firstFrameReported:Z

    .line 653
    return-void
.end method

.method private startCaptureOnCameraThread(III)V
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 320
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 321
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/webrtc/VideoCapturerAndroid;->startStartTimeNs:J

    .line 322
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_1

    .line 323
    const-string v1, "VideoCapturerAndroid"

    const-string/jumbo v2, "startCaptureOnCameraThread: Camera is stopped"

    invoke-static {v1, v2}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    if-eqz v1, :cond_2

    .line 327
    const-string v1, "VideoCapturerAndroid"

    const-string/jumbo v2, "startCaptureOnCameraThread: Camera has already been started."

    invoke-static {v1, v2}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_2
    iput-boolean v5, p0, Lorg/webrtc/VideoCapturerAndroid;->firstFrameReported:Z

    .line 334
    :try_start_0
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraIdLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :try_start_1
    const-string v1, "VideoCapturerAndroid"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Opening camera "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v1, :cond_3

    .line 337
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    iget v3, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    invoke-static {v3}, Lorg/webrtc/Camera1Enumerator;->getDeviceName(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraOpening(Ljava/lang/String;)V

    .line 339
    :cond_3
    iget v1, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    invoke-static {v1}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    .line 340
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iput-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    .line 341
    iget v1, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    invoke-static {v1, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 342
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    :try_start_2
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    invoke-virtual {v2}, Lorg/webrtc/SurfaceTextureHelper;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 360
    const-string v1, "VideoCapturerAndroid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Camera orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    iget v3, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " .Device orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 361
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->getDeviceOrientation()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 360
    invoke-static {v1, v2}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraErrorCallback:Landroid/hardware/Camera$ErrorCallback;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 363
    invoke-direct {p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid;->startPreviewOnCameraThread(III)V

    .line 364
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onCapturerStarted(Z)V

    .line 365
    iget-boolean v1, p0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    if-eqz v1, :cond_4

    .line 366
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    invoke-virtual {v1, p0}, Lorg/webrtc/SurfaceTextureHelper;->startListening(Lorg/webrtc/SurfaceTextureHelper$OnTextureFrameAvailableListener;)V

    .line 370
    :cond_4
    new-instance v1, Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    invoke-direct {v1, v2, v3}, Lorg/webrtc/CameraVideoCapturer$CameraStatistics;-><init>(Lorg/webrtc/SurfaceTextureHelper;Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;)V

    iput-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v1, "VideoCapturerAndroid"

    const-string/jumbo v2, "startCapture failed"

    invoke-static {v1, v2, v0}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 374
    invoke-direct {p0, v6}, Lorg/webrtc/VideoCapturerAndroid;->stopCaptureOnCameraThread(Z)V

    .line 375
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    invoke-interface {v1, v5}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onCapturerStarted(Z)V

    .line 376
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    const-string v2, "Camera can not be started."

    invoke-interface {v1, v2}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraError(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 342
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 343
    :catch_1
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_5
    iget v1, p0, Lorg/webrtc/VideoCapturerAndroid;->openCameraAttempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/webrtc/VideoCapturerAndroid;->openCameraAttempts:I

    .line 345
    iget v1, p0, Lorg/webrtc/VideoCapturerAndroid;->openCameraAttempts:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_5

    .line 346
    const-string v1, "VideoCapturerAndroid"

    const-string v2, "Camera.open failed, retrying"

    invoke-static {v1, v2, v0}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 347
    const/16 v1, 0x1f4

    new-instance v2, Lorg/webrtc/VideoCapturerAndroid$5;

    invoke-direct {v2, p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid$5;-><init>(Lorg/webrtc/VideoCapturerAndroid;III)V

    invoke-direct {p0, v1, v2}, Lorg/webrtc/VideoCapturerAndroid;->maybePostDelayedOnCameraThread(ILjava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 371
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v0

    goto :goto_1

    .line 355
    .restart local v0    # "e":Ljava/lang/RuntimeException;
    :cond_5
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2
.end method

.method private startPreviewOnCameraThread(III)V
    .locals 16
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    .line 384
    invoke-direct/range {p0 .. p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 385
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    if-nez v13, :cond_2

    .line 386
    :cond_0
    const-string v13, "VideoCapturerAndroid"

    const-string/jumbo v14, "startPreviewOnCameraThread: Camera is stopped"

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_1
    :goto_0
    return-void

    .line 389
    :cond_2
    const-string v13, "VideoCapturerAndroid"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "startPreviewOnCameraThread requested: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "x"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "@"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lorg/webrtc/VideoCapturerAndroid;->requestedWidth:I

    .line 393
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/webrtc/VideoCapturerAndroid;->requestedHeight:I

    .line 394
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lorg/webrtc/VideoCapturerAndroid;->requestedFramerate:I

    .line 397
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v8

    .line 399
    .local v8, "parameters":Landroid/hardware/Camera$Parameters;
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v13

    invoke-static {v13}, Lorg/webrtc/Camera1Enumerator;->convertFramerates(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 400
    .local v11, "supportedFramerates":Ljava/util/List;, "Ljava/util/List<Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;>;"
    const-string v13, "VideoCapturerAndroid"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Available fps ranges: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    move/from16 v0, p3

    invoke-static {v11, v0}, Lorg/webrtc/CameraEnumerationAndroid;->getClosestSupportedFramerateRange(Ljava/util/List;I)Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;

    move-result-object v5

    .line 406
    .local v5, "fpsRange":Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v13

    invoke-static {v13}, Lorg/webrtc/Camera1Enumerator;->convertSizes(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 408
    .local v12, "supportedPreviewSizes":Ljava/util/List;, "Ljava/util/List<Lorg/webrtc/Size;>;"
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v12, v0, v1}, Lorg/webrtc/CameraEnumerationAndroid;->getClosestSupportedSize(Ljava/util/List;II)Lorg/webrtc/Size;

    move-result-object v10

    .line 409
    .local v10, "previewSize":Lorg/webrtc/Size;
    sget-object v13, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidResolutionHistogram:Lorg/webrtc/Histogram;

    invoke-static {v13, v10}, Lorg/webrtc/CameraEnumerationAndroid;->reportCameraResolution(Lorg/webrtc/Histogram;Lorg/webrtc/Size;)V

    .line 411
    const-string v13, "VideoCapturerAndroid"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Available preview sizes: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    new-instance v3, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    iget v13, v10, Lorg/webrtc/Size;->width:I

    iget v14, v10, Lorg/webrtc/Size;->height:I

    invoke-direct {v3, v13, v14, v5}, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;-><init>(IILorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;)V

    .line 417
    .local v3, "captureFormat":Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    invoke-virtual {v3, v13}, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 422
    const-string v13, "VideoCapturerAndroid"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isVideoStabilizationSupported: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 424
    const/4 v13, 0x1

    invoke-virtual {v8, v13}, Landroid/hardware/Camera$Parameters;->setVideoStabilization(Z)V

    .line 428
    :cond_3
    iget-object v13, v3, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->framerate:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;

    iget v13, v13, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;->max:I

    if-lez v13, :cond_4

    .line 429
    iget-object v13, v3, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->framerate:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;

    iget v13, v13, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;->min:I

    iget-object v14, v3, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->framerate:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;

    iget v14, v14, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat$FramerateRange;->max:I

    invoke-virtual {v8, v13, v14}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 431
    :cond_4
    iget v13, v10, Lorg/webrtc/Size;->width:I

    iget v14, v10, Lorg/webrtc/Size;->height:I

    invoke-virtual {v8, v13, v14}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 433
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    if-nez v13, :cond_5

    .line 434
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v13, 0x11

    invoke-virtual {v8, v13}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 439
    :cond_5
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v13

    invoke-static {v13}, Lorg/webrtc/Camera1Enumerator;->convertSizes(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 438
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v13, v0, v1}, Lorg/webrtc/CameraEnumerationAndroid;->getClosestSupportedSize(Ljava/util/List;II)Lorg/webrtc/Size;

    move-result-object v9

    .line 440
    .local v9, "pictureSize":Lorg/webrtc/Size;
    iget v13, v9, Lorg/webrtc/Size;->width:I

    iget v14, v9, Lorg/webrtc/Size;->height:I

    invoke-virtual {v8, v13, v14}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 443
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    if-eqz v13, :cond_6

    .line 444
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->stopPreview()V

    .line 447
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 450
    :cond_6
    invoke-virtual {v8}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v4

    .line 451
    .local v4, "focusModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v13, "continuous-video"

    invoke-interface {v4, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 452
    const-string v13, "VideoCapturerAndroid"

    const-string v14, "Enable continuous auto focus mode."

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v13, "continuous-video"

    invoke-virtual {v8, v13}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 457
    :cond_7
    const-string v13, "VideoCapturerAndroid"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start capturing: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    .line 460
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13, v8}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 462
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 463
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    if-nez v13, :cond_9

    .line 464
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->queuedBuffers:Ljava/util/Set;

    invoke-interface {v13}, Ljava/util/Set;->clear()V

    .line 465
    invoke-virtual {v3}, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->frameSize()I

    move-result v6

    .line 466
    .local v6, "frameSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v13, 0x3

    if-ge v7, v13, :cond_8

    .line 467
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 468
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->queuedBuffers:Ljava/util/Set;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 469
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 466
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 471
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 473
    .end local v6    # "frameSize":I
    .end local v7    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->startPreview()V

    goto/16 :goto_0
.end method

.method private stopCaptureOnCameraThread(Z)V
    .locals 6
    .param p1, "stopHandler"    # Z

    .prologue
    const/4 v5, 0x0

    .line 504
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 505
    const-string v3, "VideoCapturerAndroid"

    const-string/jumbo v4, "stopCaptureOnCameraThread"

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 511
    .local v0, "stopStartTime":J
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    if-eqz v3, :cond_0

    .line 512
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    invoke-virtual {v3}, Lorg/webrtc/SurfaceTextureHelper;->stopListening()V

    .line 514
    :cond_0
    if-eqz p1, :cond_1

    .line 522
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 523
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    invoke-virtual {v3, p0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 525
    :cond_1
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    if-eqz v3, :cond_2

    .line 526
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    invoke-virtual {v3}, Lorg/webrtc/CameraVideoCapturer$CameraStatistics;->release()V

    .line 527
    iput-object v5, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    .line 529
    :cond_2
    const-string v3, "VideoCapturerAndroid"

    const-string v4, "Stop preview."

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    if-eqz v3, :cond_3

    .line 531
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->stopPreview()V

    .line 532
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3, v5}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 534
    :cond_3
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->queuedBuffers:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 535
    iput-object v5, p0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    .line 537
    const-string v3, "VideoCapturerAndroid"

    const-string v4, "Release camera."

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    if-eqz v3, :cond_4

    .line 539
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->release()V

    .line 540
    iput-object v5, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    .line 542
    :cond_4
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v3, :cond_5

    .line 543
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    invoke-interface {v3}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraClosed()V

    .line 545
    :cond_5
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    long-to-int v2, v4

    .line 546
    .local v2, "stopTimeMs":I
    sget-object v3, Lorg/webrtc/VideoCapturerAndroid;->videoCapturerAndroidStopTimeMsHistogram:Lorg/webrtc/Histogram;

    invoke-virtual {v3, v2}, Lorg/webrtc/Histogram;->addSample(I)V

    .line 547
    const-string v3, "VideoCapturerAndroid"

    const-string/jumbo v4, "stopCaptureOnCameraThread done"

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    return-void
.end method

.method private switchCameraOnCameraThread()V
    .locals 3

    .prologue
    .line 551
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 552
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    const-string v0, "VideoCapturerAndroid"

    const-string/jumbo v1, "switchCameraOnCameraThread: Camera is stopped"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    :goto_0
    return-void

    .line 556
    :cond_0
    const-string v0, "VideoCapturerAndroid"

    const-string/jumbo v1, "switchCameraOnCameraThread"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/webrtc/VideoCapturerAndroid;->stopCaptureOnCameraThread(Z)V

    .line 558
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraIdLock:Ljava/lang/Object;

    monitor-enter v1

    .line 559
    :try_start_0
    iget v0, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    rem-int/2addr v0, v2

    iput v0, p0, Lorg/webrtc/VideoCapturerAndroid;->id:I

    .line 560
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    iget v0, p0, Lorg/webrtc/VideoCapturerAndroid;->requestedWidth:I

    iget v1, p0, Lorg/webrtc/VideoCapturerAndroid;->requestedHeight:I

    iget v2, p0, Lorg/webrtc/VideoCapturerAndroid;->requestedFramerate:I

    invoke-direct {p0, v0, v1, v2}, Lorg/webrtc/VideoCapturerAndroid;->startCaptureOnCameraThread(III)V

    .line 562
    const-string v0, "VideoCapturerAndroid"

    const-string/jumbo v1, "switchCameraOnCameraThread done"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 560
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public changeCaptureFormat(III)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    .line 201
    new-instance v0, Lorg/webrtc/VideoCapturerAndroid$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid$3;-><init>(Lorg/webrtc/VideoCapturerAndroid;III)V

    invoke-direct {p0, v0}, Lorg/webrtc/VideoCapturerAndroid;->maybePostOnCameraThread(Ljava/lang/Runnable;)Z

    .line 207
    return-void
.end method

.method public dispose()V
    .locals 2

    .prologue
    .line 257
    const-string v0, "VideoCapturerAndroid"

    const-string v1, "dispose"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public initialize(Lorg/webrtc/SurfaceTextureHelper;Landroid/content/Context;Lorg/webrtc/VideoCapturer$CapturerObserver;)V
    .locals 2
    .param p1, "surfaceTextureHelper"    # Lorg/webrtc/SurfaceTextureHelper;
    .param p2, "applicationContext"    # Landroid/content/Context;
    .param p3, "frameObserver"    # Lorg/webrtc/VideoCapturer$CapturerObserver;

    .prologue
    .line 267
    const-string v0, "VideoCapturerAndroid"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    if-nez p2, :cond_0

    .line 269
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationContext not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_0
    if-nez p3, :cond_1

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "frameObserver not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_1
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_2
    iput-object p2, p0, Lorg/webrtc/VideoCapturerAndroid;->applicationContext:Landroid/content/Context;

    .line 278
    iput-object p3, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    .line 279
    iput-object p1, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    .line 280
    if-nez p1, :cond_3

    const/4 v0, 0x0

    .line 281
    :goto_0
    iput-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    .line 282
    return-void

    .line 281
    :cond_3
    invoke-virtual {p1}, Lorg/webrtc/SurfaceTextureHelper;->getHandler()Landroid/os/Handler;

    move-result-object v0

    goto :goto_0
.end method

.method public isCapturingToTexture()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCapturingToTexture:Z

    return v0
.end method

.method public isScreencast()Z
    .locals 1

    .prologue
    .line 657
    const/4 v0, 0x0

    return v0
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 8
    .param p1, "data"    # [B
    .param p2, "callbackCamera"    # Landroid/hardware/Camera;

    .prologue
    .line 598
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 599
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 600
    const-string v0, "VideoCapturerAndroid"

    const-string v1, "onPreviewFrame: Camera is stopped"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 603
    :cond_1
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->queuedBuffers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    if-eq v0, p2, :cond_2

    .line 608
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected camera in callback!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 611
    :cond_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    .line 613
    .local v6, "captureTimeNs":J
    iget-boolean v0, p0, Lorg/webrtc/VideoCapturerAndroid;->firstFrameReported:Z

    if-nez v0, :cond_3

    .line 614
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->onFirstFrameAvailable()V

    .line 616
    :cond_3
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    invoke-virtual {v0}, Lorg/webrtc/CameraVideoCapturer$CameraStatistics;->addFrame()V

    .line 617
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    iget v3, v0, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->width:I

    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    iget v4, v0, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->height:I

    .line 618
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->getFrameOrientation()I

    move-result v5

    move-object v2, p1

    .line 617
    invoke-interface/range {v1 .. v7}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onByteBufferFrameCaptured([BIIIJ)V

    .line 619
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    goto :goto_0
.end method

.method public onTextureFrameAvailable(I[FJ)V
    .locals 9
    .param p1, "oesTextureId"    # I
    .param p2, "transformMatrix"    # [F
    .param p3, "timestampNs"    # J

    .prologue
    .line 624
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->checkIsOnCameraThread()V

    .line 625
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    const-string v0, "VideoCapturerAndroid"

    const-string v1, "onTextureFrameAvailable: Camera is stopped"

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    invoke-virtual {v0}, Lorg/webrtc/SurfaceTextureHelper;->returnTextureFrame()V

    .line 643
    :goto_0
    return-void

    .line 630
    :cond_0
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->getFrameOrientation()I

    move-result v5

    .line 631
    .local v5, "rotation":I
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->info:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 635
    invoke-static {}, Lorg/webrtc/RendererCommon;->horizontalFlipMatrix()[F

    move-result-object v0

    invoke-static {p2, v0}, Lorg/webrtc/RendererCommon;->multiplyMatrices([F[F)[F

    move-result-object p2

    .line 637
    :cond_1
    iget-boolean v0, p0, Lorg/webrtc/VideoCapturerAndroid;->firstFrameReported:Z

    if-nez v0, :cond_2

    .line 638
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->onFirstFrameAvailable()V

    .line 640
    :cond_2
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraStatistics:Lorg/webrtc/CameraVideoCapturer$CameraStatistics;

    invoke-virtual {v0}, Lorg/webrtc/CameraVideoCapturer$CameraStatistics;->addFrame()V

    .line 641
    iget-object v0, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    iget v1, v1, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->width:I

    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->captureFormat:Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;

    iget v2, v2, Lorg/webrtc/CameraEnumerationAndroid$CaptureFormat;->height:I

    move v3, p1

    move-object v4, p2

    move-wide v6, p3

    invoke-interface/range {v0 .. v7}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onTextureFrameCaptured(III[FIJ)V

    goto :goto_0
.end method

.method public printStackTrace()V
    .locals 7

    .prologue
    .line 137
    const/4 v1, 0x0

    .line 138
    .local v1, "cameraThread":Ljava/lang/Thread;
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 139
    iget-object v3, p0, Lorg/webrtc/VideoCapturerAndroid;->cameraThreadHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    .line 141
    :cond_0
    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 143
    .local v0, "cameraStackTraces":[Ljava/lang/StackTraceElement;
    array-length v3, v0

    if-lez v3, :cond_1

    .line 144
    const-string v3, "VideoCapturerAndroid"

    const-string v4, "VideoCapturerAndroid stacks trace:"

    invoke-static {v3, v4}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 146
    .local v2, "stackTrace":Ljava/lang/StackTraceElement;
    const-string v5, "VideoCapturerAndroid"

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 150
    .end local v0    # "cameraStackTraces":[Ljava/lang/StackTraceElement;
    .end local v2    # "stackTrace":Ljava/lang/StackTraceElement;
    :cond_1
    return-void
.end method

.method public startCapture(III)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    const/4 v4, 0x0

    .line 288
    const-string v1, "VideoCapturerAndroid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startCapture requested: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-direct {p0}, Lorg/webrtc/VideoCapturerAndroid;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "startCapture called in uninitialized state"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 292
    :cond_0
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->surfaceHelper:Lorg/webrtc/SurfaceTextureHelper;

    if-nez v1, :cond_2

    .line 293
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    invoke-interface {v1, v4}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onCapturerStarted(Z)V

    .line 294
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v1, :cond_1

    .line 295
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    const-string v2, "No SurfaceTexture created."

    invoke-interface {v1, v2}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraError(Ljava/lang/String;)V

    .line 317
    :cond_1
    :goto_0
    return-void

    .line 299
    :cond_2
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 300
    const-string v1, "VideoCapturerAndroid"

    const-string v2, "Camera has already been started."

    invoke-static {v1, v2}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :cond_3
    new-instance v1, Lorg/webrtc/VideoCapturerAndroid$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lorg/webrtc/VideoCapturerAndroid$4;-><init>(Lorg/webrtc/VideoCapturerAndroid;III)V

    invoke-direct {p0, v1}, Lorg/webrtc/VideoCapturerAndroid;->maybePostOnCameraThread(Ljava/lang/Runnable;)Z

    move-result v0

    .line 310
    .local v0, "didPost":Z
    if-nez v0, :cond_1

    .line 311
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    invoke-interface {v1, v4}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onCapturerStarted(Z)V

    .line 312
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v1, :cond_4

    .line 313
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    const-string v2, "Could not post task to camera thread."

    invoke-interface {v1, v2}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraError(Ljava/lang/String;)V

    .line 315
    :cond_4
    iget-object v1, p0, Lorg/webrtc/VideoCapturerAndroid;->isCameraRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method public stopCapture()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 479
    const-string v2, "VideoCapturerAndroid"

    const-string/jumbo v3, "stopCapture"

    invoke-static {v2, v3}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 481
    .local v0, "barrier":Ljava/util/concurrent/CountDownLatch;
    new-instance v2, Lorg/webrtc/VideoCapturerAndroid$6;

    invoke-direct {v2, p0, v0}, Lorg/webrtc/VideoCapturerAndroid$6;-><init>(Lorg/webrtc/VideoCapturerAndroid;Ljava/util/concurrent/CountDownLatch;)V

    invoke-direct {p0, v2}, Lorg/webrtc/VideoCapturerAndroid;->maybePostOnCameraThread(Ljava/lang/Runnable;)Z

    move-result v1

    .line 488
    .local v1, "didPost":Z
    if-nez v1, :cond_0

    .line 489
    const-string v2, "VideoCapturerAndroid"

    const-string v3, "Calling stopCapture() for already stopped camera."

    invoke-static {v2, v3}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :goto_0
    return-void

    .line 492
    :cond_0
    const-wide/16 v2, 0x1b58

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 493
    const-string v2, "VideoCapturerAndroid"

    const-string v3, "Camera stop timeout"

    invoke-static {v2, v3}, Lorg/webrtc/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    invoke-virtual {p0}, Lorg/webrtc/VideoCapturerAndroid;->printStackTrace()V

    .line 495
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    if-eqz v2, :cond_1

    .line 496
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->eventsHandler:Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;

    const-string v3, "Camera stop timeout"

    invoke-interface {v2, v3}, Lorg/webrtc/CameraVideoCapturer$CameraEventsHandler;->onCameraError(Ljava/lang/String;)V

    .line 499
    :cond_1
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->frameObserver:Lorg/webrtc/VideoCapturer$CapturerObserver;

    invoke-interface {v2}, Lorg/webrtc/VideoCapturer$CapturerObserver;->onCapturerStopped()V

    .line 500
    const-string v2, "VideoCapturerAndroid"

    const-string/jumbo v3, "stopCapture done"

    invoke-static {v2, v3}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchCamera(Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;)V
    .locals 4
    .param p1, "switchEventsHandler"    # Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;

    .prologue
    .line 156
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 157
    if-eqz p1, :cond_0

    .line 158
    const-string v1, "No camera to switch to."

    invoke-interface {p1, v1}, Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;->onCameraSwitchError(Ljava/lang/String;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitchLock:Ljava/lang/Object;

    monitor-enter v2

    .line 163
    :try_start_0
    iget-boolean v1, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitch:Z

    if-eqz v1, :cond_3

    .line 166
    const-string v1, "VideoCapturerAndroid"

    const-string v3, "Ignoring camera switch request."

    invoke-static {v1, v3}, Lorg/webrtc/Logging;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    if-eqz p1, :cond_2

    .line 168
    const-string v1, "Pending camera switch already in progress."

    invoke-interface {p1, v1}, Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;->onCameraSwitchError(Ljava/lang/String;)V

    .line 170
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 172
    :cond_3
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitch:Z

    .line 173
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    new-instance v1, Lorg/webrtc/VideoCapturerAndroid$2;

    invoke-direct {v1, p0, p1}, Lorg/webrtc/VideoCapturerAndroid$2;-><init>(Lorg/webrtc/VideoCapturerAndroid;Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;)V

    invoke-direct {p0, v1}, Lorg/webrtc/VideoCapturerAndroid;->maybePostOnCameraThread(Ljava/lang/Runnable;)Z

    move-result v0

    .line 187
    .local v0, "didPost":Z
    if-nez v0, :cond_0

    .line 188
    iget-object v2, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitchLock:Ljava/lang/Object;

    monitor-enter v2

    .line 189
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lorg/webrtc/VideoCapturerAndroid;->pendingCameraSwitch:Z

    .line 190
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 191
    if-eqz p1, :cond_0

    .line 192
    const-string v1, "Camera is stopped."

    invoke-interface {p1, v1}, Lorg/webrtc/CameraVideoCapturer$CameraSwitchHandler;->onCameraSwitchError(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method
