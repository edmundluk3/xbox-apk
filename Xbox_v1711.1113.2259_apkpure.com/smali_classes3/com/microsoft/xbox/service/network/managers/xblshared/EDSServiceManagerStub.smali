.class public Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerStub;
.super Ljava/lang/Object;
.source "EDSServiceManagerStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;


# static fields
.field private static final XSTS_AUDIENCE:Ljava/lang/String; = "http://xboxlive.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 109
    sget-object v5, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_RELEASEDATE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerStub;->browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public browseMediaItemList(Ljava/lang/String;IILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 119
    const/4 v1, 0x0

    .line 121
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const-string v3, ""

    .line 122
    .local v3, "fileName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 123
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v6, 0x0

    .line 124
    .local v6, "stream":Ljava/io/InputStream;
    packed-switch p2, :pswitch_data_0

    .line 138
    :goto_0
    :pswitch_0
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v7, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 139
    const-class v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    if-eqz v6, :cond_0

    .line 146
    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 153
    :cond_0
    :goto_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 156
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 126
    .end local v4    # "i":I
    :pswitch_1
    const-string v3, "XBLShared/EDSV2ItemMusicAlbumTracksResponse.json"

    .line 127
    goto :goto_0

    .line 129
    :pswitch_2
    const-string v3, "XBLShared/EDSV2ItemTVSeriesResponse.json"

    .line 130
    goto :goto_0

    .line 132
    :pswitch_3
    const-string v3, "XBLShared/EDSV2TVSeasonResponse.json"

    goto :goto_0

    .line 141
    :catch_0
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "EDSServiceManagerStub"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 144
    if-eqz v6, :cond_0

    .line 146
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 147
    :catch_1
    move-exception v7

    goto :goto_1

    .line 144
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_1

    .line 146
    :try_start_4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 149
    :cond_1
    :goto_3
    throw v7

    .line 147
    :catch_2
    move-exception v7

    goto :goto_1

    :catch_3
    move-exception v8

    goto :goto_3

    .line 159
    :cond_2
    return-object v1

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public browseMediaItemList(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaTypeString"    # Ljava/lang/String;
    .param p3, "mediaItemType"    # I
    .param p4, "impressionGuid"    # Ljava/lang/String;
    .param p5, "orderBy"    # Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .param p6, "genre"    # Ljava/lang/String;
    .param p7, "skipItems"    # I
    .param p8, "maxvalue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 298
    const/4 v0, 0x0

    return-object v0
.end method

.method public browseSnappableAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">()",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 285
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBundles(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCombinedContentRating()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 227
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "subscriptionLevel":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getAllowExplicitContent()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 231
    .local v0, "isAdultAccount":Z
    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerStub;->getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 229
    .end local v0    # "isAdultAccount":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCombinedContentRating(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "legalLocale"    # Ljava/lang/String;
    .param p2, "subscriptionLevel"    # Ljava/lang/String;
    .param p3, "isAdultAccount"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 235
    const-string v0, ""

    return-object v0
.end method

.method public getFutureShowtimes(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "headendIdList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 219
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameDlc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGenreList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "mediaType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 269
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIncludedContent(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 211
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMediaItemDetail(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 8
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "partnerMediaId"    # Ljava/lang/String;
    .param p3, "titleId"    # J
    .param p5, "mediaGroup"    # I
    .param p6, "impressionGuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JI",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInitializeComplete()Z

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 59
    const/4 v3, 0x0

    .line 60
    .local v3, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v4, 0x0

    .line 61
    .local v4, "stream":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 62
    .local v2, "fileName":Ljava/lang/String;
    packed-switch p5, :pswitch_data_0

    .line 79
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 84
    :goto_0
    :try_start_0
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v5, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 85
    const-class v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    if-eqz v4, :cond_0

    .line 92
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 99
    :cond_0
    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 100
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 103
    :goto_2
    return-object v5

    .line 64
    :pswitch_0
    const-string v2, "XBLShared/EDSV2ItemAppDetailResponse.json"

    .line 65
    goto :goto_0

    .line 67
    :pswitch_1
    const-string v2, "XBLShared/EDSV2ItemGameDetailResponse.json"

    .line 68
    goto :goto_0

    .line 70
    :pswitch_2
    const-string v2, "XBLShared/EDSV2ItemMovieDetailResponse.json"

    .line 71
    goto :goto_0

    .line 73
    :pswitch_3
    const-string v2, "XBLShared/EDSV2ItemTVSeriesDetailResponse.json"

    .line 74
    goto :goto_0

    .line 76
    :pswitch_4
    const-string v2, "XBLShared/EDSV2ItemMusicAlbumDetailResponse.json"

    .line 77
    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "EDSServiceManagerStub"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 90
    if-eqz v4, :cond_0

    .line 92
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 93
    :catch_1
    move-exception v5

    goto :goto_1

    .line 90
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_1

    .line 92
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 95
    :cond_1
    :goto_3
    throw v5

    .line 103
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 93
    :catch_2
    move-exception v5

    goto :goto_1

    :catch_3
    move-exception v6

    goto :goto_3

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getProgrammingItems2()Lcom/microsoft/xbox/service/model/edsv2/EDSV2DiscoverData;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRelated(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 9
    .param p1, "canonicalId"    # Ljava/lang/String;
    .param p2, "desiredMediaType"    # I
    .param p3, "mediaItemType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 164
    const/4 v1, 0x0

    .line 165
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v5, 0x0

    .line 166
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v6, 0x0

    .line 167
    .local v6, "stream":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 168
    .local v3, "fileName":Ljava/lang/String;
    sparse-switch p2, :sswitch_data_0

    .line 181
    const-string v3, "XBLShared/EDSV2MovieRelatedResponse.json"

    .line 186
    :goto_0
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v7, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 187
    const-class v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    if-eqz v6, :cond_0

    .line 194
    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 201
    :cond_0
    :goto_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 204
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 170
    .end local v4    # "i":I
    :sswitch_0
    const-string v3, "XBLShared/EDSV2GameRelatedResponse.json"

    .line 171
    goto :goto_0

    .line 173
    :sswitch_1
    const-string v3, "XBLShared/EDSV2MovieRelatedResponse.json"

    .line 174
    goto :goto_0

    .line 178
    :sswitch_2
    const-string v3, "XBLShared/EDSV2TvSeriesRelatedResponse.json"

    .line 179
    goto :goto_0

    .line 189
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "EDSServiceManagerStub"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    if-eqz v6, :cond_0

    .line 194
    :try_start_3
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 195
    :catch_1
    move-exception v7

    goto :goto_1

    .line 192
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_1

    .line 194
    :try_start_4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 197
    :cond_1
    :goto_3
    throw v7

    .line 195
    :catch_2
    move-exception v7

    goto :goto_1

    :catch_3
    move-exception v8

    goto :goto_3

    .line 207
    :cond_2
    return-object v1

    .line 168
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3e8 -> :sswitch_1
        0x3eb -> :sswitch_2
        0x3ec -> :sswitch_2
        0x3ed -> :sswitch_2
    .end sparse-switch
.end method

.method public getSeriesFromSeaonId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSmartDJ(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SmartDJResult;
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 265
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStoreItems(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1
    .param p1, "searchType"    # Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .param p3, "skipItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 309
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleImage(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "titleId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleImageFromId(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "Ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public initializeServiceManager()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public searchMediaItems(Ljava/lang/String;ILjava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 6
    .param p1, "searchTerm"    # Ljava/lang/String;
    .param p2, "filter"    # I
    .param p3, "continuationToken"    # Ljava/lang/String;
    .param p4, "resultsPerPage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 239
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 240
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getMembershipLevel()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 242
    const/4 v2, 0x0

    .line 243
    .local v2, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    const/4 v3, 0x0

    .line 245
    .local v3, "stream":Ljava/io/InputStream;
    :try_start_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v5, "XBLShared/EDSV2SearchResponse.json"

    invoke-virtual {v4, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 246
    const-class v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    if-eqz v3, :cond_0

    .line 253
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 260
    :cond_0
    :goto_0
    return-object v2

    .line 248
    :catch_0
    move-exception v1

    .line 249
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ESServiceManagerStub"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    if-eqz v3, :cond_0

    .line 253
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 254
    :catch_1
    move-exception v4

    goto :goto_0

    .line 251
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_1

    .line 253
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 256
    :cond_1
    :goto_1
    throw v4

    .line 254
    :catch_2
    move-exception v4

    goto :goto_0

    :catch_3
    move-exception v5

    goto :goto_1
.end method

.method public setCombinedContentRating(Ljava/lang/String;)V
    .locals 0
    .param p1, "combinedContentRating"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 223
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 224
    return-void
.end method
