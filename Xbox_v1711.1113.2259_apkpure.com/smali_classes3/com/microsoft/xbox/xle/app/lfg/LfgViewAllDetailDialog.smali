.class public Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "LfgViewAllDetailDialog.java"


# static fields
.field private static final DEFAULT_TAG_COLOR:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementIdHashCode:I

.field private final backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final descriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private final rootView:Landroid/widget/RelativeLayout;

.field private tagIdHashCode:I

.field private tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

.field private tagsListView:Landroid/support/v7/widget/RecyclerView;

.field private viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->TAG:Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c014e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->DEFAULT_TAG_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030161

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e022a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e077c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->descriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0778

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e077a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsListView:Landroid/support/v7/widget/RecyclerView;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    sget v1, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->DEFAULT_TAG_COLOR:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsListView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsListView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->setContentView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 52
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->updateDialog()V

    .line 55
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->dismissLfgViewAllDetailDialog()V

    .line 72
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->dismissLfgViewAllDetailDialog()V

    .line 77
    return-void
.end method

.method private setTagsArrayAdapterData(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V
    .locals 15
    .param p1, "handleData"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .param p2, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .prologue
    .line 94
    if-eqz p1, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v13

    if-eqz v13, :cond_7

    .line 95
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->tags()Ljava/util/List;

    move-result-object v9

    .line 96
    .local v9, "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->achievementIds()Ljava/util/List;

    move-result-object v2

    .line 97
    .local v2, "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/List;->hashCode()I

    move-result v10

    .line 98
    .local v10, "tagIdsHashCode":I
    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v3

    .line 102
    .local v3, "achievementIdsHashCode":I
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v13

    if-eqz v13, :cond_7

    iget v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagIdHashCode:I

    if-ne v13, v10, :cond_0

    iget v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->achievementIdHashCode:I

    if-eq v13, v3, :cond_7

    .line 103
    :cond_0
    iput v10, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagIdHashCode:I

    .line 104
    iput v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->achievementIdHashCode:I

    .line 106
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v12

    .line 107
    .local v12, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 109
    .local v5, "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v4, "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v11, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v6, "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 114
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 115
    .local v0, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v14, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-interface {v5, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 117
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_1
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 118
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 119
    .restart local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v14, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-interface {v5, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 123
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 124
    .local v1, "achievementId":Ljava/lang/String;
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 126
    .restart local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    if-eqz v0, :cond_3

    .line 127
    invoke-static {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->with(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v14

    invoke-interface {v4, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 131
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v1    # "achievementId":Ljava/lang/String;
    :cond_4
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 132
    .local v8, "tagId":Ljava/lang/String;
    sget-object v14, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v14, v8}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v7

    .line 134
    .local v7, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v7, :cond_5

    .line 135
    invoke-interface {v11, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 137
    :cond_5
    invoke-static {v8}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    move-result-object v14

    invoke-interface {v6, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 141
    .end local v7    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v8    # "tagId":Ljava/lang/String;
    :cond_6
    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v13}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 142
    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v13, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 143
    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v13, v6}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 144
    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v13, v11}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 145
    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->tagsArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v13}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 148
    .end local v2    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "achievementIdsHashCode":I
    .end local v4    # "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    .end local v5    # "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v6    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v9    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "tagIdsHashCode":I
    .end local v11    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v12    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_7
    return-void
.end method


# virtual methods
.method public setViewModel(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .line 152
    return-void
.end method

.method public updateDialog()V
    .locals 4

    .prologue
    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    .line 84
    .local v0, "handleData":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->viewModel:Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSelectedTitle()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v1

    .line 85
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->descriptionText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->setTagsArrayAdapterData(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V

    .line 91
    :cond_1
    return-void
.end method
