.class public final enum Lcom/microsoft/xbox/service/comments/CommentsService;
.super Ljava/lang/Enum;
.source "CommentsService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/comments/ICommentsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/comments/CommentsService;",
        ">;",
        "Lcom/microsoft/xbox/service/comments/ICommentsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/comments/CommentsService;

.field private static final ACTION_BATCH_ENDPOINT:Ljava/lang/String; = "https://comments.xboxlive.com/actions/batch"

.field private static final CONTRACT_VERSION:I = 0x1

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/comments/CommentsService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/comments/CommentsService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsService;

    .line 23
    new-array v0, v3, [Lcom/microsoft/xbox/service/comments/CommentsService;

    sget-object v1, Lcom/microsoft/xbox/service/comments/CommentsService;->INSTANCE:Lcom/microsoft/xbox/service/comments/CommentsService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->$VALUES:[Lcom/microsoft/xbox/service/comments/CommentsService;

    .line 26
    const-class v0, Lcom/microsoft/xbox/service/comments/CommentsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->TAG:Ljava/lang/String;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->STATIC_HEADERS:Ljava/util/List;

    .line 36
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/comments/CommentsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 39
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic lambda$getActionBatchResponse$0(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "locators"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x64L
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    const-string v0, "https://comments.xboxlive.com/actions/batch"

    sget-object v1, Lcom/microsoft/xbox/service/comments/CommentsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsBatchRequest;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsBatchRequest;-><init>(Ljava/util/List;)V

    .line 53
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/comments/CommentsService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/comments/CommentsService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/comments/CommentsService;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->$VALUES:[Lcom/microsoft/xbox/service/comments/CommentsService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/comments/CommentsService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/comments/CommentsService;

    return-object v0
.end method


# virtual methods
.method public getActionBatchResponse(Ljava/util/List;)Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x64L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "locators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/comments/CommentsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getActionBatchResponse(locators: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x64

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/service/comments/CommentsService$$Lambda$1;->lambdaFactory$(Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    return-object v0
.end method
