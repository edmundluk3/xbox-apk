.class public final enum Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
.super Ljava/lang/Enum;
.source "CreateLfgDetailsViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LfgVisibilityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

.field public static final enum Club:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

.field public static final enum Private:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

.field public static final enum XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;


# instance fields
.field private displayName:Ljava/lang/String;

.field private final sessionVisibility:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 637
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    const-string v1, "XboxLive"

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070722

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UNKNOWN"

    sget-object v5, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->XboxLive:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 638
    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    const-string v4, "Private"

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070721

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "UNKNOWN"

    sget-object v8, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->Friends:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V

    sput-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Private:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 639
    new-instance v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    const-string v4, "Club"

    const-string v6, ""

    const-string v7, "UNKNOWN"

    sget-object v8, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->Friends:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V

    sput-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Club:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 636
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Private:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Club:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    aput-object v1, v0, v10

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->$VALUES:[Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "telemetryName"    # Ljava/lang/String;
    .param p5, "sessionVisibility"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;",
            ")V"
        }
    .end annotation

    .prologue
    .line 647
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 648
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 649
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 650
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 652
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->displayName:Ljava/lang/String;

    .line 653
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->telemetryName:Ljava/lang/String;

    .line 654
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->sessionVisibility:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    .line 655
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 636
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
    .locals 1

    .prologue
    .line 636
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->$VALUES:[Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 660
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionVisibility()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 676
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->sessionVisibility:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 666
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->telemetryName:Ljava/lang/String;

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 670
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 671
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->displayName:Ljava/lang/String;

    .line 672
    return-void
.end method
