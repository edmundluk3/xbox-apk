.class public Lcom/microsoft/xbox/service/model/serialization/PresenceInfo;
.super Ljava/lang/Object;
.source "PresenceInfo.java"


# instance fields
.field public DetailedPresence:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public LastSeenDateTime:Ljava/util/Date;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/convert/Convert;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;
    .end annotation
.end field

.field public LastSeenTitleId:J
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public LastSeenTitleName:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public OnlineState:I
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
