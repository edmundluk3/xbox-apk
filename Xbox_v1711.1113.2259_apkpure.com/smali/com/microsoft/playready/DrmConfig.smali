.class public final Lcom/microsoft/playready/DrmConfig;
.super Lcom/microsoft/playready/Native_Class3;
.source "DrmConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DrmConfig"

.field private static s_HLSPlaylistFormattingOverride:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

.field private static s_HttpServerPort:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0xfa1

    sput v0, Lcom/microsoft/playready/DrmConfig;->s_HttpServerPort:I

    .line 23
    sget-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    sput-object v0, Lcom/microsoft/playready/DrmConfig;->s_HLSPlaylistFormattingOverride:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/playready/Native_Class3;-><init>()V

    return-void
.end method

.method private static checkAndCreatePath(Ljava/lang/String;)V
    .locals 6
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 112
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    :goto_0
    return-void

    .line 121
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 122
    const-string v2, "%1$s exists as a file, expected it to be a directory."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    goto :goto_0
.end method

.method static getMediaProxyPort()I
    .locals 1

    .prologue
    .line 98
    sget v0, Lcom/microsoft/playready/DrmConfig;->s_HttpServerPort:I

    return v0
.end method

.method static getPlaylistFormattingOverride()Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/microsoft/playready/DrmConfig;->s_HLSPlaylistFormattingOverride:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    return-object v0
.end method

.method static initDrmPath(Landroid/content/Context;)V
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 89
    const-string v0, "drm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/playready/DrmConfig;->setDrmPathOverride(Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public static setActivationUrlOverride(Ljava/lang/String;)V
    .locals 2
    .param p0, "urlOverride"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 140
    sget-object v1, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/playready/DrmConfig;->_method_2(Ljava/lang/String;)V

    .line 140
    monitor-exit v1

    .line 144
    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static setAndroidId(Landroid/content/Context;)V
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 73
    const/16 v3, 0xa

    new-array v0, v3, [C

    fill-array-data v0, :array_0

    .line 75
    .local v0, "data":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-lt v1, v3, :cond_0

    .line 79
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    .line 82
    .local v2, "temp":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    monitor-enter v4

    .line 84
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/playready/DrmConfig;->_method_3(Ljava/lang/String;)V

    .line 82
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    return-void

    .line 77
    .end local v2    # "temp":Ljava/lang/String;
    :cond_0
    aget-char v3, v0, v1

    xor-int/lit8 v3, v3, 0x7a

    int-to-char v3, v3

    aput-char v3, v0, v1

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    .restart local v2    # "temp":Ljava/lang/String;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 73
    nop

    :array_0
    .array-data 2
        0x1bs
        0x14s
        0x1es
        0x8s
        0x15s
        0x13s
        0x1es
        0x25s
        0x13s
        0x1es
    .end array-data
.end method

.method public static setDrmPathOverride(Ljava/lang/String;)V
    .locals 2
    .param p0, "drmPath"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 153
    invoke-static {p0}, Lcom/microsoft/playready/DrmConfig;->checkAndCreatePath(Ljava/lang/String;)V

    .line 156
    sget-object v1, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 158
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/playready/DrmConfig;->_method_1(Ljava/lang/String;)V

    .line 156
    monitor-exit v1

    .line 160
    return-void

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setMediaProxyPort(I)V
    .locals 0
    .param p0, "portNumber"    # I

    .prologue
    .line 93
    sput p0, Lcom/microsoft/playready/DrmConfig;->s_HttpServerPort:I

    .line 94
    return-void
.end method

.method public static setPlaylistFormattingOverride(Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;)V
    .locals 0
    .param p0, "formattingOverride"    # Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .prologue
    .line 102
    sput-object p0, Lcom/microsoft/playready/DrmConfig;->s_HLSPlaylistFormattingOverride:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 103
    return-void
.end method
