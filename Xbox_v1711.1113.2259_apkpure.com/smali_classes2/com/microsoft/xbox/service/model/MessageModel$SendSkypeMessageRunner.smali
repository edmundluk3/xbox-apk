.class Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendSkypeMessageRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final recipientId:Ljava/lang/String;

.field private final request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "recipientId"    # Ljava/lang/String;
    .param p4, "request"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .prologue
    .line 980
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 981
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 982
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->recipientId:Ljava/lang/String;

    .line 983
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .line 984
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 988
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 989
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 990
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 991
    .local v2, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 992
    .local v4, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;>;"
    sget-object v5, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->name()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 993
    new-instance v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->recipientId:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 994
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v5

    invoke-interface {v5, v0, v2, v4}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->getUserPermissions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    move-result-object v3

    .line 995
    .local v3, "response":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$600(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v1

    .line 998
    .local v1, "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    if-eqz v1, :cond_0

    .line 999
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x26e8

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5

    .line 1002
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    const-string v6, "8:xbox:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->recipientId:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->getSendSkypeMessageRequestBody(Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendSkypeMessage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    return-object v5
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 973
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1034
    const-wide/16 v0, 0xbec

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1011
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 1012
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessageError(J)V

    .line 1014
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->messagetype:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1015
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->messagetype:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 1016
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel$4;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1026
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$900()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SendSkypeMessageRunner onPostExecute - unsupported request type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 1018
    .restart local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v3, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->recipientId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$700(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    goto :goto_0

    .line 1021
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v3, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeMessageRunner;->recipientId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$800(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    goto :goto_0

    .line 1016
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1007
    return-void
.end method
