.class public interface abstract Lcom/microsoft/xbox/service/clubs/IClubModerationService;
.super Ljava/lang/Object;
.source "IClubModerationService.java"


# virtual methods
.method public abstract batchDeleteItemReports(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getReportedItems(J)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract reportItem(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
