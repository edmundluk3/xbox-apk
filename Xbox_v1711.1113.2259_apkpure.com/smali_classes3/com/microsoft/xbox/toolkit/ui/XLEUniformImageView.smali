.class public Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageView;
.source "XLEUniformImageView.java"


# static fields
.field public static final FIX_HEIGHT:I = 0x2

.field public static final FIX_WIDTH:I = 0x1


# instance fields
.field private aspectRatio:F

.field private currentErrorResourceId:I

.field private currentLoadingResourceId:I

.field private currentUri:Ljava/lang/String;

.field private fixHeight:Z

.field private fixWidth:Z

.field private maxHeight:I

.field private maxWidth:I

.field private measuredHeight:I

.field private measuredWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;)V

    .line 18
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    .line 19
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    .line 28
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentLoadingResourceId:I

    .line 29
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentErrorResourceId:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    .line 34
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setSoundEffectsEnabled(Z)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    .line 19
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    .line 28
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentLoadingResourceId:I

    .line 29
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentErrorResourceId:I

    .line 30
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    .line 40
    sget-object v4, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEUniformImageView:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 43
    .local v1, "fixDimension":I
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 44
    .local v3, "maxWidth":I
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 46
    .local v2, "maxHeight":I
    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    invoke-virtual {p0, v1, v3, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setFixDimension(III)V

    .line 51
    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 52
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setSoundEffectsEnabled(Z)V

    .line 53
    return-void
.end method

.method private bindIfPossible()V
    .locals 3

    .prologue
    .line 155
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredHeight:I

    if-lez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentLoadingResourceId:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentErrorResourceId:I

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 158
    :cond_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private calculateAspectRatioFromResource(I)V
    .locals 3
    .param p1, "errorResourceId"    # I

    .prologue
    .line 72
    if-lez p1, :cond_0

    .line 73
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 74
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 75
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 76
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    .line 78
    .end local v0    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    return-void
.end method

.method private sizeToAR(II)[I
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    const/4 v5, 0x0

    .line 162
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    .line 163
    move v2, p2

    .line 164
    .local v2, "newHeight":I
    move v3, p1

    .line 169
    .local v3, "newWidth":I
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    .line 171
    .local v0, "bmpAspectRatio":F
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 172
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->aspectRatio:F

    .line 175
    :cond_0
    int-to-float v4, p2

    int-to-float v5, p1

    div-float v1, v4, v5

    .line 177
    .local v1, "destAspectRatio":F
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    if-eqz v4, :cond_3

    .line 179
    cmpg-float v4, v1, v0

    if-gez v4, :cond_2

    .line 181
    move v2, p2

    .line 182
    int-to-float v4, p2

    div-float/2addr v4, v0

    float-to-int v3, v4

    .line 200
    :cond_1
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v3, v4, v5

    const/4 v5, 0x1

    aput v2, v4, v5

    .line 203
    .end local v0    # "bmpAspectRatio":F
    .end local v1    # "destAspectRatio":F
    .end local v2    # "newHeight":I
    .end local v3    # "newWidth":I
    :goto_1
    return-object v4

    .line 185
    .restart local v0    # "bmpAspectRatio":F
    .restart local v1    # "destAspectRatio":F
    .restart local v2    # "newHeight":I
    .restart local v3    # "newWidth":I
    :cond_2
    move v3, p1

    .line 186
    int-to-float v4, p1

    mul-float/2addr v4, v0

    float-to-int v2, v4

    goto :goto_0

    .line 188
    :cond_3
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    if-eqz v4, :cond_4

    .line 190
    move v3, p1

    .line 191
    int-to-float v4, p1

    mul-float/2addr v4, v0

    float-to-int v2, v4

    goto :goto_0

    .line 192
    :cond_4
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    if-eqz v4, :cond_1

    .line 194
    move v2, p2

    .line 195
    int-to-float v4, p2

    div-float/2addr v4, v0

    float-to-int v3, v4

    goto :goto_0

    .line 203
    .end local v0    # "bmpAspectRatio":F
    .end local v1    # "destAspectRatio":F
    .end local v2    # "newHeight":I
    .end local v3    # "newWidth":I
    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, -0x1

    .line 118
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v1, v2, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    if-eqz v1, :cond_2

    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, v2, :cond_2

    .line 120
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 121
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 123
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->onMeasure(II)V

    .line 125
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxWidth:I

    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxHeight:I

    .line 135
    :cond_2
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxWidth:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLeftPaddingOffset()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getRightPaddingOffset()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxHeight:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getTopPaddingOffset()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getBottomPaddingOffset()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->sizeToAR(II)[I

    move-result-object v0

    .line 137
    .local v0, "size":[I
    if-eqz v0, :cond_3

    .line 138
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getMeasuredWidth()I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredWidth:I

    .line 139
    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getMeasuredHeight()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredHeight:I

    .line 146
    :cond_3
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredWidth:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setMeasuredDimension(II)V

    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->bindIfPossible()V

    .line 152
    return-void

    .line 138
    :cond_4
    const/4 v1, 0x0

    aget v1, v0, v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getLeftPaddingOffset()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getRightPaddingOffset()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 139
    :cond_5
    const/4 v1, 0x1

    aget v1, v0, v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getTopPaddingOffset()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->getBottomPaddingOffset()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_1
.end method

.method public setFixDimension(III)V
    .locals 2
    .param p1, "fixFlag"    # I
    .param p2, "maxWidth"    # I
    .param p3, "maxHeight"    # I

    .prologue
    const/4 v1, 0x1

    .line 56
    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixWidth:Z

    .line 59
    :cond_0
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->fixHeight:Z

    .line 63
    :cond_1
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxWidth:I

    .line 64
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->maxHeight:I

    .line 65
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->calculateAspectRatioFromResource(I)V

    .line 104
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredWidth:I

    .line 105
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredHeight:I

    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->requestLayout()V

    .line 108
    return-void
.end method

.method public setImageURI2(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "defaultResourceId"    # I

    .prologue
    .line 68
    invoke-virtual {p0, p1, p2, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 69
    return-void
.end method

.method public setImageURI2(Ljava/lang/String;II)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "loadingResourceId"    # I
    .param p3, "errorResourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 81
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentLoadingResourceId:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentErrorResourceId:I

    if-eq p3, v0, :cond_2

    .line 83
    :cond_1
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->calculateAspectRatioFromResource(I)V

    .line 85
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentLoadingResourceId:I

    .line 86
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentErrorResourceId:I

    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->currentUri:Ljava/lang/String;

    .line 90
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredWidth:I

    .line 91
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->measuredHeight:I

    .line 93
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->bindIfPossible()V

    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->requestLayout()V

    .line 97
    :cond_2
    return-void
.end method
