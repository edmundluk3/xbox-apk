.class public Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
.super Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;
.source "ActivityAlertListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityAlertItemViewHolder"
.end annotation


# instance fields
.field private contentTextView:Landroid/widget/TextView;

.field private gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamertagTextView:Landroid/widget/TextView;

.field private realNameTextView:Landroid/widget/TextView;

.field private timestampTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;-><init>()V

    return-void
.end method

.method private getActivityAlertText(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "count"    # J

    .prologue
    const-wide/16 v6, 0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 170
    .end local p1    # "gamertag":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 162
    .restart local p1    # "gamertag":Ljava/lang/String;
    :cond_0
    cmp-long v0, p2, v6

    if-nez v0, :cond_1

    .line 163
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 166
    :cond_1
    cmp-long v0, p2, v6

    if-lez v0, :cond_2

    .line 167
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 170
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private getCommentText(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;)Ljava/lang/String;
    .locals 6
    .param p1, "commentEntity"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 145
    if-eqz p1, :cond_1

    .line 146
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->isCommentOnMeActivity(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->ownerGamertag:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->text:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 150
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0700ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->text:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static inflateView(Landroid/content/Context;)Landroid/view/View;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 56
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030022

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;-><init>()V

    .line 59
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;
    const v3, 0x7f0e011e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 60
    const v3, 0x7f0e011f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->realNameTextView:Landroid/widget/TextView;

    .line 61
    const v3, 0x7f0e0120

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->timestampTextView:Landroid/widget/TextView;

    .line 62
    const v3, 0x7f0e011c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 63
    const v3, 0x7f0e0121

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    .line 65
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 67
    return-object v1
.end method

.method private isCommentOnMeActivity(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;)Z
    .locals 2
    .param p1, "activeEntity"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    .prologue
    .line 174
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->ownerXuid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->ownerXuid:Ljava/lang/String;

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    .line 175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public updateUI(Ljava/lang/Object;)V
    .locals 14
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 71
    if-eqz p1, :cond_3

    .line 73
    const/4 v0, 0x0

    .local v0, "actionText":Ljava/lang/String;
    move-object v7, v0

    .local v7, "timestamp":Ljava/lang/Object;
    move-object v5, v0

    .local v5, "realName":Ljava/lang/Object;
    move-object v3, v0

    .line 74
    .local v3, "gamerTag":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 75
    .local v2, "gamerPic":Ljava/lang/String;
    const/4 v4, 0x0

    .line 77
    .local v4, "isNew":Z
    const/4 v6, 0x0

    .line 78
    .local v6, "timeStampNarratorContent":Ljava/lang/String;
    instance-of v9, p1, Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v9, :cond_4

    move-object v9, p1

    .line 80
    check-cast v9, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v3

    .local v3, "gamerTag":Ljava/lang/String;
    move-object v9, p1

    .line 81
    check-cast v9, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v5

    .local v5, "realName":Ljava/lang/String;
    move-object v9, p1

    .line 82
    check-cast v9, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v9

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v7

    .local v7, "timestamp":Ljava/lang/String;
    move-object v9, p1

    .line 83
    check-cast v9, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateNarratorContentUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 84
    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700ac

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v9, p1

    .line 85
    check-cast v9, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v8

    .line 86
    .local v8, "url":Ljava/lang/String;
    move-object v2, v8

    .line 87
    check-cast p1, Lcom/microsoft/xbox/service/model/FollowersData;

    .end local p1    # "item":Ljava/lang/Object;
    iget-boolean v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 116
    .end local v3    # "gamerTag":Ljava/lang/String;
    .end local v5    # "realName":Ljava/lang/String;
    .end local v7    # "timestamp":Ljava/lang/String;
    .end local v8    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-static {v9, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 117
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->realNameTextView:Landroid/widget/TextView;

    invoke-static {v9, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 118
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->timestampTextView:Landroid/widget/TextView;

    invoke-static {v9, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 119
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->timestampTextView:Landroid/widget/TextView;

    invoke-static {v9, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 120
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    invoke-static {v9, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 121
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v9, :cond_1

    .line 122
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    const v10, 0x7f020125

    const v11, 0x7f020125

    invoke-virtual {v9, v2, v10, v11}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 126
    :cond_1
    if-eqz v4, :cond_9

    .line 127
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    if-eqz v9, :cond_2

    .line 128
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0142

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    :cond_2
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    if-eqz v9, :cond_3

    .line 131
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0142

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 142
    .end local v0    # "actionText":Ljava/lang/String;
    .end local v2    # "gamerPic":Ljava/lang/String;
    .end local v4    # "isNew":Z
    .end local v6    # "timeStampNarratorContent":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 88
    .restart local v0    # "actionText":Ljava/lang/String;
    .restart local v2    # "gamerPic":Ljava/lang/String;
    .local v3, "gamerTag":Ljava/lang/Object;
    .restart local v4    # "isNew":Z
    .local v5, "realName":Ljava/lang/Object;
    .restart local v6    # "timeStampNarratorContent":Ljava/lang/String;
    .local v7, "timestamp":Ljava/lang/Object;
    .restart local p1    # "item":Ljava/lang/Object;
    :cond_4
    instance-of v9, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v9, :cond_0

    move-object v9, p1

    .line 90
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 91
    const/4 v1, 0x0

    .local v1, "activeEntity":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;
    move-object v9, p1

    .line 92
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v10, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    move-object v9, p1

    .line 93
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v1, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    .line 94
    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b0

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_5
    :goto_2
    if-eqz v1, :cond_6

    .line 104
    iget-object v10, v1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->gamertag:Ljava/lang/String;

    move-object v9, p1

    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-wide v12, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->count:J

    invoke-direct {p0, v10, v12, v13}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->getActivityAlertText(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    .line 105
    .local v3, "gamerTag":Ljava/lang/String;
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->date:Ljava/util/Date;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v7

    .line 106
    .local v7, "timestamp":Ljava/lang/String;
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->date:Ljava/util/Date;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateNarratorContentUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .end local v1    # "activeEntity":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;
    .end local v3    # "gamerTag":Ljava/lang/String;
    .end local v7    # "timestamp":Ljava/lang/String;
    :cond_6
    move-object v9, p1

    .line 109
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v5, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->realName:Ljava/lang/String;

    .local v5, "realName":Ljava/lang/String;
    move-object v9, p1

    .line 110
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v8, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->profileImageUrl:Ljava/lang/String;

    .line 111
    .restart local v8    # "url":Ljava/lang/String;
    move-object v2, v8

    .line 112
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .end local p1    # "item":Ljava/lang/Object;
    iget-boolean v4, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    goto/16 :goto_0

    .end local v8    # "url":Ljava/lang/String;
    .restart local v1    # "activeEntity":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;
    .local v3, "gamerTag":Ljava/lang/Object;
    .local v5, "realName":Ljava/lang/Object;
    .local v7, "timestamp":Ljava/lang/Object;
    .restart local p1    # "item":Ljava/lang/Object;
    :cond_7
    move-object v9, p1

    .line 95
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v10, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    move-object v9, p1

    .line 96
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v1, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    .line 97
    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v10, 0x7f0700b2

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_8
    move-object v9, p1

    .line 98
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v9, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v10, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    move-object v9, p1

    .line 99
    check-cast v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    iget-object v1, v9, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    .line 100
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->getCommentText(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 134
    .end local v1    # "activeEntity":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;
    .end local v3    # "gamerTag":Ljava/lang/Object;
    .end local v5    # "realName":Ljava/lang/Object;
    .end local v7    # "timestamp":Ljava/lang/Object;
    .end local p1    # "item":Ljava/lang/Object;
    :cond_9
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    if-eqz v9, :cond_a

    .line 135
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->gamertagTextView:Landroid/widget/TextView;

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0139

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 137
    :cond_a
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    if-eqz v9, :cond_3

    .line 138
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter$ActivityAlertItemViewHolder;->contentTextView:Landroid/widget/TextView;

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f0c0139

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method
