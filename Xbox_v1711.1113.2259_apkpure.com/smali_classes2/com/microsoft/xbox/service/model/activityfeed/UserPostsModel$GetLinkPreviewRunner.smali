.class Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "UserPostsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetLinkPreviewRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final previewLink:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "previewLink"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 162
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;->previewLink:Ljava/lang/String;

    .line 163
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;->previewLink:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->getPostPreview(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;->buildData()Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 180
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 167
    return-void
.end method
