.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;
.super Ljava/lang/Object;
.source "UTCConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;,
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    }
.end annotation


# static fields
.field private static PARTCVERSION:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->PARTCVERSION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->PARTCVERSION:I

    return v0
.end method

.method public static trackAutomaticallyConnect(Z)V
    .locals 1
    .param p0, "autoConnect"    # Z

    .prologue
    .line 218
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$8;-><init>(Z)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 224
    return-void
.end method

.method public static trackCloseConsoleManager()V
    .locals 1

    .prologue
    .line 292
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$15;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$15;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 298
    return-void
.end method

.method public static trackConnectSuccess(JZZZILjava/lang/String;Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 14
    .param p0, "pairLatencyMS"    # J
    .param p2, "isManual"    # Z
    .param p3, "isIpAddressEnteredManually"    # Z
    .param p4, "isRetry"    # Z
    .param p5, "statusCode"    # I
    .param p6, "startTimeUTCString"    # Ljava/lang/String;
    .param p7, "sessionId"    # Ljava/util/UUID;
    .param p8, "consoleLocale"    # Ljava/lang/String;
    .param p9, "consoleOSVersion"    # Ljava/lang/String;
    .param p10, "tvProviderId"    # I

    .prologue
    .line 96
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;

    move-wide v2, p0

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p2

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p7

    move-object/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;-><init>(JZZZILjava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/lang/String;I)V

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 137
    return-void
.end method

.method public static trackConnectToXbox()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$5;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$5;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 187
    return-void
.end method

.method public static trackConnectoToXbox()V
    .locals 1

    .prologue
    .line 265
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$12;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$12;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 271
    return-void
.end method

.method public static trackDisconnect()V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$4;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 174
    return-void
.end method

.method public static trackDisconnectSuccess(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;Ljava/lang/String;J)V
    .locals 2
    .param p0, "mode"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "duration"    # J

    .prologue
    .line 148
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;

    invoke-direct {v0, p1, p2, p3, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$3;-><init>(Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 161
    return-void
.end method

.method public static trackDiscoveredConsoles(I)V
    .locals 1
    .param p0, "consoleCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 232
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$9;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 243
    return-void
.end method

.method public static trackForgetConsole()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$7;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 211
    return-void
.end method

.method public static trackMRUAction(Ljava/lang/String;I)V
    .locals 1
    .param p0, "productId"    # Ljava/lang/String;
    .param p1, "index"    # I

    .prologue
    .line 71
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$1;-><init>(Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 81
    return-void
.end method

.method public static trackNavigateToConsoleManager()V
    .locals 1

    .prologue
    .line 256
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$11;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 262
    return-void
.end method

.method public static trackSetupConsole()V
    .locals 1

    .prologue
    .line 274
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$13;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 280
    return-void
.end method

.method public static trackSwitchConsole()V
    .locals 1

    .prologue
    .line 283
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$14;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$14;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 289
    return-void
.end method

.method public static trackTurnOff()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$6;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$6;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 199
    return-void
.end method

.method public static trackViewConsoleManager()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$10;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$10;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 253
    return-void
.end method
