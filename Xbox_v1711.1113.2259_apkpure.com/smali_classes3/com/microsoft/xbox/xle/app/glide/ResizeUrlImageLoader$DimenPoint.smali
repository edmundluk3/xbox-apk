.class public abstract Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
.super Ljava/lang/Object;
.source "ResizeUrlImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "DimenPoint"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(II)Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    .locals 8
    .param p0, "w"    # I
    .param p1, "h"    # I

    .prologue
    .line 461
    new-instance v0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;

    mul-int v3, p0, p1

    int-to-double v4, p0

    int-to-double v6, p1

    div-double/2addr v4, v6

    move v1, p0

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;-><init>(IIID)V

    return-object v0
.end method


# virtual methods
.method public abstract area()I
.end method

.method public abstract aspectRatio()D
.end method

.method public distanceTo(Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;)D
    .locals 8
    .param p1, "other"    # Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->area()I

    move-result v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->area()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-double v0, v4

    .line 466
    .local v0, "x":D
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->aspectRatio()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->aspectRatio()D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 467
    .local v2, "y":D
    mul-double v4, v0, v0

    mul-double v6, v2, v2

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public abstract height()I
.end method

.method public abstract width()I
.end method
