.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;
.super Ljava/lang/Enum;
.source "UTCChangeRelationship.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Relationship"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

.field public static final enum ADDFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

.field public static final enum EXISTINGFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

.field public static final enum NOTCHANGED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

.field public static final enum REMOVEFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    const-string v1, "ADDFRIEND"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->ADDFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    const-string v1, "REMOVEFRIEND"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->REMOVEFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    const-string v1, "EXISTINGFRIEND"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->EXISTINGFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    const-string v1, "NOTCHANGED"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->NOTCHANGED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->ADDFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->REMOVEFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->EXISTINGFRIEND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->NOTCHANGED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->value:I

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;->value:I

    return v0
.end method
