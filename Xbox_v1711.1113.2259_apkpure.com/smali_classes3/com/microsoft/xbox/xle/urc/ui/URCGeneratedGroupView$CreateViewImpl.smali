.class Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;
.super Ljava/lang/Object;
.source "URCGeneratedGroupView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CreateViewImpl"
.end annotation


# instance fields
.field private layout:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "layout"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;->layout:I

    .line 43
    return-void
.end method

.method public static createLayoutParams(Landroid/content/Context;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    .line 52
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 53
    .local v1, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v0, v2

    .line 54
    .local v0, "margin":I
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 55
    return-object v1
.end method


# virtual methods
.method public create(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;->layout:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 47
    .local v0, "v":Landroid/view/View;
    invoke-static {p1}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;->createLayoutParams(Landroid/content/Context;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    return-object v0
.end method
