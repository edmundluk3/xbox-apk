.class final Lcom/microsoft/xbox/xle/app/LaunchUtils$4;
.super Ljava/lang/Object;
.source "LaunchUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$count:I

.field final synthetic val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;


# direct methods
.method constructor <init>(ILcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 0

    .prologue
    .line 295
    iput p1, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;->val$count:I

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 299
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;->val$count:I

    add-int/lit8 v1, v1, 0x1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_0
    return-void

    .line 300
    :catch_0
    move-exception v7

    .line 301
    .local v7, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "could not find CanvasWebViewActivity.class in stack"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
