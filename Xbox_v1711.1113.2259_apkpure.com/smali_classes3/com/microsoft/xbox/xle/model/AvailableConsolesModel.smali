.class public Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "AvailableConsolesModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;,
        Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final AUTOCONNECT_FILE_NAME:Ljava/lang/String; = "autoconnect_v1.json"

.field private static final TIME_OUT:I = 0x2710


# instance fields
.field private consolesToDisplay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field protected isDiscovering:Z

.field private lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field protected lifetime:J

.field private neverAutoConnectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation
.end field

.field private preferedAutoConnectConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isDiscovering:Z

    .line 48
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lifetime:J

    .line 60
    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    .line 62
    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->preferedAutoConnectConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 63
    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->clearObservers()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p3, "x3"    # Ljava/util/List;
    .param p4, "x4"    # Ljava/util/List;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->onDiscoverConsolesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->access$000()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$persistAutoConnectStates$1(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    .prologue
    .line 360
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "autoconnect_v1.json"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 361
    .local v0, "autoconnectFile":Ljava/io/File;
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 362
    .local v2, "writer":Ljava/io/FileWriter;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 363
    const-string v3, "AvailableConsolesModel"

    const-string v4, "neverlist wrote to file"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    .end local v0    # "autoconnectFile":Ljava/io/File;
    .end local v2    # "writer":Ljava/io/FileWriter;
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AvailableConsolesModel"

    const-string v4, "Failed to marshall json: "

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$reset$0()V
    .locals 4

    .prologue
    .line 186
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "autoconnect_v1.json"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 187
    .local v0, "autoconnectFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "AvailableConsolesModel"

    const-string v3, "Failed to delete file"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private onDiscoverConsolesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/model/ConsoleData;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p2, "prefered"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    .local p3, "discovered":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    .local p4, "autoList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 124
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 126
    const/4 v0, 0x0

    .line 127
    .local v0, "consoleCount":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v4, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->sortDiscoveredConsoles(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    .line 129
    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->preferedAutoConnectConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 130
    iput-object p4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 134
    const-string v1, "AvailableConsolesModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Console Discover completed. Consoles: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 140
    :goto_1
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isLoading:Z

    .line 142
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackDiscoveredConsoles(I)V

    .line 144
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v4, v2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v1, v3, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 145
    return-void

    .end local v0    # "consoleCount":I
    :cond_0
    move v1, v3

    .line 124
    goto :goto_0

    .line 137
    .restart local v0    # "consoleCount":I
    :cond_1
    const-string v1, "AvailableConsolesModel"

    const-string v4, "failed to discover consoles"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private persistAutoConnectStates()V
    .locals 3

    .prologue
    .line 353
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 354
    const-string v1, "AvailableConsoleModel"

    const-string v2, "never list is null, you should not persist anything"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :goto_0
    return-void

    .line 358
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$2;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 370
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0
.end method

.method public static reset()V
    .locals 3

    .prologue
    .line 184
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$$Lambda$1;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$1;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 196
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$AvailableConsolesModelContainer;->access$300()V

    .line 198
    return-void
.end method

.method private sortDiscoveredConsoles(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "discovered":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v3, "sleepingConsoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v2, "previouslyConnectedConsoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v1, "consolesNeverConnected":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/model/ConsoleData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 152
    .local v0, "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    iget-wide v6, v0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 153
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 155
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .end local v0    # "console":Lcom/microsoft/xbox/xle/model/ConsoleData;
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 162
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 163
    return-object v2
.end method


# virtual methods
.method public addConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 4
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 92
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isLoading:Z

    .line 93
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 94
    return-void
.end method

.method public getDefaultConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->preferedAutoConnectConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method public getDiscoveredConsoles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/model/ConsoleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    return-object v0
.end method

.method public getIsDiscovering()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->isDiscovering:Z

    return v0
.end method

.method public getLastConnectedConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method public loadAsync(Z)V
    .locals 5
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 169
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    if-nez v1, :cond_1

    :goto_1
    invoke-direct {v0, p0, p0, v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Z)V

    .line 172
    .local v0, "discoverConsolesRunner":Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Discover"

    const-string v3, "Connection - Connection View"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ConsoleDiscovery:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {p0, p1, v1, v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 175
    return-void

    .end local v0    # "discoverConsolesRunner":Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    :cond_0
    move v1, v3

    .line 168
    goto :goto_0

    :cond_1
    move v2, v3

    .line 169
    goto :goto_1
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v0, p0, p0, v1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;-><init>(Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;Z)V

    .line 179
    .local v0, "discoverConsolesRunner":Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1

    .line 178
    .end local v0    # "discoverConsolesRunner":Lcom/microsoft/xbox/xle/model/AvailableConsolesModel$DiscoverConsolesRunner;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeDiscoveredConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 2
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 97
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->consolesToDisplay:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->forgetDevice(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public setLastConnectedConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 3
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 108
    if-nez p1, :cond_0

    .line 109
    const-string v0, "AvailableConsoleModel"

    const-string v1, "reset last connected console to null; "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 117
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    goto :goto_0

    .line 114
    :cond_1
    const-string v0, "AvailableConsoleModel"

    const-string v1, "last connected console is not auto connect, ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iput-object v2, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->lastConnectedConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    goto :goto_0
.end method

.method public updateAutoConnectState(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 7
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 375
    const/4 v1, -0x1

    .line 376
    .local v1, "found":I
    const/4 v3, 0x0

    .line 378
    .local v3, "shouldSave":Z
    const-string v4, "AvailableConsolesModel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update console auto connect state "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 381
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 382
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 383
    .local v0, "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 384
    move v1, v2

    .line 390
    .end local v0    # "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    .end local v2    # "i":I
    :cond_0
    if-ltz v1, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 391
    const/4 v3, 0x1

    .line 392
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 403
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 404
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->persistAutoConnectStates()V

    .line 406
    :cond_2
    return-void

    .line 381
    .restart local v0    # "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    .restart local v2    # "i":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 394
    .end local v0    # "c":Lcom/microsoft/xbox/xle/model/ConsoleData;
    .end local v2    # "i":I
    :cond_4
    if-gez v1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->isAutoConnect()Z

    move-result v4

    if-nez v4, :cond_1

    .line 396
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    if-nez v4, :cond_5

    .line 397
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    .line 399
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->neverAutoConnectList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    const/4 v3, 0x1

    goto :goto_1
.end method
