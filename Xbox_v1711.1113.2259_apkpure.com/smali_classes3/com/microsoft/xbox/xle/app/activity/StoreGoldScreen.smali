.class public Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "StoreGoldScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;

    .line 19
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "Store - Gold"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;->onCreateContentView()V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen$StoreGoldAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 28
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->setAsPivotPane()V

    .line 29
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->onCreate()V

    .line 31
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 32
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f030215

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/StoreGoldScreen;->setContentView(I)V

    .line 37
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
