.class public final Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;
.super Ljava/lang/Object;
.source "PartyInteractor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/domain/party/PartyInteractor;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final partyChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private final userSummaryRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p2, "userSummaryRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p3, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-boolean v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->userSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 31
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p1, "userSummaryRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p2, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .locals 4

    .prologue
    .line 35
    new-instance v3, Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 36
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->userSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 37
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 38
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-direct {v3, v0, v1, v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;)V

    .line 35
    return-object v3
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->get()Lcom/microsoft/xbox/domain/party/PartyInteractor;

    move-result-object v0

    return-object v0
.end method
