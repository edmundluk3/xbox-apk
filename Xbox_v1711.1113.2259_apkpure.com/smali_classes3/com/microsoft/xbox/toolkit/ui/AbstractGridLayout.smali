.class public abstract Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;
.super Landroid/widget/GridLayout;
.source "AbstractGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

.field private mAdapterDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public getGridAdapter()Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    return-object v0
.end method

.method public abstract notifyDataChanged()V
.end method

.method public setGridAdapter(Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapterDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapterDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->onDestory()V

    .line 49
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;-><init>(Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapterDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapter:Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->mAdapterDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout$AdapterDataSetObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->notifyDataChanged()V

    .line 57
    :cond_1
    return-void
.end method
