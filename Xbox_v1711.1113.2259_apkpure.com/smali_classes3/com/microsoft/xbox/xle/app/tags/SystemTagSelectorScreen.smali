.class public Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "SystemTagSelectorScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070700

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;->onCreateContentView()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f030228

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;->setContentView(I)V

    .line 33
    return-void
.end method
