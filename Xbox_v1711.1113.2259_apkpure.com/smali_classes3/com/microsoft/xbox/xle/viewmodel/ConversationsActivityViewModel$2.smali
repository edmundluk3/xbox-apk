.class Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;
.super Ljava/lang/Object;
.source "ConversationsActivityViewModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->deleteSkypeConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

.field final synthetic val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ConversationDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel$2;->val$summary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->deleteSkypeConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 212
    return-void
.end method
