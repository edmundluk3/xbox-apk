.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;
.source "AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest.java"


# instance fields
.field private final roles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p1, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null roles"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;->roles:Lcom/google/common/collect/ImmutableList;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 34
    if-ne p1, p0, :cond_0

    .line 35
    const/4 v1, 0x1

    .line 41
    :goto_0
    return v1

    .line 37
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;

    .line 39
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;->roles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 41
    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x1

    .line 47
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;->roles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 49
    return v0
.end method

.method public roles()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;->roles:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubMembersRoleRequest{roles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMembersRoleRequest;->roles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
