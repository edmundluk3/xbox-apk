.class public Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "TagRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TagItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
        ">;"
    }
.end annotation


# instance fields
.field private achievementLogo:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private cancel:Landroid/widget/TextView;

.field private tag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private tagContainer:Landroid/widget/LinearLayout;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 166
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 168
    const v0, 0x7f0e0aa0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0aa2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0aa3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->cancel:Landroid/widget/TextView;

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0aa1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->achievementLogo:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 172
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "dataObject"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 192
    invoke-interface {p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;->onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 194
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 8
    .param p1, "dataObject"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 176
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 178
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 180
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 181
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 184
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v2

    .line 186
    .local v2, "tagBackgroundColor":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getDisplayText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 187
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->achievementLogo:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-interface {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getSymbol()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 189
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

    move-result-object v1

    .line 190
    .local v1, "listener":Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
    if-eqz v1, :cond_1

    .line 191
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    invoke-static {p0, v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    invoke-interface {v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;->isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 197
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I

    move-result v2

    .line 201
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->cancel:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 203
    new-instance v0, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    .line 204
    .local v0, "backgroundDrawable":Landroid/graphics/drawable/PaintDrawable;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadius(F)V

    .line 205
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->tagContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 206
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 159
    check-cast p1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;->onBind(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    return-void
.end method
