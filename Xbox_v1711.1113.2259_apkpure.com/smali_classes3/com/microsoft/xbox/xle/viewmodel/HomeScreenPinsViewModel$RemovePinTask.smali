.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "HomeScreenPinsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemovePinTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 0
    .param p2, "pin"    # Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 178
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 179
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 198
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    .line 199
    .local v0, "pm":Lcom/microsoft/xbox/service/model/pins/PinsModel;
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->remove([Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 189
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 209
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method
