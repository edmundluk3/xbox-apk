.class public Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
.super Lcom/microsoft/xbox/data/repository/PagedRepository;
.source "BeamRepository.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/data/repository/PagedRepository",
        "<",
        "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
        "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

.field private nextPageToLoad:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 2
    .param p1, "beamService"    # Lcom/microsoft/xbox/data/service/beam/BeamService;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;
    .param p3, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/data/repository/PagedRepository;-><init>(Lcom/microsoft/xbox/data/repository/DataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->nextPageToLoad:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 28
    return-void
.end method

.method static synthetic lambda$loadNextPage$0(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Ljava/util/List;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "ignore"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->nextPageToLoad:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    return-void
.end method


# virtual methods
.method protected hasMoreToLoad()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method protected loadNextPage()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->beamService:Lcom/microsoft/xbox/data/service/beam/BeamService;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->nextPageToLoad:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/service/beam/BeamService;->getTrendingChannels(I)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method protected resetContinuationToken()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->nextPageToLoad:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 47
    return-void
.end method
