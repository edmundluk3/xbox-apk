.class final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest;
.super Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_SearchRequest;
.source "AutoValue_MediaHubDataTypes_SearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_SearchRequest$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .param p4, "maxPageSize"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    .local p3, "sort":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequestSort;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_SearchRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;)V

    .line 19
    return-void
.end method
