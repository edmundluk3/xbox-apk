.class public Lcom/microsoft/xbox/xle/model/ConsoleData;
.super Ljava/lang/Object;
.source "ConsoleData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/model/ConsoleData$ConsoleComparator;
    }
.end annotation


# instance fields
.field private autoConnect:Z

.field public friendlyName:Ljava/lang/String;

.field public host:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field private ipAddressManuallyEntered:Z

.field private isSleeping:Z

.field public lastConnected:J

.field public lastUpdated:J

.field public service:Ljava/lang/String;

.field private transient sessionState:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    .line 29
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->host:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    .line 30
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->service:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    .line 31
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->lastConnected:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    .line 32
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->lastUpdated:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    .line 33
    iput v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->sessionState:I

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->autoConnect:Z

    .line 35
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 4
    .param p1, "xboxName"    # Ljava/lang/String;
    .param p2, "deviceId"    # Ljava/lang/String;
    .param p3, "ipAddress"    # Ljava/lang/String;
    .param p4, "connectionState"    # I
    .param p5, "shouldConnectAutomatically"    # Z
    .param p6, "ipAddressManuallyEntered"    # Z

    .prologue
    const-wide/16 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->service:Ljava/lang/String;

    .line 43
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    .line 44
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastUpdated:J

    .line 45
    iput p4, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->sessionState:I

    .line 46
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->autoConnect:Z

    .line 47
    iput-boolean p6, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->ipAddressManuallyEntered:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping:Z

    .line 49
    return-void
.end method

.method private getUniqueId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 135
    if-ne p0, p1, :cond_1

    .line 136
    const/4 v1, 0x1

    .line 148
    :cond_0
    :goto_0
    return v1

    .line 139
    :cond_1
    if-eqz p1, :cond_0

    .line 143
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 147
    check-cast v0, Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 148
    .local v0, "other":Lcom/microsoft/xbox/xle/model/ConsoleData;
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getIpAddressManuallyEntered()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->ipAddressManuallyEntered:Z

    return v0
.end method

.method public getLastAutoConnectDate()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    return-wide v0
.end method

.method public getSessionState()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->sessionState:I

    return v0
.end method

.method public getXboxName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getUniqueId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getUniqueId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 130
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoConnect()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->autoConnect:Z

    return v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->sessionState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSleeping()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping:Z

    return v0
.end method

.method public setAutoConnect(Z)V
    .locals 0
    .param p1, "autoConnect"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->autoConnect:Z

    .line 98
    return-void
.end method

.method public setIpAddressManuallyEntered(Z)V
    .locals 0
    .param p1, "manuallyEntered"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->ipAddressManuallyEntered:Z

    .line 118
    return-void
.end method

.method public setIsSleeping(Z)V
    .locals 0
    .param p1, "isSleeping"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->isSleeping:Z

    .line 102
    return-void
.end method

.method public setSessionState(I)V
    .locals 0
    .param p1, "sessionState"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->sessionState:I

    .line 90
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    return-object v0
.end method
