.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Sku"
.end annotation


# instance fields
.field public final properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Properties"
    .end annotation
.end field

.field public final skuId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SkuId"
    .end annotation
.end field

.field private final skuLocalizedProperties:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocalizedProperties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;",
            ">;"
        }
    .end annotation
.end field

.field public final skuType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SkuType"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "properties"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "skuId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "skuType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;",
            ">;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 629
    .local p1, "skuLocalizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 630
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 631
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 633
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuLocalizedProperties:Ljava/util/List;

    .line 634
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;

    .line 635
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuId:Ljava/lang/String;

    .line 636
    iput-object p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuType:Ljava/lang/String;

    .line 637
    return-void
.end method


# virtual methods
.method public getSkuIdInt()I
    .locals 5

    .prologue
    .line 645
    const v1, 0x7fffffff

    .line 647
    .local v1, "skuIdInt":I
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 651
    :goto_0
    return v1

    .line 648
    :catch_0
    move-exception v0

    .line 649
    .local v0, "ex":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse SkuId to int. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSkuLocalizedProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;->skuLocalizedProperties:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;

    return-object v0
.end method
