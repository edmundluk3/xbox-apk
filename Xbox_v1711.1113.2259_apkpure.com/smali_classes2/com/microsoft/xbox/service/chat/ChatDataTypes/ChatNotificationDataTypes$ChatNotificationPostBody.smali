.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;
.super Ljava/lang/Object;
.source "ChatNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChatNotificationPostBody"
.end annotation


# instance fields
.field private final allNotificationChannels:Ljava/util/Set;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;"
        }
    .end annotation
.end field

.field private final directMentionChannels:Ljava/util/Set;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;"
        }
    .end annotation
.end field

.field public final platform:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final pushChannel:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .param p1, "pushChannel"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, "allNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    .local p3, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "Android"

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->platform:Ljava/lang/String;

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 50
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->pushChannel:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->allNotificationChannels:Ljava/util/Set;

    .line 55
    iput-object p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->directMentionChannels:Ljava/util/Set;

    .line 56
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 72
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;

    if-nez v3, :cond_2

    move v1, v2

    .line 73
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 75
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;

    .line 76
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;
    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->pushChannel:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->pushChannel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->allNotificationChannels:Ljava/util/Set;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->allNotificationChannels:Ljava/util/Set;

    .line 77
    invoke-interface {v3, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->directMentionChannels:Ljava/util/Set;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->directMentionChannels:Ljava/util/Set;

    .line 78
    invoke-interface {v3, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getAllNotificationChannels()Ljava/util/Set;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->allNotificationChannels:Ljava/util/Set;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getDirectMentionChannels()Ljava/util/Set;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->directMentionChannels:Ljava/util/Set;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 84
    const/16 v0, 0x11

    .line 85
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->pushChannel:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 86
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->allNotificationChannels:Ljava/util/Set;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 87
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->directMentionChannels:Ljava/util/Set;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 89
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
