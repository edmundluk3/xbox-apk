.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvailabilityProperties"
.end annotation


# instance fields
.field public final originalReleaseDateUTC:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OriginalReleaseDate"
    .end annotation
.end field

.field private volatile transient originalReleaseeDate:Ljava/util/Date;

.field private volatile transient preOrderReleaseDate:Ljava/util/Date;

.field public final preOrderReleaseDateUTC:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PreOrderReleaseDate"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "preOrderReleaseDate"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "originalReleaseDate"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 587
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->preOrderReleaseDateUTC:Ljava/lang/String;

    .line 588
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->originalReleaseDateUTC:Ljava/lang/String;

    .line 589
    return-void
.end method


# virtual methods
.method public getOriginalReleaseDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->originalReleaseeDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->originalReleaseDateUTC:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->originalReleaseeDate:Ljava/util/Date;

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->originalReleaseeDate:Ljava/util/Date;

    return-object v0
.end method

.method public getPreOrderReleaseDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 593
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->preOrderReleaseDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->preOrderReleaseDateUTC:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->preOrderReleaseDate:Ljava/util/Date;

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;->preOrderReleaseDate:Ljava/util/Date;

    return-object v0
.end method
