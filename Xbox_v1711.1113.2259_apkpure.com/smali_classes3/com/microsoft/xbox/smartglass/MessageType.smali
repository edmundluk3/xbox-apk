.class public final enum Lcom/microsoft/xbox/smartglass/MessageType;
.super Ljava/lang/Enum;
.source "MessageType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/MessageType;

.field public static final enum Json:Lcom/microsoft/xbox/smartglass/MessageType;

.field public static final enum None:Lcom/microsoft/xbox/smartglass/MessageType;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/MessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageType;

    const-string v3, "None"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MessageType;->None:Lcom/microsoft/xbox/smartglass/MessageType;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageType;

    const-string v3, "Json"

    const/16 v4, 0x1c

    invoke-direct {v2, v3, v5, v4}, Lcom/microsoft/xbox/smartglass/MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MessageType;->Json:Lcom/microsoft/xbox/smartglass/MessageType;

    .line 10
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/MessageType;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MessageType;->None:Lcom/microsoft/xbox/smartglass/MessageType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/MessageType;->Json:Lcom/microsoft/xbox/smartglass/MessageType;

    aput-object v3, v2, v5

    sput-object v2, Lcom/microsoft/xbox/smartglass/MessageType;->$VALUES:[Lcom/microsoft/xbox/smartglass/MessageType;

    .line 17
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MessageType;->_lookup:Landroid/util/SparseArray;

    .line 20
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MessageType;->values()[Lcom/microsoft/xbox/smartglass/MessageType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/smartglass/MessageType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 21
    sget-object v4, Lcom/microsoft/xbox/smartglass/MessageType;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/MessageType;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 20
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 23
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/microsoft/xbox/smartglass/MessageType;->_value:I

    .line 27
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/MessageType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 45
    sget-object v1, Lcom/microsoft/xbox/smartglass/MessageType;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MessageType;

    .line 46
    .local v0, "type":Lcom/microsoft/xbox/smartglass/MessageType;
    if-nez v0, :cond_0

    .line 47
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 50
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MessageType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/MessageType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/MessageType;->$VALUES:[Lcom/microsoft/xbox/smartglass/MessageType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/MessageType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MessageType;->_value:I

    return v0
.end method
