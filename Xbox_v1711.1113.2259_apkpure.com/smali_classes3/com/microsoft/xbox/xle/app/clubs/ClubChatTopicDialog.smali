.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubChatTopicDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

.field private final postButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final rootView:Landroid/widget/RelativeLayout;

.field private final topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02db

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02dd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02de

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02df

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02e0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->postButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->postButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->setContentView(Landroid/view/View;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;-><init>(Landroid/content/Context;)V

    .line 39
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->updateDialog()V

    .line 44
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->dismissKeyboard()V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->dismissClubChatTopicDialog()V

    .line 109
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->dismissSelf()V

    .line 59
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clicked post button: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->dismissKeyboard()V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->setMessageOfTheDay(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatEditTopicPost(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 68
    return-void
.end method


# virtual methods
.method protected dismissKeyboard()V
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 115
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->dismissSelf()V

    .line 104
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 99
    return-void
.end method

.method public updateDialog()V
    .locals 10

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    if-eqz v5, :cond_2

    .line 78
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v6, v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 79
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f070304

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v9, v9, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    aput-object v9, v8, v3

    .line 80
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 79
    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 86
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerSetChatTopic()Z

    move-result v0

    .line 87
    .local v0, "canViewerSetChatTopic":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->postButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 89
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 90
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getWhoCanSetChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getErrorRoleStringId(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)I

    move-result v1

    .line 91
    .local v1, "roleStringId":I
    if-eqz v1, :cond_1

    .line 92
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-nez v0, :cond_3

    move v2, v4

    :goto_1
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0702f6

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 93
    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v3

    invoke-virtual {v6, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-static {v5, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 95
    :cond_1
    return-void

    .line 82
    .end local v0    # "canViewerSetChatTopic":Z
    .end local v1    # "roleStringId":I
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .restart local v0    # "canViewerSetChatTopic":Z
    .restart local v1    # "roleStringId":I
    :cond_3
    move v2, v3

    .line 92
    goto :goto_1
.end method
