.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
.super Ljava/lang/Object;
.source "PartyAndLfgListViewAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LfgListItem"
.end annotation


# instance fields
.field public volatile transient hashCode:I

.field public final host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field public final lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p1, "lfg"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "host"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 420
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 421
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 422
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 426
    if-ne p1, p0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v1

    .line 428
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 429
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 431
    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;

    .line 432
    .local v0, "other":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 433
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 439
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 440
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    .line 441
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->lfg:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    .line 442
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    .line 445
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$LfgListItem;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
