.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PersonViewHolder"
.end annotation


# instance fields
.field broadcastingIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08ea
    .end annotation
.end field

.field favoriteIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0230
    .end annotation
.end field

.field gamerPicView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03d4
    .end annotation
.end field

.field gameractivityTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08ec
    .end annotation
.end field

.field gamertagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06cb
    .end annotation
.end field

.field partyIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08eb
    .end annotation
.end field

.field presenceImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03d5
    .end annotation
.end field

.field realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06cc
    .end annotation
.end field

.field recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06cd
    .end annotation
.end field

.field recommendationIconText:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06ce
    .end annotation
.end field

.field rootView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08e9
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 204
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 205
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 14
    .param p1, "followerData"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/16 v13, 0x8

    .line 208
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 210
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .local v7, "contentDescription":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->presenceImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamerPicView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamerPicView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 219
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getSearchResultPerson()Lcom/microsoft/xbox/service/model/SearchResultPerson;

    move-result-object v2

    .line 220
    .local v2, "searchResultPerson":Lcom/microsoft/xbox/service/model/SearchResultPerson;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gamertagTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/SearchResultPerson;->GamertagMatch:Ljava/lang/String;

    :goto_0
    const v5, 0x7f080290

    const v6, 0x7f080209

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    .line 228
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 232
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_6

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/SearchResultPerson;->RealNameMatch:Ljava/lang/String;

    :goto_1
    const v5, 0x7f080291

    const v6, 0x7f08020d

    .line 229
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 237
    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :goto_2
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    if-eqz v0, :cond_a

    move-object v9, p1

    .line 244
    check-cast v9, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    .line 246
    .local v9, "recommendationsPeopleData":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gameractivityTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationFirstReason()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 247
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->SMALL:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v8

    .line 248
    .local v8, "icon":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v8, :cond_8

    move v0, v11

    :goto_3
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconText:Landroid/view/View;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->PhoneContact:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    if-ne v1, v3, :cond_9

    :goto_4
    invoke-static {v0, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 267
    .end local v8    # "icon":Ljava/lang/String;
    .end local v9    # "recommendationsPeopleData":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :goto_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->favoriteIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-boolean v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 268
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v0, :cond_2

    .line 269
    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070149

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->presenceImageView:Landroid/widget/ImageView;

    invoke-static {v0, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 271
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->favoriteIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v3, :cond_d

    const v0, 0x7f0c000c

    :goto_6
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setTextColorIfNotNull(Landroid/widget/TextView;I)V

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->partyIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-boolean v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isInParty:Z

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 275
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isInParty:Z

    if-eqz v0, :cond_3

    .line 276
    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a36

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->broadcastingIconView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-boolean v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isBroadcasting:Z

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 280
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isBroadcasting:Z

    if-eqz v0, :cond_4

    .line 281
    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070574

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 285
    return-void

    :cond_5
    move-object v4, v10

    .line 220
    goto/16 :goto_0

    :cond_6
    move-object v4, v10

    .line 232
    goto/16 :goto_1

    .line 239
    :cond_7
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->realnameTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    .restart local v8    # "icon":Ljava/lang/String;
    .restart local v9    # "recommendationsPeopleData":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_8
    move v0, v12

    .line 251
    goto/16 :goto_3

    :cond_9
    move v11, v12

    .line 252
    goto/16 :goto_4

    .line 254
    .end local v8    # "icon":Ljava/lang/String;
    .end local v9    # "recommendationsPeopleData":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_a
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->gameractivityTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    .line 257
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredDescriptionText(Lcom/microsoft/xbox/service/model/FollowersData;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_b

    iget-object v4, v2, Lcom/microsoft/xbox/service/model/SearchResultPerson;->StatusMatch:Ljava/lang/String;

    :goto_7
    const v5, 0x7f08028f

    const v6, 0x7f080206

    .line 254
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->presenceImageView:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v3, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v1, v3, :cond_c

    :goto_8
    invoke-static {v0, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v0, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->recommendationIconText:Landroid/view/View;

    invoke-static {v0, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 264
    const-string v0, ", "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFilteredDescriptionText(Lcom/microsoft/xbox/service/model/FollowersData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_b
    move-object v4, v10

    .line 257
    goto :goto_7

    :cond_c
    move v11, v12

    .line 261
    goto :goto_8

    .line 271
    :cond_d
    const v0, 0x7f0c0012

    goto/16 :goto_6
.end method
