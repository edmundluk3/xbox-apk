.class Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;
.super Ljava/lang/Object;
.source "URCPowerLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->addPowerLine(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;

.field final synthetic val$deviceID:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;->val$deviceID:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 180
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 181
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 183
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;->val$deviceID:Ljava/lang/String;

    const-string v2, "btn.power"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout$3;->val$deviceID:Ljava/lang/String;

    const-string v2, "btn.power_off"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 186
    :cond_0
    return-void
.end method
