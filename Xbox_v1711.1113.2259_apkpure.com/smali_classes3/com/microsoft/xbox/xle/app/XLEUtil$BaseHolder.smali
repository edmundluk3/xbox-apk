.class public abstract Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;
.super Ljava/lang/Object;
.source "XLEUtil.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/XLEUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseHolder"
.end annotation


# instance fields
.field private timer:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewTimer(Landroid/os/CountDownTimer;)V
    .locals 1
    .param p1, "timer"    # Landroid/os/CountDownTimer;

    .prologue
    .line 744
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 747
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    .line 748
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 752
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 756
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 758
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;->timer:Landroid/os/CountDownTimer;

    .line 760
    :cond_0
    return-void
.end method
