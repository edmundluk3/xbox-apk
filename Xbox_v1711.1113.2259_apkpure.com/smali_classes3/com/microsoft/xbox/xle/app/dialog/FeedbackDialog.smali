.class public Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "FeedbackDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final feedbackAlias:Landroid/widget/EditText;

.field private final feedbackMessage:Landroid/widget/EditText;

.field private final feedbackRateExperienceText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final feedbackRatingSection:Landroid/widget/LinearLayout;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final negativeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final positiveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final problemCheckBox:Landroid/widget/CheckBox;

.field private final sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final suggestionCheckBox:Landroid/widget/CheckBox;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    .prologue
    .line 50
    const v0, 0x7f080263

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 51
    const v0, 0x7f030103

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->setContentView(I)V

    .line 52
    const v0, 0x7f0e05a5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 53
    const v0, 0x7f0e05a9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->positiveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 54
    const v0, 0x7f0e05aa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->negativeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 55
    const v0, 0x7f0e05ab

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->problemCheckBox:Landroid/widget/CheckBox;

    .line 56
    const v0, 0x7f0e05ac

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->suggestionCheckBox:Landroid/widget/CheckBox;

    .line 57
    const v0, 0x7f0e05ad

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    .line 58
    const v0, 0x7f0e05af

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 60
    const v0, 0x7f0e05a8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackRatingSection:Landroid/widget/LinearLayout;

    .line 61
    const v0, 0x7f0e05a7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackRateExperienceText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    const v0, 0x7f0e05ae

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackAlias:Landroid/widget/EditText;

    .line 63
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->inflater:Landroid/view/LayoutInflater;

    .line 64
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->positiveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->negativeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method public static hideKeyboard(Landroid/view/View;Landroid/content/Context;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    if-eqz p0, :cond_0

    .line 79
    const-string v1, "input_method"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 81
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 83
    .end local v0    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method


# virtual methods
.method public OnAnimationInEnd()V
    .locals 0

    .prologue
    .line 197
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->OnAnimationInEnd()V

    .line 198
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->close()V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFeedbackDialog()V

    goto :goto_0
.end method

.method protected dismissKeyboard()V
    .locals 1

    .prologue
    .line 201
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 204
    :cond_0
    return-void
.end method

.method protected getBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .locals 3
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;
    .param p2, "goingBack"    # Z

    .prologue
    .line 290
    const/4 v0, 0x0

    .line 291
    .local v0, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->getDialogBody()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v1

    const-string v2, "SearchDialog"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->getDialogBody()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v0

    .line 295
    :cond_0
    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->close()V

    .line 213
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 92
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackAlias:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsCrash()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackRateExperienceText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackRatingSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->suggestionCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->problemCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 187
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->positiveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->negativeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->problemCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->suggestionCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog$8;-><init>(Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->dismissKeyboard()V

    .line 192
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 193
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 88
    return-void
.end method

.method public sendFeedback()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 246
    const/4 v2, 0x0

    .line 247
    .local v2, "messageText":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    if-eqz v7, :cond_0

    .line 248
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 251
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    if-eqz v7, :cond_2

    .line 252
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->setFeedbackMessage(Ljava/lang/String;)V

    .line 261
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getRating()Ljava/lang/String;

    move-result-object v7

    const-string v8, "smile"

    if-ne v7, v8, :cond_3

    const/4 v4, 0x5

    .line 262
    .local v4, "rating":I
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getPageName()Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "pageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 264
    .local v5, "ratingType":I
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackType()Ljava/lang/String;

    move-result-object v8

    const/4 v7, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_1
    move v6, v7

    :goto_1
    packed-switch v6, :pswitch_data_0

    .line 278
    const/4 v5, 0x0

    .line 280
    :goto_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackMessage()Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "comment":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getReferenceId()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "logData":Ljava/lang/String;
    invoke-static {v4, v5, v3, v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFeedback;->trackFeedback(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsCrash()Z

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/Feedback;->sendFeedback(ZLcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V

    .line 286
    .end local v0    # "comment":Ljava/lang/String;
    .end local v1    # "logData":Ljava/lang/String;
    .end local v3    # "pageName":Ljava/lang/String;
    .end local v4    # "rating":I
    .end local v5    # "ratingType":I
    :cond_2
    return-void

    :cond_3
    move v4, v6

    .line 261
    goto :goto_0

    .line 264
    .restart local v3    # "pageName":Ljava/lang/String;
    .restart local v4    # "rating":I
    .restart local v5    # "ratingType":I
    :sswitch_0
    const-string v6, "Problem"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :sswitch_1
    const-string v9, "Rating Only"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    goto :goto_1

    :sswitch_2
    const-string v6, "Crash"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x2

    goto :goto_1

    :sswitch_3
    const-string v6, "Suggestion"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x3

    goto :goto_1

    .line 266
    :pswitch_0
    const/4 v5, 0x1

    .line 267
    goto :goto_2

    .line 269
    :pswitch_1
    const/4 v5, 0x2

    .line 270
    goto :goto_2

    .line 272
    :pswitch_2
    const/4 v5, 0x3

    .line 273
    goto :goto_2

    .line 275
    :pswitch_3
    const/4 v5, 0x4

    .line 276
    goto :goto_2

    .line 264
    :sswitch_data_0
    .sparse-switch
        -0x41ca59dc -> :sswitch_3
        0x3e57387 -> :sswitch_2
        0x50c55a7f -> :sswitch_0
        0x780bf44f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected showKeyboard(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showKeyboard(Landroid/view/View;I)V

    .line 208
    return-void
.end method

.method public toggleSendButton()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsCrash()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 243
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getHasRatingBeenSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getHasReportTypeBeenSelected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 229
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 233
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackMessage()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getHasRatingBeenSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getHasReportTypeBeenSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 235
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    goto :goto_0

    .line 238
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "hint":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->feedbackMessage:Landroid/widget/EditText;

    const v3, 0x7f07074b

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 242
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    goto :goto_0

    .line 238
    .end local v0    # "hint":Ljava/lang/String;
    :cond_4
    const-string v0, ""

    goto :goto_1
.end method
