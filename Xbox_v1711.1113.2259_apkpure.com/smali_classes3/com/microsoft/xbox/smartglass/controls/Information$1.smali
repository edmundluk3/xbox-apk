.class Lcom/microsoft/xbox/smartglass/controls/Information$1;
.super Lcom/microsoft/xbox/smartglass/TokenListener;
.source "Information.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/Information;->getUserPrivileges(ILcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

.field final synthetic val$currentParameters:Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;

.field final synthetic val$currentRequestId:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/Information;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentParameters:Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;

    iput p3, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentRequestId:I

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/TokenListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Lcom/microsoft/xbox/smartglass/SGResult;Lcom/microsoft/xbox/smartglass/Token;)V
    .locals 5
    .param p1, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;
    .param p2, "token"    # Lcom/microsoft/xbox/smartglass/Token;

    .prologue
    .line 166
    const-string v0, ""

    .line 167
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/SGResult;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    const-string v1, "Failed to get user privileges for \'%s\'."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentParameters:Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;

    iget-object v4, v4, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;->audienceUri:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentRequestId:I

    invoke-interface {v1, v2, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 177
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    const-string v3, "GetUserPrivileges"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentRequestId:I

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void

    .line 172
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$1;->val$currentRequestId:I

    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/JsonInformationPrivileges;

    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/Token;->privileges:[I

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationPrivileges;-><init>([I)V

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v1

    goto :goto_0
.end method
