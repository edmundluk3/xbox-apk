.class Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ClubCreationScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubCreationViewPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$1;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 88
    check-cast p3, Landroid/view/View;

    .end local p3    # "view":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 89
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getPageCount()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 80
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getCreationPage(I)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;

    move-result-object v0

    .line 81
    .local v0, "page":Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;->onCreateContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 82
    .local v1, "pageView":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 83
    return-object v1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 75
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateCurrentPage()V
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter$ClubCreationViewPagerAdapter;->this$0:Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getCurrentPage()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getCreationPage(I)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;

    move-result-object v0

    .line 93
    .local v0, "page":Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;->update()V

    .line 94
    return-void
.end method
