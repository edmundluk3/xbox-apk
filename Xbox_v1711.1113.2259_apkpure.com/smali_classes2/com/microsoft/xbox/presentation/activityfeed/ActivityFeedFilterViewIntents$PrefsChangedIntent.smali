.class public abstract Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
.super Ljava/lang/Object;
.source "ActivityFeedFilterViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PrefsChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
    .locals 1
    .param p0, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 21
    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;-><init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V

    return-object v0
.end method


# virtual methods
.method public abstract prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
