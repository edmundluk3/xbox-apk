.class final enum Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;
.super Ljava/lang/Enum;
.source "GameStreamingNetworkTestScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "NetworkTestError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

.field public static final enum CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

.field public static final enum NO_ERROR:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

.field public static final enum USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    const-string v1, "NO_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->NO_ERROR:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    const-string v1, "CONSOLE_DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    const-string v1, "USER_NOT_PAIRED"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->UNKNOWN:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->NO_ERROR:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->UNKNOWN:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->$VALUES:[Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->$VALUES:[Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    return-object v0
.end method
