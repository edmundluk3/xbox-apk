.class Lorg/simpleframework/xml/core/ClassCreator;
.super Ljava/lang/Object;
.source "ClassCreator.java"

# interfaces
.implements Lorg/simpleframework/xml/core/Creator;


# instance fields
.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Initializer;",
            ">;"
        }
    .end annotation
.end field

.field private final primary:Lorg/simpleframework/xml/core/Initializer;

.field private final signature:Lorg/simpleframework/xml/core/Signature;

.field private final type:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/util/List;Lorg/simpleframework/xml/core/Signature;Lorg/simpleframework/xml/core/Initializer;)V
    .locals 1
    .param p2, "signature"    # Lorg/simpleframework/xml/core/Signature;
    .param p3, "primary"    # Lorg/simpleframework/xml/core/Initializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Initializer;",
            ">;",
            "Lorg/simpleframework/xml/core/Signature;",
            "Lorg/simpleframework/xml/core/Initializer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lorg/simpleframework/xml/core/Initializer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p2}, Lorg/simpleframework/xml/core/Signature;->getType()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/ClassCreator;->type:Ljava/lang/Class;

    .line 65
    iput-object p2, p0, Lorg/simpleframework/xml/core/ClassCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    .line 66
    iput-object p3, p0, Lorg/simpleframework/xml/core/ClassCreator;->primary:Lorg/simpleframework/xml/core/Initializer;

    .line 67
    iput-object p1, p0, Lorg/simpleframework/xml/core/ClassCreator;->list:Ljava/util/List;

    .line 68
    return-void
.end method

.method private getInitializer(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)Lorg/simpleframework/xml/core/Initializer;
    .locals 8
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v4, p0, Lorg/simpleframework/xml/core/ClassCreator;->primary:Lorg/simpleframework/xml/core/Initializer;

    .line 126
    .local v4, "result":Lorg/simpleframework/xml/core/Initializer;
    const-wide/16 v2, 0x0

    .line 128
    .local v2, "max":D
    iget-object v5, p0, Lorg/simpleframework/xml/core/ClassCreator;->list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simpleframework/xml/core/Initializer;

    .line 129
    .local v1, "initializer":Lorg/simpleframework/xml/core/Initializer;
    invoke-virtual {v1, p1, p2}, Lorg/simpleframework/xml/core/Initializer;->getScore(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)D

    move-result-wide v6

    .line 131
    .local v6, "score":D
    cmpl-double v5, v6, v2

    if-lez v5, :cond_0

    .line 132
    move-object v4, v1

    .line 133
    move-wide v2, v6

    goto :goto_0

    .line 136
    .end local v1    # "initializer":Lorg/simpleframework/xml/core/Initializer;
    .end local v6    # "score":D
    :cond_1
    return-object v4
.end method


# virtual methods
.method public getInitializers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Initializer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/simpleframework/xml/core/ClassCreator;->list:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getInstance(Lorg/simpleframework/xml/core/Context;)Ljava/lang/Object;
    .locals 1
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lorg/simpleframework/xml/core/ClassCreator;->primary:Lorg/simpleframework/xml/core/Initializer;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/core/Initializer;->getInstance(Lorg/simpleframework/xml/core/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)Ljava/lang/Object;
    .locals 6
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ClassCreator;->getInitializer(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)Lorg/simpleframework/xml/core/Initializer;

    move-result-object v0

    .line 108
    .local v0, "initializer":Lorg/simpleframework/xml/core/Initializer;
    if-nez v0, :cond_0

    .line 109
    new-instance v1, Lorg/simpleframework/xml/core/PersistenceException;

    const-string v2, "Constructor not matched for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/simpleframework/xml/core/ClassCreator;->type:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lorg/simpleframework/xml/core/PersistenceException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1

    .line 111
    :cond_0
    invoke-virtual {v0, p1, p2}, Lorg/simpleframework/xml/core/Initializer;->getInstance(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getParameter(Ljava/lang/String;)Lorg/simpleframework/xml/core/Parameter;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v0, p0, Lorg/simpleframework/xml/core/ClassCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/core/Signature;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/Parameter;

    return-object v0
.end method

.method public getParameters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Parameter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lorg/simpleframework/xml/core/ClassCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Signature;->getParameters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isDefault()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/simpleframework/xml/core/ClassCreator;->primary:Lorg/simpleframework/xml/core/Initializer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 186
    const-string v0, "creator for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/simpleframework/xml/core/ClassCreator;->type:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
