.class public Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;
.super Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
.source "NowPlayingMediaView.java"


# static fields
.field private static final MEDIA_PROGRESS_REGISTRATION_KEY_FORMAT:Ljava/lang/String; = "NowPlayingMedia%d"

.field private static mediaProgressRegistrationId:I


# instance fields
.field private mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

.field private mediaProgressbarConnected:Z

.field private nowPlayingMediaControls:Landroid/view/View;

.field private nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressRegistrationId:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 28
    const v0, 0x7f030196

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;Lcom/microsoft/xbox/toolkit/MediaProgressData;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->updateProgressBarInfo(Lcom/microsoft/xbox/toolkit/MediaProgressData;)V

    return-void
.end method

.method private getMediaProgressRegistrationKey()Ljava/lang/String;
    .locals 5

    .prologue
    .line 98
    const-string v0, "NowPlayingMedia%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget v3, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressRegistrationId:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressRegistrationId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateProgressBarInfo(Lcom/microsoft/xbox/toolkit/MediaProgressData;)V
    .locals 6
    .param p1, "data"    # Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    iget-wide v2, p1, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    iget-wide v4, p1, Lcom/microsoft/xbox/toolkit/MediaProgressData;->durationInSeconds:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->initialize(JJ)V

    .line 95
    return-void
.end method


# virtual methods
.method public attachToContainer()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->attachToContainer()V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->inflatedView:Landroid/view/View;

    const v1, 0x7f0e07fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaControls:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->inflatedView:Landroid/view/View;

    const v1, 0x7f0e07fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->inflatedView:Landroid/view/View;

    const v1, 0x7f0e07ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/MediaButtons;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressbarConnected:Z

    .line 39
    return-void
.end method

.method public cleanup()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->cleanup()V

    .line 84
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressbarConnected:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->getInstance()Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->unRegister(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setUserInitiatedProgressListener(Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressbarConnected:Z

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->cleanup()V

    .line 91
    return-void
.end method

.method public update()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 46
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->update()V

    .line 48
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->shouldShowMediaProgress()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 49
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaControls:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 51
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getEnabledMediaCommands()Ljava/util/EnumSet;

    move-result-object v0

    .line 52
    .local v0, "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    if-eqz v0, :cond_0

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Seek:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v0, v4}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setSeekable(Z)V

    .line 54
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressbarConnected:Z

    if-nez v1, :cond_1

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->getInstance()Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;

    move-result-object v1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->getMediaProgressRegistrationKey()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$1;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;)V

    invoke-virtual {v1, v3, v4}, Lcom/microsoft/xbox/toolkit/MediaProgressReceiver;->register(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/MediaProgressReceiver$IUpdate;)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaProgressbar:Lcom/microsoft/xbox/xle/ui/MediaProgressBar;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;)V

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/MediaProgressBar;->setUserInitiatedProgressListener(Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;)V

    .line 71
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaProgressbarConnected:Z

    .line 77
    .end local v0    # "enabledCommands":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaControlCommands;>;"
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->mediaButtons:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->updateView(Ljava/lang/Object;)V

    .line 78
    return-void

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->nowPlayingMediaControls:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
