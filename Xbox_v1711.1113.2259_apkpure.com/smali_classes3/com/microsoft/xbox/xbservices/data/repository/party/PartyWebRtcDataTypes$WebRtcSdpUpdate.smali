.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
.super Ljava/lang/Object;
.source "PartyWebRtcDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WebRtcSdpUpdate"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lorg/webrtc/SessionDescription;)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;
    .locals 1
    .param p0, "sdp"    # Lorg/webrtc/SessionDescription;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 55
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcSdpUpdate;-><init>(Lorg/webrtc/SessionDescription;)V

    return-object v0
.end method


# virtual methods
.method public abstract sdp()Lorg/webrtc/SessionDescription;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
