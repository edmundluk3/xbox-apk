.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;
.super Ljava/lang/Object;
.source "UTCCommonData.java"


# static fields
.field static final BETA:I = 0x2

.field static final CELLULAR:I = 0x2

.field static final EVENTVERSION:Ljava/lang/String; = "1.1"

.field static final PHONE:I = 0x1

.field static final PROD:I = 0x1

.field static final TABLET:I = 0x2

.field static final UNKNOWN:I = 0x0

.field static final UTCLOGTAG:Ljava/lang/String; = "UTCLOGGING"

.field static final WIFI:I = 0x1

.field static final WIRED:I = 0x3

.field static netType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCommonData(I)Lxbox/smartglass/CommonData;
    .locals 1
    .param p0, "partCVersion"    # I

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;

    move-result-object v0

    return-object v0
.end method

.method public static getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;
    .locals 8
    .param p0, "partCVersion"    # I
    .param p1, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 70
    new-instance v0, Lxbox/smartglass/CommonData;

    invoke-direct {v0}, Lxbox/smartglass/CommonData;-><init>()V

    .line 73
    .local v0, "common":Lxbox/smartglass/CommonData;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s.%s"

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "1.1"

    aput-object v7, v5, v6

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxbox/smartglass/CommonData;->setEventVersion(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxbox/smartglass/CommonData;->setDeviceModel(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0, v2}, Lxbox/smartglass/CommonData;->setDeviceFormFactor(I)V

    .line 84
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setRelease(I)V

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setClientLanguage(Ljava/lang/String;)V

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getNetworkConnection()I

    move-result v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setNetwork(I)V

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setSandboxId(Ljava/lang/String;)V

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getConsoleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setConsoleId(Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentUser()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setXuid(Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getAppSessionId()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setAppSessionId(Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentMemoryKB()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lxbox/smartglass/CommonData;->setMemoryKb(J)V

    .line 108
    if-nez p1, :cond_0

    .line 109
    new-instance p1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .end local p1    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-direct {p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 113
    .restart local p1    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    const-string v1, "ExperimentTreatments"

    invoke-static {}, Lcom/microsoft/xbox/service/model/ExperimentModel;->getInstance()Lcom/microsoft/xbox/service/model/ExperimentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ExperimentModel;->getTreatments()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 115
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->toJson()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setAdditionalInfo(Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->getAccessibilityInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setAccessibilitySettings(Ljava/lang/String;)V

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getAttributionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxbox/smartglass/CommonData;->setAttributionId(Ljava/lang/String;)V

    .line 123
    return-object v0

    :cond_1
    move v1, v2

    .line 84
    goto :goto_0
.end method

.method private static getNetworkConnection()I
    .locals 2

    .prologue
    .line 46
    sget v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    if-nez v1, :cond_0

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "conn":Ljava/lang/String;
    const-string v1, "Cellular"

    if-ne v0, v1, :cond_1

    .line 50
    const/4 v1, 0x2

    sput v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    .line 57
    :cond_0
    :goto_0
    sget v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    return v1

    .line 51
    :cond_1
    const-string v1, "Wifi"

    if-ne v0, v1, :cond_2

    .line 52
    const/4 v1, 0x1

    sput v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    goto :goto_0

    .line 53
    :cond_2
    const-string v1, "Wired"

    if-ne v0, v1, :cond_0

    .line 54
    const/4 v1, 0x3

    sput v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->netType:I

    goto :goto_0
.end method

.method public static varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 33
    sget-boolean v2, Lcom/microsoft/xbox/toolkit/Build;->isBeta:Z

    if-nez v2, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    if-eqz p0, :cond_0

    .line 35
    :try_start_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "newMessage":Ljava/lang/String;
    const-string v2, "UTCLOGGING"

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 37
    .end local v1    # "newMessage":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 39
    .local v0, "exception":Ljava/lang/Exception;
    const-string v2, "UTCLOGGING"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
