.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract allowedInBroadcast(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;
.end method

.method public abstract clientType(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract deviceId(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract isBroadcasting(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract protocolVersion(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public simpleConnectionState(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 1
    .param p1, "simpleConnectionState"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;

    .prologue
    .line 1669
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;->simpleConnectionState(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;

    move-result-object v0

    return-object v0
.end method

.method public abstract simpleConnectionState(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract webRtcDtlsCertificateAlgorithm(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract webRtcDtlsCertificateThumbprint(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract webRtcIcePwd(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method

.method public abstract webRtcIceUfrag(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.end method
