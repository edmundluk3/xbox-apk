.class Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;
.super Ljava/lang/Object;
.source "TagPickerDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z
    .locals 1
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 203
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 3
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 190
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 192
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$600(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    .line 197
    .local v0, "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->refreshAdapter()V

    goto :goto_0

    .line 199
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :cond_0
    return-void
.end method
