.class public Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
.super Ljava/lang/Object;
.source "BeamNavigator.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final deepLinker:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;

.field private final telemetryService:Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;)V
    .locals 0
    .param p1, "deepLinker"    # Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->deepLinker:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;

    .line 24
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;

    .line 25
    return-void
.end method

.method private sendTelemetry(Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;I)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    .param p2, "channelId"    # I

    .prologue
    .line 36
    sget-object v0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator$1;->$SwitchMap$com$microsoft$xbox$toolkit$deeplink$BeamDeepLinker$LaunchResult:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    :goto_0
    return-void

    .line 38
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;->trackLaunchBeamStreamInApp(I)V

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;->trackLaunchBeamStoreListing()V

    goto :goto_0

    .line 46
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService;->trackLaunchBeamStreamInWeb(I)V

    goto :goto_0

    .line 50
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to launch Beam for channelId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public navigateToChannel(Lcom/microsoft/xbox/domain/beam/BeamChannel;)V
    .locals 4
    .param p1, "channel"    # Lcom/microsoft/xbox/domain/beam/BeamChannel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    sget-object v1, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "navigateToChannel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->deepLinker:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->channelId()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->launchStream(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    move-result-object v0

    .line 32
    .local v0, "result":Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->channelId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->sendTelemetry(Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;I)V

    .line 33
    return-void
.end method
