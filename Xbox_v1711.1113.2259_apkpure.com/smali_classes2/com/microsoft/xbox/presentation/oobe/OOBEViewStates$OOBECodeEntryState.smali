.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
.super Ljava/lang/Object;
.source "OOBEViewStates.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OOBECodeEntryState"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .locals 1
    .param p0, "settings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "badCode"    # Z
    .param p2, "fatalError"    # Z
    .annotation build Landroid/support/annotation/RestrictTo;
        value = {
            .enum Landroid/support/annotation/RestrictTo$Scope;->TESTS:Landroid/support/annotation/RestrictTo$Scope;
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V

    return-object v0
.end method

.method public static withBadCode(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .locals 3
    .param p0, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "badCode"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V

    return-object v0
.end method

.method public static withFatalError(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .locals 4
    .param p0, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->badCode()Z

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V

    return-object v0
.end method

.method public static withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .locals 2
    .param p0, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 30
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;

    invoke-direct {v0, p0, v1, v1}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V

    return-object v0
.end method

.method public static withSettings(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
    .locals 3
    .param p0, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->badCode()Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBECodeEntryState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;ZZ)V

    return-object v0
.end method


# virtual methods
.method public abstract badCode()Z
.end method

.method public abstract fatalError()Z
.end method

.method public abstract oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBECodeEntryState, badCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->badCode()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
