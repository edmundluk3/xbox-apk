.class public Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "UnsharedActivityFeedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;,
        Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

.field private shouldReload:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    .line 39
    return-void
.end method


# virtual methods
.method public closeDialog()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->goBack()V

    .line 43
    return-void
.end method

.method protected getActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getUnsharedActivityFeedData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getUnsharedActivityFeedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadUnsharedActivityFeed(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 73
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to get unshared activity feed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->shouldReload:Z

    .line 77
    return-object v0
.end method

.method public onItemClick(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 12
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 97
    if-eqz p1, :cond_0

    .line 98
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    .line 99
    .local v1, "itemRoot":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v8

    .line 101
    .local v8, "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$UnsharedActivityFeedScreenViewModel$LaunchSource:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->access$000(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 119
    .end local v1    # "itemRoot":Ljava/lang/String;
    .end local v8    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :cond_0
    :goto_0
    return-void

    .line 103
    .restart local v1    # "itemRoot":Ljava/lang/String;
    .restart local v8    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0, p0, v1, v8}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    goto :goto_0

    .line 106
    :pswitch_1
    const-string v0, "Activity Feed Share to Message"

    const-string v3, "[()]"

    const-string v4, "0"

    invoke-static {v1, v3, v5, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->access$100(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToMessageScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZLcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .line 110
    :pswitch_2
    const-string v0, "Activity Feed Share to Activity Feed"

    const-string v3, "[()]"

    const-string v4, "0"

    invoke-static {v1, v3, v5, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :pswitch_3
    const-string v0, "Activity Feed Share to Club"

    const-string v3, "[()]"

    const-string v4, "0"

    invoke-static {v1, v3, v5, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->access$200(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)J

    move-result-wide v4

    sget-object v6, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->access$200(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move v3, v2

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZJLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->shouldReload:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshUnsharedActivityFeed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 83
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 94
    return-void

    .line 86
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->shouldReload:Z

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
