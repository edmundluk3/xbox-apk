.class public Lcom/microsoft/xbox/xle/ui/SocialBarValue;
.super Lcom/microsoft/xbox/toolkit/ui/XLEButton;
.source "SocialBarValue.java"


# static fields
.field public static final MAX_NUMBER:I = 0x1869f


# instance fields
.field private socialBarValue:I

.field private textOne:Ljava/lang/String;

.field private textTwoOrMore:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method private SetTextMinWidth(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 78
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 80
    .local v0, "scale":F
    const/high16 v2, 0x41c80000    # 25.0f

    mul-float/2addr v2, v0

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v1, v2

    .line 81
    .local v1, "width":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setMinWidth(I)V

    .line 84
    .end local v0    # "scale":F
    .end local v1    # "width":I
    :cond_0
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->SocialBarValue:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 32
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textOne:Ljava/lang/String;

    .line 33
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textTwoOrMore:Ljava/lang/String;

    .line 34
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->updateSocialBarTextValue()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 39
    return-void

    .line 37
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method public static setSocialBarValueIfNotNull(Lcom/microsoft/xbox/xle/ui/SocialBarValue;I)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/SocialBarValue;
    .param p1, "socialBarValue"    # I

    .prologue
    .line 87
    if-eqz p0, :cond_0

    .line 88
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setSocialBarValue(I)V

    .line 91
    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setVisibility(I)V

    .line 93
    :cond_0
    return-void

    .line 91
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSocialBarTextValue()V
    .locals 5

    .prologue
    const v3, 0x1869f

    const/4 v2, 0x1

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "text":Ljava/lang/String;
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    if-ne v1, v2, :cond_1

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textOne:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textOne:Ljava/lang/String;

    .line 65
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->SetTextMinWidth(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void

    .line 58
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    if-le v1, v3, :cond_2

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textTwoOrMore:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->textTwoOrMore:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static updateSocialBarValueContainerVisibility(Landroid/view/View;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;Lcom/microsoft/xbox/xle/ui/SocialBarValue;)V
    .locals 2
    .param p0, "container"    # Landroid/view/View;
    .param p1, "likes"    # Lcom/microsoft/xbox/xle/ui/SocialBarValue;
    .param p2, "comments"    # Lcom/microsoft/xbox/xle/ui/SocialBarValue;
    .param p3, "shares"    # Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .prologue
    const/16 v1, 0x8

    .line 97
    if-eqz p0, :cond_0

    .line 98
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    .line 99
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->getSocialBarValue()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->getSocialBarValue()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-virtual {p3}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->getSocialBarValue()I

    move-result v0

    if-gtz v0, :cond_1

    .line 100
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getSocialBarValue()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    return v0
.end method

.method public setSocialBarValue(I)V
    .locals 0
    .param p1, "socialBarValue"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->socialBarValue:I

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->updateSocialBarTextValue()V

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->invalidate()V

    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/SocialBarValue;->requestLayout()V

    .line 50
    return-void
.end method
