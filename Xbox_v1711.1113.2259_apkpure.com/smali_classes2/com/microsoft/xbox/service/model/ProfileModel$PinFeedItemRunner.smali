.class Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PinFeedItemRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemRoot:Ljava/lang/String;

.field private final pin:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final timelineId:Ljava/lang/String;

.field private final timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/ProfileModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "pin"    # Z
    .param p4, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "timelineId"    # Ljava/lang/String;

    .prologue
    .line 4682
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4683
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 4684
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 4685
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->itemRoot:Ljava/lang/String;

    .line 4686
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->pin:Z

    .line 4687
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 4688
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->timelineId:Ljava/lang/String;

    .line 4689
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4697
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->itemRoot:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->pin:Z

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->timelineId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->pinActivityItem(Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4675
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$PinFeedItemRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4706
    const-wide/16 v0, 0xc8d

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4702
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4693
    return-void
.end method
