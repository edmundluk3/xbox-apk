.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error$Code;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Code"
.end annotation


# static fields
.field public static final OOBEInvalidSetupCode:Ljava/lang/String; = "90015"

.field public static final OOBESaveSettingsFailure:Ljava/lang/String; = "90016"

.field public static final PartyChatBlocked:Ljava/lang/String; = "90010"

.field public static final PartyChatDoesntExist:Ljava/lang/String; = "90023"

.field public static final PartyChatFailedToChangeRestriction:Ljava/lang/String; = "90020"

.field public static final PartyChatFailedToInvite:Ljava/lang/String; = "90018"

.field public static final PartyChatFailedToJoin:Ljava/lang/String; = "90022"

.field public static final PartyChatFailedToKick:Ljava/lang/String; = "90026"

.field public static final PartyChatFailedToLeave:Ljava/lang/String; = "90021"

.field public static final PartyChatFailedToLoadDetails:Ljava/lang/String; = "90019"

.field public static final PartyChatFailedToMute:Ljava/lang/String; = "90025"

.field public static final PartyChatFailedToSendTextMessage:Ljava/lang/String; = "90016"

.field public static final PartyChatFailedToStart:Ljava/lang/String; = "90024"

.field public static final PartyChatFailedToUploadPingTime:Ljava/lang/String; = "90017"

.field public static final PartyChatFull:Ljava/lang/String; = "90011"

.field public static final PartyChatGeneric:Ljava/lang/String; = "90027"

.field public static final PartyChatInvalidInvitation:Ljava/lang/String; = "90014"

.field public static final PartyChatInviteExceedsCapacity:Ljava/lang/String; = "90015"

.field public static final PartyChatLocalNetwork:Ljava/lang/String; = "90013"

.field public static final PartyChatXBLNetworking:Ljava/lang/String; = "90012"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
