.class Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "CustomizeProfileScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckGamertagAvailabilityAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final gamertag:Ljava/lang/String;

.field private isAvailable:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 967
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 968
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->gamertag:Ljava/lang/String;

    .line 969
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 973
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 974
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 1003
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    .line 1006
    .local v1, "serviceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->gamertag:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->checkGamertagAvailability(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->isAvailable:Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v2

    .line 1007
    :catch_0
    move-exception v0

    .line 1008
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 998
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 979
    const-string v0, "CheckGamertagAvailabilityAsyncTask should always run if requested"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 980
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 992
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$2002(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 993
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->gamertag:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->isAvailable:Z

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$2100(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Z)V

    .line 994
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 962
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 984
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 985
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$2002(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 987
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$CheckGamertagAvailabilityAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    .line 988
    return-void
.end method
