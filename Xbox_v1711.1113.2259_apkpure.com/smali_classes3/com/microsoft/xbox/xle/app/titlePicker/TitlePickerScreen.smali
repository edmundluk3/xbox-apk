.class public Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "TitlePickerScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const-string v0, "Clubs - Title Picker view"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;->onCreateContentView()V

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 14
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x7f030234

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;->setContentView(I)V

    .line 19
    return-void
.end method
