.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;
.super Ljava/lang/Object;
.source "HoverChatManager.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
.implements Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation
.end field

.field private final chatHeadViewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation
.end field

.field private final contentDescriptionDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;

.field private final dialogManager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

.field private final imageDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;

.field private final isCreated:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

.field private final presenterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

.field private final rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p3, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .param p4, "imageDownloaderFactory"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;
    .param p5, "contentDescriptionDownloaderFactory"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;
    .param p6, "navigationManager"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p7, "dialogManager"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;
    .param p8, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 58
    .local p1, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    .local p2, "chatHeadViewAdapter":Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;, "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 60
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadViewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    .line 61
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 62
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->imageDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;

    .line 63
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->contentDescriptionDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;

    .line 64
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .line 65
    iput-object p7, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->dialogManager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    .line 66
    iput-object p8, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 67
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->isCreated:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->presenterMap:Ljava/util/Map;

    .line 70
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->addChatHead(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->removeChatHead(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    return-void
.end method

.method private addChatHead(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 8
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .prologue
    const/4 v7, 0x1

    .line 101
    sget-object v4, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addChatHead: key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getArrangementType()Ljava/lang/Class;

    move-result-object v4

    if-nez v4, :cond_0

    .line 104
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v5, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->ensureMenuAdded()V

    .line 109
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const/4 v5, 0x0

    invoke-interface {v4, p1, v5, v7}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->addChatHead(Ljava/io/Serializable;ZZ)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    .line 110
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    invoke-virtual {v0, v7}, Lcom/flipkart/chatheads/ui/ChatHead;->setClickable(Z)V

    .line 111
    const-string v4, ""

    invoke-virtual {v0, v4}, Lcom/flipkart/chatheads/ui/ChatHead;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->presenterMap:Ljava/util/Map;

    monitor-enter v5

    .line 114
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->presenterMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 115
    new-instance v3, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;-><init>(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 116
    .local v3, "view":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;
    new-instance v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->imageDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;

    invoke-virtual {v4, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->contentDescriptionDownloaderFactory:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;

    invoke-virtual {v6, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;->get(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;

    move-result-object v6

    invoke-direct {v1, v4, v6}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadParticipantsDownloader;)V

    .line 117
    .local v1, "interactor":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;
    new-instance v2, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct {v2, v3, v4, v1, v6}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 118
    .local v2, "presenter":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->presenterMap:Ljava/util/Map;

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    .end local v1    # "interactor":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;
    .end local v2    # "presenter":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .end local v3    # "view":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getArrangementType()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    if-ne v4, v5, :cond_2

    .line 123
    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 125
    :cond_2
    return-void

    .line 120
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private ensureMenuAdded()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    sget-object v2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;->INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    invoke-interface {v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->findChatHeadByKey(Ljava/io/Serializable;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    sget-object v2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;->INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    invoke-interface {v1, v2, v3, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->addChatHead(Ljava/io/Serializable;ZZ)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v0

    .line 130
    .local v0, "menuHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    invoke-virtual {v0, v3}, Lcom/flipkart/chatheads/ui/ChatHead;->setClickable(Z)V

    .line 131
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070680

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHead;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    .end local v0    # "menuHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;"
    :cond_0
    return-void
.end method

.method static synthetic lambda$onCreate$0(Ljava/lang/Boolean;)Z
    .locals 1
    .param p0, "enabled"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreate$1(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;
    .param p1, "ignore"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->onChatHeadsDisabled()V

    return-void
.end method

.method private onChatHeadsDisabled()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Chat heads disabled. Removing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeAllChatHeads(Z)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    .line 150
    return-void
.end method

.method private removeChatHead(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeChatHead(Ljava/io/Serializable;Z)Z

    .line 137
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->isCreated:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadViewAdapter:Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setViewAdapter(Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->setActiveChatHeadManager(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->addOnNavigatedListener(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->dialogManager:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->addDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x3

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 82
    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getClubChatKeys()Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getMessageKeys()Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {v2, v3}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 83
    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 84
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 86
    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledGloballyObs()Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 87
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 88
    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 89
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 91
    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getLeftMessageGroupEvents()Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 92
    invoke-interface {v4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 93
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 81
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    const-string v0, "Already created"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeAllChatHeads(Z)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->setActiveChatHeadManager(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->removeOnNavigatedListener(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;)V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 144
    return-void
.end method

.method public onDialogShown(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 4
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V

    .line 164
    return-void
.end method

.method public onPageNavigated(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "from"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "to"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;Z)V

    .line 155
    return-void
.end method

.method public onPageRestarted(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 159
    return-void
.end method
