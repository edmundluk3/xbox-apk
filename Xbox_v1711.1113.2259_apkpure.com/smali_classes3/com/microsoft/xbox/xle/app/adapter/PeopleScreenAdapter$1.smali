.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;
.super Ljava/lang/Object;
.source "PeopleScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 152
    .local v0, "filter":Lcom/microsoft/xbox/service/model/FollowersFilter;
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFriends;->trackFriendsFilterAction(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->setFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)Landroid/widget/AbsListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 156
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V

    .line 157
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
