.class public Lcom/microsoft/xbox/xle/remote/TextInputModel;
.super Lcom/microsoft/xbox/smartglass/TextManagerListener;
.source "TextInputModel.java"


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/remote/TextInputModel;


# instance fields
.field private currentSelectLength:I

.field private currentSelectStart:I

.field private currentText:Ljava/lang/String;

.field private currentTextConfiguration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

.field private receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

.field private started:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->instance:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/TextManagerListener;-><init>()V

    .line 25
    return-void
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/xle/remote/TextInputModel;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->instance:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/remote/TextInputModel;)Lcom/microsoft/xbox/smartglass/TextManagerListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputModel;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/remote/TextInputModel;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->instance:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    return-object v0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 164
    iput v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectStart:I

    .line 165
    iput v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectLength:I

    .line 166
    iput-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentText:Ljava/lang/String;

    .line 167
    iput-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentTextConfiguration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .line 168
    return-void
.end method


# virtual methods
.method public attach(Lcom/microsoft/xbox/smartglass/TextManagerListener;)V
    .locals 1
    .param p1, "receiver"    # Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNull(Ljava/lang/Object;)V

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 65
    return-void
.end method

.method public detach()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->receiver:Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 70
    return-void
.end method

.method public getCurrentSelectLength()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectLength:I

    return v0
.end method

.method public getCurrentSelectStart()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectStart:I

    return v0
.end method

.method public getCurrentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentText:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTextConfiguation()Lcom/microsoft/xbox/smartglass/TextConfiguration;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentTextConfiguration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

    return-object v0
.end method

.method public onCompleted()V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->reset()V

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/TextInputModel$3;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 104
    return-void
.end method

.method public onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 1
    .param p1, "configuration"    # Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentTextConfiguration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .line 149
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;Lcom/microsoft/xbox/smartglass/TextConfiguration;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 161
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 53
    const-string v0, "TextInputModel"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->started:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/smartglass/TextManager;->removeListener(Ljava/lang/Object;)V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->started:Z

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "TextInputModel"

    const-string v1, "skipping onPause"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 32
    const-string v1, "TextInputModel"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->started:Z

    if-nez v1, :cond_0

    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->reset()V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/xle/remote/TextInputModel$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/remote/TextInputModel$1;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/remote/TextInputModel$2;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 45
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 46
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->started:Z

    .line 50
    .end local v0    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :goto_0
    return-void

    .line 48
    :cond_0
    const-string v1, "TextInputModel"

    const-string v2, "skipping onResume"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentText:Ljava/lang/String;

    .line 115
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel$4;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputModel$4;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 122
    return-void
.end method

.method public onTextSelected(II)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectStart:I

    .line 132
    iput p2, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel;->currentSelectLength:I

    .line 134
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputModel$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/xle/remote/TextInputModel$5;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;II)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 141
    return-void
.end method
