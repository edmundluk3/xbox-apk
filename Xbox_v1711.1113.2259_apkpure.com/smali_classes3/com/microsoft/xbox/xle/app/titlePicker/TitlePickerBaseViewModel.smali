.class public abstract Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "TitlePickerBaseViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;,
        Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TITLE_HUB_SERVICE:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

.field private static miniCatalog:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final recentlyPlayedModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

.field private static final storeService:Lcom/microsoft/xbox/service/store/IStoreService;


# instance fields
.field private pcSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private recentTitles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation
.end field

.field private searchAutoSuggestionsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

.field private searchResults:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

.field private searchResultsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

.field private searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->TAG:Ljava/lang/String;

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->TITLE_HUB_SERVICE:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentlyPlayedModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "parentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 56
    return-void
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/service/store/IStoreService;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->onLoadAutoSuggestionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;)V

    return-void
.end method

.method static synthetic access$200()Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->miniCatalog:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 36
    sput-object p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->miniCatalog:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->TITLE_HUB_SERVICE:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .param p3, "x3"    # Ljava/util/List;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->onLoadSearchResultsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Ljava/util/List;)V

    return-void
.end method

.method private onLoadAutoSuggestionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "results"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 191
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 205
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->updateAdapter()V

    .line 206
    return-void

    .line 195
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;->getResults()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 201
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadSearchResultsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Ljava/util/List;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "results"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "pcResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;>;"
    const/4 v2, 0x0

    .line 209
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->updateAdapter()V

    .line 224
    return-void

    .line 213
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResults:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 214
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->pcSearchResults:Ljava/util/List;

    goto :goto_0

    .line 218
    :pswitch_1
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResults:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 219
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->pcSearchResults:Ljava/util/List;

    goto :goto_0

    .line 209
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onRecentTitlesLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 172
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 187
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->updateAdapter()V

    .line 188
    return-void

    .line 176
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getRecentTitles()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentTitles:Ljava/util/List;

    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentTitles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 179
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->onRecentTitleListUpdated()V

    goto :goto_0

    .line 177
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 183
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public clearSearchResults()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 94
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    .line 95
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResults:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->updateAdapter()V

    .line 97
    return-void
.end method

.method protected abstract createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.end method

.method public getPcSearchResults()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->pcSearchResults:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRecentlyPlayedTitles()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentTitles:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSearchResults()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResults:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    return-object v0
.end method

.method public getSearchSuggestions()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchSuggestions:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentlyPlayedModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 141
    if-nez p1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 143
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->loadRecentlyPlayed(Z)V

    .line 150
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getRecentTitles()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentTitles:Ljava/util/List;

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentTitles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 148
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->updateAdapter()V

    goto :goto_0

    .line 146
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method public loadAutoSuggestAsync(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 100
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchAutoSuggestionsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchAutoSuggestionsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->cancel()V

    .line 106
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;-><init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchAutoSuggestionsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchAutoSuggestionsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->execute()V

    .line 108
    return-void
.end method

.method public loadSearchResultsAsync(Ljava/lang/String;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResultsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResultsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->cancel()V

    .line 115
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;-><init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResultsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->searchResultsTask:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSearchResultsTask;->execute()V

    .line 117
    return-void
.end method

.method protected abstract onRecentTitleListUpdated()V
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 127
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentlyPlayedModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 122
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->recentlyPlayedModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 132
    return-void
.end method

.method public onTapCloseButton()V
    .locals 1

    .prologue
    .line 89
    const-string v0, "LFG - Close"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->goBack()V

    .line 91
    return-void
.end method

.method public abstract selectTitle(JLjava/lang/String;)V
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 155
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 157
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 158
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 160
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 165
    sget-object v2, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown update type ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    return-void

    .line 162
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->onRecentTitlesLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
