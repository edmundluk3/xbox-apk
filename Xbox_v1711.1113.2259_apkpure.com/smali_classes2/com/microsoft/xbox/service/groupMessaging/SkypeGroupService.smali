.class public final enum Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;
.super Ljava/lang/Enum;
.source "SkypeGroupService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;",
        ">;",
        "Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

.field private static final CONSUMPTION_HORIZON_PROPERTY:Ljava/lang/String; = "consumptionhorizon"

.field private static final HTTP_ACCEPTABLE_CODES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

.field private static final LOCATION_HEADER:Ljava/lang/String; = "Location"

.field private static final MUTED_PROPERTY:Ljava/lang/String; = "alerts"

.field private static final PRIVACY_ENDPOINT_FORMAT:Ljava/lang/String; = "https://privacy.xboxlive.com/users/xuid(%s)/permission/validate"

.field private static final SKYPE_AUTH_COOKIE_HEADER:Ljava/lang/String; = "Cookie"

.field private static SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String; = null

.field private static SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String; = null

.field private static final SKYPE_CLIENT_NAME_HEADER:Ljava/lang/String; = "x-xbl-client-name"

.field private static final SKYPE_CLIENT_NAME_HEADER_VALUE:Ljava/lang/String; = "Smartglass Android"

.field private static final SKYPE_CREATE_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/users/ME/endpoints/%s"

.field private static final SKYPE_CREATE_GROUP_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/threads"

.field private static final SKYPE_GROUP_CONVERSATION_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/users/ME/conversations/%s/messages%s"

.field private static final SKYPE_GROUP_MEMBERS_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/threads/%s/?view=msnp24Equivalent"

.field private static final SKYPE_GROUP_ROSTER_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/threads/%s/members/%s"

.field private static final SKYPE_REGISTRATION_TOKEN_HEADER:Ljava/lang/String; = "RegistrationToken"

.field private static final SKYPE_SET_REGISTRATION_TOKEN_HEADER:Ljava/lang/String; = "Set-RegistrationToken"

.field private static final SKYPE_SUBSCRIBE_FORMAT:Ljava/lang/String; = "https://%s/v1/users/ME/endpoints/%s/subscriptions"

.field private static final SKYPE_UPDATE_CONVERSATION_PROPERTY_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/users/ME/conversations/%s/properties?name=%s&bookmark=false"

.field private static final SKYPE_UPDATE_CONVERSATION_THREAD_PROPERTY_ENDPOINT_FORMAT:Ljava/lang/String; = "https://%s/v1/threads/%s/properties?name=%s"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final TOPIC_PROPERTY:Ljava/lang/String; = "topic"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    .line 31
    new-array v0, v3, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->INSTANCE:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    .line 52
    const-string v0, "ClientInfo"

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    .line 53
    const-string v0, "os=Android; osVer=not defined; proc=not defined; lcid=%s; deviceType=1; country=%s; clientName=XboxAppAndroid; clientVer=1.0"

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    .line 59
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0xca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xcb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0xcc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xcd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0xcf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xd0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xe2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    .line 63
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-client-type"

    const-string v3, "XboxApp"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-client-name"

    const-string v3, "Smartglass Android"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 67
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private createSkypeEndpointForLongPoll(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p1, "regId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "putBody"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 432
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createSkypeEndpointForLongPoll: regId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " putBody:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 434
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 436
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "https://%s/v1/users/ME/endpoints/%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 438
    .local v5, "url":Ljava/lang/String;
    const/4 v4, 0x0

    .line 440
    .local v4, "result":Z
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 442
    .local v3, "responseHeaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 443
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    invoke-static {v5, v0, p2, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v2

    .line 447
    .local v2, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v2, :cond_1

    .line 448
    iget-boolean v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 450
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 451
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 454
    :cond_0
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 455
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 456
    sget-object v6, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 460
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v4, :cond_2

    .line 461
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v7, "Error creating long poll endpoint"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_2
    return v4
.end method

.method private static extractThreadId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "link"    # Ljava/lang/String;

    .prologue
    .line 533
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 534
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 536
    .local v0, "urlTokens":[Ljava/lang/String;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 537
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    .line 541
    .end local v0    # "urlTokens":[Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getHeaderValueFromResponseHeaders(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "headerName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 521
    .local p0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    if-eqz p0, :cond_1

    .line 522
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 523
    .local v0, "header":Lorg/apache/http/Header;
    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 524
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 529
    .end local v0    # "header":Lorg/apache/http/Header;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getHeaders()Ljava/util/List;
    .locals 8
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 501
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 503
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    sget-object v1, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 504
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "RegistrationToken"

    sget-object v3, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v3}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    :goto_0
    return-object v0

    .line 506
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Cookie"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "skypetoken=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v7

    invoke-virtual {v7}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getLocationFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 517
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const-string v0, "Location"

    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaderValueFromResponseHeaders(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const-string v0, "Set-RegistrationToken"

    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaderValueFromResponseHeaders(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createGroup$0(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createSkypeEndpointForLongPoll$10(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "putBody"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "responseHeaders"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 445
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$deleteGroup$6(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 295
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getGroupMembersForConversation$8(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 368
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getUserPermissions$3(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 203
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$joinGroupConversation$2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "requestBody"    # Ljava/lang/String;
    .param p3, "responseHeaders"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$leaveGroupConversation$4(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$renameGroupConversation$7(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "requestBody"    # Ljava/lang/String;
    .param p3, "responseHeaders"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 334
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$setConversationToMute$5(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "requestBody"    # Ljava/lang/String;
    .param p3, "responseHeaders"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$skypeLongPoll$11(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 478
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$subscribeWithSkypeChatServiceForLongPoll$9(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "postBody"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 406
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$updateConsumptionHorizon$1(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/List;
    .param p2, "requestBody"    # Ljava/lang/String;
    .param p3, "responseHeaders"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->putStreamWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->$VALUES:[Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;

    return-object v0
.end method


# virtual methods
.method public createGroup(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "topic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "members":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v7, "Creating skype group"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 77
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "https://%s/v1/threads"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "url":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;

    invoke-direct {v6, p1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;-><init>(Ljava/util/List;)V

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequest;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "requestBody":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 81
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {v5, v0, v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$1;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v3

    .line 84
    .local v3, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v3, :cond_5

    .line 86
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    iget-object v7, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 90
    :cond_0
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 92
    sget-object v6, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 95
    :cond_1
    iget-boolean v6, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    if-eqz v6, :cond_3

    .line 96
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getLocationFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, "threadUrl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 98
    invoke-static {v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->extractThreadId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 107
    .end local v1    # "registrationToken":Ljava/lang/String;
    .end local v4    # "threadUrl":Ljava/lang/String;
    :goto_1
    return-object v6

    .line 78
    .end local v0    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v2    # "requestBody":Ljava/lang/String;
    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    :cond_2
    new-instance v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;

    invoke-direct {v6, p1, p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$CreateGroupRequestWithProperties;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 101
    .restart local v0    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v1    # "registrationToken":Ljava/lang/String;
    .restart local v2    # "requestBody":Ljava/lang/String;
    .restart local v3    # "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    :cond_3
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v7, "Error creating thread"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_4
    :goto_2
    const/4 v6, 0x0

    goto :goto_1

    .line 104
    :cond_5
    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v7, "Error creating thread"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public deleteGroup(Ljava/lang/String;)Z
    .locals 13
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 284
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v6, "Delete group"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 287
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 289
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "https://%s/v1/users/ME/conversations/%s/messages%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    aput-object p1, v7, v12

    const-string v8, ""

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 291
    .local v4, "url":Ljava/lang/String;
    const/4 v3, 0x0

    .line 293
    .local v3, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 294
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    invoke-static {v4, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$7;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v2

    .line 297
    .local v2, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v2, :cond_1

    .line 298
    iget-boolean v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 300
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 301
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 304
    :cond_0
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 306
    sget-object v5, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 310
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v3, :cond_2

    .line 311
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v6, "Error deleting group conversation"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_2
    return v3
.end method

.method public getGroupMembersForConversation(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;
    .locals 12
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 359
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v5, "Get group members"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 362
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 364
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "https://%s/v1/threads/%s/?view=msnp24Equivalent"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    aput-object p1, v6, v11

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 366
    .local v3, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 367
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v7, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    invoke-static {v3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$9;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/lang/Class;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v2

    .line 370
    .local v2, "serviceResponse":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;, "Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;>;"
    if-eqz v2, :cond_2

    .line 372
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 373
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 376
    :cond_0
    iget-boolean v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    if-nez v4, :cond_1

    .line 377
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v5, "Error getting group members"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_1
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 382
    sget-object v4, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 386
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_2
    if-eqz v2, :cond_3

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->response:Ljava/lang/Object;

    check-cast v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberResponse;

    :goto_0
    return-object v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getUserPermissions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
    .locals 6
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;",
            ">;)",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p2, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 192
    sget-object v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v5, "getting permissions from privacy service"

    invoke-static {v2, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 195
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 196
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 197
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 199
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "https://privacy.xboxlive.com/users/xuid(%s)/permission/validate"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v5, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "url":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;

    invoke-direct {v2, p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "requestBody":Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    return-object v2

    .end local v0    # "requestBody":Ljava/lang/String;
    .end local v1    # "url":Ljava/lang/String;
    :cond_0
    move v2, v4

    .line 196
    goto :goto_0

    :cond_1
    move v2, v4

    .line 197
    goto :goto_1
.end method

.method public joinGroupConversation(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)Z
    .locals 15
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "userId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "role"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 151
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Join group conversation"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 154
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 155
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 156
    invoke-static/range {p3 .. p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "https://%s/v1/threads/%s/members/%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    const/4 v11, 0x2

    aput-object p2, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 159
    .local v7, "url":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;

    move-object/from16 v0, p3

    invoke-direct {v8, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;-><init>(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$JoinGroupRequest;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "requestBody":Ljava/lang/String;
    const/4 v6, 0x0

    .line 163
    .local v6, "result":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v5, "responseHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 166
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v11, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    invoke-static {v7, v1, v3, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/concurrent/Callable;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v4

    .line 169
    .local v4, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v4, :cond_1

    .line 170
    iget-boolean v6, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 172
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 176
    :cond_0
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 177
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 178
    sget-object v8, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v8, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 182
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_2

    .line 183
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "Error joining group conversation %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_2
    return v6
.end method

.method public leaveGroupConversation(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "userId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 209
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v6, "Leave group conversation"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 212
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 213
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 215
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "https://%s/v1/threads/%s/members/%s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    aput-object p1, v7, v12

    aput-object p2, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 217
    .local v4, "url":Ljava/lang/String;
    const/4 v3, 0x0

    .line 219
    .local v3, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 220
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-static {v4, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$5;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v2

    .line 223
    .local v2, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v2, :cond_1

    .line 224
    iget-boolean v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 226
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 227
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 230
    :cond_0
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 232
    sget-object v5, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 236
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v3, :cond_2

    .line 237
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v6, "Error leaving group conversation"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_2
    return v3
.end method

.method public renameGroupConversation(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "topicName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 319
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Rename group conversation"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 322
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 323
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 325
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "https://%s/v1/threads/%s/properties?name=%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    const/4 v11, 0x2

    const-string v12, "topic"

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 326
    .local v7, "url":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$TopicRenameRequest;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$TopicRenameRequest;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$TopicRenameRequest;->toString()Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, "requestBody":Ljava/lang/String;
    const/4 v6, 0x0

    .line 330
    .local v6, "result":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 332
    .local v5, "responseHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 333
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v11, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-static {v7, v1, v3, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$8;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/concurrent/Callable;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v4

    .line 336
    .local v4, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v4, :cond_1

    .line 337
    iget-boolean v6, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 339
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 340
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 343
    :cond_0
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 344
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 345
    sget-object v8, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v8, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 349
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_2

    .line 350
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Error renaming group conversation"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :cond_2
    return v6
.end method

.method public setConversationToMute(Ljava/lang/String;Z)Z
    .locals 15
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "isMuted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 245
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "set conversation to mute/unmute"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 248
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 250
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "https://%s/v1/users/ME/conversations/%s/properties?name=%s&bookmark=false"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    const/4 v11, 0x2

    const-string v12, "alerts"

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 251
    .local v7, "url":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;

    move/from16 v0, p2

    invoke-direct {v8, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;-><init>(Z)V

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$MuteRequest;->toString()Ljava/lang/String;

    move-result-object v3

    .line 253
    .local v3, "requestBody":Ljava/lang/String;
    const/4 v6, 0x0

    .line 255
    .local v6, "result":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v5, "responseHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 258
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v11, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-static {v7, v1, v3, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$6;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/concurrent/Callable;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v4

    .line 261
    .local v4, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v4, :cond_1

    .line 262
    iget-boolean v6, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 264
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 265
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 268
    :cond_0
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 270
    sget-object v8, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v8, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 274
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_2

    .line 275
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Error setting skype muted property"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_2
    return v6
.end method

.method public skypeLongPoll(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;
    .locals 7
    .param p1, "locationUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 470
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "skypeLongPoll - locationUrl:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 472
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 474
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/poll"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 476
    .local v3, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 478
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    invoke-static {v3, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$12;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/lang/Class;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v2

    .line 480
    .local v2, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;, "Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;>;"
    if-eqz v2, :cond_2

    .line 482
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 483
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 486
    :cond_0
    iget-boolean v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    if-nez v4, :cond_1

    .line 487
    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v5, "Error starting Skype long poll"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :cond_1
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 491
    .local v1, "registrationToken":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 492
    sget-object v4, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 496
    .end local v1    # "registrationToken":Ljava/lang/String;
    :cond_2
    if-eqz v2, :cond_3

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->response:Ljava/lang/Object;

    check-cast v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;

    :goto_0
    return-object v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public subscribeWithSkypeChatServiceForLongPoll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "regId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "postBody"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 392
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "subscribeWithSkypeChatServiceForLongPoll - regId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " postBody:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 394
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 395
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 397
    new-instance v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageNotificationRequest;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageNotificationRequest;-><init>()V

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$ServiceMessageNotificationRequest;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->createSkypeEndpointForLongPoll(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 399
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "https://%s/v1/users/ME/endpoints/%s/subscriptions"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    aput-object p1, v7, v12

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 401
    .local v4, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v0

    .line 402
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    const-string v1, ""

    .line 406
    .local v1, "locationUrl":Ljava/lang/String;
    invoke-static {v4, v0, p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$10;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v3

    .line 407
    .local v3, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v3, :cond_1

    .line 409
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 410
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 413
    :cond_0
    iget-boolean v5, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    if-eqz v5, :cond_2

    .line 414
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getLocationFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 419
    :goto_0
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 420
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 421
    sget-object v5, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 428
    .end local v0    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v1    # "locationUrl":Ljava/lang/String;
    .end local v2    # "registrationToken":Ljava/lang/String;
    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v1

    .line 416
    .restart local v0    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v1    # "locationUrl":Ljava/lang/String;
    .restart local v3    # "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    sget-object v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v6, "Error subscribing Skype chat service for long poll"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    .end local v0    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v1    # "locationUrl":Ljava/lang/String;
    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    goto :goto_1
.end method

.method public updateConsumptionHorizon(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 15
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "consumptionHorizon"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 112
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Updating consumption horizon"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 115
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 116
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 118
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "https://%s/v1/users/ME/conversations/%s/properties?name=%s&bookmark=false"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeHostname()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p1, v10, v11

    const/4 v11, 0x2

    const-string v12, "consumptionhorizon"

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "url":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UpdateConsumptionHorizonRequest;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UpdateConsumptionHorizonRequest;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UpdateConsumptionHorizonRequest;->toString()Ljava/lang/String;

    move-result-object v3

    .line 121
    .local v3, "requestBody":Ljava/lang/String;
    const/4 v6, 0x0

    .line 123
    .local v6, "result":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v5, "responseHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 126
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v11, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-static {v7, v1, v3, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$2;->lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/concurrent/Callable;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->HTTP_ACCEPTABLE_CODES:Ljava/util/List;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->redirectRequest(Ljava/util/concurrent/Callable;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;

    move-result-object v4

    .line 129
    .local v4, "response":Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;
    if-eqz v4, :cond_1

    .line 130
    iget-boolean v6, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->result:Z

    .line 132
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->redirectUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 136
    :cond_0
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/ResponseWithRedirectUrl;->headers:Ljava/util/List;

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getSkypeRegistrationTokenFromResponseHeaders(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 138
    sget-object v8, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v8, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 142
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_2

    .line 143
    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->TAG:Ljava/lang/String;

    const-string v9, "Error updating consumption horizon"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_2
    return v6
.end method
