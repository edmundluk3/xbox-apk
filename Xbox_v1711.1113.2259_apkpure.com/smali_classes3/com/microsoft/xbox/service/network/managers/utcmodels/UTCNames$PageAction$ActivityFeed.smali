.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$ActivityFeed;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityFeed"
.end annotation


# static fields
.field public static final Achievement:Ljava/lang/String; = "Activity Feed - Achievement Selection"

.field public static final AddFriend:Ljava/lang/String; = "Activity Feed - Add Friend Selection"

.field public static final AddPinItem:Ljava/lang/String; = "Activity Feed - Add Pin Item"

.field public static final ApplyFilterPreferences:Ljava/lang/String; = "Activity Feed - Apply Filter Preferences"

.field public static final Broadcast:Ljava/lang/String; = "Activity Feed - Broadcast Play"

.field public static final CloseFilterPreferences:Ljava/lang/String; = "Activity Feed - Close Filter Preferences"

.field public static final Comment:Ljava/lang/String; = "Activity Feed Comment"

.field public static final CommentPost:Ljava/lang/String; = "Activity Feed Comment Post"

.field public static final DeleteComment:Ljava/lang/String; = "Activity Feed Delete Comment"

.field public static final DeleteItem:Ljava/lang/String; = "Activity Feed Delete Item"

.field public static final DetailFilter:Ljava/lang/String; = "Activity Detail - Filter"

.field public static final FilterActivityFeed:Ljava/lang/String; = "Activity Feed - Filter Activity Feed"

.field public static final GameDVR:Ljava/lang/String; = "Activity Feed - Game DVR Play"

.field public static final HideAllItems:Ljava/lang/String; = "Activity Feed - Hide All Items"

.field public static final HideItem:Ljava/lang/String; = "Activity Feed - Hide Item"

.field public static final Like:Ljava/lang/String; = "Activity Feed Like"

.field public static final NotificationItem:Ljava/lang/String; = "Activity Feed Notification Item"

.field public static final Profile:Ljava/lang/String; = "Activity Feed - Profile Selection"

.field public static final RemovePinItem:Ljava/lang/String; = "Activity Feed - Remove Pin Item"

.field public static final ReportItem:Ljava/lang/String; = "Activity Detail - Report Item"

.field public static final Screenshot:Ljava/lang/String; = "Activity Feed - Screenshot Display"

.field public static final Share:Ljava/lang/String; = "Activity Feed Share"

.field public static final ShareAchievement:Ljava/lang/String; = "Activity Feed Share - Achievement Selection"

.field public static final ShareAddFriend:Ljava/lang/String; = "Activity Feed Share - Add Friend Selection"

.field public static final ShareBroadcast:Ljava/lang/String; = "Activity Feed Share - Broadcast Play"

.field public static final ShareClub:Ljava/lang/String; = "Activity Feed Share to Club"

.field public static final ShareFeed:Ljava/lang/String; = "Activity Feed Share to Activity Feed"

.field public static final ShareFeedPost:Ljava/lang/String; = "Activity Feed Share Feed Post"

.field public static final ShareGameDVR:Ljava/lang/String; = "Activity Feed Share - Game DVR Play"

.field public static final ShareMessage:Ljava/lang/String; = "Activity Feed Share to Message"

.field public static final ShareMessagePost:Ljava/lang/String; = "Activity Feed Share Message Post"

.field public static final ShareProfile:Ljava/lang/String; = "Activity Feed Share - Profile Selection"

.field public static final ShareScreenshot:Ljava/lang/String; = "Activity Feed Share - Screenshot Display"

.field public static final ShareStatus:Ljava/lang/String; = "Activity Feed Share - Status Selection"

.field public static final ShowMoreActions:Ljava/lang/String; = "Activity Detail - Show More Actions"

.field public static final SocialBar:Ljava/lang/String; = "Activity Social Bar"

.field public static final Status:Ljava/lang/String; = "Activity Feed - Status Selection"

.field public static final StatusPost:Ljava/lang/String; = "Activity Feed Status Post"

.field public static final SuggestedFriendAdd:Ljava/lang/String; = "Activity Feed - Add Suggested Friend"

.field public static final SuggestedFriendRemove:Ljava/lang/String; = "Activity Feed - Remove Suggested Friend"

.field public static final SuggestedFriendSeeAll:Ljava/lang/String; = "Activity Feed - See All Suggested Friends"

.field public static final SuggestedFriendViewProfile:Ljava/lang/String; = "Activity Feed - View Suggested Friend Profile"

.field public static final TrendingSeeAll:Ljava/lang/String; = "Activity Feed - See All Trending"

.field public static final UnhideItem:Ljava/lang/String; = "Activity Feed - Unhide Item"

.field public static final Unlike:Ljava/lang/String; = "Activity Feed Un-Like"

.field public static final WebLinkLaunch:Ljava/lang/String; = "Activity Feed - Web Link Launch"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
