.class public Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManagerStub;
.super Ljava/lang/Object;
.source "SLSServiceManagerStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public SearchGamertag(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .locals 1
    .param p1, "gamertag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 424
    const/4 v0, 0x0

    return-object v0
.end method

.method public addFriendToShareIdentitySetting(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 647
    const/4 v0, 0x1

    return v0
.end method

.method public addStaticSLSHeaders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 772
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    return-void
.end method

.method public addUserToFavoriteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 395
    const/4 v0, 0x0

    return v0
.end method

.method public addUserToFollowingList(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .locals 1
    .param p1, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 407
    const/4 v0, 0x0

    return-object v0
.end method

.method public addUserToNeverList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 383
    const/4 v0, 0x0

    return v0
.end method

.method public changeGamertag(Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 1
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "preview"    # Z
    .param p3, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 725
    const/4 v0, 0x1

    return v0
.end method

.method public checkGamertagAvailability(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 730
    const/4 v0, 0x1

    return v0
.end method

.method public deleteConversation(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 554
    const/4 v0, 0x0

    return v0
.end method

.method public deleteMessage(Ljava/lang/String;J)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 332
    const/4 v0, 0x0

    return v0
.end method

.method public deleteSkypeConversation(Ljava/lang/String;)Z
    .locals 1
    .param p1, "targetXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public editFirstName(Ljava/lang/String;)Z
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 668
    const/4 v0, 0x1

    return v0
.end method

.method public editLastName(Ljava/lang/String;)Z
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 673
    const/4 v0, 0x1

    return v0
.end method

.method public genericDeleteWithUri(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 571
    const/4 v0, 0x1

    return v0
.end method

.method public getAchievementDetailInfo(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 475
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAchievements(JI)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 3
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "number"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 783
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 786
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "Achievements.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :goto_0
    return-object v1

    .line 787
    :catch_0
    move-exception v0

    .line 788
    .local v0, "ex":Ljava/io/IOException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error parsing achievements stub data"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 789
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getActivityAlertSummaryInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    .locals 1
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 619
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBatchFeedItems(Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x32L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 777
    .local p1, "locators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getClubActivityFeed(JLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "clubId"    # J
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 451
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommentAlertInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 583
    const/4 v0, 0x0

    return-object v0
.end method

.method public getConversationDetail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    .locals 1
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .param p3, "startDate"    # Ljava/util/Date;
    .param p4, "endDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 548
    const/4 v0, 0x0

    return-object v0
.end method

.method public getConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 1
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 525
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntity(Ljava/lang/String;)Lcom/google/gson/JsonElement;
    .locals 1
    .param p1, "entityUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 519
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFamilySettings(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 503
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFeedItemActions(Ljava/lang/String;J)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "errorCode"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFollowerInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 481
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFollowersFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 706
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFollowingFromPeopleHub(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 701
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFollowingSummary()Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 662
    const/4 v0, 0x0

    .line 663
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    return-object v0
.end method

.method public getFriendFinderSettings()Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 720
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFriendsWhoEarnedAchievementInfo(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I
    .param p4, "startDate"    # Ljava/lang/String;
    .param p5, "endDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 681
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameClipInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "gameClipId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;-><init>()V

    .line 135
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;-><init>()V

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 136
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 137
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;-><init>()V

    .line 138
    .local v1, "uri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    const-string v2, "http://bing.com"

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    .line 139
    const-string v2, "Download"

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    .line 140
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    const-string v3, "123"

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    .line 141
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    return-object v0
.end method

.method public getGameClipsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 8
    .param p1, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "qualifier"    # Ljava/lang/String;
    .param p3, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const v7, 0x12d685    # 1.729994E-39f

    .line 256
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;-><init>()V

    .line 257
    .local v3, "profileShowcaseResult":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    .line 259
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;-><init>()V

    .line 261
    .local v0, "gameClip1":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    const-string v6, "Forza 4 Awesome Drift"

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    .line 262
    const-string v6, "2012-12-23T12:00:00Z"

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->dateRecorded:Ljava/lang/String;

    .line 263
    const-string v6, "2012-12-23T12:00:00Z"

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->lastModified:Ljava/lang/String;

    .line 264
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    .line 266
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 267
    .local v4, "thumbnail1":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    const-string v6, "http://i4.ytimg.com/vi/KR-Z3Hfl0vY/mqdefault.jpg"

    iput-object v6, v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 268
    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 271
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;-><init>()V

    .line 272
    .local v2, "gameClipUri":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    iput v7, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->fileSize:I

    .line 273
    const-string v6, "http://gdvr.origin.mediaservices.windows.net/f383db46-55c4-4b46-ae91-bdcc10e4e305/Halo%204%20Gameplay%20Launch%20Trailer.ism/Manifest"

    iput-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    .line 274
    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    const/4 v6, 0x4

    iput v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->rating:I

    .line 277
    const/16 v6, 0x3ff

    iput v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->views:I

    .line 278
    const-string v6, "Earn a perfect drift score"

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    .line 279
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;-><init>()V

    .line 283
    .local v1, "gameClip2":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    const-string v6, "Call of duty"

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->clipName:Ljava/lang/String;

    .line 284
    const-string v6, "2012-01-13T12:00:00Z"

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->dateRecorded:Ljava/lang/String;

    .line 285
    const-string v6, "2012-01-13T12:00:00Z"

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->lastModified:Ljava/lang/String;

    .line 286
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    .line 288
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;-><init>()V

    .line 289
    .local v5, "thumbnail2":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;
    const-string v6, "http://i3.ytimg.com/vi/FQmHD11VYO8/mqdefault.jpg"

    iput-object v6, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Thumbnail;->uri:Ljava/lang/String;

    .line 290
    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->thumbnails:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 293
    iput v7, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->fileSize:I

    .line 294
    const-string v6, "http://gdvr.origin.mediaservices.windows.net/f383db46-55c4-4b46-ae91-bdcc10e4e305/Halo%204%20Gameplay%20Launch%20Trailer.ism/Manifest"

    iput-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uri:Ljava/lang/String;

    .line 295
    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    const/4 v6, 0x3

    iput v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->rating:I

    .line 298
    const/16 v6, 0x100

    iput v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->views:I

    .line 299
    const-string v6, "Test caption"

    iput-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->userCaption:Ljava/lang/String;

    .line 300
    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    return-object v3
.end method

.method public getGameProgress360AchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameProgress360EarnedAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 320
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameProgressLimitedTimeChallengeInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 469
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 308
    const/4 v0, 0x0

    return-object v0
.end method

.method public getGamertagSuggestions(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p1, "seed"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 735
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getLikeDataInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .locals 5
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 158
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;-><init>()V

    .line 159
    .local v0, "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    .line 160
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    .line 161
    .local v1, "sum1":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    .line 162
    .local v2, "sum2":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;-><init>()V

    .line 164
    .local v3, "sum3":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    const/16 v4, 0x4d2

    iput v4, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 165
    const-string v4, "garbage/0662ce01-2faf-48c8-b08a-c5a5fd9e640c"

    iput-object v4, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 166
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    const/16 v4, 0x929

    iput v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 169
    const-string v4, "garbage/2a37012b-51c1-4454-ab59-1d54ed3d1124"

    iput-object v4, v2, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 170
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    const/16 v4, 0xd80

    iput v4, v3, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 173
    const-string v4, "garbage/56b90f58-473b-4281-a924-bdffddbd6a23"

    iput-object v4, v3, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 174
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    return-object v0
.end method

.method public getLiveTVSettings(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    .locals 2
    .param p1, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 369
    new-instance v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LiveTVSettings;-><init>()V

    .line 370
    .local v0, "settings":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED:I

    .line 371
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSGUIDEENABLED_BETA:I

    .line 372
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED:I

    .line 373
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSURCENABLED_BETA:I

    .line 374
    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSSHOWSPECIFICCONTENTRATINGS:I

    .line 375
    const/4 v1, 0x0

    iput v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSTREATMISSINGRATINGASADULT:I

    .line 376
    const-string v1, "ACMA:G;ACMA:P;ACMA:C;ACMA:PG;ACMA:M;ACMA:MA15;ACMA:AV15;OFLC:E;OFLC:G;OFLC:PG;OFLC:M;OFLC:MA15"

    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->SMARTGLASSNONADULTCONTENTRATINGS:Ljava/lang/String;

    .line 377
    return-object v0
.end method

.method public getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .locals 3
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 181
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;-><init>()V

    .line 182
    .local v1, "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    .line 184
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;-><init>()V

    .line 185
    .local v0, "like1":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    const-string v2, "1234"

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->xuid:Ljava/lang/String;

    .line 186
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    return-object v1
.end method

.method public getMeProfile()Lcom/microsoft/xbox/service/network/managers/UserProfile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 508
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMyShortCircuitProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 739
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNeverListInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 358
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPageActivityFeedInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 642
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleActivityFeed(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Ljava/lang/String;IZZ)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p3, "filters"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .param p4, "continuationToken"    # Ljava/lang/String;
    .param p5, "numItems"    # I
    .param p6, "getLegacyFeed"    # Z
    .param p7, "includeSelf"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;",
            ">;",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            "Ljava/lang/String;",
            "IZZ)",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 441
    .local p2, "feedTypes":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleHubBatchSummaries(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "xuilds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleHubFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 686
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleHubPeoplePlayedTitle(Ljava/lang/String;J)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 203
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleHubPerson(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleHubRecommendations()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 715
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPopularGamesWithFriends(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrivacySetting(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .locals 1
    .param p1, "settingId"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfileActivityFeedInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 566
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 430
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfileSummaryInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 463
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfileUsers(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    .locals 1
    .param p2, "settings"    # [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;",
            ")",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 595
    .local p1, "xuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRecent360ProgressAndAchievementInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    .locals 6
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x188

    const/4 v4, 0x1

    .line 218
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;-><init>()V

    .line 219
    .local v0, "recentProgressAndAchievementResult":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;->titles:Ljava/util/ArrayList;

    .line 221
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;-><init>()V

    .line 223
    .local v1, "title":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    const/16 v2, 0xc

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->currentAchievements:I

    .line 224
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->setCurrentGamerscore(I)V

    .line 225
    new-instance v2, Ljava/util/Date;

    const-string v3, "2013-05-20T03:05:56.6630000"

    invoke-direct {v2, v3}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->lastPlayed:Ljava/util/Date;

    .line 226
    const-string v2, "Modern Warfare"

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    .line 227
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->platforms:Ljava/util/ArrayList;

    .line 228
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->platforms:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    iput v5, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->sequence:I

    .line 230
    const v2, 0x10aaf9

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleId:I

    .line 231
    iput v4, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleType:I

    .line 232
    const/16 v2, 0x64

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalAchievements:I

    .line 233
    const/16 v2, 0xc8

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalGamerscore:I

    .line 235
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;->titles:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    const/16 v2, 0x22

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->currentAchievements:I

    .line 238
    const/16 v2, 0x4e

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->setCurrentGamerscore(I)V

    .line 239
    new-instance v2, Ljava/util/Date;

    const-string v3, "2013-05-20T03:05:56.6630000"

    invoke-direct {v2, v3}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->lastPlayed:Ljava/util/Date;

    .line 240
    const-string v2, "Modern Warfare"

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    .line 241
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->platforms:Ljava/util/ArrayList;

    .line 242
    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->platforms:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    iput v5, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->sequence:I

    .line 244
    const v2, 0x1ffa1d

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleId:I

    .line 245
    iput v4, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleType:I

    .line 246
    const/16 v2, 0xea

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalAchievements:I

    .line 247
    const/16 v2, 0x7b

    iput v2, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->totalGamerscore:I

    .line 249
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;->titles:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    return-object v0
.end method

.method public getRecentsFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 711
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScreenshotInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "screenshotId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;-><init>()V

    .line 148
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    return-object v0
.end method

.method public getSkypeConversationMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .locals 1
    .param p1, "friendXuid"    # Ljava/lang/String;
    .param p2, "syncState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 542
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSkypeConversationsList()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 531
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSkypeConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .locals 1
    .param p1, "paginationUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 537
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSkypeToken()Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 352
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSmartglassSettings()Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 364
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSocialTitleVips(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleActivityFeedInfo(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "titleId"    # J
    .param p3, "startDateTime"    # Ljava/lang/String;
    .param p4, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 637
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleFollowingState(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 753
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleLeaderBoardInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "statName"    # Ljava/lang/String;
    .param p4, "skipToUser"    # Z
    .param p5, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 436
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitleProgressInfo(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 457
    .local p2, "titleId":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUnsharedActivityFeed(Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "numItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 446
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserFollowingPages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 762
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserListPresenceInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 487
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .locals 4
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;-><init>()V

    .line 88
    .local v2, "userProfileResult":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    .line 90
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;-><init>()V

    .line 91
    .local v1, "userProfile":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    .line 93
    const-string v3, "2533274794434629"

    iput-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    .line 95
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;-><init>()V

    .line 96
    .local v0, "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    .line 97
    const-string v3, "BuckskinAuto829"

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 98
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AccountTier:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    .line 101
    const-string v3, "Gold"

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 102
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamerscore:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    .line 105
    const-string v3, "0"

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 106
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    .line 109
    const-string v3, "BuckskinAuto829"

    iput-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 110
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    return-object v2
.end method

.method public getUserProfilePrivacySettings()Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 677
    const/4 v0, 0x0

    return-object v0
.end method

.method public getXTokenPrivileges()[I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 498
    const/4 v0, 0x0

    return-object v0
.end method

.method public getXuidString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 153
    const-string v0, "2533274794434629"

    return-object v0
.end method

.method public markConversationRead(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 561
    return-void
.end method

.method public postComment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 613
    const/4 v0, 0x0

    return-object v0
.end method

.method public postTextToFeed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 578
    return-void
.end method

.method public removeFriendFromShareIdentitySetting(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 652
    const/4 v0, 0x0

    return v0
.end method

.method public removeUserFromFavoriteList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 401
    const/4 v0, 0x0

    return v0
.end method

.method public removeUserFromFollowingList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 413
    const/4 v0, 0x0

    return v0
.end method

.method public removeUserFromNeverList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 389
    const/4 v0, 0x0

    return v0
.end method

.method public removeUserFromRecommendationList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 418
    const/4 v0, 0x0

    return v0
.end method

.method public resetNewCommentAlert(Ljava/lang/String;)V
    .locals 0
    .param p1, "watermark"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 608
    return-void
.end method

.method public resetNewFollowerAlert(Ljava/lang/String;)V
    .locals 0
    .param p1, "watermark"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 602
    return-void
.end method

.method public sendMeLike(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .param p2, "currLikeState"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 514
    const/4 v0, 0x0

    return-object v0
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 326
    const/4 v0, 0x0

    return v0
.end method

.method public sendMessageWithAttachement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 625
    const/4 v0, 0x0

    return v0
.end method

.method public sendShortCircuitProfile(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 1
    .param p1, "request"    # Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 743
    const/4 v0, 0x0

    return-object v0
.end method

.method public sendSkypeMessage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "sendRequestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 337
    const/4 v0, 0x0

    return v0
.end method

.method public setFollowingPageState(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "follow"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 767
    return-void
.end method

.method public setFollowingState(JZ)V
    .locals 0
    .param p1, "titleId"    # J
    .param p3, "follow"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 758
    return-void
.end method

.method public setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;
    .param p2, "optInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 696
    const/4 v0, 0x0

    return v0
.end method

.method public setPrivacySettings(Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)Z
    .locals 1
    .param p1, "settings"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public setUserProfileShareIdentitySetting(Ljava/lang/String;)Z
    .locals 1
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 657
    const/4 v0, 0x1

    return v0
.end method

.method public shareToFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "itemLocator"    # Ljava/lang/String;
    .param p3, "caption"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 631
    const/4 v0, 0x0

    return-object v0
.end method

.method public updatePhoneContacts(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    .locals 1
    .param p1, "request"    # Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 747
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateThirdPartyToken(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Ljava/lang/String;)Z
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;
    .param p2, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 691
    const/4 v0, 0x0

    return v0
.end method

.method public updateUserProfileInfo(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)Z
    .locals 1
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method public uploadUserPresenceHeartBeat(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 493
    return-void
.end method
