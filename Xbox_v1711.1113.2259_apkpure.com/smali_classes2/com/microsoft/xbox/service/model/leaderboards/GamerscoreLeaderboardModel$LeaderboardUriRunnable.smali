.class Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GamerscoreLeaderboardModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LeaderboardUriRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;",
        ">;"
    }
.end annotation


# instance fields
.field private final meXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

.field private final youXuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "meXuid"    # Ljava/lang/String;
    .param p3, "youXuid"    # Ljava/lang/String;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 323
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    .line 324
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->youXuid:Ljava/lang/String;

    .line 325
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 333
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 335
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$300(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-object v4

    .line 340
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->youXuid:Ljava/lang/String;

    if-nez v5, :cond_2

    .line 341
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$400(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/util/Calendar;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->getGamerscoreLeaderboarUri(Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Ljava/util/Collection;)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;

    move-result-object v4

    goto :goto_0

    .line 343
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getCurrentLeaderboardMonth()Ljava/util/Calendar;

    move-result-object v3

    .line 344
    .local v3, "thisMonth":Ljava/util/Calendar;
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->youXuid:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v5, v6, v3, v7}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->createGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Collection;)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;

    move-result-object v0

    .line 345
    .local v0, "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;->instanceId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 346
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$400(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/util/Calendar;

    move-result-object v7

    iget-object v8, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;->instanceId:Ljava/lang/String;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->meXuid:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->youXuid:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->getGamerscoreLeaderboarUri(Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Ljava/util/Collection;)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 349
    .end local v0    # "creationResponse":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationResponse;
    .end local v3    # "thisMonth":Ljava/util/Calendar;
    :catch_0
    move-exception v1

    .line 350
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Failed to get leaderboard ID"

    invoke-static {v5, v6, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->buildData()Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 362
    const-wide/16 v0, 0x2134

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardUri;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardUriRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$800(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 358
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method
