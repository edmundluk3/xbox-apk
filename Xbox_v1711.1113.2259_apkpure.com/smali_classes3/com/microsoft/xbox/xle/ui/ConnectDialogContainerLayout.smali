.class public Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;
.super Landroid/widget/RelativeLayout;
.source "ConnectDialogContainerLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;
    }
.end annotation


# static fields
.field private static final STATE_CONNECTED:[I

.field private static final STATE_CONNECTING:[I

.field private static final STATE_CONNECT_FAILED:[I

.field private static final STATE_DISCONNECTED:[I

.field private static final STATE_DISCONNECTING:[I

.field private static final STATE_NOT_LISTED:[I


# instance fields
.field private consoleState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field private listener:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-array v0, v3, [I

    const v1, 0x7f010117

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECTED:[I

    .line 21
    new-array v0, v3, [I

    const v1, 0x7f010118

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECTING:[I

    .line 22
    new-array v0, v3, [I

    const v1, 0x7f010119

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECT_FAILED:[I

    .line 23
    new-array v0, v3, [I

    const v1, 0x7f01011a

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_DISCONNECTING:[I

    .line 24
    new-array v0, v3, [I

    const v1, 0x7f01011b

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_DISCONNECTED:[I

    .line 25
    new-array v0, v3, [I

    const v1, 0x7f01011c

    aput v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_NOT_LISTED:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->consoleState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    .line 44
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/widget/RelativeLayout;->drawableStateChanged()V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->listener:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->listener:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;->onDrawableStateChanged()V

    .line 88
    :cond_0
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 3
    .param p1, "extraSpace"    # I

    .prologue
    .line 57
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 59
    .local v0, "drawableState":[I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->consoleState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    if-eqz v1, :cond_0

    .line 60
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$ConnectDialogViewModel$ConsoleState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->consoleState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 78
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v1

    :goto_0
    return-object v1

    .line 62
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECTING:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 64
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECTED:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 66
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_CONNECT_FAILED:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 68
    :pswitch_3
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_DISCONNECTING:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 70
    :pswitch_4
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_DISCONNECTED:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 72
    :pswitch_5
    sget-object v1, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->STATE_NOT_LISTED:[I

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->mergeDrawableStates([I[I)[I

    move-result-object v1

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setConsoleState(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;)V
    .locals 2
    .param p1, "consoleState"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    .prologue
    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->consoleState:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    .line 50
    const-string v0, "ConnectDialogContainer"

    const-string v1, "refresh drawable"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->refreshDrawableState()V

    .line 52
    const-string v0, "ConnectDialogContainer"

    const-string v1, "refresh drawable done"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public setListener(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;->listener:Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout$Listener;

    .line 92
    return-void
.end method
