.class public Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;
.super Ljava/lang/Object;
.source "ProtectedRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final runnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->runnable:Ljava/lang/Runnable;

    .line 14
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 18
    const/4 v3, 0x0

    .line 19
    .local v3, "success":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-nez v3, :cond_0

    const/16 v6, 0xa

    if-ge v1, v6, :cond_0

    .line 21
    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    const-string v7, "Protected run begins"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    iget-object v6, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->runnable:Ljava/lang/Runnable;

    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    .line 23
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    const-string v7, "Protected run ends"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const/4 v3, 0x1

    .line 25
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Proteced run succeeded on attempt #"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 26
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/LinkageError;
    const-wide/16 v4, 0x1f4

    .line 38
    .local v4, "sleepTime":J
    :try_start_1
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    const-string v7, "Error during protected run, sleeping for 500 milliseconds"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 40
    :catch_1
    move-exception v2

    .line 41
    .local v2, "ie":Ljava/lang/InterruptedException;
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error while sleeping"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 45
    .end local v0    # "e":Ljava/lang/LinkageError;
    .end local v2    # "ie":Ljava/lang/InterruptedException;
    .end local v4    # "sleepTime":J
    :cond_0
    if-nez v3, :cond_1

    .line 46
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;->TAG:Ljava/lang/String;

    const-string v7, "protected run failed"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_1
    return-void
.end method
