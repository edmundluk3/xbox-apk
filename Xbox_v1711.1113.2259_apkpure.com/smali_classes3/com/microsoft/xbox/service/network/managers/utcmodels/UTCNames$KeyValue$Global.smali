.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$Global;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Global"
.end annotation


# static fields
.field public static final MeProfile:Ljava/lang/String; = "MeProfile"

.field public static final No:Ljava/lang/String; = "No"

.field public static final OptIn:Ljava/lang/String; = "OptIn"

.field public static final OptOut:Ljava/lang/String; = "OptOut"

.field public static final Unknown:Ljava/lang/String; = "UNKNOWN"

.field public static final Yes:Ljava/lang/String; = "Yes"

.field public static final YouProfile:Ljava/lang/String; = "YouProfile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
