.class public Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;
.super Landroid/widget/ProgressBar;
.source "XLELoadingIndicator.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->init()V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->init()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->init()V

    .line 23
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 27
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;->setIndeterminate(Z)V

    .line 28
    return-void
.end method
