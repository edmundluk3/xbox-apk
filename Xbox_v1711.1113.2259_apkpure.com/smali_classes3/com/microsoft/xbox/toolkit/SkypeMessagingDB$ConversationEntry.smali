.class public abstract Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;
.super Ljava/lang/Object;
.source "SkypeMessagingDB.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ConversationEntry"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;

.field public static final COL_CLEAREDAT:Ljava/lang/String; = "clearedat"

.field public static final COL_CONSUMPTIONHORIZON:Ljava/lang/String; = "consumptionhorizon"

.field public static final COL_GAMERPICURL:Ljava/lang/String; = "gamerpicurl"

.field public static final COL_ID:Ljava/lang/String; = "id"

.field public static final COL_ISGROUPCONVERSATION:Ljava/lang/String; = "isgroupconversation"

.field public static final COL_LASTJOINED:Ljava/lang/String; = "lastjoined"

.field public static final COL_LASTMSG_CLIENTMESSAGEID:Ljava/lang/String; = "lastmsgclientmessageid"

.field public static final COL_LASTMSG_CONTENT:Ljava/lang/String; = "lastmsgcontent"

.field public static final COL_LASTMSG_CONVERSATIONID:Ljava/lang/String; = "lastmsgconversationid"

.field public static final COL_LASTMSG_CONVERSATIONLINK:Ljava/lang/String; = "lastmsgconversationlink"

.field public static final COL_LASTMSG_FROM:Ljava/lang/String; = "lastmsgfrom"

.field public static final COL_LASTMSG_ID:Ljava/lang/String; = "lastmsgid"

.field public static final COL_LASTMSG_MESSAGETYPE:Ljava/lang/String; = "lastmsgmessagetype"

.field public static final COL_LASTMSG_ORIGINALARRIVALTIME:Ljava/lang/String; = "lastmsgoriginalarrivaltime"

.field public static final COL_LASTMSG_SKYPEEDITEDID:Ljava/lang/String; = "lastmsgskypeeditedid"

.field public static final COL_LASTMSG_TYPE:Ljava/lang/String; = "lastmsgtype"

.field public static final COL_MESSAGETYPE:Ljava/lang/String; = "messagetype"

.field public static final COL_MUTED:Ljava/lang/String; = "muted"

.field public static final COL_REALNAME:Ljava/lang/String; = "realname"

.field public static final COL_SENDERGAMERTAG:Ljava/lang/String; = "sendergamertag"

.field public static final COL_TOPIC:Ljava/lang/String; = "topicname"

.field public static final COL_TYPE:Ljava/lang/String; = "type"

.field public static final CREATE_CONVERSATIONS_TABLE:Ljava/lang/String;

.field public static final DELETE_CONVERSATIONS_TABLE:Ljava/lang/String;

.field public static final TABLE_NAME:Ljava/lang/String; = "conversations"


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1491
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s %s (%s %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s, %s %s)"

    const/16 v2, 0x31

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "CREATE TABLE IF NOT EXISTS"

    aput-object v3, v2, v5

    const-string v3, "conversations"

    aput-object v3, v2, v6

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "INTEGER"

    aput-object v3, v2, v8

    const-string v3, "PRIMARY KEY AUTOINCREMENT"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "type"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "messagetype"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "consumptionhorizon"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "muted"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "INTEGER"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "clearedat"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "sendergamertag"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "realname"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string v4, "gamerpicurl"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string v4, "topicname"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "lastjoined"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string v4, "isgroupconversation"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string v4, "INTEGER"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string v4, "lastmsgoriginalarrivaltime"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    const-string v4, "INTEGER"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string v4, "lastmsgmessagetype"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string v4, "lastmsgcontent"

    aput-object v4, v2, v3

    const/16 v3, 0x22

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string v4, "lastmsgid"

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x25

    const-string v4, "lastmsgfrom"

    aput-object v4, v2, v3

    const/16 v3, 0x26

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x27

    const-string v4, "lastmsgconversationlink"

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x29

    const-string v4, "lastmsgskypeeditedid"

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    const-string v4, "lastmsgtype"

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    const-string v4, "lastmsgconversationid"

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    const-string v4, "lastmsgclientmessageid"

    aput-object v4, v2, v3

    const/16 v3, 0x30

    const-string v4, "TEXT"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->CREATE_CONVERSATIONS_TABLE:Ljava/lang/String;

    .line 1519
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s %s"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "DROP TABLE IF EXISTS"

    aput-object v3, v2, v5

    const-string v3, "conversations"

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->DELETE_CONVERSATIONS_TABLE:Ljava/lang/String;

    .line 1524
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "id"

    aput-object v1, v0, v6

    const-string v1, "type"

    aput-object v1, v0, v7

    const-string v1, "messagetype"

    aput-object v1, v0, v8

    const-string v1, "consumptionhorizon"

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const-string v2, "muted"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "clearedat"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sendergamertag"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "realname"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "gamerpicurl"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "topicname"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "lastjoined"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "isgroupconversation"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "lastmsgoriginalarrivaltime"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "lastmsgmessagetype"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "lastmsgcontent"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "lastmsgid"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "lastmsgfrom"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "lastmsgconversationlink"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "lastmsgskypeeditedid"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "lastmsgtype"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "lastmsgconversationid"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "lastmsgclientmessageid"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
