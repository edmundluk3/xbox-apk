.class public Lcom/microsoft/xle/test/interop/CrashReporter;
.super Ljava/lang/Object;
.source "CrashReporter.java"


# static fields
.field private static handleThrowableMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private static takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    sput-object v0, Lcom/microsoft/xle/test/interop/CrashReporter;->handleThrowableMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 10
    sput-object v0, Lcom/microsoft/xle/test/interop/CrashReporter;->takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleThrowable(Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 26
    return-void
.end method

.method public static setTakeScreenshot(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "action":Lcom/microsoft/xle/test/interop/delegates/Action1;, "Lcom/microsoft/xle/test/interop/delegates/Action1<Landroid/view/View;>;"
    sput-object p0, Lcom/microsoft/xle/test/interop/CrashReporter;->takeSnapshotMethod:Lcom/microsoft/xle/test/interop/delegates/Action1;

    .line 32
    return-void
.end method

.method public static setThrowableHandler(Lcom/microsoft/xle/test/interop/delegates/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xle/test/interop/delegates/Action1",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "action":Lcom/microsoft/xle/test/interop/delegates/Action1;, "Lcom/microsoft/xle/test/interop/delegates/Action1<Ljava/lang/Throwable;>;"
    return-void
.end method

.method public static takeScreenshot()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
