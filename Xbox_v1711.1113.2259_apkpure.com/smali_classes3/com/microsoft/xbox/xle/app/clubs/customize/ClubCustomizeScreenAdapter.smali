.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubCustomizeScreenAdapter.java"


# static fields
.field private static final COLUMN_MARGIN:I = 0xf

.field private static final COLUMN_WIDTH:I = 0x64


# instance fields
.field private final addBackgroundImageTitle:Landroid/widget/TextView;

.field private final addGamesDescription:Landroid/widget/TextView;

.field private final addGamesTitle:Landroid/widget/TextView;

.field private final addTagsDescription:Landroid/widget/TextView;

.field private final addTagsTitle:Landroid/widget/TextView;

.field private final clubBackgroundImageView:Landroid/widget/ImageView;

.field private final clubDescriptionTextView:Landroid/widget/TextView;

.field private final clubDescriptionTitleTextView:Landroid/widget/TextView;

.field private final clubDisplayImageView:Landroid/widget/ImageView;

.field private final clubNameTextView:Landroid/widget/TextView;

.field private final customizeNameContainer:Landroid/view/View;

.field private final imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

.field private final rootView:Landroid/view/View;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final tagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field private final tagFlowLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

.field private final titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 8
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v7, 0xf

    .line 58
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 59
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .line 62
    const v2, 0x7f0e0a95

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 63
    const v2, 0x7f0e032e

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->rootView:Landroid/view/View;

    .line 65
    const v2, 0x7f0e032f

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->customizeNameContainer:Landroid/view/View;

    .line 67
    const v2, 0x7f0e0341

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 72
    .local v0, "adjustment":I
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const/16 v3, 0x64

    invoke-static {v2, v7, v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->calculateGridColumnCount(Landroid/content/Context;III)I

    move-result v1

    .line 73
    .local v1, "columnNumbers":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/GridLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v3, v4, v1, v5, v6}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;

    invoke-direct {v3, v1, v7}, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 75
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;-><init>(Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 78
    const v2, 0x7f0e0342

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addGamesTitle:Landroid/widget/TextView;

    .line 79
    const v2, 0x7f0e0343

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addGamesDescription:Landroid/widget/TextView;

    .line 81
    const v2, 0x7f0e0336

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addBackgroundImageTitle:Landroid/widget/TextView;

    .line 83
    const v2, 0x7f0e0338

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagFlowLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    .line 84
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0c014e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 85
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagFlowLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 87
    const v2, 0x7f0e033a

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addTagsTitle:Landroid/widget/TextView;

    .line 88
    const v2, 0x7f0e033b

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addTagsDescription:Landroid/widget/TextView;

    .line 90
    const v2, 0x7f0e0335

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDisplayImageView:Landroid/widget/ImageView;

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDisplayImageView:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v2, 0x7f0e0332

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubBackgroundImageView:Landroid/widget/ImageView;

    .line 94
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubBackgroundImageView:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v2, 0x7f0e033e

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDescriptionTextView:Landroid/widget/TextView;

    .line 97
    const v2, 0x7f0e033d

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDescriptionTitleTextView:Landroid/widget/TextView;

    .line 98
    const v2, 0x7f0e0330

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubNameTextView:Landroid/widget/TextView;

    .line 100
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->customizeNameContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v2, 0x7f0e0337

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v2, 0x7f0e033c

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v2, 0x7f0e0344

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const v2, 0x7f0e0331

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v2, 0x7f0e0345

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchDisplayImageSelection()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchBackgroundImageSelection()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchClubRename()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchTagsSelection()V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchDescriptionSelection()V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchTitleSelection()V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->launchColorSelection()V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->finishCustomization()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 12

    .prologue
    const v11, 0x7f020122

    const v10, 0x7f020115

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 110
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    .line 111
    .local v5, "viewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 112
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v5, v6, :cond_2

    .line 114
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->rootView:Landroid/view/View;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubColor()I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 116
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->customizeNameContainer:Landroid/view/View;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->isCustomizeNameEnabled()Z

    move-result v9

    invoke-static {v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 118
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubDisplayImage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 119
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDisplayImageView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubDisplayImage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9, v11, v10}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 122
    :cond_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubBackgroundImage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 123
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubBackgroundImageView:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubBackgroundImage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9, v11, v10}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 124
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addBackgroundImageTitle:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubNameTextView:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubDescription()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "clubDescription":Ljava/lang/String;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    :goto_1
    invoke-static {v9, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 133
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDescriptionTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    invoke-static {v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 134
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getClubTags()Ljava/util/List;

    move-result-object v1

    .line 137
    .local v1, "clubTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    .line 138
    .local v2, "emptyTags":Z
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addTagsTitle:Landroid/widget/TextView;

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 139
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addTagsDescription:Landroid/widget/TextView;

    invoke-static {v6, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 140
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagFlowLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    if-nez v2, :cond_5

    move v6, v7

    :goto_2
    invoke-static {v9, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 141
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 142
    if-nez v2, :cond_1

    .line 143
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->tagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 146
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;->getTitleImages()Ljava/util/List;

    move-result-object v4

    .line 147
    .local v4, "titlesImages":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    .line 148
    .local v3, "emptyTitles":Z
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addGamesTitle:Landroid/widget/TextView;

    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 149
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addGamesDescription:Landroid/widget/TextView;

    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 150
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->titlesRecyclerView:Landroid/support/v7/widget/RecyclerView;

    if-nez v3, :cond_6

    :goto_3
    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 151
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->clear()V

    .line 152
    if-nez v3, :cond_2

    .line 153
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->addAll(Ljava/util/Collection;)V

    .line 154
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->imageAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->notifyDataSetChanged()V

    .line 157
    .end local v0    # "clubDescription":Ljava/lang/String;
    .end local v1    # "clubTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v2    # "emptyTags":Z
    .end local v3    # "emptyTitles":Z
    .end local v4    # "titlesImages":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    return-void

    .line 126
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->clubBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;->addBackgroundImageTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .restart local v0    # "clubDescription":Ljava/lang/String;
    :cond_4
    move v6, v8

    .line 132
    goto :goto_1

    .restart local v1    # "clubTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .restart local v2    # "emptyTags":Z
    :cond_5
    move v6, v8

    .line 140
    goto :goto_2

    .restart local v3    # "emptyTitles":Z
    .restart local v4    # "titlesImages":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_6
    move v7, v8

    .line 150
    goto :goto_3
.end method
