.class public interface abstract Lcom/flipkart/chatheads/ui/ChatHeadListener;
.super Ljava/lang/Object;
.source "ChatHeadListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onAllChatHeadsRemoved(Z)V
.end method

.method public abstract onChatHeadAdded(Ljava/io/Serializable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract onChatHeadAnimateEnd(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onChatHeadAnimateStart(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onChatHeadArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V
.end method

.method public abstract onChatHeadRemoved(Ljava/io/Serializable;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation
.end method
