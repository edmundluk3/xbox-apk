.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;
.super Ljava/lang/Object;
.source "UTCGameHub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackFollowToggleAction(JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$follow:Z

.field final synthetic val$titleid:J


# direct methods
.method constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 154
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;->val$titleid:J

    iput-boolean p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;->val$follow:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 157
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 158
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;->val$titleid:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;->val$follow:Z

    if-eqz v1, :cond_0

    .line 161
    const-string v1, "Game Hub - Follow"

    const-string v2, "Game Hub - Info View"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v1, "Game Hub - Unfollow"

    const-string v2, "Game Hub - Info View"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    goto :goto_0
.end method
