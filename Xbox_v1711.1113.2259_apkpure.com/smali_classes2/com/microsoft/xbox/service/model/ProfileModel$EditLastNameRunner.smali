.class Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditLastNameRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private lastName:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "lastName"    # Ljava/lang/String;

    .prologue
    .line 2594
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2595
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2596
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->lastName:Ljava/lang/String;

    .line 2597
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2601
    new-instance v1, Lcom/microsoft/xbox/service/model/sls/EditLastNameRequest;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->lastName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/sls/EditLastNameRequest;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/sls/EditLastNameRequest;->getEditLastNameRequestBody(Lcom/microsoft/xbox/service/model/sls/EditLastNameRequest;)Ljava/lang/String;

    move-result-object v0

    .line 2602
    .local v0, "postBody":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->editLastName(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2590
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2612
    const-wide/16 v0, 0xf9a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2617
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$EditLastNameRunner;->lastName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->onEditLastNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 2618
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2607
    return-void
.end method
