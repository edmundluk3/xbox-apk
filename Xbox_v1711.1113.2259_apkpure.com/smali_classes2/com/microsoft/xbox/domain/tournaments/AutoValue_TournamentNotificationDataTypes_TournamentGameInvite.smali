.class final Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;
.super Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;
.source "AutoValue_TournamentNotificationDataTypes_TournamentGameInvite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;)V
    .locals 0
    .param p1, "sessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .param p2, "tournamentRef"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .param p3, "invitedXuid"    # Ljava/lang/String;
    .param p4, "titleId"    # J
    .param p6, "titleName"    # Ljava/lang/String;
    .param p7, "titleImageUri"    # Ljava/lang/String;
    .param p8, "teamName"    # Ljava/lang/String;
    .param p9, "teamId"    # Ljava/lang/String;
    .param p10, "tournamentName"    # Ljava/lang/String;
    .param p11, "tournamentImageUri"    # Ljava/lang/String;
    .param p12, "gameTypes"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    .param p13, "continuationUris"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .prologue
    .line 20
    invoke-direct/range {p0 .. p13}, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;)V

    .line 21
    return-void
.end method
