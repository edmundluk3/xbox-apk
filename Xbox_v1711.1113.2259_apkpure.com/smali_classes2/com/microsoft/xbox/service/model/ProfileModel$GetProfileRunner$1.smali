.class Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;
.super Ljava/lang/Object;
.source "ProfileModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->buildData()Lcom/microsoft/xbox/service/model/ProfileData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

.field final synthetic val$profileUser:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

.field final synthetic val$serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    .prologue
    .line 2252
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$profileUser:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2257
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->access$400(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFamilySettings(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FamilySettings;

    move-result-object v0

    .line 2259
    .local v0, "familySettings":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/FamilySettings;->familyUsers:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 2260
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/FamilySettings;->familyUsers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2261
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/FamilySettings;->familyUsers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;->xuid:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->access$400(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2262
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$profileUser:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/FamilySettings;->familyUsers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;

    iget-boolean v3, v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;->canViewTVAdultContent:Z

    iput-boolean v3, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->canViewTVAdultContent:Z

    .line 2263
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$profileUser:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/FamilySettings;->familyUsers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;

    iget v3, v3, Lcom/microsoft/xbox/service/network/managers/FamilyUser;->maturityLevel:I

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->setmaturityLevel(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 2276
    .end local v0    # "familySettings":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .end local v1    # "i":I
    :cond_0
    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->val$serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMeProfile()Lcom/microsoft/xbox/service/network/managers/UserProfile;

    move-result-object v2

    .line 2277
    .local v2, "userProfile":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;->firstName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->setFirstName(Ljava/lang/String;)V

    .line 2278
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;->lastName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->setLastName(Ljava/lang/String;)V

    .line 2279
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;->email:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->setEmail(Ljava/lang/String;)V

    .line 2280
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-wide v4, v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;->homeConsole:J

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$502(Lcom/microsoft/xbox/service/model/ProfileModel;J)J

    .line 2281
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    iget-boolean v4, v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;->isAdult:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->setIsMeAdult(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2284
    .end local v2    # "userProfile":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    :goto_2
    return-void

    .line 2260
    .restart local v0    # "familySettings":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2282
    .end local v0    # "familySettings":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .end local v1    # "i":I
    :catch_0
    move-exception v3

    goto :goto_2

    .line 2268
    :catch_1
    move-exception v3

    goto :goto_1
.end method
