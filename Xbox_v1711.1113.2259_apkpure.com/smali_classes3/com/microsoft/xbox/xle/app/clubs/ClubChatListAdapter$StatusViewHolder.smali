.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;
.source "ClubChatListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "StatusViewHolder"
.end annotation


# instance fields
.field private final statusText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .line 273
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->statusText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 278
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 6
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 281
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 283
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    iget-object v1, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 293
    const-string v0, "Not supported message type for status"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->statusText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 296
    :goto_0
    return-void

    .line 285
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->statusText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 286
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0702ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 285
    invoke-static {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 289
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;->statusText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 290
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070300

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 289
    invoke-static {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
