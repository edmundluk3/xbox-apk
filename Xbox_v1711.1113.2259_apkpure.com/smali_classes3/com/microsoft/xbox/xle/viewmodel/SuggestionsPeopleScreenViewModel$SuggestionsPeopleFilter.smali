.class public final enum Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
.super Ljava/lang/Enum;
.source "SuggestionsPeopleScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestionsPeopleFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field public static final enum ALL_PEOPLE:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field public static final enum FACEBOOK_FRIENDS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field public static final enum PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field public static final enum PYMK:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

.field public static final enum VIPS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 46
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    const-string v1, "ALL_PEOPLE"

    const v2, 0x7f070c91

    const-string v3, "AllPeople"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ALL_PEOPLE:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    const-string v1, "PYMK"

    const v2, 0x7f070c95

    const-string v3, "People you may know"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PYMK:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    const-string v1, "VIPS"

    const v2, 0x7f070c96

    const-string v3, "VIPs"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->VIPS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    const-string v1, "FACEBOOK_FRIENDS"

    const v2, 0x7f070c92

    const-string v3, "Facebook Friends"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->FACEBOOK_FRIENDS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    const-string v1, "PHONE_CONTACTS"

    const v2, 0x7f070551

    const-string v3, "Phone Friends"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->ALL_PEOPLE:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PYMK:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->VIPS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->FACEBOOK_FRIENDS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    aput-object v1, v0, v8

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 59
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->resId:I

    .line 60
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->telemetryName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
