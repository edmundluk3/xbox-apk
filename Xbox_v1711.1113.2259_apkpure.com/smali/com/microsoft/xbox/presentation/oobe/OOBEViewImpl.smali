.class public Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviActivityBase;
.source "OOBEViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/oobe/OOBEView;


# static fields
.field private static final CODE_ENTRY_STATE:I = 0x0

.field private static final COLOR_PROGRESS_CURRENT:I = -0xef83f0

.field private static final COLOR_PROGRESS_NEXT:I = -0xcccccd

.field private static final COLOR_PROGRESS_PREVIOUS:I = -0xeaa8eb

.field private static final COMPLETION_IME_ACTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMPLETION_STATE:I = 0x4

.field private static final POWER_SETTINGS_STATE:I = 0x2

.field private static final TIME_ZONE_STATE:I = 0x1

.field private static final UPDATE_SETTINGS_STATE:I = 0x3


# instance fields
.field private additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field backButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0826
    .end annotation
.end field

.field closeButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e080e
    .end annotation
.end field

.field completionDescription:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0825
    .end annotation
.end field

.field completionHeader:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0824
    .end annotation
.end field

.field enterCodeDetails:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0817
    .end annotation
.end field

.field enterCodeError:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0819
    .end annotation
.end field

.field enterCodeHeader:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0816
    .end annotation
.end field

.field enterCodeInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0818
    .end annotation
.end field

.field private fatalAlertVisible:Z

.field private firstRefreshComplete:Z

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0827
    .end annotation
.end field

.field powerEnergySaving:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081f
    .end annotation
.end field

.field powerInstantOn:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0820
    .end annotation
.end field

.field powerSettingsGroup:Landroid/widget/RadioGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081e
    .end annotation
.end field

.field powerSettingsHeader:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081d
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field progressViews:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f0e0810,
            0x7f0e0811,
            0x7f0e0812,
            0x7f0e0813,
            0x7f0e0814
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0815
    .end annotation
.end field

.field timeZoneAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081c
    .end annotation
.end field

.field timeZoneHeader:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081a
    .end annotation
.end field

.field timeZoneSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e081b
    .end annotation
.end field

.field updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0822
    .end annotation
.end field

.field updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0823
    .end annotation
.end field

.field updateSettingsHeader:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0821
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    new-array v1, v7, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x6

    .line 77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 78
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 79
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 80
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 81
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 76
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->COMPLETION_IME_ACTIONS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;-><init>()V

    .line 155
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 155
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 166
    return-void
.end method

.method private hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 435
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 436
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 437
    return-void
.end method

.method static synthetic lambda$onClose$17(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->hideSoftKeyboard()V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->presenter:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->trackClose()V

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 285
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 180
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->COMPLETION_IME_ACTIONS:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;->with(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->hideSoftKeyboard()V

    .line 185
    const/4 v0, 0x1

    .line 188
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Integer;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignore"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->firstRefreshComplete:Z

    return v0
.end method

.method static synthetic lambda$onCreateContentView$10(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;->with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoAppUpdatesChangedIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$11(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;
    .locals 1
    .param p0, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 221
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$12(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$13(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 239
    const-string v0, "Unexpected switch panel state"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->error(Ljava/lang/String;)V

    .line 240
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;

    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->hideSoftKeyboard()V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;->with(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$EnterCodeIntent;

    move-result-object v0

    goto :goto_0

    .line 232
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$NextIntent;

    goto :goto_0

    .line 234
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    goto :goto_0

    .line 236
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic lambda$onCreateContentView$14(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Long;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$15(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Long;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$16(Ljava/lang/Long;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;
    .locals 1
    .param p0, "ignored"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 248
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$PingIntent;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$2(Ljava/lang/Integer;)Z
    .locals 2
    .param p0, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreateContentView$3(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Integer;)Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "index"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$4(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;->with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DSTOptionChangedIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$5(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderPowerNextButton()V

    return-void
.end method

.method static synthetic lambda$onCreateContentView$6(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;
    .locals 1
    .param p0, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 208
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;->with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$7(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderPowerNextButton()V

    return-void
.end method

.method static synthetic lambda$onCreateContentView$8(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;
    .locals 1
    .param p0, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;->with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$InstantOnChangedIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$9(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;->with(Z)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$AutoSystemUpdatesChangedIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$showServerErrorDialog$18(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .prologue
    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private renderCodeEntryState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 338
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 339
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderProgress(I)V

    .line 341
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeError:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->badCode()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 347
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderOptions(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    .line 349
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;->fatalError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->showServerErrorDialog()V

    .line 352
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 341
    goto :goto_0
.end method

.method private renderCompletionState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    .prologue
    const/4 v3, 0x4

    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 397
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderProgress(I)V

    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->closeButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 402
    return-void
.end method

.method private renderOptions(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V
    .locals 4
    .param p1, "options"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .prologue
    const/4 v1, 0x0

    .line 405
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->timeZone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->getPosition(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 406
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoDST()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 409
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v0

    .line 410
    .local v0, "instantOn":Ljava/lang/Boolean;
    if-eqz v0, :cond_1

    .line 411
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 412
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 418
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateSystem()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 419
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateApps()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 420
    return-void

    .line 414
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 415
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method private renderPowerNextButton()V
    .locals 2

    .prologue
    .line 378
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 379
    return-void

    .line 378
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private renderPowerSettingsState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;)V
    .locals 3
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    .prologue
    const/4 v1, 0x2

    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 367
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderProgress(I)V

    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 371
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderPowerNextButton()V

    .line 373
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderOptions(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    .line 374
    return-void
.end method

.method private renderProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 423
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 424
    if-ge v0, p1, :cond_0

    .line 425
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->progressViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v2, -0xeaa8eb

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 423
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 426
    :cond_0
    if-ne v0, p1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->progressViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v2, -0xef83f0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->progressViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v2, -0xcccccd

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 432
    :cond_2
    return-void
.end method

.method private renderTimeZoneState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    .prologue
    const/4 v3, 0x1

    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 356
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderProgress(I)V

    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 362
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderOptions(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    .line 363
    return-void
.end method

.method private renderUpdateSettingsState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;)V
    .locals 3
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    .prologue
    const/4 v1, 0x3

    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 383
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderProgress(I)V

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 388
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderOptions(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    .line 390
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->fatalError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->showServerErrorDialog()V

    .line 393
    :cond_0
    return-void
.end method

.method private declared-synchronized showServerErrorDialog()V
    .locals 5

    .prologue
    .line 310
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->fatalAlertVisible:Z

    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->fatalAlertVisible:Z

    .line 312
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    .line 313
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0707fd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 314
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0707fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 315
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0707fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Ljava/lang/Runnable;

    move-result-object v4

    .line 312
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :cond_0
    monitor-exit p0

    return-void

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    const-string v0, "OOBE"

    return-object v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->presenter:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    return-object v0
.end method

.method public onBackButtonPressed()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 307
    :goto_0
    return-void

    .line 296
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->onClose()V

    goto :goto_0

    .line 301
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$BackIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 304
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$DoneIntent;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 294
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method onClose()V
    .locals 6
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0e080e
        }
    .end annotation

    .prologue
    .line 277
    .line 278
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0707cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 280
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070738

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Ljava/lang/Runnable;

    move-result-object v3

    .line 286
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070736

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 277
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 288
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->onCreate()V

    .line 171
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V

    .line 172
    return-void
.end method

.method public onCreateContentView()V
    .locals 5

    .prologue
    .line 176
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviActivityBase;->onCreateContentView()V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 191
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03022d

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->getAllItems()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03022e

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 195
    const/16 v0, 0xa

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneSpinner:Landroid/widget/Spinner;

    .line 196
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/widget/RxAdapterView;->itemSelections(Landroid/widget/AdapterView;)Lcom/jakewharton/rxbinding2/InitialValueObservable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 197
    invoke-virtual {v2, v3}, Lcom/jakewharton/rxbinding2/InitialValueObservable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 198
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 199
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 200
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 201
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;

    .line 203
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 204
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    .line 206
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 207
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 208
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    .line 210
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 211
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 212
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 214
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 215
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 217
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 218
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    .line 220
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 221
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    .line 223
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 224
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v3

    .line 225
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-wide/16 v2, 0x1e

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 245
    invoke-static {v2, v3, v4}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 246
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)Lio/reactivex/functions/Predicate;

    move-result-object v3

    .line 247
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl$$Lambda$19;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 248
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->additionalViewIntents:Lcom/jakewharton/rxrelay2/PublishRelay;

    aput-object v2, v0, v1

    .line 195
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 252
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;)V
    .locals 1
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    .prologue
    .line 322
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    if-eqz v0, :cond_1

    .line 323
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;

    .end local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderCodeEntryState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECodeEntryState;)V

    .line 334
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->firstRefreshComplete:Z

    .line 335
    return-void

    .line 324
    .restart local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    if-eqz v0, :cond_2

    .line 325
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;

    .end local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderTimeZoneState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBETimeZoneState;)V

    goto :goto_0

    .line 326
    .restart local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_2
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    if-eqz v0, :cond_3

    .line 327
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;

    .end local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderPowerSettingsState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEPowerSettingsState;)V

    goto :goto_0

    .line 328
    .restart local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_3
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    if-eqz v0, :cond_4

    .line 329
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    .end local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderUpdateSettingsState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;)V

    goto :goto_0

    .line 330
    .restart local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    :cond_4
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    if-eqz v0, :cond_0

    .line 331
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;

    .end local p1    # "viewState":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->renderCompletionState(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBECompletionState;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->render(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;)V

    return-void
.end method

.method protected setContentView()V
    .locals 1

    .prologue
    .line 267
    const v0, 0x7f03019b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->setContentView(I)V

    .line 268
    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<+",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
