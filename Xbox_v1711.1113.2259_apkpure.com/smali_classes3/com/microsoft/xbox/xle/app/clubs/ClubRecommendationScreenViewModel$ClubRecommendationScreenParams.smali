.class public Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "ClubRecommendationScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClubRecommendationScreenParams"
.end annotation


# instance fields
.field private final criteria:Ljava/lang/String;

.field private final header:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "criteria"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 157
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->header:Ljava/lang/String;

    .line 160
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->criteria:Ljava/lang/String;

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->criteria:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$ClubRecommendationScreenParams;->header:Ljava/lang/String;

    return-object v0
.end method
