.class public abstract Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "HomeScreenPopupScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;,
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;"
    }
.end annotation


# instance fields
.field protected commands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->commands:Ljava/util/ArrayList;

    return-void
.end method

.method protected static getPlaceholderRid(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)I
    .locals 2
    .param p0, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 106
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 107
    .local v0, "mediaType":I
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v1

    return v1

    .line 106
    .end local v0    # "mediaType":I
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public abstract createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<TT;>;"
        }
    .end annotation
.end method

.method public getCommands()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->commands:Ljava/util/ArrayList;

    return-object v0
.end method

.method public abstract getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract handleCommand(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    return-void
.end method

.method public onCloseTouched()V
    .locals 1

    .prologue
    .line 49
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 50
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 22
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 23
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    return-void
.end method
