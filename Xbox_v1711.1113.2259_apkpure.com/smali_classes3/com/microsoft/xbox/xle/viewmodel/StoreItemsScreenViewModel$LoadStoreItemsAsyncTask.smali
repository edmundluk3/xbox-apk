.class Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "StoreItemsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadStoreItemsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

.field private loadMore:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;Z)V
    .locals 0
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .param p3, "loadMore"    # Z

    .prologue
    .line 270
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 271
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 272
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->loadMore:Z

    .line 273
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 277
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 278
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->loadMore:Z

    if-eqz v0, :cond_0

    .line 279
    const/4 v0, 0x1

    .line 281
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->shouldRefresh()Z

    move-result v0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 309
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->loadMore:Z

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->loadMore()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 312
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 286
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->onLoadItemsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V

    .line 288
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->onLoadItemsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)V

    .line 299
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 265
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 292
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel$LoadStoreItemsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;Z)Z

    .line 294
    return-void
.end method
