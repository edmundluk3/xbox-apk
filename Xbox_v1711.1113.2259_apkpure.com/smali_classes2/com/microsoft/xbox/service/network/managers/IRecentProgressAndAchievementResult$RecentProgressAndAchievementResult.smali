.class public Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
.super Ljava/lang/Object;
.source "IRecentProgressAndAchievementResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecentProgressAndAchievementResult"
.end annotation


# instance fields
.field public pagingInfo:Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$PagingInfo;

.field public titles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 91
    const-class v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;

    const-class v1, Ljava/util/Date;

    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;-><init>()V

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;

    return-object v0
.end method
