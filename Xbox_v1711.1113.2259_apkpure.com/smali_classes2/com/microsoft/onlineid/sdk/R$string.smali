.class public final Lcom/microsoft/onlineid/sdk/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/onlineid/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_menu_add_account:I = 0x7f070047

.field public static final account_picker_list_body:I = 0x7f070048

.field public static final account_picker_list_header:I = 0x7f070049

.field public static final account_picker_menu_dismiss:I = 0x7f07004a

.field public static final account_setting_up:I = 0x7f07004b

.field public static final account_setting_up_header:I = 0x7f07004c

.field public static final app_market:I = 0x7f07004d

.field public static final authentication_button_finish:I = 0x7f07004e

.field public static final authentication_button_next:I = 0x7f07004f

.field public static final authentication_button_previous:I = 0x7f070050

.field public static final common_google_play_services_enable_button:I = 0x7f070029

.field public static final common_google_play_services_enable_text:I = 0x7f07002a

.field public static final common_google_play_services_enable_title:I = 0x7f07002b

.field public static final common_google_play_services_install_button:I = 0x7f07002c

.field public static final common_google_play_services_install_text_phone:I = 0x7f07002d

.field public static final common_google_play_services_install_text_tablet:I = 0x7f07002e

.field public static final common_google_play_services_install_title:I = 0x7f07002f

.field public static final common_google_play_services_unknown_issue:I = 0x7f070031

.field public static final common_google_play_services_unsupported_text:I = 0x7f070032

.field public static final common_google_play_services_unsupported_title:I = 0x7f070033

.field public static final common_google_play_services_update_button:I = 0x7f070034

.field public static final common_google_play_services_update_text:I = 0x7f070035

.field public static final common_google_play_services_update_title:I = 0x7f070036

.field public static final common_signin_button_text:I = 0x7f07003b

.field public static final common_signin_button_text_long:I = 0x7f07003c

.field public static final error_body_generic_failure:I = 0x7f070051

.field public static final error_header_generic_failure:I = 0x7f070052

.field public static final error_header_server_network_error:I = 0x7f070053

.field public static final error_overlay_no_network:I = 0x7f070054

.field public static final sdk_version_name:I = 0x7f07108d

.field public static final sign_out_dialog_button_cancel:I = 0x7f070055

.field public static final sign_out_dialog_button_sign_out:I = 0x7f070056

.field public static final sign_out_dialog_checkbox:I = 0x7f070057

.field public static final sign_out_dialog_title:I = 0x7f070058

.field public static final user_tile_image_content_description:I = 0x7f070059

.field public static final webflow_header:I = 0x7f07005a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
