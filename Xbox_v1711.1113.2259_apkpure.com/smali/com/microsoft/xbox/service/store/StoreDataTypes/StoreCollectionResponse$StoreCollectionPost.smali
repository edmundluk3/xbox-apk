.class public Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;
.super Ljava/lang/Object;
.source "StoreCollectionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StoreCollectionPost"
.end annotation


# instance fields
.field private final beneficiaries:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Beneficiaries"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final checkSatisfyingEntitlements:Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CheckSatisfyingEntitlements"
    .end annotation
.end field

.field private final entitlementFilters:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "EntitlementFilters"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final market:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Market"
    .end annotation
.end field

.field private final productSkuIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductSkuIds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;",
            ">;"
        }
    .end annotation
.end field

.field public final showSatisfiedBy:Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ShowSatisfiedBy"
    .end annotation
.end field

.field public final validityType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ValidityType"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->beneficiaries:Ljava/util/List;

    .line 159
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->checkSatisfyingEntitlements:Z

    .line 160
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->showSatisfiedBy:Z

    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->market:Ljava/lang/String;

    .line 162
    const-string v1, "ValidAndFuture"

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->validityType:Ljava/lang/String;

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->entitlementFilters:Ljava/util/List;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->productSkuIds:Ljava/util/List;

    .line 165
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    .local v0, "productId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->productSkuIds:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    .end local v0    # "productId":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 172
    if-ne p1, p0, :cond_0

    .line 173
    const/4 v1, 0x1

    .line 179
    :goto_0
    return v1

    .line 174
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;

    if-nez v1, :cond_1

    .line 175
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 177
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;

    .line 179
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->productSkuIds:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->productSkuIds:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 185
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->hashCode:I

    if-nez v0, :cond_0

    .line 186
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->hashCode:I

    .line 187
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->productSkuIds:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->hashCode:I

    .line 189
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
