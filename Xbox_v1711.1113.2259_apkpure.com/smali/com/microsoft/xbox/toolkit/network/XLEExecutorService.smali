.class public final Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;
.super Ljava/lang/Object;
.source "XLEExecutorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;
    }
.end annotation


# static fields
.field public static final BI:Ljava/util/concurrent/ExecutorService;

.field public static final NATIVE:Ljava/util/concurrent/ExecutorService;

.field public static final NETWORK:Ljava/util/concurrent/ExecutorService;

.field public static final TEXTURE:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;

    const-string v1, "XLENativeOperationsPool"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;

    const-string v1, "XLENetworkOperationsPool"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;

    const-string v1, "XLETexturePool"

    invoke-direct {v0, v1, v4, v3}, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->TEXTURE:Ljava/util/concurrent/ExecutorService;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;

    const-string v1, "XLEPerfMarkerOperationsPool"

    invoke-direct {v0, v1, v4, v3}, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;-><init>(Ljava/lang/String;ILcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->BI:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
