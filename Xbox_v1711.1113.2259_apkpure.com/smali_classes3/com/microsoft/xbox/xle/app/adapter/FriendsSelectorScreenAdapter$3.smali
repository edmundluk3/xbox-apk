.class Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;
.super Ljava/lang/Object;
.source "FriendsSelectorScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 155
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$900(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 158
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 161
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 163
    .local v0, "isEmpty":Z
    if-nez v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$1002(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Z)Z

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->buildFilteredFriendList(Ljava/lang/String;)V

    .line 170
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$1002(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;Z)Z

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$3;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->onSearchBarClear()V

    goto :goto_0
.end method
