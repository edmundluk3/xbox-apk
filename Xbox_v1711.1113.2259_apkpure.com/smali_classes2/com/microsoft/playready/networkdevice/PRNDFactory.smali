.class public Lcom/microsoft/playready/networkdevice/PRNDFactory;
.super Ljava/lang/Object;
.source "PRNDFactory.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/IPRNDFactory;
.implements Lcom/microsoft/playready/IPlayReadyFactory;


# instance fields
.field private mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/microsoft/playready/PlayReadySuperFactory;->createFactory(Landroid/content/Context;)Lcom/microsoft/playready/IPlayReadyFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    .line 28
    return-void
.end method


# virtual methods
.method public createDomainHandler()Lcom/microsoft/playready/IDomainHandler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    invoke-interface {v0}, Lcom/microsoft/playready/IPlayReadyFactory;->createDomainHandler()Lcom/microsoft/playready/IDomainHandler;

    move-result-object v0

    return-object v0
.end method

.method public createLicenseAcquirer()Lcom/microsoft/playready/ILicenseAcquirer;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    invoke-interface {v0}, Lcom/microsoft/playready/IPlayReadyFactory;->createLicenseAcquirer()Lcom/microsoft/playready/ILicenseAcquirer;

    move-result-object v0

    return-object v0
.end method

.method public createMeteringReporter()Lcom/microsoft/playready/IMeteringReporter;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    invoke-interface {v0}, Lcom/microsoft/playready/IPlayReadyFactory;->createMeteringReporter()Lcom/microsoft/playready/IMeteringReporter;

    move-result-object v0

    return-object v0
.end method

.method public createPRActivator()Lcom/microsoft/playready/IPRActivator;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    invoke-interface {v0}, Lcom/microsoft/playready/IPlayReadyFactory;->createPRActivator()Lcom/microsoft/playready/IPRActivator;

    move-result-object v0

    return-object v0
.end method

.method public createPRMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/PRNDFactory;->mBaseFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    invoke-interface {v0}, Lcom/microsoft/playready/IPlayReadyFactory;->createPRMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method public createPRNDMediaPlayer(Lcom/microsoft/playready/networkdevice/IReceiverSession;)Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;
    .locals 1
    .param p1, "prndSession"    # Lcom/microsoft/playready/networkdevice/IReceiverSession;

    .prologue
    .line 68
    invoke-static {p1}, Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;->createPRNDMediaPlayer(Lcom/microsoft/playready/networkdevice/IReceiverSession;)Lcom/microsoft/playready/networkdevice/PRNDMediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method public createPRNDReceiver()Lcom/microsoft/playready/networkdevice/Receiver;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/microsoft/playready/networkdevice/Receiver;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/networkdevice/Receiver;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;)V

    return-object v0
.end method
