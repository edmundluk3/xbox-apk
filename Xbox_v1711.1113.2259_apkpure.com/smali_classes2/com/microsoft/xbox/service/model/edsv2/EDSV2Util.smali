.class public final Lcom/microsoft/xbox/service/model/edsv2/EDSV2Util;
.super Ljava/lang/Object;
.source "EDSV2Util.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type shouln\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static getLocalizedParentalRating(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "parentalRatings":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;>;"
    const/4 v1, 0x0

    .line 15
    .local v1, "s":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 16
    const/4 v2, 0x0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    .line 17
    .local v0, "rating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->Rating:Ljava/lang/String;

    .line 18
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->ShortName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 19
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v1, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->ShortName:Ljava/lang/String;

    .line 23
    .end local v0    # "rating":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    :cond_0
    return-object v1
.end method
