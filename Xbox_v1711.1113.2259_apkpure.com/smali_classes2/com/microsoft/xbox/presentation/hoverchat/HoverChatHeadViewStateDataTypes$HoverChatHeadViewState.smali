.class public abstract Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
.super Ljava/lang/Object;
.source "HoverChatHeadViewStateDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HoverChatHeadViewState"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .locals 6
    .param p0, "badgeViewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "isMaximized"    # Z
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "shape"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;"
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "imageUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "participants":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 107
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 108
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;

    .line 111
    invoke-static {p2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 113
    invoke-static {p4}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    move-object v1, p0

    move v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Lcom/google/common/collect/ImmutableList;)V

    .line 108
    return-object v0
.end method


# virtual methods
.method public abstract badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public equalsIgnoreIsMaximized(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract imageUrls()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isMaximized()Z
.end method

.method public abstract participants()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
