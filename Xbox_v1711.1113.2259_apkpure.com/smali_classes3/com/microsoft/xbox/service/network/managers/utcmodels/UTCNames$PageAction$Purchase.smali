.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Purchase;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Purchase"
.end annotation


# static fields
.field public static final App:Ljava/lang/String; = "Purchase - App"

.field public static final Consumable:Ljava/lang/String; = "Purchase - Consumable"

.field public static final Durable:Ljava/lang/String; = "Purchase - Durable"

.field public static final Game:Ljava/lang/String; = "Purchase - Game"

.field public static final InstallToXbox:Ljava/lang/String; = "Purchase - Install to Xbox"

.field public static final RedeemCode:Ljava/lang/String; = "Purchase - Redeem Code"

.field public static final Success:Ljava/lang/String; = "Purchase - Success"

.field public static final WebBlendCanceledLong:Ljava/lang/String; = "Purchase - WebBlend Canceled Long"

.field public static final WebBlendCanceledShort:Ljava/lang/String; = "Purchase - WebBlend Canceled Short"

.field public static final WebBlendLoaded:Ljava/lang/String; = "Purchase - WebBlend Loaded"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
