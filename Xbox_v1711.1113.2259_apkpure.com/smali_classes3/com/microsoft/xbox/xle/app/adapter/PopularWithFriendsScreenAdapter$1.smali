.class Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;
.super Ljava/lang/Object;
.source "PopularWithFriendsScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .line 50
    .local v0, "game":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->bingId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-result-object v1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/PopularGame;->bingId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->navigateToGameDetails(Ljava/lang/String;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    const-string v1, "PopularWithFriendsScreenAdapter"

    const-string v2, "No bingId for this game"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
