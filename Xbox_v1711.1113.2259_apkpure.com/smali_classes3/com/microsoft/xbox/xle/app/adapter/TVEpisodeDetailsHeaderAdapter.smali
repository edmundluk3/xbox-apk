.class public Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TVEpisodeDetailsHeaderAdapter.java"


# instance fields
.field private tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private tvEpisodeExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tvEpisodeReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tvEpisodeSeriesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private tvEpisodeTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private tvEpisodeTitleSmallTextView:Landroid/widget/TextView;

.field private tvEpisodeTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    .line 26
    const v0, 0x7f0e0b56

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->screenBody:Landroid/view/View;

    .line 28
    const v0, 0x7f0e0b57

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 29
    const v0, 0x7f0e04fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 30
    const v0, 0x7f0e0b59

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v0, 0x7f0e0b58

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e0b5b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e0b5a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeSeriesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e0b5c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTitleSmallTextView:Landroid/widget/TextView;

    .line 35
    return-void
.end method


# virtual methods
.method public onStop()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 44
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    const v2, 0x7f0201f7

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTitleView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTitleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getReleaseData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeSeriesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getSeriesTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeExtraTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getTVEpisodeExtraData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->tvEpisodeBackgroundImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v3, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 58
    :cond_0
    return-void
.end method
