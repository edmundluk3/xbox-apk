.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;
.super Landroid/os/AsyncTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegisterSLSTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

.field private final isLogin:Z

.field private lastXTokenString:Ljava/lang/String;

.field private systemId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

.field private userFlags:I


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;IZ)V
    .locals 0
    .param p2, "flags"    # I
    .param p3, "isLogin"    # Z

    .prologue
    .line 838
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 839
    iput p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->userFlags:I

    .line 840
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->isLogin:Z

    .line 841
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Registering with SLS, flags: 0x%x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v8, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->userFlags:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    iget v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->userFlags:I

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int v4, v0, v1

    .line 860
    .local v4, "flags":I
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isGCMRegistered()Z

    move-result v0

    if-nez v0, :cond_0

    .line 861
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RegisterSLSTask, registrationId is not ensured"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x4

    invoke-direct {v3, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    .line 877
    :goto_0
    return-object v0

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->systemId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 866
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1300(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->systemId:Ljava/lang/String;

    .line 869
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->systemId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->lastXTokenString:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->isLogin:Z

    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->enableNotifications(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v7

    .line 870
    .local v7, "endpointId":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 871
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    iget v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->userFlags:I

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0

    .line 873
    .end local v7    # "endpointId":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 874
    .local v6, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v0, v1, v2, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 831
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 882
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 883
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 884
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 885
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isSLSRegistered()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 886
    const-string v1, "endpoint.id"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->endpointId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 887
    const-string v1, "endpoint.flags"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 888
    const-string v1, "last.sls.registration.time.04"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 889
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Successfully registered with SLS"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 900
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1502(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 901
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 903
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 891
    .restart local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->isLogin:Z

    if-nez v1, :cond_2

    .line 895
    const-string v1, "user.flags"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 897
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to register with SLS"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 831
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 845
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 846
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    .line 847
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "system.id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->systemId:Ljava/lang/String;

    .line 848
    const-string v0, "https://xboxlive.com"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ESServiceManager;->getLastXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->lastXTokenString:Ljava/lang/String;

    .line 850
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 851
    return-void
.end method
