.class public Lcom/microsoft/xbox/xle/ui/XLEIconButton;
.super Landroid/widget/RelativeLayout;
.source "XLEIconButton.java"


# static fields
.field private static final VIEWID_ICON:I = 0x1
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field


# instance fields
.field private icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private iconDrawableDefault:Landroid/graphics/drawable/Drawable;

.field private iconDrawablePressed:Landroid/graphics/drawable/Drawable;

.field private label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    const v0, 0x7f01004a

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 58
    .local v2, "theme":Landroid/content/res/Resources$Theme;
    const/4 v3, 0x2

    new-array v3, v3, [[I

    sget-object v4, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEIconButton:[I

    aput-object v4, v3, v5

    sget-object v4, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateArrays([[I)[I

    move-result-object v1

    .line 60
    .local v1, "attrsToGet":[I
    invoke-virtual {v2, p2, v1, p3, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->initIcon(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->initLabel(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setClickable(Z)V

    .line 68
    new-instance v3, Lcom/microsoft/xbox/xle/ui/XLEIconButton$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/XLEIconButton$1;-><init>(Lcom/microsoft/xbox/xle/ui/XLEIconButton;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method private initIcon(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "a"    # Landroid/content/res/TypedArray;

    .prologue
    .line 83
    const/4 v13, 0x4

    const/high16 v14, 0x41000000    # 8.0f

    invoke-static {v14}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->SPtoPixels(F)I

    move-result v14

    int-to-float v14, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    .line 84
    .local v2, "textSize":F
    const/4 v13, 0x5

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 85
    .local v3, "color":I
    const/4 v13, 0x6

    const/4 v14, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    .line 86
    .local v11, "typefaceIndex":I
    const/16 v13, 0x8

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    .line 87
    .local v10, "styleIndex":I
    const/4 v13, 0x7

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 88
    .local v12, "typefaceSource":Ljava/lang/String;
    if-nez v12, :cond_2

    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$TypefaceXml;->typefaceFromIndex(I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-static {v13, v10}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v4

    .line 90
    .local v4, "typeface":Landroid/graphics/Typeface;
    :goto_0
    const/16 v13, 0xc

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 91
    .local v5, "eraseColor":I
    const/16 v13, 0xa

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    .line 92
    .local v6, "adjustForImageSize":Z
    const/4 v7, 0x0

    .line 93
    .local v7, "textAspectRatio":Ljava/lang/Float;
    const/16 v13, 0x9

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 94
    const/16 v13, 0x9

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    .line 96
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;-><init>(FILandroid/graphics/Typeface;IZLjava/lang/Float;)V

    .line 98
    .local v1, "textParams":Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;
    new-instance v8, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;

    new-instance v13, Lcom/microsoft/xbox/toolkit/ui/XLETextArg;

    invoke-direct {v13, v1}, Lcom/microsoft/xbox/toolkit/ui/XLETextArg;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;)V

    const/4 v14, 0x0

    invoke-direct {v8, v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLETextArg;Z)V

    .line 99
    .local v8, "imageParams":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;
    new-instance v13, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;)V

    iput-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 100
    const/16 v13, 0xb

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 101
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/16 v14, 0xb

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    :cond_1
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setDuplicateParentStateEnabled(Z)V

    .line 105
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setId(I)V

    .line 107
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget-object v14, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 108
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v13, -0x2

    const/4 v14, -0x2

    invoke-direct {v9, v13, v14}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 109
    .local v9, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v13, 0x9

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 110
    const/16 v13, 0xf

    invoke-virtual {v9, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 111
    const/high16 v13, 0x42100000    # 36.0f

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v13

    iput v13, v9, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 112
    const/high16 v13, 0x42000000    # 32.0f

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v13

    iput v13, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 113
    const/high16 v13, 0x40800000    # 4.0f

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v13

    iput v13, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 114
    const/high16 v13, 0x40800000    # 4.0f

    invoke-static {v13}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v13

    iput v13, v9, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 116
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v13, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    const/4 v13, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 119
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v14, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(Ljava/lang/String;)V

    .line 131
    :goto_1
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v13}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->addView(Landroid/view/View;)V

    .line 132
    return-void

    .line 88
    .end local v1    # "textParams":Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;
    .end local v4    # "typeface":Landroid/graphics/Typeface;
    .end local v5    # "eraseColor":I
    .end local v6    # "adjustForImageSize":Z
    .end local v7    # "textAspectRatio":Ljava/lang/Float;
    .end local v8    # "imageParams":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;
    .end local v9    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v12}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    goto/16 :goto_0

    .line 121
    .restart local v1    # "textParams":Lcom/microsoft/xbox/toolkit/ui/XLETextArg$Params;
    .restart local v4    # "typeface":Landroid/graphics/Typeface;
    .restart local v5    # "eraseColor":I
    .restart local v6    # "adjustForImageSize":Z
    .restart local v7    # "textAspectRatio":Ljava/lang/Float;
    .restart local v8    # "imageParams":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView$Params;
    .restart local v9    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    iput-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 122
    const-string v13, "Required xml attribute \'icon\' missing"

    iget-object v14, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawableDefault:Landroid/graphics/drawable/Drawable;

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 124
    const/4 v13, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    iput-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 125
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->isIconASelector()Z

    move-result v13

    if-nez v13, :cond_4

    .line 126
    const-string v13, "Required xml attribute \'iconPressed\' missing"

    iget-object v14, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawablePressed:Landroid/graphics/drawable/Drawable;

    invoke-static {v13, v14}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 129
    :cond_4
    iget-object v13, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v14, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawableDefault:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v14}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private initLabel(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x2

    .line 139
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setDuplicateParentStateEnabled(Z)V

    .line 142
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 143
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->icon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 144
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 145
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundColor(I)V

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->addView(Landroid/view/View;)V

    .line 151
    return-void
.end method

.method private isIconASelector()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->iconDrawableDefault:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/StateListDrawable;

    return v0
.end method


# virtual methods
.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->label:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :cond_0
    return-void
.end method
