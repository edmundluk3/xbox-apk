.class public Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AlbumDetailsScreenFromTrackViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 209
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;

    .line 210
    .local v2, "trackModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 211
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 213
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbum()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 214
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    .line 215
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 218
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;)V

    .line 186
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;)V

    .line 199
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->isLoading:Z

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->localAlbumModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;->updateAdapter()V

    .line 193
    return-void
.end method
