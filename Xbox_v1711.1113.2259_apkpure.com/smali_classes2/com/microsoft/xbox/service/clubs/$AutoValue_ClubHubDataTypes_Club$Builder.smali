.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
.source "$AutoValue_ClubHubDataTypes_Club.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private bannerImageUrl:Ljava/lang/String;

.field private clubPresence:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation
.end field

.field private clubPresenceCount:Ljava/lang/Long;

.field private clubPresenceInGameCount:Ljava/lang/Long;

.field private clubPresenceTodayCount:Ljava/lang/Long;

.field private clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

.field private creationDateUtc:Ljava/lang/String;

.field private followersCount:Ljava/lang/Long;

.field private founderXuid:Ljava/lang/Long;

.field private glyphImageUrl:Ljava/lang/String;

.field private id:Ljava/lang/Long;

.field private maxMembersInGame:Ljava/lang/Long;

.field private maxMembersPerClub:Ljava/lang/Long;

.field private membersCount:Ljava/lang/Long;

.field private moderatorsCount:Ljava/lang/Long;

.field private ownerXuid:Ljava/lang/String;

.field private profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

.field private recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

.field private recommendedCount:Ljava/lang/Long;

.field private reportCount:Ljava/lang/Long;

.field private reportedItemsCount:Ljava/lang/Long;

.field private requestedToJoinCount:Ljava/lang/Long;

.field private roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

.field private settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

.field private shortName:Ljava/lang/String;

.field private state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

.field private suspendedUntilUtc:Ljava/util/Date;

.field private targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

.field private titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;-><init>()V

    .line 446
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 2
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 447
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;-><init>()V

    .line 448
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->id:Ljava/lang/Long;

    .line 449
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 450
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 451
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->shortName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->shortName:Ljava/lang/String;

    .line 452
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->ownerXuid:Ljava/lang/String;

    .line 453
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->founderXuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->founderXuid:Ljava/lang/Long;

    .line 454
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->creationDateUtc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->creationDateUtc:Ljava/lang/String;

    .line 455
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->glyphImageUrl:Ljava/lang/String;

    .line 456
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->bannerImageUrl:Ljava/lang/String;

    .line 457
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 458
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->followersCount:Ljava/lang/Long;

    .line 459
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->membersCount:Ljava/lang/Long;

    .line 460
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->moderatorsCount:Ljava/lang/Long;

    .line 461
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendedCount:Ljava/lang/Long;

    .line 462
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->requestedToJoinCount:Ljava/lang/Long;

    .line 463
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceCount:Ljava/lang/Long;

    .line 464
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceTodayCount:Ljava/lang/Long;

    .line 465
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceInGameCount:Ljava/lang/Long;

    .line 466
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 467
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 468
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 469
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresence:Lcom/google/common/collect/ImmutableList;

    .line 470
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 471
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 472
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->suspendedUntilUtc:Ljava/util/Date;

    .line 473
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportCount:Ljava/lang/Long;

    .line 474
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportedItemsCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportedItemsCount:Ljava/lang/Long;

    .line 475
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersPerClub()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersPerClub:Ljava/lang/Long;

    .line 476
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersInGame()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersInGame:Ljava/lang/Long;

    .line 477
    return-void
.end method


# virtual methods
.method public bannerImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "bannerImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 520
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->bannerImageUrl:Ljava/lang/String;

    .line 521
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 47

    .prologue
    .line 625
    const-string v2, ""

    .line 626
    .local v2, "missing":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->id:Ljava/lang/Long;

    if-nez v3, :cond_0

    .line 627
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 629
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->founderXuid:Ljava/lang/Long;

    if-nez v3, :cond_1

    .line 630
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " founderXuid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 632
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->creationDateUtc:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 633
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " creationDateUtc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 635
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->followersCount:Ljava/lang/Long;

    if-nez v3, :cond_3

    .line 636
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " followersCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 638
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->membersCount:Ljava/lang/Long;

    if-nez v3, :cond_4

    .line 639
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " membersCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 641
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->moderatorsCount:Ljava/lang/Long;

    if-nez v3, :cond_5

    .line 642
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " moderatorsCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 644
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendedCount:Ljava/lang/Long;

    if-nez v3, :cond_6

    .line 645
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " recommendedCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 647
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->requestedToJoinCount:Ljava/lang/Long;

    if-nez v3, :cond_7

    .line 648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requestedToJoinCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 650
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceCount:Ljava/lang/Long;

    if-nez v3, :cond_8

    .line 651
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " clubPresenceCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 653
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceTodayCount:Ljava/lang/Long;

    if-nez v3, :cond_9

    .line 654
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " clubPresenceTodayCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 656
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceInGameCount:Ljava/lang/Long;

    if-nez v3, :cond_a

    .line 657
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " clubPresenceInGameCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 659
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-nez v3, :cond_b

    .line 660
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 662
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportCount:Ljava/lang/Long;

    if-nez v3, :cond_c

    .line 663
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " reportCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 665
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportedItemsCount:Ljava/lang/Long;

    if-nez v3, :cond_d

    .line 666
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " reportedItemsCount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 668
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersPerClub:Ljava/lang/Long;

    if-nez v3, :cond_e

    .line 669
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " maxMembersPerClub"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 671
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersInGame:Ljava/lang/Long;

    if-nez v3, :cond_f

    .line 672
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " maxMembersInGame"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 674
    :cond_f
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_10

    .line 675
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Missing required properties:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 677
    :cond_10
    new-instance v3, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->id:Ljava/lang/Long;

    .line 678
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->shortName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->ownerXuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->founderXuid:Ljava/lang/Long;

    .line 683
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->creationDateUtc:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->glyphImageUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->bannerImageUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->followersCount:Ljava/lang/Long;

    move-object/from16 v16, v0

    .line 688
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->membersCount:Ljava/lang/Long;

    move-object/from16 v18, v0

    .line 689
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->moderatorsCount:Ljava/lang/Long;

    move-object/from16 v20, v0

    .line 690
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendedCount:Ljava/lang/Long;

    move-object/from16 v22, v0

    .line 691
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->requestedToJoinCount:Ljava/lang/Long;

    move-object/from16 v24, v0

    .line 692
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceCount:Ljava/lang/Long;

    move-object/from16 v26, v0

    .line 693
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceTodayCount:Ljava/lang/Long;

    move-object/from16 v28, v0

    .line 694
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceInGameCount:Ljava/lang/Long;

    move-object/from16 v30, v0

    .line 695
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresence:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->suspendedUntilUtc:Ljava/util/Date;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportCount:Ljava/lang/Long;

    move-object/from16 v39, v0

    .line 703
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportedItemsCount:Ljava/lang/Long;

    move-object/from16 v41, v0

    .line 704
    invoke-virtual/range {v41 .. v41}, Ljava/lang/Long;->longValue()J

    move-result-wide v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersPerClub:Ljava/lang/Long;

    move-object/from16 v43, v0

    .line 705
    invoke-virtual/range {v43 .. v43}, Ljava/lang/Long;->longValue()J

    move-result-wide v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersInGame:Ljava/lang/Long;

    move-object/from16 v45, v0

    .line 706
    invoke-virtual/range {v45 .. v45}, Ljava/lang/Long;->longValue()J

    move-result-wide v45

    invoke-direct/range {v3 .. v46}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;-><init>(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;JJJJJJJJLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;Ljava/util/Date;JJJJ)V

    .line 677
    return-object v3
.end method

.method public clubPresence(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;"
        }
    .end annotation

    .prologue
    .line 585
    .local p1, "clubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresence:Lcom/google/common/collect/ImmutableList;

    .line 586
    return-object p0

    .line 585
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public clubPresenceCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "clubPresenceCount"    # J

    .prologue
    .line 555
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceCount:Ljava/lang/Long;

    .line 556
    return-object p0
.end method

.method public clubPresenceInGameCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "clubPresenceInGameCount"    # J

    .prologue
    .line 565
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceInGameCount:Ljava/lang/Long;

    .line 566
    return-object p0
.end method

.method public clubPresenceTodayCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "clubPresenceTodayCount"    # J

    .prologue
    .line 560
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubPresenceTodayCount:Ljava/lang/Long;

    .line 561
    return-object p0
.end method

.method public clubType(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 490
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 491
    return-object p0
.end method

.method public creationDateUtc(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "creationDateUtc"    # Ljava/lang/String;

    .prologue
    .line 510
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->creationDateUtc:Ljava/lang/String;

    .line 511
    return-object p0
.end method

.method public followersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "followersCount"    # J

    .prologue
    .line 530
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->followersCount:Ljava/lang/Long;

    .line 531
    return-object p0
.end method

.method public founderXuid(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "founderXuid"    # J

    .prologue
    .line 505
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->founderXuid:Ljava/lang/Long;

    .line 506
    return-object p0
.end method

.method public glyphImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "glyphImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 515
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->glyphImageUrl:Ljava/lang/String;

    .line 516
    return-object p0
.end method

.method public id(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 480
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->id:Ljava/lang/Long;

    .line 481
    return-object p0
.end method

.method public maxMembersInGame(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "maxMembersInGame"    # J

    .prologue
    .line 620
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersInGame:Ljava/lang/Long;

    .line 621
    return-object p0
.end method

.method public maxMembersPerClub(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "maxMembersPerClub"    # J

    .prologue
    .line 615
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->maxMembersPerClub:Ljava/lang/Long;

    .line 616
    return-object p0
.end method

.method public membersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "membersCount"    # J

    .prologue
    .line 535
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->membersCount:Ljava/lang/Long;

    .line 536
    return-object p0
.end method

.method public moderatorsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "moderatorsCount"    # J

    .prologue
    .line 540
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->moderatorsCount:Ljava/lang/Long;

    .line 541
    return-object p0
.end method

.method public ownerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "ownerXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 500
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->ownerXuid:Ljava/lang/String;

    .line 501
    return-object p0
.end method

.method public profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 485
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 486
    return-object p0
.end method

.method public recommendation(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "recommendation"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 581
    return-object p0
.end method

.method public recommendedCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "recommendedCount"    # J

    .prologue
    .line 545
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->recommendedCount:Ljava/lang/Long;

    .line 546
    return-object p0
.end method

.method public reportCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "reportCount"    # J

    .prologue
    .line 605
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportCount:Ljava/lang/Long;

    .line 606
    return-object p0
.end method

.method public reportedItemsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "reportedItemsCount"    # J

    .prologue
    .line 610
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->reportedItemsCount:Ljava/lang/Long;

    .line 611
    return-object p0
.end method

.method public requestedToJoinCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1
    .param p1, "requestedToJoinCount"    # J

    .prologue
    .line 550
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->requestedToJoinCount:Ljava/lang/Long;

    .line 551
    return-object p0
.end method

.method public roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 570
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 571
    return-object p0
.end method

.method public settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "settings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 525
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 526
    return-object p0
.end method

.method public shortName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "shortName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 495
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->shortName:Ljava/lang/String;

    .line 496
    return-object p0
.end method

.method public state(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .prologue
    .line 595
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 596
    return-object p0
.end method

.method public suspendedUntilUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "suspendedUntilUtc"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 600
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->suspendedUntilUtc:Ljava/util/Date;

    .line 601
    return-object p0
.end method

.method public targetRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "targetRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 575
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 576
    return-object p0
.end method

.method public titleDeeplinks(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 0
    .param p1, "titleDeeplinks"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 591
    return-object p0
.end method
