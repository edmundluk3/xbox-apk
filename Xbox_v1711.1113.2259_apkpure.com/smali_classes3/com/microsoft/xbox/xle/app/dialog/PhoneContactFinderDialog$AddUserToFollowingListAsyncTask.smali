.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PhoneContactFinderDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddUserToFollowingListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private followingUserXuids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p2, "followingUserXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 327
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->followingUserXuids:Ljava/util/ArrayList;

    .line 328
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 332
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 333
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 359
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 360
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    .line 361
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->forceLoad:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->followingUserXuids:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->addUserToFollowingList(ZLjava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 363
    .local v2, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 364
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAddUserToFollowingResult()Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    move-result-object v1

    .line 366
    .local v1, "response":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->getAddFollowingRequestStatus()Z

    move-result v3

    if-nez v3, :cond_1

    iget v3, v1, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->code:I

    const/16 v4, 0x404

    if-ne v3, v4, :cond_1

    .line 367
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 375
    .end local v1    # "response":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .end local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    :goto_0
    return-object v2

    .line 369
    .restart local v1    # "response":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .restart local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadProfileSummary(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    goto :goto_0

    .line 375
    .end local v1    # "response":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .end local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 354
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 338
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 340
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 350
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 323
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 344
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 345
    return-void
.end method
