.class public Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "TrendingTagsModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrendingTagsModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;",
        ">;"
    }
.end annotation


# instance fields
.field private final scid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

.field private trendingTags:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "scid"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->this$0:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 72
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 73
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->scid:Ljava/lang/String;

    .line 74
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->scid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->onGetTrendingTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGetTrendingTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->trendingTags:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    .line 92
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->access$100(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 93
    return-void
.end method


# virtual methods
.method public getTrendingTags()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->trendingTags:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    return-object v0
.end method

.method public loadTrendingTagsAsync(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetTrendingTags:Lcom/microsoft/xbox/service/model/UpdateType;

    new-instance v1, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;-><init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$1;)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 78
    return-void
.end method
