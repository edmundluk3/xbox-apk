.class public Lcom/microsoft/xbox/xle/ui/EPGView;
.super Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.source "EPGView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;
.implements Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;,
        Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;,
        Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;
    }
.end annotation


# static fields
.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "EPGView"

.field private static final UNDEFINED_CHANNEL_INDEX:I = -0x1

.field private static final UNDEFINED_ROW_SELECTION:I = -0x1


# instance fields
.field private mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

.field private mDelayedClearOnRefreshViewPort:Z

.field private mDelayedRefreshViewPort:Z

.field private mFillingInMissingProgramViews:Z

.field private mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

.field private final mOffsetDate:Ljava/util/Date;

.field private mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

.field private mPixelsInSecond:F

.field private final mProgramViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

.field private mRestoreState:Z

.field private final mRestoreStateRunner:Ljava/lang/Runnable;

.field private mScreen:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

.field private mScreenWidthInSeconds:F

.field private mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

.field private mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

.field private mSelectedDetailsView:Landroid/view/View;

.field private mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

.field private mStartDate:Ljava/util/Date;

.field private final mStartDateAutoUpdate:Ljava/lang/Runnable;

.field private mTimeLineIntervalSeconds:I

.field private mTimeLineIntervalWidth:I

.field private final mTimeViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOffsetDate:Ljava/util/Date;

    .line 86
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mProgramViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    .line 98
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreState:Z

    .line 274
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDateAutoUpdate:Ljava/lang/Runnable;

    .line 428
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$5;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreStateRunner:Ljava/lang/Runnable;

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->initialize()V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOffsetDate:Ljava/util/Date;

    .line 86
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mProgramViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    .line 98
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreState:Z

    .line 274
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDateAutoUpdate:Ljava/lang/Runnable;

    .line 428
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$5;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreStateRunner:Ljava/lang/Runnable;

    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->initialize()V

    .line 124
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/ui/EPGView;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreenWidthInSeconds:F

    return v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/ui/EPGView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalWidth:I

    return v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/ui/EPGView;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/ui/EPGView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    return v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/ui/EPGView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->activateContinuousScroll(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/EPGView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->autoSelectProgramView()V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/ui/EPGView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDelayedRefreshViewPort:Z

    return p1
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/ui/EPGView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDelayedClearOnRefreshViewPort:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/ui/EPGView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mFillingInMissingProgramViews:Z

    return v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/ui/EPGView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mFillingInMissingProgramViews:Z

    return p1
.end method

.method static synthetic access$701(Lcom/microsoft/xbox/xle/ui/EPGView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->refreshViewPort(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/ui/EPGView;J)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p1, "x1"    # J

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertTimeToPixels(J)I

    move-result v0

    return v0
.end method

.method private activateContinuousScroll(I)Z
    .locals 5
    .param p1, "dpsPerFrame"    # I

    .prologue
    const/4 v3, 0x0

    .line 1175
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getExtraRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v1

    .line 1176
    .local v1, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    if-nez v1, :cond_0

    .line 1187
    :goto_0
    return v3

    .line 1179
    :cond_0
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v2

    .line 1180
    .local v2, "scroller":Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    if-nez p1, :cond_1

    .line 1181
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setAutoScroll(I)V

    .line 1187
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 1183
    :cond_1
    int-to-float v3, p1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v0, v3

    .line 1184
    .local v0, "pixels":I
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setAutoScroll(I)V

    goto :goto_1
.end method

.method private alignDate(Ljava/util/Date;)Ljava/util/Date;
    .locals 6
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 638
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v0, v2

    .line 639
    .local v0, "interval":J
    new-instance v2, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    div-long/2addr v4, v0

    mul-long/2addr v4, v0

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    return-object v2
.end method

.method private autoSelectProgramView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 537
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getHorizontalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-eq v2, v3, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v2, :cond_0

    .line 543
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->LiveTV:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    if-ne v2, v3, :cond_0

    .line 546
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->getChannelIndexIfVisible(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)I

    move-result v1

    .line 547
    .local v1, "row":I
    if-ltz v1, :cond_0

    .line 550
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->getProgramView(ILcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v0

    .line 551
    .local v0, "pv":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    if-nez v0, :cond_0

    .line 555
    invoke-direct {p0, v1, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->getProgramView(II)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_0

    .line 557
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v3

    invoke-direct {p0, v2, v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V

    goto :goto_0
.end method

.method private convertPixelsToTime(I)J
    .locals 4
    .param p1, "pixels"    # I

    .prologue
    .line 655
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertPixelsToSeconds(I)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private convertTimeToPixels(J)I
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 659
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/16 v4, 0x3e8

    div-long v0, v2, v4

    .line 660
    .local v0, "seconds":J
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertSecondsToPixels(J)I

    move-result v2

    return v2
.end method

.method private convertTimeToPixels(Ljava/util/Date;)I
    .locals 2
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 664
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertTimeToPixels(J)I

    move-result v0

    return v0
.end method

.method private getChannelIndexIfVisible(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)I
    .locals 2
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 493
    .local v0, "row":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 494
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    .end local v0    # "row":I
    :goto_1
    return v0

    .line 497
    .restart local v0    # "row":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 500
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getProgramView(II)Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    .locals 4
    .param p1, "row"    # I
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 505
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVirtualGridRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v0

    .line 506
    .local v0, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    if-nez v0, :cond_0

    move-object v1, v2

    .line 513
    :goto_0
    return-object v1

    .line 509
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 510
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_1

    instance-of v3, v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    if-eqz v3, :cond_1

    .line 511
    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 513
    goto :goto_0
.end method

.method private getProgramView(ILcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    .locals 6
    .param p1, "row"    # I
    .param p2, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    const/4 v4, 0x0

    .line 518
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVirtualGridRow(I)Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v2

    .line 519
    .local v2, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    if-nez v2, :cond_1

    move-object v1, v4

    .line 530
    :cond_0
    :goto_0
    return-object v1

    .line 522
    :cond_1
    const/4 v0, 0x0

    .line 523
    .local v0, "index":I
    :goto_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 524
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_2

    move-object v1, v4

    .line 525
    goto :goto_0

    .line 527
    :cond_2
    instance-of v5, v3, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    if-eqz v5, :cond_3

    move-object v1, v3

    .line 528
    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .line 529
    .local v1, "pv":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 522
    .end local v1    # "pv":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
    .locals 1
    .param p0, "channel1"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p1, "channel2"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 476
    if-ne p0, p1, :cond_0

    .line 477
    const/4 v0, 0x1

    .line 482
    :goto_0
    return v0

    .line 479
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 480
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 482
    :cond_2
    invoke-interface {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z
    .locals 2
    .param p0, "program1"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p1, "program2"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    .line 465
    if-ne p0, p1, :cond_0

    .line 466
    const/4 v0, 0x1

    .line 471
    :goto_0
    return v0

    .line 468
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 469
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 471
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private scheduleAutoUpdate()V
    .locals 8

    .prologue
    .line 285
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDateAutoUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 288
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long v2, v4, v6

    .line 289
    .local v2, "updateTime":J
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 290
    .local v0, "delay":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDateAutoUpdate:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-virtual {p0, v4, v6, v7}, Lcom/microsoft/xbox/xle/ui/EPGView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 291
    return-void
.end method

.method private setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V
    .locals 11
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p2, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p3, "animate"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 562
    if-eqz p1, :cond_0

    .line 563
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "ShowExpanded"

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getChannelGuid()Ljava/lang/String;

    move-result-object v7

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getChannelIndexIfVisible(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)I

    move-result v1

    .line 568
    .local v1, "oldRow":I
    move v0, v1

    .line 570
    .local v0, "newRow":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-static {p1, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 571
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .line 572
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getChannelIndexIfVisible(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)I

    move-result v0

    .line 575
    iput-object v10, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedDetailsView:Landroid/view/View;

    .line 578
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-static {p2, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 581
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getProgramView(ILcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v2

    .line 582
    .local v2, "selectedView":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    if-eqz v2, :cond_2

    .line 583
    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setSelected(Z)V

    .line 585
    :cond_2
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 587
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getProgramView(ILcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v2

    .line 588
    if-eqz v2, :cond_3

    .line 589
    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setSelected(Z)V

    .line 592
    :cond_3
    iput-object v10, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedDetailsView:Landroid/view/View;

    .line 596
    .end local v2    # "selectedView":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    :cond_4
    if-ne v1, v0, :cond_6

    .line 598
    invoke-virtual {p0, v1, v8}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateBottomView(IZ)Z

    .line 610
    :goto_1
    return-void

    .line 563
    .end local v0    # "newRow":I
    .end local v1    # "oldRow":I
    :cond_5
    const-string v3, ""

    goto :goto_0

    .line 603
    .restart local v0    # "newRow":I
    .restart local v1    # "oldRow":I
    :cond_6
    invoke-direct {p0, v1, v9}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateRowState(IZ)V

    .line 604
    invoke-direct {p0, v0, v8}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateRowState(IZ)V

    .line 607
    invoke-virtual {p0, v0, p3}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateBottomView(IZ)Z

    .line 608
    invoke-virtual {p0, v1, p3}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateBottomView(IZ)Z

    goto :goto_1
.end method

.method public static setSkippedFrameWarningLimit(I)Z
    .locals 7
    .param p0, "frames"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1156
    if-ge p0, v4, :cond_0

    .line 1169
    :goto_0
    return v3

    .line 1160
    :cond_0
    :try_start_0
    const-string v5, "android.view.Choreographer"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1162
    .local v0, "classObject":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "SKIPPED_FRAME_WARNING_LIMIT"

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 1163
    .local v2, "field":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1164
    const/4 v5, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 1166
    goto :goto_0

    .line 1167
    .end local v0    # "classObject":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 1168
    .local v1, "error":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private updateDayView()V
    .locals 6

    .prologue
    .line 630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOffsetDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->setDate(Ljava/util/Date;)V

    .line 632
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOffsetDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->showNow(Z)V

    .line 634
    :cond_0
    return-void

    .line 632
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateRowState(IZ)V
    .locals 3
    .param p1, "row"    # I
    .param p2, "selected"    # Z

    .prologue
    .line 614
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getRowView(I)Landroid/view/View;

    move-result-object v1

    .line 615
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 616
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .line 617
    .local v0, "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->getTitleView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 618
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->getTitleView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/view/View;->setSelected(Z)V

    .line 619
    if-eqz p2, :cond_1

    .end local p1    # "row":I
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->saveSelectedRow(I)V

    .line 622
    .end local v0    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    :cond_0
    return-void

    .line 619
    .restart local v0    # "rv":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    .restart local p1    # "row":I
    :cond_1
    const/4 p1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addVerticalScrollAccelerator()V
    .locals 4

    .prologue
    .line 174
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    if-eqz v2, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListFrame()Landroid/widget/RelativeLayout;

    move-result-object v0

    .line 178
    .local v0, "frame":Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .line 179
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->setListener(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;)V

    .line 188
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getHeaderView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 192
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 193
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getHeaderView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method public convertPixelsToSeconds(I)I
    .locals 2
    .param p1, "pixels"    # I

    .prologue
    .line 647
    int-to-float v0, p1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mPixelsInSecond:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public convertSecondsToPixels(J)I
    .locals 3
    .param p1, "seconds"    # J

    .prologue
    .line 651
    long-to-float v0, p1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mPixelsInSecond:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public createDayView(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 625
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    .line 626
    return-void
.end method

.method public dataArrived(IIZ)V
    .locals 5
    .param p1, "startChannel"    # I
    .param p2, "endChannel"    # I
    .param p3, "refresh"    # Z

    .prologue
    .line 1130
    const-string v2, "EPGView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getScreenString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "dataArrived"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1134
    .local v0, "firstViewPortRow":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mListView:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->getLastVisiblePosition()I

    move-result v1

    .line 1136
    .local v1, "lastViewPortRow":I
    if-lt v1, p1, :cond_0

    if-ge v0, p2, :cond_0

    .line 1137
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/ui/EPGView;->refreshViewPort(Z)V

    .line 1139
    :cond_0
    return-void
.end method

.method public getBottomView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "row"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 799
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isChannelSelected(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 800
    const/4 v1, 0x0

    .line 809
    :goto_0
    return-object v1

    .line 802
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedDetailsView:Landroid/view/View;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    if-eqz v1, :cond_1

    .line 803
    invoke-static {p2}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    move-result-object v0

    .line 804
    .local v0, "view":Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    .line 806
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedDetailsView:Landroid/view/View;

    .line 809
    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedDetailsView:Landroid/view/View;

    goto :goto_0
.end method

.method protected getChannelType(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderRowCount()I
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getLiveTvChannelCount()I

    move-result v0

    .line 684
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeightSpec(I)I
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 690
    if-gez p1, :cond_0

    const v0, 0x7f0902c7

    .line 691
    .local v0, "id":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    .line 690
    .end local v0    # "id":I
    :cond_0
    const v0, 0x7f090291

    goto :goto_0
.end method

.method protected getRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1107
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->getRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1108
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1109
    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .line 1110
    .local v0, "vgbr":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mPixelsInSecond:F

    const/high16 v3, 0x43e10000    # 450.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->setXTweenPosition(I)V

    .line 1113
    .end local v0    # "vgbr":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;
    :cond_0
    return-object v1
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannelCount()I

    move-result v0

    .line 676
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getRowType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1101
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->getChannelType(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method protected getRowTypeCount()I
    .locals 1

    .prologue
    .line 1096
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->Count:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method public getScreen()Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreen:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    return-object v0
.end method

.method public getScreenString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreen:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "row"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 758
    if-gez p1, :cond_0

    .line 759
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateDayView()V

    .line 760
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    .line 792
    :goto_0
    return-object v3

    .line 763
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v5, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v0

    .line 765
    .local v0, "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    if-eqz p2, :cond_1

    instance-of v5, p2, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    if-nez v5, :cond_5

    .line 766
    :cond_1
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v5

    invoke-static {p3, v5}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getInstance(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;)Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    move-result-object v3

    .line 771
    .local v3, "view":Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-static {v5, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setSelected(Z)V

    .line 775
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    if-eqz v5, :cond_3

    .line 777
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->getWidth()I

    move-result v5

    if-nez v5, :cond_2

    .line 778
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateDayView()V

    .line 782
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->getWidth()I

    move-result v5

    if-eqz v5, :cond_3

    .line 783
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 784
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDayView:Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGHeaderDayView;->getWidth()I

    move-result v5

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 785
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 789
    .end local v1    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    sget-boolean v5, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->SHOW_CHANNEL_LOGO_ON_VERTICAL_FLING:Z

    if-nez v5, :cond_4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVerticalScrollState()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v5

    if-nez v5, :cond_6

    :cond_4
    const/4 v2, 0x1

    .line 790
    .local v2, "showLogo":Z
    :goto_2
    invoke-virtual {v3, v0, v2, v4}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->setModel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;ZZ)V

    goto :goto_0

    .end local v2    # "showLogo":Z
    .end local v3    # "view":Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    :cond_5
    move-object v3, p2

    .line 768
    check-cast v3, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    .restart local v3    # "view":Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;
    goto :goto_1

    :cond_6
    move v2, v4

    .line 789
    goto :goto_2
.end method

.method public getViewIterator(IIZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    .locals 6
    .param p1, "row"    # I
    .param p2, "pos"    # I
    .param p3, "forward"    # Z

    .prologue
    .line 815
    if-gez p1, :cond_0

    .line 816
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    invoke-virtual {v0, p2, p3}, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->reset(IZ)V

    .line 817
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;

    .line 820
    :goto_0
    return-object v0

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mProgramViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertPixelsToTime(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, p1, v2, v3, p3}, Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;->reset(IJZ)V

    .line 820
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mProgramViewIterator:Lcom/microsoft/xbox/xle/ui/EPGView$ProgramViewIterator;

    goto :goto_0
.end method

.method protected initialize()V
    .locals 2

    .prologue
    .line 135
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->getSharedInstance()Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    move-result-object v0

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->setSharedInstance(Lcom/microsoft/xbox/xle/ui/EPGViewConfig;)V

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mPixelsInSecond:F

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mPixelsInSecond:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreenWidthInSeconds:F

    .line 140
    const/16 v0, 0x708

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setTimeLineInterval(I)V

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setBackgroundColor(I)V

    .line 145
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 147
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setListener(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IListener;)V

    .line 148
    return-void
.end method

.method public isChannelSelected(I)Z
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannelCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadSelectedRow()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 727
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;

    if-eqz v1, :cond_1

    .line 728
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getTVSelectedChannel(Ljava/lang/String;)I

    move-result v0

    .line 733
    :cond_0
    :goto_0
    return v0

    .line 729
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;

    if-eqz v1, :cond_2

    .line 730
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getFavoritesSelectedChannel(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 732
    :cond_2
    const-string v1, "EPGView"

    const-string v2, "loadSelectedRow(): Could not determine model type, view in bad state"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadStartingRow()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 696
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;

    if-eqz v1, :cond_1

    .line 697
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getTVLastChannel(Ljava/lang/String;)I

    move-result v0

    .line 702
    :cond_0
    :goto_0
    return v0

    .line 698
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;

    if-eqz v1, :cond_2

    .line 699
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getFavoritesLastChannel(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 701
    :cond_2
    const-string v1, "EPGView"

    const-string v2, "loadStartingRow(): Could not determine model type, view in bad state"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public modelChanged()V
    .locals 0

    .prologue
    .line 1125
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->refreshAll()V

    .line 1126
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 302
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onAttachedToWindow()V

    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_0

    .line 306
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->setStartDate(Ljava/util/Date;Z)V

    .line 307
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->scheduleAutoUpdate()V

    .line 309
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 313
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onDetachedFromWindow()V

    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDateAutoUpdate:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 317
    return-void
.end method

.method protected onHorizontalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 1
    .param p1, "oldState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .param p2, "newState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    .line 410
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onHorizontalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    .line 413
    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->IDLE:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-ne p2, v0, :cond_0

    .line 415
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDelayedRefreshViewPort:Z

    if-eqz v0, :cond_1

    .line 417
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mDelayedClearOnRefreshViewPort:Z

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->refreshViewPort(Z)V

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->autoSelectProgramView()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 321
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 322
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 329
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 325
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onItemClick(ILandroid/view/View;)V
    .locals 13
    .param p1, "row"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v12, 0x1

    const/4 v7, 0x0

    .line 1023
    if-eqz p2, :cond_0

    if-gez p1, :cond_1

    .line 1076
    .end local p2    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1027
    .restart local p2    # "view":Landroid/view/View;
    :cond_1
    instance-of v6, p2, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    if-eqz v6, :cond_7

    .line 1036
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v6, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v0

    .line 1039
    .local v0, "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    instance-of v6, v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;

    if-eqz v6, :cond_2

    .line 1040
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    invoke-interface {v6, v0}, Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;->onChannelTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1044
    :cond_2
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isChannelSelected(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1045
    invoke-direct {p0, v7, v7, v12}, Lcom/microsoft/xbox/xle/ui/EPGView;->setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V

    goto :goto_0

    .line 1048
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v2, v8, v10

    .line 1049
    .local v2, "currentTimeSeconds":J
    const/4 v5, 0x0

    .line 1050
    .local v5, "pv":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    const/4 v4, 0x0

    .line 1052
    .local v4, "programIndex":I
    :cond_4
    invoke-direct {p0, p1, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->getProgramView(II)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v5

    .line 1053
    add-int/lit8 v4, v4, 0x1

    .line 1055
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v6

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getDuration()I

    move-result v8

    add-int/2addr v6, v8

    int-to-long v8, v6

    cmp-long v6, v8, v2

    if-ltz v6, :cond_4

    .line 1057
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v6

    :goto_1
    invoke-direct {p0, v0, v6, v12}, Lcom/microsoft/xbox/xle/ui/EPGView;->setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V

    goto :goto_0

    :cond_6
    move-object v6, v7

    goto :goto_1

    .line 1059
    .end local v0    # "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .end local v2    # "currentTimeSeconds":J
    .end local v4    # "programIndex":I
    .end local v5    # "pv":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    :cond_7
    instance-of v6, p2, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    if-eqz v6, :cond_0

    .line 1061
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v6, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v0

    .line 1064
    .restart local v0    # "channel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    if-eqz v6, :cond_8

    .line 1065
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    move-object v6, p2

    check-cast v6, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v6

    invoke-interface {v8, v0, v6}, Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;->onProgramTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1069
    :cond_8
    check-cast p2, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .end local p2    # "view":Landroid/view/View;
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v1

    .line 1070
    .local v1, "program":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mSelectedProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-static {v6, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1071
    invoke-direct {p0, v7, v7, v12}, Lcom/microsoft/xbox/xle/ui/EPGView;->setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V

    goto/16 :goto_0

    .line 1073
    :cond_9
    invoke-direct {p0, v0, v1, v12}, Lcom/microsoft/xbox/xle/ui/EPGView;->setSelectedChannelAndProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Z)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 447
    invoke-super/range {p0 .. p5}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onLayout(ZIIII)V

    .line 450
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreState:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreState:Z

    .line 457
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mRestoreStateRunner:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onVerticalScroll(III)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "visible"    # I
    .param p3, "total"    # I

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    if-eqz v1, :cond_0

    .line 1082
    sub-int v1, p3, p2

    if-nez v1, :cond_1

    .line 1083
    const/4 v0, 0x0

    .line 1087
    .local v0, "scrollPercentage":F
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->setSelectedPosition(F)V

    .line 1089
    .end local v0    # "scrollPercentage":F
    :cond_0
    return-void

    .line 1085
    :cond_1
    int-to-float v1, p1

    int-to-float v2, p3

    int-to-float v3, p2

    sub-float/2addr v2, v3

    div-float v0, v1, v2

    .restart local v0    # "scrollPercentage":F
    goto :goto_0
.end method

.method protected onVerticalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V
    .locals 4
    .param p1, "oldState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;
    .param p2, "newState"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->JUMP:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;

    if-eq p2, v0, :cond_0

    .line 372
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isScrolling()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isScrolling()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->tweenIn(Ljava/lang/Object;)V

    .line 380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 386
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isFast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 390
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$4;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->enumerateVisibleRows(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IRowProcessor;)Ljava/lang/Object;

    .line 401
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    .line 404
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->onVerticalScrollingStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;)V

    .line 405
    return-void

    .line 374
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isScrolling()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$ScrollState;->isScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScrollBar:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->tweenOutOnDelay(Ljava/lang/Object;J)V

    goto :goto_0
.end method

.method public onVirtualOffsetUpdated(I)V
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOffsetDate:Ljava/util/Date;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertPixelsToTime(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 336
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->updateDayView()V

    .line 337
    return-void
.end method

.method public reclaimView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 827
    instance-of v0, p1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    if-eqz v0, :cond_1

    .line 828
    check-cast p1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->reclaim()V

    .line 834
    :cond_0
    :goto_0
    return-void

    .line 829
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    if-eqz v0, :cond_2

    .line 830
    check-cast p1, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->reclaim()V

    goto :goto_0

    .line 831
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    instance-of v0, p1, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    if-eqz v0, :cond_0

    .line 832
    check-cast p1, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->reclaim()V

    goto :goto_0
.end method

.method public refreshViewPort(Z)V
    .locals 1
    .param p1, "clear"    # Z

    .prologue
    .line 343
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGView$3;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$3;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;Z)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->post(Ljava/lang/Runnable;)Z

    .line 366
    return-void
.end method

.method public saveSelectedRow(I)V
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 738
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;

    if-eqz v0, :cond_1

    .line 739
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 740
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setTVSelectedChannel(Ljava/lang/String;I)V

    .line 753
    :goto_0
    return-void

    .line 742
    :cond_0
    const-string v0, "EPGView"

    const-string v1, "saveStartingRow(int): Called on TV VM with no providers"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 744
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;

    if-eqz v0, :cond_3

    .line 745
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 746
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setFavoritesSelectedChannel(Ljava/lang/String;I)V

    goto :goto_0

    .line 748
    :cond_2
    const-string v0, "EPGView"

    const-string v1, "saveStartingRow(int): Called on TV VM with no providers"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 751
    :cond_3
    const-string v0, "EPGView"

    const-string v1, "saveSelectedRow(int): Could not determine model type, view in bad state"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public saveStartingRow(I)V
    .locals 2
    .param p1, "row"    # I

    .prologue
    .line 708
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/TvListingsScreenViewModel;

    if-eqz v0, :cond_1

    .line 709
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setTVLastChannel(Ljava/lang/String;I)V

    .line 723
    :goto_0
    return-void

    .line 712
    :cond_0
    const-string v0, "EPGView"

    const-string v1, "saveStartingRow(int): Called on TV VM with no providers"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 714
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;

    if-eqz v0, :cond_3

    .line 715
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 716
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setFavoritesLastChannel(Ljava/lang/String;I)V

    goto :goto_0

    .line 718
    :cond_2
    const-string v0, "EPGView"

    const-string v1, "saveStartingRow(int): Called on TV VM with no providers"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 721
    :cond_3
    const-string v0, "EPGView"

    const-string v1, "saveStartingRow(int): Could not determine model type, view in bad state"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public scrollTo(Ljava/util/Date;)V
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 294
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->alignDate(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertTimeToPixels(Ljava/util/Date;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->smoothScrollTo(I)V

    .line 295
    return-void
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "seconds"    # I

    .prologue
    .line 239
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    div-int v0, p1, v1

    .line 240
    .local v0, "intervals":I
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalWidth:I

    mul-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->setVirtualWidth(I)V

    .line 241
    return-void
.end method

.method public setModel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 3
    .param p1, "model"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-ne v0, p1, :cond_0

    .line 227
    :goto_0
    return-void

    .line 206
    :cond_0
    const-string v0, "EPGView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreen:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setModel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V

    .line 212
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    if-eqz v0, :cond_2

    .line 216
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->setStartDate(Ljava/util/Date;Z)V

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getTimeSpan()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setDuration(I)V

    .line 219
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;)V

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mModel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V

    goto :goto_0

    .line 225
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;)V

    goto :goto_0
.end method

.method public setOnTapListener(Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mOnTapListener:Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;

    .line 199
    return-void
.end method

.method public setScreen(Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mScreen:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    .line 156
    return-void
.end method

.method public setStartDate(Ljava/util/Date;Z)V
    .locals 8
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "seekToStart"    # Z

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->alignDate(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v2

    .line 246
    .local v2, "startDate":Ljava/util/Date;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v3, v2}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 247
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVirtualOffset()I

    move-result v3

    if-nez v3, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    if-eqz p2, :cond_3

    .line 253
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    .line 255
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->setVirtualOffset(I)V

    .line 269
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getVirtualOffset()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->onVirtualOffsetUpdated(I)V

    .line 271
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->scheduleAutoUpdate()V

    goto :goto_0

    .line 258
    :cond_3
    const-wide/16 v0, 0x0

    .line 259
    .local v0, "offsetSeconds":J
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    if-eqz v3, :cond_4

    .line 260
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long v0, v4, v6

    .line 262
    :cond_4
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mStartDate:Ljava/util/Date;

    .line 264
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-eqz v3, :cond_2

    .line 265
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertSecondsToPixels(J)I

    move-result v3

    neg-int v3, v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->OffsetVirtualSpace(I)V

    goto :goto_1
.end method

.method public setTimeLineInterval(I)V
    .locals 2
    .param p1, "seconds"    # I

    .prologue
    .line 231
    const/16 v0, 0x384

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    .line 232
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalSeconds:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGView;->convertSecondsToPixels(J)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalWidth:I

    .line 233
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView;->mTimeLineIntervalWidth:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->setAlignment(I)V

    .line 234
    return-void
.end method

.method protected showDebugMenu(Landroid/view/View;)V
    .locals 4
    .param p1, "anchor"    # Landroid/view/View;

    .prologue
    .line 1192
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 1193
    .local v1, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 1195
    .local v0, "menu":Landroid/view/Menu;
    const-string v2, "Continuous scroll: stop"

    invoke-interface {v0, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$6;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$6;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1203
    const-string v2, "Continuous scroll: speed 1"

    invoke-interface {v0, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$7;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$7;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1211
    const-string v2, "Continuous scroll: speed 2"

    invoke-interface {v0, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$8;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$8;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1219
    sget-boolean v2, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_HOLES:Z

    if-eqz v2, :cond_0

    const-string v2, "Turn off EPG holes"

    :goto_0
    invoke-interface {v0, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$9;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$9;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1227
    sget-boolean v2, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->CREATE_OVERLAPS:Z

    if-eqz v2, :cond_1

    const-string v2, "Turn off EPG overlap"

    :goto_1
    invoke-interface {v0, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGView$10;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/ui/EPGView$10;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1235
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 1236
    return-void

    .line 1219
    :cond_0
    const-string v2, "Turn on EPG holes"

    goto :goto_0

    .line 1227
    :cond_1
    const-string v2, "Turn on EPG overlap"

    goto :goto_1
.end method

.method protected toast(Ljava/lang/String;)Landroid/widget/Toast;
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1147
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x31

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGView;->getTop()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1148
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1149
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1151
    return-object v0
.end method
