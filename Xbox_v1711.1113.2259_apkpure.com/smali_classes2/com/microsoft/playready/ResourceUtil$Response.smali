.class public Lcom/microsoft/playready/ResourceUtil$Response;
.super Ljava/lang/Object;
.source "ResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/ResourceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# instance fields
.field private mData:[B

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mHttpStatus:I

.field private mScheme:Ljava/lang/String;


# direct methods
.method constructor <init>([BLjava/lang/String;Ljava/util/Map;I)V
    .locals 0
    .param p1, "response"    # [B
    .param p2, "scheme"    # Ljava/lang/String;
    .param p4, "httpStatus"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;I)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mData:[B

    .line 63
    iput-object p2, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mScheme:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHeaders:Ljava/util/Map;

    .line 65
    iput p4, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHttpStatus:I

    .line 66
    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mData:[B

    return-object v0
.end method

.method public getHeader(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v5, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHeaders:Ljava/util/Map;

    if-eqz v5, :cond_1

    .line 75
    iget-object v5, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHeaders:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 76
    .local v2, "headerKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 77
    .local v3, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 88
    .end local v2    # "headerKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    :goto_0
    return-object v5

    .line 78
    .restart local v2    # "headerKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v3    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    .local v0, "header":Ljava/lang/String;
    if-nez v0, :cond_3

    if-eqz p1, :cond_4

    .line 80
    :cond_3
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 81
    :cond_4
    iget-object v5, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHeaders:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 82
    .local v1, "headerItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    .line 83
    .local v4, "r":[Ljava/lang/String;
    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHeaders:Ljava/util/Map;

    return-object v0
.end method

.method public getHttpStatusCode()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mHttpStatus:I

    return v0
.end method

.method public getScheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/playready/ResourceUtil$Response;->mScheme:Ljava/lang/String;

    return-object v0
.end method
