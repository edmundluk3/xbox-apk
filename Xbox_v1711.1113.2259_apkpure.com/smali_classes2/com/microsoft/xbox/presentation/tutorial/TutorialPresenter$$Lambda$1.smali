.class final synthetic Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

.field private final arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$4:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

.field private final arg$5:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

.field private final arg$6:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$4:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iput-object p5, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$5:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    iput-object p6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$6:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/ObservableTransformer;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;-><init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 7

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$4:Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$5:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->arg$6:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->lambda$new$23(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
