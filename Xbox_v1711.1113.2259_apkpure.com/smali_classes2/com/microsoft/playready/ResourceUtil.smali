.class Lcom/microsoft/playready/ResourceUtil;
.super Ljava/lang/Object;
.source "ResourceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;,
        Lcom/microsoft/playready/ResourceUtil$HeaderItem;,
        Lcom/microsoft/playready/ResourceUtil$Peeker;,
        Lcom/microsoft/playready/ResourceUtil$Response;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doHttpTransaction(Ljava/lang/String;Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;[Lcom/microsoft/playready/ResourceUtil$HeaderItem;[B)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 13
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "spec"    # Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;
    .param p2, "headers"    # [Lcom/microsoft/playready/ResourceUtil$HeaderItem;
    .param p3, "body"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 180
    const/4 v9, 0x0

    .line 181
    .local v9, "u":Ljava/net/URL;
    const/4 v1, 0x0

    .line 182
    .local v1, "conn":Ljava/net/HttpURLConnection;
    const/4 v3, 0x0

    .line 183
    .local v3, "is":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 184
    .local v5, "os":Ljava/io/OutputStream;
    if-nez p1, :cond_0

    .line 185
    new-instance p1, Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;

    .end local p1    # "spec":Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;
    invoke-direct {p1}, Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;-><init>()V

    .line 189
    .restart local p1    # "spec":Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;
    :cond_0
    :try_start_0
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 190
    .end local v9    # "u":Ljava/net/URL;
    .local v10, "u":Ljava/net/URL;
    :try_start_1
    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 191
    iget v11, p1, Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;->mTimeOutMs:I

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 192
    iget v11, p1, Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;->mTimeOutMs:I

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 193
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 195
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    array-length v11, v0

    if-nez v11, :cond_7

    .line 196
    :cond_1
    const-string v11, "GET"

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 197
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 203
    :goto_0
    if-eqz p2, :cond_2

    .line 204
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v11, p2

    if-lt v2, v11, :cond_b

    .line 208
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 209
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v11

    const-string v12, "POST"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 210
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 211
    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/io/OutputStream;->write([B)V

    .line 214
    :cond_3
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    .line 215
    .local v8, "responseCode":I
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v7

    .line 216
    .local v7, "respHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {v4, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    .end local v3    # "is":Ljava/io/InputStream;
    .local v4, "is":Ljava/io/InputStream;
    :try_start_2
    invoke-static {v4}, Lcom/microsoft/playready/ResourceUtil;->dumpStreamToByteArray(Ljava/io/InputStream;)[B

    move-result-object v6

    .line 219
    .local v6, "respData":[B
    new-instance v11, Lcom/microsoft/playready/ResourceUtil$Response;

    invoke-virtual {v10}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v6, v12, v7, v8}, Lcom/microsoft/playready/ResourceUtil$Response;-><init>([BLjava/lang/String;Ljava/util/Map;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 221
    if-eqz v4, :cond_4

    .line 222
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 223
    :cond_4
    if-eqz v5, :cond_5

    .line 224
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 225
    :cond_5
    if-eqz v1, :cond_6

    .line 226
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 219
    :cond_6
    return-object v11

    .line 199
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v6    # "respData":[B
    .end local v7    # "respHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v8    # "responseCode":I
    .restart local v3    # "is":Ljava/io/InputStream;
    :cond_7
    :try_start_3
    const-string v11, "POST"

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 200
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v11

    move-object v9, v10

    .line 221
    .end local v10    # "u":Ljava/net/URL;
    .restart local v9    # "u":Ljava/net/URL;
    :goto_2
    if-eqz v3, :cond_8

    .line 222
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 223
    :cond_8
    if-eqz v5, :cond_9

    .line 224
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 225
    :cond_9
    if-eqz v1, :cond_a

    .line 226
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 227
    :cond_a
    throw v11

    .line 205
    .end local v9    # "u":Ljava/net/URL;
    .restart local v2    # "i":I
    .restart local v10    # "u":Ljava/net/URL;
    :cond_b
    :try_start_4
    aget-object v11, p2, v2

    iget-object v11, v11, Lcom/microsoft/playready/ResourceUtil$HeaderItem;->mName:Ljava/lang/String;

    aget-object v12, p2, v2

    iget-object v12, v12, Lcom/microsoft/playready/ResourceUtil$HeaderItem;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v11, v12}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 204
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 220
    .end local v2    # "i":I
    .end local v10    # "u":Ljava/net/URL;
    .restart local v9    # "u":Ljava/net/URL;
    :catchall_1
    move-exception v11

    goto :goto_2

    .end local v3    # "is":Ljava/io/InputStream;
    .end local v9    # "u":Ljava/net/URL;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v7    # "respHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .restart local v8    # "responseCode":I
    .restart local v10    # "u":Ljava/net/URL;
    :catchall_2
    move-exception v11

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    move-object v9, v10

    .end local v10    # "u":Ljava/net/URL;
    .restart local v9    # "u":Ljava/net/URL;
    goto :goto_2
.end method

.method public static downloadUrlResource(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "os":Ljava/io/OutputStream;
    const/4 v4, 0x0

    .line 166
    .local v4, "succeeded":Z
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/playready/ResourceUtil;->getUrlResource(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v3

    .line 167
    .local v3, "resp":Lcom/microsoft/playready/ResourceUtil$Response;
    invoke-virtual {v3}, Lcom/microsoft/playready/ResourceUtil$Response;->getData()[B

    move-result-object v2

    .line 168
    .local v2, "res":[B
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    .end local v0    # "os":Ljava/io/OutputStream;
    .local v1, "os":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 170
    const/4 v4, 0x1

    .line 172
    if-eqz v1, :cond_0

    .line 173
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 176
    :cond_0
    return v4

    .line 171
    .end local v1    # "os":Ljava/io/OutputStream;
    .end local v2    # "res":[B
    .end local v3    # "resp":Lcom/microsoft/playready/ResourceUtil$Response;
    .restart local v0    # "os":Ljava/io/OutputStream;
    :catchall_0
    move-exception v5

    .line 172
    :goto_0
    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 175
    :cond_1
    throw v5

    .line 171
    .end local v0    # "os":Ljava/io/OutputStream;
    .restart local v1    # "os":Ljava/io/OutputStream;
    .restart local v2    # "res":[B
    .restart local v3    # "resp":Lcom/microsoft/playready/ResourceUtil$Response;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "os":Ljava/io/OutputStream;
    .restart local v0    # "os":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method private static dumpStreamToByteArray(Ljava/io/InputStream;)[B
    .locals 6
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x1000

    .line 231
    const/4 v0, 0x0

    .line 232
    .local v0, "nReadSize":I
    new-instance v1, Lorg/apache/http/util/ByteArrayBuffer;

    invoke-direct {v1, v4}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 233
    .local v1, "readBab":Lorg/apache/http/util/ByteArrayBuffer;
    new-array v2, v4, [B

    .line 234
    .local v2, "readBuf":[B
    :goto_0
    invoke-virtual {p0, v2, v5, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 238
    invoke-virtual {v1}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 235
    :cond_0
    invoke-virtual {v1, v2, v5, v0}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    goto :goto_0
.end method

.method public static getFileResource(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v1, 0x0

    .line 135
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v2}, Lcom/microsoft/playready/ResourceUtil;->dumpStreamToByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 139
    .local v0, "data":[B
    new-instance v3, Lcom/microsoft/playready/ResourceUtil$Response;

    const-string v4, "file"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/microsoft/playready/ResourceUtil$Response;-><init>([BLjava/lang/String;Ljava/util/Map;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 141
    if-eqz v2, :cond_0

    .line 142
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 139
    :cond_0
    return-object v3

    .line 140
    .end local v0    # "data":[B
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    .line 141
    :goto_0
    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 144
    :cond_1
    throw v3

    .line 140
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public static getResource(Landroid/net/Uri;)[B
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p0}, Lcom/microsoft/playready/ResourceUtil;->getResource2(Landroid/net/Uri;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v0

    .line 101
    .local v0, "resp":Lcom/microsoft/playready/ResourceUtil$Response;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/microsoft/playready/ResourceUtil$Response;->getData()[B

    move-result-object v1

    .line 104
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getResource(Ljava/lang/String;)[B
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 96
    .local v0, "_uri":Landroid/net/Uri;
    invoke-static {v0}, Lcom/microsoft/playready/ResourceUtil;->getResource(Landroid/net/Uri;)[B

    move-result-object v1

    return-object v1
.end method

.method public static getResource2(Landroid/net/Uri;)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    if-nez p0, :cond_0

    .line 115
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid Argument to GetResource: uri must not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "scheme":Ljava/lang/String;
    const/4 v0, 0x0

    .line 119
    .local v0, "resp":Lcom/microsoft/playready/ResourceUtil$Response;
    if-eqz v1, :cond_1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/playready/ResourceUtil;->getFileResource(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 121
    :cond_2
    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 122
    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/playready/ResourceUtil;->getUrlResource(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v0

    .line 123
    goto :goto_0

    .line 124
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 125
    const-string v3, "Invalid Argument to GetResource: unsupported URI scheme: %1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getResource2(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 110
    .local v0, "_uri":Landroid/net/Uri;
    invoke-static {v0}, Lcom/microsoft/playready/ResourceUtil;->getResource2(Landroid/net/Uri;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v1

    return-object v1
.end method

.method private static getUrlResource(Ljava/lang/String;)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/playready/ResourceUtil;->getUrlResource(Ljava/lang/String;Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v0

    return-object v0
.end method

.method private static getUrlResource(Ljava/lang/String;Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;)Lcom/microsoft/playready/ResourceUtil$Response;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "spec"    # Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-static {p0, p1, v0, v0}, Lcom/microsoft/playready/ResourceUtil;->doHttpTransaction(Ljava/lang/String;Lcom/microsoft/playready/ResourceUtil$ConnectionSpec;[Lcom/microsoft/playready/ResourceUtil$HeaderItem;[B)Lcom/microsoft/playready/ResourceUtil$Response;

    move-result-object v0

    return-object v0
.end method
