.class public final Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.super Ljava/lang/Object;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostTopic;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingPostCategories;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategory;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingQueryType;,
        Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
