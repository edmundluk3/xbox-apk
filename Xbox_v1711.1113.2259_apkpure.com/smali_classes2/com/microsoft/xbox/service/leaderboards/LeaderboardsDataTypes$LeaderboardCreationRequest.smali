.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaderboardCreationRequest"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final keepUpdated:Z

.field private final xuids:Ljava/util/Collection;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "targetXuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->keepUpdated:Z

    .line 73
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->xuids:Ljava/util/Collection;

    .line 76
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 80
    if-ne p1, p0, :cond_0

    .line 81
    const/4 v1, 0x1

    .line 86
    :goto_0
    return v1

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;

    if-nez v1, :cond_1

    .line 83
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 85
    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;

    .line 86
    .local v0, "other":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->xuids:Ljava/util/Collection;

    iget-object v2, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->xuids:Ljava/util/Collection;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 92
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->hashCode:I

    if-nez v0, :cond_0

    .line 93
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->hashCode:I

    .line 94
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->xuids:Ljava/util/Collection;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->hashCode:I

    .line 97
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardCreationRequest;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
