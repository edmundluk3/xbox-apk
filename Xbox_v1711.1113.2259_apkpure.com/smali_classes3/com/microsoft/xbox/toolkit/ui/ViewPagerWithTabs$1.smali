.class Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;
.super Ljava/lang/Object;
.source "ViewPagerWithTabs.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->ensureInitialized()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 7
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 265
    if-nez p1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$500(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;

    move-result-object v1

    const-string v2, "pagetransitionorder"

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$600(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$500(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/toolkit/functions/GenericFunction;->eval(Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 270
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$602(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Ljava/lang/String;)Ljava/lang/String;

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    const-string v1, "%s"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "stateFlag":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$600(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$600(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$602(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 246
    return-void
.end method

.method public onPageSelected(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 250
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$100(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$000(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Landroid/support/design/widget/TabLayout;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderText(Ljava/lang/CharSequence;)V

    .line 251
    const-string v1, "unknown"

    .line 252
    .local v1, "tabName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$200(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$200(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$200(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-gt p1, v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$200(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    aget-object v1, v2, p1

    .line 255
    :cond_0
    const-string v0, "unknown"

    .line 256
    .local v0, "actionName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$300(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$300(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$300(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-gt p1, v2, :cond_1

    .line 257
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$300(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, p1

    .line 259
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "pagename"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "pageaction"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    return-void
.end method
