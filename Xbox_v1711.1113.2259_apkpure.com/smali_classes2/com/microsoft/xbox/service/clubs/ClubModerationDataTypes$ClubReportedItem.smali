.class public abstract Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
.super Ljava/lang/Object;
.source "ClubModerationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubReportedItem"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract contentId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract lastReported()Ljava/util/Date;
.end method

.method public abstract reportCount()I
.end method

.method public abstract reportId()J
.end method

.method public abstract reports()Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;"
        }
    .end annotation
.end method
