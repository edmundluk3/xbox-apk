.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplaySkuAvailability"
.end annotation


# instance fields
.field private final availabilities:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Availabilities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;",
            ">;"
        }
    .end annotation
.end field

.field public final sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Sku"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "sku"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;",
            ">;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;",
            ")V"
        }
    .end annotation

    .prologue
    .line 484
    .local p1, "availabilities":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 487
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->availabilities:Ljava/util/List;

    .line 488
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->sku:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;

    .line 489
    return-void
.end method


# virtual methods
.method public getAvailabilities()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;",
            ">;"
        }
    .end annotation

    .prologue
    .line 493
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;->availabilities:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
