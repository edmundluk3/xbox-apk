.class Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckPinFeedItemRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final timelineId:Ljava/lang/String;

.field private final timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V
    .locals 0
    .param p2, "timelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "timelineId"    # Ljava/lang/String;

    .prologue
    .line 4649
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4650
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 4651
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 4652
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->timelineId:Ljava/lang/String;

    .line 4653
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4661
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->timelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->timelineId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->getFeedPinInfo(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;

    move-result-object v0

    .line 4662
    .local v0, "pinsInfo":Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->numItems()I

    move-result v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PinsInfo;->maxPins()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4644
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$CheckPinFeedItemRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4671
    const-wide/16 v0, 0xc8e

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4667
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4657
    return-void
.end method
