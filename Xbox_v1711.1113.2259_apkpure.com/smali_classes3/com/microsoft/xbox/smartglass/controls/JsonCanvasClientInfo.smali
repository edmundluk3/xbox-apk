.class Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;
.super Ljava/lang/Object;
.source "JsonCanvasClientInfo.java"


# instance fields
.field public final clientVersion:Ljava/lang/String;

.field public final majorVersion:I

.field public final minorVersion:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 18
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "clientVersion"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->clientVersion:Ljava/lang/String;

    .line 21
    const-string v4, "majorVersion"

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 22
    .local v1, "major":I
    const-string v4, "minorVersion"

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 23
    .local v2, "minor":I
    if-nez v1, :cond_0

    .line 24
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->clientVersion:Ljava/lang/String;

    const-string v5, "\\."

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 25
    .local v3, "versionParts":[Ljava/lang/String;
    aget-object v4, v3, v6

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 26
    const/4 v4, 0x1

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 29
    .end local v3    # "versionParts":[Ljava/lang/String;
    :cond_0
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->majorVersion:I

    .line 30
    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->minorVersion:I

    .line 31
    return-void
.end method


# virtual methods
.method public getVersion()Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    iget v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->majorVersion:I

    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->minorVersion:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;-><init>(II)V

    return-object v0
.end method
