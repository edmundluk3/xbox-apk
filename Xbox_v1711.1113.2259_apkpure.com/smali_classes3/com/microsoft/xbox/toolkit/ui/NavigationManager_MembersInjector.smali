.class public final Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;
.super Ljava/lang/Object;
.source "NavigationManager_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final authStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final homeScreenRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p2, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p3, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 31
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p1, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p2, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAuthStateManager(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 64
    return-void
.end method

.method public static injectHomeScreenRepository(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 54
    return-void
.end method

.method public static injectTutorialRepository(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 59
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 43
    if-nez p1, :cond_0

    .line 44
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 49
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->injectMembers(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    return-void
.end method
