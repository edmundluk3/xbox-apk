.class abstract Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;
.super Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
.source "$AutoValue_MultiplayerSessionDataTypes_SessionReference.java"


# instance fields
.field private final name:Ljava/lang/String;

.field private final scid:Ljava/lang/String;

.field private final templateName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "scid"    # Ljava/lang/String;
    .param p2, "templateName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null scid"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->scid:Ljava/lang/String;

    .line 22
    if-nez p2, :cond_1

    .line 23
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null templateName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->templateName:Ljava/lang/String;

    .line 26
    if-nez p3, :cond_2

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->name:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 64
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 66
    .local v0, "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->scid:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->scid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->templateName:Ljava/lang/String;

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->templateName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->name:Ljava/lang/String;

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    :cond_3
    move v1, v2

    .line 70
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 75
    const/4 v0, 0x1

    .line 76
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->scid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 78
    mul-int/2addr v0, v2

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->templateName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 80
    mul-int/2addr v0, v2

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 82
    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->name:Ljava/lang/String;

    return-object v0
.end method

.method public scid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->scid:Ljava/lang/String;

    return-object v0
.end method

.method public templateName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->templateName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SessionReference{scid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->scid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", templateName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->templateName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_SessionReference;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
