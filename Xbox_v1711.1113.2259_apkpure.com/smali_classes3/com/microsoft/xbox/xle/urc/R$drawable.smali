.class public final Lcom/microsoft/xbox/xle/urc/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final btn_113x_circle_gray:I = 0x7f020069

.field public static final btn_113x_circle_gray_press:I = 0x7f02006a

.field public static final btn_113x_circle_white:I = 0x7f02006b

.field public static final btn_113x_circle_white_press:I = 0x7f02006c

.field public static final btn_139x72_btn:I = 0x7f02006d

.field public static final btn_139x72_btn_press:I = 0x7f02006e

.field public static final btn_186x_circle_white:I = 0x7f02006f

.field public static final btn_186x_circle_white_press:I = 0x7f020070

.field public static final btn_190x72_btn:I = 0x7f020071

.field public static final btn_190x72_btn_press:I = 0x7f020072

.field public static final btn_45x10_more:I = 0x7f020073

.field public static final btn_45x10_more_2x:I = 0x7f020074

.field public static final btn_a_corner:I = 0x7f020075

.field public static final btn_a_corner_press:I = 0x7f020076

.field public static final btn_arrow_dn:I = 0x7f020077

.field public static final btn_arrow_dn_2x:I = 0x7f020078

.field public static final btn_arrow_left:I = 0x7f020079

.field public static final btn_arrow_left_2x:I = 0x7f02007a

.field public static final btn_arrow_right:I = 0x7f02007b

.field public static final btn_arrow_right_2x:I = 0x7f02007c

.field public static final btn_arrow_up:I = 0x7f02007d

.field public static final btn_arrow_up_2x:I = 0x7f02007e

.field public static final btn_b_corner:I = 0x7f02007f

.field public static final btn_b_corner_press:I = 0x7f020080

.field public static final btn_big_white_circle:I = 0x7f020081

.field public static final btn_c_corner:I = 0x7f020082

.field public static final btn_c_corner_press:I = 0x7f020083

.field public static final btn_close:I = 0x7f020084

.field public static final btn_color_rect:I = 0x7f020085

.field public static final btn_d_corner:I = 0x7f020086

.field public static final btn_d_corner_press:I = 0x7f020087

.field public static final btn_dark_circle:I = 0x7f020088

.field public static final btn_nav_a:I = 0x7f020089

.field public static final btn_nav_b:I = 0x7f02008a

.field public static final btn_nav_c:I = 0x7f02008b

.field public static final btn_nav_d:I = 0x7f02008c

.field public static final btn_numpad_rect:I = 0x7f02008d

.field public static final btn_remote_vol_dn:I = 0x7f02008e

.field public static final btn_remote_vol_md:I = 0x7f02008f

.field public static final btn_remote_vol_up:I = 0x7f020090

.field public static final btn_small_rect:I = 0x7f020091

.field public static final btn_vol_dn:I = 0x7f020092

.field public static final btn_vol_up:I = 0x7f020093

.field public static final btn_white:I = 0x7f020094

.field public static final btn_white_circle:I = 0x7f020095

.field public static final btn_white_press:I = 0x7f020096

.field public static final button_shape_off:I = 0x7f02009f

.field public static final button_shape_on:I = 0x7f0200a0

.field public static final circle_white:I = 0x7f0200a8

.field public static final circle_white_press:I = 0x7f0200a9

.field public static final dot_blue:I = 0x7f020105

.field public static final dot_green:I = 0x7f020106

.field public static final dot_red:I = 0x7f020107

.field public static final dot_yellow:I = 0x7f020108

.field public static final gesture_area_gradient:I = 0x7f020128

.field public static final ic_launcher:I = 0x7f020139

.field public static final ico_dash:I = 0x7f020142

.field public static final ico_dn_arrow:I = 0x7f020143

.field public static final ico_fwd:I = 0x7f020144

.field public static final ico_menu:I = 0x7f020145

.field public static final ico_minus:I = 0x7f020146

.field public static final ico_mute:I = 0x7f020147

.field public static final ico_next:I = 0x7f020148

.field public static final ico_pause:I = 0x7f020149

.field public static final ico_play:I = 0x7f02014a

.field public static final ico_plus:I = 0x7f02014b

.field public static final ico_power_off:I = 0x7f02014c

.field public static final ico_power_on:I = 0x7f02014d

.field public static final ico_record:I = 0x7f02014e

.field public static final ico_replay:I = 0x7f02014f

.field public static final ico_reset:I = 0x7f020150

.field public static final ico_rwd:I = 0x7f020151

.field public static final ico_skip:I = 0x7f020152

.field public static final ico_sm_arrow_dn:I = 0x7f020153

.field public static final ico_sm_arrow_dn_2x:I = 0x7f020154

.field public static final ico_sm_arrow_up:I = 0x7f020155

.field public static final ico_sm_arrow_up_2x:I = 0x7f020156

.field public static final ico_stop:I = 0x7f020157

.field public static final ico_up_arrow:I = 0x7f020158

.field public static final icon_arrow_down:I = 0x7f020159

.field public static final icon_arrow_left:I = 0x7f02015a

.field public static final icon_arrow_right:I = 0x7f02015b

.field public static final icon_arrow_up:I = 0x7f02015c

.field public static final remote_center_button:I = 0x7f0201b0

.field public static final remote_center_button_press:I = 0x7f0201b1

.field public static final remote_chdn_arrow_icon:I = 0x7f0201b2

.field public static final remote_chup_arrow_icon:I = 0x7f0201b3

.field public static final remote_down_button:I = 0x7f0201b4

.field public static final remote_down_button_press:I = 0x7f0201b5

.field public static final remote_minus_icon:I = 0x7f0201c0

.field public static final remote_mute_icon:I = 0x7f0201c1

.field public static final remote_numberpad_button:I = 0x7f0201c2

.field public static final remote_numberpad_button_press:I = 0x7f0201c3

.field public static final remote_plus_icon:I = 0x7f0201c7

.field public static final remote_return_icon:I = 0x7f0201c8

.field public static final remote_up_button:I = 0x7f0201c9

.field public static final remote_up_button_press:I = 0x7f0201ca

.field public static final vol_ch:I = 0x7f0201fe

.field public static final vol_ch_press:I = 0x7f0201ff

.field public static final vol_ch_up:I = 0x7f020200

.field public static final vol_ch_up_press:I = 0x7f020201


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
