.class public Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubNameSelectionDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;
    }
.end annotation


# instance fields
.field private checkAvailabilityBtn:Landroid/widget/Button;

.field private clearEditText:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeDialog:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private clubNameEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "clubNameSelectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 37
    const v2, 0x7f08021b

    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 38
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 39
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 42
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030076

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 44
    .local v1, "root":Landroid/view/View;
    const v2, 0x7f0e0312

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->closeDialog:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 45
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->closeDialog:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v2, 0x7f0e0313

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->checkAvailabilityBtn:Landroid/widget/Button;

    .line 48
    const v2, 0x7f0e0314

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    .line 49
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 50
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    invoke-static {p0, p3}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 59
    const v2, 0x7f0e0315

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clearEditText:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 60
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clearEditText:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->checkAvailabilityBtn:Landroid/widget/Button;

    invoke-static {p0, p3}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->setContentView(Landroid/view/View;)V

    .line 70
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->dismiss()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "clubNameSelectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;
    .param p2, "v"    # Landroid/widget/TextView;
    .param p3, "actionId"    # I
    .param p4, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 52
    const/4 v0, 0x6

    if-eq p3, v0, :cond_0

    if-nez p3, :cond_1

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;->onClubNameSelected(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->dismiss()V

    .line 56
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->checkAvailabilityBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "clubNameSelectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->clubNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog$ClubNameSelectionListener;->onClubNameSelected(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 85
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 75
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionDialog;->checkAvailabilityBtn:Landroid/widget/Button;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x4

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 80
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
