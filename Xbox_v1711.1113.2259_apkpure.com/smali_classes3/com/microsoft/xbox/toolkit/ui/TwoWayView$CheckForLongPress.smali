.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;
.super Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;
.source "TwoWayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 1

    .prologue
    .line 6097
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6097
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 6100
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v4

    .line 6101
    .local v4, "motionPosition":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v6

    sub-int v6, v4, v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 6103
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 6104
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/widget/ListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 6106
    .local v2, "longPressId":J
    const/4 v1, 0x0

    .line 6107
    .local v1, "handled":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->sameWindow()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 6108
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5, v0, v4, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$4000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;IJ)Z

    move-result v1

    .line 6111
    :cond_0
    if-eqz v1, :cond_2

    .line 6112
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v6, -0x1

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 6113
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 6114
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 6119
    .end local v1    # "handled":Z
    .end local v2    # "longPressId":J
    :cond_1
    :goto_0
    return-void

    .line 6116
    .restart local v1    # "handled":Z
    .restart local v2    # "longPressId":J
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    goto :goto_0
.end method
