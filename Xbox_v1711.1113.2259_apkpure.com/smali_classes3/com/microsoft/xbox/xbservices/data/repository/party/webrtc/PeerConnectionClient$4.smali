.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Lorg/webrtc/voiceengine/WebRtcAudioRecord$WebRtcAudioRecordErrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createPeerConnectionFactoryInternal(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onWebRtcAudioRecordError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onWebRtcAudioRecordError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    .line 357
    return-void
.end method

.method public onWebRtcAudioRecordInitError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onWebRtcAudioRecordInitError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method public onWebRtcAudioRecordStartError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onWebRtcAudioRecordStartError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$4;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    .line 352
    return-void
.end method
