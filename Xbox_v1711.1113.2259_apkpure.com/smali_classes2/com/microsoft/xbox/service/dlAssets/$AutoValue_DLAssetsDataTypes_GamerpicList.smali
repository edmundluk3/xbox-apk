.class abstract Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;
.super Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;
.source "$AutoValue_DLAssetsDataTypes_GamerpicList.java"


# instance fields
.field private final gamerpics:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;"
        }
    .end annotation
.end field

.field private final version:I


# direct methods
.method constructor <init>(ILcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "version"    # I
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "gamerpics":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;-><init>()V

    .line 17
    iput p1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->version:I

    .line 18
    iput-object p2, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;
    iget v3, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->version:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;->version()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_3

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;->gamerpics()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;->gamerpics()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;
    :cond_4
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public gamerpics()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 57
    iget v1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->version:I

    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v2

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 60
    return v0

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GamerpicList{version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->version:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerpics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->gamerpics:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public version()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/microsoft/xbox/service/dlAssets/$AutoValue_DLAssetsDataTypes_GamerpicList;->version:I

    return v0
.end method
