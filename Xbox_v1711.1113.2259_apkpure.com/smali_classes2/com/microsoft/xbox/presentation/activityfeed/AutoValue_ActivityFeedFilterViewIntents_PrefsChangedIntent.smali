.class final Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;
.super Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
.source "AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent.java"


# instance fields
.field private final prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V
    .locals 2
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null prefs"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;->prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 34
    if-ne p1, p0, :cond_0

    .line 35
    const/4 v1, 0x1

    .line 41
    :goto_0
    return v1

    .line 37
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;

    .line 39
    .local v0, "that":Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;->prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;->prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 41
    .end local v0    # "that":Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x1

    .line 47
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;->prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 49
    return v0
.end method

.method public prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;->prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrefsChangedIntent{prefs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/AutoValue_ActivityFeedFilterViewIntents_PrefsChangedIntent;->prefs:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
