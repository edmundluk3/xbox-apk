.class public Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "UnsharedActivityFeedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnsharedActivityFeedScreenParameters"
.end annotation


# instance fields
.field private clubId:J

.field private conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

.field private launchSource:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;)V
    .locals 2
    .param p1, "launchSource"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 134
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;-><init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;J)V

    .line 135
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;J)V
    .locals 2
    .param p1, "launchSource"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 138
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 139
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->ClubFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    if-eq p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 141
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->launchSource:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 142
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->clubId:J

    .line 143
    return-void

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V
    .locals 0
    .param p1, "launchSource"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "conversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->launchSource:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    .line 148
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 149
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->launchSource:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;->clubId:J

    return-wide v0
.end method
