.class final enum Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;
.super Ljava/lang/Enum;
.source "ConnectDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "InteralState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

.field public static final enum DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

.field public static final enum NORMAL:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 369
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->NORMAL:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    const-string v1, "DISCONNECTING"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    .line 368
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->NORMAL:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 368
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 368
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;
    .locals 1

    .prologue
    .line 368
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$InteralState;

    return-object v0
.end method
