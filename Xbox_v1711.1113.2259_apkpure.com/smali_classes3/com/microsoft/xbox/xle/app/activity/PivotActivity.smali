.class public abstract Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PivotActivity.java"


# instance fields
.field protected pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method private addPivotPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)V
    .locals 8
    .param p1, "paneData"    # Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    .param p2, "index"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 373
    const-string v1, "PivotActivity"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Adding pivot pane class \'%s\'"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIsStarted()Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 381
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->addPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 382
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_1

    .line 384
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    .line 385
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 386
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 387
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setActiveOnAdd(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 390
    invoke-virtual {p1, v6}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->setIsDisplayed(Z)V

    .line 393
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 398
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v1

    if-lt v1, p2, :cond_2

    .line 399
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    .line 404
    :goto_0
    const-string v1, "DetailsPivotActivity"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Successfully added pivot pane class \'%s\'"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->getPaneClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_1
    return-void

    .line 401
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    goto :goto_0
.end method


# virtual methods
.method public addPivotPane(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v0

    .line 361
    .local v0, "index":I
    const/16 v1, 0x64

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->addPivotPane(Ljava/lang/Class;II)V

    .line 362
    return-void
.end method

.method public addPivotPane(Ljava/lang/Class;IF)V
    .locals 2
    .param p2, "index"    # I
    .param p3, "dips"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;IF)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->setScreenDIPs(F)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->addPivotPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)V

    .line 370
    return-void
.end method

.method public addPivotPane(Ljava/lang/Class;II)V
    .locals 2
    .param p2, "index"    # I
    .param p3, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;->setScreenRatio(I)Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->addPivotPane(Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;I)V

    .line 366
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 2
    .param p1, "bottomMargin"    # I

    .prologue
    const v1, 0x7f090054

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr p1, v0

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->adjustBottomMargin(I)V

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr p1, v0

    goto :goto_0
.end method

.method public adjustPaneSize(Ljava/lang/Class;I)V
    .locals 1
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 410
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->adjustPaneSize(Ljava/lang/Class;I)V

    .line 411
    return-void
.end method

.method public animateToActivePivotPane(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "screen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v0

    .line 308
    .local v0, "currentIndex":I
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v1

    .line 310
    .local v1, "newIndex":I
    if-ne v0, v1, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    .line 313
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->animateToCurrentPaneIndex(I)V

    goto :goto_0
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->shouldRefreshAsPivotHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceRefresh()V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->forceRefresh()V

    .line 293
    return-void
.end method

.method public forceUpdateViewImmediately()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 233
    return-void
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    const-string v0, "DefaultPivotActivity"

    return-object v0
.end method

.method protected getChannelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    const-string v0, "DefaultPivotChannel"

    return-object v0
.end method

.method public getCurrentPivotPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    return-object v0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    return v0
.end method

.method public getPaneCount()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getCurrentPivotPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 62
    .local v0, "currentPivotPane":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 65
    :cond_0
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onAnimateInCompleted()V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onAnimateInCompleted()V

    .line 227
    :cond_0
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onAnimateInStarted()V

    .line 219
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 193
    return-void
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onDestroy()V

    .line 158
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onPause()V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onPause()V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 119
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onRehydrate()V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "PivotActivity"

    const-string v1, "onRehydrate called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onRehydrate()V

    .line 183
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onResume()V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onResume()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 130
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 237
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 239
    const-string v0, "PivotActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saving current pivot pane screen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    return-void
.end method

.method public onSetActive()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetActive()V

    .line 135
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v2, :cond_0

    .line 138
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getAndResetActivePivotPaneClass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 139
    .local v1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    if-eqz v1, :cond_1

    .line 140
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    .line 141
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 142
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onSetActive(I)V

    .line 150
    .end local v0    # "index":I
    .end local v1    # "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_0
    :goto_0
    return-void

    .line 146
    .restart local v1    # "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onSetActive()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 69
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v2, :cond_1

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getActivePivotPaneClass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 73
    .local v1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    .line 75
    .local v0, "index":I
    if-ltz v0, :cond_2

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setStartPaneIndex(I)V

    .line 83
    .end local v0    # "index":I
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onStart()V

    .line 85
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    new-instance v3, Lcom/microsoft/xbox/xle/app/activity/PivotActivity$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/PivotActivity;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V

    .line 96
    .end local v1    # "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_1
    return-void

    .line 78
    .restart local v0    # "index":I
    .restart local v1    # "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setStartPaneIndex(I)V

    .line 79
    const-string v2, "PivotActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t find start pane "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStop()V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onStop()V

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V

    .line 106
    :cond_0
    return-void
.end method

.method public onTombstone()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onTombstone()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onTombstone()V

    .line 173
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->isTombstoned:Z

    .line 174
    return-void
.end method

.method public removeBottomMargin()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->adjustBottomMargin(I)V

    .line 277
    :cond_0
    return-void
.end method

.method public removePivotPane(Ljava/lang/Class;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pivotPaneClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 318
    const-string v3, "DetailsPivotActivity"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Removing pivot pane class \'%s\'"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIsStarted()Z

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 323
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    .line 325
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 326
    const-string v3, "PIvotActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "can\'t find pane for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 331
    .local v1, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v1, :cond_3

    .line 333
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 334
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 335
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 336
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    .line 339
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getTotalPaneCount()I

    move-result v2

    .line 340
    .local v2, "totalPaneCount":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->isPageIndicatorVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setTotalPageCount(I)V

    .line 345
    :cond_1
    if-lez v0, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v3

    if-lt v3, v0, :cond_2

    .line 346
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    .line 351
    :goto_1
    const-string v3, "DetailsPivotActivity"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Successfully removed pivot pane class \'%s\'"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 348
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setCurrentPaneIndex(I)V

    goto :goto_1

    .line 355
    .end local v2    # "totalPaneCount":I
    :cond_3
    const-string v3, "DetailsPivotActivity"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Failed to remove pivot pane class \'%s\'"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetBottomMargin()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->resetBottomMargin()V

    .line 284
    :cond_0
    return-void
.end method

.method public setActivePivotPane(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "screen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v0

    .line 297
    .local v0, "currentIndex":I
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v1

    .line 299
    .local v1, "newIndex":I
    if-ne v0, v1, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    .line 302
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->switchToPane(I)V

    goto :goto_0
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 415
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->setScreenState(I)V

    .line 416
    return-void
.end method

.method public xleFindViewId(I)Landroid/view/View;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 420
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->xleFindViewId(I)Landroid/view/View;

    move-result-object v0

    .line 421
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->xleFindViewId(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-object v0
.end method
