.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.source "HomeScreenNowPlayingAdapterPhone.java"


# static fields
.field private static final BAT_HEIGHT_RATIO:F = 0.75949365f

.field private static final WIDTH_IN_CELLS:I = 0x4


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p2, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p2, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p3, "container"    # Landroid/view/View;
    .param p4, "section"    # Landroid/view/View;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Landroid/view/View;Landroid/view/View;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected bindRecentsRecents()V
    .locals 2

    .prologue
    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->createScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v0

    .line 40
    .local v0, "state":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindRecentsRecents()V

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->applyScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 42
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->onStart()V

    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->applyScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 26
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->createScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 31
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->onStop()V

    .line 32
    return-void
.end method

.method protected resizeFullContainer(Landroid/view/View;)V
    .locals 4
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 47
    .local v1, "lpFull":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->getRootSize()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->computeCellSize(I)I

    move-result v0

    .line 48
    .local v0, "cellSize":I
    const/4 v2, 0x4

    invoke-virtual {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->computeMultiCellSize(II)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 49
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->computeMultiCellSize(II)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3f426e2d

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 50
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    return-void
.end method

.method protected resizeRecents(Landroid/view/View;)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 55
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapterPhone;->resizeTwoWayView(Landroid/view/View;I)V

    .line 56
    return-void
.end method
