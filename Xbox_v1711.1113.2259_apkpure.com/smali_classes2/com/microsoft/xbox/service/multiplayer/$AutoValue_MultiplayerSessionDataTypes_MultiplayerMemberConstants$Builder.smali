.class final Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;
.super Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants$Builder;
.source "$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private custom:Lcom/google/gson/JsonObject;

.field private system:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants$Builder;-><init>()V

    .line 68
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants$Builder;-><init>()V

    .line 70
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->custom()Lcom/google/gson/JsonObject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->custom:Lcom/google/gson/JsonObject;

    .line 71
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->system:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    .line 72
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;
    .locals 3

    .prologue
    .line 85
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants;

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->custom:Lcom/google/gson/JsonObject;

    iget-object v2, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->system:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants;-><init>(Lcom/google/gson/JsonObject;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;)V

    return-object v0
.end method

.method public custom(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants$Builder;
    .locals 0
    .param p1, "custom"    # Lcom/google/gson/JsonObject;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->custom:Lcom/google/gson/JsonObject;

    .line 76
    return-object p0
.end method

.method public system(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants$Builder;
    .locals 0
    .param p1, "system"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 80
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstants$Builder;->system:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    .line 81
    return-object p0
.end method
