.class public Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreen;
.super Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;
.source "ClubSearchScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "Clubs - Search Clubs view"

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f030099

    return v0
.end method

.method protected getRootViewId()I
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f03009a

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->onCreate()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    return-void
.end method
