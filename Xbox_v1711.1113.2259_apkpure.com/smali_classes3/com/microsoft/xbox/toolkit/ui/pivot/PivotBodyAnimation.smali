.class public Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;
.super Landroid/view/animation/Animation;
.source "PivotBodyAnimation.java"


# instance fields
.field private body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

.field private endX:F

.field private startX:F


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;FF)V
    .locals 0
    .param p1, "body"    # Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;
    .param p2, "startX"    # F
    .param p3, "endX"    # F

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    .line 30
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->startX:F

    .line 31
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->endX:F

    .line 32
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 36
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->endX:F

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->startX:F

    sub-float v0, v2, v3

    .line 37
    .local v0, "dx":F
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->startX:F

    mul-float v3, p1, v0

    add-float v1, v2, v3

    .line 42
    .local v1, "x":F
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    float-to-int v3, v1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setScrollX(I)V

    .line 43
    return-void
.end method

.method public setAnimationEndPostRunnable(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 67
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method
