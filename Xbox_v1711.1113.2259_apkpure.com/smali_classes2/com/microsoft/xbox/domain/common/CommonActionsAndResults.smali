.class public final Lcom/microsoft/xbox/domain/common/CommonActionsAndResults;
.super Ljava/lang/Object;
.source "CommonActionsAndResults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ItemClickedResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$CancelledIntentResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ConfirmedIntentResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LceResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreActionWithParameter;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;,
        Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
