.class public Lcom/microsoft/xbox/service/model/serialization/GameInfo;
.super Ljava/lang/Object;
.source "GameInfo.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
.end annotation


# static fields
.field public static final GAMETYPE_ALL:I = 0xf

.field public static final GAMETYPE_NONE:I = 0x0

.field public static final GAMETYPE_WEB:I = 0x2

.field public static final GAMETYPE_WINDOSWPC:I = 0x8

.field public static final GAMETYPE_WINDOWSPHONE:I = 0x1

.field public static final GAMETYPE_XBOXCONSOLE:I = 0x4


# instance fields
.field public AchievementsEarned:I
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public GameUrl:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public Gamerscore:I
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public Id:J
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public ImageUri:Ljava/lang/String;

.field private ImageUrl:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public LastPlayed:Ljava/util/Date;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/convert/Convert;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;
    .end annotation
.end field

.field public Name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public TotalAchievements:I
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public TotalPossibleGamerscore:I
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public Type:I
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
