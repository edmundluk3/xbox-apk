.class public Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConversationsReportScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "Report Screen"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;->onCreateContentView()V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsReportScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 15
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const v0, 0x7f0300bd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;->setContentView(I)V

    .line 20
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;->setShowUtilityBar(Z)V

    .line 21
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsReportScreen;->setShowRightPane(Z)V

    .line 22
    return-void
.end method
