.class public final Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;
.super Ljava/lang/Object;
.source "BackCompatDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackCompatResponse"
.end annotation


# instance fields
.field private final backCompatItems:Ljava/util/Map;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "settings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "backCompatItems":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 39
    if-ne p1, p0, :cond_1

    move v1, v2

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 44
    check-cast v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    .line 45
    .local v0, "other":Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    iget-object v4, v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    .line 46
    invoke-interface {v3, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getBackCompatItems()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->hashCode:I

    if-nez v0, :cond_0

    .line 53
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->hashCode:I

    .line 54
    iget v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->backCompatItems:Ljava/util/Map;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->hashCode:I

    .line 57
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
