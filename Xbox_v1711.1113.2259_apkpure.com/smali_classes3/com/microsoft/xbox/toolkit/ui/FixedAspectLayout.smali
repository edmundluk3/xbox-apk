.class public Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;
.super Landroid/widget/FrameLayout;
.source "FixedAspectLayout.java"


# static fields
.field public static final FIX_HEIGHT:I = 0x2

.field public static final FIX_WIDTH:I = 0x1

.field private static final NOT_DEFINED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "FixedAspectLayout"


# instance fields
.field private aspectX:I

.field private aspectY:I

.field private fixHeight:Z

.field private fixWidth:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->Init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->Init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method private Init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 47
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->FixedAspectLayout:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    .line 50
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    .line 52
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 53
    .local v1, "fixDimensionFlags":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->setFixDimension(I)V

    .line 55
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    return-void
.end method


# virtual methods
.method public getAspectX()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    return v0
.end method

.method public getAspectY()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x1

    .line 91
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    if-lez v5, :cond_0

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    if-gtz v5, :cond_1

    .line 92
    :cond_0
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    .line 93
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    .line 94
    const-string v5, "FixedAspectLayout"

    const-string v6, "Aspect ratio undefined. Assuming 1x1"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 98
    .local v4, "originalWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 101
    .local v3, "originalHeight":I
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixWidth:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixHeight:Z

    if-nez v5, :cond_2

    .line 102
    move v2, v4

    .line 103
    .local v2, "finalWidth":I
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    mul-int/2addr v5, v4

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    div-int v1, v5, v6

    .line 118
    .local v1, "finalHeight":I
    :goto_0
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-super {p0, v5, v6}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 119
    return-void

    .line 104
    .end local v1    # "finalHeight":I
    .end local v2    # "finalWidth":I
    :cond_2
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixHeight:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixWidth:Z

    if-nez v5, :cond_3

    .line 105
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    mul-int/2addr v5, v3

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    div-int v2, v5, v6

    .line 106
    .restart local v2    # "finalWidth":I
    move v1, v3

    .restart local v1    # "finalHeight":I
    goto :goto_0

    .line 108
    .end local v1    # "finalHeight":I
    .end local v2    # "finalWidth":I
    :cond_3
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    mul-int/2addr v5, v4

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    div-int v0, v5, v6

    .line 109
    .local v0, "calculatedHeight":I
    if-le v0, v3, :cond_4

    .line 110
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    mul-int/2addr v5, v3

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    div-int v2, v5, v6

    .line 111
    .restart local v2    # "finalWidth":I
    move v1, v3

    .restart local v1    # "finalHeight":I
    goto :goto_0

    .line 113
    .end local v1    # "finalHeight":I
    .end local v2    # "finalWidth":I
    :cond_4
    move v2, v4

    .line 114
    .restart local v2    # "finalWidth":I
    move v1, v0

    .restart local v1    # "finalHeight":I
    goto :goto_0
.end method

.method public setAspectX(I)V
    .locals 0
    .param p1, "aspectX"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectX:I

    .line 64
    return-void
.end method

.method public setAspectY(I)V
    .locals 0
    .param p1, "aspectY"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->aspectY:I

    .line 72
    return-void
.end method

.method public setFixDimension(I)V
    .locals 3
    .param p1, "fixFlags"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 75
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 76
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixWidth:Z

    .line 77
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixHeight:Z

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixWidth:Z

    .line 84
    :cond_2
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/FixedAspectLayout;->fixHeight:Z

    goto :goto_0
.end method
