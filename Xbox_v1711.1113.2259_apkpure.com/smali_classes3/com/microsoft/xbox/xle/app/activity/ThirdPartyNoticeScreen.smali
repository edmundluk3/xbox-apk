.class public Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "ThirdPartyNoticeScreen.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ThirdPartyNoticeScreen"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    .line 13
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;->setShowUtilityBar(Z)V

    .line 14
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;->setDesiredDrawerLockState(I)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string v0, "ThirdPartyNoticeScreen"

    return-object v0
.end method

.method public getShouldShowAppbar()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;->onCreateContentView()V

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ThirdPartyNoticeSceenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 22
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f03022b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ThirdPartyNoticeScreen;->setContentView(I)V

    .line 27
    return-void
.end method
