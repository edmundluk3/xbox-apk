.class public Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "ConversationsActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter$ConversationSummaryListItemTagData;
    }
.end annotation


# instance fields
.field private convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

.field private conversationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;",
            ">;"
        }
    .end annotation
.end field

.field private listView:Landroid/support/v7/widget/RecyclerView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private updateViewRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

.field private writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
    .locals 6
    .param p1, "messageViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->updateViewRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 41
    const v1, 0x7f0e04a7

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->screenBody:Landroid/view/View;

    .line 42
    const v1, 0x7f0e04a9

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->content:Landroid/view/View;

    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->content:Landroid/view/View;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 47
    const v1, 0x7f0e04ac

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 49
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 50
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    .line 51
    const v1, 0x7f0e04aa

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-direct {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;-><init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->updateViewRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-wide/16 v4, 0x1f4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 64
    invoke-virtual {v2, v4, v5, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v2

    .line 65
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 66
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 62
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 68
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;
    .param p1, "ignore"    # Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->updateViewInternal()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackCreateMessage()V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->navigateToFriendsPickerScreen()V

    .line 78
    return-void
.end method

.method private updateViewInternal()V
    .locals 3

    .prologue
    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getListState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->getSLSConversationList()Ljava/util/ArrayList;

    move-result-object v0

    .line 109
    .local v0, "newData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->conversationList:Ljava/util/List;

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->shouldForceUpdateSummaryAdapter()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;->clearForceUpdateSummaryAdapter()V

    .line 112
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->conversationList:Ljava/util/List;

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->clear()V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->conversationList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->convListAdapter:Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsListAdapter;->notifyDataSetChanged()V

    .line 118
    :cond_1
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onPause()V

    .line 85
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 89
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onResume()V

    .line 90
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->writeMessage:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 99
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;->updateViewRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;->INSTANCE:Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 103
    return-void
.end method
