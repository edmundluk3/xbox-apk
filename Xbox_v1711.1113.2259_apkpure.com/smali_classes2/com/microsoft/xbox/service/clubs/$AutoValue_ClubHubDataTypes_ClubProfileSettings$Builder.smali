.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubProfileSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private delete:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private update:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;-><init>()V

    .line 86
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;-><init>()V

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->update:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->delete:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 91
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    .locals 4

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfileSettings;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->update:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->delete:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfileSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-object v0
.end method

.method public delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "delete":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->delete:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 100
    return-object p0
.end method

.method public update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "update":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->update:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 95
    return-object p0
.end method

.method public view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "view":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfileSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 105
    return-object p0
.end method
