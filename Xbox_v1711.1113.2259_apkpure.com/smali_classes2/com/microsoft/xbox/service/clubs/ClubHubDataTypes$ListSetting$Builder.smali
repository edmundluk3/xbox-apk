.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1011
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract allowedValues(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract canViewerChangeSetting(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract value(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation
.end method
