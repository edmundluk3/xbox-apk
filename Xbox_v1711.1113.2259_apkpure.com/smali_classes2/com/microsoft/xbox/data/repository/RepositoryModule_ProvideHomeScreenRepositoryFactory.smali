.class public final Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;
.super Ljava/lang/Object;
.source "RepositoryModule_ProvideHomeScreenRepositoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

.field private final sharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p3, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 33
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p2, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;-><init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideHomeScreenRepository(Lcom/microsoft/xbox/data/repository/RepositoryModule;Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideHomeScreenRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .locals 3

    .prologue
    .line 37
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 39
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->telemetryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    .line 38
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideHomeScreenRepository(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;)Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 37
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->get()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    move-result-object v0

    return-object v0
.end method
