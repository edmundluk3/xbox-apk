.class public Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;
.super Ljava/lang/Object;
.source "CustomPicPrivCheck.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static getPrivResult()Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;
    .locals 9

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 34
    .local v0, "ageGroup":Ljava/lang/String;
    :try_start_0
    const-string v3, "https://xboxlive.com"

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getAgeGroup(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    :goto_0
    const/16 v3, 0xd3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilege(I)Z

    move-result v2

    .line 41
    .local v2, "hasSharePriv":Z
    sget-object v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "getPrivResult() - hasSharePriv: %s, ageGroup: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v2, :cond_1

    .line 44
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->NO_PRIV:Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    .line 48
    :goto_1
    return-object v3

    .line 35
    .end local v2    # "hasSharePriv":Z
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get age group"

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 45
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "hasSharePriv":Z
    :cond_1
    const-string v3, "Child"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "Teen"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 46
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->CHILD:Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    goto :goto_1

    .line 48
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->SUCCESS:Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    goto :goto_1
.end method

.method public static showChildFailureDialog()V
    .locals 5

    .prologue
    .line 61
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v2, 0x7f070d22

    .line 62
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v3, 0x7f070d21

    .line 63
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v4, 0x7f0707c7

    .line 64
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 61
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 66
    return-void
.end method

.method public static showPrivFailureDialog()V
    .locals 5

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v2, 0x7f07061d

    .line 54
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v3, 0x7f070d27

    .line 55
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v4, 0x7f0707c7

    .line 56
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 53
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 58
    return-void
.end method
