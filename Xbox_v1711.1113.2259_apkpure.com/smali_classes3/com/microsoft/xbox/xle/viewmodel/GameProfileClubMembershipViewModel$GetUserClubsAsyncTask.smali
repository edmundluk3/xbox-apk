.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileClubMembershipViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUserClubsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$1;

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->loadUserClubs(J)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 167
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {v0, v2, p0, v2, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 159
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 177
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 178
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
