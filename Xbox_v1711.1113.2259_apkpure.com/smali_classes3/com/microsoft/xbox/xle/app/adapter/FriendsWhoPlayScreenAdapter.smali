.class public Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "FriendsWhoPlayScreenAdapter.java"


# instance fields
.field private friendsWhoPlayItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

.field private pageLabelTextView:Landroid/widget/TextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private useDarkStyle:Z

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->useDarkStyle:Z

    .line 31
    const v0, 0x7f0e05e0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->screenBody:Landroid/view/View;

    .line 32
    const v0, 0x7f0e05e2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    const v0, 0x7f0e05e1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->pageLabelTextView:Landroid/widget/TextView;

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .line 36
    const v0, 0x7f0e05e3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 38
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03010b

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 75
    :cond_0
    return-void
.end method

.method public setUseDarkStyle(Z)V
    .locals 1
    .param p1, "useDark"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->useDarkStyle:Z

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->setUseDarkStyle(Z)V

    .line 47
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->getShouldHideScreen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->updateLoadingIndicator(Z)V

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 85
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->useDarkStyle:Z

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->pageLabelTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->pageLabelTextView:Landroid/widget/TextView;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c0141

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 92
    .local v0, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->friendsWhoPlayItems:Ljava/util/List;

    if-eq v1, v0, :cond_0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->clear()V

    .line 95
    if-eqz v0, :cond_3

    .line 96
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayListAdapter;->addAll(Ljava/util/Collection;)V

    .line 98
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 99
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->friendsWhoPlayItems:Ljava/util/List;

    goto :goto_0
.end method
