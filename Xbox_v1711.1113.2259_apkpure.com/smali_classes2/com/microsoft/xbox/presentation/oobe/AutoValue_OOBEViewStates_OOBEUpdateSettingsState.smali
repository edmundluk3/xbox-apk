.class final Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;
.super Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
.source "AutoValue_OOBEViewStates_OOBEUpdateSettingsState.java"


# instance fields
.field private final fatalError:Z

.field private final oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;Z)V
    .locals 0
    .param p1, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "fatalError"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 18
    iput-boolean p2, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->fatalError:Z

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->fatalError:Z

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->fatalError()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 47
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    :cond_4
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public fatalError()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->fatalError:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v2

    .line 59
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->fatalError:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    xor-int/2addr v0, v1

    .line 60
    return v0

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 59
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1
.end method

.method public oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBEUpdateSettingsState{oobeSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->oobeSettings:Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;->fatalError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
