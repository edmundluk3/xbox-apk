.class Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubCreationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReserveNameTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final name:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 255
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->name:Ljava/lang/String;

    .line 256
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;

    .prologue
    .line 251
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 4

    .prologue
    .line 275
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    .line 277
    .local v0, "clubAccountsService":Lcom/microsoft/xbox/service/clubs/IClubAccountsService;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->name:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->reserveClubName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 281
    :goto_0
    return-object v2

    .line 278
    :catch_0
    move-exception v1

    .line 279
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$400()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error reserving club name"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$502(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->isSelectedClubNameValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->name:Ljava/lang/String;

    const-string v1, "Yes"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateCheckAvailableResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$600(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;->onClubNameReserved()V

    .line 298
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->name:Ljava/lang/String;

    const-string v1, "No"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateCheckAvailableResult(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 251
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ReserveNameTask;->onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method
