.class public Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;
.super Ljava/lang/Object;
.source "AuxiliaryStreamReadResult.java"


# instance fields
.field public final data:[B

.field public final readStats:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;


# direct methods
.method constructor <init>([BJJ)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "receivedBytes"    # J
    .param p4, "availableBytes"    # J

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;->data:[B

    .line 16
    new-instance v0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;-><init>(JJ)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadResult;->readStats:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;

    .line 17
    return-void
.end method
