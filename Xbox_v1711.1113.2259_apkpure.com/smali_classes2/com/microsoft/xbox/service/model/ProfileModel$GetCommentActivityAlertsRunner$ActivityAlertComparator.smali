.class Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;
.super Ljava/lang/Object;
.source "ProfileModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityAlertComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;)V
    .locals 0

    .prologue
    .line 3665
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/ProfileModel$1;

    .prologue
    .line 3665
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;)V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5
    .param p1, "object1"    # Ljava/lang/Object;
    .param p2, "object2"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 3669
    instance-of v4, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v4, :cond_1

    .line 3670
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;

    check-cast p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .end local p1    # "object1":Ljava/lang/Object;
    invoke-static {v4, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->access$4000(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;

    move-result-object v0

    .line 3678
    .local v0, "date1":Ljava/util/Date;
    :goto_0
    instance-of v4, p2, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v4, :cond_2

    .line 3679
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;->this$1:Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .end local p2    # "object2":Ljava/lang/Object;
    invoke-static {v4, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->access$4000(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;

    move-result-object v1

    .line 3686
    .local v1, "date2":Ljava/util/Date;
    :goto_1
    if-nez v0, :cond_4

    .line 3694
    .end local v0    # "date1":Ljava/util/Date;
    .end local v1    # "date2":Ljava/util/Date;
    :cond_0
    :goto_2
    return v2

    .line 3671
    .restart local p1    # "object1":Ljava/lang/Object;
    .restart local p2    # "object2":Ljava/lang/Object;
    :cond_1
    instance-of v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v4, :cond_0

    .line 3672
    check-cast p1, Lcom/microsoft/xbox/service/model/FollowersData;

    .end local p1    # "object1":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v0

    .restart local v0    # "date1":Ljava/util/Date;
    goto :goto_0

    .line 3680
    :cond_2
    instance-of v4, p2, Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v4, :cond_3

    .line 3681
    check-cast p2, Lcom/microsoft/xbox/service/model/FollowersData;

    .end local p2    # "object2":Ljava/lang/Object;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v1

    .restart local v1    # "date2":Ljava/util/Date;
    goto :goto_1

    .end local v1    # "date2":Ljava/util/Date;
    .restart local p2    # "object2":Ljava/lang/Object;
    :cond_3
    move v2, v3

    .line 3683
    goto :goto_2

    .line 3690
    .end local p2    # "object2":Ljava/lang/Object;
    .restart local v1    # "date2":Ljava/util/Date;
    :cond_4
    if-nez v1, :cond_5

    move v2, v3

    .line 3691
    goto :goto_2

    .line 3694
    :cond_5
    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v2

    neg-int v2, v2

    goto :goto_2
.end method
