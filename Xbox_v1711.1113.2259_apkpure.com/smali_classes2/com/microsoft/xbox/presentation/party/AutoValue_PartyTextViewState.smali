.class final Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;
.super Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
.source "AutoValue_PartyTextViewState.java"


# instance fields
.field private final error:Ljava/lang/Throwable;

.field private final isLoading:Z

.field private final memberStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field

.field private final partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

.field private final textStream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final userSummaries:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Lio/reactivex/Observable;Lio/reactivex/Observable;ZLjava/lang/Throwable;)V
    .locals 0
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "isLoading"    # Z
    .param p6, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;Z",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    .local p3, "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    .local p4, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    .line 31
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    .line 33
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    .line 34
    iput-boolean p5, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->isLoading:Z

    .line 35
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    if-ne p1, p0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    if-eqz v3, :cond_8

    move-object v0, p1

    .line 91
    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    .line 92
    .local v0, "that":Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_4

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    if-nez v3, :cond_5

    .line 94
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->textStream()Lio/reactivex/Observable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    if-nez v3, :cond_6

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->isLoading:Z

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->isLoading()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    if-nez v3, :cond_7

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 92
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 93
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 94
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->textStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 95
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 97
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->error()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    :cond_8
    move v1, v2

    .line 99
    goto/16 :goto_0
.end method

.method public error()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 104
    const/4 v0, 0x1

    .line 105
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v3

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v3

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 111
    mul-int/2addr v0, v3

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 113
    mul-int/2addr v0, v3

    .line 114
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->isLoading:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x4cf

    :goto_4
    xor-int/2addr v0, v1

    .line 115
    mul-int/2addr v0, v3

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    if-nez v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 117
    return v0

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_1

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 112
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    .line 114
    :cond_4
    const/16 v1, 0x4d5

    goto :goto_4

    .line 116
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->isLoading:Z

    return v0
.end method

.method public memberStream()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    return-object v0
.end method

.method public partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    return-object v0
.end method

.method public textStream()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyTextViewState{partySession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userSummaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", textStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->textStream:Lio/reactivex/Observable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memberStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->memberStream:Lio/reactivex/Observable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userSummaries()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewState;->userSummaries:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
