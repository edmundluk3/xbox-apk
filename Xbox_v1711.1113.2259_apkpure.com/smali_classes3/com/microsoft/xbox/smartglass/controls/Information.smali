.class abstract Lcom/microsoft/xbox/smartglass/controls/Information;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Information.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;
    }
.end annotation


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Information"

.field private static final GetStateMethod:Ljava/lang/String; = "GetState"

.field private static final GetUserGamercardMethod:Ljava/lang/String; = "GetUserGamercard"

.field private static final GetUserInfoMethod:Ljava/lang/String; = "GetUserInfo"

.field private static final GetUserPrivilegesMethod:Ljava/lang/String; = "GetUserPrivileges"

.field private static final LaunchTitleMethod:Ljava/lang/String; = "LaunchTitle"

.field private static final LaunchUriMethod:Ljava/lang/String; = "LaunchUri"

.field private static final TitleIdHexLength:I = 0x8

.field private static final UnsnapMethod:Ljava/lang/String; = "Unsnap"


# instance fields
.field private _activeTitleId:I

.field private _activeTitles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private _associatedChannels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private _isConnected:Z

.field private _isIdentityPaired:Z

.field private _isTitleChannelEstablished:Z

.field private final _listener:Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;

.field private final _tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 2
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    const/4 v1, 0x0

    .line 54
    const-string v0, "Information"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 56
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitleId:I

    .line 57
    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isTitleChannelEstablished:Z

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_associatedChannels:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/controls/Information$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_listener:Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_listener:Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 66
    const-string v0, "LaunchTitle"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 67
    const-string v0, "LaunchUri"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 68
    const-string v0, "Unsnap"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 69
    const-string v0, "GetUserInfo"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 70
    const-string v0, "GetUserGamercard"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 71
    const-string v0, "GetUserPrivileges"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 72
    const-string v0, "GetState"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->registerMethod(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->UpdateConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;)Z

    .line 75
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Information;->UpdatePairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;)Z

    .line 76
    return-void
.end method

.method private UpdateConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;)Z
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 230
    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v3, :cond_1

    move v1, v0

    .line 231
    .local v1, "isConnected":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 232
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/Information;->updateTitleInformation(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    .line 235
    :cond_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isConnected:Z

    if-eq v3, v1, :cond_2

    .line 236
    .local v0, "isChanged":Z
    :goto_1
    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isConnected:Z

    .line 238
    return v0

    .end local v0    # "isChanged":Z
    .end local v1    # "isConnected":Z
    :cond_1
    move v1, v2

    .line 230
    goto :goto_0

    .restart local v1    # "isConnected":Z
    :cond_2
    move v0, v2

    .line 235
    goto :goto_1
.end method

.method private UpdatePairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;)Z
    .locals 5
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 242
    sget-object v4, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->Paired:Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    if-ne p1, v4, :cond_0

    move v1, v2

    .line 243
    .local v1, "isIdentityPaired":Z
    :goto_0
    iget-boolean v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isIdentityPaired:Z

    if-eq v4, v1, :cond_1

    move v0, v2

    .line 245
    .local v0, "isChanged":Z
    :goto_1
    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isIdentityPaired:Z

    .line 246
    return v0

    .end local v0    # "isChanged":Z
    .end local v1    # "isIdentityPaired":Z
    :cond_0
    move v1, v3

    .line 242
    goto :goto_0

    .restart local v1    # "isIdentityPaired":Z
    :cond_1
    move v0, v3

    .line 243
    goto :goto_1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/Information;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/ConnectionState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/ConnectionState;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->UpdateConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/smartglass/controls/Information;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isConnected:Z

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/PairedIdentityState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->UpdatePairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/smartglass/controls/Information;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isIdentityPaired:Z

    return v0
.end method

.method private getUserPrivileges(ILcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;)V
    .locals 9
    .param p1, "requestId"    # I
    .param p2, "parameters"    # Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;

    .prologue
    const/4 v8, 0x0

    .line 154
    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;->audienceUri:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 155
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "Invalid URI."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 185
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v4

    const-string v5, "GetUserPrivileges"

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(Ljava/lang/String;I)V

    .line 162
    move v1, p1

    .line 163
    .local v1, "currentRequestId":I
    move-object v0, p2

    .line 164
    .local v0, "currentParameters":Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v4

    iget-object v5, p2, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;->audienceUri:Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Lcom/microsoft/xbox/smartglass/controls/Information$1;

    invoke-direct {v7, p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/Information$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;I)V

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/smartglass/TokenManager;->getToken(Ljava/lang/String;ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    :catch_0
    move-exception v2

    .line 181
    .local v2, "ex":Ljava/lang/Exception;
    const-string v4, "Failed to get user privileges for \'%s\'."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;->audienceUri:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 182
    .local v3, "message":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v4, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v4

    const-string v5, "GetUserPrivileges"

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/smartglass/controls/Information;->Origin:Ljava/lang/String;

    invoke-virtual {v4, v5, p1, v6, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private launchTitle(ILcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "parameters"    # Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;

    .prologue
    .line 250
    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;->titleId:I

    if-nez v3, :cond_0

    .line 251
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v4, "Invalid titleId."

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 276
    :goto_0
    return-void

    .line 255
    :cond_0
    const/4 v2, 0x0

    .line 256
    .local v2, "foundTitleId":Z
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v4, v3, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    aget v0, v4, v3

    .line 257
    .local v0, "allowedTitleId":I
    iget v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;->titleId:I

    if-ne v0, v6, :cond_2

    .line 258
    const/4 v2, 0x1

    .line 263
    .end local v0    # "allowedTitleId":I
    :cond_1
    sget-boolean v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-nez v3, :cond_3

    if-nez v2, :cond_3

    .line 264
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v4, "titleId is not in the list of allowed titleIds."

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 256
    .restart local v0    # "allowedTitleId":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 269
    .end local v0    # "allowedTitleId":I
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v3

    const-string v4, "LaunchTitle"

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v3

    iget v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;->titleId:I

    iget-object v5, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;->launchParameters:Ljava/lang/String;

    iget-object v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v3, v4, v5, v6}, Lcom/microsoft/xbox/smartglass/SessionManager;->launch(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    .line 271
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v3, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 272
    :catch_0
    move-exception v1

    .line 273
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to launch title. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private launchUri(ILcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;)V
    .locals 8
    .param p1, "requestId"    # I
    .param p2, "parameters"    # Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;

    .prologue
    .line 279
    iget-object v5, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->uri:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 280
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v6, "Invalid URI."

    invoke-interface {v5, p1, v6}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 284
    :cond_0
    sget-boolean v5, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-nez v5, :cond_3

    .line 285
    const/4 v4, 0x0

    .line 287
    .local v4, "titleId":I
    :try_start_0
    iget-object v5, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->uri:Ljava/lang/String;

    const-string v6, "ms-xbl-"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x8

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 293
    const/4 v3, 0x0

    .line 294
    .local v3, "foundTitleId":Z
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v6, v5, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedTitleIds:[I

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_1

    aget v0, v6, v5

    .line 295
    .local v0, "allowedTitleId":I
    if-ne v0, v4, :cond_2

    .line 296
    const/4 v3, 0x1

    .line 301
    .end local v0    # "allowedTitleId":I
    :cond_1
    if-nez v3, :cond_3

    .line 302
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v6, "Title is not in the list of allowed titles."

    invoke-interface {v5, p1, v6}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 288
    .end local v3    # "foundTitleId":Z
    :catch_0
    move-exception v2

    .line 289
    .local v2, "ex":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v6, "Invalid URI"

    invoke-interface {v5, p1, v6}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 294
    .end local v2    # "ex":Ljava/lang/Exception;
    .restart local v0    # "allowedTitleId":I
    .restart local v3    # "foundTitleId":Z
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 308
    .end local v0    # "allowedTitleId":I
    .end local v3    # "foundTitleId":Z
    .end local v4    # "titleId":I
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v5

    const-string v6, "LaunchUri"

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v5

    iget-object v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->uri:Ljava/lang/String;

    iget-object v7, p2, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/SessionManager;->launch(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    .line 310
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v5, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 311
    :catch_1
    move-exception v1

    .line 312
    .local v1, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to launch title. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, p1, v6}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private unsnap(I)V
    .locals 4
    .param p1, "requestId"    # I

    .prologue
    .line 318
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "Unsnap"

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/Information;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->unsnap()V

    .line 320
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to unsnap. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->dispose()V

    .line 104
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_listener:Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->removeListener(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->cancelAll()V

    .line 106
    return-void
.end method

.method public getCurrentState()Lorg/json/JSONObject;
    .locals 10

    .prologue
    .line 80
    const/4 v9, 0x0

    .line 83
    .local v9, "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->updateTitleInformation(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    .line 85
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;

    iget v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitleId:I

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    .line 86
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isConnected:Z

    iget-boolean v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isConnected:Z

    iget-boolean v5, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isIdentityPaired:Z

    iget-boolean v6, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isTitleChannelEstablished:Z

    .line 91
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPrimaryDeviceState()Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->locale:Ljava/lang/String;

    iget-object v8, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_associatedChannels:Ljava/util/ArrayList;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;-><init>(ILjava/util/Collection;ZZZZLjava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .end local v9    # "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    .local v0, "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    :goto_0
    return-object v0

    .line 94
    .end local v0    # "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    .restart local v9    # "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    :catch_0
    move-exception v1

    move-object v0, v9

    .end local v9    # "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    .restart local v0    # "currentState":Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
    goto :goto_0
.end method

.method protected abstract getUserGamercard(ILjava/lang/String;)V
.end method

.method protected getUserGamercardInternal(ILjava/lang/String;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 143
    :try_start_0
    new-instance v2, Ljava/math/BigInteger;

    invoke-direct {v2, p2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-direct {v1, p1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;-><init>(ILcom/microsoft/xbox/smartglass/controls/WebComponentContainer;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;)V

    .line 150
    .local v1, "request":Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;
    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;->send(Ljava/lang/String;)V

    .line 151
    .end local v1    # "request":Lcom/microsoft/xbox/smartglass/controls/UserGamercardRequest;
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "ex":Ljava/lang/NumberFormatException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v3, "Invalid XUID."

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Information;->validateMethod(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 111
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Information;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    :try_start_0
    const-string v3, "LaunchTitle"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 117
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;-><init>(Ljava/lang/String;)V

    .line 118
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->launchTitle(ILcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLaunchTitleParameters;
    :catch_0
    move-exception v0

    .line 136
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid JSON. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v0    # "ex":Lorg/json/JSONException;
    :cond_2
    :try_start_1
    const-string v3, "LaunchUri"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 120
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;-><init>(Ljava/lang/String;)V

    .line 121
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->launchUri(ILcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;)V

    goto :goto_0

    .line 122
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLaunchUriParameters;
    :cond_3
    const-string v3, "Unsnap"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 123
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->unsnap(I)V

    goto :goto_0

    .line 124
    :cond_4
    const-string v3, "GetUserInfo"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 125
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-direct {v2, p1, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;-><init>(ILcom/microsoft/xbox/smartglass/controls/WebComponentContainer;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;)V

    .line 126
    .local v2, "request":Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;
    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->send()V

    goto :goto_0

    .line 127
    .end local v2    # "request":Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;
    :cond_5
    const-string v3, "GetUserGamercard"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 128
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Information;->getUserGamercard(ILjava/lang/String;)V

    goto :goto_0

    .line 129
    :cond_6
    const-string v3, "GetUserPrivileges"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 130
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;-><init>(Ljava/lang/String;)V

    .line 131
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->getUserPrivileges(ILcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;)V

    goto :goto_0

    .line 132
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;
    :cond_7
    const-string v3, "GetState"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/Information;->getCurrentState()Lorg/json/JSONObject;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public updateTitleInformation(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 11
    .param p1, "currentActiveTitle"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v7, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    .line 189
    .local v7, "associatedChannels":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;>;"
    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    move v9, v3

    .line 191
    .local v9, "hasAssociatedChannels":Z
    :goto_0
    if-eqz v9, :cond_2

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v1, v1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    :goto_1
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitleId:I

    .line 192
    if-eqz v9, :cond_0

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    iget-boolean v1, v1, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->isChannelEstablished:Z

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_isTitleChannelEstablished:Z

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 195
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 197
    .local v6, "activeTitleState":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;

    iget v1, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    iget-object v2, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-boolean v3, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    iget-object v4, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->productId:Ljava/lang/String;

    iget-object v5, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->aumId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;-><init>(ILcom/microsoft/xbox/smartglass/ActiveTitleLocation;ZLjava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v0, "json":Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    iget v2, v6, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 200
    .end local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;
    :catch_0
    move-exception v1

    goto :goto_2

    .end local v6    # "activeTitleState":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    .end local v9    # "hasAssociatedChannels":Z
    :cond_1
    move v9, v2

    .line 189
    goto :goto_0

    .restart local v9    # "hasAssociatedChannels":Z
    :cond_2
    move v1, v2

    .line 191
    goto :goto_1

    .line 205
    :cond_3
    if-eqz p1, :cond_4

    .line 207
    :try_start_1
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;

    iget v1, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-boolean v3, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->productId:Ljava/lang/String;

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->aumId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;-><init>(ILcom/microsoft/xbox/smartglass/ActiveTitleLocation;ZLjava/lang/String;Ljava/lang/String;)V

    .line 209
    .restart local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_activeTitles:Ljava/util/HashMap;

    iget v2, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 214
    .end local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_associatedChannels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    monitor-enter v2

    .line 216
    :try_start_2
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->associatedChannels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;

    .line 217
    .local v8, "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219
    .local v0, "json":Lorg/json/JSONObject;
    :try_start_3
    const-string v3, "titleId"

    iget-object v4, v8, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v4, v4, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 220
    const-string v3, "isChannelEstablished"

    iget-boolean v4, v8, Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;->isChannelEstablished:Z

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 224
    :goto_5
    :try_start_4
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Information;->_associatedChannels:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 226
    .end local v0    # "json":Lorg/json/JSONObject;
    .end local v8    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :cond_5
    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 227
    return-void

    .line 221
    .restart local v0    # "json":Lorg/json/JSONObject;
    .restart local v8    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catch_1
    move-exception v3

    goto :goto_5

    .line 210
    .end local v0    # "json":Lorg/json/JSONObject;
    .end local v8    # "channel":Lcom/microsoft/xbox/smartglass/controls/AssociatedChannel;
    :catch_2
    move-exception v1

    goto :goto_3
.end method
