.class final synthetic Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field private static final instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiFunction;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/util/List;

    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->lambda$downloadReportedItems$13(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
