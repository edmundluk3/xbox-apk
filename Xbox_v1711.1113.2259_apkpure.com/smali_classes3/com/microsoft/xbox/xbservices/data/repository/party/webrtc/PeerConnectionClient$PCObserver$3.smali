.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->onIceConnectionChange(Lorg/webrtc/PeerConnection$IceConnectionState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

.field final synthetic val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/PeerConnection$IceConnectionState;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    .prologue
    .line 845
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 848
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IceConnectionState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;

    sget-object v1, Lorg/webrtc/PeerConnection$IceConnectionState;->CONNECTED:Lorg/webrtc/PeerConnection$IceConnectionState;

    if-ne v0, v1, :cond_1

    .line 850
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$900(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;->onIceConnected()V

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 851
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;

    sget-object v1, Lorg/webrtc/PeerConnection$IceConnectionState;->DISCONNECTED:Lorg/webrtc/PeerConnection$IceConnectionState;

    if-ne v0, v1, :cond_2

    .line 852
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$900(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;->onIceDisconnected()V

    goto :goto_0

    .line 853
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->val$newState:Lorg/webrtc/PeerConnection$IceConnectionState;

    sget-object v1, Lorg/webrtc/PeerConnection$IceConnectionState;->FAILED:Lorg/webrtc/PeerConnection$IceConnectionState;

    if-ne v0, v1, :cond_0

    .line 854
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    const-string v1, "ICE connection failed."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Ljava/lang/String;)V

    goto :goto_0
.end method
