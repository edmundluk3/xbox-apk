.class Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$1;
.super Ljava/lang/Object;
.source "PivotWithTabs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/CharSequence;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eval(Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "arg"    # Landroid/view/View;

    .prologue
    .line 67
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;

    .end local p1    # "arg":Landroid/view/View;
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;->getHeader()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    :goto_0
    return-object v0

    .restart local p1    # "arg":Landroid/view/View;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$1;->eval(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
