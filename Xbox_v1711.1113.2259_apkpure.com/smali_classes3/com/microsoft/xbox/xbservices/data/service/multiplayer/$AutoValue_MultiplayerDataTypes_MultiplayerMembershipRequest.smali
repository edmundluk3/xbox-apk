.class abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;
.source "$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest.java"


# instance fields
.field private final xuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null xuids"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;->xuids:Ljava/util/List;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 36
    if-ne p1, p0, :cond_0

    .line 37
    const/4 v1, 0x1

    .line 43
    :goto_0
    return v1

    .line 39
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;

    .line 41
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;->xuids:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;->xuids()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 43
    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMembershipRequest;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x1

    .line 49
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;->xuids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 51
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MultiplayerMembershipRequest{xuids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;->xuids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public xuids()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMembershipRequest;->xuids:Ljava/util/List;

    return-object v0
.end method
