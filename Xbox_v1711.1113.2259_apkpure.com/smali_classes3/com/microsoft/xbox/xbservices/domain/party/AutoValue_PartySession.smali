.class final Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
.source "AutoValue_PartySession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;
    }
.end annotation


# instance fields
.field private final multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

.field private final muted:Z

.field private final roster:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V
    .locals 0
    .param p1, "muted"    # Z
    .param p3, "multiplayerSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "roster":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;-><init>()V

    .line 20
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->muted:Z

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->roster:Lcom/google/common/collect/ImmutableList;

    .line 22
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;)V
    .locals 0
    .param p1, "x0"    # Z
    .param p2, "x1"    # Lcom/google/common/collect/ImmutableList;
    .param p3, "x2"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .param p4, "x3"    # Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;

    .prologue
    .line 10
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;-><init>(ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    .line 57
    .local v0, "that":Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->muted:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isMuted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->roster:Lcom/google/common/collect/ImmutableList;

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 59
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getMultiplayerSession()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    :cond_3
    move v1, v2

    .line 61
    goto :goto_0
.end method

.method public getMultiplayerSession()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    return-object v0
.end method

.method public getRoster()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->roster:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 66
    const/4 v0, 0x1

    .line 67
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 68
    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->muted:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 69
    mul-int/2addr v0, v2

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->roster:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 71
    mul-int/2addr v0, v2

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 73
    return v0

    .line 68
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public isMuted()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->muted:Z

    return v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartySession{muted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->muted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roster="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->roster:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", multiplayerSession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
