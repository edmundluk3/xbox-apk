.class public final enum Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;
.super Ljava/lang/Enum;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RecentsState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

.field public static final enum PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

.field public static final enum RECENTS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    const-string v1, "RECENTS"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->RECENTS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    const-string v1, "PROMO"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    .line 726
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->RECENTS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 726
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;
    .locals 1

    .prologue
    .line 726
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    return-object v0
.end method
