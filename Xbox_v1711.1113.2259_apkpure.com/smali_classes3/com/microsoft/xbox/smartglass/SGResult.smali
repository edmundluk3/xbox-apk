.class public Lcom/microsoft/xbox/smartglass/SGResult;
.super Ljava/lang/Object;
.source "SGResult.java"


# instance fields
.field public final error:Lcom/microsoft/xbox/smartglass/SGError;

.field public final innerError:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "independentError"    # I
    .param p2, "platformError"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/SGError;->fromInt(I)Lcom/microsoft/xbox/smartglass/SGError;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    .line 21
    iput p2, p0, Lcom/microsoft/xbox/smartglass/SGResult;->innerError:I

    .line 22
    return-void
.end method


# virtual methods
.method public isFailure()Z
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SGResult;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
