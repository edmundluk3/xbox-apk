.class public final Lcom/microsoft/xbox/xle/app/glide/ResizeUrlLoaderFactory;
.super Ljava/lang/Object;
.source "ResizeUrlLoaderFactory.java"

# interfaces
.implements Lcom/bumptech/glide/load/model/ModelLoaderFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/bumptech/glide/load/model/ModelLoaderFactory",
        "<",
        "Ljava/lang/String;",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# static fields
.field private static final URL_CACHE:Lcom/bumptech/glide/load/model/ModelCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bumptech/glide/load/model/ModelCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/bumptech/glide/load/model/GlideUrl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/bumptech/glide/load/model/ModelCache;

    const/16 v1, 0x96

    invoke-direct {v0, v1}, Lcom/bumptech/glide/load/model/ModelCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlLoaderFactory;->URL_CACHE:Lcom/bumptech/glide/load/model/ModelCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Landroid/content/Context;Lcom/bumptech/glide/load/model/GenericLoaderFactory;)Lcom/bumptech/glide/load/model/ModelLoader;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "factories"    # Lcom/bumptech/glide/load/model/GenericLoaderFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/bumptech/glide/load/model/GenericLoaderFactory;",
            ")",
            "Lcom/bumptech/glide/load/model/ModelLoader",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;

    sget-object v1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlLoaderFactory;->URL_CACHE:Lcom/bumptech/glide/load/model/ModelCache;

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader;-><init>(Landroid/content/Context;Lcom/bumptech/glide/load/model/ModelCache;)V

    return-object v0
.end method

.method public teardown()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method
