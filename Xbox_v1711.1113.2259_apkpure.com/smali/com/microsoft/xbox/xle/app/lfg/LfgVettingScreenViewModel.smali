.class public Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "LfgVettingScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

.field private final getStatsResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;"
        }
    .end annotation
.end field

.field private getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

.field private interestedMembers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;"
        }
    .end annotation
.end field

.field multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

.field private sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private timePlayedValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private titleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "parentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 77
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)V

    .line 79
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 80
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    .line 82
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 83
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->timePlayedValue:Ljava/util/Map;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 86
    .local v0, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->titleId:J

    .line 87
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMultiplayerHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 88
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMultiplayerSession()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getLfgVettingScreenAdapter(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 90
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->onSessionLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->onUserStatsLoadCompleted(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V

    return-void
.end method

.method private declared-synchronized onSessionLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "session"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .prologue
    .line 338
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 352
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    monitor-exit p0

    return-void

    .line 342
    :pswitch_0
    if-eqz p2, :cond_0

    .line 343
    :try_start_1
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 348
    :pswitch_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 338
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "onUserDataLoadCompleted"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 278
    .local v3, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 275
    .end local v3    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 281
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 282
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v0

    .line 283
    .local v0, "interestedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 284
    .local v2, "memberIndex":Ljava/lang/String;
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 286
    .local v1, "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 287
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    new-instance v8, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->timePlayedValue:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-direct {v8, v2, v1, v4, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 291
    .end local v1    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v2    # "memberIndex":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized onUserStatsLoadCompleted(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V
    .locals 10
    .param p1, "result"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onUserStatsLoadCompleted "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 299
    .local v2, "memberHeroStats":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;>;>;"
    if-eqz p1, :cond_1

    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 300
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 301
    .local v0, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->name:Ljava/lang/String;

    sget-object v7, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->Hero:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 302
    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 303
    .local v4, "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v7, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->arrangebyfieldid:Ljava/lang/String;

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 295
    .end local v0    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    .end local v2    # "memberHeroStats":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;>;>;"
    .end local v4    # "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 309
    .restart local v2    # "memberHeroStats":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;>;>;"
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 310
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    .line 311
    .local v1, "member":Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->addHeroStats(Ljava/util/List;)V

    goto :goto_1

    .line 315
    .end local v1    # "member":Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
    :cond_2
    if-eqz p1, :cond_6

    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 316
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 317
    .restart local v4    # "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v4, :cond_3

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 318
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 319
    .local v3, "stat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    if-eqz v3, :cond_4

    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v8, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 320
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->timePlayedValue:Ljava/util/Map;

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    .line 323
    .restart local v1    # "member":Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
    iget-object v8, v1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 324
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->timePlayedValue:Ljava/util/Map;

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->setTimePlayed(Ljava/lang/Integer;)V

    goto :goto_2

    .line 334
    .end local v1    # "member":Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
    .end local v3    # "stat":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v4    # "statistics":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V
    .locals 10
    .param p1, "session"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v1, "interestedXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 235
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v0

    .line 236
    .local v0, "interestedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 237
    .local v3, "memberIndex":Ljava/lang/String;
    const-string v4, "0"

    invoke-static {v4, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 238
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 240
    .local v2, "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;

    move-result-object v4

    if-nez v4, :cond_0

    .line 241
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 242
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 230
    .end local v0    # "interestedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .end local v1    # "interestedXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v3    # "memberIndex":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 244
    .restart local v0    # "interestedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;>;"
    .restart local v1    # "interestedXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .restart local v3    # "memberIndex":Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    new-instance v8, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->timePlayedValue:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-direct {v8, v3, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 250
    .end local v2    # "member":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
    .end local v3    # "memberIndex":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v4, :cond_3

    .line 251
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 254
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 255
    new-instance v4, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v4, v1, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 256
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 258
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    if-eqz v4, :cond_4

    .line 259
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->cancel()V

    .line 262
    :cond_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    new-instance v5, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;

    sget-object v6, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->Hero:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    .line 264
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v6

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->titleId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;

    sget-object v7, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    .line 265
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->titleId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v4, v1, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    .line 267
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->load(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    :goto_1
    monitor-exit p0

    return-void

    .line 270
    :cond_5
    :try_start_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public approveMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 3
    .param p1, "interestedMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 155
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionReference()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackApproveMember(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getHandleId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberIndex:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->approveLfgMember(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V

    .line 160
    return-void
.end method

.method public declineMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 3
    .param p1, "interestedMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 163
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionReference()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackDeclineMember(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getHandleId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberIndex:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->declineLfgMember(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V

    .line 168
    return-void
.end method

.method public getHandleId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->searchHandle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getInterestedList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->interestedMembers:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNeedCount()I
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->getNeedCount()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->members()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getInterestedList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getSessionReference()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    return-object v0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getIsModifyingSession()Z

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    .line 122
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    .line 123
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    .line 124
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getIsModifyingSession()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    .line 124
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 134
    return-void
.end method

.method public navigateToEnforcement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "displayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 178
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 179
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 182
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->LfgUser:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getHandleId()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->sessionReference:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)V

    .line 183
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 184
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 99
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getLfgVettingScreenAdapter(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 100
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 95
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getStatsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->cancel()V

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->cancel()V

    .line 117
    :cond_2
    return-void
.end method

.method public onTapCloseButton()V
    .locals 0

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->goBack()V

    .line 152
    return-void
.end method

.method public reportMember(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 2
    .param p1, "interestedMember"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 171
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 173
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionReference()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackReportMember(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Ljava/lang/String;)V

    .line 174
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->navigateToEnforcement(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 190
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 193
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 221
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown update type ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 227
    return-void

    .line 196
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Completed call to joinLfgSession or declineLFGSession"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 218
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 201
    :pswitch_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getContext()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v2, :cond_1

    .line 202
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getContext()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->updateSession(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    goto :goto_1

    .line 207
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Call failed, refreshing session"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    if-eqz v2, :cond_2

    .line 210
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->cancel()V

    .line 213
    :cond_2
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->searchHandle()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    .line 214
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->getSessionTask:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->load(Z)V

    goto :goto_1

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 197
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
