.class Lcom/microsoft/xbox/xle/ui/EPGView$1;
.super Ljava/lang/Object;
.source "EPGView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGView;->addVerticalScrollAccelerator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollAcceleratorEvent(F)V
    .locals 5
    .param p1, "touchPosition"    # F

    .prologue
    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannelCount()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    sub-int v1, v2, v3

    .line 183
    .local v1, "scrollableRows":I
    int-to-float v2, v1

    mul-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 184
    .local v0, "row":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->scrollToRow(I)V

    .line 185
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->saveStartingRow(I)V

    .line 186
    return-void
.end method
