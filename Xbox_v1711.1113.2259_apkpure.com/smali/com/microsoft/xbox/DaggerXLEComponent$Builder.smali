.class public final Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerXLEComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/DaggerXLEComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private beamServiceModule:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

.field private dataModule:Lcom/microsoft/xbox/data/DataModule;

.field private editorialServiceModule:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

.field private gameServerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

.field private hoverChatModule:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

.field private mediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

.field private modelManagerModule:Lcom/microsoft/xbox/domain/ModelManagerModule;

.field private multiplayerModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

.field private multiplayerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

.field private oOBEServiceModule:Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

.field private peopleHubServiceModule:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

.field private privacyServiceModule:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

.field private profileColorServiceModule:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

.field private releaseMediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

.field private releaseServiceModule:Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

.field private repositoryModule:Lcom/microsoft/xbox/data/repository/RepositoryModule;

.field private serviceModule:Lcom/microsoft/xbox/data/service/ServiceModule;

.field private toolkitModule:Lcom/microsoft/xbox/toolkit/ToolkitModule;

.field private xLEAppModule:Lcom/microsoft/xbox/XLEAppModule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/DaggerXLEComponent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$1;

    .prologue
    .line 1974
    invoke-direct {p0}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->mediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->peopleHubServiceModule:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->privacyServiceModule:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->gameServerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->oOBEServiceModule:Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->profileColorServiceModule:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->editorialServiceModule:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->hoverChatModule:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/toolkit/ToolkitModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->toolkitModule:Lcom/microsoft/xbox/toolkit/ToolkitModule;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ReleaseServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseServiceModule:Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->serviceModule:Lcom/microsoft/xbox/data/service/ServiceModule;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseMediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->xLEAppModule:Lcom/microsoft/xbox/XLEAppModule;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->repositoryModule:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->beamServiceModule:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/domain/ModelManagerModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->modelManagerModule:Lcom/microsoft/xbox/domain/ModelManagerModule;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/DataModule;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->dataModule:Lcom/microsoft/xbox/data/DataModule;

    return-object v0
.end method


# virtual methods
.method public beamServiceModule(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "beamServiceModule"    # Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .prologue
    .line 2107
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->beamServiceModule:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .line 2108
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/XLEComponent;
    .locals 3

    .prologue
    .line 2016
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->mediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    if-nez v0, :cond_0

    .line 2017
    new-instance v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->mediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseServiceModule:Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    if-nez v0, :cond_1

    .line 2020
    new-instance v0, Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/ReleaseServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseServiceModule:Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    .line 2022
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->serviceModule:Lcom/microsoft/xbox/data/service/ServiceModule;

    if-nez v0, :cond_2

    .line 2023
    new-instance v0, Lcom/microsoft/xbox/data/service/ServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/ServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->serviceModule:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 2025
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseMediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    if-nez v0, :cond_3

    .line 2026
    new-instance v0, Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseMediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    .line 2028
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->xLEAppModule:Lcom/microsoft/xbox/XLEAppModule;

    if-nez v0, :cond_4

    .line 2029
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/microsoft/xbox/XLEAppModule;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2031
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->repositoryModule:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    if-nez v0, :cond_5

    .line 2032
    new-instance v0, Lcom/microsoft/xbox/data/repository/RepositoryModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/repository/RepositoryModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->repositoryModule:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 2034
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->beamServiceModule:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    if-nez v0, :cond_6

    .line 2035
    new-instance v0, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->beamServiceModule:Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    .line 2037
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->modelManagerModule:Lcom/microsoft/xbox/domain/ModelManagerModule;

    if-nez v0, :cond_7

    .line 2038
    new-instance v0, Lcom/microsoft/xbox/domain/ModelManagerModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/domain/ModelManagerModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->modelManagerModule:Lcom/microsoft/xbox/domain/ModelManagerModule;

    .line 2040
    :cond_7
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->dataModule:Lcom/microsoft/xbox/data/DataModule;

    if-nez v0, :cond_8

    .line 2041
    new-instance v0, Lcom/microsoft/xbox/data/DataModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/DataModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->dataModule:Lcom/microsoft/xbox/data/DataModule;

    .line 2043
    :cond_8
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->peopleHubServiceModule:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    if-nez v0, :cond_9

    .line 2044
    new-instance v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->peopleHubServiceModule:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .line 2046
    :cond_9
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    if-nez v0, :cond_a

    .line 2047
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .line 2049
    :cond_a
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->privacyServiceModule:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    if-nez v0, :cond_b

    .line 2050
    new-instance v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->privacyServiceModule:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    .line 2052
    :cond_b
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->gameServerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    if-nez v0, :cond_c

    .line 2053
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->gameServerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    .line 2055
    :cond_c
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->oOBEServiceModule:Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    if-nez v0, :cond_d

    .line 2056
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->oOBEServiceModule:Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    .line 2058
    :cond_d
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->profileColorServiceModule:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    if-nez v0, :cond_e

    .line 2059
    new-instance v0, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->profileColorServiceModule:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    .line 2061
    :cond_e
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    if-nez v0, :cond_f

    .line 2062
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    .line 2064
    :cond_f
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->editorialServiceModule:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    if-nez v0, :cond_10

    .line 2065
    new-instance v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->editorialServiceModule:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    .line 2067
    :cond_10
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->hoverChatModule:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    if-nez v0, :cond_11

    .line 2068
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->hoverChatModule:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .line 2070
    :cond_11
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->toolkitModule:Lcom/microsoft/xbox/toolkit/ToolkitModule;

    if-nez v0, :cond_12

    .line 2071
    new-instance v0, Lcom/microsoft/xbox/toolkit/ToolkitModule;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ToolkitModule;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->toolkitModule:Lcom/microsoft/xbox/toolkit/ToolkitModule;

    .line 2073
    :cond_12
    new-instance v0, Lcom/microsoft/xbox/DaggerXLEComponent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/DaggerXLEComponent;-><init>(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;Lcom/microsoft/xbox/DaggerXLEComponent$1;)V

    return-object v0
.end method

.method public dataModule(Lcom/microsoft/xbox/data/DataModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "dataModule"    # Lcom/microsoft/xbox/data/DataModule;

    .prologue
    .line 2162
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/DataModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->dataModule:Lcom/microsoft/xbox/data/DataModule;

    .line 2163
    return-object p0
.end method

.method public editorialServiceModule(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "editorialServiceModule"    # Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    .prologue
    .line 2112
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->editorialServiceModule:Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    .line 2113
    return-object p0
.end method

.method public gameServerServiceModule(Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "gameServerServiceModule"    # Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    .prologue
    .line 2117
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->gameServerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    .line 2118
    return-object p0
.end method

.method public hoverChatModule(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "hoverChatModule"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .prologue
    .line 2082
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->hoverChatModule:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .line 2083
    return-object p0
.end method

.method public mediaHubServiceModule(Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "mediaHubServiceModule"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    .prologue
    .line 2178
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->mediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    .line 2179
    return-object p0
.end method

.method public modelManagerModule(Lcom/microsoft/xbox/domain/ModelManagerModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "modelManagerModule"    # Lcom/microsoft/xbox/domain/ModelManagerModule;

    .prologue
    .line 2092
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/ModelManagerModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->modelManagerModule:Lcom/microsoft/xbox/domain/ModelManagerModule;

    .line 2093
    return-object p0
.end method

.method public multiplayerModule(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "multiplayerModule"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .prologue
    .line 2122
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .line 2123
    return-object p0
.end method

.method public multiplayerServiceModule(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "multiplayerServiceModule"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    .prologue
    .line 2127
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->multiplayerServiceModule:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    .line 2128
    return-object p0
.end method

.method public oOBEServiceModule(Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "oOBEServiceModule"    # Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    .prologue
    .line 2132
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->oOBEServiceModule:Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    .line 2133
    return-object p0
.end method

.method public peopleHubServiceModule(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "peopleHubServiceModule"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .prologue
    .line 2137
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->peopleHubServiceModule:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    .line 2138
    return-object p0
.end method

.method public privacyServiceModule(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "privacyServiceModule"    # Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    .prologue
    .line 2142
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->privacyServiceModule:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    .line 2143
    return-object p0
.end method

.method public profileColorServiceModule(Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "profileColorServiceModule"    # Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    .prologue
    .line 2147
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->profileColorServiceModule:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    .line 2148
    return-object p0
.end method

.method public releaseDataModule(Lcom/microsoft/xbox/data/ReleaseDataModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 0
    .param p1, "releaseDataModule"    # Lcom/microsoft/xbox/data/ReleaseDataModule;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2157
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2158
    return-object p0
.end method

.method public releaseMediaHubServiceModule(Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "releaseMediaHubServiceModule"    # Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    .prologue
    .line 2173
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseMediaHubServiceModule:Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    .line 2174
    return-object p0
.end method

.method public releaseServiceModule(Lcom/microsoft/xbox/data/service/ReleaseServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "releaseServiceModule"    # Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    .prologue
    .line 2097
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->releaseServiceModule:Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    .line 2098
    return-object p0
.end method

.method public repositoryModule(Lcom/microsoft/xbox/data/repository/RepositoryModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "repositoryModule"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .prologue
    .line 2167
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/RepositoryModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->repositoryModule:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 2168
    return-object p0
.end method

.method public serviceModule(Lcom/microsoft/xbox/data/service/ServiceModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "serviceModule"    # Lcom/microsoft/xbox/data/service/ServiceModule;

    .prologue
    .line 2102
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/ServiceModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->serviceModule:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 2103
    return-object p0
.end method

.method public toolkitModule(Lcom/microsoft/xbox/toolkit/ToolkitModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "toolkitModule"    # Lcom/microsoft/xbox/toolkit/ToolkitModule;

    .prologue
    .line 2087
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ToolkitModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->toolkitModule:Lcom/microsoft/xbox/toolkit/ToolkitModule;

    .line 2088
    return-object p0
.end method

.method public xLEAppModule(Lcom/microsoft/xbox/XLEAppModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 1
    .param p1, "xLEAppModule"    # Lcom/microsoft/xbox/XLEAppModule;

    .prologue
    .line 2077
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/XLEAppModule;

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->xLEAppModule:Lcom/microsoft/xbox/XLEAppModule;

    .line 2078
    return-object p0
.end method
