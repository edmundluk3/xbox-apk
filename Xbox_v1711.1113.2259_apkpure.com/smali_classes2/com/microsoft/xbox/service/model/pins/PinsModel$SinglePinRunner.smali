.class Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;
.super Ljava/lang/Object;
.source "PinsModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/pins/PinsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SinglePinRunner"
.end annotation


# instance fields
.field private final doneSignal:Ljava/util/concurrent/CountDownLatch;

.field private item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/epg/EPListItem;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/epg/EPListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "doneSignal"    # Ljava/util/concurrent/CountDownLatch;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 252
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 254
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 255
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    .line 256
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 261
    :try_start_0
    sget-object v4, Lcom/microsoft/xbox/service/model/pins/PinsModel$1;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV3MediaType:[I

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/epg/EPItem;->ContentType:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->ordinal()I

    move-result v5

    aget v4, v4, v5
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    packed-switch v4, :pswitch_data_0

    .line 296
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 298
    :goto_1
    return-void

    .line 277
    :pswitch_0
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$400(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/epg/EPListItem;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v2

    .line 279
    .local v2, "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    if-eqz v2, :cond_0

    .line 280
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    .line 281
    .local v3, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getEPListPinItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v1

    .line 282
    .local v1, "fetchedPin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    iget v4, v4, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Index:I

    iput v4, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->index:I

    .line 283
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v5

    monitor-enter v5
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 284
    :try_start_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 293
    .end local v1    # "fetchedPin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    .end local v2    # "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v3    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :catch_0
    move-exception v0

    .line 294
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_4
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$600()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unable to fetch item data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPItem;->ProviderId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->item:Lcom/microsoft/xbox/service/model/epg/EPListItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - Code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 296
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_1
    move-exception v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v4

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
