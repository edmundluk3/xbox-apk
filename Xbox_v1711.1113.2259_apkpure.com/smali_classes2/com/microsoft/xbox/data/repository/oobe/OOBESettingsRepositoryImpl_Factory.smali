.class public final Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;
.super Ljava/lang/Object;
.source "OOBESettingsRepositoryImpl_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p2, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 24
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 26
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "sharedPreferencesProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/SharedPreferences;>;"
    .local p1, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;
    .locals 3

    .prologue
    .line 30
    new-instance v2, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 31
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;

    invoke-direct {v2, v0, v1}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;-><init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper;)V

    .line 30
    return-object v2
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl_Factory;->get()Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepositoryImpl;

    move-result-object v0

    return-object v0
.end method
