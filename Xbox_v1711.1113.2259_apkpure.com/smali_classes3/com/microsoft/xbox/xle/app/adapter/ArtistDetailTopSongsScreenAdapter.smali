.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "ArtistDetailTopSongsScreenAdapter.java"


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V
    .locals 5
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 26
    const v1, 0x7f0e01e3

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->screenBody:Landroid/view/View;

    .line 27
    const v1, 0x7f0e01e8

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    .line 30
    const v1, 0x7f0e01e9

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 32
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f030209

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 35
    const v1, 0x7f0e01e4

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 37
    .local v0, "playButton":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 38
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->playTopSongs()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->playTrack(I)V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 48
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 55
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 56
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->items:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->getTopTracks()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->getTopTracks()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->items:Ljava/util/List;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;->clear()V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 72
    :cond_1
    return-void
.end method
