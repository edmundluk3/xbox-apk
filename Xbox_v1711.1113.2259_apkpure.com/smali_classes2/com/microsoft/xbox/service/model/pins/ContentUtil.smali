.class public final Lcom/microsoft/xbox/service/model/pins/ContentUtil;
.super Ljava/lang/Object;
.source "ContentUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    }
.end annotation


# static fields
.field public static final DAPP:Ljava/lang/String; = "DApp"

.field public static final DEVICE_TYPE_XBOX_ONE:Ljava/lang/String; = "XboxOne"

.field public static final DGAME:Ljava/lang/String; = "DGame"

.field public static final DGameDemo:Ljava/lang/String; = "DGameDemo"

.field public static final MUSIC_PLAYLIST:Ljava/lang/String; = "MusicPlaylist"

.field public static final PROVIDER_LIVE_TV:Ljava/lang/String; = "LiveTV"

.field public static final PROVIDER_TV:Ljava/lang/String; = "TV"

.field public static final PROVIDER_VIDEO:Ljava/lang/String; = "Video"

.field public static final PROVIDER_X13_VIDEO:Ljava/lang/String; = "X13-Video"

.field public static final PROVIDER_XBOX_VIDEO:Ljava/lang/String; = "Xbox Video"

.field private static final TAG:Ljava/lang/String;

.field public static final TVCHANNEL:Ljava/lang/String; = "TVChannel"

.field public static final WEB_LINK:Ljava/lang/String; = "WebLink"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static computeHasState(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 337
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isLiveTV(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isOneGuide(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 338
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_ONEGUIDE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .line 344
    .local v0, "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :goto_0
    return-object v0

    .line 339
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_1
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasCompanion()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 340
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_COMPANION:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0

    .line 342
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_REMOTE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0
.end method

.method public static computeHasStateForBat(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 349
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isLiveTV(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_ONEGUIDE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .line 360
    .local v0, "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :goto_0
    return-object v0

    .line 351
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_0
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasCompanion()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_COMPANION:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0

    .line 353
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_1
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 354
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_REMOTE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0

    .line 355
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_2
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasDetails()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 356
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_NOTHING:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0

    .line 358
    .end local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_REMOTE:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .restart local v0    # "ret":Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    goto :goto_0
.end method

.method public static convertToActiveIfNowPlaying(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 327
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->findNowPlayingModel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 328
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-nez v0, :cond_0

    .end local p0    # "li":Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    :goto_0
    return-object p0

    .restart local p0    # "li":Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public static findNowPlayingModel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 10
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 294
    const/4 v2, 0x0

    .line 295
    .local v2, "ret":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->hasProvider()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 296
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 297
    .local v1, "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v1, :cond_1

    .line 299
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 300
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getItemId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getItemId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 301
    sget-object v5, Lcom/microsoft/xbox/service/model/pins/ContentUtil$1;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 299
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 307
    :pswitch_0
    move-object v2, v0

    .line 317
    .end local v0    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v1    # "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    return-object v2

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getMediaTypeResourceId(I)I
    .locals 3
    .param p0, "mediaType"    # I

    .prologue
    .line 254
    sparse-switch p0, :sswitch_data_0

    .line 282
    sget-object v0, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported media type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 257
    :sswitch_0
    const v0, 0x7f070407

    goto :goto_0

    .line 261
    :sswitch_1
    const v0, 0x7f070411

    goto :goto_0

    .line 264
    :sswitch_2
    const v0, 0x7f070413

    goto :goto_0

    .line 267
    :sswitch_3
    const v0, 0x7f07041b

    goto :goto_0

    .line 271
    :sswitch_4
    const v0, 0x7f0703ec

    goto :goto_0

    .line 274
    :sswitch_5
    const v0, 0x7f070405

    goto :goto_0

    .line 276
    :sswitch_6
    const v0, 0x7f07042e

    goto :goto_0

    .line 278
    :sswitch_7
    const v0, 0x7f070409

    goto :goto_0

    .line 254
    nop

    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_4
        0x3ec -> :sswitch_4
        0x3ed -> :sswitch_4
        0x3ee -> :sswitch_5
        0x3ef -> :sswitch_6
        0x3f1 -> :sswitch_7
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_1
    .end sparse-switch
.end method

.method public static getShouldShowBackgroundColor(Ljava/lang/String;)Z
    .locals 1
    .param p0, "contentType"    # Ljava/lang/String;

    .prologue
    .line 332
    if-eqz p0, :cond_0

    const-string v0, "DApp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasDetails(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 1
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 4
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 74
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 90
    const-string v0, "DApp"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 86
    const-string v0, "DGame"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DGameDemo"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLiveTV(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 4
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 98
    const-wide/32 v0, 0x162615ad

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 78
    const-string v0, "MusicPlaylist"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isOneGuide(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 4
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 106
    const-wide/32 v0, 0x7a8015dd

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSnappable(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 6
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 62
    const/4 v2, 0x0

    .line 63
    .local v2, "ret":Z
    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v0

    .line 64
    .local v0, "providerTitleId":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v3

    .line 66
    .local v3, "snapModel":Lcom/microsoft/xbox/service/model/SnappableAppsModel;
    invoke-virtual {v3, v0, v1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->isSnappable(J)Z

    move-result v2

    .line 70
    .end local v3    # "snapModel":Lcom/microsoft/xbox/service/model/SnappableAppsModel;
    :goto_0
    return v2

    .line 68
    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 94
    const-string v0, "TVChannel"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isVideo(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 4
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 102
    const-wide/32 v0, 0x3d705025

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 82
    const-string v0, "WebLink"

    invoke-interface {p0}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static titleIdToProvider(J)Ljava/lang/String;
    .locals 6
    .param p0, "titleId"    # J

    .prologue
    .line 116
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "0x%08x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toMediaItem(Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 6
    .param p0, "pi"    # Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    const-wide/16 v4, 0x0

    .line 126
    const/4 v0, 0x0

    .line 127
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz p0, :cond_4

    .line 128
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getBingId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 131
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    const-string v2, "ContentUtil"

    const-string v3, "the bing id for the media item is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_0
    const-string v2, "PackageFamilyName"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIdType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setPackageFamilyName(Ljava/lang/String;)V

    .line 139
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderMediaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setPartnerMediaId(Ljava/lang/String;)V

    .line 140
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 141
    .local v1, "titleId":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 142
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    .line 144
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 147
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 148
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 151
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 152
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 156
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 157
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 160
    .end local v1    # "titleId":Ljava/lang/Long;
    :cond_4
    return-object v0

    .line 154
    .restart local v1    # "titleId":Ljava/lang/Long;
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 2
    .param p0, "ri"    # Lcom/microsoft/xbox/service/model/recents/RecentItem;

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz p0, :cond_0

    .line 172
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_0

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 175
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 180
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 184
    :cond_0
    return-object v0

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method private static toMediaItem(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 4
    .param p0, "contentType"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x232b

    .line 189
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 247
    sget-object v1, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported content type for details "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    .line 250
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :goto_0
    return-object v0

    .line 192
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_0
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>()V

    .line 193
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 198
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_1
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 199
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const/16 v1, 0x2329

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 202
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_2
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 203
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    const/16 v1, 0x232a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 206
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_3
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;-><init>()V

    .line 207
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 210
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_4
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;-><init>()V

    .line 211
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 215
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_5
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V

    .line 216
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 219
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_6
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;-><init>()V

    .line 220
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 224
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_7
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;-><init>()V

    .line 225
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 227
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_8
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;-><init>()V

    .line 228
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 231
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_9
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;-><init>()V

    .line 232
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 236
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_a
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;-><init>()V

    .line 237
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 239
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_b
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;-><init>()V

    .line 240
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 243
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :sswitch_c
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;-><init>()V

    .line 244
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    goto :goto_0

    .line 189
    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_5
        0x3e8 -> :sswitch_6
        0x3ea -> :sswitch_7
        0x3eb -> :sswitch_7
        0x3ec -> :sswitch_8
        0x3ed -> :sswitch_9
        0x3ee -> :sswitch_a
        0x3ef -> :sswitch_b
        0x3f1 -> :sswitch_c
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_1
        0x232a -> :sswitch_2
        0x232b -> :sswitch_3
        0x232c -> :sswitch_4
    .end sparse-switch
.end method
