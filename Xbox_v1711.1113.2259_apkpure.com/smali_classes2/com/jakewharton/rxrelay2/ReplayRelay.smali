.class public final Lcom/jakewharton/rxrelay2/ReplayRelay;
.super Lcom/jakewharton/rxrelay2/Relay;
.source "ReplayRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$Node;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;,
        Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jakewharton/rxrelay2/Relay",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final EMPTY:[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

.field private static final EMPTY_ARRAY:[Ljava/lang/Object;


# instance fields
.field final buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer",
            "<TT;>;"
        }
    .end annotation
.end field

.field final observers:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    new-array v0, v1, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    sput-object v0, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 242
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "buffer":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer<TT;>;"
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/Relay;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    .line 193
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    .line 194
    return-void
.end method

.method public static create()Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v1, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v0
.end method

.method public static create(I)Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 2
    .param p0, "capacityHint"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v1, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;

    invoke-direct {v1, p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$UnboundedReplayBuffer;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v0
.end method

.method static createUnbounded()Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v1, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;

    const v2, 0x7fffffff

    invoke-direct {v1, v2}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v0
.end method

.method public static createWithSize(I)Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 2
    .param p0, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v1, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;

    invoke-direct {v1, p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeBoundReplayBuffer;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v0
.end method

.method public static createWithTime(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 8
    .param p0, "maxAge"    # J
    .param p2, "unit"    # Ljava/util/concurrent/TimeUnit;
    .param p3, "scheduler"    # Lio/reactivex/Scheduler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v6, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;

    const v1, 0x7fffffff

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;-><init>(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V

    invoke-direct {v6, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v6
.end method

.method public static createWithTimeAndSize(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;I)Lcom/jakewharton/rxrelay2/ReplayRelay;
    .locals 8
    .param p0, "maxAge"    # J
    .param p2, "unit"    # Ljava/util/concurrent/TimeUnit;
    .param p3, "scheduler"    # Lio/reactivex/Scheduler;
    .param p4, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            "I)",
            "Lcom/jakewharton/rxrelay2/ReplayRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v6, Lcom/jakewharton/rxrelay2/ReplayRelay;

    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;

    move v1, p4

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;-><init>(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V

    invoke-direct {v6, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay;-><init>(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;)V

    return-object v6
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "value == null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    .line 217
    .local v0, "b":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer<TT;>;"
    invoke-interface {v0, p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->add(Ljava/lang/Object;)V

    .line 219
    iget-object v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v2, v3

    .line 220
    .local v1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-interface {v0, v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->replay(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V

    .line 219
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 222
    .end local v1    # "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    :cond_1
    return-void
.end method

.method add(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    const/4 v4, 0x0

    .line 284
    :cond_0
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 285
    .local v0, "a":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    array-length v2, v0

    .line 287
    .local v2, "len":I
    add-int/lit8 v3, v2, 0x1

    new-array v1, v3, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 288
    .local v1, "b":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    aput-object p1, v1, v2

    .line 290
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    const/4 v3, 0x1

    return v3
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 238
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    invoke-interface {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->getValue()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValues()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 250
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    sget-object v0, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 251
    .local v0, "a":[Ljava/lang/Object;, "[TT;"
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay;->getValues([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .line 252
    .local v1, "b":[Ljava/lang/Object;, "[TT;"
    sget-object v2, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    if-ne v1, v2, :cond_0

    .line 253
    const/4 v2, 0x0

    new-array v1, v2, [Ljava/lang/Object;

    .line 255
    .end local v1    # "b":[Ljava/lang/Object;, "[TT;"
    :cond_0
    return-object v1
.end method

.method public getValues([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    invoke-interface {v0, p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->getValues([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasObservers()Z
    .locals 1

    .prologue
    .line 226
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 275
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    invoke-interface {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method observerCount()I
    .locals 1

    .prologue
    .line 230
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    array-length v0, v0

    return v0
.end method

.method remove(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    const/4 v7, 0x0

    .line 299
    :cond_0
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 300
    .local v0, "a":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    sget-object v5, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    if-ne v0, v5, :cond_2

    .line 324
    :cond_1
    :goto_0
    return-void

    .line 303
    :cond_2
    array-length v4, v0

    .line 304
    .local v4, "len":I
    const/4 v3, -0x1

    .line 305
    .local v3, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 306
    aget-object v5, v0, v2

    if-ne v5, p1, :cond_4

    .line 307
    move v3, v2

    .line 312
    :cond_3
    if-ltz v3, :cond_1

    .line 316
    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 317
    sget-object v1, Lcom/jakewharton/rxrelay2/ReplayRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 323
    .local v1, "b":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    :goto_2
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->observers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 305
    .end local v1    # "b":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 319
    :cond_5
    add-int/lit8 v5, v4, -0x1

    new-array v1, v5, [Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    .line 320
    .restart local v1    # "b":[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "[Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-static {v0, v7, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 321
    add-int/lit8 v5, v3, 0x1

    sub-int v6, v4, v3

    add-int/lit8 v6, v6, -0x1

    invoke-static {v0, v5, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method size()I
    .locals 1

    .prologue
    .line 279
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    invoke-interface {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->size()I

    move-result v0

    return v0
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay;, "Lcom/jakewharton/rxrelay2/ReplayRelay<TT;>;"
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;

    invoke-direct {v0, p1, p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;-><init>(Lio/reactivex/Observer;Lcom/jakewharton/rxrelay2/ReplayRelay;)V

    .line 199
    .local v0, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 201
    iget-boolean v1, v0, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-nez v1, :cond_0

    .line 202
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay;->add(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    iget-boolean v1, v0, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v1, :cond_1

    .line 204
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay;->remove(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay;->buffer:Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;

    invoke-interface {v1, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;->replay(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V

    goto :goto_0
.end method
