.class public interface abstract Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;
.super Ljava/lang/Object;
.source "ItemSelectedListener.java"


# annotations
.annotation runtime Lcom/microsoft/xbox/toolkit/java8/FunctionalInterface;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation
.end method
