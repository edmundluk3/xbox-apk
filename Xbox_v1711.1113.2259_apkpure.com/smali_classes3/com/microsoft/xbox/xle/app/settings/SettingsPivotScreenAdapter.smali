.class public Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "SettingsPivotScreenAdapter.java"


# instance fields
.field private final pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 17
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;

    .line 20
    const v0, 0x7f0e0a13

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070bf2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method
