.class public final enum Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
.super Ljava/lang/Enum;
.source "MediaHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CustomPicType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

.field public static final enum ClubBackground:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

.field public static final enum ClubLogo:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

.field public static final enum Gamerpic:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 524
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->Unknown:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 525
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    const-string v1, "Gamerpic"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->Gamerpic:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 526
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    const-string v1, "ClubLogo"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubLogo:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 527
    new-instance v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    const-string v1, "ClubBackground"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubBackground:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 523
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->Unknown:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->Gamerpic:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubLogo:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubBackground:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 523
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 523
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .locals 1

    .prologue
    .line 523
    sget-object v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->$VALUES:[Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    return-object v0
.end method
