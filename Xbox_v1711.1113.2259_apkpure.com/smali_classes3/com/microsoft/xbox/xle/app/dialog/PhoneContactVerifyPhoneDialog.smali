.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PhoneContactVerifyPhoneDialog.java"


# instance fields
.field private busyIndicator:Landroid/view/View;

.field private callMeButton:Landroid/widget/RelativeLayout;

.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private errorPanel:Landroid/view/ViewGroup;

.field private messagePanel:Landroid/view/ViewGroup;

.field private phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private resendTheCodeButton:Landroid/widget/RelativeLayout;

.field private verificationCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 33
    const v0, 0x7f0301d0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->setContentView(I)V

    .line 35
    const v0, 0x7f0e094b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->messagePanel:Landroid/view/ViewGroup;

    .line 36
    const v0, 0x7f0e094d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->errorPanel:Landroid/view/ViewGroup;

    .line 37
    const v0, 0x7f0e0946

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 38
    const v0, 0x7f0e0951

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 39
    const v0, 0x7f0e093f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 40
    const v0, 0x7f0e0952

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->busyIndicator:Landroid/view/View;

    .line 41
    const v0, 0x7f0e0961

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 42
    const v0, 0x7f0e0962

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    .line 43
    const v0, 0x7f0e0965

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method private hideResendAndCallMeButtonsForOneMinute()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 169
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Ljava/lang/Runnable;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 173
    return-void
.end method

.method static synthetic lambda$hideResendAndCallMeButtonsForOneMinute$5(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 172
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;Landroid/view/View;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Verify code"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsVerifyPhone()V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->verificationCode:Ljava/lang/String;

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->verificationCode:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v4, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsCancel()V

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->dismiss()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 96
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsResendCode()V

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->hideResendAndCallMeButtonsForOneMinute()V

    .line 99
    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 104
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsCallme()V

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->hideResendAndCallMeButtonsForOneMinute()V

    .line 107
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->setBusy(Z)V

    .line 161
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 162
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->dismiss()V

    .line 156
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsVerifyPhoneView()V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsClose()V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_5

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->phoneVerificationCodeEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 127
    :cond_5
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->resendTheCodeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->callMeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    :cond_4
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 151
    return-void
.end method

.method public setBusy(Z)V
    .locals 2
    .param p1, "isBusy"    # Z

    .prologue
    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->busyIndicator:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    return-void

    .line 47
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
    .param p2, "isBusy"    # Z

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->updateContent(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;)V

    .line 52
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->setBusy(Z)V

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->show()V

    .line 54
    return-void
.end method

.method public updateContent(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne p1, v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->errorPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->messagePanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v1, 0x7f070738

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 68
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->messagePanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->errorPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->hideResendAndCallMeButtonsForOneMinute()V

    goto :goto_0
.end method
