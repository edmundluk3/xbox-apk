.class public Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "ClubCardListAdapter$ClubCardViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    .line 24
    const v0, 0x7f0e02b7

    const-string v1, "field \'clubPicFrame\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPicFrame:Landroid/view/View;

    .line 25
    const v0, 0x7f0e02b8

    const-string v1, "field \'clubBanner\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubBanner:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f0e02b9

    const-string v1, "field \'clubPic\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPic:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f0e02ba

    const-string v1, "field \'clubName\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e02c1

    const-string v1, "field \'members\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->members:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v0, 0x7f0e02c2

    const-string v1, "field \'hereToday\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->hereToday:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 30
    const v0, 0x7f0e02c4

    const-string v1, "field \'tagsList\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 31
    const v0, 0x7f0e02c3

    const-string v1, "field \'description\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e02be

    const-string v1, "field \'clubType\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e02bc

    const-string v1, "field \'clubTypeIcon\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e02bd

    const-string v1, "field \'customGlyph\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->customGlyph:Landroid/widget/ImageView;

    .line 35
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    .line 41
    .local v0, "target":Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPicFrame:Landroid/view/View;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubBanner:Landroid/widget/ImageView;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPic:Landroid/widget/ImageView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->members:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->hereToday:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->customGlyph:Landroid/widget/ImageView;

    .line 55
    return-void
.end method
