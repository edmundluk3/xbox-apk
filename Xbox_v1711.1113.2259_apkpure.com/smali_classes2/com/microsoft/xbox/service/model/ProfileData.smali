.class public Lcom/microsoft/xbox/service/model/ProfileData;
.super Ljava/lang/Object;
.source "ProfileData.java"


# instance fields
.field private privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

.field private profileResult:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

.field private shareRealName:Z

.field private shareRealNameStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field private sharingRealNameTransitively:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V
    .locals 0
    .param p1, "profileResult"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .param p2, "privacySettings"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileData;->profileResult:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;Z)V
    .locals 1
    .param p1, "profileResult"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .param p2, "shareRealName"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileData;->profileResult:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    .line 21
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/ProfileData;->shareRealName:Z

    .line 22
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->NotSet:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->shareRealNameStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;ZLcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;Z)V
    .locals 0
    .param p1, "profileResult"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .param p2, "shareRealName"    # Z
    .param p3, "shareRealNameStatus"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .param p4, "sharingRealNameTransitively"    # Z

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileData;->profileResult:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    .line 27
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/ProfileData;->shareRealName:Z

    .line 28
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileData;->shareRealNameStatus:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 29
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/ProfileData;->sharingRealNameTransitively:Z

    .line 30
    return-void
.end method


# virtual methods
.method public getCanCommunicateWithTextAndVoice()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getCommunicateUsingTextAndVoice()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCanShareRealName()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getCanShareRealName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCanShareRecordedGameSessions()Z
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getShareRecordedGameSessions()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCanViewOtherProfiles()Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getCanViewOtherProfiles()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProfileResult()Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->profileResult:Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    return-object v0
.end method

.method public getShareRealName()Z
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSharingRealNameTransitively()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileData;->privacySettings:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getSharingRealNameTransitively()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
