.class Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;
.super Lorg/json/JSONObject;
.source "JsonTitleChannelChanged.java"


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "associatedChannels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 15
    const-string v0, "associatedChannels"

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChannelChanged;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 16
    return-void
.end method
