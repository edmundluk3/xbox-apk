.class Lcom/microsoft/playready/Native_Class8;
.super Ljava/lang/Object;
.source "Native_Class8.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/Native_Class8$internal_class_1;,
        Lcom/microsoft/playready/Native_Class8$internal_class_2;
    }
.end annotation


# instance fields
.field protected mField1:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 16
    invoke-static {}, Lcom/microsoft/playready/NativeLoadLibrary;->loadLibrary()V

    .line 18
    invoke-static {}, Lcom/microsoft/playready/Native_Class8;->_method_1()V

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/playready/Native_Class8;->mField1:J

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/playready/Native_Class8;->_method_2()V

    .line 52
    return-void
.end method

.method protected static final native _method_1()V
.end method


# virtual methods
.method protected native _method_2()V
.end method

.method protected native _method_3()V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/playready/Native_Class8;->_method_3()V

    .line 60
    return-void
.end method

.method public native method_14(Lcom/microsoft/playready/Native_Class8$internal_class_2;Ljava/lang/String;)[B
.end method

.method public native method_15(Lcom/microsoft/playready/Native_Class8$internal_class_2;Ljava/lang/String;)[B
.end method

.method public native method_16([B)Ljava/lang/String;
.end method

.method public native method_17([B)Ljava/lang/String;
.end method

.method public native method_5(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/Native_Class8$internal_class_2;)Lcom/microsoft/playready/Native_Class8$internal_class_1;
.end method

.method public native method_6([B)Ljava/lang/String;
.end method

.method public native method_7(Ljava/lang/String;)V
.end method

.method public native method_8([BLjava/lang/String;)Lcom/microsoft/playready/Native_Class8$internal_class_1;
.end method

.method public native method_9([B)Ljava/lang/String;
.end method
