.class Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;
.super Landroid/widget/BaseAdapter;
.source "GameOverviewScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private setIconView(ILandroid/view/View;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 462
    const v2, 0x7f0e0600

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 463
    .local v0, "iconView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v2, 0x7f0e0601

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 464
    .local v1, "labelView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$3;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$StoreItemDetailResponse$PlatformType:[I

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->getItem(I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 474
    :goto_0
    return-void

    .line 466
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070f08

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f071086

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 470
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070fae

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f071085

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 464
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getAvailableOnPlatforms()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getAvailableOnPlatforms()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->getItem(I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 447
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 452
    move-object v0, p2

    .line 454
    .local v0, "cell":Landroid/view/View;
    if-nez v0, :cond_0

    .line 455
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03010f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 457
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter$2;->setIconView(ILandroid/view/View;)V

    .line 458
    return-object v0
.end method
