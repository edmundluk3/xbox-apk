.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubSettings"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 1

    .prologue
    .line 729
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;-><init>()V

    return-object v0
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z
    .locals 1
    .param p0, "settings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 713
    if-eqz p0, :cond_0

    .line 714
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 715
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 713
    :goto_0
    return v0

    .line 719
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 723
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
