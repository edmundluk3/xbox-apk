.class public abstract Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
.super Ljava/lang/Object;
.source "HoverChatHeadViewStateDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BadgeViewState"
.end annotation


# instance fields
.field private transient hashCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .locals 1
    .param p0, "isHidden"    # Z
    .param p1, "position"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;-><init>(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    if-nez v3, :cond_2

    move v1, v2

    .line 57
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 59
    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    .line 62
    .local v0, "other":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v4

    if-ne v3, v4, :cond_3

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    if-nez v0, :cond_0

    .line 71
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    .line 72
    iget v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    .line 79
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode:I

    return v0
.end method

.method public abstract isHidden()Z
.end method

.method public isLeft()Z
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Left:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRight()Z
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Right:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
