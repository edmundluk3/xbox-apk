.class Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateTopicNameAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final conversationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

.field private final topic:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "conversationId"    # Ljava/lang/String;
    .param p3, "topic"    # Ljava/lang/String;

    .prologue
    .line 906
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 907
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->conversationId:Ljava/lang/String;

    .line 908
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->topic:Ljava/lang/String;

    .line 909
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 913
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 914
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 942
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 943
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->conversationId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->topic:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->updateGroupTopicNameConversation(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 945
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v1, :cond_0

    .line 946
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->conversationId:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages(Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 949
    :cond_0
    return-object v0

    .line 943
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 937
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 919
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 920
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->topic:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 921
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 932
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->topic:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 933
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 901
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 925
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 926
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Z)Z

    .line 927
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 928
    return-void
.end method
