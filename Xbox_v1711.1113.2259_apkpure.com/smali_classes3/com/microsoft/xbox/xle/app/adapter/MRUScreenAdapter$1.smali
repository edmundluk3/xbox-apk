.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method private getFullType(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Z)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
    .locals 1
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "isRecent"    # Z

    .prologue
    .line 154
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    if-eqz p2, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 163
    :goto_0
    return-object v0

    .line 155
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 156
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    if-eqz p2, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 160
    :cond_3
    if-eqz p2, :cond_4

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 163
    :cond_5
    if-eqz p2, :cond_6

    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0
.end method

.method private getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    :cond_0
    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    .line 115
    :cond_1
    if-nez p1, :cond_2

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecent(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    goto :goto_0
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 123
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_PROMO:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 150
    :goto_0
    return-object v3

    .line 127
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    .line 128
    .local v0, "liBatItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-nez p1, :cond_4

    if-eqz v0, :cond_4

    .line 129
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 131
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v1

    .line 132
    .local v1, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->isOOBE(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 133
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_PROMO:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 135
    :cond_2
    invoke-direct {p0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->getFullType(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Z)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    move-result-object v3

    goto :goto_0

    .line 138
    .end local v1    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_3
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-direct {p0, v3, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->getFullType(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Z)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    move-result-object v3

    goto :goto_0

    .line 142
    :cond_4
    if-ne p1, v5, :cond_6

    .line 143
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecent(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v2

    .line 144
    .local v2, "recentItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 145
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->SNAP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 147
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0

    .line 150
    .end local v2    # "recentItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    goto :goto_0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getRecentsCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
    .param p2, "position"    # I

    .prologue
    .line 92
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->getItem(I)Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;->bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 94
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const v5, 0x7f03017c

    const v3, 0x7f03017b

    const v2, 0x7f03017a

    const/4 v4, 0x0

    .line 67
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$MRUScreenAdapter$ViewType:[I

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03017e

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    :goto_0
    return-object v0

    .line 69
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto :goto_0

    .line 71
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto :goto_0

    .line 73
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto :goto_0

    .line 75
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto :goto_0

    .line 77
    :pswitch_4
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v5, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto/16 :goto_0

    .line 79
    :pswitch_5
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v5, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto/16 :goto_0

    .line 81
    :pswitch_6
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullPromoHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03017d

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullPromoHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto/16 :goto_0

    .line 83
    :pswitch_7
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03017f

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    goto/16 :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
