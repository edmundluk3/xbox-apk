.class public interface abstract Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
.super Ljava/lang/Object;
.source "EPGModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IAllProvidersListener"
.end annotation


# virtual methods
.method public abstract onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
.end method

.method public abstract onDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V
.end method

.method public abstract onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
.end method

.method public abstract onFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
.end method

.method public abstract onProviderListChanged()V
.end method
