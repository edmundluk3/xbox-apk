.class public Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;
.super Ljava/lang/Object;
.source "AccountProvisioningResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

.field private final gamerTag:Ljava/lang/String;

.field private final xuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->TAG:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->gamerTag:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->xuid:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 30
    .local v0, "ordinal":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;->values()[Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    move-result-object v1

    aget-object v1, v1, v0

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    .line 31
    return-void

    .line 30
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "gamerTag"    # Ljava/lang/String;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->gamerTag:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->xuid:Ljava/lang/String;

    .line 24
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public getAgeGroup()Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->gamerTag:Ljava/lang/String;

    return-object v0
.end method

.method public getXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method public setAgeGroup(Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;)V
    .locals 0
    .param p1, "ageGroup"    # Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    .line 48
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->gamerTag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->xuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->ageGroup:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;->ordinal()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    return-void

    .line 59
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
