.class public Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "FutureShowtimesScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;
    }
.end annotation


# instance fields
.field private airingComparator:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->airingComparator:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;

    .line 30
    const v0, 0x7f0e079c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->screenBody:Landroid/view/View;

    .line 31
    const v0, 0x7f0e079d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    .line 34
    const v0, 0x7f0e079e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->updateLoadingIndicator(Z)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    .line 71
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->airingComparator:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;

    if-nez v0, :cond_2

    .line 76
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03016a

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->restoreListPosition()V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 87
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 88
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->items:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->notifyDataSetChanged()V

    goto :goto_1

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    goto :goto_1
.end method
