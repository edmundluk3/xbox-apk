.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileClubRecommendationsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecommendationsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$1;

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 181
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;)J

    move-result-wide v2

    const/16 v1, 0xf

    invoke-virtual {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->loadRecommendedClubs(JI)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 176
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {v0, v2, p0, v2, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 191
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 163
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method
