.class public Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CastCrewListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
        ">;"
    }
.end annotation


# instance fields
.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 18
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    .line 19
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 23
    const/4 v3, 0x0

    .line 24
    .local v3, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
    move-object v1, p2

    .line 25
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_2

    .line 26
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 27
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030175

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 28
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;

    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
    invoke-direct {v3, v1}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;-><init>(Landroid/view/View;)V

    .line 29
    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 34
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    .line 35
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->getNameView()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 37
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->getNameView()Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    :cond_0
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->getRoleView()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 41
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->getRoleView()Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Role:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :cond_1
    return-object v1

    .line 31
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;

    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
    goto :goto_0
.end method
