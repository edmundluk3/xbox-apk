.class public final Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;
.super Ljava/lang/Object;
.source "DLAssetsModel.java"


# static fields
.field private static final SERVICE:Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->TAG:Ljava/lang/String;

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getDLAssetsService()Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->SERVICE:Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getClubpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;
    .locals 2
    .param p0    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Lretrofit2/Call;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "successCallback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/List<Ljava/lang/String;>;>;"
    .local p1, "failureCallback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/lang/Void;>;"
    sget-object v1, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->SERVICE:Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    invoke-interface {v1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;->getClubpicsManifest()Lretrofit2/Call;

    move-result-object v0

    .line 29
    .local v0, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;>;"
    new-instance v1, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$1;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 57
    return-object v0
.end method

.method public static getGamerpicListAsync(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)Lretrofit2/Call;
    .locals 2
    .param p0    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Lretrofit2/Call;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "successCallback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/List<Ljava/lang/String;>;>;"
    .local p1, "failureCallback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/lang/Void;>;"
    sget-object v1, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel;->SERVICE:Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    invoke-interface {v1}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;->getGamerpicsManifest()Lretrofit2/Call;

    move-result-object v0

    .line 62
    .local v0, "call":Lretrofit2/Call;, "Lretrofit2/Call<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;>;"
    new-instance v1, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/service/model/dlAssets/DLAssetsModel$2;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 89
    return-object v0
.end method
