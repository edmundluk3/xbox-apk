.class public abstract Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
.super Ljava/lang/Object;
.source "ClubRosterDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubMemberError"
.end annotation


# static fields
.field public static final LIMIT_FOR_ROLE_CODE:I = 0x3ec

.field private static UNKNOWN:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMemberError$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMemberError$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static unknown()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 201
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->UNKNOWN:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    if-nez v0, :cond_0

    .line 202
    const/4 v0, 0x0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->with(ILjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->UNKNOWN:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    .line 205
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->UNKNOWN:Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    return-object v0
.end method

.method public static with(ILjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .locals 1
    .param p0, "code"    # I
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 195
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 196
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMemberError;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMemberError;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract code()I
.end method

.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
