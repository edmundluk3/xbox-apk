.class final Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;
.super Lio/reactivex/Observable;
.source "ChatHeadManagerEventObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lio/reactivex/Observable",
        "<",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 0
    .param p1    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>;"
    .local p1, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<TT;>;"
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 32
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>;"
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent<TT;>;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {v0, p0, v1, p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;-><init>(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;Lcom/flipkart/chatheads/ui/ChatHeadManager;Lio/reactivex/Observer;)V

    .line 37
    .local v0, "listener":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>.Listener<TT;>;"
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->access$000(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;)Lcom/flipkart/chatheads/ui/ChatHeadListener;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setListener(Lcom/flipkart/chatheads/ui/ChatHeadListener;)V

    .line 39
    return-void
.end method
