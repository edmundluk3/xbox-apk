.class Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "SystemSettingsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetLiveTVSettingsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/xle/app/LiveTVSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;->this$0:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 379
    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;->caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 380
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getConnectedLocale()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "connectedLocale":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 392
    .local v1, "logicalLocale":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_1

    .line 393
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LiveTVSettings;->fromUnknownLocale()Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    move-result-object v2

    .line 396
    :goto_1
    return-object v2

    .line 388
    .end local v1    # "logicalLocale":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 396
    .restart local v1    # "logicalLocale":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getLiveTVSettings(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    move-result-object v2

    goto :goto_1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;->buildData()Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 410
    const-wide/16 v0, 0x1389

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/xle/app/LiveTVSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 405
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/xle/app/LiveTVSettings;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel$GetLiveTVSettingsRunner;->caller:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->access$200(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 406
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method
