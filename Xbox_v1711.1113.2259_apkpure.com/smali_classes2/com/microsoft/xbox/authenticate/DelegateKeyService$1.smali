.class Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;
.super Lcom/microsoft/xbox/authenticate/IDelegateKeyService$Stub;
.source "DelegateKeyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/authenticate/DelegateKeyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-direct {p0}, Lcom/microsoft/xbox/authenticate/IDelegateKeyService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public requestDelegateRPSTicketSilently()Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    new-instance v8, Lcom/microsoft/xbox/idp/interop/LocalConfig;

    invoke-direct {v8}, Lcom/microsoft/xbox/idp/interop/LocalConfig;-><init>()V

    .line 76
    .local v8, "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-virtual {v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 77
    .local v1, "ctx":Landroid/content/Context;
    const-string v11, ""

    .line 79
    .local v11, "ticket":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$000(Lcom/microsoft/xbox/authenticate/DelegateKeyService;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 81
    .local v7, "appUri":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$100(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-result-object v3

    sget-object v5, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_SUCCESS:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    if-ne v3, v5, :cond_0

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 82
    invoke-virtual {v8}, Lcom/microsoft/xbox/idp/interop/LocalConfig;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    sget-object v3, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_NOCID:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$102(Lcom/microsoft/xbox/authenticate/DelegateKeyService;Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 109
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v2}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$100(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->trackDelegatedSigninComplete(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)V

    .line 110
    new-instance v2, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;

    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$100(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-result-object v3

    invoke-direct {v2, v3, v11, v1}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;-><init>(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;Ljava/lang/String;Landroid/content/Context;)V

    return-object v2

    .line 85
    :cond_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "scope=service::user.auth.xboxlive.com::MBI_SSL&client_id=%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v7, v6, v12

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "brokerScope":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$200(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

    move-result-object v3

    const-string v5, "TOKEN_BROKER"

    invoke-virtual {v8}, Lcom/microsoft/xbox/idp/interop/LocalConfig;->getCid()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v0, "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    monitor-enter v0

    .line 90
    :try_start_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;->start()Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    .line 91
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_1
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-static {v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$200(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;->getTicket()Ljava/lang/String;

    move-result-object v11

    .line 101
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 102
    const-string v3, "Unexpected empty ticket"

    const-string v5, "DelegateKeyService"

    invoke-static {v3, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v2, "DelegateKeyService"

    const-string v3, "Unexpected empty ticket"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v2, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$1;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    sget-object v3, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->RESULT_UNEXPECTED:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/authenticate/DelegateKeyService;->access$102(Lcom/microsoft/xbox/authenticate/DelegateKeyService;Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;)Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    goto :goto_0

    .line 92
    :catch_0
    move-exception v10

    .line 93
    .local v10, "exception":Ljava/lang/Exception;
    :try_start_2
    const-string v9, "Failed to request Delegate RPS Ticket"

    .line 94
    .local v9, "errorCode":Ljava/lang/String;
    const-string v3, "Failed to request Delegate RPS Ticket"

    invoke-static {v3, v10}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 95
    const-string v3, "DelegateKeyService"

    const-string v5, "Unexpected exception"

    invoke-static {v3, v5, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 97
    .end local v9    # "errorCode":Ljava/lang/String;
    .end local v10    # "exception":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
