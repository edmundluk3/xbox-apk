.class public Lcom/microsoft/cll/android/AndroidCll;
.super Lcom/microsoft/cll/android/Cll;
.source "AndroidCll.java"

# interfaces
.implements Lcom/microsoft/cll/android/SettingsStore$UpdateListener;


# instance fields
.field private final cllPreferences:Landroid/content/SharedPreferences;

.field private final hostPreferences:Landroid/content/SharedPreferences;

.field private final sharedCllPreferencesName:Ljava/lang/String;

.field private final sharedHostPreferencesName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1, "iKey"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    new-instance v0, Lcom/microsoft/cll/android/CorrelationVector;

    invoke-direct {v0}, Lcom/microsoft/cll/android/CorrelationVector;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/cll/android/AndroidCll;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/microsoft/cll/android/CorrelationVector;)V

    .line 26
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/microsoft/cll/android/CorrelationVector;)V
    .locals 7
    .param p1, "iKey"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "correlationVector"    # Lcom/microsoft/cll/android/CorrelationVector;

    .prologue
    const/4 v6, 0x0

    .line 29
    invoke-static {}, Lcom/microsoft/cll/android/AndroidLogger;->getInstance()Lcom/microsoft/cll/android/ILogger;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/microsoft/cll/android/AndroidPartA;

    invoke-static {}, Lcom/microsoft/cll/android/AndroidLogger;->getInstance()Lcom/microsoft/cll/android/ILogger;

    move-result-object v0

    invoke-direct {v4, v0, p1, p2, p3}, Lcom/microsoft/cll/android/AndroidPartA;-><init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Landroid/content/Context;Lcom/microsoft/cll/android/CorrelationVector;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/cll/android/Cll;-><init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/CorrelationVector;)V

    .line 14
    const-string v0, "AndroidCllSettingsSharedPreferences"

    iput-object v0, p0, Lcom/microsoft/cll/android/AndroidCll;->sharedCllPreferencesName:Ljava/lang/String;

    .line 15
    const-string v0, "AndroidHostSettingsSharedPreferences"

    iput-object v0, p0, Lcom/microsoft/cll/android/AndroidCll;->sharedHostPreferencesName:Ljava/lang/String;

    .line 31
    const-string v0, "AndroidCllSettingsSharedPreferences"

    invoke-virtual {p2, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/AndroidCll;->cllPreferences:Landroid/content/SharedPreferences;

    .line 32
    const-string v0, "AndroidHostSettingsSharedPreferences"

    invoke-virtual {p2, v0, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/AndroidCll;->hostPreferences:Landroid/content/SharedPreferences;

    .line 34
    invoke-static {p0}, Lcom/microsoft/cll/android/SettingsStore;->setUpdateListener(Lcom/microsoft/cll/android/SettingsStore$UpdateListener;)V

    .line 35
    invoke-direct {p0}, Lcom/microsoft/cll/android/AndroidCll;->setSettingsStoreValues()V

    .line 36
    return-void
.end method

.method public static initialize(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)Lcom/microsoft/telemetry/IChannel;
    .locals 1
    .param p0, "iKey"    # Ljava/lang/String;
    .param p1, "app"    # Landroid/content/Context;
    .param p2, "endpoint"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Lcom/microsoft/cll/android/AndroidCll;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/cll/android/AndroidCll;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 66
    .local v0, "cll":Lcom/microsoft/cll/android/AndroidCll;
    invoke-virtual {v0, p2}, Lcom/microsoft/cll/android/AndroidCll;->setEndpointUrl(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/cll/android/AndroidCll;->start()V

    .line 68
    return-object v0
.end method

.method private setSettingsStoreValues()V
    .locals 5

    .prologue
    .line 53
    iget-object v3, p0, Lcom/microsoft/cll/android/AndroidCll;->cllPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 54
    .local v2, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 55
    .local v1, "setting":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/cll/android/SettingsStore$Settings;->valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/SettingsStore$Settings;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/microsoft/cll/android/SettingsStore;->updateCllSetting(Lcom/microsoft/cll/android/SettingsStore$Settings;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    .end local v1    # "setting":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    iget-object v3, p0, Lcom/microsoft/cll/android/AndroidCll;->hostPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 59
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 60
    .restart local v1    # "setting":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/cll/android/SettingsStore;->updateHostSetting(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 62
    .end local v1    # "setting":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    return-void
.end method


# virtual methods
.method public OnCllSettingUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "settingValue"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v1, p0, Lcom/microsoft/cll/android/AndroidCll;->cllPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 48
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 49
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 50
    return-void
.end method

.method public OnHostSettingUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "settingValue"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/microsoft/cll/android/AndroidCll;->hostPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 41
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 42
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 43
    return-void
.end method
