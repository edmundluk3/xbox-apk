.class public Lcom/microsoft/xbox/service/model/pins/Pin;
.super Ljava/lang/Object;
.source "Pin.java"


# instance fields
.field public DateAdded:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public DateModified:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public Index:I

.field public Item:Lcom/microsoft/xbox/service/model/pins/PinItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
