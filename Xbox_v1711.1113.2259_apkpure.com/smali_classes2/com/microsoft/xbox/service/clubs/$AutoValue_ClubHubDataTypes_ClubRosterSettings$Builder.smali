.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubRosterSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private inviteOrAccept:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private kickOrBan:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;-><init>()V

    .line 86
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;-><init>()V

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->inviteOrAccept:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->kickOrBan:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 91
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    .locals 4

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterSettings;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->inviteOrAccept:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->kickOrBan:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-object v0
.end method

.method public inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "inviteOrAccept":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->inviteOrAccept:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 95
    return-object p0
.end method

.method public kickOrBan(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "kickOrBan":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->kickOrBan:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 100
    return-object p0
.end method

.method public view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "view":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRosterSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 105
    return-object p0
.end method
