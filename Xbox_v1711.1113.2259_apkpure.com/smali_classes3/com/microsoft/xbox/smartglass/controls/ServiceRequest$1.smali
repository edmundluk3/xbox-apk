.class Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;
.super Lcom/microsoft/xbox/smartglass/TokenListener;
.source "ServiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->runInternal()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

.field final synthetic val$client:Lcom/microsoft/xbox/smartglass/controls/HttpClient;

.field final synthetic val$method:Lorg/apache/http/client/methods/HttpUriRequest;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/microsoft/xbox/smartglass/controls/HttpClient;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->val$method:Lorg/apache/http/client/methods/HttpUriRequest;

    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->val$client:Lcom/microsoft/xbox/smartglass/controls/HttpClient;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/TokenListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Lcom/microsoft/xbox/smartglass/SGResult;Lcom/microsoft/xbox/smartglass/Token;)V
    .locals 6
    .param p1, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;
    .param p2, "token"    # Lcom/microsoft/xbox/smartglass/Token;

    .prologue
    .line 114
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$002(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Lcom/microsoft/xbox/smartglass/TokenResult;)Lcom/microsoft/xbox/smartglass/TokenResult;

    .line 115
    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/SGResult;->isFailure()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/microsoft/xbox/smartglass/Token;->authorization:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to obtain a user token for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-static {v5}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$200(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    move-result-object v5

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->audienceUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;->onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V

    .line 122
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->val$method:Lorg/apache/http/client/methods/HttpUriRequest;

    const-string v3, "Authorization"

    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/Token;->getAuthorizationHeaderForRequest()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->val$client:Lcom/microsoft/xbox/smartglass/controls/HttpClient;

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->val$method:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->processRequest(Lcom/microsoft/xbox/smartglass/controls/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_2
    :goto_0
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "message":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 129
    const-string v1, "Unknown failure."

    .line 132
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->access$100(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;->onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V

    goto :goto_0
.end method
