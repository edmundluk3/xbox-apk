.class final Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;
.super Ljava/lang/Object;
.source "VortexServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->VortexInitialize(ZLandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$launchIntent:Landroid/content/Intent;

.field final synthetic val$launchType:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;->val$launchType:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;->val$launchIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 221
    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->access$102(Z)Z

    .line 222
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->access$000()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->startCll()V

    .line 223
    const-string v2, "VortexServiceManager"

    const-string v3, "App Launch "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->access$000()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->setEnabled(Z)V

    .line 227
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;->val$launchType:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager$2;->val$launchIntent:Landroid/content/Intent;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->access$200(Ljava/lang/String;Landroid/content/Intent;)V

    .line 234
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Could not initialize correlation vector"

    .line 230
    .local v1, "errorCode":Ljava/lang/String;
    const-string v2, "Could not initialize correlation vector"

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
