.class public final enum Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
.super Ljava/lang/Enum;
.source "EPGViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

.field public static final enum AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

.field public static final enum Count:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

.field public static final enum LiveTV:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    const-string v1, "LiveTV"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->LiveTV:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    const-string v1, "AppChannel"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    const-string v1, "Count"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->Count:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->LiveTV:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->Count:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    return-object v0
.end method
