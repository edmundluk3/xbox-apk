.class public Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
.super Ljava/lang/Object;
.source "ProgrammingContentManifest.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "ContentManifest"
.end annotation


# instance fields
.field public Content:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        name = "Content"
        required = false
    .end annotation
.end field

.field public Culture:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Culture"
        required = false
    .end annotation
.end field

.field public LastUpdated:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "LastUpdated"
        required = false
    .end annotation
.end field

.field public Name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Name"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
