.class public final enum Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
.super Ljava/lang/Enum;
.source "SkypeMessageType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum DeleteMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum IsTyping:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum RoleUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum Service:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field public static final enum TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

.field private static final stringToTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "Text"

    const-string v4, "Text"

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 11
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "Activity"

    const-string v4, "Xbox/Activity"

    invoke-direct {v2, v3, v6, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 12
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "AddMember"

    const-string v4, "ThreadActivity/AddMember"

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 13
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "RemoveMember"

    const-string v4, "ThreadActivity/DeleteMember"

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "Service"

    const-string v4, "Xbox/Service"

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Service:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 15
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "Attachment"

    const/4 v4, 0x5

    const-string v5, "Xbox/Attachment"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "RoleUpdate"

    const/4 v4, 0x6

    const-string v5, "ThreadActivity/RoleUpdate"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RoleUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "TopicUpdate"

    const/4 v4, 0x7

    const-string v5, "ThreadActivity/TopicUpdate"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "DeleteMessage"

    const/16 v4, 0x8

    const-string v5, "HistoryActivity/DeleteMessage"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->DeleteMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "IsTyping"

    const/16 v4, 0x9

    const-string v5, "Control/Typing"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->IsTyping:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    const-string v3, "Invalid"

    const/16 v4, 0xa

    const-string v5, "Invalid"

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 9
    const/16 v2, 0xb

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Service:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RoleUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->DeleteMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->IsTyping:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->$VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 23
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->stringToTypeMap:Ljava/util/Map;

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->values()[Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v2

    array-length v3, v2

    .local v0, "messageType":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 35
    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->stringToTypeMap:Ljava/util/Map;

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->type:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->type:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 41
    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->stringToTypeMap:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 42
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    if-nez v0, :cond_0

    .line 43
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    .line 49
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->$VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    return-object v0
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->type:Ljava/lang/String;

    return-object v0
.end method
