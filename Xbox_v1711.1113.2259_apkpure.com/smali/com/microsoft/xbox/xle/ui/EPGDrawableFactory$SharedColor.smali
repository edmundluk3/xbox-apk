.class public Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;
.super Ljava/lang/Object;
.source "EPGDrawableFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedColor"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;
    }
.end annotation


# instance fields
.field private mColor:I

.field private mListeners:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mListeners:Ljava/util/WeakHashMap;

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mColor:I

    .line 38
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mListeners:Ljava/util/WeakHashMap;

    .line 41
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mColor:I

    .line 42
    return-void
.end method

.method private callSubscribers()V
    .locals 4

    .prologue
    .line 64
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 65
    .local v1, "listeners":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;

    .line 66
    .local v0, "listener":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;
    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;->onSharedColorChanged(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;)V

    goto :goto_0

    .line 68
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;)V
    .locals 2
    .param p1, "drawable"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mListeners:Ljava/util/WeakHashMap;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mColor:I

    return v0
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;)V
    .locals 1
    .param p1, "drawable"    # Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor$Listener;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mListeners:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mColor:I

    if-eq v0, p1, :cond_0

    .line 46
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->mColor:I

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedColor;->callSubscribers()V

    .line 49
    :cond_0
    return-void
.end method
