.class public final Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
.super Ljava/lang/Object;
.source "PeopleHubSocialListItems.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PersonListItem"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final person:Lcom/microsoft/xbox/service/model/FollowersData;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 0
    .param p1, "person"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 190
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I
    .locals 3
    .param p1, "another"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .prologue
    const/4 v1, -0x1

    .line 254
    if-nez p1, :cond_1

    .line 262
    :cond_0
    :goto_0
    return v1

    .line 256
    :cond_1
    instance-of v2, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 257
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    .line 258
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringCompareUserLocale(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 259
    .end local v0    # "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    :cond_2
    instance-of v2, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$HeaderListItem;

    if-nez v2, :cond_3

    instance-of v2, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ClubListItem;

    if-eqz v2, :cond_0

    .line 260
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 182
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->compareTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 227
    if-ne p1, p0, :cond_0

    .line 228
    const/4 v1, 0x1

    .line 233
    :goto_0
    return v1

    .line 229
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    if-nez v1, :cond_1

    .line 230
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 232
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;

    .line 233
    .local v0, "other":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/FollowersData;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getGamerRealName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPersonImage()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPersonXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method public getPresonPresence()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 239
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 240
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->hashCode:I

    .line 241
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->person:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->hashCode:I

    .line 244
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->hashCode:I

    return v0
.end method

.method public onClick(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 219
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 221
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackSelectFromFriendsListAction(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;)V

    .line 222
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$PersonListItem;->getPersonXuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
