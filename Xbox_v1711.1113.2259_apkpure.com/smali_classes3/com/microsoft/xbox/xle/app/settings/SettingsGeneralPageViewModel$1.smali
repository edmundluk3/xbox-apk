.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;
.super Ljava/lang/Object;
.source "SettingsGeneralPageViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 332
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->updateAdapter()V

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$002(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z

    .line 338
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->reset()V

    .line 339
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onPause()V

    .line 340
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->resetSettings()V

    .line 341
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signOutWithUi()V

    .line 343
    :cond_0
    return-void
.end method
