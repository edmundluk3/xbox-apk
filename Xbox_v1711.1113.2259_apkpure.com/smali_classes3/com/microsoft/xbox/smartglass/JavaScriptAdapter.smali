.class public Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "JavaScriptAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;,
        Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/JavaScriptAdapterListener;",
        ">;"
    }
.end annotation


# instance fields
.field private _initialized:Z

.field private _lock:Ljava/lang/Object;

.field private _pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "gnustl_shared"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_initialized:Z

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_lock:Ljava/lang/Object;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)Lcom/microsoft/xbox/smartglass/RefTPtr;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;JDDDLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;
    .param p1, "x1"    # J
    .param p3, "x2"    # D
    .param p5, "x3"    # D
    .param p7, "x4"    # D
    .param p9, "x5"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct/range {p0 .. p9}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->fireSensorReadingEvent(JDDDLjava/lang/String;)V

    return-void
.end method

.method private createFailureJsonString(Lcom/microsoft/xbox/smartglass/SGError;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "sgresult"    # Lcom/microsoft/xbox/smartglass/SGError;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 87
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 88
    .local v0, "dataObj":Lorg/json/JSONObject;
    const-string v3, "message"

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 90
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 91
    .local v2, "resultObj":Lorg/json/JSONObject;
    const-string v3, "data"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 92
    const-string v3, "sgresult"

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 94
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 96
    .end local v0    # "dataObj":Lorg/json/JSONObject;
    .end local v2    # "resultObj":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 95
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Lorg/json/JSONException;
    const-string v3, ""

    goto :goto_0
.end method

.method private native deserializeCommand(JLjava/lang/String;)Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
.end method

.method private native deserializeSensorArguments(JLjava/lang/String;)Lcom/microsoft/xbox/smartglass/SensorArguments;
.end method

.method private native fireSensorReadingEvent(JDDDLjava/lang/String;)V
.end method

.method private native getSensorResultJson(JILjava/lang/String;Z)Ljava/lang/String;
.end method

.method private native initialize(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private onEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventJson"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapterListener;

    .line 117
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/JavaScriptAdapterListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapterListener;->onEvent(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/JavaScriptAdapterListener;
    :cond_0
    return-void
.end method

.method private native sendCommand(JLjava/lang/String;)Ljava/lang/String;
.end method

.method private native setClientInfoJson(JLjava/lang/String;)V
.end method


# virtual methods
.method public initialize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 6
    .param p1, "configuration"    # Ljava/lang/String;
    .param p2, "appKey"    # Ljava/lang/String;
    .param p3, "clientInfo"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    iget-boolean v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_initialized:Z

    if-nez v1, :cond_1

    .line 27
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_lock:Ljava/lang/Object;

    monitor-enter v2

    .line 28
    :try_start_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_initialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 30
    :try_start_1
    new-instance v0, Lcom/microsoft/xbox/smartglass/ClientInformation;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/ClientInformation;-><init>()V

    .line 31
    .local v0, "clientInformation":Lcom/microsoft/xbox/smartglass/ClientInformation;
    iput-object p1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->configuration:Ljava/lang/String;

    .line 32
    iput-object p2, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->appKey:Ljava/lang/String;

    .line 33
    invoke-static {v0, p4}, Lcom/microsoft/xbox/smartglass/SGPlatform;->initialize(Lcom/microsoft/xbox/smartglass/ClientInformation;Landroid/content/Context;)V

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v3, "Cordova.Initialize"

    invoke-virtual {v1, v3, p2}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39
    .end local v0    # "clientInformation":Lcom/microsoft/xbox/smartglass/ClientInformation;
    :goto_0
    :try_start_2
    new-instance v1, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getCurrent()Lcom/microsoft/xbox/smartglass/SGPlatform;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getPlatform()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->initialize(J)J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, p3}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->setClientInfoJson(JLjava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->getAccelerometer()Lcom/microsoft/xbox/smartglass/Accelerometer;

    move-result-object v1

    new-instance v3, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$AccelListener;-><init>(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)V

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/smartglass/Accelerometer;->addListener(Ljava/lang/Object;)V

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->getGyrometer()Lcom/microsoft/xbox/smartglass/Gyrometer;

    move-result-object v1

    new-instance v3, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;-><init>(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)V

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/smartglass/Gyrometer;->addListener(Ljava/lang/Object;)V

    .line 45
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_initialized:Z

    .line 47
    :cond_0
    monitor-exit v2

    .line 49
    :cond_1
    return-void

    .line 47
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 35
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public sendCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-boolean v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_initialized:Z

    if-eqz v1, :cond_5

    .line 53
    const-string v7, ""

    .line 55
    .local v7, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-direct {p0, v2, v3, p1}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->deserializeCommand(JLjava/lang/String;)Lcom/microsoft/xbox/smartglass/JavaScriptCommand;

    move-result-object v0

    .line 56
    .local v0, "command":Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
    if-nez v0, :cond_0

    .line 57
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    const-string v2, "SendCommand failed to deserialize command"

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->createFailureJsonString(Lcom/microsoft/xbox/smartglass/SGError;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 80
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
    .end local v7    # "result":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 60
    .restart local v0    # "command":Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
    .restart local v7    # "result":Ljava/lang/String;
    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->className:Ljava/lang/String;

    const-string v2, "Accelerometer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->args:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->deserializeSensorArguments(JLjava/lang/String;)Lcom/microsoft/xbox/smartglass/SensorArguments;

    move-result-object v8

    .line 62
    .local v8, "sensorArguments":Lcom/microsoft/xbox/smartglass/SensorArguments;
    if-nez v8, :cond_1

    .line 63
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    const-string v2, "SendCommand failed to deserialize sensor arguments"

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->createFailureJsonString(Lcom/microsoft/xbox/smartglass/SGError;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 65
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->getAccelerometer()Lcom/microsoft/xbox/smartglass/Accelerometer;

    move-result-object v1

    invoke-virtual {v1, v0, v8}, Lcom/microsoft/xbox/smartglass/Accelerometer;->handleCommand(Lcom/microsoft/xbox/smartglass/JavaScriptCommand;Lcom/microsoft/xbox/smartglass/SensorArguments;)Lcom/microsoft/xbox/smartglass/SensorResult;

    move-result-object v9

    .line 66
    .local v9, "sensorResult":Lcom/microsoft/xbox/smartglass/SensorResult;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-object v1, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v4

    iget-object v5, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldName:Ljava/lang/String;

    iget-boolean v6, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldValue:Z

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->getSensorResultJson(JILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 67
    goto :goto_0

    .end local v8    # "sensorArguments":Lcom/microsoft/xbox/smartglass/SensorArguments;
    .end local v9    # "sensorResult":Lcom/microsoft/xbox/smartglass/SensorResult;
    :cond_2
    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->className:Ljava/lang/String;

    const-string v2, "Gyrometer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-object v1, v0, Lcom/microsoft/xbox/smartglass/JavaScriptCommand;->args:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->deserializeSensorArguments(JLjava/lang/String;)Lcom/microsoft/xbox/smartglass/SensorArguments;

    move-result-object v8

    .line 69
    .restart local v8    # "sensorArguments":Lcom/microsoft/xbox/smartglass/SensorArguments;
    if-nez v8, :cond_3

    .line 70
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Fail:Lcom/microsoft/xbox/smartglass/SGError;

    const-string v2, "SendCommand failed to deserialize sensor arguments"

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->createFailureJsonString(Lcom/microsoft/xbox/smartglass/SGError;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 72
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->getGyrometer()Lcom/microsoft/xbox/smartglass/Gyrometer;

    move-result-object v1

    invoke-virtual {v1, v0, v8}, Lcom/microsoft/xbox/smartglass/Gyrometer;->handleCommand(Lcom/microsoft/xbox/smartglass/JavaScriptCommand;Lcom/microsoft/xbox/smartglass/SensorArguments;)Lcom/microsoft/xbox/smartglass/SensorResult;

    move-result-object v9

    .line 73
    .restart local v9    # "sensorResult":Lcom/microsoft/xbox/smartglass/SensorResult;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-object v1, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->result:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v4

    iget-object v5, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldName:Ljava/lang/String;

    iget-boolean v6, v9, Lcom/microsoft/xbox/smartglass/SensorResult;->boolFieldValue:Z

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->getSensorResultJson(JILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 74
    goto/16 :goto_0

    .line 75
    .end local v8    # "sensorArguments":Lcom/microsoft/xbox/smartglass/SensorArguments;
    .end local v9    # "sensorResult":Lcom/microsoft/xbox/smartglass/SensorResult;
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->_pJavaScriptAdapterProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-direct {p0, v2, v3, p1}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->sendCommand(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 80
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/JavaScriptCommand;
    .end local v7    # "result":Ljava/lang/String;
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->NotInitialized:Lcom/microsoft/xbox/smartglass/SGError;

    const-string v2, "SendCommand failed: JavaScriptAdapter not initialized"

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->createFailureJsonString(Lcom/microsoft/xbox/smartglass/SGError;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0
.end method
