.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2CriticReview;
.super Ljava/lang/Object;
.source "EDSV2CriticReview.java"


# instance fields
.field public Critic:Ljava/lang/String;

.field public CriticText:Ljava/lang/String;

.field public Date:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public Publication:Ljava/lang/String;

.field public PublicationUrl:Ljava/lang/String;

.field public ScoreDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
