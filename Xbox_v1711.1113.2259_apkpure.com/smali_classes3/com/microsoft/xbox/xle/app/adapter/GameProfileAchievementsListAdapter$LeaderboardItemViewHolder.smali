.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LeaderboardItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;"
    }
.end annotation


# instance fields
.field heroStatDetailsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0684
    .end annotation
.end field

.field heroStatTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0683
    .end annotation
.end field

.field statRibbonBackground:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e068a
    .end annotation
.end field

.field statRibbonView:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0689
    .end annotation
.end field

.field statValueTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0688
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 153
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 154
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    return-void
.end method

.method private getProgressPercentage(Ljava/lang/String;)I
    .locals 8
    .param p1, "percentage"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x64

    .line 251
    const-wide/16 v4, 0x0

    invoke-static {p1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 253
    .local v1, "percent":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 254
    .local v0, "finalPercent":I
    :goto_0
    if-ltz v0, :cond_2

    .line 255
    if-le v0, v2, :cond_0

    move v0, v2

    .line 261
    .end local v0    # "finalPercent":I
    :cond_0
    :goto_1
    return v0

    .line 253
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 261
    .restart local v0    # "finalPercent":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->getAdapterPosition()I

    move-result v1

    .line 158
    .local v1, "position":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    move-result-object v0

    .line 159
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v2

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->navigateToStatisticsLeaderboard(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 162
    :cond_0
    return-void
.end method

.method private setHeroStatisticsTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 9
    .param p1, "heroStatTextView"    # Landroid/widget/TextView;
    .param p2, "heroStat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    const/16 v4, 0x8

    const/4 v8, 0x1

    const v7, 0x7f070d48

    const/4 v6, 0x0

    .line 221
    if-eqz p2, :cond_2

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 222
    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->getProgressPercentage(Ljava/lang/String;)I

    move-result v1

    .line 224
    .local v1, "percentageProgress":I
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07034b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 225
    invoke-static {p1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 226
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    if-lez v1, :cond_0

    const-string v2, "%d%%"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 227
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    if-lez v1, :cond_1

    const-string v2, "%d%%"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 243
    .end local v1    # "percentageProgress":I
    :goto_2
    return-void

    .line 226
    .restart local v1    # "percentageProgress":I
    :cond_0
    const-string v2, "--"

    goto :goto_0

    .line 227
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 228
    .end local v1    # "percentageProgress":I
    :cond_2
    if-eqz p2, :cond_5

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 229
    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "minutesPlayed":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07034c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 231
    invoke-static {p1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 232
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object v2, v0

    :goto_3
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 233
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .end local v0    # "minutesPlayed":Ljava/lang/String;
    :goto_4
    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 232
    .restart local v0    # "minutesPlayed":Ljava/lang/String;
    :cond_3
    const-string v2, "--"

    goto :goto_3

    .line 233
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 234
    .end local v0    # "minutesPlayed":Ljava/lang/String;
    :cond_5
    if-eqz p2, :cond_8

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v2, :cond_8

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 235
    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 236
    invoke-static {p1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 237
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    :goto_5
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 238
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    :goto_6
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 237
    :cond_6
    const-string v2, "--"

    goto :goto_5

    .line 238
    :cond_7
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 240
    :cond_8
    invoke-static {p1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 241
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public getStatDetailsText(I)Ljava/lang/String;
    .locals 6
    .param p1, "rank"    # I

    .prologue
    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getPeopleWhoPlayedCount()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 247
    .local v0, "totalPeoplePlayed":I
    if-lez p1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V
    .locals 1
    .param p1, "compositeItem"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 167
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    move-result-object v0

    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->updateUIGamerscore()V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->updateUI(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 136
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V

    return-void
.end method

.method public updateUI(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .prologue
    const/4 v1, 0x0

    .line 199
    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v2, :cond_1

    .line 200
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatTextView:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->setHeroStatisticsTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 201
    iget v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->rank:I

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->getStatDetailsText(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "statsText":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 204
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonView:Landroid/widget/RelativeLayout;

    iget v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->rank:I

    const/4 v4, 0x1

    if-lt v3, v4, :cond_3

    iget v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->rank:I

    const/4 v4, 0x3

    if-gt v3, v4, :cond_3

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 214
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonBackground:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v2

    iget v3, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;->rank:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getRibbonViewColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 217
    .end local v0    # "statsText":Ljava/lang/String;
    :cond_1
    return-void

    .line 208
    .restart local v0    # "statsText":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 213
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public updateUIGamerscore()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 175
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatTextView:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0705b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 176
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatTextView:Landroid/widget/TextView;

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 179
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%d/%d"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 181
    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getCurrentGamerScore()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 182
    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getTotalGamerScore()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    .line 178
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, "statValueTextViewValue":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 185
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070d71

    .line 186
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 187
    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getCurrentGamerScore()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 188
    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getTotalGamerScore()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    .line 185
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "statValueTextViewContentDescription":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 192
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 193
    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0705ac

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 194
    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getGamerEarnedAchievements()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 192
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "heroStatDetailsTextViewValue":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method
