.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2MusicArtistMediaItem.java"


# instance fields
.field public Genres:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation
.end field

.field public SortName:Ljava/lang/String;

.field public ZuneId:Ljava/lang/String;

.field private generatedProvider:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 23
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 24
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Genres:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Genres:Ljava/util/ArrayList;

    move-object v0, p1

    .line 25
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->SortName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->SortName:Ljava/lang/String;

    .line 26
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    .line 28
    :cond_0
    const/16 v0, 0x3f1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->setMediaItemTypeFromInt(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    .line 63
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Images:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getForegroundImageURI(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->Images:Ljava/util/ArrayList;

    const-string v1, "Thumbnail"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/EDSServiceManagerUtil;->getFilteredImageUrl(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->foregroundImageUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->generatedProvider:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getMusicProviders(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->generatedProvider:Ljava/util/ArrayList;

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->generatedProvider:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method
