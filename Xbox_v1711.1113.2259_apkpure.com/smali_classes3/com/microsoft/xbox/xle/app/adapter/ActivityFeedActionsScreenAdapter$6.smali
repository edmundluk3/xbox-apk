.class Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;
.super Ljava/lang/Object;
.source "ActivityFeedActionsScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;->setMessage(Ljava/lang/String;)V

    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter$6;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;Z)V

    .line 361
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 355
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 350
    return-void
.end method
