.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerMember"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;
    }
.end annotation


# static fields
.field public static final transient HOST_INDEX:Ljava/lang/String; = "0"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;
    .locals 1

    .prologue
    .line 1210
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMember$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMember$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1247
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMember$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMember$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract activeTitleId()Ljava/lang/Long;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract gamertag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1202
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->xuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstants;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->xuid()Ljava/lang/String;

    move-result-object v0

    .line 1205
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public abstract properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberRoles;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
