.class public Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "TvMyShowsScreenAdapter.java"


# instance fields
.field private moviesItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private moviesListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

.field private moviesListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private tvItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private tvListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

.field private tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    .prologue
    const v2, 0x7f03023f

    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 36
    const v0, 0x7f0e0b2b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->screenBody:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0b2c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    .line 40
    const v0, 0x7f0e0b2d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 41
    const v0, 0x7f0e0b2e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 53
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 61
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 4

    .prologue
    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->getMockTvData()Ljava/util/List;

    move-result-object v1

    .line 70
    .local v1, "newTvData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;>;"
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvItems:Ljava/util/List;

    if-eq v2, v1, :cond_0

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;->clear()V

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->onDataUpdated()V

    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->restorePosition()V

    .line 76
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->tvItems:Ljava/util/List;

    .line 79
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->getMockMoviesData()Ljava/util/List;

    move-result-object v0

    .line 80
    .local v0, "newMoviesData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;>;"
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesItems:Ljava/util/List;

    if-eq v2, v0, :cond_1

    .line 81
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;->clear()V

    .line 82
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesListView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->onDataUpdated()V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->restorePosition()V

    .line 86
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;->moviesItems:Ljava/util/List;

    .line 88
    :cond_1
    return-void
.end method
