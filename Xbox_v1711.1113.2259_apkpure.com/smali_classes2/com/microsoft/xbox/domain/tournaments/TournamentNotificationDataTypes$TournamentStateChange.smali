.class public abstract Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
.super Ljava/lang/Object;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TournamentStateChange"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentStateChange$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentStateChange$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract newState()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract recipientXuid()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract teamName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract titleId()J
.end method

.method public abstract titleName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract tournamentImageUri()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract tournamentName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
