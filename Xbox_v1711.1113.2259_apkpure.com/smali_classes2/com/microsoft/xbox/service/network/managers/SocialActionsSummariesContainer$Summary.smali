.class public Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
.super Ljava/lang/Object;
.source "SocialActionsSummariesContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Summary"
.end annotation


# instance fields
.field public commentCount:I

.field public isLiked:Z

.field public likeCount:I

.field public path:Ljava/lang/String;

.field public shareCount:I

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equalCounts(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .prologue
    .line 22
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    iget v1, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    iget v1, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    iget v1, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;)V
    .locals 1
    .param p1, "summary"    # Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .prologue
    .line 26
    if-eqz p1, :cond_0

    .line 27
    iget v0, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->commentCount:I

    .line 28
    iget v0, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 29
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    .line 30
    iget v0, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->shareCount:I

    .line 31
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->type:Ljava/lang/String;

    .line 33
    :cond_0
    return-void
.end method
