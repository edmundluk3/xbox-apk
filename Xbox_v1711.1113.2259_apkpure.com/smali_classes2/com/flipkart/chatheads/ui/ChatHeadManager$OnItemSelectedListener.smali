.class public interface abstract Lcom/flipkart/chatheads/ui/ChatHeadManager$OnItemSelectedListener;
.super Ljava/lang/Object;
.source "ChatHeadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/ChatHeadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnItemSelectedListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onChatHeadRollOut(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onChatHeadRollOver(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onChatHeadSelected(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/flipkart/chatheads/ui/ChatHead;",
            ")Z"
        }
    .end annotation
.end method
