.class public Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "TutorialPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/tutorial/TutorialView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 6
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;
    .param p2, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p3, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p5, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 45
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    .line 47
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 104
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$24(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$23(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p4, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p5, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p6, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 48
    invoke-static/range {p0 .. p5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p6, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$34;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$31;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setFacebookWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$12(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$13(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$30;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$14(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setSuggestedFriendsWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$15(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$16(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$29;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$17(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setClubWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$18(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$19(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;)V
    .locals 4
    .param p0, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "onActivityResultIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    .line 95
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->requestCode()I

    move-result v1

    .line 96
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->resultCode()I

    move-result v2

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->data()Landroid/content/Intent;

    move-result-object v3

    .line 94
    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/CallbackManager;->onActivityResult(IILandroid/content/Intent;)Z

    .line 99
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setSetupWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$20(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$21(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p3, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p4, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p5, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    const/16 v0, 0x8

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 51
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 52
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->initialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;

    .line 55
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 56
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 57
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 58
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 59
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;

    .line 61
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 62
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 63
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 64
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 65
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;

    .line 67
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 68
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 69
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 70
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$17;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$FacebookCardClickedIntent;

    .line 73
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 74
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 75
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 76
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$20;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 77
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SuggestedFriendsCardClickedIntent;

    .line 79
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 80
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 81
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$22;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 82
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$23;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 83
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ClubCardClickedIntent;

    .line 85
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 86
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$24;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 87
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$25;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 88
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$26;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 89
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;

    .line 91
    invoke-virtual {p5, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    .line 92
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p4}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$27;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 93
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$28;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 100
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 50
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$22(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p4, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p5, "facebookManager"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p6, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {p6}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 49
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1, p2, p3, p4, p5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$SetupCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$33;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setRedeemWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$RedeemCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;)V
    .locals 1
    .param p0, "tutorialNavigator"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$32;->lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;->navigate(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;)V
    .locals 1
    .param p0, "welcomeCardCompletionRepository"    # Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;->setShopWelcomeCardCompleted(Z)V

    return-void
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewIntents$ShopCardClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .locals 5
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 107
    instance-of v1, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    if-eqz v1, :cond_2

    move-object v0, p2

    .line 108
    check-cast v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    .line 110
    .local v0, "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Landroid/support/v4/util/Pair<Ljava/lang/Boolean;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    check-cast v1, Landroid/support/v4/util/Pair;

    iget-object v1, v1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    check-cast v1, Landroid/support/v4/util/Pair;

    iget-object v1, v1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->withContent(ZLcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object p1

    .line 120
    .end local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Landroid/support/v4/util/Pair<Ljava/lang/Boolean;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;>;>;"
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    :goto_0
    return-object p1

    .line 112
    .restart local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Landroid/support/v4/util/Pair<Ljava/lang/Boolean;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;>;>;"
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    iget-object v1, v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->error:Ljava/lang/Throwable;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object p1

    goto :goto_0

    .line 115
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object p1

    goto :goto_0

    .line 119
    .end local v0    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Landroid/support/v4/util/Pair<Ljava/lang/Boolean;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;>;>;"
    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Couldn\'t explicitly reduce state. previousState: %s. result: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 126
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 127
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 128
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 129
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 131
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 132
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 134
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 135
    return-void
.end method
