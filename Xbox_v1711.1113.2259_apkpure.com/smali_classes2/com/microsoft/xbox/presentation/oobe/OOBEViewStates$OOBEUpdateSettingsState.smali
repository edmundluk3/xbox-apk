.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
.super Ljava/lang/Object;
.source "OOBEViewStates.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OOBEUpdateSettingsState"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static withFatalError(Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    .locals 3
    .param p0, "previousState"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 116
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 117
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;Z)V

    return-object v0
.end method

.method public static withSettings(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;
    .locals 2
    .param p0, "oobeSettings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBEViewStates_OOBEUpdateSettingsState;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract fatalError()Z
.end method

.method public abstract oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBEUpdateSettingsState, settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewStates$OOBEUpdateSettingsState;->oobeSettings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
