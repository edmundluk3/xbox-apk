.class Lorg/simpleframework/xml/core/ExpressionBuilder;
.super Ljava/lang/Object;
.source "ExpressionBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;
    }
.end annotation


# instance fields
.field private final cache:Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

.field private final type:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/Class;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

    invoke-direct {v0, p0}, Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;-><init>(Lorg/simpleframework/xml/core/ExpressionBuilder;)V

    iput-object v0, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->cache:Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

    .line 56
    iput-object p1, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->type:Ljava/lang/Class;

    .line 57
    return-void
.end method

.method private create(Ljava/lang/String;)Lorg/simpleframework/xml/core/Expression;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lorg/simpleframework/xml/core/PathParser;

    iget-object v1, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->type:Ljava/lang/Class;

    invoke-direct {v0, v1, p1}, Lorg/simpleframework/xml/core/PathParser;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 90
    .local v0, "expression":Lorg/simpleframework/xml/core/Expression;
    iget-object v1, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->cache:Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->cache:Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

    invoke-virtual {v1, p1, v0}, Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_0
    return-object v0
.end method


# virtual methods
.method public build(Ljava/lang/String;)Lorg/simpleframework/xml/core/Expression;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v1, p0, Lorg/simpleframework/xml/core/ExpressionBuilder;->cache:Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;

    invoke-virtual {v1, p1}, Lorg/simpleframework/xml/core/ExpressionBuilder$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/Expression;

    .line 72
    .local v0, "expression":Lorg/simpleframework/xml/core/Expression;
    if-nez v0, :cond_0

    .line 73
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/ExpressionBuilder;->create(Ljava/lang/String;)Lorg/simpleframework/xml/core/Expression;

    move-result-object v0

    .line 75
    .end local v0    # "expression":Lorg/simpleframework/xml/core/Expression;
    :cond_0
    return-object v0
.end method
