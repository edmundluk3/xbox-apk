.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserPermission"
.end annotation


# instance fields
.field private final isAllowed:Z

.field private final reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .locals 1
    .param p1, "isAllowed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p2, "reasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->isAllowed:Z

    .line 255
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->reasons:Ljava/util/List;

    .line 256
    return-void
.end method


# virtual methods
.method public getIsAllowed()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->isAllowed:Z

    return v0
.end method

.method public getReason()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->reasons:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermission;->reasons:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionReason;->getReason()Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
