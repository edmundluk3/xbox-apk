.class public interface abstract Lcom/microsoft/xbox/data/service/editorial/EditorialService;
.super Ljava/lang/Object;
.source "EditorialService.java"


# virtual methods
.method public abstract getLfgLanguages()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "Site/XboxLiveFeeds/Content/systemTags/lfgLanguages"
    .end annotation
.end method

.method public abstract getSystemTags()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "Site/XboxLiveFeeds/Content/systemTags/xboxTags"
    .end annotation
.end method

.method public abstract getXboxNewsInfo()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "Site/XboxLiveFeeds/Content/entities/XboxNews/info"
    .end annotation
.end method
