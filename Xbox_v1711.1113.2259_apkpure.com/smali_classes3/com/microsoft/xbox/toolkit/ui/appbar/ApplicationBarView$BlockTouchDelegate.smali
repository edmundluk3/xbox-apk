.class public Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView$BlockTouchDelegate;
.super Landroid/view/TouchDelegate;
.source "ApplicationBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "BlockTouchDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    .param p2, "delegateView"    # Landroid/view/View;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView$BlockTouchDelegate;->this$0:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {p0, v0, p2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 35
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 39
    const-string v0, "ApplicationBar"

    const-string v1, "Swallowing touch inside the linear layout"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x1

    return v0
.end method
