.class Lcom/microsoft/playready/PRMediaPlayer$8;
.super Ljava/lang/Object;
.source "PRMediaPlayer.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/playready/PRMediaPlayer;->startProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/playready/PRMediaPlayer;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/PRMediaPlayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer$8;->this$0:Lcom/microsoft/playready/PRMediaPlayer;

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleReactiveLicenseAcquisitionRequest(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;)V
    .locals 3
    .param p1, "context"    # Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 188
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer$8;->this$0:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-static {v2}, Lcom/microsoft/playready/PRMediaPlayer;->access$0(Lcom/microsoft/playready/PRMediaPlayer;)Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/microsoft/playready/IReactiveLicenseAcquirer;->acquireLicense(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;)Lcom/microsoft/playready/ILicenseAcquisitionTask;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/playready/ILicenseAcquisitionTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    return-void

    .line 189
    :catch_0
    move-exception v1

    .line 193
    .local v1, "ee":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 194
    .local v0, "cause":Ljava/lang/Throwable;
    if-eqz v0, :cond_0

    instance-of v2, v0, Ljava/lang/Exception;

    if-eqz v2, :cond_0

    .line 195
    check-cast v0, Ljava/lang/Exception;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 197
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_0
    throw v1
.end method
