.class public Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "ConsoleConnectionScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

.field private connectionState:I

.field private connectionStateStream:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionStateStream:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 35
    return-void
.end method


# virtual methods
.method public connect()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    .line 94
    return-void
.end method

.method protected createAdapter()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    if-nez v0, :cond_0

    .line 48
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Not creating adapter because adapter provider is not set"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;->getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    goto :goto_0
.end method

.method public disconnectConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 3
    .param p1, "consoleData"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackDisconnect()V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->setLastConnectedConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 99
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setAutoConnect(Z)V

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getInstance()Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->updateAutoConnectState(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 104
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->setCurrentConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 105
    return-void
.end method

.method public getConnectionState()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    return v0
.end method

.method public getConnectionStateStream()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionStateStream:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->createAdapter()V

    .line 39
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->createAdapter()V

    .line 44
    return-void
.end method

.method protected onStartOverride()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 74
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v1

    .line 75
    .local v1, "sessionState":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionStateStream:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 77
    if-ne v1, v6, :cond_0

    const/4 v0, 0x1

    .line 78
    .local v0, "isCurrentlyConnected":Z
    :goto_0
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartOverride - isCurrentlyConnected? "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    if-eqz v0, :cond_1

    .line 80
    iput v6, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    .line 84
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 85
    return-void

    .end local v0    # "isCurrentlyConnected":Z
    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    .line 82
    .restart local v0    # "isCurrentlyConnected":Z
    :cond_1
    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    goto :goto_1
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 90
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v5, 0x2

    .line 109
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 126
    :goto_0
    return-void

    .line 111
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v1

    .line 112
    .local v1, "sessionState":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionStateStream:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 114
    if-ne v1, v5, :cond_0

    const/4 v0, 0x1

    .line 115
    .local v0, "isCurrentlyConnected":Z
    :goto_1
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUpdateOverride - isCurrentlyConnected? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    if-eqz v0, :cond_1

    .line 117
    iput v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    .line 121
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 114
    .end local v0    # "isCurrentlyConnected":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 119
    .restart local v0    # "isCurrentlyConnected":Z
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;->connectionState:I

    goto :goto_2

    .line 109
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
