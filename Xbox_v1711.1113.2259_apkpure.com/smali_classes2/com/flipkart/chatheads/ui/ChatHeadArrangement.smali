.class public abstract Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
.super Ljava/lang/Object;
.source "ChatHeadArrangement.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract canDrag(Lcom/flipkart/chatheads/ui/ChatHead;)Z
.end method

.method public abstract getHeroIndex()Ljava/lang/Integer;
.end method

.method public abstract getRetainBundle()Landroid/os/Bundle;
.end method

.method public handleRawTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public abstract handleTouchUp(Lcom/flipkart/chatheads/ui/ChatHead;IILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Z)Z
.end method

.method public abstract onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V
.end method

.method public abstract onCapture(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract onChatHeadAdded(Lcom/flipkart/chatheads/ui/ChatHead;Z)V
.end method

.method public abstract onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract onConfigChanged(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
.end method

.method public abstract onDeactivate(II)V
.end method

.method public abstract onReloadFragment(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract onSpringUpdate(Lcom/flipkart/chatheads/ui/ChatHead;ZIILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;I)V
.end method

.method public abstract removeOldestChatHead()V
.end method

.method public abstract selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V
.end method

.method public abstract setContainer(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
.end method

.method public abstract shouldShowCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)Z
.end method
