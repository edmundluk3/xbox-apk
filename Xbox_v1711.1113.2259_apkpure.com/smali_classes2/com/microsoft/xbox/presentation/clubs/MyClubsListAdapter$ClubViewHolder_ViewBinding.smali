.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "MyClubsListAdapter$ClubViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;

    .line 23
    const v0, 0x7f0e08cf

    const-string v1, "field \'clubImage\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    .line 24
    const v0, 0x7f0e08d1

    const-string v1, "field \'clubName\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubName:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0e08d2

    const-string v1, "field \'clubGlyph\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f0e08d3

    const-string v1, "field \'clubType\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubType:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f0e08d5

    const-string v1, "field \'clubActivityText\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubActivityText:Landroid/widget/TextView;

    .line 28
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;

    .line 34
    .local v0, "target":Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 35
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;

    .line 37
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    .line 38
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubName:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubType:Landroid/widget/TextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubActivityText:Landroid/widget/TextView;

    .line 42
    return-void
.end method
