.class public Lcom/microsoft/playready/SimpleDomainHandlingPlugin;
.super Ljava/lang/Object;
.source "SimpleDomainHandlingPlugin.java"

# interfaces
.implements Lcom/microsoft/playready/IDomainHandlingPlugin;


# static fields
.field private static synthetic $SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mChallengeCustomData:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType()[I
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->values()[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v1}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v1}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->mChallengeCustomData:Ljava/lang/String;

    .line 5
    return-void
.end method


# virtual methods
.method public doDomainOperation([BLjava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)[B
    .locals 12
    .param p1, "domainChallenge"    # [B
    .param p2, "domainServerUri"    # Ljava/lang/String;
    .param p3, "opType"    # Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    .local v0, "domainResponse":Lcom/microsoft/playready/HttpResponse;
    const/4 v4, 0x0

    .line 30
    .local v4, "retryCount":I
    const/4 v2, 0x0

    .line 31
    .local v2, "fSuccess":Z
    move-object v5, p2

    .line 32
    .local v5, "serverUri":Ljava/lang/String;
    const/4 v6, 0x0

    .line 34
    .local v6, "urlToTry":Ljava/lang/String;
    move-object v6, v5

    .line 36
    const/4 v3, 0x0

    .line 38
    .local v3, "httpHeader":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType()[I

    move-result-object v7

    invoke-virtual {p3}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 49
    sget-boolean v7, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 41
    :pswitch_0
    const-string v3, "Content-Type: text/xml; charset=utf-8\r\nSOAPAction: \"http://schemas.microsoft.com/DRM/2007/03/protocols/JoinDomain\"\r\n"

    .line 54
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 56
    new-instance v7, Ljava/lang/InterruptedException;

    invoke-direct {v7}, Ljava/lang/InterruptedException;-><init>()V

    throw v7

    .line 45
    :pswitch_1
    const-string v3, "Content-Type: text/xml; charset=utf-8\r\nSOAPAction: \"http://schemas.microsoft.com/DRM/2007/03/protocols/LeaveDomain\"\r\n"

    .line 47
    goto :goto_0

    .line 63
    :cond_1
    const/4 v7, 0x0

    .line 61
    :try_start_0
    invoke-static {v6, v7, v3, p1}, Lcom/microsoft/playready/HttpClient;->doTransaction(Ljava/lang/String;Lcom/microsoft/playready/HttpClient$ConnectionSpec;Ljava/lang/String;[B)Lcom/microsoft/playready/HttpResponse;
    :try_end_0
    .catch Lcom/microsoft/playready/HttpRedirectRequestedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 68
    const/4 v2, 0x1

    .line 84
    :goto_1
    if-eqz v2, :cond_0

    .line 85
    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v7

    const/16 v8, 0x1f4

    if-ne v7, v8, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getResponse()[B

    move-result-object v7

    if-nez v7, :cond_5

    .line 92
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v7

    const/16 v8, 0xc8

    if-lt v7, v8, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v7

    const/16 v8, 0x12b

    if-le v7, v8, :cond_5

    .line 98
    :cond_3
    new-instance v7, Lcom/microsoft/playready/HttpException;

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getStatusCode()I

    move-result v8

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getStatusMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v9, v10}, Lcom/microsoft/playready/HttpException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 70
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Lcom/microsoft/playready/HttpRedirectRequestedException;
    sget v7, Lcom/microsoft/playready/HttpClient;->RECCOMENDED_RETRY_COUNT:I

    if-ge v4, v7, :cond_4

    .line 74
    const-string v7, "DomainHandler"

    const-string v8, "Domain server redirected ( %d ) from %s to %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v6, v9, v10

    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpRedirectRequestedException;->getRedirectUrl()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    add-int/lit8 v4, v4, 0x1

    .line 76
    invoke-virtual {v1}, Lcom/microsoft/playready/HttpRedirectRequestedException;->getRedirectUrl()Ljava/lang/String;

    move-result-object v6

    .line 77
    goto :goto_1

    .line 80
    :cond_4
    const-string v7, "DomainHandler"

    const-string v8, "Exceeded maximum number of redirects attempting to reach %s, giving up."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    throw v1

    .line 101
    .end local v1    # "e":Lcom/microsoft/playready/HttpRedirectRequestedException;
    :cond_5
    invoke-virtual {v0}, Lcom/microsoft/playready/HttpResponse;->getResponse()[B

    move-result-object v7

    return-object v7

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getChallengeCustomData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->mChallengeCustomData:Ljava/lang/String;

    return-object v0
.end method

.method public setChallengeCustomData(Ljava/lang/String;)V
    .locals 0
    .param p1, "customData"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/microsoft/playready/SimpleDomainHandlingPlugin;->mChallengeCustomData:Ljava/lang/String;

    .line 13
    return-void
.end method
