.class public final Lcom/microsoft/xbox/toolkit/ListenerManager;
.super Ljava/lang/Object;
.source "ListenerManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/toolkit/EquatableWeakRef",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ListenerManager;, "Lcom/microsoft/xbox/toolkit/ListenerManager<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ListenerManager;->listeners:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public addListener(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ListenerManager;, "Lcom/microsoft/xbox/toolkit/ListenerManager<TT;>;"
    .local p1, "listener":Ljava/lang/Object;, "TT;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ListenerManager;->listeners:Ljava/util/Set;

    new-instance v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public removeListener(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ListenerManager;, "Lcom/microsoft/xbox/toolkit/ListenerManager<TT;>;"
    .local p1, "listener":Ljava/lang/Object;, "TT;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ListenerManager;->listeners:Ljava/util/Set;

    new-instance v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public run(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ListenerManager;, "Lcom/microsoft/xbox/toolkit/ListenerManager<TT;>;"
    .local p1, "action":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<TT;>;"
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ListenerManager;->listeners:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;

    .line 27
    .local v1, "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/EquatableWeakRef;->get()Ljava/lang/Object;

    move-result-object v0

    .line 29
    .local v0, "listener":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 30
    invoke-interface {p1, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    goto :goto_0

    .line 32
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ListenerManager;->listeners:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35
    .end local v0    # "listener":Ljava/lang/Object;, "TT;"
    .end local v1    # "weakRef":Lcom/microsoft/xbox/toolkit/EquatableWeakRef;, "Lcom/microsoft/xbox/toolkit/EquatableWeakRef<TT;>;"
    :cond_1
    return-void
.end method
