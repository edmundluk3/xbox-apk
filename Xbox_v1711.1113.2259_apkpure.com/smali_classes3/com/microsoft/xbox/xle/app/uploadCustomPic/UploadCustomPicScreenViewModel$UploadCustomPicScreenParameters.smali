.class public final Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "UploadCustomPicScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadCustomPicScreenParameters"
.end annotation


# static fields
.field private static final ASPECT_RATIO_H_KEY:Ljava/lang/String; = "aspect_ratio_h"

.field private static final ASPECT_RATIO_W_KEY:Ljava/lang/String; = "aspect_ratio_w"

.field private static final ASSOCIATED_ID_KEY:Ljava/lang/String; = "associated_id"

.field private static final CROP_SHAPE_KEY:Ljava/lang/String; = "crop_shape"

.field private static final PIC_TYPE_KEY:Ljava/lang/String; = "pic_type"


# instance fields
.field private final activityResult:Lcom/microsoft/xbox/toolkit/ActivityResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final aspectRatio:Landroid/util/Pair;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final associatedId:J

.field private final cropShape:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final onSuccess:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/ActivityResult;)V
    .locals 3
    .param p1, "associatedId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "picType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "cropShape"    # Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "activityResult"    # Lcom/microsoft/xbox/toolkit/ActivityResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ActivityResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 252
    .local p4, "aspectRatio":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p6, "onSuccess":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Landroid/net/Uri;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 253
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 254
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 256
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->associatedId:J

    .line 257
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 258
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    .line 259
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->cropShape:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    .line 260
    iput-object p6, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->onSuccess:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 261
    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->activityResult:Lcom/microsoft/xbox/toolkit/ActivityResult;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Landroid/util/Pair;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->cropShape:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->associatedId:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->onSuccess:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;)Lcom/microsoft/xbox/toolkit/ActivityResult;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->activityResult:Lcom/microsoft/xbox/toolkit/ActivityResult;

    return-object v0
.end method

.method public static fromActivityResult(Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ActivityResult;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    .locals 10
    .param p0, "savedState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "activityResult"    # Lcom/microsoft/xbox/toolkit/ActivityResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 290
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 291
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 293
    const-string v1, "associated_id"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 294
    .local v2, "associatedId":J
    const-string v1, "pic_type"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 295
    .local v4, "picType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    const-string v1, "aspect_ratio_w"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 296
    .local v9, "aspectRatioW":Ljava/lang/Integer;
    const-string v1, "aspect_ratio_h"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 297
    .local v0, "aspectRatioH":Ljava/lang/Integer;
    const-string v1, "crop_shape"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    .line 299
    .local v6, "cropShape":Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 300
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 302
    if-eqz v4, :cond_0

    if-nez v6, :cond_1

    :cond_0
    move-object v1, v7

    .line 305
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    if-eqz v9, :cond_2

    if-nez v0, :cond_3

    :cond_2
    move-object v5, v7

    :goto_1
    move-object v8, p1

    .line 308
    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;-><init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/ActivityResult;)V

    goto :goto_0

    :cond_3
    invoke-static {v9, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_1
.end method

.method public static getOvalInstance(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/microsoft/xbox/toolkit/generics/Action;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    .locals 10
    .param p0, "associatedId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "picType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;"
        }
    .end annotation

    .prologue
    .line 273
    .local p3, "aspectRatio":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p4, "onSuccess":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Landroid/net/Uri;>;"
    new-instance v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    sget-object v6, Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;->OVAL:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    const/4 v8, 0x0

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;-><init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/ActivityResult;)V

    return-object v1
.end method

.method public static getRectangleInstance(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/microsoft/xbox/toolkit/generics/Action;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    .locals 10
    .param p0, "associatedId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "picType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;"
        }
    .end annotation

    .prologue
    .line 285
    .local p3, "aspectRatio":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p4, "onSuccess":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Landroid/net/Uri;>;"
    new-instance v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    sget-object v6, Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;->RECTANGLE:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    const/4 v8, 0x0

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;-><init>(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/ActivityResult;)V

    return-object v1
.end method


# virtual methods
.method public saveToBundle(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 316
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 317
    const-string v0, "associated_id"

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->associatedId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 318
    const-string v0, "pic_type"

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->picType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 319
    const-string v2, "aspect_ratio_w"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 320
    const-string v2, "aspect_ratio_h"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->aspectRatio:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    :goto_1
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 321
    const-string v0, "crop_shape"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->cropShape:Lcom/theartofdev/edmodo/cropper/CropImageView$CropShape;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 322
    return-void

    :cond_0
    move-object v0, v1

    .line 319
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 320
    goto :goto_1
.end method
