.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AchievementsEmptyViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;"
    }
.end annotation


# instance fields
.field switchPane:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a2
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 311
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 312
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 313
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V
    .locals 2
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;->switchPane:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getNoContentText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 306
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V

    return-void
.end method
