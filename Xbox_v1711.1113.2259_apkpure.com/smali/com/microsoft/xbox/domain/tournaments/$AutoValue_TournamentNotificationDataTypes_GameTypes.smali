.class abstract Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;
.super Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
.source "$AutoValue_TournamentNotificationDataTypes_GameTypes.java"


# instance fields
.field private final era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

.field private final uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

.field private final uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;)V
    .locals 2
    .param p1, "uwpDesktop"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .param p2, "uwpXboxOne"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .param p3, "era"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null uwpDesktop"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null uwpXboxOne"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 27
    if-nez p3, :cond_2

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null era"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 68
    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 69
    .local v0, "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 71
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->era()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    :cond_3
    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public era()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 78
    const/4 v0, 0x1

    .line 79
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v2

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 83
    mul-int/2addr v0, v2

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 85
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GameTypes{uwpDesktop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uwpXboxOne="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", era="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->era:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_desktop"
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    return-object v0
.end method

.method public uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_xboxone"
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_GameTypes;->uwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    return-object v0
.end method
