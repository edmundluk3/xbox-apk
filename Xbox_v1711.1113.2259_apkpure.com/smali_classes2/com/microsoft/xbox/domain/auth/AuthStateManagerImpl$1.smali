.class Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;
.super Ljava/lang/Object;
.source "AuthStateManagerImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/interop/XsapiUser$SignInSilentlyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignInSilentlyXsapi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(IILjava/lang/String;)V
    .locals 3
    .param p1, "httpStatusCode"    # I
    .param p2, "errorCode"    # I
    .param p3, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 172
    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "signInSilently:onError - httpStatusCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method public onSuccess(Lcom/microsoft/xbox/idp/interop/XsapiUser$SignInStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/idp/interop/XsapiUser$SignInStatus;

    .prologue
    .line 155
    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "signInSilently:onSuccess - status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$3;->$SwitchMap$com$microsoft$xbox$idp$interop$XsapiUser$SignInStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/idp/interop/XsapiUser$SignInStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 168
    :goto_0
    return-void

    .line 159
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 162
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUserCancelled:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInUiRequired:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
