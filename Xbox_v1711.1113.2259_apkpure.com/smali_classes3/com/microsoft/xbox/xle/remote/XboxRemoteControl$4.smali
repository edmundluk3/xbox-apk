.class Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;
.super Ljava/lang/Object;
.source "XboxRemoteControl.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->useAlternatedPadHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 265
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "arg0"    # Landroid/view/MotionEvent;
    .param p2, "arg1"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v2, 0x1

    .line 270
    const-string v3, "dpad"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "fling velocityX=%f, velocityY=%f"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    move v1, v2

    .line 273
    .local v1, "horizontalMovement":Z
    :cond_0
    if-eqz v1, :cond_2

    .line 274
    cmpl-float v3, p3, v8

    if-lez v3, :cond_1

    sget-object v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadRight:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 278
    .local v0, "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V

    .line 279
    return v2

    .line 274
    .end local v0    # "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadLeft:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    goto :goto_0

    .line 276
    :cond_2
    cmpl-float v3, p4, v8

    if-lez v3, :cond_3

    sget-object v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadDown:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .restart local v0    # "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :goto_1
    goto :goto_0

    .end local v0    # "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadUp:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 289
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1, "arg0"    # Landroid/view/MotionEvent;
    .param p2, "arg1"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v6, 0x1

    .line 293
    const-string v0, "dpad"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "scroll x=%f y=%f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    return v6
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 285
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 299
    const-string v0, "dpad"

    const-string v1, "single tap up"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$4;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadA:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V

    .line 301
    return v2
.end method
