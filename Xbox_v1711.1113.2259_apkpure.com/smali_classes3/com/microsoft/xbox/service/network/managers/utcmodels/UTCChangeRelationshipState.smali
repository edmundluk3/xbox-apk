.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;
.super Ljava/lang/Object;
.source "UTCChangeRelationshipState.java"


# instance fields
.field private currentFavoriteStatus:Ljava/lang/String;

.field private currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

.field private currentRealNameStatus:Ljava/lang/String;

.field private currentRelationshipStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRelationshipStatus:Ljava/lang/String;

    .line 18
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentFavoriteStatus:Ljava/lang/String;

    .line 19
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRealNameStatus:Ljava/lang/String;

    .line 20
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;ZZZZZLjava/util/HashSet;)V
    .locals 2
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "personSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p3, "initialRealNameSharingState"    # Z
    .param p4, "finalRealNameSharingState"    # Z
    .param p5, "isFollowing"    # Z
    .param p6, "isFavorite"    # Z
    .param p7, "isFacebook"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            "ZZZZZ",
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p8, "changeFriendshipForm":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;>;"
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRelationshipStatus:Ljava/lang/String;

    .line 18
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentFavoriteStatus:Ljava/lang/String;

    .line 19
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRealNameStatus:Ljava/lang/String;

    .line 20
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 26
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 27
    invoke-direct {p0, p2, p1, p7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getGamerType(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentGamerType(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;)V

    .line 29
    if-eqz p5, :cond_0

    .line 31
    const-string v0, "ExistingFriend"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRelationshipStatus(Ljava/lang/String;)V

    .line 34
    :cond_0
    if-eqz p6, :cond_1

    .line 36
    const-string v0, "ExistingFavorite"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentFavoriteStatus(Ljava/lang/String;)V

    .line 40
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 42
    if-nez p5, :cond_2

    .line 45
    const-string v0, "FriendAdd"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRelationshipStatus(Ljava/lang/String;)V

    .line 52
    :cond_2
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 55
    if-nez p6, :cond_3

    .line 56
    const-string v0, "Favorited"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentFavoriteStatus(Ljava/lang/String;)V

    .line 63
    :cond_3
    :goto_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 65
    if-nez p5, :cond_7

    .line 67
    const-string v0, "SharingOn"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    .line 96
    :cond_4
    :goto_2
    return-void

    .line 47
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    const-string v0, "FriendRemove"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRelationshipStatus(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 60
    const-string v0, "Unfavorited"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentFavoriteStatus(Ljava/lang/String;)V

    goto :goto_1

    .line 70
    :cond_7
    if-ne p3, p4, :cond_8

    if-ne p4, v1, :cond_8

    .line 72
    const-string v0, "ExisitingSharingOn"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    goto :goto_2

    .line 73
    :cond_8
    if-eq p3, p4, :cond_4

    if-ne p4, v1, :cond_4

    .line 75
    const-string v0, "SharingOn"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    goto :goto_2

    .line 78
    :cond_9
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {p8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    if-nez p5, :cond_a

    .line 83
    const-string v0, "SharingOff"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    goto :goto_2

    .line 86
    :cond_a
    if-ne p3, p4, :cond_b

    if-nez p4, :cond_b

    .line 88
    const-string v0, "ExisitingSharingOff"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    goto :goto_2

    .line 89
    :cond_b
    if-eq p3, p4, :cond_4

    if-nez p4, :cond_4

    .line 91
    const-string v0, "SharingOff"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getGamerType(Lcom/microsoft/xbox/service/model/FollowersData;Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "isFacebook"    # Z

    .prologue
    .line 143
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 146
    .local v0, "gamerType":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p1, Lcom/microsoft/xbox/service/model/GameProfileVipData;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    if-eqz v1, :cond_1

    .line 147
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 152
    :cond_1
    if-eqz p3, :cond_2

    .line 153
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 156
    :cond_2
    return-object v0
.end method

.method private getGamerType(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "isFacebook"    # Z

    .prologue
    .line 169
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 172
    .local v0, "gamerType":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    if-eqz v1, :cond_0

    .line 173
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 178
    :cond_0
    if-eqz p3, :cond_1

    .line 179
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 182
    :cond_1
    return-object v0
.end method


# virtual methods
.method public getCurrentFavoriteStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentFavoriteStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentGamerType()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    return-object v0
.end method

.method public getCurrentRealNameStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRealNameStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentRelationshipStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRelationshipStatus:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrentFavoriteStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentFavoriteStatus:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setCurrentGamerType(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;)V
    .locals 0
    .param p1, "status"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 128
    return-void
.end method

.method public setCurrentGamerType(Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersData;Z)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "basicData"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p3, "isFacebook"    # Z

    .prologue
    .line 131
    invoke-direct {p0, p2, p1, p3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getGamerType(Lcom/microsoft/xbox/service/model/FollowersData;Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentGamerType:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 132
    return-void
.end method

.method public setCurrentRealNameStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRealNameStatus:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setCurrentRelationshipStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->currentRelationshipStatus:Ljava/lang/String;

    .line 116
    return-void
.end method
