.class final enum Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
.super Ljava/lang/Enum;
.source "ConnectDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

.field public static final enum CONSOLE:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

.field public static final enum FOOTER:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

.field public static final enum NO_CONSOLES:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 595
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    const-string v1, "CONSOLE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->CONSOLE:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    const-string v1, "NO_CONSOLES"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->NO_CONSOLES:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    const-string v1, "FOOTER"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->FOOTER:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    .line 594
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->CONSOLE:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->NO_CONSOLES:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->FOOTER:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 594
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;
    .locals 1

    .prologue
    .line 594
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$ViewType;

    return-object v0
.end method
