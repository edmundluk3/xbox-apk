.class public Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
.super Ljava/lang/Object;
.source "TextInputControlViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;,
        Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;
    }
.end annotation


# static fields
.field private static final TIMER_PERIOD:J = 0xfaL


# instance fields
.field private final VIBRATE_PATTERN:[J

.field private checkForDirtyContentTask:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

.field private deviceTextBuffer:Ljava/lang/String;

.field private hasBeenDirty:Z

.field private isDirty:Z

.field private textBufferSent:Ljava/lang/String;

.field private textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

.field private textMessageListener:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;

.field private timerCheckForDirty:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputControl;)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->VIBRATE_PATTERN:[J

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textMessageListener:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;

    .line 45
    iput-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    .line 46
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->isDirty:Z

    .line 47
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->hasBeenDirty:Z

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->checkForDirtyContentTask:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

    .line 49
    return-void

    .line 29
    :array_0
    .array-data 8
        0x0
        0x64
        0x64
        0x64
    .end array-data
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->isDirty:Z

    return v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->isDirty:Z

    return p1
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textBufferSent:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->hasBeenDirty:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Lcom/microsoft/xbox/xle/remote/TextInputControl;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

    return-object v0
.end method

.method private vibrate()V
    .locals 3

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "vibrator"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 132
    .local v0, "v":Landroid/os/Vibrator;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->VIBRATE_PATTERN:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 135
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelTextSession()V
    .locals 2

    .prologue
    .line 124
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextAction;->Cancel:Lcom/microsoft/xbox/smartglass/TextAction;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->completeTextEntry(Lcom/microsoft/xbox/smartglass/TextAction;)V

    .line 126
    const-string v0, "textinputvm"

    const-string v1, "text session cancelled"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public completeTextSession()V
    .locals 2

    .prologue
    .line 117
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->checkForDirtyContentTask:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->run()V

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/TextAction;->Accept:Lcom/microsoft/xbox/smartglass/TextAction;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->completeTextEntry(Lcom/microsoft/xbox/smartglass/TextAction;)V

    .line 120
    const-string v0, "textinputvm"

    const-string v1, "text session completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public onStart()V
    .locals 8

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    .line 56
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getInstance()Lcom/microsoft/xbox/xle/remote/TextInputModel;

    move-result-object v6

    .line 57
    .local v6, "model":Lcom/microsoft/xbox/xle/remote/TextInputModel;
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentText()Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, "tempText":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onTextChangedMessage(Ljava/lang/String;)V

    .line 62
    :cond_1
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentSelectStart()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 63
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentSelectStart()I

    move-result v0

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentSelectLength()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onTextSelectionMessage(II)V

    .line 66
    :cond_2
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentTextConfiguation()Lcom/microsoft/xbox/smartglass/TextConfiguration;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getCurrentTextConfiguation()Lcom/microsoft/xbox/smartglass/TextConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->updateConfiguration(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V

    .line 70
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textMessageListener:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;

    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->attach(Lcom/microsoft/xbox/smartglass/TextManagerListener;)V

    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->vibrate()V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->checkForDirtyContentTask:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 74
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 79
    iput-object v3, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->timerCheckForDirty:Ljava/util/Timer;

    .line 82
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getInstance()Lcom/microsoft/xbox/xle/remote/TextInputModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->detach()V

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Text Input"

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->hasBeenDirty:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    return-void

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextChangedMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->isDirty:Z

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textBufferSent:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->updateText(Ljava/lang/String;)V

    .line 101
    :cond_0
    return-void
.end method

.method public onTextSelectionMessage(II)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 104
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->textInputControl:Lcom/microsoft/xbox/xle/remote/TextInputControl;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->updateTextSelection(II)V

    .line 106
    return-void
.end method

.method public updateDeviceTextBuffer(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->isDirty:Z

    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->deviceTextBuffer:Ljava/lang/String;

    .line 114
    :cond_0
    return-void
.end method
