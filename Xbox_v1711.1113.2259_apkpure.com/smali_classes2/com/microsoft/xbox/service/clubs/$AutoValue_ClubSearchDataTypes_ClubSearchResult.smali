.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;
.super Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;
.source "$AutoValue_ClubSearchDataTypes_ClubSearchResult.java"


# instance fields
.field private final description:Ljava/lang/String;

.field private final displayImageUrl:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

.field private final score:Ljava/lang/Float;

.field private final tags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "displayImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "score"    # Ljava/lang/Float;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "preferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p6, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p7, "titles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;-><init>()V

    .line 31
    if-nez p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->id:Ljava/lang/String;

    .line 35
    if-nez p2, :cond_1

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->name:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    .line 42
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    .line 43
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    .line 44
    iput-object p8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 45
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    return-object v0
.end method

.method public displayImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    if-ne p1, p0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v1

    .line 114
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    if-eqz v3, :cond_9

    move-object v0, p1

    .line 115
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    .line 116
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->name:Ljava/lang/String;

    .line 117
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 118
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 119
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->displayImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    if-nez v3, :cond_5

    .line 120
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->score()Ljava/lang/Float;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_6

    .line 121
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_7

    .line 122
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v3, :cond_8

    .line 123
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 118
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 119
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->displayImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 120
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->score()Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 121
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 122
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 123
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;
    :cond_9
    move v1, v2

    .line 125
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 130
    const/4 v0, 0x1

    .line 131
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 133
    mul-int/2addr v0, v3

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 135
    mul-int/2addr v0, v3

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 137
    mul-int/2addr v0, v3

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 139
    mul-int/2addr v0, v3

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 141
    mul-int/2addr v0, v3

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 143
    mul-int/2addr v0, v3

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 145
    mul-int/2addr v0, v3

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-nez v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 147
    return v0

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_2

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_3

    .line 144
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_4

    .line 146
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->id:Ljava/lang/String;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->name:Ljava/lang/String;

    return-object v0
.end method

.method public preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method public score()Ljava/lang/Float;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    return-object v0
.end method

.method public tags()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public titles()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubSearchResult{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->displayImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->score:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->titles:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", preferredColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubSearchDataTypes_ClubSearchResult;->preferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
