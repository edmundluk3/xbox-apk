.class Lorg/simpleframework/xml/core/ConstructorScanner;
.super Ljava/lang/Object;
.source "ConstructorScanner.java"


# instance fields
.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Initializer;",
            ">;"
        }
    .end annotation
.end field

.field private primary:Lorg/simpleframework/xml/core/Initializer;

.field private signature:Lorg/simpleframework/xml/core/Signature;

.field private type:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->list:Ljava/util/List;

    .line 79
    new-instance v0, Lorg/simpleframework/xml/core/Signature;

    invoke-direct {v0, p1}, Lorg/simpleframework/xml/core/Signature;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->signature:Lorg/simpleframework/xml/core/Signature;

    .line 80
    iput-object p1, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->type:Ljava/lang/Class;

    .line 81
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/ConstructorScanner;->scan(Ljava/lang/Class;)V

    .line 82
    return-void
.end method

.method private build(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V
    .locals 2
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;
    .param p2, "signature"    # Lorg/simpleframework/xml/core/Signature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v0, Lorg/simpleframework/xml/core/Initializer;

    invoke-direct {v0, p1, p2}, Lorg/simpleframework/xml/core/Initializer;-><init>(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V

    .line 163
    .local v0, "initializer":Lorg/simpleframework/xml/core/Initializer;
    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Initializer;->isDefault()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    iput-object v0, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->primary:Lorg/simpleframework/xml/core/Initializer;

    .line 166
    :cond_0
    iget-object v1, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method private create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;
    .locals 3
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;
    .param p2, "label"    # Ljava/lang/annotation/Annotation;
    .param p3, "ordinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 214
    invoke-static {p1, p2, p3}, Lorg/simpleframework/xml/core/ParameterFactory;->getInstance(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v1

    .line 215
    .local v1, "value":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v1}, Lorg/simpleframework/xml/core/Parameter;->getName()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v2, v0}, Lorg/simpleframework/xml/core/Signature;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    invoke-direct {p0, v1, v0}, Lorg/simpleframework/xml/core/ConstructorScanner;->validate(Lorg/simpleframework/xml/core/Parameter;Ljava/lang/String;)V

    .line 220
    :cond_0
    return-object v1
.end method

.method private isInstantiable(Ljava/lang/Class;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/Class;

    .prologue
    const/4 v1, 0x1

    .line 257
    invoke-virtual {p1}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    .line 259
    .local v0, "modifiers":I
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 262
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Class;->isMemberClass()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private process(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;
    .locals 1
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;
    .param p2, "label"    # Ljava/lang/annotation/Annotation;
    .param p3, "ordinal"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 181
    instance-of v0, p2, Lorg/simpleframework/xml/Attribute;

    if-eqz v0, :cond_0

    .line 182
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    .line 184
    :cond_0
    instance-of v0, p2, Lorg/simpleframework/xml/ElementList;

    if-eqz v0, :cond_1

    .line 185
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    goto :goto_0

    .line 187
    :cond_1
    instance-of v0, p2, Lorg/simpleframework/xml/ElementArray;

    if-eqz v0, :cond_2

    .line 188
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    goto :goto_0

    .line 190
    :cond_2
    instance-of v0, p2, Lorg/simpleframework/xml/ElementMap;

    if-eqz v0, :cond_3

    .line 191
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_3
    instance-of v0, p2, Lorg/simpleframework/xml/Element;

    if-eqz v0, :cond_4

    .line 194
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_4
    instance-of v0, p2, Lorg/simpleframework/xml/Text;

    if-eqz v0, :cond_5

    .line 197
    invoke-direct {p0, p1, p2, p3}, Lorg/simpleframework/xml/core/ConstructorScanner;->create(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v0

    goto :goto_0

    .line 199
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private scan(Ljava/lang/Class;)V
    .locals 10
    .param p1, "type"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 107
    .local v1, "array":[Ljava/lang/reflect/Constructor;
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/ConstructorScanner;->isInstantiable(Ljava/lang/Class;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 108
    new-instance v6, Lorg/simpleframework/xml/core/ConstructorException;

    const-string v7, "Can not construct inner %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-direct {v6, v7, v8}, Lorg/simpleframework/xml/core/ConstructorException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v6

    .line 110
    :cond_0
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/reflect/Constructor;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v2, v0, v3

    .line 111
    .local v2, "factory":Ljava/lang/reflect/Constructor;
    new-instance v4, Lorg/simpleframework/xml/core/Signature;

    invoke-direct {v4, p1}, Lorg/simpleframework/xml/core/Signature;-><init>(Ljava/lang/Class;)V

    .line 113
    .local v4, "index":Lorg/simpleframework/xml/core/Signature;
    invoke-virtual {p1}, Ljava/lang/Class;->isPrimitive()Z

    move-result v6

    if-nez v6, :cond_1

    .line 114
    invoke-direct {p0, v2, v4}, Lorg/simpleframework/xml/core/ConstructorScanner;->scan(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V

    .line 110
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 117
    .end local v2    # "factory":Ljava/lang/reflect/Constructor;
    .end local v4    # "index":Lorg/simpleframework/xml/core/Signature;
    :cond_2
    return-void
.end method

.method private scan(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V
    .locals 10
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;
    .param p2, "map"    # Lorg/simpleframework/xml/core/Signature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v2

    .line 130
    .local v2, "labels":[[Ljava/lang/annotation/Annotation;
    invoke-virtual {p1}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    .line 132
    .local v4, "types":[Ljava/lang/Class;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v4

    if-ge v0, v6, :cond_3

    .line 133
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    aget-object v6, v2, v0

    array-length v6, v6

    if-ge v1, v6, :cond_2

    .line 134
    aget-object v6, v2, v0

    aget-object v6, v6, v1

    invoke-direct {p0, p1, v6, v0}, Lorg/simpleframework/xml/core/ConstructorScanner;->process(Ljava/lang/reflect/Constructor;Ljava/lang/annotation/Annotation;I)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v5

    .line 136
    .local v5, "value":Lorg/simpleframework/xml/core/Parameter;
    if-eqz v5, :cond_1

    .line 137
    invoke-interface {v5}, Lorg/simpleframework/xml/core/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    .line 139
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p2, v3}, Lorg/simpleframework/xml/core/Signature;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 140
    new-instance v6, Lorg/simpleframework/xml/core/PersistenceException;

    const-string v7, "Parameter \'%s\' is a duplicate in %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    invoke-direct {v6, v7, v8}, Lorg/simpleframework/xml/core/PersistenceException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v6

    .line 142
    :cond_0
    iget-object v6, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v6, v3, v5}, Lorg/simpleframework/xml/core/Signature;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-virtual {p2, v3, v5}, Lorg/simpleframework/xml/core/Signature;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    .end local v5    # "value":Lorg/simpleframework/xml/core/Parameter;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    .end local v1    # "j":I
    :cond_3
    array-length v6, v4

    invoke-virtual {p2}, Lorg/simpleframework/xml/core/Signature;->size()I

    move-result v7

    if-ne v6, v7, :cond_4

    .line 148
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ConstructorScanner;->build(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V

    .line 150
    :cond_4
    return-void
.end method

.method private validate(Lorg/simpleframework/xml/core/Parameter;Ljava/lang/String;)V
    .locals 8
    .param p1, "parameter"    # Lorg/simpleframework/xml/core/Parameter;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 233
    iget-object v3, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v3, p2}, Lorg/simpleframework/xml/core/Signature;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simpleframework/xml/core/Parameter;

    .line 234
    .local v2, "other":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v2}, Lorg/simpleframework/xml/core/Parameter;->getAnnotation()Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 236
    .local v1, "label":Ljava/lang/annotation/Annotation;
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Parameter;->getAnnotation()Ljava/lang/annotation/Annotation;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 237
    new-instance v3, Lorg/simpleframework/xml/core/MethodException;

    const-string v4, "Annotations do not match for \'%s\' in %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v6

    iget-object v6, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->type:Ljava/lang/Class;

    aput-object v6, v5, v7

    invoke-direct {v3, v4, v5}, Lorg/simpleframework/xml/core/MethodException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v3

    .line 239
    :cond_0
    invoke-interface {v2}, Lorg/simpleframework/xml/core/Parameter;->getType()Ljava/lang/Class;

    move-result-object v0

    .line 241
    .local v0, "expect":Ljava/lang/Class;
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Parameter;->getType()Ljava/lang/Class;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 242
    new-instance v3, Lorg/simpleframework/xml/core/MethodException;

    const-string v4, "Method types do not match for \'%s\' in %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v6

    iget-object v6, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->type:Ljava/lang/Class;

    aput-object v6, v5, v7

    invoke-direct {v3, v4, v5}, Lorg/simpleframework/xml/core/MethodException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v3

    .line 244
    :cond_1
    return-void
.end method


# virtual methods
.method public getCreator()Lorg/simpleframework/xml/core/Creator;
    .locals 4

    .prologue
    .line 94
    new-instance v0, Lorg/simpleframework/xml/core/ClassCreator;

    iget-object v1, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->list:Ljava/util/List;

    iget-object v2, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->signature:Lorg/simpleframework/xml/core/Signature;

    iget-object v3, p0, Lorg/simpleframework/xml/core/ConstructorScanner;->primary:Lorg/simpleframework/xml/core/Initializer;

    invoke-direct {v0, v1, v2, v3}, Lorg/simpleframework/xml/core/ClassCreator;-><init>(Ljava/util/List;Lorg/simpleframework/xml/core/Signature;Lorg/simpleframework/xml/core/Initializer;)V

    return-object v0
.end method
