.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
.super Ljava/lang/Object;
.source "EDSV2ActivityProviderPolicy.java"


# instance fields
.field private isDefault:Z

.field private requiresParentPurchase:Z

.field private title:Ljava/lang/String;

.field private titleId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIsDefault()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->isDefault:Z

    return v0
.end method

.method public getRequiresParentPurchase()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->requiresParentPurchase:Z

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->titleId:J

    return-wide v0
.end method

.method public setIsDefault(Z)V
    .locals 0
    .param p1, "isDefault"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->isDefault:Z

    .line 13
    return-void
.end method

.method public setRequiresParentPurchase(Z)V
    .locals 0
    .param p1, "requiresParentPurchase"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->requiresParentPurchase:Z

    .line 21
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->title:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setTitleId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->titleId:J

    .line 37
    return-void
.end method
