.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
.source "$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private allowedInBroadcast:Ljava/lang/Boolean;

.field private clientType:Ljava/lang/Integer;

.field private deviceId:Ljava/lang/String;

.field private isBroadcasting:Ljava/lang/Boolean;

.field private protocolVersion:Ljava/lang/Integer;

.field private simpleConnectionState:Ljava/lang/Integer;

.field private webRtcDtlsCertificateAlgorithm:Ljava/lang/String;

.field private webRtcDtlsCertificateThumbprint:Ljava/lang/String;

.field private webRtcIcePwd:Ljava/lang/String;

.field private webRtcIceUfrag:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;-><init>()V

    .line 179
    return-void
.end method


# virtual methods
.method public allowedInBroadcast(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "allowedInBroadcast"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 202
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->allowedInBroadcast:Ljava/lang/Boolean;

    .line 203
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties;
    .locals 11

    .prologue
    .line 232
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->clientType:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->protocolVersion:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->simpleConnectionState:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->deviceId:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->allowedInBroadcast:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->isBroadcasting:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateAlgorithm:Ljava/lang/String;

    iget-object v8, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateThumbprint:Ljava/lang/String;

    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcIceUfrag:Ljava/lang/String;

    iget-object v10, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcIcePwd:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public clientType(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "clientType"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 182
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->clientType:Ljava/lang/Integer;

    .line 183
    return-object p0
.end method

.method public deviceId(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->deviceId:Ljava/lang/String;

    .line 198
    return-object p0
.end method

.method public isBroadcasting(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "isBroadcasting"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->isBroadcasting:Ljava/lang/Boolean;

    .line 208
    return-object p0
.end method

.method public protocolVersion(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 187
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->protocolVersion:Ljava/lang/Integer;

    .line 188
    return-object p0
.end method

.method public simpleConnectionState(Ljava/lang/Integer;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "simpleConnectionState"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 192
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->simpleConnectionState:Ljava/lang/Integer;

    .line 193
    return-object p0
.end method

.method public webRtcDtlsCertificateAlgorithm(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "webRtcDtlsCertificateAlgorithm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateAlgorithm:Ljava/lang/String;

    .line 213
    return-object p0
.end method

.method public webRtcDtlsCertificateThumbprint(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "webRtcDtlsCertificateThumbprint"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 217
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcDtlsCertificateThumbprint:Ljava/lang/String;

    .line 218
    return-object p0
.end method

.method public webRtcIcePwd(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "webRtcIcePwd"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 227
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcIcePwd:Ljava/lang/String;

    .line 228
    return-object p0
.end method

.method public webRtcIceUfrag(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberCustomProperties$Builder;
    .locals 0
    .param p1, "webRtcIceUfrag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerMemberCustomProperties$Builder;->webRtcIceUfrag:Ljava/lang/String;

    .line 223
    return-object p0
.end method
