.class public Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "ClubChatModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;,
        Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;,
        Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/chat/ChatEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final INITIAL_HISTORY_MESSAGE_COUNT:I = 0x50

.field private static final JOIN_TIMEOUT_MS:J = 0x2710L

.field private static final LOAD_HISTORY_THRESHOLD:F = 0.7f

.field private static final MORE_HISTORY_MESSAGE_COUNT:I = 0x1e

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private final chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

.field private final chatService:Lcom/microsoft/xbox/service/chat/IChatService;

.field private chatTicket:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

.field private final clubChatManagementService:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

.field private hasJoinedChannel:Z

.field private joinTimeoutFuture:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private loadMoreHistoryContinuationToken:J

.field private loadMoreHistoryRunnable:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;

.field private final loadMoreHistoryStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

.field private final messages:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
            ">;"
        }
    .end annotation
.end field

.field private numHistoryMessagesLoaded:I

.field private final typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

.field private webSocketConnectAsyncTask:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 2
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 82
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 84
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubChatManagementService()Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->clubChatManagementService:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getChatService()Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 89
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    .line 91
    new-instance v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;-><init>(Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 94
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/model/chat/ChatManager;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onWebSocketConnectCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;JI)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p1, "x1"    # J
    .param p3, "x2"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadHistoryInternal(JI)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onLoadMoreHistoryCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/chat/IChatService;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;ZLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onDeleteMessageCompleted(ZLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    return-void
.end method

.method private clear()V
    .locals 2

    .prologue
    .line 619
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    monitor-enter v1

    .line 620
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 621
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->clear()V

    .line 624
    return-void

    .line 621
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private clearTypingDataCache(Ljava/lang/String;)V
    .locals 3
    .param p1, "gamerTag"    # Ljava/lang/String;

    .prologue
    .line 570
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 572
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearTypingDataCache: the gamerTag is empty or null: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :goto_0
    return-void

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->clearTypingIndicator(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteCachedMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z
    .locals 3
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 312
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 315
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    monitor-enter v2

    .line 316
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 317
    .local v0, "deleted":Z
    monitor-exit v2

    .line 319
    return v0

    .line 317
    .end local v0    # "deleted":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private joinChannel()V
    .locals 5

    .prologue
    .line 126
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "joinChannel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "joinChannel: has already joined: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->sendJoinMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 135
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->joinTimeoutFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 141
    return-void
.end method

.method static synthetic lambda$joinChannel$1(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .prologue
    .line 136
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "joinChannel - timeout"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveChannel()V

    .line 139
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onError()V

    .line 140
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->IsTyping:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    return-void
.end method

.method static synthetic lambda$notifyObserversOnUiThread$2(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .param p1, "event"    # Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .prologue
    .line 648
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private loadHistoryInternal(JI)I
    .locals 11
    .param p1, "messageId"    # J
    .param p3, "count"    # I

    .prologue
    const-wide/16 v8, 0x0

    .line 276
    const/4 v1, 0x0

    .line 277
    .local v1, "numLoaded":I
    int-to-float v3, p3

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v3, v6

    float-to-int v2, v3

    .line 278
    .local v2, "numRequired":I
    move-wide v4, p1

    .line 280
    .local v4, "requestMessageId":J
    :goto_0
    if-ge v1, v2, :cond_0

    .line 281
    invoke-direct {p0, v4, v5, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->requestBackwardHistoryMessages(JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;

    move-result-object v0

    .line 282
    .local v0, "historyMessages":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    if-eqz v0, :cond_1

    .line 283
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->getMessages()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->updateMessageCacheWithHistory(Ljava/util/List;)I

    move-result v3

    add-int/2addr v1, v3

    .line 286
    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->continuationToken:J

    iput-wide v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    .line 291
    :goto_1
    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    cmp-long v3, v6, v8

    if-nez v3, :cond_2

    .line 298
    .end local v0    # "historyMessages":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    :cond_0
    return v1

    .line 288
    .restart local v0    # "historyMessages":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    :cond_1
    iput-wide v8, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    goto :goto_1

    .line 295
    :cond_2
    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    .line 296
    goto :goto_0
.end method

.method private loadInitialHistory(J)V
    .locals 3
    .param p1, "joinMessageId"    # J

    .prologue
    .line 268
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "loadInitialHistory"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 272
    const/16 v0, 0x50

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadHistoryInternal(JI)I

    .line 273
    return-void
.end method

.method private loadMessageOfTheDay()V
    .locals 3

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->requestMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;

    move-result-object v0

    .line 325
    .local v0, "chatMessageOfTheDay":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageStatus:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    sget-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Ok:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    if-ne v1, v2, :cond_0

    .line 328
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v2, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 330
    :cond_0
    return-void
.end method

.method private notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    .prologue
    .line 648
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/model/chat/ChatEvent;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 649
    return-void
.end method

.method private onDeleteMessageCompleted(ZLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 3
    .param p1, "success"    # Z
    .param p2, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .prologue
    .line 609
    if-eqz p1, :cond_0

    .line 610
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->deleteCachedMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z

    .line 616
    :goto_0
    return-void

    .line 613
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onDeleteMessageCompleted - delete failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->DeleteFailed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method

.method private onLoadMoreHistoryCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 595
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Integer;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadMoreHistoryCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 598
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 599
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->numHistoryMessagesLoaded:I

    .line 600
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->History:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 606
    :goto_0
    return-void

    .line 603
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->numHistoryMessagesLoaded:I

    .line 604
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->History:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method

.method private onWebSocketConnectCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 580
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onWebSocketConnectCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onWebSocketConnectCompleted receive unsupported status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 592
    :goto_0
    return-void

    .line 584
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->joinChannel()V

    goto :goto_0

    .line 587
    :pswitch_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onError()V

    goto :goto_0

    .line 581
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private requestBackwardHistoryMessages(JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    .locals 7
    .param p1, "messageId"    # J
    .param p3, "count"    # I

    .prologue
    .line 229
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    sget-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v3

    move-wide v4, p1

    move v6, p3

    invoke-interface/range {v1 .. v6}, Lcom/microsoft/xbox/service/chat/IChatService;->getBackwardHistoryMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 236
    :goto_0
    return-object v1

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestBackwardHistoryMessages failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private requestMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    .locals 4

    .prologue
    .line 241
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 244
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    sget-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/chat/IChatService;->getMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 248
    :goto_0
    return-object v1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestMessageOfTheDay failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private requestTicket()Z
    .locals 4

    .prologue
    .line 215
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 217
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v2, "requestTicket"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->clubChatManagementService:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;->refreshClubChatTicket(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 224
    :goto_0
    return v1

    .line 221
    :catch_0
    move-exception v0

    .line 223
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestTicket throws exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateMessageCache(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z
    .locals 4
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 538
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateMessageCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 542
    iget-object v0, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 544
    .local v0, "messageType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 554
    const-string v1, "not supported message type"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 555
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 549
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    monitor-enter v2

    .line 550
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v2

    goto :goto_0

    .line 551
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 544
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateMessageCacheWithHistory(Ljava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 507
    .local p1, "historyMessages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 509
    const/4 v0, 0x0

    .line 510
    .local v0, "count":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    monitor-enter v3

    .line 511
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    .line 512
    .local v1, "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    iget-object v4, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageStatus:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    sget-object v5, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Ok:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    if-ne v4, v5, :cond_0

    iget-object v4, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    if-eqz v4, :cond_0

    .line 514
    sget-object v4, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    iget-object v5, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 524
    sget-object v4, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateMessageCacheWithHistory - filter out messageType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 530
    .end local v1    # "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 518
    .restart local v1    # "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    :pswitch_0
    :try_start_1
    new-instance v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-direct {v4, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->updateMessageCache(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 529
    .end local v1    # "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 533
    .end local v0    # "count":I
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 514
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateTypingDataCache(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .param p1, "gamerTag"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/util/Date;

    .prologue
    .line 561
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTypingDataCache: the gamerTag is empty or null: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->updateTypingIndicator(Ljava/lang/String;Ljava/util/Date;)V

    goto :goto_0
.end method


# virtual methods
.method public connectAndJoinIfNecessary()V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 108
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "connectAndJoinIfNecessary - should connect and join"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->connectWebSocketAndJoin()V

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "connectAndJoinIfNecessary - has alread joined"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Joined:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    goto :goto_0
.end method

.method public connectWebSocketAndJoin()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->webSocketConnectAsyncTask:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->webSocketConnectAsyncTask:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->cancel()V

    .line 121
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->webSocketConnectAsyncTask:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->webSocketConnectAsyncTask:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$WebSocketConnectAsyncTask;->load(Z)V

    .line 123
    return-void
.end method

.method public deleteMessageAsync(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 4
    .param p1, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 302
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteMessageAsync - message:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 305
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 307
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    .line 308
    .local v0, "deleteMessageAsyncTask":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->load(Z)V

    .line 309
    return-void
.end method

.method public getChannelId()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 628
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method public getChatTicket()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatTicket:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    return-object v0
.end method

.method public getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    return-object v0
.end method

.method public getMessages()Ljava/util/List;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    monitor-enter v1

    .line 634
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messages:Ljava/util/TreeSet;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 635
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNumHistoryMessagesLoaded()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->numHistoryMessagesLoaded:I

    return v0
.end method

.method public getTypingData()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->getTypingIndicatorList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasJoinedChannel()Z
    .locals 1

    .prologue
    .line 644
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    return v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getObservers()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveChannel()V
    .locals 3

    .prologue
    .line 151
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "leaveChannel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 154
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "leaveChannel: not in the channel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->leaveJoinedChannel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 163
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->stopTypingExpirationTask()V

    .line 165
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->clear()V

    .line 166
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->closeWebSocketIfNoActiveChannels()V

    goto :goto_0
.end method

.method public leaveNotAllowedChannel()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveChannel()V

    .line 146
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "leaveNotAllowedChannel - notify NotAllowed event"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->NotAllowed:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    .line 148
    return-void
.end method

.method public loadMoreHistoryAsync()Z
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    .line 253
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v4, "loadMoreHistoryAsync"

    invoke-static {v0, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 257
    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    cmp-long v0, v4, v2

    if-nez v0, :cond_0

    .line 258
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "loadMoreHistoryAsync - loadMoreHistoryContinuationToken is 0"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const/4 v1, 0x0

    .line 264
    :goto_0
    return v1

    .line 262
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryContinuationToken:J

    invoke-direct {v0, p0, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;J)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryRunnable:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;

    .line 263
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMoreHistoryRunnable:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$LoadMoreHistoryRunnable;

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    goto :goto_0
.end method

.method public onConnected()V
    .locals 2

    .prologue
    .line 376
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onWebSocketConnectCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 379
    return-void
.end method

.method public onDeleted(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;)V
    .locals 5
    .param p1, "chatHeader"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 484
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDeleted: chatHeader:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 488
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;)V

    .line 490
    .local v1, "deletedMessage":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v3, "onDeleted - delete current message of the day"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 493
    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->MessageOfTheDay:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    .line 504
    :goto_0
    return-void

    .line 496
    :cond_0
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->deleteCachedMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z

    move-result v0

    .line 498
    .local v0, "deleted":Z
    if-eqz v0, :cond_1

    .line 499
    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Deleted:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    goto :goto_0

    .line 501
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v3, "onDeleted - not exist in cache"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 369
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onError"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    .line 372
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Error:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    .line 373
    return-void
.end method

.method public onJoined(J)V
    .locals 3
    .param p1, "joinMessageId"    # J

    .prologue
    const/4 v2, 0x1

    .line 351
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onJoined"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->joinTimeoutFuture:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 355
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 356
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onJoined - join successful"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->typingIndicatorManager:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->startTypingExpirationTask()V

    .line 360
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->clear()V

    .line 361
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadInitialHistory(J)V

    .line 362
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->loadMessageOfTheDay()V

    .line 365
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Joined:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    .line 366
    return-void
.end method

.method public onMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V
    .locals 5
    .param p1, "chatHeader"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "messageText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 442
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 443
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 445
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v3, :cond_1

    .line 446
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v4, "onMessage - not joined the channel"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 450
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    .line 452
    .local v1, "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    iget-object v4, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 479
    const-string v3, "not supported message type"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 454
    :pswitch_1
    iget-object v2, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    .line 455
    .local v2, "messageXuid":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "meXuid":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 458
    iget-object v3, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    iget-object v4, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->updateTypingDataCache(Ljava/lang/String;Ljava/util/Date;)V

    goto :goto_0

    .line 471
    .end local v0    # "meXuid":Ljava/lang/String;
    .end local v2    # "messageXuid":Ljava/lang/String;
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->updateMessageCache(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 472
    sget-object v3, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->Message:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    .line 475
    :cond_2
    iget-object v3, v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->clearTypingDataCache(Ljava/lang/String;)V

    goto :goto_0

    .line 452
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V
    .locals 3
    .param p1, "chatheader"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "messageOfTheDayText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 426
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMessageOfTheDayUpdated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " text: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 429
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 431
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 432
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onMessageOfTheDay - not joined the channel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :goto_0
    return-void

    .line 436
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 438
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->MessageOfTheDay:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    goto :goto_0
.end method

.method public onTicketRefresh()V
    .locals 2

    .prologue
    .line 397
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onTicketRefresh"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_1

    .line 400
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onTicketRefresh - not joined the channel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->requestTicket()Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v1, "onTicketRefresh - refresh ticket failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveNotAllowedChannel()V

    goto :goto_0
.end method

.method public onTicketRequest()V
    .locals 3

    .prologue
    .line 382
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v2, "onTicketRequest"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->joinTimeoutFuture:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 386
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->requestTicket()Z

    move-result v0

    .line 387
    .local v0, "success":Z
    if-eqz v0, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->joinChannel()V

    .line 394
    :goto_0
    return-void

    .line 391
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    const-string v2, "onTicketRequest - request ticket failed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveNotAllowedChannel()V

    goto :goto_0
.end method

.method public onUserInfo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;)V
    .locals 3
    .param p1, "chatTicket"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 411
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUserInfo - ticket:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 415
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatTicket:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    .line 418
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatTicket:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->canRead()Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveNotAllowedChannel()V

    .line 423
    :goto_0
    return-void

    .line 421
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatEvent;->UserInfo:Lcom/microsoft/xbox/service/model/chat/ChatEvent;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->notifyObserversOnUiThread(Lcom/microsoft/xbox/service/model/chat/ChatEvent;)V

    goto :goto_0
.end method

.method public removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/chat/ChatEvent;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/chat/ChatEvent;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEObservable;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    move-result v0

    .line 99
    .local v0, "success":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->getObservers()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->leaveChannel()V

    .line 103
    :cond_0
    return v0
.end method

.method public sendDirectMentionMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Ljava/util/List;)V
    .locals 3
    .param p1, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p2, "directMentionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendDirectMentionMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " directMentionList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 193
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 195
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 196
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendDirectMentionMessage: not in the channel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1, p1, p2}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->sendDirectMentionMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Ljava/util/List;)V

    goto :goto_0
.end method

.method public sendIsTypingMessage()V
    .locals 3

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 171
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendIsTypingMessage: not in the channel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->sendIsTypingMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    goto :goto_0
.end method

.method public sendMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 3
    .param p1, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 179
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 182
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 183
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendMessage: not in the channel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->sendMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    goto :goto_0
.end method

.method public setMessageOfTheDay(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 204
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMessageOfTheDay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel:Z

    if-nez v0, :cond_0

    .line 207
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMessageOfTheDay: not in the channel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->chatManager:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->channelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->setMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Ljava/lang/String;)V

    goto :goto_0
.end method
