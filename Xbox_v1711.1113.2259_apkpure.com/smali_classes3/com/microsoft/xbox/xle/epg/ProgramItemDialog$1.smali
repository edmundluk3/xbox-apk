.class Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;
.super Ljava/lang/Object;
.source "ProgramItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field final synthetic val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->tuneToProgram(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v0

    .line 101
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 102
    const v1, 0x7f070455

    invoke-static {v3, v1, v3}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$1;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->safeDismiss()V

    .line 108
    .end local v0    # "success":Z
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    goto :goto_0
.end method
