.class public Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
.super Lcom/microsoft/xbox/toolkit/ui/XLEImageView;
.source "XLEConstrainedImageView.java"


# static fields
.field public static final FIX_HEIGHT:I = 0x2

.field public static final FIX_WIDTH:I = 0x1

.field private static final LOG_DEBUG:Z = false

.field private static final NOT_DEFINED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "XLEConstrainedImageView"


# instance fields
.field private aspectX:I

.field private aspectY:I

.field private bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

.field private currentErrorResourceId:I

.field private currentLoadingResourceId:I

.field private currentUri:Ljava/lang/String;

.field private fixHeight:Z

.field private fixWidth:Z

.field private maxHeight:I

.field private maxWidth:I

.field private measureCount:I

.field private measuredHeight:I

.field private measuredWidth:I

.field private paddingBottom:I

.field private paddingLeft:I

.field private paddingRight:I

.field private paddingTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const v5, 0x7fffffff

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measureCount:I

    .line 32
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    .line 33
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    .line 42
    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentLoadingResourceId:I

    .line 43
    sget v2, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentErrorResourceId:I

    .line 44
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentUri:Ljava/lang/String;

    .line 57
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->XLEConstrainedImageView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 59
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxWidth:I

    .line 60
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxHeight:I

    .line 61
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    .line 62
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    .line 64
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 65
    .local v1, "fixDimensionFlags":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setFixDimension(I)V

    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setSoundEffectsEnabled(Z)V

    .line 73
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->initPaddingVars()V

    .line 74
    return-void
.end method

.method private bindIfPossible()V
    .locals 3

    .prologue
    .line 254
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredHeight:I

    if-lez v0, :cond_0

    const v0, 0x7f0e001c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 255
    const v0, 0x7f0e001d

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setTag(ILjava/lang/Object;)V

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentUri:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentLoadingResourceId:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentErrorResourceId:I

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 258
    :cond_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private calcMeasuredDimension(II)V
    .locals 3
    .param p1, "maxImageWidth"    # I
    .param p2, "maxImageHeight"    # I

    .prologue
    .line 243
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->sizeToAR(II)[I

    move-result-object v0

    .line 245
    .local v0, "size":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingLeft:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingRight:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredWidth:I

    .line 246
    const/4 v1, 0x1

    aget v1, v0, v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingTop:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingBottom:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredHeight:I

    .line 251
    return-void
.end method

.method private checkPrereqsAndFailFast(IIII)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I
    .param p3, "wSpecMode"    # I
    .param p4, "hSpecMode"    # I

    .prologue
    const/4 v2, -0x1

    .line 209
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    if-ne v1, v2, :cond_1

    .line 210
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getResourceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Both aspectX and aspectY must be defined when either or both of layout_width or layout_height is \'wrap_content\' or \'match_parent\'/\'fill_parent\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 213
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    if-nez v1, :cond_3

    .line 214
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getResourceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": neither aspectX nor aspectY should be zero.  aspectX=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", aspectY=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 218
    :cond_3
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    .line 219
    .local v0, "isSquare":Z
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getResourceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": fixDimension=\'width|height\' requires that aspectX == aspectY (i.e. a ratio of 1.0, or a square).  aspectX=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", aspectY=="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 223
    :cond_4
    return-void

    .line 218
    .end local v0    # "isSquare":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMeasureSpecModeName(I)Ljava/lang/String;
    .locals 1
    .param p1, "measureSpec"    # I

    .prologue
    .line 230
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 238
    const-string v0, "[Invalid MeasureSpec Type]"

    :goto_0
    return-object v0

    .line 232
    :sswitch_0
    const-string v0, "AT_MOST"

    goto :goto_0

    .line 234
    :sswitch_1
    const-string v0, "EXACTLY"

    goto :goto_0

    .line 236
    :sswitch_2
    const-string v0, "UNSPECIFIED"

    goto :goto_0

    .line 230
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method private getResourceName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "R.id."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initPaddingVars()V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getLeftPaddingOffset()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingLeft:I

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getRightPaddingOffset()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingRight:I

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getTopPaddingOffset()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingTop:I

    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->getBottomPaddingOffset()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingBottom:I

    .line 81
    return-void
.end method

.method private sizeToAR(II)[I
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 261
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    int-to-float v6, v6

    div-float v1, v5, v6

    .line 263
    .local v1, "calculatedAspectRatio":F
    move v3, p2

    .line 264
    .local v3, "newHeight":I
    move v4, p1

    .line 269
    .local v4, "newWidth":I
    move v0, v1

    .line 271
    .local v0, "bmpAspectRatio":F
    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-lez v5, :cond_0

    .line 272
    move v0, v1

    .line 275
    :cond_0
    int-to-float v5, p2

    int-to-float v6, p1

    div-float v2, v5, v6

    .line 277
    .local v2, "destAspectRatio":F
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    if-eqz v5, :cond_3

    .line 279
    cmpg-float v5, v2, v0

    if-gez v5, :cond_2

    .line 281
    move v3, p2

    .line 282
    int-to-float v5, p2

    div-float/2addr v5, v0

    float-to-int v4, v5

    .line 300
    :cond_1
    :goto_0
    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v4, v5, v6

    const/4 v6, 0x1

    aput v3, v5, v6

    return-object v5

    .line 285
    :cond_2
    move v4, p1

    .line 286
    int-to-float v5, p1

    mul-float/2addr v5, v0

    float-to-int v3, v5

    goto :goto_0

    .line 288
    :cond_3
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    if-eqz v5, :cond_4

    .line 290
    move v4, p1

    .line 291
    int-to-float v5, p1

    mul-float/2addr v5, v0

    float-to-int v3, v5

    goto :goto_0

    .line 292
    :cond_4
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    if-eqz v5, :cond_1

    .line 294
    move v3, p2

    .line 295
    int-to-float v5, p2

    div-float/2addr v5, v0

    float-to-int v4, v5

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 168
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measureCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measureCount:I

    .line 170
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 171
    .local v6, "wSpecMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 172
    .local v7, "wSpecSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 173
    .local v0, "hSpecMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 181
    .local v1, "hSpecSize":I
    if-ne v6, v9, :cond_0

    if-ne v0, v9, :cond_0

    .line 182
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxWidth:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxHeight:I

    invoke-static {v1, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setMeasuredDimension(II)V

    .line 183
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->bindIfPossible()V

    .line 197
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-direct {p0, p1, p2, v6, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->checkPrereqsAndFailFast(IIII)V

    .line 189
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingLeft:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingRight:I

    add-int v2, v8, v9

    .line 190
    .local v2, "horizontalPadding":I
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingTop:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->paddingBottom:I

    add-int v5, v8, v9

    .line 191
    .local v5, "verticalPadding":I
    sub-int v8, v1, v5

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxHeight:I

    sub-int/2addr v9, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 192
    .local v3, "maxBitmapHeight":I
    sub-int v8, v7, v2

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxWidth:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 194
    .local v4, "maxBitmapWidth":I
    invoke-direct {p0, v4, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->calcMeasuredDimension(II)V

    .line 195
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredWidth:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredHeight:I

    invoke-virtual {p0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setMeasuredDimension(II)V

    .line 196
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->bindIfPossible()V

    goto :goto_0
.end method

.method public setAspectX(I)V
    .locals 0
    .param p1, "aspectX"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    .line 106
    return-void
.end method

.method public setAspectXY([I)V
    .locals 5
    .param p1, "aspectXY"    # [I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    const-string v3, "aspectXY must be an int[2] with [aspectX, aspectY]"

    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 114
    aget v0, p1, v2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectX:I

    .line 115
    aget v0, p1, v1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    .line 116
    return-void

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0
.end method

.method public setAspectY(I)V
    .locals 0
    .param p1, "aspectY"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->aspectY:I

    .line 110
    return-void
.end method

.method public setBitmapSetListener(Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->bitmapSetListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    .line 128
    return-void
.end method

.method public setFixDimension(I)V
    .locals 3
    .param p1, "fixFlags"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 91
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    .line 92
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixWidth:Z

    .line 99
    :cond_2
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->containsFlag(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->fixHeight:Z

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    const/4 v0, 0x0

    .line 156
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredWidth:I

    .line 157
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredHeight:I

    .line 159
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->requestLayout()V

    .line 160
    return-void
.end method

.method public setImageURI2(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "defaultResourceId"    # I

    .prologue
    .line 131
    invoke-virtual {p0, p1, p2, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 132
    return-void
.end method

.method public setImageURI2(Ljava/lang/String;II)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "loadingResourceId"    # I
    .param p3, "errorResourceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 135
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentLoadingResourceId:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentErrorResourceId:I

    if-eq p3, v0, :cond_2

    .line 138
    :cond_1
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentLoadingResourceId:I

    .line 139
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentErrorResourceId:I

    .line 140
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->currentUri:Ljava/lang/String;

    .line 143
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredWidth:I

    .line 144
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->measuredHeight:I

    .line 145
    const v0, 0x7f0e001c

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setTag(ILjava/lang/Object;)V

    .line 147
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->bindIfPossible()V

    .line 149
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->requestLayout()V

    .line 151
    :cond_2
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1, "maxHeight"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxHeight:I

    .line 124
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1, "maxWidth"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->maxWidth:I

    .line 120
    return-void
.end method

.method public setPadding(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 85
    invoke-super {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setPadding(IIII)V

    .line 86
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->initPaddingVars()V

    .line 87
    return-void
.end method
