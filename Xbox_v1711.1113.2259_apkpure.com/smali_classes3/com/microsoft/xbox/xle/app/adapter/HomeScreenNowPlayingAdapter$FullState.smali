.class public final enum Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
.super Ljava/lang/Enum;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FullState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

.field public static final enum APP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

.field public static final enum GAME:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

.field public static final enum NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

.field public static final enum PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 731
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    const-string v1, "PROMO"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    const-string v1, "GAME"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->GAME:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    const-string v1, "APP"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->APP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    const-string v1, "NO_STATS"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    .line 730
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->GAME:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->APP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 730
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 730
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
    .locals 1

    .prologue
    .line 730
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    return-object v0
.end method
