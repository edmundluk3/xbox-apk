.class public Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConversationsViewAllMembersScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    const-string v0, "Conversations View All Member"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;->onCreateContentView()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationViewAllMembersScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 13
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    const v0, 0x7f0300c8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;->setContentView(I)V

    .line 24
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;->setShowUtilityBar(Z)V

    .line 25
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;->setShowRightPane(Z)V

    .line 26
    return-void
.end method
