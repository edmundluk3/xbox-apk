.class Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubCreationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateClubTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubName:Ljava/lang/String;

.field private final clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    .locals 0
    .param p2, "clubName"    # Ljava/lang/String;
    .param p3, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 307
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubName:Ljava/lang/String;

    .line 308
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 309
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 4

    .prologue
    .line 328
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    .line 330
    .local v0, "clubAccountsService":Lcom/microsoft/xbox/service/clubs/IClubAccountsService;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubName:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->createClub(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 334
    :goto_0
    return-object v2

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    .line 334
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$702(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubName:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->id()J

    move-result-wide v2

    const-string v4, "Yes"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateClubResult(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;JLjava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$800(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->id()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;->onClubCreated(J)V

    .line 352
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubName:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    const-wide/16 v2, 0x0

    const-string v4, "No"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateClubResult(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;JLjava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$800(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->code()I

    move-result v0

    :goto_1
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;->onClubCreationError(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 301
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$CreateClubTask;->onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 340
    return-void
.end method
