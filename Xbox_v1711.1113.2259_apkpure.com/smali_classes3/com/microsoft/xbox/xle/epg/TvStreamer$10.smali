.class Lcom/microsoft/xbox/xle/epg/TvStreamer$10;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupFullScreenUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v6, 0x1388

    const/16 v5, 0x8

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 468
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_4

    .line 471
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$300(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    .line 472
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 473
    :cond_0
    const-string v2, "TvStreamer"

    const-string v3, "Hide MTC controls"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 476
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$600(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 477
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 478
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$700(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 499
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$800(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 500
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$900(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getCurrentSource()Ljava/lang/String;

    move-result-object v3

    const-string v4, "tuner"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsLoading()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1000(Lcom/microsoft/xbox/xle/epg/TvStreamer;Z)V

    .line 504
    :cond_2
    :goto_1
    return v1

    .line 480
    :cond_3
    const-string v2, "TvStreamer"

    const-string v3, "Show MTC controls"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const-string v2, "TvStreamer"

    const-string v3, "Requesting LiveTVInfo"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestLiveTvInfo()Z

    .line 487
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 488
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 489
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$600(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 490
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setAlpha(F)V

    .line 491
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 492
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$700(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 495
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$600(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v6, v7}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 496
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$10;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$700(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v6, v7}, Landroid/widget/Button;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_4
    move v1, v0

    .line 504
    goto :goto_1
.end method
