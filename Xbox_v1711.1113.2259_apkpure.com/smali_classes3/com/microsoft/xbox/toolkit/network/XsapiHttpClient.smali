.class public Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;
.super Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;
.source "XsapiHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/AbstractXLEHttpClient;-><init>()V

    return-void
.end method

.method private addBody(Lcom/microsoft/xbox/idp/util/HttpCall;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 5
    .param p1, "httpCall"    # Lcom/microsoft/xbox/idp/util/HttpCall;
    .param p2, "req"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    instance-of v4, p2, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    if-eqz v4, :cond_2

    move-object v2, p2

    .line 131
    check-cast v2, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    .line 132
    .local v2, "requestBase":Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;
    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 133
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    const/4 v0, 0x0

    .line 134
    .local v0, "body":[B
    if-eqz v1, :cond_0

    .line 135
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 136
    .local v3, "stream":Ljava/io/InputStream;
    if-eqz v3, :cond_0

    .line 137
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->toBuffer(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 140
    .end local v3    # "stream":Ljava/io/InputStream;
    :cond_0
    if-nez v0, :cond_1

    .line 141
    const/4 v4, 0x0

    new-array v0, v4, [B

    .line 143
    :cond_1
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/idp/util/HttpCall;->setRequestBody([B)V

    .line 145
    .end local v0    # "body":[B
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v2    # "requestBase":Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;
    :cond_2
    return-void
.end method

.method static synthetic lambda$getHttpStatusAndStreamInternal$0(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;ILjava/io/InputStream;Lcom/microsoft/xbox/idp/util/HttpHeaders;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;
    .param p1, "rd"    # Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;
    .param p2, "latch"    # Ljava/util/concurrent/CountDownLatch;
    .param p3, "httpStatus"    # I
    .param p4, "stream"    # Ljava/io/InputStream;
    .param p5, "headers"    # Lcom/microsoft/xbox/idp/util/HttpHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    :try_start_0
    iput p3, p1, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->httpStatus:I

    .line 69
    invoke-direct {p0, p4}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->toBuffer(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 70
    .local v0, "buffer":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v2, p1, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->stream:Ljava/io/InputStream;

    .line 71
    array-length v2, v0

    int-to-long v2, v2

    iput-wide v2, p1, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->streamLength:J

    .line 72
    iput-object p5, p1, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->headers:Lcom/microsoft/xbox/idp/util/HttpHeaders;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 79
    .end local v0    # "buffer":[B
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v1

    .line 74
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->TAG:Ljava/lang/String;

    const-string v3, "getResponseAsync call back ex: "

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->stopSampling()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v2
.end method

.method private toApacheDeprecatedHeaders(Ljava/util/Collection;)[Lorg/apache/http/Header;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;",
            ">;)[",
            "Lorg/apache/http/Header;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "col":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;>;"
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v1, v3, [Lorg/apache/http/Header;

    .line 122
    .local v1, "headers":[Lorg/apache/http/Header;
    const/4 v2, -0x1

    .line 123
    .local v2, "idx":I
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;

    .line 124
    .local v0, "h":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    add-int/lit8 v2, v2, 0x1

    new-instance v4, Lorg/apache/http/message/BasicHeader;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v1, v2

    goto :goto_0

    .line 126
    .end local v0    # "h":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    :cond_0
    return-object v1
.end method

.method private toBuffer(Ljava/io/InputStream;)[B
    .locals 5
    .param p1, "s"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 150
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 152
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 155
    .local v1, "bis":Ljava/io/BufferedInputStream;
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->read()I

    move-result v3

    .local v3, "ch":I
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 156
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 159
    .end local v3    # "ch":I
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 164
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 167
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_2
    move-exception v4

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v4

    .line 159
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v3    # "ch":I
    :cond_0
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 161
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 162
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v4

    .line 164
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 167
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 162
    return-object v4
.end method


# virtual methods
.method protected execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p1, "req"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCookieStore()Lorg/apache/http/client/CookieStore;
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHttpStatusAndStreamInternal(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 14
    .param p1, "req"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "printStreamDebug"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v9}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 45
    .local v8, "uri":Landroid/net/Uri;
    new-instance v3, Lcom/microsoft/xbox/idp/util/HttpCall;

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8}, Lcom/microsoft/xbox/idp/util/HttpUtil;->getEndpoint(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v8}, Lcom/microsoft/xbox/idp/util/HttpUtil;->getPathAndQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v3, v9, v10, v11, v12}, Lcom/microsoft/xbox/idp/util/HttpCall;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 48
    .local v3, "httpCall":Lcom/microsoft/xbox/idp/util/HttpCall;
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v10

    array-length v11, v10

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v11, :cond_1

    aget-object v2, v10, v9

    .line 50
    .local v2, "h":Lorg/apache/http/Header;
    const-string v12, "Authorization"

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 51
    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Lcom/microsoft/xbox/idp/util/HttpCall;->setCustomHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "h":Lorg/apache/http/Header;
    :cond_1
    :try_start_0
    invoke-direct {p0, v3, p1}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->addBody(Lcom/microsoft/xbox/idp/util/HttpCall;Lorg/apache/http/client/methods/HttpUriRequest;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    new-instance v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;

    const/4 v9, 0x0

    invoke-direct {v6, v9}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;-><init>(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$1;)V

    .line 62
    .local v6, "rd":Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    const/4 v9, 0x1

    invoke-direct {v4, v9}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 64
    .local v4, "latch":Ljava/util/concurrent/CountDownLatch;
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->startSampling()V

    .line 66
    invoke-static {p0, v6, v4}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;Ljava/util/concurrent/CountDownLatch;)Lcom/microsoft/xbox/idp/util/HttpCall$Callback;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/idp/util/HttpCall;->getResponseAsync(Lcom/microsoft/xbox/idp/util/HttpCall$Callback;)V

    .line 82
    :try_start_1
    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 89
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->stopSampling()V

    .line 91
    new-instance v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    invoke-direct {v7}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;-><init>()V

    .line 93
    .local v7, "rv":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iget v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->httpStatus:I

    iput v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    .line 94
    iget v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->httpStatus:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    .line 96
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->headers:Lcom/microsoft/xbox/idp/util/HttpHeaders;

    const-string v10, "Location"

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/idp/util/HttpHeaders;->getLastHeader(Ljava/lang/String;)Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;

    move-result-object v5

    .line 97
    .local v5, "location":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    if-eqz v5, :cond_2

    .line 98
    invoke-virtual {v5}, Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;->getValue()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->redirectUrl:Ljava/lang/String;

    .line 101
    :cond_2
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->headers:Lcom/microsoft/xbox/idp/util/HttpHeaders;

    invoke-virtual {v9}, Lcom/microsoft/xbox/idp/util/HttpHeaders;->getAllHeaders()Ljava/util/Collection;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->toApacheDeprecatedHeaders(Ljava/util/Collection;)[Lorg/apache/http/Header;

    move-result-object v9

    iput-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    .line 103
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_3

    .line 104
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->stream:Ljava/io/InputStream;

    iput-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    .line 105
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->headers:Lcom/microsoft/xbox/idp/util/HttpHeaders;

    const-string v10, "Content-Encoding"

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/idp/util/HttpHeaders;->getFirstHeader(Ljava/lang/String;)Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;

    move-result-object v1

    .line 106
    .local v1, "encoding":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    if-eqz v1, :cond_3

    const-string v9, "gzip"

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 108
    :try_start_2
    new-instance v9, Ljava/util/zip/GZIPInputStream;

    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-direct {v9, v10}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 115
    .end local v1    # "encoding":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    :cond_3
    iget-wide v10, v6, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;->streamLength:J

    iput-wide v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->streamLength:J

    .line 117
    return-object v7

    .line 57
    .end local v4    # "latch":Ljava/util/concurrent/CountDownLatch;
    .end local v5    # "location":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    .end local v6    # "rd":Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;
    .end local v7    # "rv":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x10

    invoke-direct {v9, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9

    .line 83
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "latch":Ljava/util/concurrent/CountDownLatch;
    .restart local v6    # "rd":Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient$ResponseData;
    :catch_1
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/XsapiHttpClient;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "InterruptedException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v9, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->stopSampling()V

    .line 86
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x13

    invoke-direct {v9, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9

    .line 109
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "encoding":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    .restart local v5    # "location":Lcom/microsoft/xbox/idp/util/HttpHeaders$Header;
    .restart local v7    # "rv":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_2
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x4

    invoke-direct {v9, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
.end method

.method public setCookieStore(Lorg/apache/http/client/CookieStore;)V
    .locals 0
    .param p1, "store"    # Lorg/apache/http/client/CookieStore;

    .prologue
    .line 184
    return-void
.end method
