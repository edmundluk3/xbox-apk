.class public Lcom/microsoft/xbox/xle/remote/TextInputControl;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "TextInputControl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TextInputControl"


# instance fields
.field private final clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final closeButton:Landroid/view/View;

.field private final confirmButton:Landroid/widget/RelativeLayout;

.field private final editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private final prompt:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const v0, 0x7f0802b1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 37
    const v0, 0x7f03022a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->setContentView(I)V

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->setCanceledOnTouchOutside(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->makeFullScreen()V

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControl;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    .line 42
    const v0, 0x7f0e0abc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 43
    const v0, 0x7f0e0abd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 44
    const v0, 0x7f0e0ab7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->closeButton:Landroid/view/View;

    .line 45
    const v0, 0x7f0e0abb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->confirmButton:Landroid/widget/RelativeLayout;

    .line 46
    const v0, 0x7f0e0ab9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->prompt:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    return-void
.end method

.method private handleConfirmOrCancelButtonClick(Z)V
    .locals 1
    .param p1, "confirm"    # Z

    .prologue
    .line 225
    if-eqz p1, :cond_0

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->completeTextSession()V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->cancelTextSession()V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/remote/TextInputControl;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/remote/TextInputControl;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->handleConfirmOrCancelButtonClick(Z)V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/remote/TextInputControl;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->handleConfirmOrCancelButtonClick(Z)V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/remote/TextInputControl;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->updateDeviceTextBuffer(Ljava/lang/String;)V

    .line 99
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/remote/TextInputControl;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/remote/TextInputControl;
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 102
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 103
    .local v0, "text":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 104
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->handleConfirmOrCancelButtonClick(Z)V

    .line 107
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static launchIfNotRunning()V
    .locals 4

    .prologue
    .line 50
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    .line 51
    .local v0, "dialogManager":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v2, v3, :cond_0

    .line 52
    const-string v2, "TextInputControl"

    const-string v3, "Stream is playing in EPG. Do not pop keyboard"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/xle/remote/RemoteControl;

    if-eqz v2, :cond_1

    .line 54
    const-string v2, "TextInputControl"

    const-string v3, "launch textinput ui"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    new-instance v1, Lcom/microsoft/xbox/xle/remote/TextInputControl;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/remote/TextInputControl;-><init>(Landroid/content/Context;)V

    .line 56
    .local v1, "textInputControl":Lcom/microsoft/xbox/xle/remote/TextInputControl;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0

    .line 59
    .end local v1    # "textInputControl":Lcom/microsoft/xbox/xle/remote/TextInputControl;
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/xle/remote/TextInputControl;

    if-eqz v2, :cond_2

    .line 60
    const-string v2, "TextInputControl"

    const-string v3, "either textinput ui is already running or a companion is running."

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    goto :goto_0
.end method

.method private setMaxLength(I)V
    .locals 10
    .param p1, "length"    # I

    .prologue
    const/4 v6, 0x0

    .line 200
    if-lez p1, :cond_3

    .line 203
    iget-object v7, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    .line 204
    .local v1, "filters":[Landroid/text/InputFilter;
    const/4 v5, 0x0

    .line 205
    .local v5, "nonMaxLengthFilterCount":I
    array-length v8, v1

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v0, v1, v7

    .line 206
    .local v0, "filter":Landroid/text/InputFilter;
    instance-of v9, v0, Landroid/text/InputFilter$LengthFilter;

    if-nez v9, :cond_0

    .line 207
    add-int/lit8 v5, v5, 0x1

    .line 205
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 211
    .end local v0    # "filter":Landroid/text/InputFilter;
    :cond_1
    add-int/lit8 v7, v5, 0x1

    new-array v4, v7, [Landroid/text/InputFilter;

    .line 212
    .local v4, "newFilters":[Landroid/text/InputFilter;
    new-instance v7, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v7, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v7, v4, v6

    .line 213
    const/4 v2, 0x1

    .line 214
    .local v2, "i":I
    array-length v7, v1

    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v0, v1, v6

    .line 215
    .restart local v0    # "filter":Landroid/text/InputFilter;
    instance-of v8, v0, Landroid/text/InputFilter$LengthFilter;

    if-nez v8, :cond_4

    .line 216
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aput-object v0, v4, v3

    .line 214
    :goto_2
    add-int/lit8 v6, v6, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 220
    .end local v0    # "filter":Landroid/text/InputFilter;
    :cond_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 222
    .end local v1    # "filters":[Landroid/text/InputFilter;
    .end local v3    # "i":I
    .end local v4    # "newFilters":[Landroid/text/InputFilter;
    .end local v5    # "nonMaxLengthFilterCount":I
    :cond_3
    return-void

    .restart local v0    # "filter":Landroid/text/InputFilter;
    .restart local v1    # "filters":[Landroid/text/InputFilter;
    .restart local v3    # "i":I
    .restart local v4    # "newFilters":[Landroid/text/InputFilter;
    .restart local v5    # "nonMaxLengthFilterCount":I
    :cond_4
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2
.end method

.method private setSelection(II)V
    .locals 6
    .param p1, "selectionIndex"    # I
    .param p2, "selectionLength"    # I

    .prologue
    const/4 v5, 0x0

    .line 233
    move v2, p1

    .line 234
    .local v2, "selectStart":I
    add-int v1, p1, p2

    .line 236
    .local v1, "selectEnd":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 237
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 239
    .local v0, "maxTextLength":I
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 240
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 242
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 243
    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 245
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getSelectionStart()I

    move-result v4

    if-ne v4, v2, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getSelectionEnd()I

    move-result v4

    if-eq v4, v1, :cond_1

    .line 246
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelection(II)V

    .line 248
    :cond_1
    return-void
.end method


# virtual methods
.method public close(Z)V
    .locals 3
    .param p1, "programatic"    # Z

    .prologue
    .line 68
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 69
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->closeButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 73
    if-nez p1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->cancelTextSession()V

    .line 76
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->close(Z)V

    .line 81
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onStart()V

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/remote/TextInputControl;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->closeButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/remote/TextInputControl;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->confirmButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/remote/TextInputControl;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/remote/TextInputControl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setTextOrSelectionChangedRunnable(Ljava/lang/Runnable;)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/remote/TextInputControl;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 111
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 112
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 113
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->viewModel:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onStop()V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->closeButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->confirmButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setTextOrSelectionChangedRunnable(Ljava/lang/Runnable;)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 126
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 132
    return-void
.end method

.method public updateConfiguration(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 3
    .param p1, "configuration"    # Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 147
    .local v0, "inputFilter":I
    sget-object v1, Lcom/microsoft/xbox/xle/remote/TextInputControl$1;->$SwitchMap$com$microsoft$xbox$smartglass$TextInputScope:[I

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->inputScope:Lcom/microsoft/xbox/smartglass/TextInputScope;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/TextInputScope;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    or-int/lit8 v0, v0, 0x1

    .line 163
    or-int/lit8 v0, v0, 0x0

    .line 167
    :goto_0
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->Password:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    or-int/lit8 v0, v0, 0x1

    .line 169
    or-int/lit16 v0, v0, 0x80

    .line 172
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->AcceptsReturn:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->MultiLine:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 173
    :cond_1
    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    .line 178
    :goto_1
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->SpellCheckEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    const v1, 0x8000

    or-int/2addr v0, v1

    .line 182
    :cond_2
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextOptions;->PredictionEnabled:Lcom/microsoft/xbox/smartglass/TextOptions;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 183
    or-int/lit8 v0, v0, 0x1

    .line 184
    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 187
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setInputType(I)V

    .line 189
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->prompt:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 190
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->prompt:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 196
    :goto_2
    iget v1, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->maxTextLength:I

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->setMaxLength(I)V

    .line 197
    return-void

    .line 149
    :pswitch_0
    or-int/lit8 v0, v0, 0x10

    .line 150
    goto :goto_0

    .line 152
    :pswitch_1
    or-int/lit8 v0, v0, 0x20

    .line 153
    goto :goto_0

    .line 156
    :pswitch_2
    or-int/lit8 v0, v0, 0x2

    .line 157
    goto :goto_0

    .line 159
    :pswitch_3
    or-int/lit8 v0, v0, 0x3

    .line 160
    goto :goto_0

    .line 175
    :cond_4
    const v1, -0x20001

    and-int/2addr v0, v1

    goto :goto_1

    .line 192
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->prompt:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->prompt:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/TextConfiguration;->prompt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updateText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 136
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 137
    .local v0, "length":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControl;->editView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelection(I)V

    .line 138
    return-void

    .line 136
    .end local v0    # "length":I
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method public updateTextSelection(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->setSelection(II)V

    .line 142
    return-void
.end method
