.class Lcom/microsoft/playready/MediaProxy;
.super Ljava/lang/Object;
.source "MediaProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;,
        Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;,
        Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;,
        Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;,
        Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;,
        Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;,
        Lcom/microsoft/playready/MediaProxy$ITracingRecordThresholdReachedListener;,
        Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;
    }
.end annotation


# instance fields
.field private mCachedPlaylistLock:Ljava/lang/Object;

.field private mExecService:Ljava/util/concurrent/ExecutorService;

.field private mLAHandler:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;

.field private final mManifestUpdateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field mManifestUpdateRunnable:Ljava/lang/Runnable;

.field private final mNativeClass:Lcom/microsoft/playready/Native_Class9;

.field private mOnManifestUpdateListenerLockObj:Ljava/lang/Object;

.field private mOnRepresentationSwitchListenerLockObj:Ljava/lang/Object;

.field private final mRepresentationSwitchListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateListeners:Ljava/util/ArrayList;

    .line 142
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mOnManifestUpdateListenerLockObj:Ljava/lang/Object;

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mRepresentationSwitchListeners:Ljava/util/ArrayList;

    .line 146
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mOnRepresentationSwitchListenerLockObj:Ljava/lang/Object;

    .line 150
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mCachedPlaylistLock:Ljava/lang/Object;

    .line 151
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateRunnable:Ljava/lang/Runnable;

    .line 162
    sget-object v1, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    new-instance v0, Lcom/microsoft/playready/Native_Class9;

    invoke-direct {v0}, Lcom/microsoft/playready/Native_Class9;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    .line 162
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    new-instance v0, Lcom/microsoft/playready/MediaProxy$1;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/MediaProxy$1;-><init>(Lcom/microsoft/playready/MediaProxy;)V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateRunnable:Ljava/lang/Runnable;

    .line 175
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    new-instance v1, Lcom/microsoft/playready/MediaProxy$2;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/MediaProxy$2;-><init>(Lcom/microsoft/playready/MediaProxy;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class9;->setHandler1(Lcom/microsoft/playready/Native_Class9$IHandler_1;)V

    .line 185
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    new-instance v1, Lcom/microsoft/playready/MediaProxy$3;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/MediaProxy$3;-><init>(Lcom/microsoft/playready/MediaProxy;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class9;->setHandler2(Lcom/microsoft/playready/Native_Class9$IHandler_2;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    new-instance v1, Lcom/microsoft/playready/MediaProxy$4;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/MediaProxy$4;-><init>(Lcom/microsoft/playready/MediaProxy;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class9;->setHandler3(Lcom/microsoft/playready/Native_Class9$IHandler_3;)V

    .line 215
    return-void

    .line 162
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private HLSMasterPlaylistInformationFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_4;)Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;
    .locals 4
    .param p1, "obfuscatedObj"    # Lcom/microsoft/playready/Native_Class9$internal_class_4;

    .prologue
    .line 135
    new-instance v0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;

    iget-object v1, p1, Lcom/microsoft/playready/Native_Class9$internal_class_4;->mField1:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/playready/Native_Class9$internal_class_4;->mField2:[I

    iget-object v3, p1, Lcom/microsoft/playready/Native_Class9$internal_class_4;->mField3:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;-><init>(Ljava/lang/String;[I[Ljava/lang/String;)V

    return-object v0
.end method

.method public static HLSPlaylistInformationFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_2;)Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;
    .locals 6
    .param p0, "obfuscatedObj"    # Lcom/microsoft/playready/Native_Class9$internal_class_2;

    .prologue
    .line 106
    new-instance v0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;

    iget v1, p0, Lcom/microsoft/playready/Native_Class9$internal_class_2;->mField1:I

    iget-object v2, p0, Lcom/microsoft/playready/Native_Class9$internal_class_2;->mField2:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/playready/Native_Class9$internal_class_2;->mField3:Ljava/lang/String;

    iget-object v4, p0, Lcom/microsoft/playready/Native_Class9$internal_class_2;->mField4:[Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/playready/Native_Class9$internal_class_2;->mField5:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;-><init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic access$0(Lcom/microsoft/playready/MediaProxy;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mExecService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$1(Lcom/microsoft/playready/MediaProxy;)Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mLAHandler:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;

    return-object v0
.end method


# virtual methods
.method public addManifestUpdateListener(Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;

    .prologue
    .line 244
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mOnManifestUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    monitor-exit v1

    .line 247
    return-void

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addRepresentationSwitchListener(Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;

    .prologue
    .line 266
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mOnRepresentationSwitchListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mRepresentationSwitchListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    monitor-exit v1

    .line 269
    return-void

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 225
    return-void

    .line 222
    :catchall_0
    move-exception v0

    .line 223
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 224
    throw v0
.end method

.method protected fragmentInfoFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_1;)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    .locals 8
    .param p1, "obfuscatedObj"    # Lcom/microsoft/playready/Native_Class9$internal_class_1;

    .prologue
    .line 290
    new-instance v1, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    iget-wide v2, p1, Lcom/microsoft/playready/Native_Class9$internal_class_1;->mField1:J

    iget-wide v4, p1, Lcom/microsoft/playready/Native_Class9$internal_class_1;->mField2:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p1, Lcom/microsoft/playready/Native_Class9$internal_class_1;->mField3:Ljava/lang/String;

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;-><init>(JJLjava/util/concurrent/TimeUnit;Ljava/lang/String;)V

    return-object v1
.end method

.method public generateSegment(II)[B
    .locals 1
    .param p1, "streamID"    # I
    .param p2, "segmentID"    # I

    .prologue
    .line 321
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/playready/Native_Class9;->method_9(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getAspectRatio()F
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class9;->method_14()F

    move-result v0

    return v0
.end method

.method public getCbcKey(II)[B
    .locals 1
    .param p1, "streamID"    # I
    .param p2, "segmentID"    # I

    .prologue
    .line 325
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/playready/Native_Class9;->method_10(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getFragmentData(III)[B
    .locals 1
    .param p1, "trackIndex"    # I
    .param p2, "qualityIndex"    # I
    .param p3, "fragmentIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/playready/Native_Class9;->method_7(III)[B

    move-result-object v0

    return-object v0
.end method

.method public getFragmentIndexAtTime(IJ)I
    .locals 4
    .param p1, "trackIndex"    # I
    .param p2, "seekTimeMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p2

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/playready/Native_Class9;->method_5(IJ)I

    move-result v0

    return v0
.end method

.method public getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    .locals 1
    .param p1, "trackIndex"    # I
    .param p2, "fragmentIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation

    .prologue
    .line 305
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/playready/Native_Class9;->method_6(II)Lcom/microsoft/playready/Native_Class9$internal_class_1;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/playready/MediaProxy;->fragmentInfoFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_1;)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v0

    return-object v0
.end method

.method public getHLSMasterPlaylist()Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class9;->method_12()Lcom/microsoft/playready/Native_Class9$internal_class_4;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/playready/MediaProxy;->HLSMasterPlaylistInformationFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_4;)Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;

    move-result-object v0

    return-object v0
.end method

.method public getHLSPlaylist(ILjava/lang/String;)Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;
    .locals 1
    .param p1, "streamID"    # I
    .param p2, "playlistRoot"    # Ljava/lang/String;

    .prologue
    .line 313
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/playready/Native_Class9;->method_8(ILjava/lang/String;)Lcom/microsoft/playready/Native_Class9$internal_class_2;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/playready/MediaProxy;->HLSPlaylistInformationFromObfuscatedObj(Lcom/microsoft/playready/Native_Class9$internal_class_2;)Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;

    move-result-object v0

    return-object v0
.end method

.method public getMediaDescription()Lcom/microsoft/playready/MediaDescription;
    .locals 2

    .prologue
    .line 329
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v1}, Lcom/microsoft/playready/Native_Class9;->method_11()Lcom/microsoft/playready/Native_Class10;

    move-result-object v0

    .line 330
    .local v0, "nativeClass":Lcom/microsoft/playready/Native_Class10;
    new-instance v1, Lcom/microsoft/playready/MediaDescription;

    invoke-direct {v1, v0}, Lcom/microsoft/playready/MediaDescription;-><init>(Lcom/microsoft/playready/Native_Class10;)V

    return-object v1
.end method

.method notifyManifestUpdated()V
    .locals 4

    .prologue
    .line 256
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mCachedPlaylistLock:Ljava/lang/Object;

    monitor-enter v1

    monitor-exit v1

    .line 258
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxy;->mOnManifestUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 259
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 258
    monitor-exit v2

    .line 263
    return-void

    .line 259
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;

    .line 260
    .local v0, "listener":Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;
    invoke-interface {v0}, Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;->onManifestUpdate()V

    goto :goto_0

    .line 258
    .end local v0    # "listener":Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method notifyRepresentationSwitch(IIJ)V
    .locals 5
    .param p1, "streamIndex"    # I
    .param p2, "repIndex"    # I
    .param p3, "startTime"    # J

    .prologue
    .line 278
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxy;->mOnRepresentationSwitchListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 279
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mRepresentationSwitchListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 278
    monitor-exit v2

    .line 283
    return-void

    .line 279
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;

    .line 280
    .local v0, "listener":Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;->onRepresentationSwitch(IIJ)V

    goto :goto_0

    .line 278
    .end local v0    # "listener":Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class9;->method_15()V

    .line 229
    return-void
.end method

.method public removeManifestUpdateListener(Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/MediaProxy$IManifestUpdateListener;

    .prologue
    .line 250
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mOnManifestUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mManifestUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 250
    monitor-exit v1

    .line 253
    return-void

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeRepresentationSwitchListener(Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/playready/MediaProxy$IRepresentationSwitchListener;

    .prologue
    .line 272
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy;->mOnRepresentationSwitchListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mRepresentationSwitchListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 272
    monitor-exit v1

    .line 275
    return-void

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class9;->method_16()V

    .line 233
    return-void
.end method

.method public setContent(Ljava/lang/String;J)V
    .locals 2
    .param p1, "pathToContent"    # Ljava/lang/String;
    .param p2, "segmentLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/DrmException;
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/playready/Native_Class9;->method_4(Ljava/lang/String;J)V

    .line 241
    return-void
.end method

.method public setReactiveLicenseAcquisitionHandler(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;)V
    .locals 0
    .param p1, "laHandler"    # Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxy;->mLAHandler:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;

    .line 287
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class9;

    invoke-virtual {v0}, Lcom/microsoft/playready/Native_Class9;->method_13()V

    .line 237
    return-void
.end method
