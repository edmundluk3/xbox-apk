.class public Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;
.super Lio/reactivex/observers/DisposableObserver;
.source "DisposableRelayObserver.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/observers/DisposableObserver",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final relay:Lcom/jakewharton/rxrelay2/Relay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/Relay",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jakewharton/rxrelay2/Relay;)V
    .locals 0
    .param p1    # Lcom/jakewharton/rxrelay2/Relay;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/Relay",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver<TT;>;"
    .local p1, "relay":Lcom/jakewharton/rxrelay2/Relay;, "Lcom/jakewharton/rxrelay2/Relay<TT;>;"
    invoke-direct {p0}, Lio/reactivex/observers/DisposableObserver;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;->relay:Lcom/jakewharton/rxrelay2/Relay;

    .line 21
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 0

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver<TT;>;"
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver<TT;>;"
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;, "Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/DisposableRelayObserver;->relay:Lcom/jakewharton/rxrelay2/Relay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    .line 26
    return-void
.end method
