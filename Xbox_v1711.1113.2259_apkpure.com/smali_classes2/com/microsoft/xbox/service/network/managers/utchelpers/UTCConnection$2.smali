.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;
.super Ljava/lang/Object;
.source "UTCConnection.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackConnectSuccess(JZZZILjava/lang/String;Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$consoleLocale:Ljava/lang/String;

.field final synthetic val$consoleOSVersion:Ljava/lang/String;

.field final synthetic val$isIpAddressEnteredManually:Z

.field final synthetic val$isManual:Z

.field final synthetic val$isRetry:Z

.field final synthetic val$pairLatencyMS:J

.field final synthetic val$sessionId:Ljava/util/UUID;

.field final synthetic val$startTimeUTCString:Ljava/lang/String;

.field final synthetic val$statusCode:I

.field final synthetic val$tvProviderId:I


# direct methods
.method constructor <init>(JZZZILjava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 96
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$pairLatencyMS:J

    iput-boolean p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isIpAddressEnteredManually:Z

    iput-boolean p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isRetry:Z

    iput-boolean p5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isManual:Z

    iput p6, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$statusCode:I

    iput-object p7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$startTimeUTCString:Ljava/lang/String;

    iput-object p8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleLocale:Ljava/lang/String;

    iput-object p9, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$sessionId:Ljava/util/UUID;

    iput-object p10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleOSVersion:Ljava/lang/String;

    iput p11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$tvProviderId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 8

    .prologue
    .line 99
    new-instance v0, Lxbox/smartglass/ConsoleConnect;

    invoke-direct {v0}, Lxbox/smartglass/ConsoleConnect;-><init>()V

    .line 100
    .local v0, "connect":Lxbox/smartglass/ConsoleConnect;
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$pairLatencyMS:J

    const-wide/32 v4, 0x7fffffff

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const v2, 0x7fffffff

    :goto_0
    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setLatency(I)V

    .line 103
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isIpAddressEnteredManually:Z

    if-eqz v2, :cond_1

    .line 104
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->IPADDRESS:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .line 112
    .local v1, "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    :goto_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->getValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setMode(I)V

    .line 114
    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$statusCode:I

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setHResult(I)V

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$startTimeUTCString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setStart(Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleLocale:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setConsoleLanguage(Ljava/lang/String;)V

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$sessionId:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setSessionId(Ljava/lang/String;)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleOSVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setConsoleOsVersion(Ljava/lang/String;)V

    .line 121
    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$tvProviderId:I

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setTvProviderID(I)V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->access$000()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lxbox/smartglass/ConsoleConnect;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 125
    const-string v2, "Connect - sessionId: %s latency: %d mode: %d hResult: %d start: %s consoleLanguage: %s consoleOsVersion: %s tvProviderId: %d"

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$sessionId:Ljava/util/UUID;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$pairLatencyMS:J

    .line 127
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 128
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$statusCode:I

    .line 129
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$startTimeUTCString:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleLocale:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$consoleOSVersion:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    iget v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$tvProviderId:I

    .line 133
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 125
    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 135
    return-void

    .line 100
    .end local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$pairLatencyMS:J

    long-to-int v2, v2

    goto/16 :goto_0

    .line 105
    :cond_1
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isRetry:Z

    if-eqz v2, :cond_2

    .line 106
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->RETRY:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .restart local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    goto/16 :goto_1

    .line 107
    .end local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    :cond_2
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$2;->val$isManual:Z

    if-eqz v2, :cond_3

    .line 108
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .restart local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    goto/16 :goto_1

    .line 110
    .end local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;->AUTOMATIC:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;

    .restart local v1    # "mode":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$ConnectionMode;
    goto/16 :goto_1
.end method
