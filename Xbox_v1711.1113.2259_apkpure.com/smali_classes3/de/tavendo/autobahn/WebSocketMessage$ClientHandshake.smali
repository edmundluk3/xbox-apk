.class public Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;
.super Lde/tavendo/autobahn/WebSocketMessage$Message;
.source "WebSocketMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientHandshake"
.end annotation


# instance fields
.field private final mOrigin:Ljava/net/URI;

.field private final mSubprotocols:[Ljava/lang/String;

.field private final mURI:Ljava/net/URI;


# direct methods
.method constructor <init>(Ljava/net/URI;)V
    .locals 1
    .param p1, "uri"    # Ljava/net/URI;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 63
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mURI:Ljava/net/URI;

    .line 64
    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mOrigin:Ljava/net/URI;

    .line 65
    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mSubprotocols:[Ljava/lang/String;

    .line 66
    return-void
.end method

.method constructor <init>(Ljava/net/URI;Ljava/net/URI;[Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "origin"    # Ljava/net/URI;
    .param p3, "subprotocols"    # [Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 69
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mURI:Ljava/net/URI;

    .line 70
    iput-object p2, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mOrigin:Ljava/net/URI;

    .line 71
    iput-object p3, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mSubprotocols:[Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method public getOrigin()Ljava/net/URI;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mOrigin:Ljava/net/URI;

    return-object v0
.end method

.method public getSubprotocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mSubprotocols:[Ljava/lang/String;

    return-object v0
.end method

.method public getURI()Ljava/net/URI;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;->mURI:Ljava/net/URI;

    return-object v0
.end method
