.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
.super Ljava/lang/Object;
.source "ChatHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    }
.end annotation


# static fields
.field private static final CHANNELID_OFFSET:I = 0x1c

.field public static final CHATHEADER_SIZE:I = 0x4c

.field private static final CLIENTSEQNUM_OFFSET:I = 0x4

.field private static final DEVICE_SEQUENCE_NUMBER:I = 0x0

.field private static final FLAGS_OFFSET:I = 0x48

.field private static final MESSAGEID_OFFSET:I = 0x14

.field private static final MESSAGELENGTH_OFFSET:I = 0xc

.field private static final MESSAGETIME_OFFSET:I = 0x8

.field private static final MESSAGETYPE_OFFSET:I = 0x10

.field private static final PROTOCOLVERSION_OFFSET:I = 0x0

.field private static final PROTOCOL_VERSION:I = 0x1

.field private static final SENDERGAMERTAG_OFFSET:I = 0x38

.field private static final SENDERGAMERTAG_SIZE:I = 0xf

.field private static final SENDERXUID_OFFSET:I = 0x30

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private clientSeqNum:I

.field private flags:I

.field private messageId:J

.field private messageLength:I

.field private messageTime:I

.field private messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

.field private protocolVersion:I

.field private senderGamerTag:Ljava/lang/String;

.field private senderXuid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V
    .locals 0
    .param p1, "protocolVersion"    # I
    .param p2, "clientSeqNum"    # I
    .param p3, "messageTime"    # I
    .param p4, "messageLength"    # I
    .param p5, "messageType"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "messageId"    # J
    .param p8, "chatChannelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p9, "senderXuid"    # J
    .param p11, "senderGamerTag"    # Ljava/lang/String;
    .param p12, "flags"    # I

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-direct/range {p0 .. p12}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->init(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V

    .line 110
    return-void
.end method

.method public constructor <init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 13
    .param p1, "messageLength"    # I
    .param p2, "messageType"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "chatChannelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    const-string v11, ""

    const/4 v12, 0x0

    move-object v0, p0

    move v4, p1

    move-object v5, p2

    move-object/from16 v8, p3

    invoke-direct/range {v0 .. v12}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->init(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V

    .line 125
    return-void
.end method

.method public constructor <init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 13
    .param p1, "messageLength"    # I
    .param p2, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "chatChannelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/4 v12, 0x0

    .line 131
    .local v12, "flags":I
    iget-boolean v0, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->containsLinks:Z

    if-eqz v0, :cond_0

    .line 132
    or-int/lit8 v12, v12, 0x4

    .line 135
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v5, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    const-string v11, ""

    move-object v0, p0

    move v4, p1

    move-object/from16 v8, p3

    invoke-direct/range {v0 .. v12}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->init(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V

    .line 145
    return-void
.end method

.method private init(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V
    .locals 0
    .param p1, "protocolVersion"    # I
    .param p2, "clientSeqNum"    # I
    .param p3, "messageTime"    # I
    .param p4, "messageLength"    # I
    .param p5, "messageType"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "messageId"    # J
    .param p8, "chatChannelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p9, "senderXuid"    # J
    .param p11, "senderGamerTag"    # Ljava/lang/String;
    .param p12, "flags"    # I

    .prologue
    .line 157
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    invoke-static {p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 160
    iput p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->protocolVersion:I

    .line 161
    iput p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->clientSeqNum:I

    .line 162
    iput p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageTime:I

    .line 163
    iput p4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageLength:I

    .line 164
    iput-object p5, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 165
    iput-wide p6, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    .line 166
    iput-object p8, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 167
    iput-wide p9, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderXuid:J

    .line 168
    iput-object p11, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderGamerTag:Ljava/lang/String;

    .line 169
    iput p12, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->flags:I

    .line 170
    return-void
.end method

.method public static readObject(Ljava/nio/ByteBuffer;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    .locals 1
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    move-result-object v0

    return-object v0
.end method

.method public static readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    .locals 14
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 227
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 228
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 230
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x4c

    if-ge v0, v1, :cond_0

    .line 231
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not enough buffer space to read ChatHeader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    add-int/lit8 v1, p1, 0x0

    .line 235
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    add-int/lit8 v2, p1, 0x4

    .line 236
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    add-int/lit8 v3, p1, 0x8

    .line 237
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    add-int/lit8 v4, p1, 0xc

    .line 238
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    add-int/lit8 v5, p1, 0x10

    .line 239
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->fromInt(I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-result-object v5

    add-int/lit8 v6, p1, 0x14

    .line 240
    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v6

    const/16 v8, 0x1c

    .line 241
    invoke-static {p0, v8}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v8

    add-int/lit8 v9, p1, 0x30

    .line 242
    invoke-virtual {p0, v9}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v9

    add-int/lit8 v11, p1, 0x38

    const/16 v12, 0xf

    sget-object v13, Lcom/microsoft/xbox/toolkit/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    .line 243
    invoke-static {p0, v11, v12, v13}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->getStringFromByteBuffer(Ljava/nio/ByteBuffer;IILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v11

    add-int/lit8 v12, p1, 0x48

    .line 244
    invoke-virtual {p0, v12}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v12

    invoke-direct/range {v0 .. v12}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(IIIILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JLjava/lang/String;I)V

    .line 234
    return-object v0
.end method


# virtual methods
.method public containsLinks()Z
    .locals 2

    .prologue
    .line 217
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->flags:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatFlags;->containsFlag(II)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 282
    if-ne p1, p0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return v1

    .line 284
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    if-nez v3, :cond_2

    move v1, v2

    .line 285
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 287
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    .line 288
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 289
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getChatChannelId()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method public getClientSeqNum()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->clientSeqNum:I

    return v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->flags:I

    return v0
.end method

.method public getMessageId()J
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    return-wide v0
.end method

.method public getMessageLength()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageLength:I

    return v0
.end method

.method public getMessageTime()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageTime:I

    return v0
.end method

.method public getMessageType()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    return-object v0
.end method

.method public getProtocolVersion()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->protocolVersion:I

    return v0
.end method

.method public getSenderGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderGamerTag:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderXuid()J
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderXuid:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 295
    const/16 v0, 0x11

    .line 296
    .local v0, "result":I
    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 297
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 299
    return v0
.end method

.method public isDeleted()Z
    .locals 2

    .prologue
    .line 213
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->flags:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatFlags;->containsFlag(II)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeObject(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;I)V

    .line 249
    return-void
.end method

.method public writeObject(Ljava/nio/ByteBuffer;I)V
    .locals 6
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 252
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 253
    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 255
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    sub-int/2addr v1, p2

    const/16 v2, 0x4c

    if-ge v1, v2, :cond_0

    .line 256
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "not enough buffer space to write ChatHeader"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 259
    :cond_0
    add-int/lit8 v1, p2, 0x0

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->protocolVersion:I

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 260
    add-int/lit8 v1, p2, 0x4

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->clientSeqNum:I

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 261
    add-int/lit8 v1, p2, 0x8

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageTime:I

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 262
    add-int/lit8 v1, p2, 0xc

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageLength:I

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 263
    add-int/lit8 v1, p2, 0x10

    iget-object v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-static {v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->access$000(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 264
    add-int/lit8 v1, p2, 0x14

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->messageId:J

    invoke-virtual {p1, v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 265
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    add-int/lit8 v2, p2, 0x1c

    invoke-virtual {v1, p1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->writeObject(Ljava/nio/ByteBuffer;I)V

    .line 266
    add-int/lit8 v1, p2, 0x30

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderXuid:J

    invoke-virtual {p1, v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderGamerTag:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->senderGamerTag:Ljava/lang/String;

    sget-object v2, Lcom/microsoft/xbox/toolkit/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 270
    .local v0, "bytes":[B
    array-length v1, v0

    const/16 v2, 0xf

    if-gt v1, v2, :cond_2

    .line 271
    const/16 v1, 0x38

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->putBytesToByteBuffer([BLjava/nio/ByteBuffer;I)V

    .line 277
    .end local v0    # "bytes":[B
    :cond_1
    :goto_0
    add-int/lit8 v1, p2, 0x48

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->flags:I

    invoke-virtual {p1, v1, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 278
    return-void

    .line 273
    .restart local v0    # "bytes":[B
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "writeObject(): gamer tag is longer than available space"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
