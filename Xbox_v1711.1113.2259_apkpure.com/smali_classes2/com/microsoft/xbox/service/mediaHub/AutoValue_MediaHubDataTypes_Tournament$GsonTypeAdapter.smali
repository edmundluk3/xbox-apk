.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_Tournament.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultOffset:Ljava/lang/Long;

.field private defaultOrganizer:Ljava/lang/String;

.field private defaultSegmentId:Ljava/lang/Long;

.field private defaultTournamentId:Ljava/lang/String;

.field private final offsetAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final organizerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final segmentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOffset:Ljava/lang/Long;

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOrganizer:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultTournamentId:Ljava/lang/String;

    .line 29
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->organizerAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->tournamentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 72
    const/4 v5, 0x0

    .line 108
    :goto_0
    return-object v5

    .line 74
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOffset:Ljava/lang/Long;

    .line 76
    .local v1, "offset":Ljava/lang/Long;
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOrganizer:Ljava/lang/String;

    .line 77
    .local v2, "organizer":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 78
    .local v3, "segmentId":Ljava/lang/Long;
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultTournamentId:Ljava/lang/String;

    .line 79
    .local v4, "tournamentId":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 80
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 82
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 85
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 103
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 85
    :sswitch_0
    const-string v6, "offset"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "organizer"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "segmentId"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "tournamentId"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 87
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "offset":Ljava/lang/Long;
    check-cast v1, Ljava/lang/Long;

    .line 88
    .restart local v1    # "offset":Ljava/lang/Long;
    goto :goto_1

    .line 91
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->organizerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "organizer":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 92
    .restart local v2    # "organizer":Ljava/lang/String;
    goto :goto_1

    .line 95
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "segmentId":Ljava/lang/Long;
    check-cast v3, Ljava/lang/Long;

    .line 96
    .restart local v3    # "segmentId":Ljava/lang/Long;
    goto :goto_1

    .line 99
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->tournamentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "tournamentId":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 100
    .restart local v4    # "tournamentId":Ljava/lang/String;
    goto :goto_1

    .line 107
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 108
    new-instance v5, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fa64cd1 -> :sswitch_1
        -0x61065852 -> :sswitch_2
        -0x3cc89b6d -> :sswitch_0
        0x25738ce4 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultOffset(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOffset"    # Ljava/lang/Long;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOffset:Ljava/lang/Long;

    .line 36
    return-object p0
.end method

.method public setDefaultOrganizer(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOrganizer"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultOrganizer:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setDefaultSegmentId(Ljava/lang/Long;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSegmentId"    # Ljava/lang/Long;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultSegmentId:Ljava/lang/Long;

    .line 44
    return-object p0
.end method

.method public setDefaultTournamentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTournamentId"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->defaultTournamentId:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    if-nez p2, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 67
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 58
    const-string v0, "offset"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->offsetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;->offset()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 60
    const-string v0, "organizer"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->organizerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;->organizer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 62
    const-string v0, "segmentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->segmentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;->segmentId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 64
    const-string v0, "tournamentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->tournamentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;->tournamentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Tournament$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;)V

    return-void
.end method
