.class Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;
.super Ljava/lang/Object;
.source "UserInfoRequest.java"


# instance fields
.field private final SERVICES_PATH:Ljava/lang/String;

.field private final SERVICES_PROFILE_SUBDOMAIN:Ljava/lang/String;

.field private final _container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

.field private final _metric:Ljava/lang/String;

.field private final _requestId:I

.field private final _tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;


# direct methods
.method public constructor <init>(ILcom/microsoft/xbox/smartglass/controls/WebComponentContainer;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "container"    # Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;
    .param p3, "tracker"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "profile"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->SERVICES_PROFILE_SUBDOMAIN:Ljava/lang/String;

    .line 13
    const-string v0, "/users/me/id"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->SERVICES_PATH:Ljava/lang/String;

    .line 14
    const-string v0, "Information.GetUserInfo"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_metric:Ljava/lang/String;

    .line 21
    iput p1, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_requestId:I

    .line 22
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    .line 23
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    .prologue
    .line 11
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_requestId:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    return-object v0
.end method


# virtual methods
.method public send()V
    .locals 9

    .prologue
    .line 27
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v5

    const-string v6, "Information.GetUserInfo"

    iget v7, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_requestId:I

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(Ljava/lang/String;I)V

    .line 30
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v6

    const-string v7, "profile"

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getXliveServiceUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/users/me/id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "servicesUrl":Ljava/lang/String;
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    const-string v5, "GET"

    const/4 v6, 0x1

    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->xliveAudienceUri:Ljava/lang/String;

    invoke-direct {v1, v5, v4, v6, v7}, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 36
    .local v1, "jsonRequest":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    new-instance v5, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;)V

    invoke-direct {v3, v1, v5}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;-><init>(Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;)V

    .line 65
    .local v3, "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->add(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 66
    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->runAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v1    # "jsonRequest":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    .end local v3    # "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    .end local v4    # "servicesUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "exception":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to get user info. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "message":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget v6, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_requestId:I

    invoke-interface {v5, v6, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v5

    const-string v6, "Information.GetUserInfo"

    iget v7, p0, Lcom/microsoft/xbox/smartglass/controls/UserInfoRequest;->_requestId:I

    sget-object v8, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8, v2}, Lcom/microsoft/xbox/smartglass/MetricsManager;->stop(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
