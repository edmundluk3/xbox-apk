.class public abstract Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "HomeScreenAdapter.java"


# instance fields
.field protected final barCount:I

.field protected final cellMargin:I

.field protected final cellUnderflow:I

.field protected final container:Landroid/view/View;

.field protected final content:Landroid/view/View;

.field private height:I

.field private final layoutListener:Landroid/view/View$OnLayoutChangeListener;

.field protected final section:Landroid/view/View;

.field private width:I


# direct methods
.method public constructor <init>(II)V
    .locals 3
    .param p1, "containerRid"    # I
    .param p2, "sectionIdx"    # I

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 19
    iput v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->width:I

    iput v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->height:I

    .line 40
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 41
    .local v0, "container":Landroid/view/ViewGroup;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->container:Landroid/view/View;

    .line 42
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    .line 43
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->content:Landroid/view/View;

    .line 44
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 45
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x7f0b001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->barCount:I

    .line 46
    const v2, 0x7f0b001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellUnderflow:I

    .line 47
    const v2, 0x7f09034b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    .line 48
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "section"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 19
    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->width:I

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->height:I

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->container:Landroid/view/View;

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    .line 24
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->content:Landroid/view/View;

    .line 25
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 26
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0b001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->barCount:I

    .line 27
    const v1, 0x7f0b001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellUnderflow:I

    .line 28
    const v1, 0x7f09034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    .line 29
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    .line 37
    return-void
.end method

.method protected static applyScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V
    .locals 2
    .param p0, "lmhlv"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .prologue
    .line 146
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->getOffset()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionFromOffset(II)V

    .line 149
    :cond_0
    return-void
.end method

.method protected static createScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 4
    .param p0, "lmhlv"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    const/4 v0, 0x0

    .line 135
    const/4 v2, 0x0

    .line 136
    .local v2, "ret":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    if-eqz p0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFirstVisiblePosition()I

    move-result v1

    .line 138
    .local v1, "position":I
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 139
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_1

    .line 140
    .local v0, "offset":I
    :goto_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .end local v2    # "ret":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    invoke-direct {v2, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    .line 142
    .end local v0    # "offset":I
    .end local v1    # "position":I
    .end local v3    # "v":Landroid/view/View;
    .restart local v2    # "ret":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    :cond_0
    return-object v2

    .line 139
    .restart local v1    # "position":I
    .restart local v3    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected computeCellSize(I)I
    .locals 4
    .param p1, "rootSize"    # I

    .prologue
    .line 89
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->barCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    mul-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method protected computeCellWidthForTwoWayView(I)I
    .locals 8
    .param p1, "rootSize"    # I

    .prologue
    .line 116
    int-to-double v0, p1

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->barCount:I

    int-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    int-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->barCount:I

    int-to-double v2, v2

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellUnderflow:I

    int-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method protected computeMultiCellSize(II)I
    .locals 3
    .param p1, "cellSize"    # I
    .param p2, "numOfCells"    # I

    .prologue
    .line 107
    mul-int v0, p2, p1

    add-int/lit8 v1, p2, -0x1

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method protected getRootSize()I
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->width:I

    .line 76
    .local v0, "rootSize":I
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method protected onLayoutChanged()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStart()V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 62
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->section:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->layoutListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStop()V

    .line 68
    return-void
.end method

.method protected resizeCellForTwoWayView(Landroid/view/View;)V
    .locals 3
    .param p1, "cell"    # Landroid/view/View;

    .prologue
    .line 120
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 121
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->getRootSize()I

    move-result v1

    .line 122
    .local v1, "rootSize":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->computeCellWidthForTwoWayView(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 123
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    return-void
.end method

.method protected resizeTwoWayView(Landroid/view/View;I)V
    .locals 4
    .param p1, "twv"    # Landroid/view/View;
    .param p2, "widthsInCells"    # I

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 128
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->getRootSize()I

    move-result v1

    .line 129
    .local v1, "rootSize":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->computeCellSize(I)I

    move-result v2

    invoke-virtual {p0, v2, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->computeMultiCellSize(II)I

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->cellMargin:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 130
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->computeCellWidthForTwoWayView(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 131
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->width:I

    .line 81
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->height:I

    .line 82
    return-void
.end method
