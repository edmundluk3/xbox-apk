.class final Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;
.super Ljava/lang/Object;
.source "TrendingXblScreenAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CarouselLoopRunnable"
.end annotation


# instance fields
.field private final pagerAdapterRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/PagerAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final viewPagerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/ViewPager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/PagerAdapter;)V
    .locals 1
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;
    .param p2, "pagerAdapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;->viewPagerRef:Ljava/lang/ref/WeakReference;

    .line 175
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;->pagerAdapterRef:Ljava/lang/ref/WeakReference;

    .line 176
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/PagerAdapter;Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/view/ViewPager;
    .param p2, "x1"    # Landroid/support/v4/view/PagerAdapter;
    .param p3, "x2"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$1;

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;-><init>(Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/PagerAdapter;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 180
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;->viewPagerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    .line 181
    .local v3, "viewPager":Landroid/support/v4/view/ViewPager;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;->pagerAdapterRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/PagerAdapter;

    .line 183
    .local v1, "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    .line 184
    invoke-virtual {v1}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    .line 186
    .local v2, "totalItems":I
    if-lez v2, :cond_0

    .line 187
    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    rem-int v0, v4, v2

    .line 188
    .local v0, "nextItem":I
    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 191
    .end local v0    # "nextItem":I
    :cond_0
    const-wide/16 v4, 0x1388

    invoke-virtual {v3, p0, v4, v5}, Landroid/support/v4/view/ViewPager;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 193
    .end local v2    # "totalItems":I
    :cond_1
    return-void
.end method
