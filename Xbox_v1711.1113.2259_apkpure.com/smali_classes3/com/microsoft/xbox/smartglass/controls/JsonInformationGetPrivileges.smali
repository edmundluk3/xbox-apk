.class Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;
.super Ljava/lang/Object;
.source "JsonInformationGetPrivileges.java"


# instance fields
.field public final audienceUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 16
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "audienceUri"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonInformationGetPrivileges;->audienceUri:Ljava/lang/String;

    .line 17
    return-void
.end method
