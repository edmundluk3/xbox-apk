.class public Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "TVSeriesOverviewScreenAdapter.java"


# instance fields
.field private activitiesData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private activitiesLayout:Landroid/widget/LinearLayout;

.field private descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

.field private durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field protected latestEpisodeButton:Landroid/widget/RelativeLayout;

.field private networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private playNowButton:Landroid/widget/RelativeLayout;

.field private ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 45
    const v0, 0x7f0e0b71

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    .line 47
    const v0, 0x7f0e0b72

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 49
    const v0, 0x7f0e0b7e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->content:Landroid/view/View;

    .line 52
    const v0, 0x7f0e0b7f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e0b80

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    const v0, 0x7f0e0b81

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    const v0, 0x7f0e0b82

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 56
    const v0, 0x7f0e0b83

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    const v0, 0x7f0e0b84

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    const v0, 0x7f0e0b85

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 59
    const v0, 0x7f0e0b7a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    .line 61
    const v0, 0x7f0e0b73

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->latestEpisodeButton:Landroid/widget/RelativeLayout;

    .line 62
    const v0, 0x7f0e0b76

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->playNowButton:Landroid/widget/RelativeLayout;

    .line 63
    return-void
.end method

.method private addActivityItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 146
    if-nez p1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 151
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 152
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030168

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 154
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 155
    const v4, 0x7f0e0791

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 156
    .local v0, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v4, 0x7f0e0792

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 157
    .local v1, "label":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 159
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getDisplayTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getIcon2x1Url()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->getDefaultImage()I

    move-result v6

    invoke-virtual {v0, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 163
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getDefaultImage()I
    .locals 1

    .prologue
    .line 169
    const v0, 0x7f0201fa

    return v0
.end method

.method static synthetic lambda$addActivityItem$3(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p2, "v1"    # Landroid/view/View;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 78
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->navigateToLatestEpisode()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 4

    .prologue
    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 70
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 71
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "activityItem":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 74
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    .end local v0    # "activityItem":Landroid/view/View;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->latestEpisodeButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 84
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->latestEpisodeButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->playNowButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->playNowButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    :cond_3
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 96
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->latestEpisodeButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->latestEpisodeButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 106
    return-void
.end method

.method public updateViewOverride()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->descriptionView:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setText(Ljava/lang/String;)V

    .line 116
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->staringListTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703d5

    .line 117
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getStars()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 116
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 119
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->networkTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getNetwork()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703cb

    .line 120
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getNetwork()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 119
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 122
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703cd

    .line 123
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 122
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->genreTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0703c8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->durationTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->resolutionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->languagesTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getActivityData()Ljava/util/ArrayList;

    move-result-object v2

    if-eq v1, v2, :cond_5

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getActivityData()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 137
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->addActivityItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    goto :goto_4

    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_0
    move v1, v3

    .line 116
    goto/16 :goto_0

    :cond_1
    move v1, v3

    .line 119
    goto/16 :goto_1

    :cond_2
    move v1, v3

    .line 122
    goto/16 :goto_2

    :cond_3
    move v2, v3

    .line 125
    goto :goto_3

    .line 140
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;->activitiesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 143
    :cond_5
    return-void
.end method
