.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;
.super Ljava/lang/Object;
.source "ChatChannelIdJsonAdapter.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
        ">;"
    }
.end annotation


# static fields
.field private static final CHANNEL_TYPE_MEMBER_NAME:Ljava/lang/String; = "channelType"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 6
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v1

    .line 19
    .local v1, "jsonObject":Lcom/google/gson/JsonObject;
    :try_start_0
    const-string v3, "channelType"

    invoke-virtual {v1, v3}, Lcom/google/gson/JsonObject;->getAsJsonPrimitive(Ljava/lang/String;)Lcom/google/gson/JsonPrimitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/JsonPrimitive;->getAsInt()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->fromValue(I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    move-result-object v2

    .line 20
    .local v2, "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatChannelId$ChannelType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 24
    new-instance v3, Lcom/google/gson/JsonParseException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported channelType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 26
    .end local v2    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :catch_0
    move-exception v0

    .line 27
    .local v0, "ex":Ljava/lang/RuntimeException;
    :goto_0
    new-instance v3, Lcom/google/gson/JsonParseException;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 22
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    .restart local v2    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :pswitch_0
    :try_start_1
    const-class v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    invoke-interface {p3, v1, v3}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v3

    .line 26
    .end local v2    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :catch_1
    move-exception v0

    goto :goto_0

    .line 20
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v0

    return-object v0
.end method
