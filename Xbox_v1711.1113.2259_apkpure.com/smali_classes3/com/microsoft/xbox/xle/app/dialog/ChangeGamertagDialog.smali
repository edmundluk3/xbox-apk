.class public Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ChangeGamertagDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private availabilityResultText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private noFreeChangeText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private policyText1:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private policyText2:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rootView:Landroid/widget/LinearLayout;

.field private final searchImeActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

.field private suggestionListAdapter:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

.field private xboxLink:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 66
    const v1, 0x7f08021b

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    new-array v2, v8, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x6

    .line 38
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 39
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 40
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 41
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    .line 42
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 37
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->searchImeActions:Ljava/util/ArrayList;

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 69
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03004f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0247

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0248

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->noFreeChangeText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0249

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->xboxLink:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText1:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024b

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText2:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024c

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e024e

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->availabilityResultText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0251

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 81
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f03021f

    invoke-direct {v1, p0, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionListAdapter:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0250

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionListAdapter:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->rootView:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->setContentView(Landroid/view/View;)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 62
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailability()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->searchImeActions:Ljava/util/ArrayList;

    return-object v0
.end method

.method private checkAvailability()V
    .locals 4

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->getEditText()Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "gamertag":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 256
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkGamertagAvailablity(Ljava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadGamertagSuggestions(Ljava/lang/String;)V

    .line 258
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 259
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 261
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->closeGamertagPickerDialog()V

    .line 108
    return-void
.end method

.method private getEditText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->dismissSelf()V

    .line 104
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setKeyStrokeListener(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->xboxLink:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$8;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$8;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->checkFreeGamertagChange()V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->loadGamertagSuggestions(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 99
    return-void
.end method

.method public updateDialog()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getIsGamertagChangeAvailable()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText1:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText2:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->noFreeChangeText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->xboxLink:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 203
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->availabilityResultText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagAvailabilityResultText()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->shouldShowGamertagAvailabilityResult()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 205
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagDialogBusy()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagReserved()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->getEditText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-static {v4, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagDialogBusy()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagReserved()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 207
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->shouldShowGamertagAvailabilityResult()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestions()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 210
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isCheckingGamertagAvailability()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .line 211
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->isGamertagReserved()Z

    move-result v0

    if-nez v0, :cond_4

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionListAdapter:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;->clear()V

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionListAdapter:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamertagSuggestions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 231
    :goto_4
    return-void

    :cond_0
    move v0, v2

    .line 203
    goto :goto_0

    :cond_1
    move v0, v1

    .line 205
    goto :goto_1

    :cond_2
    move v3, v1

    .line 206
    goto :goto_2

    :cond_3
    move v0, v1

    .line 207
    goto :goto_3

    .line 218
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_4

    .line 222
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText1:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->policyText2:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->editText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->checkAvailabilityButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->claimButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->noFreeChangeText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->xboxLink:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->suggestionList:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_4
.end method
