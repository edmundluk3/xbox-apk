.class Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;
.super Ljava/lang/Object;
.source "TvStreamerModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeferredChannelChange"
.end annotation


# instance fields
.field mChannelId:Ljava/lang/String;

.field mSource:Ljava/lang/String;

.field mUserCanViewChannel:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "channelId"    # Ljava/lang/String;
    .param p4, "userCanViewChannel"    # Ljava/lang/String;

    .prologue
    .line 827
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mSource:Ljava/lang/String;

    .line 829
    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mChannelId:Ljava/lang/String;

    .line 830
    iput-object p4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mUserCanViewChannel:Ljava/lang/String;

    .line 831
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$302(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;Z)Z

    .line 832
    return-void
.end method


# virtual methods
.method public activate()V
    .locals 4

    .prologue
    .line 835
    const-string v0, "TvStreamerModel"

    const-string v1, "Starting deferred channel change"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->access$400(Lcom/microsoft/xbox/xle/epg/TvStreamerModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mSource:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mChannelId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->mUserCanViewChannel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamerModel$DeferredChannelChange;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onStreamFormatChanged()V

    .line 840
    :cond_0
    return-void
.end method
