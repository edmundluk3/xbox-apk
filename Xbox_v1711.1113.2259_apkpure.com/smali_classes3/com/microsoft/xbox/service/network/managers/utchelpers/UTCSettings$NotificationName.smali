.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;
.super Ljava/lang/Enum;
.source "UTCSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubDemotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubInviteRequest:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubInvited:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubJoined:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubNewMember:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubPromotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubRecommendation:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubReport:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum ClubTransfer:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum FavoriteOnline:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum LFGEnabled:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum Message:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum TournamentMatchReady:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum TournamentStatus:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

.field public static final enum TournamentTeam:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "Broadcast"

    const-string v2, "Settings - Broadcast Notifications"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "FavoriteOnline"

    const-string v2, "Settings - Favorite Online Notifications"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->FavoriteOnline:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "LFGEnabled"

    const-string v2, "Settings - LFG Notifications"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->LFGEnabled:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubNewMember"

    const-string v2, "Settings - Club Members Notifications"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubNewMember:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubPromotion"

    const-string v2, "Settings - Club Admin Promotion Notifications"

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubPromotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubDemotion"

    const/4 v2, 0x5

    const-string v3, "Settings - Club Admin Demotion Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubDemotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubInviteRequest"

    const/4 v2, 0x6

    const-string v3, "Settings - Club Invitation Request Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInviteRequest:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubInvited"

    const/4 v2, 0x7

    const-string v3, "Settings - Club Invitation Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInvited:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubRecommendation"

    const/16 v2, 0x8

    const-string v3, "Settings - Club Recommendations Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubRecommendation:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubReport"

    const/16 v2, 0x9

    const-string v3, "Settings - Club Report Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubReport:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubTransfer"

    const/16 v2, 0xa

    const-string v3, "Settings - Club Transfer Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubTransfer:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "ClubJoined"

    const/16 v2, 0xb

    const-string v3, "Settings - Club Joined Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubJoined:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "Message"

    const/16 v2, 0xc

    const-string v3, "Settings - New Message Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Message:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "Achievement"

    const/16 v2, 0xd

    const-string v3, "Settings - Achievement Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "TournamentMatchReady"

    const/16 v2, 0xe

    const-string v3, "Settings - Tournament Match Ready"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentMatchReady:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "TournamentStatus"

    const/16 v2, 0xf

    const-string v3, "Settings - Tournament Status"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentStatus:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    const-string v1, "TournamentTeam"

    const/16 v2, 0x10

    const-string v3, "Settings - Tournament Team Status"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentTeam:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    .line 8
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->FavoriteOnline:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->LFGEnabled:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubNewMember:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubPromotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubDemotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInviteRequest:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInvited:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubRecommendation:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubReport:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubTransfer:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubJoined:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Message:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentMatchReady:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentStatus:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentTeam:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "actionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->name:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->name:Ljava/lang/String;

    return-object v0
.end method
