.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleHub;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleImage;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;,
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type cannot be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
