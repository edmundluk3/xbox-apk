.class public final Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ActivityFeedFilterPrefs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultShowClubs:Z

.field private defaultShowFavorites:Z

.field private defaultShowFriends:Z

.field private defaultShowGames:Z

.field private defaultShowPopular:Z

.field private final showClubsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showFavoritesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showFriendsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showGamesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showPopularAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFriends:Z

    .line 26
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFavorites:Z

    .line 27
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowClubs:Z

    .line 28
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowGames:Z

    .line 29
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowPopular:Z

    .line 31
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFriendsAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFavoritesAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showClubsAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showGamesAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showPopularAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 8
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 80
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 83
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFriends:Z

    .line 84
    .local v1, "showFriends":Z
    iget-boolean v2, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFavorites:Z

    .line 85
    .local v2, "showFavorites":Z
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowClubs:Z

    .line 86
    .local v3, "showClubs":Z
    iget-boolean v4, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowGames:Z

    .line 87
    .local v4, "showGames":Z
    iget-boolean v5, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowPopular:Z

    .line 88
    .local v5, "showPopular":Z
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 90
    .local v6, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 94
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 116
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 94
    :sswitch_0
    const-string v7, "showFriends"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v7, "showFavorites"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "showClubs"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v7, "showGames"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v7, "showPopular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFriendsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 97
    goto :goto_1

    .line 100
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFavoritesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 101
    goto :goto_1

    .line 104
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 105
    goto :goto_1

    .line 108
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showGamesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 109
    goto/16 :goto_1

    .line 112
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showPopularAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 113
    goto/16 :goto_1

    .line 120
    .end local v6    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 121
    new-instance v0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs;-><init>(ZZZZZ)V

    goto/16 :goto_0

    .line 94
    :sswitch_data_0
    .sparse-switch
        -0x73008280 -> :sswitch_2
        -0x72cd423c -> :sswitch_3
        -0xca1e028 -> :sswitch_0
        -0x5836e4 -> :sswitch_4
        0x6f5dbbda -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultShowClubs(Z)Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShowClubs"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowClubs:Z

    .line 47
    return-object p0
.end method

.method public setDefaultShowFavorites(Z)Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShowFavorites"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFavorites:Z

    .line 43
    return-object p0
.end method

.method public setDefaultShowFriends(Z)Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShowFriends"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowFriends:Z

    .line 39
    return-object p0
.end method

.method public setDefaultShowGames(Z)Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShowGames"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowGames:Z

    .line 51
    return-object p0
.end method

.method public setDefaultShowPopular(Z)Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShowPopular"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->defaultShowPopular:Z

    .line 55
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    if-nez p2, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 75
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 64
    const-string v0, "showFriends"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFriendsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    const-string v0, "showFavorites"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showFavoritesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    const-string v0, "showClubs"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showClubsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 70
    const-string v0, "showGames"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showGamesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 72
    const-string v0, "showPopular"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->showPopularAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V

    return-void
.end method
