.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteClubTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

.field private response:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

.field private final resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1673
    .local p2, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1674
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 1675
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    .line 1676
    return-void
.end method

.method private getEarliestSuspension(Ljava/util/List;)Ljava/util/Date;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;)",
            "Ljava/util/Date;"
        }
    .end annotation

    .prologue
    .line 1730
    .local p1, "suspensions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    const/4 v1, 0x0

    .line 1732
    .local v1, "earliest":Ljava/util/Date;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    .line 1733
    .local v2, "suspension":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;->deleteAfter()Ljava/util/Date;

    move-result-object v0

    .line 1735
    .local v0, "current":Ljava/util/Date;
    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1736
    :cond_1
    move-object v1, v0

    goto :goto_0

    .line 1740
    .end local v0    # "current":Ljava/util/Date;
    .end local v2    # "suspension":Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;
    :cond_2
    return-object v1
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1680
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 1696
    const/4 v1, 0x0

    .line 1699
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->deleteClub(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->response:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1700
    const/4 v1, 0x1

    .line 1705
    :goto_0
    if-eqz v1, :cond_0

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_1
    return-object v2

    .line 1701
    :catch_0
    move-exception v0

    .line 1702
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to delete club"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1705
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1666
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1691
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1666
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 1685
    const-string v0, "There should always be an action here."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 1686
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 1687
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1714
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1726
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 1727
    return-void

    .line 1717
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->response:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->response:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1718
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateAfterSuccessfulDelete()V

    goto :goto_0

    .line 1720
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->response:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->getEarliestSuspension(Ljava/util/List;)Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$1100(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/Date;)V

    goto :goto_0

    .line 1714
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1666
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$DeleteClubTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1710
    return-void
.end method
