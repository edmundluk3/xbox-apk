.class public Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "PeopleHubPersonSummaryManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static INSTANCE:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager; = null

.field private static final MAX_SUMMARY_COUNT:I = 0x3ff

.field private static SLS_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager; = null

.field private static final TAG:Ljava/lang/String;

.field private static final TIMER_INTERVAL_MS:J = 0xbb8L


# instance fields
.field private final performLazyQueryTask:Ljava/lang/Runnable;

.field private final personSummaries:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final requestedXuids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private scheduledFuture:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->SLS_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->INSTANCE:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 51
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x3ff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->personSummaries:Landroid/util/LruCache;

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    .line 54
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 56
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->performLazyQueryTask:Ljava/lang/Runnable;

    .line 57
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->performLazyQuery()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->INSTANCE:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    return-object v0
.end method

.method private performLazyQuery()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 103
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    monitor-enter v3

    .line 104
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 105
    .local v1, "queryXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 106
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    :goto_0
    return-void

    .line 106
    .end local v1    # "queryXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 112
    .restart local v1    # "queryXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "performLazyQuery: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :try_start_2
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->queryList(Ljava/util/ArrayList;)Ljava/util/List;
    :try_end_2
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_2 .. :try_end_2} :catch_0

    .line 121
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->PeopleHubPersonSummaryLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v4, v5, v6, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;Ljava/lang/Object;)V

    invoke-direct {v2, v3, p0, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to load PeopleHubPersonSummary"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->PeopleHubPersonSummaryLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v2, v3, p0, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method

.method private queryList(Ljava/util/ArrayList;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 79
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->SLS_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    invoke-interface {v3, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getPeopleHubBatchSummaries(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v0

    .line 80
    .local v0, "peopleSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 81
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    .line 82
    .local v2, "queryResult":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 83
    .local v1, "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v1, :cond_0

    .line 84
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->personSummaries:Landroid/util/LruCache;

    iget-object v5, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    monitor-enter v4

    .line 87
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 88
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 96
    .end local v0    # "peopleSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v1    # "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v2    # "queryResult":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :cond_2
    return-object v2
.end method

.method private startPerformLazyQueryTask()V
    .locals 5

    .prologue
    .line 71
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startPerformLazyQueryTask"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->performLazyQueryTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 75
    return-void
.end method


# virtual methods
.method public getPersonSummary(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->personSummaries:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public immediateQuery(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 141
    sget-object v8, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "immediateQuery: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 143
    .local v6, "size":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 145
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v4, "queryXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_1

    .line 147
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 148
    .local v7, "xuid":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->personSummaries:Landroid/util/LruCache;

    invoke-virtual {v8, v7}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 149
    .local v2, "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-nez v2, :cond_0

    .line 150
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 153
    :cond_0
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    .end local v2    # "personSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v7    # "xuid":Ljava/lang/String;
    :cond_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 161
    :try_start_0
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->queryList(Ljava/util/ArrayList;)Ljava/util/List;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 168
    .local v3, "queryResult":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :goto_2
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 169
    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 173
    .end local v3    # "queryResult":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    :cond_2
    return-object v5

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v8, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->TAG:Ljava/lang/String;

    const-string v9, "Failed to load PeopleHubPersonSummary"

    invoke-static {v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .restart local v3    # "queryResult":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    goto :goto_2
.end method

.method public lazyQuery(Ljava/lang/String;)V
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 125
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getObservers()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->scheduledFuture:Ljava/util/concurrent/ScheduledFuture;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 128
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->startPerformLazyQueryTask()V

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->requestedXuids:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    monitor-exit v1

    .line 134
    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
