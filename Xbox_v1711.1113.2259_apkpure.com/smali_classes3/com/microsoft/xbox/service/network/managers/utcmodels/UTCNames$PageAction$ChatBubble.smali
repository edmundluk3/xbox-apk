.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$ChatBubble;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatBubble"
.end annotation


# static fields
.field public static final AddPeople:Ljava/lang/String; = "ChatBubble - Add People"

.field public static final Club:Ljava/lang/String; = "ChatBubble - Show Club as ChatBubble"

.field public static final ClubDeleteMessage:Ljava/lang/String; = "ChatBubble - Club Delete Message"

.field public static final ClubReportToAdmin:Ljava/lang/String; = "ChatBubble - Club Report Message to Admin"

.field public static final ClubReportToXbox:Ljava/lang/String; = "ChatBubble - Club Report Message to Xbox"

.field public static final ClubSendMessage:Ljava/lang/String; = "ChatBubble - Club Send Message"

.field public static final ClubSettings:Ljava/lang/String; = "ChatBubble - Club Settings"

.field public static final ClubTab:Ljava/lang/String; = "ChatBubble - Show Club Tab"

.field public static final ClubViewProfile:Ljava/lang/String; = "ChatBubble - Club View Profile"

.field public static final Conversation:Ljava/lang/String; = "ChatBubble - Show Conversation as ChatBubble"

.field public static final CopyText:Ljava/lang/String; = "ChatBubble - Copy Text"

.field public static final CreateGroup:Ljava/lang/String; = "ChatBubble - Create Group Conversation"

.field public static final Delete:Ljava/lang/String; = "ChatBubble - Delete Bubble"

.field public static final DeleteMessage:Ljava/lang/String; = "ChatBubble - Delete Message"

.field public static final Expand:Ljava/lang/String; = "ChatBubble - Expand Bubbles"

.field public static final Hide:Ljava/lang/String; = "ChatBubble - Hide Bubbles"

.field public static final Leave:Ljava/lang/String; = "ChatBubble - Leave Group Conversation"

.field public static final MessageOfTheDay:Ljava/lang/String; = "ChatBubble - Message of the Day"

.field public static final MessageTab:Ljava/lang/String; = "ChatBubble - Show Message Tab"

.field public static final Mute:Ljava/lang/String; = "ChatBubble - Mute"

.field public static final OpenAsChatBubble:Ljava/lang/String; = "ChatBubble - Open as ChatBubble"

.field public static final Rename:Ljava/lang/String; = "ChatBubble - Rename Conversation"

.field public static final Report:Ljava/lang/String; = "ChatBubble - Report Message"

.field public static final SendMessage:Ljava/lang/String; = "ChatBubble - Send Message"

.field public static final Settings:Ljava/lang/String; = "ChatBubble - Settings"

.field public static final Share:Ljava/lang/String; = "ChatBubble - Share"

.field public static final ShowClub:Ljava/lang/String; = "ChatBubble - Show Club"

.field public static final ShowConversation:Ljava/lang/String; = "ChatBubble - Show Conversation"

.field public static final Unmute:Ljava/lang/String; = "ChatBubble - Unmute"

.field public static final ViewPeople:Ljava/lang/String; = "ChatBubble - View People"

.field public static final ViewProfile:Ljava/lang/String; = "ChatBubble - View Profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
