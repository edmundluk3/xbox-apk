.class Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PinsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/pins/PinsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPinsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/pins/PinsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/pins/PinsModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/pins/PinsModel$1;

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 312
    sget-object v6, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_Pins:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const/16 v7, 0xc8

    invoke-static {v6, v5, v7}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getList(Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;II)[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    move-result-object v2

    .line 314
    .local v2, "listItems":[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 315
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    array-length v6, v2

    invoke-direct {v3, v6}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 317
    .local v3, "pinHydrateComplete":Ljava/util/concurrent/CountDownLatch;
    array-length v6, v2

    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v1, v2, v5

    .line 318
    .local v1, "listItem":Lcom/microsoft/xbox/service/model/epg/EPListItem;
    sget-object v7, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v8, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {v8, v9, v1, v3}, Lcom/microsoft/xbox/service/model/pins/PinsModel$SinglePinRunner;-><init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/service/model/epg/EPListItem;Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {v7, v8}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 317
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 322
    .end local v1    # "listItem":Lcom/microsoft/xbox/service/model/epg/EPListItem;
    :cond_0
    const-wide/16 v6, 0x7530

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v6, v7, v5}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v6

    monitor-enter v6

    .line 329
    :try_start_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 330
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 331
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    .end local v3    # "pinHydrateComplete":Ljava/util/concurrent/CountDownLatch;
    .end local v4    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    :goto_2
    return-object v4

    .line 323
    .restart local v3    # "pinHydrateComplete":Ljava/util/concurrent/CountDownLatch;
    :catch_0
    move-exception v0

    .line 324
    .local v0, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$600()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Interrupted app retrieval: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 331
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 337
    .end local v3    # "pinHydrateComplete":Ljava/util/concurrent/CountDownLatch;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_2
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 347
    const-wide/16 v0, 0xfba

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 342
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$700(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 343
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 306
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$500(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 307
    monitor-exit v1

    .line 308
    return-void

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
