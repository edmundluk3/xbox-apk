.class public Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "PageUserActivityFeedScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private pageId:Ljava/lang/String;

.field private pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 28
    const-string v2, ""

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageId:Ljava/lang/String;

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 33
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPageUserAuthorInfo()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-result-object v0

    .line 34
    .local v0, "info":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
    if-eqz v0, :cond_0

    .line 35
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageId:Ljava/lang/String;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageId:Ljava/lang/String;

    .line 37
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/PagesModel;->getInstance()Lcom/microsoft/xbox/service/model/PagesModel;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/PagesModel;->getPageModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    .line 38
    return-void
.end method


# virtual methods
.method public canPinPosts()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method protected getActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->getPageActivityFeedItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705b5

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method public getTimelineId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Page:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->loadPageActivityFeed(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 93
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to get page activity feed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_0
    return-object v0
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserActivityFeedScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->shouldRefreshPageActivityFeed()Z

    move-result v0

    return v0
.end method

.method public showActivityAlertNotificationBar()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
