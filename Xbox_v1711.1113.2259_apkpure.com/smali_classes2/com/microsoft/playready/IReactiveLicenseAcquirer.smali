.class interface abstract Lcom/microsoft/playready/IReactiveLicenseAcquirer;
.super Ljava/lang/Object;
.source "ILicenseAcquirer.java"

# interfaces
.implements Lcom/microsoft/playready/IExtensibleLicenseAcquirer;


# virtual methods
.method public abstract acquireLicense(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;)Lcom/microsoft/playready/ILicenseAcquisitionTask;
.end method

.method public abstract addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
.end method

.method public abstract removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
.end method
