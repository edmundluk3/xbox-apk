.class public final Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes;
.super Ljava/lang/Object;
.source "ClubSearchDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;,
        Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;,
        Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;,
        Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
