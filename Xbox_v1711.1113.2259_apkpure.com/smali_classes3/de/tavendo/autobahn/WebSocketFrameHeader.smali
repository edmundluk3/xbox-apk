.class public Lde/tavendo/autobahn/WebSocketFrameHeader;
.super Ljava/lang/Object;
.source "WebSocketFrameHeader.java"


# instance fields
.field private mFin:Z

.field private mHeaderLen:I

.field private mMask:[B

.field private mOpcode:I

.field private mPayloadLen:I

.field private mReserved:I

.field private mTotalLen:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHeaderLength()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mHeaderLen:I

    return v0
.end method

.method public getMask()[B
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mMask:[B

    return-object v0
.end method

.method public getOpcode()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mOpcode:I

    return v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mPayloadLen:I

    return v0
.end method

.method public getReserved()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mReserved:I

    return v0
.end method

.method public getTotalLength()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mTotalLen:I

    return v0
.end method

.method public isFin()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mFin:Z

    return v0
.end method

.method public setFin(Z)V
    .locals 0
    .param p1, "fin"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mFin:Z

    .line 23
    return-void
.end method

.method public setHeaderLength(I)V
    .locals 0
    .param p1, "headerLength"    # I

    .prologue
    .line 34
    iput p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mHeaderLen:I

    .line 35
    return-void
.end method

.method public setMask([B)V
    .locals 0
    .param p1, "mask"    # [B

    .prologue
    .line 52
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mMask:[B

    .line 53
    return-void
.end method

.method public setOpcode(I)V
    .locals 0
    .param p1, "opcode"    # I

    .prologue
    .line 16
    iput p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mOpcode:I

    .line 17
    return-void
.end method

.method public setPayloadLength(I)V
    .locals 0
    .param p1, "payloadLength"    # I

    .prologue
    .line 40
    iput p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mPayloadLen:I

    .line 41
    return-void
.end method

.method public setReserved(I)V
    .locals 0
    .param p1, "reserved"    # I

    .prologue
    .line 28
    iput p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mReserved:I

    .line 29
    return-void
.end method

.method public setTotalLen(I)V
    .locals 0
    .param p1, "totalLength"    # I

    .prologue
    .line 46
    iput p1, p0, Lde/tavendo/autobahn/WebSocketFrameHeader;->mTotalLen:I

    .line 47
    return-void
.end method
