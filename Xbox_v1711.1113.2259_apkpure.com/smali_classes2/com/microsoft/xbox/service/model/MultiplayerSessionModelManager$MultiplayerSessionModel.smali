.class Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiplayerSessionModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
        ">;>;"
    }
.end annotation


# instance fields
.field private clubIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

.field private listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

.field private serviceConfigId:Ljava/lang/String;

.field private sessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "serviceConfigId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 515
    .local p4, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 516
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->serviceConfigId:Ljava/lang/String;

    .line 517
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 518
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 519
    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->clubIds:Ljava/util/List;

    .line 520
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->sessions:Ljava/util/List;

    .line 521
    return-void

    .line 519
    :cond_0
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getSessions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 525
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->sessions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 6
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 533
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->serviceConfigId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->clubIds:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 534
    .local v0, "runner":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadAsync(Z)V
    .locals 7
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 538
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    move-object v6, v0

    .line 539
    :goto_0
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->serviceConfigId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->clubIds:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 538
    invoke-virtual {p0, p1, v6, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 541
    return-void

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 539
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->access$200(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    move-object v6, v0

    goto :goto_0
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->sessions:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 545
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 546
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 547
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 548
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->sessions:Ljava/util/List;

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-eqz v0, :cond_1

    .line 552
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->listType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$300(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 556
    :goto_0
    return-void

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$400(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method
