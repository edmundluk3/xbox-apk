.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PinUnpinFeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final feedItemIdForDeletion:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final itemRoot:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final locatorForDeletion:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final pin:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "pin"    # Z
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "feedItemIdForDeletion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "locatorForDeletion"    # Ljava/lang/String;

    .prologue
    .line 1432
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1422
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 1433
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1434
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    .line 1435
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->pin:Z

    .line 1436
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->feedItemIdForDeletion:Ljava/lang/String;

    .line 1437
    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->locatorForDeletion:Ljava/lang/String;

    .line 1438
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1442
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1443
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 1459
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    .line 1460
    .local v1, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getTimelineId()Ljava/lang/String;

    move-result-object v0

    .line 1462
    .local v0, "timelineId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->pin:Z

    .line 1463
    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->pinFeedItem(Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 1462
    :goto_0
    return-object v2

    .line 1463
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1421
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1454
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1421
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1448
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->pin:Z

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1450
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->pin:Z

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->feedItemIdForDeletion:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->locatorForDeletion:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1476
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1421
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1469
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$PinUnpinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1471
    return-void
.end method
