.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
.source "ClubAdminReportListItems.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubReportCommentListItem"
.end annotation


# instance fields
.field private final comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;)V
    .locals 0
    .param p1, "reportedItem"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "comment"    # Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)V

    .line 312
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    .line 313
    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    iget-object v0, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->text:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCreatorXuid()Ljava/lang/Long;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    iget-object v0, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->xuid:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGamerpic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGamertag()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    const-string v1, ""

    .line 342
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 341
    :goto_0
    return-object v0

    .line 342
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getParentLocator()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    iget-object v0, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->parentPath:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 348
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    const-string v1, ""

    .line 349
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 348
    :goto_0
    return-object v0

    .line 349
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public hasValidContent()Z
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
