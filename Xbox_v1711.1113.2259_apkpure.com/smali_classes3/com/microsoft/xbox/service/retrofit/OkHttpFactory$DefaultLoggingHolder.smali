.class Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;
.super Ljava/lang/Object;
.source "OkHttpFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultLoggingHolder"
.end annotation


# static fields
.field private static final INSTANCE:Lokhttp3/logging/HttpLoggingInterceptor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lokhttp3/logging/HttpLoggingInterceptor;

    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder$$Lambda$1;->lambdaFactory$()Lokhttp3/logging/HttpLoggingInterceptor$Logger;

    move-result-object v1

    invoke-direct {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;-><init>(Lokhttp3/logging/HttpLoggingInterceptor$Logger;)V

    sput-object v0, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;->INSTANCE:Lokhttp3/logging/HttpLoggingInterceptor;

    .line 21
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;->INSTANCE:Lokhttp3/logging/HttpLoggingInterceptor;

    sget-object v1, Lokhttp3/logging/HttpLoggingInterceptor$Level;->BASIC:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    invoke-virtual {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;->setLevel(Lokhttp3/logging/HttpLoggingInterceptor$Level;)Lokhttp3/logging/HttpLoggingInterceptor;

    .line 22
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$300()Lokhttp3/logging/HttpLoggingInterceptor;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;->INSTANCE:Lokhttp3/logging/HttpLoggingInterceptor;

    return-object v0
.end method

.method static synthetic lambda$static$0(Ljava/lang/String;)V
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 20
    const-string v0, "OkHttp"

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
