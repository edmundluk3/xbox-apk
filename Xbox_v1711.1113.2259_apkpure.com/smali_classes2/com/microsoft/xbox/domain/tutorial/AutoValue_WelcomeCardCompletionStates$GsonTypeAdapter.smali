.class public final Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_WelcomeCardCompletionStates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultClubWelcomeCardCompleted:Z

.field private defaultFacebookWelcomeCardCompleted:Z

.field private defaultRedeemWelcomeCardCompleted:Z

.field private defaultSetupWelcomeCardCompleted:Z

.field private defaultShopWelcomeCardCompleted:Z

.field private defaultSuggestedFriendsWelcomeCardCompleted:Z

.field private final facebookWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final setupWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final shopWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestedFriendsWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 28
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSetupWelcomeCardCompleted:Z

    .line 29
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultRedeemWelcomeCardCompleted:Z

    .line 30
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultShopWelcomeCardCompleted:Z

    .line 31
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultFacebookWelcomeCardCompleted:Z

    .line 32
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSuggestedFriendsWelcomeCardCompleted:Z

    .line 33
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultClubWelcomeCardCompleted:Z

    .line 35
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->setupWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->redeemWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->shopWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->facebookWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->suggestedFriendsWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->clubWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 9
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 92
    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    .line 94
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 95
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSetupWelcomeCardCompleted:Z

    .line 96
    .local v1, "setupWelcomeCardCompleted":Z
    iget-boolean v2, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultRedeemWelcomeCardCompleted:Z

    .line 97
    .local v2, "redeemWelcomeCardCompleted":Z
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultShopWelcomeCardCompleted:Z

    .line 98
    .local v3, "shopWelcomeCardCompleted":Z
    iget-boolean v4, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultFacebookWelcomeCardCompleted:Z

    .line 99
    .local v4, "facebookWelcomeCardCompleted":Z
    iget-boolean v5, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSuggestedFriendsWelcomeCardCompleted:Z

    .line 100
    .local v5, "suggestedFriendsWelcomeCardCompleted":Z
    iget-boolean v6, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultClubWelcomeCardCompleted:Z

    .line 101
    .local v6, "clubWelcomeCardCompleted":Z
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 102
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 103
    .local v7, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_1

    .line 104
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 107
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 133
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 107
    :sswitch_0
    const-string v8, "setupWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "redeemWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "shopWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "facebookWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "suggestedFriendsWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "clubWelcomeCardCompleted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    .line 109
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->setupWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 110
    goto :goto_1

    .line 113
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->redeemWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 114
    goto :goto_1

    .line 117
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->shopWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 118
    goto/16 :goto_1

    .line 121
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->facebookWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 122
    goto/16 :goto_1

    .line 125
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->suggestedFriendsWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 126
    goto/16 :goto_1

    .line 129
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->clubWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 130
    goto/16 :goto_1

    .line 137
    .end local v7    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 138
    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates;-><init>(ZZZZZZ)V

    goto/16 :goto_0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        -0x72a95711 -> :sswitch_2
        -0x3797010a -> :sswitch_0
        -0x29b90f15 -> :sswitch_4
        0x22b460bf -> :sswitch_3
        0x6cc912af -> :sswitch_5
        0x7d7bb895 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultClubWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClubWelcomeCardCompleted"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultClubWelcomeCardCompleted:Z

    .line 64
    return-object p0
.end method

.method public setDefaultFacebookWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFacebookWelcomeCardCompleted"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultFacebookWelcomeCardCompleted:Z

    .line 56
    return-object p0
.end method

.method public setDefaultRedeemWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRedeemWelcomeCardCompleted"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultRedeemWelcomeCardCompleted:Z

    .line 48
    return-object p0
.end method

.method public setDefaultSetupWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSetupWelcomeCardCompleted"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSetupWelcomeCardCompleted:Z

    .line 44
    return-object p0
.end method

.method public setDefaultShopWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShopWelcomeCardCompleted"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultShopWelcomeCardCompleted:Z

    .line 52
    return-object p0
.end method

.method public setDefaultSuggestedFriendsWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSuggestedFriendsWelcomeCardCompleted"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->defaultSuggestedFriendsWelcomeCardCompleted:Z

    .line 60
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    if-nez p2, :cond_0

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 87
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 74
    const-string v0, "setupWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->setupWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->setupWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 76
    const-string v0, "redeemWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->redeemWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->redeemWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 78
    const-string v0, "shopWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->shopWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->shopWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 80
    const-string v0, "facebookWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->facebookWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->facebookWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 82
    const-string v0, "suggestedFriendsWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->suggestedFriendsWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 84
    const-string v0, "clubWelcomeCardCompleted"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->clubWelcomeCardCompletedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->clubWelcomeCardCompleted()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 86
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)V

    return-void
.end method
