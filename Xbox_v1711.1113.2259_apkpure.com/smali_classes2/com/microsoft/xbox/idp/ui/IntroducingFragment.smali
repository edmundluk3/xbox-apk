.class public Lcom/microsoft/xbox/idp/ui/IntroducingFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "IntroducingFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;,
        Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;,
        Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;
    }
.end annotation


# static fields
.field private static final KEY_STATE:Ljava/lang/String; = "IntroducingFragment.KEY_STATE"

.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final bitmapCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/BitmapLoader$Result;",
            ">;"
        }
    .end annotation
.end field

.field private bottomBarShadow:Landroid/view/View;

.field private callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

.field private displayNameText:Landroid/widget/TextView;

.field private gamerTagText:Landroid/widget/TextView;

.field private gamerpicView:Landroid/widget/ImageView;

.field private final loaderMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final profileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/Profile$Response;",
            ">;>;"
        }
    .end annotation
.end field

.field private scrollView:Landroid/widget/ScrollView;

.field private state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

.field private user:Lcom/microsoft/xbox/idp/model/Profile$User;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    .line 311
    new-instance v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$4;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->loaderMap:Landroid/util/SparseArray;

    .line 47
    sget-object v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    .line 181
    new-instance v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$2;-><init>(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->bitmapCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 211
    new-instance v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$3;-><init>(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->profileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_intro_gamer_profile:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->profileCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_intro_gamer_image:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/BitmapLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->bitmapCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/BitmapLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->scrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->bottomBarShadow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->gamerpicView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Lcom/microsoft/xbox/idp/model/Profile$User;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->user:Lcom/microsoft/xbox/idp/model/Profile$User;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;Lcom/microsoft/xbox/idp/model/Profile$User;)Lcom/microsoft/xbox/idp/model/Profile$User;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;
    .param p1, "x1"    # Lcom/microsoft/xbox/idp/model/Profile$User;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->user:Lcom/microsoft/xbox/idp/model/Profile$User;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->displayNameText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->gamerTagText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getLoaderInfo(I)Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;
    .locals 1
    .param p1, "loaderId"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->loaderMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 147
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->getActivityResult(IILandroid/content/Intent;)Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;

    move-result-object v0

    .line 151
    .local v0, "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;->isTryAgain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 153
    sget-object v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    const-string v2, "Trying again"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->deleteLoader()V

    .line 162
    .end local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_0
    :goto_0
    return-void

    .line 156
    .restart local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iput-object v3, v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    .line 157
    sget-object v1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    const-string v2, "onActivityResult"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 66
    check-cast p1, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    .line 67
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 167
    .local v0, "id":I
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_done:I

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->user:Lcom/microsoft/xbox/idp/model/Profile$User;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCIntroducing;->trackDone(Lcom/microsoft/xbox/idp/model/Profile$User;Ljava/lang/CharSequence;)V

    .line 172
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    sget-object v3, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;->NO_ERROR:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->user:Lcom/microsoft/xbox/idp/model/Profile$User;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->user:Lcom/microsoft/xbox/idp/model/Profile$User;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/model/Profile$User;->id:Ljava/lang/String;

    :goto_0
    invoke-interface {v2, v3, v1}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Status;Ljava/lang/String;)V

    .line 174
    :cond_0
    return-void

    .line 172
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 83
    if-nez p1, :cond_1

    new-instance v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->setActivityContext(Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;)V

    .line 88
    :cond_0
    return-void

    .line 83
    :cond_1
    const-string v0, "IntroducingFragment.KEY_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_introducing:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->removePage()V

    .line 75
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 76
    sget-object v0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$Callbacks;

    .line 77
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 124
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onResume()V

    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 127
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 128
    sget-object v2, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    const-string v3, "Initializing loader_intro_gamer_profile"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 130
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_RESULT_KEY"

    new-instance v3, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;

    const-class v4, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;

    sget v5, Lcom/microsoft/xbox/idp/R$id;->loader_intro_gamer_profile:I

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 131
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget v3, Lcom/microsoft/xbox/idp/R$id;->loader_intro_gamer_profile:I

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->initLoader(ILandroid/os/Bundle;)Z

    .line 137
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->TAG:Ljava/lang/String;

    const-string v3, "No arguments provided"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 142
    const-string v0, "IntroducingFragment.KEY_STATE"

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->state:Lcom/microsoft/xbox/idp/ui/IntroducingFragment$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 143
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_scroll_container:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->scrollView:Landroid/widget/ScrollView;

    .line 100
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_bottom_bar_shadow:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->bottomBarShadow:Landroid/view/View;

    .line 101
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->scrollView:Landroid/widget/ScrollView;

    new-instance v4, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment$1;-><init>(Lcom/microsoft/xbox/idp/ui/IntroducingFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 107
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_gamerpic:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->gamerpicView:Landroid/widget/ImageView;

    .line 108
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_display_name:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->displayNameText:Landroid/widget/TextView;

    .line 109
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_gamertag:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->gamerTagText:Landroid/widget/TextView;

    .line 110
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_done:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 111
    .local v2, "doneButton":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/microsoft/xbox/idp/R$string;->xbid_lets_play_android:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/idp/ui/UiUtil;->applyTypeface(Ljava/lang/CharSequence;Landroid/content/Context;)Landroid/text/SpannableString;

    move-result-object v0

    .line 112
    .local v0, "applyStyleText":Landroid/text/SpannableString;
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/IntroducingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 117
    .local v1, "args":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    const-string v3, "ARG_ALT_BUTTON_TEXT"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    const-string v3, "ARG_ALT_BUTTON_TEXT"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_0
    return-void
.end method
