.class public Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
.super Ljava/util/HashMap;
.source "InstrumentationParameters.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final PurchaseOriginatingSource:Ljava/lang/String; = "PurchaseOriginatingSource"

.field private static final StoreFilterPosition:Ljava/lang/String; = "StoreFilterPos"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 12
    return-void
.end method


# virtual methods
.method public getHasStoreFilterPosition()Z
    .locals 1

    .prologue
    .line 19
    const-string v0, "StoreFilterPos"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPurchaseOriginatingSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "PurchaseOriginatingSource"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStoreFilterPosition()I
    .locals 1

    .prologue
    .line 23
    const-string v0, "StoreFilterPos"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public putPurchaseOriginatingSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 27
    const-string v0, "PurchaseOriginatingSource"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method

.method public putStoreFilterPosition(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 15
    const-string v0, "StoreFilterPos"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    return-void
.end method
