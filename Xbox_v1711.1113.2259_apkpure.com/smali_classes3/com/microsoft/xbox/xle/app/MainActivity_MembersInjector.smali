.class public final Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;
.super Ljava/lang/Object;
.source "MainActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/xle/app/MainActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final authStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final homeScreenRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final partyChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final partyEventNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final reactInstanceManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p2, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    .local p3, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p4, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p5, "partyEventNotifierProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;>;"
    .local p6, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    .line 39
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->reactInstanceManagerProvider:Ljavax/inject/Provider;

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 43
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 45
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->partyEventNotifierProvider:Ljavax/inject/Provider;

    .line 47
    sget-boolean v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 49
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    .local p1, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    .local p2, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    .local p3, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p4, "partyEventNotifierProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;>;"
    .local p5, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAuthStateManager(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 108
    return-void
.end method

.method public static injectHomeScreenRepository(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "homeScreenRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 83
    return-void
.end method

.method public static injectPartyChatRepository(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "partyChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 93
    return-void
.end method

.method public static injectPartyEventNotifier(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "partyEventNotifierProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyEventNotifier:Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    .line 103
    return-void
.end method

.method public static injectReactInstanceManager(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "reactInstanceManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/facebook/react/ReactInstanceManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactInstanceManager;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 88
    return-void
.end method

.method public static injectTutorialRepository(Lcom/microsoft/xbox/xle/app/MainActivity;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 98
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/xle/app/MainActivity;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/xle/app/MainActivity;

    .prologue
    .line 69
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->homeScreenRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->reactInstanceManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/ReactInstanceManager;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->partyEventNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->partyEventNotifier:Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 78
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->injectMembers(Lcom/microsoft/xbox/xle/app/MainActivity;)V

    return-void
.end method
