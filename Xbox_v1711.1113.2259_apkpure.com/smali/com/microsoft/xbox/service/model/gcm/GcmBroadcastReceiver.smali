.class public Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GcmBroadcastReceiver.java"


# static fields
.field private static MESSAGE_MODEL_OBSERVER:Lcom/microsoft/xbox/toolkit/XLEObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getMessageText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "conversationId"    # Ljava/lang/String;
    .param p2, "skypeMessageType"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 704
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object p3, v1

    .line 723
    .end local p3    # "content":Ljava/lang/String;
    :goto_0
    return-object p3

    .line 708
    .restart local p3    # "content":Ljava/lang/String;
    :cond_1
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 710
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$1;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 712
    :pswitch_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070639

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberInitiator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getAddMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupSenderGamertagText(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 714
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07065a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getDeleteMemberSkypeId(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupSenderGamertagText(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 716
    :pswitch_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07063d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupMemberGamertag(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_0

    :pswitch_3
    move-object p3, v1

    .line 723
    goto/16 :goto_0

    .line 710
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSenderXuidFromSkypeNotification(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "senderId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 727
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 738
    :goto_0
    return-object v1

    .line 731
    :cond_0
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 733
    .local v0, "skypeId":[Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 734
    const/4 v1, 0x1

    aget-object v1, v0, v1

    goto :goto_0

    .line 737
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid skype id received in push notification payload: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isConversationPropertyAddRemoveNotification(Ljava/lang/String;)Z
    .locals 3
    .param p1, "messageType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 686
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 691
    :cond_0
    :goto_0
    return v1

    .line 689
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 691
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isConversationPropertyNotification(Ljava/lang/String;)Z
    .locals 3
    .param p1, "messageType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 695
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 700
    :cond_0
    :goto_0
    return v1

    .line 698
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 700
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->AddMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->RemoveMember:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static synthetic lambda$processSkypeMessage$0(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;
    .param p1, "messageModel"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p2, "conversationId"    # Ljava/lang/String;
    .param p3, "notificationResponseData"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    .param p4, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p5, "threadTopic"    # Ljava/lang/String;
    .param p6, "asyncResult"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 384
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    .line 385
    .local v0, "conversationSummary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    if-nez v3, :cond_0

    .line 402
    :goto_0
    return-void

    .line 389
    :cond_0
    invoke-virtual {p6}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 391
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 393
    :pswitch_0
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v4, "Finished loading skype conversation messages"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->MESSAGE_MODEL_OBSERVER:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {p1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 397
    iget-object v3, p3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    iget-object v4, p3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->content:Ljava/lang/String;

    invoke-direct {p0, p2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->getMessageText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "messageText":Ljava/lang/String;
    invoke-virtual {p4, v1, p2, p5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private processAchievementNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 178
    const-string v4, "gcm.notification.title"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "title":Ljava/lang/String;
    const-string v4, "gcm.notification.body"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "body":Ljava/lang/String;
    const-string/jumbo v4, "xbl"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "messageDetailsJson":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 183
    const-class v4, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    .line 184
    .local v2, "notification":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    if-eqz v2, :cond_1

    .line 185
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v0, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayAchievementNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    .end local v2    # "notification":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    :cond_0
    :goto_0
    return-void

    .line 187
    .restart local v2    # "notification":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not parse achievement notification details: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processChatNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 453
    const-string v6, "gcm.notification.title_loc_key"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 454
    .local v5, "titleStringId":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Process chat notification of type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 457
    const-string v6, "gcm.notification.title_loc_args"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 458
    .local v4, "titleStringArgs":Ljava/lang/String;
    const-string/jumbo v6, "xbl"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 459
    .local v2, "notificationDetailJson":Ljava/lang/String;
    const-class v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;

    .line 463
    .local v0, "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v7, "string"

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v6, v5, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 464
    .local v3, "resourceId":I
    if-lez v3, :cond_2

    .line 466
    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;->getDetails()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;

    move-result-object v6

    :goto_0
    invoke-virtual {p1, v7, v4, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayChatNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    .end local v0    # "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    .end local v2    # "notificationDetailJson":Ljava/lang/String;
    .end local v3    # "resourceId":I
    .end local v4    # "titleStringArgs":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 466
    .restart local v0    # "chatNotification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotification;
    .restart local v2    # "notificationDetailJson":Ljava/lang/String;
    .restart local v3    # "resourceId":I
    .restart local v4    # "titleStringArgs":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 467
    :catch_0
    move-exception v1

    .line 468
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not find resource with ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 471
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v7, "gcm.notification.title_loc_key null or empty"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private processClubNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 251
    const-string v6, "gcm.notification.body_loc_key"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 252
    .local v4, "messageStringId":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Processing club notification of type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 255
    const-string v6, "gcm.notification.body_loc_args"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 256
    .local v3, "messageStringArgs":Ljava/lang/String;
    const-string/jumbo v6, "xbl"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 257
    .local v2, "messageDetailsJson":Ljava/lang/String;
    const-class v6, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;

    .line 261
    .local v0, "clubNotification":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v7, "string"

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v6, v4, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 264
    .local v5, "resourceId":I
    if-nez v5, :cond_0

    .line 265
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_Android"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "string"

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 268
    :cond_0
    if-lez v5, :cond_2

    .line 270
    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;->details()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    move-result-object v6

    :goto_0
    invoke-virtual {p1, v7, v3, v4, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayClubsNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    .end local v0    # "clubNotification":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;
    .end local v2    # "messageDetailsJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "resourceId":I
    :goto_1
    return-void

    .line 270
    .restart local v0    # "clubNotification":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;
    .restart local v2    # "messageDetailsJson":Ljava/lang/String;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "resourceId":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to load string resource with ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 275
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not find resource with ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 278
    .end local v0    # "clubNotification":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;
    .end local v2    # "messageDetailsJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "resourceId":I
    :cond_3
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v7, "body_loc_key null or empty"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private processConsumptionHorizonMessage(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 325
    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Processing skype push notification for consumption horizon message"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 328
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Invalid bundle data"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_1
    :goto_0
    return-void

    .line 332
    :cond_2
    const-string v4, "conversationId"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 333
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v2

    .line 334
    .local v2, "isGroupConversation":Z
    if-eqz v2, :cond_3

    move-object v0, v1

    .line 335
    .local v0, "conversationId":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v3

    .line 337
    .local v3, "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v4

    if-nez v4, :cond_1

    .line 338
    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "The app is not paused, the message model is present - loading current message list"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 341
    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "current conversation is active"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 334
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v3    # "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    :cond_3
    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 346
    .restart local v0    # "conversationId":Ljava/lang/String;
    .restart local v3    # "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    :cond_4
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    goto :goto_0
.end method

.method private processFavBroadcast(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 172
    const-string v2, "gamertag"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "gamertag":Ljava/lang/String;
    const-string/jumbo v2, "xuid"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "xuid":Ljava/lang/String;
    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayFavBroadcast(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method private processFavOnline(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 166
    const-string v2, "gamertag"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "gamertag":Ljava/lang/String;
    const-string/jumbo v2, "xuid"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, "xuid":Ljava/lang/String;
    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayFavOnline(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method private processLfgNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 223
    const-string v8, "gcm.notification.click_action"

    const-string v9, ""

    invoke-virtual {p2, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "clickAction":Ljava/lang/String;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Processing lfg notification with type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v8, "gcm.notification.body_loc_key"

    const-string v9, ""

    invoke-virtual {p2, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 227
    .local v4, "messageStringId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 228
    const-string v8, "gcm.notification.body_loc_args"

    const-string v9, ""

    invoke-virtual {p2, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 229
    .local v3, "messageStringArgs":Ljava/lang/String;
    const-string/jumbo v8, "xbl"

    const-string v9, ""

    invoke-virtual {p2, v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 230
    .local v2, "messageHandleJson":Ljava/lang/String;
    const-class v8, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;

    invoke-static {v2, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;

    .line 234
    .local v6, "searchHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v9, "string"

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v8, v4, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 236
    .local v5, "resourceId":I
    if-lez v5, :cond_2

    .line 238
    :try_start_0
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;->getSearchHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v8

    :goto_0
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;->lfg_notification()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotificationBody;->type()Ljava/lang/String;

    move-result-object v7

    :cond_0
    invoke-virtual {p1, v9, v3, v8, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayLfgNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    .end local v2    # "messageHandleJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "resourceId":I
    .end local v6    # "searchHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    :goto_1
    return-void

    .restart local v2    # "messageHandleJson":Ljava/lang/String;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "resourceId":I
    .restart local v6    # "searchHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    :cond_1
    move-object v8, v7

    .line 238
    goto :goto_0

    .line 239
    :catch_0
    move-exception v1

    .line 240
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v7, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to load string resource with ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 243
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_2
    sget-object v7, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not find resource with ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 246
    .end local v2    # "messageHandleJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "resourceId":I
    .end local v6    # "searchHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgNotification;
    :cond_3
    sget-object v7, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v8, "body_loc_key null or empty"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private processPartyInvite(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 193
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v7, "processing party invite"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v6, "gcm.notification.body_loc_key"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 196
    .local v4, "messageStringId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 197
    const-string v6, "gcm.notification.body_loc_args"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 198
    .local v3, "messageStringArgs":Ljava/lang/String;
    const-string/jumbo v6, "xbl"

    const-string v7, ""

    invoke-virtual {p2, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "messageHandleJson":Ljava/lang/String;
    const-class v6, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;

    .line 201
    .local v1, "inviteNotification":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    if-eqz v1, :cond_1

    .line 204
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v7, "string"

    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v6, v4, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 205
    .local v5, "resourceId":I
    if-lez v5, :cond_0

    .line 207
    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;->inviteHandle()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;

    move-result-object v7

    invoke-virtual {p1, v6, v3, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayPartyInviteNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$InviteHandle;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .end local v1    # "inviteNotification":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    .end local v2    # "messageHandleJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "resourceId":I
    :goto_0
    return-void

    .line 208
    .restart local v1    # "inviteNotification":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    .restart local v2    # "messageHandleJson":Ljava/lang/String;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "resourceId":I
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to load string resource with ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_0
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not find resource with ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    .end local v5    # "resourceId":I
    :cond_1
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to parse invite json: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    .end local v1    # "inviteNotification":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartyInviteNotification;
    .end local v2    # "messageHandleJson":Ljava/lang/String;
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v7, "body_loc_key null or empty"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processSkypeMessage(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 23
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 351
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Processing skype push notification"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 354
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Invalid bundle data"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_1
    :goto_0
    return-void

    .line 358
    :cond_2
    const-string v1, "rawPayload"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 360
    .local v18, "skypePayloadJSONString":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 361
    const-class v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    move-object/from16 v0, v18

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 363
    .local v4, "notificationResponseData":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    iget-object v1, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 364
    .local v10, "id":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v10}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v12, 0x1

    .line 365
    .local v12, "isGroupConversation":Z
    :goto_1
    iget-object v14, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    .line 366
    .local v14, "messageId":Ljava/lang/String;
    iget-object v9, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->imdisplayname:Ljava/lang/String;

    .line 367
    .local v9, "gamerTag":Ljava/lang/String;
    iget-object v6, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->threadtopic:Ljava/lang/String;

    .line 368
    .local v6, "threadTopic":Ljava/lang/String;
    const-string/jumbo v1, "senderId"

    const-string v5, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->getSenderXuidFromSkypeNotification(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 369
    .local v16, "senderXuid":Ljava/lang/String;
    if-nez v12, :cond_4

    move-object/from16 v3, v16

    .line 372
    .local v3, "conversationId":Ljava/lang/String;
    :goto_2
    iget-object v1, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->isConversationPropertyAddRemoveNotification(Ljava/lang/String;)Z

    move-result v7

    .line 373
    .local v7, "addRemoveNotification":Z
    if-eqz v7, :cond_5

    .line 374
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Ignoring push notification for add/remove group members"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 364
    .end local v3    # "conversationId":Ljava/lang/String;
    .end local v6    # "threadTopic":Ljava/lang/String;
    .end local v7    # "addRemoveNotification":Z
    .end local v9    # "gamerTag":Ljava/lang/String;
    .end local v12    # "isGroupConversation":Z
    .end local v14    # "messageId":Ljava/lang/String;
    .end local v16    # "senderXuid":Ljava/lang/String;
    :cond_3
    const/4 v12, 0x0

    goto :goto_1

    .restart local v6    # "threadTopic":Ljava/lang/String;
    .restart local v9    # "gamerTag":Ljava/lang/String;
    .restart local v12    # "isGroupConversation":Z
    .restart local v14    # "messageId":Ljava/lang/String;
    .restart local v16    # "senderXuid":Ljava/lang/String;
    :cond_4
    move-object v3, v10

    .line 369
    goto :goto_2

    .line 378
    .restart local v3    # "conversationId":Ljava/lang/String;
    .restart local v7    # "addRemoveNotification":Z
    :cond_5
    iget-object v1, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->isConversationPropertyNotification(Ljava/lang/String;)Z

    move-result v11

    .line 379
    .local v11, "isConversationProperty":Z
    iget-object v1, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    iget-object v5, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->content:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v1, v5}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->getMessageText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 380
    .local v13, "message":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    .line 382
    .local v2, "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    if-eqz v11, :cond_7

    move-object/from16 v1, p0

    move-object/from16 v5, p1

    .line 383
    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/XLEObserver;

    move-result-object v1

    sput-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->MESSAGE_MODEL_OBSERVER:Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 404
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->MESSAGE_MODEL_OBSERVER:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 405
    invoke-virtual {v2, v3, v12}, Lcom/microsoft/xbox/service/model/MessageModel;->updateFromNotification(Ljava/lang/String;Z)V

    .line 429
    :cond_6
    if-nez v11, :cond_1

    .line 430
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 431
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Displaying notification in system notification bar"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v1

    iget-object v5, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 435
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070b0d

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v9, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v1, v5, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 442
    .local v15, "notificationText":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v3, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    .end local v15    # "notificationText":Ljava/lang/String;
    :cond_7
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Loading current message list"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 410
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-class v5, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    .line 411
    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isServiceRunning(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    const/16 v17, 0x1

    .line 413
    .local v17, "shouldUpdateFromNotification":Z
    :goto_4
    if-eqz v17, :cond_b

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 414
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "current conversation is active - updating the screen. The notification won\'t be displayed."

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    invoke-virtual {v2, v3, v12}, Lcom/microsoft/xbox/service/model/MessageModel;->updateFromNotification(Ljava/lang/String;Z)V

    .line 419
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackMarkConversationRead(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto/16 :goto_0

    .line 411
    .end local v17    # "shouldUpdateFromNotification":Z
    :cond_a
    const/16 v17, 0x0

    goto :goto_4

    .line 423
    .restart local v17    # "shouldUpdateFromNotification":Z
    :cond_b
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v8

    .line 424
    .local v8, "conversationSummary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    if-eqz v8, :cond_6

    iget-boolean v1, v8, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 436
    .end local v8    # "conversationSummary":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .end local v17    # "shouldUpdateFromNotification":Z
    :cond_c
    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Attachment:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v1

    iget-object v5, v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 437
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070b09

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v9, v19, v20

    const/16 v20, 0x1

    sget-object v21, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v22, 0x7f07075d

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v1, v5, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "notificationText":Ljava/lang/String;
    goto/16 :goto_3

    .line 439
    .end local v15    # "notificationText":Ljava/lang/String;
    :cond_d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v19, 0x7f070b09

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v9, v19, v20

    const/16 v20, 0x1

    aput-object v13, v19, v20

    move-object/from16 v0, v19

    invoke-static {v1, v5, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "notificationText":Ljava/lang/String;
    goto/16 :goto_3

    .line 444
    .end local v15    # "notificationText":Ljava/lang/String;
    :cond_e
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Invalid conversationId received in notification"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 448
    .end local v2    # "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    .end local v3    # "conversationId":Ljava/lang/String;
    .end local v4    # "notificationResponseData":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    .end local v6    # "threadTopic":Ljava/lang/String;
    .end local v7    # "addRemoveNotification":Z
    .end local v9    # "gamerTag":Ljava/lang/String;
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "isConversationProperty":Z
    .end local v12    # "isGroupConversation":Z
    .end local v13    # "message":Ljava/lang/String;
    .end local v14    # "messageId":Ljava/lang/String;
    .end local v16    # "senderXuid":Ljava/lang/String;
    :cond_f
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Notification payload was empty, discarding notification"

    invoke-static {v1, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private processSkypeServiceMessage(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 283
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Processing skype push notification for service message"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 286
    :cond_0
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Invalid bundle data"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_1
    :goto_0
    return-void

    .line 290
    :cond_2
    const-string v5, "rawPayload"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 292
    .local v4, "skypePayloadJSONString":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 293
    const-class v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;

    .line 295
    .local v3, "notificationResponseData":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    if-eqz v3, :cond_1

    .line 296
    iget-object v5, v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->conversationLink:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getSkypeIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "conversationId":Ljava/lang/String;
    iget-object v1, v3, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;->id:Ljava/lang/String;

    .line 299
    .local v1, "messageId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    .line 301
    .local v2, "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v5

    if-nez v5, :cond_3

    .line 302
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "The app is not paused, the message model is present - loading current message list"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 305
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->isCurrentConversation(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 306
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "current conversation is active - updating the screen. The notification won\'t be displayed."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 311
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 312
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Displaying service message notification in system notification bar"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070b0a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 315
    :cond_4
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Invalid conversation id received in notification"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 319
    .end local v0    # "conversationId":Ljava/lang/String;
    .end local v1    # "messageId":Ljava/lang/String;
    .end local v2    # "messageModel":Lcom/microsoft/xbox/service/model/MessageModel;
    .end local v3    # "notificationResponseData":Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeMessage;
    :cond_5
    sget-object v5, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Notification payload was empty, discarding notification"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processTournamentMatchNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 15
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 477
    const-string v1, "gcm.notification.body_loc_key"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 478
    .local v9, "messageStringId":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Processing tournament match notification of type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :try_start_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 482
    const-string v1, "gcm.notification.body_loc_args"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 483
    .local v3, "messageStringArgs":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    const-string/jumbo v1, "xbl"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v14, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 484
    .local v14, "xblJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournament_match_available"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 486
    .local v11, "payloadJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournamentGameInvite"

    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 488
    .local v10, "payload":Ljava/lang/String;
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    invoke-static {v10, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    .line 490
    .local v13, "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    if-nez v13, :cond_0

    .line 491
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to deserialize TournamentGameInvite from: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v13    # "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 495
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v13    # "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v2, "string"

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v1, v9, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v12

    .line 497
    .local v12, "resourceId":I
    if-lez v12, :cond_1

    .line 499
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 500
    invoke-virtual {v1, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 503
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->organizer()Ljava/lang/String;

    move-result-object v5

    .line 504
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->tournamentId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p1

    .line 499
    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v7

    .line 506
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load string resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 514
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v8

    .line 515
    .local v8, "ex":Lorg/json/JSONException;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "Failed to parse tournament match json"

    invoke-static {v1, v2, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 509
    .end local v8    # "ex":Lorg/json/JSONException;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v12    # "resourceId":I
    .restart local v13    # "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 512
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "tournamentGameInvite":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "body_loc_key null or empty"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private processTournamentStatusChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 15
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 520
    const-string v1, "gcm.notification.body_loc_key"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 521
    .local v9, "messageStringId":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Processing tournament status change notification of type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :try_start_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 525
    const-string v1, "gcm.notification.body_loc_args"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 526
    .local v3, "messageStringArgs":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    const-string/jumbo v1, "xbl"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v14, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 527
    .local v14, "xblJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournament_status_change"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 529
    .local v11, "payloadJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournamentStateChangeNotification"

    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 531
    .local v10, "payload":Ljava/lang/String;
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;

    invoke-static {v10, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;

    .line 533
    .local v13, "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    if-nez v13, :cond_0

    .line 534
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to deserialize TournamentStateChange from: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v13    # "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 538
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v13    # "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v2, "string"

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v1, v9, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v12

    .line 540
    .local v12, "resourceId":I
    if-lez v12, :cond_1

    .line 542
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 543
    invoke-virtual {v1, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 546
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->organizer()Ljava/lang/String;

    move-result-object v5

    .line 547
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->tournamentId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p1

    .line 542
    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 548
    :catch_0
    move-exception v7

    .line 549
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load string resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 557
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v8

    .line 558
    .local v8, "ex":Lorg/json/JSONException;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "Failed to parse tournament status change json"

    invoke-static {v1, v2, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 552
    .end local v8    # "ex":Lorg/json/JSONException;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v12    # "resourceId":I
    .restart local v13    # "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 555
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "tournamentStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "body_loc_key null or empty"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private processTournamentTeamInviteNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 606
    const-string v0, "gcm.notification.body_loc_key"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 607
    .local v8, "messageStringId":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing tournament team invite change notification: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :try_start_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 611
    const-string v0, "gcm.notification.body_loc_args"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 612
    .local v2, "messageStringArgs":Ljava/lang/String;
    new-instance v10, Lorg/json/JSONObject;

    const-string/jumbo v0, "xbl"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 614
    .local v10, "xblJson":Lorg/json/JSONObject;
    const-string/jumbo v0, "triggered_notification"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "triggeredInvite"

    .line 615
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "relatedInfo"

    .line 616
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "keywords"

    .line 617
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v1, 0x0

    .line 618
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    .line 619
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v5, v0, v1

    .line 621
    .local v5, "tournamentId":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v1, "string"

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v0, v8, v1, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    .line 623
    .local v9, "resourceId":I
    if-lez v9, :cond_0

    .line 625
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 626
    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string/jumbo v4, "xboxlive"

    move-object v0, p1

    .line 625
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 643
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 631
    .restart local v2    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "tournamentId":Ljava/lang/String;
    .restart local v9    # "resourceId":I
    .restart local v10    # "xblJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v6

    .line 632
    .local v6, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load string resource with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 640
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v6    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v7

    .line 641
    .local v7, "ex":Lorg/json/JSONException;
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v1, "Failed to parse tournament team status change json"

    invoke-static {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 635
    .end local v7    # "ex":Lorg/json/JSONException;
    .restart local v2    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "tournamentId":Ljava/lang/String;
    .restart local v9    # "resourceId":I
    .restart local v10    # "xblJson":Lorg/json/JSONObject;
    :cond_0
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find resource with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 638
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v1, "body_loc_key null or empty"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private processTournamentTeamMembershipChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 11
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "type"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 646
    const-string v0, "gcm.notification.body_loc_key"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 647
    .local v8, "messageStringId":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing tournament team membership change notification: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    :try_start_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 651
    const-string v0, "gcm.notification.body_loc_args"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 652
    .local v2, "messageStringArgs":Ljava/lang/String;
    new-instance v10, Lorg/json/JSONObject;

    const-string/jumbo v0, "xbl"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 654
    .local v10, "xblJson":Lorg/json/JSONObject;
    const-string/jumbo v0, "triggered_notification"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string/jumbo v1, "triggeredMembershipChange"

    .line 655
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "relatedInfo"

    .line 656
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "keywords"

    .line 657
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v1, 0x0

    .line 658
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    .line 659
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v5, v0, v1

    .line 661
    .local v5, "tournamentId":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v1, "string"

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v0, v8, v1, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    .line 663
    .local v9, "resourceId":I
    if-lez v9, :cond_0

    .line 665
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 666
    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "xboxlive"

    move-object v0, p1

    move-object v3, p3

    .line 665
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 683
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 671
    .restart local v2    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "tournamentId":Ljava/lang/String;
    .restart local v9    # "resourceId":I
    .restart local v10    # "xblJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v6

    .line 672
    .local v6, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load string resource with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 680
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v6    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v7

    .line 681
    .local v7, "ex":Lorg/json/JSONException;
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v1, "Failed to parse tournament team status change json"

    invoke-static {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 675
    .end local v7    # "ex":Lorg/json/JSONException;
    .restart local v2    # "messageStringArgs":Ljava/lang/String;
    .restart local v5    # "tournamentId":Ljava/lang/String;
    .restart local v9    # "resourceId":I
    .restart local v10    # "xblJson":Lorg/json/JSONObject;
    :cond_0
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find resource with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 678
    .end local v2    # "messageStringArgs":Ljava/lang/String;
    .end local v5    # "tournamentId":Ljava/lang/String;
    .end local v9    # "resourceId":I
    .end local v10    # "xblJson":Lorg/json/JSONObject;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v1, "body_loc_key null or empty"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private processTournamentTeamStatusChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V
    .locals 15
    .param p1, "disp"    # Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 563
    const-string v1, "gcm.notification.body_loc_key"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 564
    .local v9, "messageStringId":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Processing tournament team status change notification of type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :try_start_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 568
    const-string v1, "gcm.notification.body_loc_args"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 569
    .local v3, "messageStringArgs":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    const-string/jumbo v1, "xbl"

    const-string v2, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v14, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 570
    .local v14, "xblJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournament_team_status_change"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 572
    .local v11, "payloadJson":Lorg/json/JSONObject;
    const-string/jumbo v1, "tournamentTeamStatusChangeNotification"

    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 574
    .local v10, "payload":Ljava/lang/String;
    const-class v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;

    invoke-static {v10, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;

    .line 576
    .local v13, "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    if-nez v13, :cond_0

    .line 577
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to deserialize TeamStateChange from : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v13    # "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 581
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v13    # "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string/jumbo v2, "string"

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v1, v9, v2, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v12

    .line 583
    .local v12, "resourceId":I
    if-lez v12, :cond_1

    .line 585
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 586
    invoke-virtual {v1, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 589
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->organizer()Ljava/lang/String;

    move-result-object v5

    .line 590
    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->tournamentId()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, p1

    .line 585
    invoke-virtual/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->displayTournamentNotification(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 591
    :catch_0
    move-exception v7

    .line 592
    .local v7, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load string resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 600
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v7    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v8

    .line 601
    .local v8, "ex":Lorg/json/JSONException;
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "Failed to parse tournament team status change json"

    invoke-static {v1, v2, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 595
    .end local v8    # "ex":Lorg/json/JSONException;
    .restart local v3    # "messageStringArgs":Ljava/lang/String;
    .restart local v10    # "payload":Ljava/lang/String;
    .restart local v11    # "payloadJson":Lorg/json/JSONObject;
    .restart local v12    # "resourceId":I
    .restart local v13    # "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    .restart local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_1
    :try_start_3
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find resource with ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 598
    .end local v3    # "messageStringArgs":Ljava/lang/String;
    .end local v10    # "payload":Ljava/lang/String;
    .end local v11    # "payloadJson":Lorg/json/JSONObject;
    .end local v12    # "resourceId":I
    .end local v13    # "teamStateChange":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TeamStateChange;
    .end local v14    # "xblJson":Lorg/json/JSONObject;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v2, "body_loc_key null or empty"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v4

    .line 60
    .local v4, "model":Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v8}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v8

    invoke-interface {v8, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;)V

    .line 62
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 63
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/os/Bundle;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_7

    .line 64
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Notification extras: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v0

    .line 66
    .local v0, "disp":Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    const-string/jumbo v8, "type"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v5

    .line 69
    .local v5, "notifyType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    if-nez v5, :cond_3

    .line 71
    const-string v8, "eventType"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "eventType":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->Service:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 74
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processSkypeServiceMessage(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    .line 163
    .end local v0    # "disp":Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .end local v2    # "eventType":Ljava/lang/String;
    .end local v5    # "notifyType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_0
    :goto_0
    return-void

    .line 78
    .restart local v0    # "disp":Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .restart local v2    # "eventType":Ljava/lang/String;
    .restart local v5    # "notifyType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    sget-object v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->ConsumptionHorizon:Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/groupMessaging/SkypeEventType;->getType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 79
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processConsumptionHorizonMessage(Landroid/os/Bundle;)V

    goto :goto_0

    .line 83
    :cond_2
    const-string v8, "rawPayload"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 85
    .local v6, "skypePayloadJSONString":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 87
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 88
    .local v7, "skypePayloadJson":Lorg/json/JSONObject;
    const-string/jumbo v8, "type"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromSkypeType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 96
    .end local v2    # "eventType":Ljava/lang/String;
    .end local v6    # "skypePayloadJSONString":Ljava/lang/String;
    .end local v7    # "skypePayloadJson":Lorg/json/JSONObject;
    :cond_3
    :goto_1
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne v5, v8, :cond_4

    .line 97
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v9, "onReceive - received Chat notification"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processChatNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto :goto_0

    .line 89
    .restart local v2    # "eventType":Ljava/lang/String;
    .restart local v6    # "skypePayloadJSONString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 90
    .local v1, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v9, "Failed to parse skype message notification payload"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 100
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v2    # "eventType":Ljava/lang/String;
    .end local v6    # "skypePayloadJSONString":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->canSLSNotify()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 101
    if-eqz v5, :cond_5

    .line 102
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NotificationType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver$1;->$SwitchMap$com$microsoft$xbox$service$model$gcm$NotificationType:[I

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 152
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown notification type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :pswitch_0
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processFavOnline(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 108
    :pswitch_1
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processFavBroadcast(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 111
    :pswitch_2
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processSkypeMessage(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 114
    :pswitch_3
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processLfgNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 125
    :pswitch_4
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processClubNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 128
    :pswitch_5
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processAchievementNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 131
    :pswitch_6
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processTournamentMatchNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 134
    :pswitch_7
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processTournamentStatusChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 137
    :pswitch_8
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processTournamentTeamStatusChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 140
    :pswitch_9
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processTournamentTeamInviteNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 144
    :pswitch_a
    invoke-direct {p0, v0, v3, v5}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processTournamentTeamMembershipChangeNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    goto/16 :goto_0

    .line 147
    :pswitch_b
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 148
    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->processPartyInvite(Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 155
    :cond_5
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown SLS notification type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "type"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :cond_6
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v9, "Received a SLS push notification, but displaying it is not allowed"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 161
    .end local v0    # "disp":Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;
    .end local v5    # "notifyType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_7
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unparseable notification received:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
