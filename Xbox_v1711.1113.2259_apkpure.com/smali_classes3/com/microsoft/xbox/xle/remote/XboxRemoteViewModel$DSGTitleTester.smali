.class Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;
.super Ljava/lang/Object;
.source "XboxRemoteViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DSGTitleTester"
.end annotation


# static fields
.field private static final DSGTitleTst_TITLE_ID:I = 0x1234abcd


# instance fields
.field private final messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;)V
    .locals 2

    .prologue
    .line 205
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    new-instance v0, Lcom/microsoft/xbox/smartglass/MessageTarget;

    const v1, 0x1234abcd

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    return-void
.end method

.method private startChannel()V
    .locals 3

    .prologue
    .line 210
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    .line 211
    return-void
.end method

.method private stopChannel()V
    .locals 2

    .prologue
    .line 214
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 215
    return-void
.end method
