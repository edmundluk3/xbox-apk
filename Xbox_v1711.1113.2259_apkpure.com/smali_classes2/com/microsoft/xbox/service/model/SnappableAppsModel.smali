.class public Lcom/microsoft/xbox/service/model/SnappableAppsModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "SnappableAppsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;,
        Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private getSnappableAppsRunner:Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;

.field private idSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->idSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->isLoading:Z

    .line 33
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;-><init>(Lcom/microsoft/xbox/service/model/SnappableAppsModel;Lcom/microsoft/xbox/service/model/SnappableAppsModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getSnappableAppsRunner:Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->idSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->appendDefaults(Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;)V

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/SnappableAppsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/SnappableAppsModel$1;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/SnappableAppsModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->clearObservers()V

    return-void
.end method

.method private static appendDefaults(Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "titleIds":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<Ljava/lang/Long;>;"
    .local p1, "ids":Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet<Ljava/lang/String;>;"
    const-wide/32 v0, 0x18ffc9f4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 106
    const-wide/32 v0, 0x3d705025

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 107
    const-wide/32 v0, 0x3d8b930f

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 108
    const-string v0, "9c7e0f20-78fb-4ea7-a8bd-cf9d78059a08"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 109
    const-string v0, "a489d977-8a87-4983-8df6-facea1ad6d93"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 110
    const-string v0, "6D96DEDC-F3C9-43F8-89E3-0C95BF76AD2A"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel$SnappableAppsModelHolder;->access$200()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v0

    return-object v0
.end method

.method public static reset()V
    .locals 0

    .prologue
    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->reset()V

    .line 54
    return-void
.end method


# virtual methods
.method public addSnappableApp(J)V
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public addSnappableApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->idSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->ifNotContainsAdd(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->isLoading:Z

    return v0
.end method

.method public isSnappable(J)Z
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->snappableTitleIdSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSnappable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->idSet:Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public loadSnappableAppsListAsync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 77
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 78
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->DiscoverData:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getSnappableAppsRunner:Lcom/microsoft/xbox/service/model/SnappableAppsModel$LoadSnappableAppRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 79
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Landroid/util/Pair<Ljava/util/ArrayList<Ljava/lang/Long;>;Ljava/util/ArrayList<Ljava/lang/String;>;>;>;"
    const/4 v3, 0x0

    .line 83
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 85
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->isLoading:Z

    .line 86
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 87
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 89
    .local v1, "titleId":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->addSnappableApp(J)V

    goto :goto_1

    .end local v1    # "titleId":Ljava/lang/Long;
    :cond_0
    move v2, v3

    .line 83
    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_2

    .line 94
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 95
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->addSnappableApp(Ljava/lang/String;)V

    goto :goto_2

    .line 98
    .end local v0    # "id":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->lastRefreshTime:Ljava/util/Date;

    .line 102
    :cond_3
    return-void
.end method
