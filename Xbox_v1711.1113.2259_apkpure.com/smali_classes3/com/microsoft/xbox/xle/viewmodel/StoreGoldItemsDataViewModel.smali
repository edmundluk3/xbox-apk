.class public Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "StoreGoldItemsDataViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;
    }
.end annotation


# instance fields
.field private goldItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field

.field private lastItemSelected:I

.field private loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

.field private model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 3
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 30
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 38
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 40
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreModel;->INSTANCE:Lcom/microsoft/xbox/service/model/StoreModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gold:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Popular:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreModel;->getStoreBrowseModel(Lcom/microsoft/xbox/service/model/StoreBrowseType;Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    .line 41
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->onLoadGoldLoungeDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private onLoadGoldLoungeDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->updateAdapter()V

    .line 102
    return-void

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->model:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getGoldLoungeResult()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->goldItems:Ljava/util/List;

    .line 93
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->updateViewModelState()V

    goto :goto_0

    .line 97
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->goldItems:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getGoldLoungeData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->goldItems:Ljava/util/List;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getlastSelectedItemIndex()I
    .locals 2

    .prologue
    .line 181
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->lastItemSelected:I

    .line 182
    .local v0, "index":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->lastItemSelected:I

    .line 183
    return v0
.end method

.method public gotoDetails(Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;I)V
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "index"    # I

    .prologue
    .line 151
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 153
    const/16 v0, 0x15

    .line 154
    .local v0, "goldStoreIndex":I
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 155
    .local v5, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    .line 156
    .local v1, "hm":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    const/16 v6, 0x15

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putStoreFilterPosition(I)V

    .line 159
    const-string v6, "Store"

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 161
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v4

    .line 163
    .local v4, "pageUri":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 164
    .local v2, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v6, p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->bigCatProductId:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setBigCatProductId(Ljava/lang/String;)V

    .line 165
    iget-object v6, p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->MediaItemType:Ljava/lang/String;

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 166
    iget-object v6, p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ID:Ljava/lang/String;

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 168
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 169
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 170
    .local v3, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v6, "MediaId"

    iget-object v7, p1, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->bigCatProductId:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    const-string v6, "ListIndex"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 172
    const-string v6, "MediaType"

    const-string v7, "Gold"

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    const-string v6, "Store - View Store Item"

    invoke-static {v6, v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 174
    invoke-virtual {p0, v2, v5}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 177
    .end local v3    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->lastItemSelected:I

    .line 178
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->cancel()V

    .line 57
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->load(Z)V

    .line 59
    return-void
.end method

.method public onSetActive()V
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    const-string v0, "Store - Gold"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    .line 83
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->onSetActive()V

    .line 84
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;->loadGoldLoungeDataTask:Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel$LoadGoldLoungeDataAsyncTask;->cancel()V

    .line 76
    :cond_0
    return-void
.end method
