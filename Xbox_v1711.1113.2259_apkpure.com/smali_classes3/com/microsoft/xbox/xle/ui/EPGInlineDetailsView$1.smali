.class Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;
.super Ljava/lang/Object;
.source "EPGInlineDetailsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

.field final synthetic val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

.field final synthetic val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

.field final synthetic val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 197
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "ShowDetails"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getChannelGuid()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 198
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->showDetailsView(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V

    .line 201
    return-void

    .line 198
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
