.class abstract Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;
.super Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
.source "$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite.java"


# instance fields
.field private final continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

.field private final gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

.field private final invitedXuid:Ljava/lang/String;

.field private final sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private final teamId:Ljava/lang/String;

.field private final teamName:Ljava/lang/String;

.field private final titleId:J

.field private final titleImageUri:Ljava/lang/String;

.field private final titleName:Ljava/lang/String;

.field private final tournamentImageUri:Ljava/lang/String;

.field private final tournamentName:Ljava/lang/String;

.field private final tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;)V
    .locals 2
    .param p1, "sessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .param p2, "tournamentRef"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .param p3, "invitedXuid"    # Ljava/lang/String;
    .param p4, "titleId"    # J
    .param p6, "titleName"    # Ljava/lang/String;
    .param p7, "titleImageUri"    # Ljava/lang/String;
    .param p8, "teamName"    # Ljava/lang/String;
    .param p9, "teamId"    # Ljava/lang/String;
    .param p10, "tournamentName"    # Ljava/lang/String;
    .param p11, "tournamentImageUri"    # Ljava/lang/String;
    .param p12, "gameTypes"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    .param p13, "continuationUris"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;-><init>()V

    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null sessionRef"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 41
    if-nez p2, :cond_1

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentRef"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 45
    if-nez p3, :cond_2

    .line 46
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null invitedXuid"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->invitedXuid:Ljava/lang/String;

    .line 49
    iput-wide p4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    .line 50
    if-nez p6, :cond_3

    .line 51
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null titleName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_3
    iput-object p6, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleName:Ljava/lang/String;

    .line 54
    if-nez p7, :cond_4

    .line 55
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null titleImageUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_4
    iput-object p7, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleImageUri:Ljava/lang/String;

    .line 58
    if-nez p8, :cond_5

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null teamName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_5
    iput-object p8, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamName:Ljava/lang/String;

    .line 62
    if-nez p9, :cond_6

    .line 63
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null teamId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_6
    iput-object p9, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamId:Ljava/lang/String;

    .line 66
    if-nez p10, :cond_7

    .line 67
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_7
    iput-object p10, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentName:Ljava/lang/String;

    .line 70
    if-nez p11, :cond_8

    .line 71
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentImageUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_8
    iput-object p11, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentImageUri:Ljava/lang/String;

    .line 74
    if-nez p12, :cond_9

    .line 75
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null gameTypes"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_9
    iput-object p12, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 78
    if-nez p13, :cond_a

    .line 79
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null continuationUris"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_a
    iput-object p13, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 82
    return-void
.end method


# virtual methods
.method public continuationUris()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 175
    if-ne p1, p0, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 178
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 179
    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    .line 180
    .local v0, "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 181
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->invitedXuid:Ljava/lang/String;

    .line 182
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->invitedXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    .line 183
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleName:Ljava/lang/String;

    .line 184
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleImageUri:Ljava/lang/String;

    .line 185
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleImageUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamName:Ljava/lang/String;

    .line 186
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->teamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamId:Ljava/lang/String;

    .line 187
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->teamId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentName:Ljava/lang/String;

    .line 188
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentImageUri:Ljava/lang/String;

    .line 189
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentImageUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 190
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->gameTypes()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 191
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->continuationUris()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    :cond_3
    move v1, v2

    .line 193
    goto/16 :goto_0
.end method

.method public gameTypes()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 198
    const/4 v0, 0x1

    .line 199
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 201
    mul-int/2addr v0, v8

    .line 202
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 203
    mul-int/2addr v0, v8

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->invitedXuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 205
    mul-int/2addr v0, v8

    .line 206
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 207
    mul-int/2addr v0, v8

    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 209
    mul-int/2addr v0, v8

    .line 210
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleImageUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 211
    mul-int/2addr v0, v8

    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 213
    mul-int/2addr v0, v8

    .line 214
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 215
    mul-int/2addr v0, v8

    .line 216
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 217
    mul-int/2addr v0, v8

    .line 218
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentImageUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 219
    mul-int/2addr v0, v8

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 221
    mul-int/2addr v0, v8

    .line 222
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 223
    return v0
.end method

.method public invitedXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->invitedXuid:Ljava/lang/String;

    return-object v0
.end method

.method public sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    return-object v0
.end method

.method public teamId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamId:Ljava/lang/String;

    return-object v0
.end method

.method public teamName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamName:Ljava/lang/String;

    return-object v0
.end method

.method public titleId()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    return-wide v0
.end method

.method public titleImageUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public titleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TournamentGameInvite{sessionRef="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentRef="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invitedXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->invitedXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleImageUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->titleImageUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", teamName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", teamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->teamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentImageUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentImageUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gameTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->gameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", continuationUris="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->continuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tournamentImageUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public tournamentName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentName:Ljava/lang/String;

    return-object v0
.end method

.method public tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    return-object v0
.end method
