.class Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;
.super Ljava/lang/Object;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageModelHolder"
.end annotation


# static fields
.field private static INSTANCE:Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 166
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;-><init>(Lcom/microsoft/xbox/service/model/MessageModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->INSTANCE:Lcom/microsoft/xbox/service/model/MessageModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/microsoft/xbox/service/model/MessageModel;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->INSTANCE:Lcom/microsoft/xbox/service/model/MessageModel;

    return-object v0
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 164
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->reset()V

    return-void
.end method

.method private static reset()V
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 170
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;-><init>(Lcom/microsoft/xbox/service/model/MessageModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/MessageModel$MessageModelHolder;->INSTANCE:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 171
    return-void
.end method
