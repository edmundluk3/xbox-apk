.class public abstract Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.source "EDSV2MediaListBrowseModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ParentT:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "ChildT:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "RelatedT:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<TParentT;TRelatedT;>;"
    }
.end annotation


# instance fields
.field protected browseListData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TChildT;>;"
        }
    .end annotation
.end field

.field private browseRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
            "<TParentT;TChildT;TRelatedT;>.BrowseEDSV2MediaItem",
            "ListRunner;"
        }
    .end annotation
.end field

.field protected lastRefreshChildTime:Ljava/util/Date;

.field protected loadingChildStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->browseRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadingChildStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 52
    return-void
.end method


# virtual methods
.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getDesiredMediaItemType()I
.end method

.method protected getDesiredMediaItemTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getDesiredMediaItemType()I

    move-result v0

    if-lez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getDesiredMediaItemType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    .line 94
    :cond_0
    const-string/jumbo v0, "you didn\'t specific desired media type, either override getDesiredMediaItemType or getDesiredMEdiaItemTypeString"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 95
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingChild()Z
    .locals 1

    .prologue
    .line 117
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadingChildStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 108
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->UnknownLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method protected getMaxCount()I
    .locals 1

    .prologue
    .line 103
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    const v0, 0x7fffffff

    return v0
.end method

.method public getMediaItemListData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TChildT;>;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->browseListData:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_MOSTPOPULAR:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method public getParentImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TParentT;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method public getParentMediaType()I
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getPartnerMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 64
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Standard:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 141
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    return-void
.end method

.method public loadListDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TChildT;>;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    const/4 v6, 0x0

    .line 126
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 127
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 129
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 131
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    .line 132
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lastRefreshChildTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadingChildStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->browseRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 134
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->browseRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel$BrowseEDSV2MediaItemListRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method protected onBrowseMediaItemListCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TChildT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TChildT;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_0

    .line 145
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lastRefreshChildTime:Ljava/util/Date;

    .line 146
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 148
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TChildT;>;"
    if-eqz v0, :cond_0

    .line 151
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "partnerMediaId":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->updateWithNewData(Ljava/util/ArrayList;)V

    .line 153
    invoke-static {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->updateModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;Ljava/lang/String;)V

    .line 156
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TChildT;>;"
    .end local v1    # "partnerMediaId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public shouldRefreshChild()Z
    .locals 4

    .prologue
    .line 121
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lastRefreshChildTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method protected updateWithNewData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TChildT;>;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel<TParentT;TChildT;TRelatedT;>;"
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TChildT;>;"
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->lastRefreshTime:Ljava/util/Date;

    .line 160
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->browseListData:Ljava/util/ArrayList;

    .line 161
    return-void
.end method
