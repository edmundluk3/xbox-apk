.class public Lcom/microsoft/xbox/service/model/sls/SendMessageWithAttachmentRequest;
.super Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;
.source "SendMessageWithAttachmentRequest.java"


# instance fields
.field public activityFeedItemLocator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "messageText"    # Ljava/lang/String;
    .param p3, "activityFeedItemLocator"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "recipients":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 15
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageWithAttachmentRequest;->activityFeedItemLocator:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public static getSendMessageWithAttachementRequestBody(Lcom/microsoft/xbox/service/model/sls/SendMessageWithAttachmentRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "sendMessageWithAttachementRequest"    # Lcom/microsoft/xbox/service/model/sls/SendMessageWithAttachmentRequest;

    .prologue
    .line 20
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 24
    :goto_0
    return-object v1

    .line 21
    :catch_0
    move-exception v0

    .line 22
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v1, "SendMessageRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const/4 v1, 0x0

    goto :goto_0
.end method
