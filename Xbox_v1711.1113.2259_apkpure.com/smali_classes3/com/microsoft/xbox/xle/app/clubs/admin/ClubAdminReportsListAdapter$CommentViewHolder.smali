.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "CommentViewHolder"
.end annotation


# instance fields
.field private final content:Landroid/widget/TextView;

.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamertag:Landroid/widget/TextView;

.field private final realname:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .line 305
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    .line 307
    const v0, 0x7f0e0284

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 308
    const v0, 0x7f0e0285

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->gamertag:Landroid/widget/TextView;

    .line 309
    const v0, 0x7f0e0286

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->realname:Landroid/widget/TextView;

    .line 310
    const v0, 0x7f0e0287

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->content:Landroid/widget/TextView;

    .line 311
    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;
    .param p1, "commentListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;
    .param p1, "commentListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V

    return-void
.end method

.method static synthetic lambda$onBind$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;
    .param p1, "commentListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 334
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->removeDecorator(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected getInnerViewId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 316
    const v0, 0x7f03005c

    return v0
.end method

.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v3, 0x7f0201fa

    .line 321
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    move-object v0, p1

    .line 323
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .line 325
    .local v0, "commentListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->hasValidContent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->hideErrorView()V

    .line 328
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 329
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->gamertag:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->realname:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->getRealName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 331
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->content:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->delete:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->showErrorView()V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 297
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$CommentViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method
