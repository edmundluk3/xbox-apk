.class final Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;
.super Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.source "$AutoValue_WelcomeCardCompletionStates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private clubWelcomeCardCompleted:Ljava/lang/Boolean;

.field private facebookWelcomeCardCompleted:Ljava/lang/Boolean;

.field private redeemWelcomeCardCompleted:Ljava/lang/Boolean;

.field private setupWelcomeCardCompleted:Ljava/lang/Boolean;

.field private shopWelcomeCardCompleted:Ljava/lang/Boolean;

.field private suggestedFriendsWelcomeCardCompleted:Ljava/lang/Boolean;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;-><init>()V

    .line 128
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;-><init>()V

    .line 130
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->setupWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->redeemWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 132
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->shopWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 133
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->facebookWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 134
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->clubWelcomeCardCompleted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 136
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 8

    .prologue
    .line 169
    const-string v7, ""

    .line 170
    .local v7, "missing":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " setupWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " redeemWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shopWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " facebookWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " suggestedFriendsWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 185
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " clubWelcomeCardCompleted"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 188
    :cond_5
    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing required properties:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_6
    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 192
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 193
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 194
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 195
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 196
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 197
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates;-><init>(ZZZZZZ)V

    .line 191
    return-object v0
.end method

.method public clubWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "clubWelcomeCardCompleted"    # Z

    .prologue
    .line 164
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 165
    return-object p0
.end method

.method public facebookWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "facebookWelcomeCardCompleted"    # Z

    .prologue
    .line 154
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 155
    return-object p0
.end method

.method public redeemWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "redeemWelcomeCardCompleted"    # Z

    .prologue
    .line 144
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 145
    return-object p0
.end method

.method public setupWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "setupWelcomeCardCompleted"    # Z

    .prologue
    .line 139
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 140
    return-object p0
.end method

.method public shopWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "shopWelcomeCardCompleted"    # Z

    .prologue
    .line 149
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 150
    return-object p0
.end method

.method public suggestedFriendsWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1
    .param p1, "suggestedFriendsWelcomeCardCompleted"    # Z

    .prologue
    .line 159
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted:Ljava/lang/Boolean;

    .line 160
    return-object p0
.end method
