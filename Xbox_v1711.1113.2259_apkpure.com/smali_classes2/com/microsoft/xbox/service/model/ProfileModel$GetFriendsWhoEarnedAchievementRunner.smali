.class Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetFriendsWhoEarnedAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final NinetyDaysMilliseconds:J

.field private achievementId:I

.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private scid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p2, "profileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "scid"    # Ljava/lang/String;
    .param p5, "achievementId"    # I

    .prologue
    .line 3845
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3839
    const-wide v0, 0x1cf7c5800L

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->NinetyDaysMilliseconds:J

    .line 3846
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3847
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->xuid:Ljava/lang/String;

    .line 3848
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->scid:Ljava/lang/String;

    .line 3849
    iput p5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->achievementId:I

    .line 3850
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3854
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string/jumbo v0, "yyyy-MM-dd\'T\'hh:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 3855
    .local v6, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 3857
    new-instance v7, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x1cf7c5800L

    sub-long/2addr v0, v2

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 3858
    .local v7, "startDate":Ljava/util/Date;
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 3859
    .local v5, "formattedEndDate":Ljava/lang/String;
    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 3861
    .local v4, "formattedStartDate":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->scid:Ljava/lang/String;

    iget v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->achievementId:I

    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFriendsWhoEarnedAchievementInfo(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3837
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3875
    const-wide/16 v0, 0xbdb

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3870
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFriendsWhoEarnedAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$4500(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 3871
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3866
    return-void
.end method
