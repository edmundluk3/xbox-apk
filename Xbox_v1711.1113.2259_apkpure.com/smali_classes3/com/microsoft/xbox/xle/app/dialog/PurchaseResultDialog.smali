.class public Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PurchaseResultDialog.java"


# instance fields
.field private bodyTextView:Landroid/widget/TextView;

.field private doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private titleTextView:Landroid/widget/TextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    const/4 v4, 0x0

    .line 24
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 25
    const v0, 0x7f0301de

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->setContentView(I)V

    .line 26
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->setCanceledOnTouchOutside(Z)V

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .line 29
    const v0, 0x7f0e0991

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->titleTextView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0e0992

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->bodyTextView:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f0e0993

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->titleTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b04

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->getPurchaseTitleText()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->bodyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->getPurchaseDiaglogBodyText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->logBIForMissingHomeConsole()V

    .line 38
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->onDonePressed()V

    return-void
.end method

.method private onDonePressed()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissPurchaseResultDialog()V

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->exit()V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->onDonePressed()V

    .line 51
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    :cond_0
    return-void
.end method
