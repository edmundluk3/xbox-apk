.class public final enum Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;
.super Ljava/lang/Enum;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnforcementConversationDataMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

.field public static final enum Club:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

.field public static final enum Group:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

.field public static final enum OneToOne:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 459
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    const-string v1, "OneToOne"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->OneToOne:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    const-string v1, "Group"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->Group:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    const-string v1, "Club"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->Club:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    .line 458
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->OneToOne:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->Group:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->Club:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 458
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 458
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;
    .locals 1

    .prologue
    .line 458
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    return-object v0
.end method
