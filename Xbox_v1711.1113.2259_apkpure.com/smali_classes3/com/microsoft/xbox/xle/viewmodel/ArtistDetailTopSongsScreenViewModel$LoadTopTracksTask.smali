.class Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ArtistDetailTopSongsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTopTracksTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$1;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->shouldRefreshTrack()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->loadTopTracks(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 113
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 124
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 103
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;Z)Z

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel$LoadTopTracksTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;->updateAdapter()V

    .line 119
    return-void
.end method
