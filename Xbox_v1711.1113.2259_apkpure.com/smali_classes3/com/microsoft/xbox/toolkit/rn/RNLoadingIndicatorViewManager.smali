.class final Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source "RNLoadingIndicatorViewManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;",
        ">;"
    }
.end annotation


# static fields
.field private static NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "RCTLoadingIndicator"

    sput-object v0, Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;->NAME:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createViewInstance(Lcom/facebook/react/uimanager/ThemedReactContext;)Landroid/view/View;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;->createViewInstance(Lcom/facebook/react/uimanager/ThemedReactContext;)Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;

    move-result-object v0

    return-object v0
.end method

.method protected createViewInstance(Lcom/facebook/react/uimanager/ThemedReactContext;)Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;
    .locals 1
    .param p1, "reactContext"    # Lcom/facebook/react/uimanager/ThemedReactContext;

    .prologue
    .line 17
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLELoadingIndicator;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/toolkit/rn/RNLoadingIndicatorViewManager;->NAME:Ljava/lang/String;

    return-object v0
.end method
