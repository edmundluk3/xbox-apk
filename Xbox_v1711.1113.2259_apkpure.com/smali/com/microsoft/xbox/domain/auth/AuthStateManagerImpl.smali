.class public final Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;
.super Ljava/lang/Object;
.source "AuthStateManagerImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/auth/AuthStateManager;
.implements Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final DNET_SCOPE:Ljava/lang/String; = "user.auth.dnet.xboxlive.com"

.field private static final POLICY:Ljava/lang/String; = "mbi_ssl"

.field private static final PROD_SCOPE:Ljava/lang/String; = "user.auth.xboxlive.com"

.field private static final RC_SIGN_IN:I = 0x2b

.field private static final RC_SIGN_OUT:I = 0x2f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final authActions:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;",
            ">;"
        }
    .end annotation
.end field

.field private final authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 3
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->Unknown:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/BiPredicate;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 65
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->onAuthAction(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;)V

    return-void
.end method

.method private doSignInSilentlyXsapi()V
    .locals 2

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v0, v1, :cond_0

    .line 146
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->TAG:Ljava/lang/String;

    const-string v1, "doSignInSilentlyXsapi: Already signed in"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 152
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getUser()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$1;-><init>(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->signInSilently(Lcom/microsoft/xbox/idp/interop/XsapiUser$SignInSilentlyCallback;)V

    goto :goto_0
.end method

.method private doSignInWithUi()V
    .locals 6

    .prologue
    const/16 v5, 0x2b

    .line 220
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->isSignedIn()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 221
    sget-object v3, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->TAG:Ljava/lang/String;

    const-string v4, "doSignInWithUi: Ignoring. Already signed in"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 227
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 229
    iget-object v3, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v4, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v3, v4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 231
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->addOnActivityResultCallback(Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;)V

    .line 233
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getUser()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->isProd()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v2, "user.auth.xboxlive.com"

    .line 234
    .local v2, "scope":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getWelcomeAltButtonText()Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "altText":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 237
    const-string v3, "mbi_ssl"

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/idp/common/AccountPicker;->newSignInIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 233
    .end local v1    # "altText":Ljava/lang/String;
    .end local v2    # "scope":Ljava/lang/String;
    :cond_2
    const-string/jumbo v2, "user.auth.dnet.xboxlive.com"

    goto :goto_1

    .line 239
    .restart local v1    # "altText":Ljava/lang/String;
    .restart local v2    # "scope":Ljava/lang/String;
    :cond_3
    const-string v3, "mbi_ssl"

    invoke-static {v0, v2, v3, v1}, Lcom/microsoft/xbox/idp/common/AccountPicker;->newSignInIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private doSignOutWithUi()V
    .locals 3

    .prologue
    .line 209
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 210
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 213
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->resetRightPaneData()V

    .line 214
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->addOnActivityResultCallback(Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;)V

    .line 215
    invoke-static {v0}, Lcom/microsoft/xbox/idp/common/AccountPicker;->newSignOutIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 217
    :cond_0
    return-void
.end method

.method private doSignOutXsapi()V
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v0, v1, :cond_0

    .line 180
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->TAG:Ljava/lang/String;

    const-string v1, "doSignOutXsapi: Already signed out."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;-><init>(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->signOut(Lcom/microsoft/xbox/idp/interop/XsapiUser$SignOutCallback;)V

    goto :goto_0
.end method

.method private getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    return-object v0
.end method

.method private getUser()Lcom/microsoft/xbox/idp/interop/XsapiUser;
    .locals 1

    .prologue
    .line 280
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$0(Landroid/util/Pair;)Z
    .locals 1
    .param p0, "pair"    # Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/auth/AuthState;->isTerminal()Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$new$1(Landroid/util/Pair;)Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;
    .locals 1
    .param p0, "pair"    # Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    return-object v0
.end method

.method private onAuthAction(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;)V
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    .prologue
    .line 125
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$3;->$SwitchMap$com$microsoft$xbox$domain$auth$AuthStateManagerImpl$AuthAction:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 142
    :goto_0
    return-void

    .line 127
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignInSilentlyXsapi()V

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignOutXsapi()V

    goto :goto_0

    .line 135
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignOutWithUi()V

    goto :goto_0

    .line 139
    :pswitch_3
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignInWithUi()V

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAuthStates()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/auth/AuthState;->isTerminal()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNewUser()Z
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getUser()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->isNewUser()Z

    move-result v0

    return v0
.end method

.method public isSignedIn()Z
    .locals 3

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getUser()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->isSignedIn()Z

    move-result v0

    .line 76
    .local v0, "isSignedIn":Z
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 78
    return v0

    .line 76
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSignedOut()Z
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSigningIn()Z
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSigningOut()Z
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->getAuthState()Lcom/microsoft/xbox/domain/auth/AuthState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutInProgress:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 246
    const/16 v1, 0x2f

    if-ne p1, v1, :cond_1

    .line 247
    packed-switch p2, :pswitch_data_0

    .line 253
    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 257
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 259
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->removeOnActivityResultCallback(Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;)V

    .line 273
    .end local v0    # "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :cond_0
    :goto_1
    return-void

    .line 249
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 262
    :cond_1
    const/16 v1, 0x2b

    if-ne p1, v1, :cond_0

    .line 263
    packed-switch p2, :pswitch_data_1

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInError:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    .line 265
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    .line 247
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    .line 263
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch
.end method

.method public signInSilentlyXsapi()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInSilentlyXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 117
    return-void
.end method

.method public signInWithUi()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignInWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method public signOutWithUi()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutWithUi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method public signOutXsapi()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->authActions:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;->SignOutXsapi:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 122
    return-void
.end method
