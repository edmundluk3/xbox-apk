.class public Lcom/microsoft/xbox/xle/ui/IconFontRingButton_ViewBinding;
.super Ljava/lang/Object;
.source "IconFontRingButton_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/IconFontRingButton;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/xle/ui/IconFontRingButton;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton_ViewBinding;-><init>(Lcom/microsoft/xbox/xle/ui/IconFontRingButton;Landroid/view/View;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/IconFontRingButton;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/ui/IconFontRingButton;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    .line 26
    const v0, 0x7f0e070a

    const-string v1, "field \'iconTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->iconTextView:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f0e070b

    const-string v1, "field \'countTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countTextView:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f0e070c

    const-string v1, "field \'textView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0e070d

    const-string v1, "field \'subTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subTextView:Landroid/widget/TextView;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    .line 36
    .local v0, "target":Lcom/microsoft/xbox/xle/ui/IconFontRingButton;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 37
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton_ViewBinding;->target:Lcom/microsoft/xbox/xle/ui/IconFontRingButton;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->iconTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countTextView:Landroid/widget/TextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subTextView:Landroid/widget/TextView;

    .line 43
    return-void
.end method
