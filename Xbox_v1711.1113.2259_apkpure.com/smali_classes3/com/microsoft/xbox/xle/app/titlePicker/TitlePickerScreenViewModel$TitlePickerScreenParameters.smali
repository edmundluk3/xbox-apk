.class public final Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "TitlePickerScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitlePickerScreenParameters"
.end annotation


# instance fields
.field private final maxSelectable:I

.field private final preSelectedTitles:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "maxSelectable"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;I",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "preSelectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    .local p3, "selectionCompletedAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 188
    const-wide/16 v0, 0x0

    int-to-long v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 189
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->preSelectedTitles:Ljava/util/List;

    .line 190
    iput p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->maxSelectable:I

    .line 191
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->preSelectedTitles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->selectionCompletedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    .prologue
    .line 175
    iget v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;->maxSelectable:I

    return v0
.end method
