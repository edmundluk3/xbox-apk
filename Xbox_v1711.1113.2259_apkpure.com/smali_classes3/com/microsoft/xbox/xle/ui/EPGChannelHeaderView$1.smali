.class Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;
.super Ljava/lang/Object;
.source "EPGChannelHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->isFavorite()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "ChannelUnfavorited"

    .line 110
    .local v0, "actionType":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;

    if-eqz v1, :cond_2

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->getProviderId()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getChannelGuid()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideAppPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->toggleFavorite()V

    .line 118
    .end local v0    # "actionType":Ljava/lang/String;
    :cond_0
    return-void

    .line 108
    :cond_1
    const-string v0, "ChannelFavorited"

    goto :goto_0

    .line 113
    .restart local v0    # "actionType":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;->access$000(Lcom/microsoft/xbox/xle/ui/EPGChannelHeaderView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getChannelGuid()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
