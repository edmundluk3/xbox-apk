.class public Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;
.source "TrendingTopicPagerAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private topics:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;Landroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
    .param p2, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 29
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getTopicData()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;-><init>(ILandroid/support/v4/view/ViewPager;)V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getTopicData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->setViewPagerVisibility()V

    .line 33
    return-void
.end method

.method static synthetic lambda$instantiateItemInternal$0(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;ILandroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->navigateToTopic(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V

    return-void
.end method

.method private setViewPagerVisibility()V
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 72
    return-void

    .line 71
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private shouldUpdateView()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 46
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getTopicData()Ljava/util/ArrayList;

    move-result-object v2

    .line 48
    .local v2, "newTopics":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    move v4, v5

    .line 67
    :cond_0
    :goto_0
    return v4

    .line 52
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 56
    new-instance v0, Ljava/util/HashSet;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v0, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 57
    .local v0, "currentSourceIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v1, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 59
    .local v1, "newSourceIds":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .line 60
    .local v3, "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->sourceId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 63
    .end local v3    # "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .line 64
    .restart local v3    # "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->sourceId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 67
    .end local v3    # "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    :cond_3
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public instantiateItemInternal(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8
    .param p1, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 76
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 77
    if-ltz p2, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge p2, v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 80
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03023a

    invoke-virtual {v2, v6, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 81
    .local v5, "trendingView":Landroid/view/View;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v6, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .line 83
    .local v4, "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    const v6, 0x7f0e0aee

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 84
    .local v1, "displayImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v6, 0x7f0e0af0

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 85
    .local v3, "name":Landroid/widget/TextView;
    const v6, 0x7f0e0af1

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    .local v0, "caption":Landroid/widget/TextView;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->displayImage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 88
    const v6, 0x7f0e0aef

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 90
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->caption()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 93
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;I)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 96
    return-object v5

    .end local v0    # "caption":Landroid/widget/TextView;
    .end local v1    # "displayImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "name":Landroid/widget/TextView;
    .end local v4    # "topic":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    .end local v5    # "trendingView":Landroid/view/View;
    :cond_0
    move v6, v7

    .line 77
    goto :goto_0
.end method

.method public updateView()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->shouldUpdateView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getTopicData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->topics:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->setCount(I)V

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->notifyDataSetChanged()V

    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->setViewPagerVisibility()V

    .line 43
    :cond_0
    return-void
.end method
