.class public abstract Lcom/microsoft/xbox/xle/app/activity/PopupScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PopupScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;


# instance fields
.field private created:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->created:Z

    return-void
.end method


# virtual methods
.method public isCreated()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->created:Z

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getIsStarted()Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->created:Z

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->created:Z

    .line 30
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 31
    return-void
.end method
