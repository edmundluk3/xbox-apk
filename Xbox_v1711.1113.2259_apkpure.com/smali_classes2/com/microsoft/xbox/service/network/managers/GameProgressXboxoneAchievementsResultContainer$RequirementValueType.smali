.class public final enum Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
.super Ljava/lang/Enum;
.source "GameProgressXboxoneAchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequirementValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field public static final enum DATETIME:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field public static final enum DOUBLE:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field public static final enum GUID:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field public static final enum INTEGER:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field private static final LOOKUP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum STRING:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 49
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "INTEGER"

    invoke-direct {v1, v2, v4}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->INTEGER:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "GUID"

    invoke-direct {v1, v2, v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->GUID:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "STRING"

    invoke-direct {v1, v2, v6}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->STRING:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "DATETIME"

    invoke-direct {v1, v2, v7}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->DATETIME:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "DOUBLE"

    invoke-direct {v1, v2, v8}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->DOUBLE:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    .line 48
    const/4 v1, 0x6

    new-array v1, v1, [Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->INTEGER:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->GUID:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v2, v1, v5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->STRING:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v2, v1, v6

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->DATETIME:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v2, v1, v7

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->DOUBLE:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    aput-object v3, v1, v2

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    .line 54
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->LOOKUP:Ljava/util/Map;

    .line 56
    const-class v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "type":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "type":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    .line 57
    .restart local v0    # "type":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->LOOKUP:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueFor(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 62
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 64
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->LOOKUP:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    .line 65
    .local v0, "type":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    .end local v0    # "type":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$RequirementValueType;

    return-object v0
.end method
