.class Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;
.super Ljava/lang/Object;
.source "ConsoleBarView.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimTabhostListener"
.end annotation


# static fields
.field private static final ALPHA_DELAY:I = 0x64

.field private static final ALPHA_TIME:I = 0x14d

.field private static final ANIMATION_TIME:I = 0x341


# instance fields
.field private currentTab:I

.field private currentView:Landroid/view/View;

.field private previousView:Landroid/view/View;

.field private tabHost:Landroid/widget/TabHost;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Landroid/widget/TabHost;)V
    .locals 1
    .param p2, "tabHost"    # Landroid/widget/TabHost;

    .prologue
    .line 767
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 768
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->tabHost:Landroid/widget/TabHost;

    .line 769
    invoke-virtual {p2}, Landroid/widget/TabHost;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    .line 770
    return-void
.end method

.method private getAlphaAnimation(Z)Landroid/view/animation/Animation;
    .locals 6
    .param p1, "out"    # Z

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 887
    if-eqz p1, :cond_0

    move v2, v3

    .line 888
    .local v2, "start":F
    :goto_0
    if-eqz p1, :cond_1

    .line 890
    .local v1, "end":F
    :goto_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 891
    .local v0, "alpha":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 892
    const-wide/16 v4, 0x14d

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 893
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 895
    return-object v0

    .end local v0    # "alpha":Landroid/view/animation/Animation;
    .end local v1    # "end":F
    .end local v2    # "start":F
    :cond_0
    move v2, v1

    .line 887
    goto :goto_0

    .restart local v2    # "start":F
    :cond_1
    move v1, v3

    .line 888
    goto :goto_1
.end method

.method private inFromLeftAnimation()Landroid/view/animation/Animation;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 850
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 851
    .local v9, "anim":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, -0x40800000    # -1.0f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 853
    .local v0, "inFromLeft":Landroid/view/animation/Animation;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->setProperties(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 854
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 855
    invoke-direct {p0, v10}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->getAlphaAnimation(Z)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 856
    return-object v9
.end method

.method private inFromRightAnimation()Landroid/view/animation/Animation;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 819
    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 820
    .local v9, "anim":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 822
    .local v0, "inFromRight":Landroid/view/animation/Animation;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->setProperties(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 823
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 824
    invoke-direct {p0, v10}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->getAlphaAnimation(Z)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 825
    return-object v9
.end method

.method private outToLeftAnimation()Landroid/view/animation/Animation;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 865
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v9, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 866
    .local v9, "anim":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, -0x40800000    # -1.0f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 868
    .local v0, "outtoLeft":Landroid/view/animation/Animation;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->setProperties(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 869
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 870
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->getAlphaAnimation(Z)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 871
    return-object v9
.end method

.method private outToRightAnimation()Landroid/view/animation/Animation;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 835
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v9, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 836
    .local v9, "anim":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 838
    .local v0, "outToRight":Landroid/view/animation/Animation;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->setProperties(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 839
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 840
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->getAlphaAnimation(Z)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 841
    return-object v9
.end method

.method private setProperties(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 881
    const-wide/16 v0, 0x341

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 882
    new-instance v0, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    const/high16 v1, 0x40e00000    # 7.0f

    sget-object v2, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseOut:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 883
    return-object p1
.end method


# virtual methods
.method public onTabChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    .line 775
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1300(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 776
    const/4 v0, 0x0

    .line 778
    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 779
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;->access$200(Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabInfo;)Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 780
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1502(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;I)I

    .line 784
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$1400(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 786
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->updateNowPlayingBarContent()V

    .line 788
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    .line 789
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentTab:I

    if-le v1, v2, :cond_5

    .line 790
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 791
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->outToLeftAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 794
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 795
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->inFromRightAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 806
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    .line 807
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentTab:I

    .line 811
    .end local v0    # "i":I
    :cond_3
    return-void

    .line 778
    .restart local v0    # "i":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 798
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 799
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->previousView:Landroid/view/View;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->outToRightAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 802
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 803
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->currentView:Landroid/view/View;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$AnimTabhostListener;->inFromLeftAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method
