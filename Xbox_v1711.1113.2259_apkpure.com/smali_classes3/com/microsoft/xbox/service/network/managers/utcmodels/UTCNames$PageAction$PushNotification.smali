.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$PushNotification;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PushNotification"
.end annotation


# static fields
.field public static final Achievement:Ljava/lang/String; = "Push Achievement"

.field public static final Broadcast:Ljava/lang/String; = "Push Broadcast"

.field public static final BroadcastInternal:Ljava/lang/String; = "Push Broadcast In-App"

.field public static final ClubChatManyMessages:Ljava/lang/String; = "Push Notification - Club Chat Many Messages"

.field public static final ClubChatMention:Ljava/lang/String; = "Push Notification - Club Chat Mention"

.field public static final ClubChatMessage:Ljava/lang/String; = "Push Notification - Club Chat Message"

.field public static final ClubsInvitationRequest:Ljava/lang/String; = "Push Notification - Clubs Invitation Request"

.field public static final ClubsInvite:Ljava/lang/String; = "Push Notification - Clubs Invite"

.field public static final ClubsJoined:Ljava/lang/String; = "Push Notification - Clubs Joined"

.field public static final ClubsJoinedOwned:Ljava/lang/String; = "Push Notification - Clubs Joined Owned"

.field public static final ClubsModeratorLeft:Ljava/lang/String; = "Push Notification - Clubs Moderator Left"

.field public static final ClubsPromoted:Ljava/lang/String; = "Push Notification - Clubs Promoted"

.field public static final ClubsRecommendations:Ljava/lang/String; = "Push Notification - Clubs Reccommendations"

.field public static final ClubsReports:Ljava/lang/String; = "Push Notification - Clubs Reports"

.field public static final ClubsTransfer:Ljava/lang/String; = "Push Notification - Clubs Transfer"

.field public static final LFGCancelled:Ljava/lang/String; = "Push Notification - LFG Scheduled Canceled"

.field public static final LFGExpired:Ljava/lang/String; = "Push Notification - LFG Scheduled Expired"

.field public static final LFGImminentAccepted:Ljava/lang/String; = "Push Notification - LFG Imminent Accepted"

.field public static final LFGImminentClosed:Ljava/lang/String; = "Push Notification - LFG Imminent Closed"

.field public static final LFGInterested:Ljava/lang/String; = "Push Notification - LFG Player Interested"

.field public static final LFGPartyInvite:Ljava/lang/String; = "PushNotification - LFG party invite"

.field public static final LFGScheduledAccepted:Ljava/lang/String; = "Push Notification - LFG Scheduled Accepted"

.field public static final LFGScheduledFull:Ljava/lang/String; = "Push Notification - LFG Scheduled Full"

.field public static final LFGWithdrawn:Ljava/lang/String; = "Push Notification - LFG Player Withdrawn"

.field public static final MemberBroadcastingAudio:Ljava/lang/String; = "PushNotification - User is broadcasting audio"

.field public static final MemberJoinedParty:Ljava/lang/String; = "PushNotification - User joined the party"

.field public static final MemberLeftParty:Ljava/lang/String; = "PushNotification - User left the party"

.field public static final MemberRemovedFromParty:Ljava/lang/String; = "PushNotification - User removed from the party"

.field public static final Message:Ljava/lang/String; = "Push Message"

.field public static final MessageInternal:Ljava/lang/String; = "Push Message In-App"

.field public static final Online:Ljava/lang/String; = "Push Online"

.field public static final OnlineInternal:Ljava/lang/String; = "Push Online In-App"

.field public static final PartyInvite:Ljava/lang/String; = "PushNotification - Party invite"

.field public static final TournamentCanceled:Ljava/lang/String; = "Push Notification - Tournament Canceled"

.field public static final TournamentCheckIn:Ljava/lang/String; = "Push Notification - Tournament Check In"

.field public static final TournamentCompleted:Ljava/lang/String; = "Push Notification - Tournament Completed"

.field public static final TournamentMatchReady:Ljava/lang/String; = "Push Notification - Tournament Match Ready"

.field public static final TournamentStarted:Ljava/lang/String; = "Push Notification - Tournament Started"

.field public static final TournamentTeamCheckIn:Ljava/lang/String; = "Push Notification - Tournament Team Check In"

.field public static final TournamentTeamEliminated:Ljava/lang/String; = "Push Notification - Tournament Team Eliminated"

.field public static final TournamentTeamInvite:Ljava/lang/String; = "Push Notification - Tournament Team Member Invite"

.field public static final TournamentTeamJoin:Ljava/lang/String; = "Push Notification - Tournament Team Member Join"

.field public static final TournamentTeamLeave:Ljava/lang/String; = "Push Notification - Tournament Team Member Leave"

.field public static final TournamentTeamMissedCheckIn:Ljava/lang/String; = "Push Notification - Tournament Team Missed Check In"

.field public static final TournamentTeamPlaying:Ljava/lang/String; = "Push Notification - Tournament Team Playing"

.field public static final TournamentTeamRegistered:Ljava/lang/String; = "Push Notification - Tournament Team Registered"

.field public static final TournamentTeamStandby:Ljava/lang/String; = "Push Notification - Tournament Team Standby"

.field public static final TournamentTeamUnregistered:Ljava/lang/String; = "Push Notification - Tournament Team Unregistered"

.field public static final TournamentTeamWaitListed:Ljava/lang/String; = "Push Notification - Tournament Team Waitlisted"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
