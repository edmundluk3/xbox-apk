.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
.super Ljava/lang/Enum;
.source "UTCChangeRelationship.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GamerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

.field public static final enum FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

.field public static final enum NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

.field public static final enum SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

.field public static final enum UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;


# instance fields
.field private name:Ljava/lang/String;

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    const-string v1, "NORMAL"

    const-string v2, "FriendAdd"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    const-string v1, "FACEBOOK"

    const-string v2, "FacebookAdd"

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    const-string v1, "SUGGESTED"

    const-string v2, "SuggestionAdd"

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->UNKNOWN:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # I
    .param p4, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->value:I

    .line 52
    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->name:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->value:I

    return v0
.end method
