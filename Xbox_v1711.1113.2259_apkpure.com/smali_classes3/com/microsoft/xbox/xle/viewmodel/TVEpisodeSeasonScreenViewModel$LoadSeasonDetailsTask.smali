.class Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TVEpisodeSeasonScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSeasonDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

.field private final seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 80
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    .line 82
    return-void
.end method

.method private onLoadSeasonDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->access$002(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;Z)Z

    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;Z)Z

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->parentSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->setEpisodes(Ljava/util/ArrayList;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->updateAdapter()V

    .line 122
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->seasonModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->loadListDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->onLoadSeasonDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 92
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->onLoadSeasonDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 103
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->access$002(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;Z)Z

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->updateAdapter()V

    .line 98
    return-void
.end method
