.class Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;
.super Ljava/lang/Object;
.source "SGHttpResponse.java"


# instance fields
.field private _headerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private _response:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_headerMap:Ljava/util/Map;

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    .line 27
    return-void
.end method


# virtual methods
.method public getAllHeaders()[Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderMap()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 34
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_headerMap:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 35
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_headerMap:Ljava/util/Map;

    .line 37
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v6

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v0, v6, v5

    .line 38
    .local v0, "header":Lorg/apache/http/Header;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v2, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v8, ","

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    move v3, v4

    :goto_1
    if-ge v3, v9, :cond_0

    aget-object v1, v8, v3

    .line 40
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 43
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_headerMap:Ljava/util/Map;

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 47
    .end local v0    # "header":Lorg/apache/http/Header;
    .end local v2    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_headerMap:Ljava/util/Map;

    return-object v3
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    return v0
.end method

.method public getStatusString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStream()Ljava/io/InputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    if-nez v5, :cond_1

    const/4 v2, 0x0

    .line 60
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    :goto_0
    const/4 v3, 0x0

    .line 61
    .local v3, "stream":Ljava/io/InputStream;
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    .line 62
    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    .line 63
    .local v0, "buffer":[B
    new-instance v3, Ljava/io/ByteArrayInputStream;

    .end local v3    # "stream":Ljava/io/InputStream;
    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 64
    .restart local v3    # "stream":Ljava/io/InputStream;
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 66
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    const-string v6, "Content-Encoding"

    invoke-interface {v5, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 67
    .local v1, "contentEncodingHeader":Lorg/apache/http/Header;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    const-string v6, "gzip"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 68
    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v3    # "stream":Ljava/io/InputStream;
    .local v4, "stream":Ljava/io/InputStream;
    move-object v3, v4

    .line 72
    .end local v0    # "buffer":[B
    .end local v1    # "contentEncodingHeader":Lorg/apache/http/Header;
    .end local v4    # "stream":Ljava/io/InputStream;
    .restart local v3    # "stream":Ljava/io/InputStream;
    :cond_0
    return-object v3

    .line 59
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    .end local v3    # "stream":Ljava/io/InputStream;
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->_response:Lorg/apache/http/HttpResponse;

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    goto :goto_0
.end method
