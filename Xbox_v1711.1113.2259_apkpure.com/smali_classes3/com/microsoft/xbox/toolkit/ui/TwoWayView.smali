.class public Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.super Landroid/widget/AdapterView;
.source "TwoWayView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;,
        Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# static fields
.field private static final AMAZON_DEVICE:Z

.field private static final CHECK_POSITION_SEARCH_DISTANCE:I = 0x14

.field private static final INVALID_POINTER:I = -0x1

.field private static final LAYOUT_FORCE_BOTTOM:I = 0x3

.field private static final LAYOUT_FORCE_TOP:I = 0x1

.field private static final LAYOUT_MOVE_SELECTION:I = 0x6

.field private static final LAYOUT_NORMAL:I = 0x0

.field private static final LAYOUT_SET_SELECTION:I = 0x2

.field private static final LAYOUT_SPECIFIC:I = 0x4

.field private static final LAYOUT_SYNC:I = 0x5

.field private static final LOGTAG:Ljava/lang/String; = "TwoWayView"

.field private static final MAX_SCROLL_FACTOR:F = 0.33f

.field private static final MIN_SCROLL_PREVIEW_PIXELS:I = 0xa

.field private static final NO_POSITION:I = -0x1

.field public static final STATE_NOTHING:[I

.field private static final SYNC_FIRST_POSITION:I = 0x1

.field private static final SYNC_MAX_DURATION_MILLIS:I = 0x64

.field private static final SYNC_SELECTED_POSITION:I = 0x0

.field private static final TOUCH_MODE_DONE_WAITING:I = 0x2

.field private static final TOUCH_MODE_DOWN:I = 0x0

.field private static final TOUCH_MODE_DRAGGING:I = 0x3

.field private static final TOUCH_MODE_FLINGING:I = 0x4

.field private static final TOUCH_MODE_OFF:I = 0x1

.field private static final TOUCH_MODE_ON:I = 0x0

.field private static final TOUCH_MODE_OVERSCROLL:I = 0x5

.field private static final TOUCH_MODE_REST:I = -0x1

.field private static final TOUCH_MODE_TAP:I = 0x1

.field private static final TOUCH_MODE_UNKNOWN:I = -0x1


# instance fields
.field private mAccessibilityDelegate:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;

.field private mActivePointerId:I

.field private mAdapter:Landroid/widget/ListAdapter;

.field private mAreAllItemsSelectable:Z

.field private final mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

.field private mBlockLayoutRequests:Z

.field private mCheckStates:Landroid/util/SparseBooleanArray;

.field mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckedItemCount:I

.field private mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDataChanged:Z

.field private mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

.field private mDesiredFocusableInTouchModeState:Z

.field private mDesiredFocusableState:Z

.field private mDrawSelectorOnTop:Z

.field private mEmptyView:Landroid/view/View;

.field private mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mFirstPosition:I

.field private final mFlingVelocity:I

.field private mHasStableIds:Z

.field private mInLayout:Z

.field private mIsAttached:Z

.field private mIsChildViewEnabled:Z

.field final mIsScrap:[Z

.field private mIsVertical:Z

.field private mItemCount:I

.field private mItemMargin:I

.field private mItemsCanFocus:Z

.field private mLastAccessibilityScrollEventFromIndex:I

.field private mLastAccessibilityScrollEventToIndex:I

.field private mLastScrollState:I

.field private mLastTouchMode:I

.field private mLastTouchPos:F

.field private mLayoutMode:I

.field private final mMaximumVelocity:I

.field private mMotionPosition:I

.field private mNeedSync:Z

.field private mNextSelectedPosition:I

.field private mNextSelectedRowId:J

.field private mOldItemCount:I

.field private mOldSelectedPosition:I

.field private mOldSelectedRowId:J

.field private mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

.field private mOverScroll:I

.field private final mOverscrollDistance:I

.field private mPendingCheckForKeyLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

.field private mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

.field private mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

.field private mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

.field private mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

.field private final mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

.field private mResurrectToPosition:I

.field private final mScroller:Landroid/widget/Scroller;

.field private mSelectedPosition:I

.field private mSelectedRowId:J

.field private mSelectedStart:I

.field private mSelectionNotifier:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;

.field private mSelector:Landroid/graphics/drawable/Drawable;

.field private mSelectorPosition:I

.field private final mSelectorRect:Landroid/graphics/Rect;

.field private mSpecificStart:I

.field private mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mSyncHeight:J

.field private mSyncMode:I

.field private mSyncPosition:I

.field private mSyncRowId:J

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchFrame:Landroid/graphics/Rect;

.field private mTouchMode:I

.field private mTouchModeReset:Ljava/lang/Runnable;

.field private mTouchRemainderPos:F

.field private final mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->STATE_NOTHING:[I

    .line 140
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->isAmazonDevice()Z

    move-result v0

    sput-boolean v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->AMAZON_DEVICE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 311
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 312
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 315
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 316
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const-wide/high16 v10, -0x8000000000000000L

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 319
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168
    new-array v5, v9, [Z

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    .line 321
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 322
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 324
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 325
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 326
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchMode:I

    .line 328
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    .line 330
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 332
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    .line 333
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastScrollState:I

    .line 335
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    .line 336
    .local v4, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchSlop:I

    .line 337
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMaximumVelocity:I

    .line 338
    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFlingVelocity:I

    .line 339
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScaledOverscrollDistance(Landroid/view/ViewConfiguration;)I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverscrollDistance:I

    .line 341
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 343
    new-instance v5, Landroid/widget/Scroller;

    invoke-direct {v5, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    .line 345
    iput-boolean v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    .line 347
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    .line 349
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    .line 351
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    invoke-direct {v5, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    .line 353
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    .line 355
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    .line 356
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 358
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 360
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 361
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    .line 362
    iput-wide v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    .line 363
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 364
    iput-wide v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    .line 365
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedPosition:I

    .line 366
    iput-wide v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedRowId:J

    .line 368
    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    .line 369
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 370
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 371
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 373
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    .line 374
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    .line 376
    iput-boolean v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAreAllItemsSelectable:Z

    .line 378
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 379
    iput-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 381
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setClickable(Z)V

    .line 382
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setFocusableInTouchMode(Z)V

    .line 383
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setWillNotDraw(Z)V

    .line 384
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 385
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setWillNotDraw(Z)V

    .line 386
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setClipToPadding(Z)V

    .line 388
    invoke-static {p0, v9}, Landroid/support/v4/view/ViewCompat;->setOverScrollMode(Landroid/view/View;I)V

    .line 390
    sget-object v5, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoWayView:[I

    invoke-virtual {p1, p2, v5, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 392
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDrawSelectorOnTop:Z

    .line 394
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 395
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 396
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 399
    :cond_0
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 400
    .local v3, "orientation":I
    if-ltz v3, :cond_1

    .line 401
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->values()[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    move-result-object v5

    aget-object v5, v5, v3

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setOrientation(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;)V

    .line 404
    :cond_1
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 405
    .local v1, "choiceMode":I
    if-ltz v1, :cond_2

    .line 406
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->values()[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    move-result-object v5

    aget-object v5, v5, v1

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setChoiceMode(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;)V

    .line 409
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 410
    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldItemCount:I

    return v0
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldItemCount:I

    return p1
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    return v0
.end method

.method static synthetic access$1802(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    return p1
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mHasStableIds:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->rememberSyncState()V

    return-void
.end method

.method static synthetic access$2100(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkFocus()V

    return-void
.end method

.method static synthetic access$2200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    return v0
.end method

.method static synthetic access$2202(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    return p1
.end method

.method static synthetic access$2300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # J

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    return-wide p1
.end method

.method static synthetic access$2402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    return p1
.end method

.method static synthetic access$2502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # J

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    return-wide p1
.end method

.method static synthetic access$2602(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fireOnSelected()V

    return-void
.end method

.method static synthetic access$2900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performAccessibilityActionsOnSelected()V

    return-void
.end method

.method static synthetic access$3000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    return v0
.end method

.method static synthetic access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    return v0
.end method

.method static synthetic access$3502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    return p1
.end method

.method static synthetic access$3600(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    return-void
.end method

.method static synthetic access$3700(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->triggerCheckForLongPress()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    return v0
.end method

.method static synthetic access$4000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;IJ)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 101
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    return-object p1
.end method

.method private adjustViewsStartOrEnd()V
    .locals 4

    .prologue
    .line 4943
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 4964
    :cond_0
    :goto_0
    return-void

    .line 4947
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4950
    .local v1, "firstChild":Landroid/view/View;
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 4951
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int v0, v2, v3

    .line 4956
    .local v0, "delta":I
    :goto_1
    if-gez v0, :cond_2

    .line 4958
    const/4 v0, 0x0

    .line 4961
    :cond_2
    if-eqz v0, :cond_0

    .line 4962
    neg-int v2, v0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetChildren(I)V

    goto :goto_0

    .line 4953
    .end local v0    # "delta":I
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int v0, v2, v3

    .restart local v0    # "delta":I
    goto :goto_1
.end method

.method private amountToScroll(II)I
    .locals 17
    .param p1, "direction"    # I
    .param p2, "nextSelectedPosition"    # I

    .prologue
    .line 2173
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 2175
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v9

    .line 2177
    .local v9, "numChildren":I
    const/16 v15, 0x82

    move/from16 v0, p1

    if-eq v0, v15, :cond_0

    const/16 v15, 0x42

    move/from16 v0, p1

    if-ne v0, v15, :cond_6

    .line 2178
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v2

    .line 2180
    .local v2, "end":I
    add-int/lit8 v6, v9, -0x1

    .line 2181
    .local v6, "indexToMakeVisible":I
    const/4 v15, -0x1

    move/from16 v0, p2

    if-eq v0, v15, :cond_1

    .line 2182
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v6, p2, v15

    .line 2185
    :cond_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int v10, v15, v6

    .line 2186
    .local v10, "positionToMakeVisible":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 2188
    .local v12, "viewToMakeVisible":Landroid/view/View;
    move v4, v2

    .line 2189
    .local v4, "goalEnd":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v15, v15, -0x1

    if-ge v10, v15, :cond_2

    .line 2190
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getArrowScrollPreviewLength()I

    move-result v15

    sub-int/2addr v4, v15

    .line 2193
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v14

    .line 2194
    .local v14, "viewToMakeVisibleStart":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v13

    .line 2196
    .local v13, "viewToMakeVisibleEnd":I
    if-gt v13, v4, :cond_3

    .line 2198
    const/4 v15, 0x0

    .line 2256
    .end local v2    # "end":I
    .end local v4    # "goalEnd":I
    :goto_0
    return v15

    .line 2201
    .restart local v2    # "end":I
    .restart local v4    # "goalEnd":I
    :cond_3
    const/4 v15, -0x1

    move/from16 v0, p2

    if-eq v0, v15, :cond_4

    sub-int v15, v4, v14

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getMaxScrollAmount()I

    move-result v16

    move/from16 v0, v16

    if-lt v15, v0, :cond_4

    .line 2203
    const/4 v15, 0x0

    goto :goto_0

    .line 2206
    :cond_4
    sub-int v1, v13, v4

    .line 2208
    .local v1, "amountToScroll":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v15, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 2209
    add-int/lit8 v15, v9, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v7

    .line 2212
    .local v7, "lastChildEnd":I
    sub-int v8, v7, v2

    .line 2213
    .local v8, "max":I
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2216
    .end local v7    # "lastChildEnd":I
    .end local v8    # "max":I
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getMaxScrollAmount()I

    move-result v15

    invoke-static {v1, v15}, Ljava/lang/Math;->min(II)I

    move-result v15

    goto :goto_0

    .line 2218
    .end local v1    # "amountToScroll":I
    .end local v2    # "end":I
    .end local v4    # "goalEnd":I
    .end local v6    # "indexToMakeVisible":I
    .end local v10    # "positionToMakeVisible":I
    .end local v12    # "viewToMakeVisible":Landroid/view/View;
    .end local v13    # "viewToMakeVisibleEnd":I
    .end local v14    # "viewToMakeVisibleStart":I
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v11

    .line 2220
    .local v11, "start":I
    const/4 v6, 0x0

    .line 2221
    .restart local v6    # "indexToMakeVisible":I
    const/4 v15, -0x1

    move/from16 v0, p2

    if-eq v0, v15, :cond_7

    .line 2222
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v6, p2, v15

    .line 2225
    :cond_7
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int v10, v15, v6

    .line 2226
    .restart local v10    # "positionToMakeVisible":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 2228
    .restart local v12    # "viewToMakeVisible":Landroid/view/View;
    move v5, v11

    .line 2229
    .local v5, "goalStart":I
    if-lez v10, :cond_8

    .line 2230
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getArrowScrollPreviewLength()I

    move-result v15

    add-int/2addr v5, v15

    .line 2233
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v14

    .line 2234
    .restart local v14    # "viewToMakeVisibleStart":I
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v13

    .line 2236
    .restart local v13    # "viewToMakeVisibleEnd":I
    if-lt v14, v5, :cond_9

    .line 2238
    const/4 v15, 0x0

    goto :goto_0

    .line 2241
    :cond_9
    const/4 v15, -0x1

    move/from16 v0, p2

    if-eq v0, v15, :cond_a

    sub-int v15, v13, v5

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getMaxScrollAmount()I

    move-result v16

    move/from16 v0, v16

    if-lt v15, v0, :cond_a

    .line 2243
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 2246
    :cond_a
    sub-int v1, v5, v14

    .line 2248
    .restart local v1    # "amountToScroll":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-nez v15, :cond_b

    .line 2249
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v3

    .line 2252
    .local v3, "firstChildStart":I
    sub-int v8, v11, v3

    .line 2253
    .restart local v8    # "max":I
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2256
    .end local v3    # "firstChildStart":I
    .end local v8    # "max":I
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getMaxScrollAmount()I

    move-result v15

    invoke-static {v1, v15}, Ljava/lang/Math;->min(II)I

    move-result v15

    goto/16 :goto_0
.end method

.method private amountToScrollToNewFocus(ILandroid/view/View;I)I
    .locals 6
    .param p1, "direction"    # I
    .param p2, "newFocus"    # Landroid/view/View;
    .param p3, "positionOfNewFocus"    # I

    .prologue
    .line 2269
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 2271
    const/4 v0, 0x0

    .line 2273
    .local v0, "amountToScroll":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v5}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2274
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2276
    const/16 v5, 0x21

    if-eq p1, v5, :cond_0

    const/16 v5, 0x11

    if-ne p1, v5, :cond_3

    .line 2277
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v4

    .line 2278
    .local v4, "start":I
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->top:I

    .line 2280
    .local v3, "newFocusStart":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 2281
    sub-int v0, v4, v3

    .line 2282
    if-lez p3, :cond_1

    .line 2283
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getArrowScrollPreviewLength()I

    move-result v5

    add-int/2addr v0, v5

    .line 2298
    .end local v3    # "newFocusStart":I
    .end local v4    # "start":I
    :cond_1
    :goto_1
    return v0

    .line 2278
    .restart local v4    # "start":I
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 2287
    .end local v4    # "start":I
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v1

    .line 2288
    .local v1, "end":I
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    .line 2290
    .local v2, "newFocusEnd":I
    :goto_2
    if-le v2, v1, :cond_1

    .line 2291
    sub-int v0, v2, v1

    .line 2292
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v5, v5, -0x1

    if-ge p3, v5, :cond_1

    .line 2293
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getArrowScrollPreviewLength()I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    .line 2288
    .end local v2    # "newFocusEnd":I
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->right:I

    goto :goto_2
.end method

.method private arrowScroll(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v2, 0x0

    .line 1859
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 1862
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    .line 1864
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->arrowScrollImpl(I)Z

    move-result v0

    .line 1865
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 1866
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->playSoundEffect(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1871
    :cond_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    .line 1869
    return v0

    .line 1871
    .end local v0    # "handled":Z
    :catchall_0
    move-exception v1

    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    throw v1
.end method

.method private arrowScrollFocused(I)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;
    .locals 23
    .param p1, "direction"    # I

    .prologue
    .line 1961
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 1963
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v17

    .line 1967
    .local v17, "selectedView":Landroid/view/View;
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->hasFocus()Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1968
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v11

    .line 1969
    .local v11, "oldFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-virtual {v0, v1, v11, v2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    .line 2002
    .end local v11    # "oldFocus":Landroid/view/View;
    .local v10, "newFocus":Landroid/view/View;
    :goto_0
    if-eqz v10, :cond_10

    .line 2003
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionOfNewFocus(Landroid/view/View;)I

    move-result v12

    .line 2007
    .local v12, "positionOfNewFocus":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-eq v12, v0, :cond_e

    .line 2008
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePositionOnScreen(I)I

    move-result v14

    .line 2010
    .local v14, "selectablePosition":I
    const/16 v21, 0x82

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    const/16 v21, 0x42

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    :cond_0
    const/4 v9, 0x1

    .line 2011
    .local v9, "movingForward":Z
    :goto_1
    const/16 v21, 0x21

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_1

    const/16 v21, 0x11

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    :cond_1
    const/4 v8, 0x1

    .line 2013
    .local v8, "movingBackward":Z
    :goto_2
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v14, v0, :cond_e

    if-eqz v9, :cond_2

    if-lt v14, v12, :cond_3

    :cond_2
    if-eqz v8, :cond_e

    if-le v14, v12, :cond_e

    .line 2014
    :cond_3
    const/16 v21, 0x0

    .line 2037
    .end local v8    # "movingBackward":Z
    .end local v9    # "movingForward":Z
    .end local v12    # "positionOfNewFocus":I
    .end local v14    # "selectablePosition":I
    :goto_3
    return-object v21

    .line 1971
    .end local v10    # "newFocus":Landroid/view/View;
    :cond_4
    const/16 v21, 0x82

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    const/16 v21, 0x42

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    .line 1972
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v18

    .line 1975
    .local v18, "start":I
    if-eqz v17, :cond_7

    .line 1976
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_6

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getTop()I

    move-result v16

    .line 1981
    .local v16, "selectedStart":I
    :goto_4
    move/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 1995
    .end local v16    # "selectedStart":I
    .end local v18    # "start":I
    .local v13, "searchPoint":I
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_a

    const/16 v19, 0x0

    .line 1996
    .local v19, "x":I
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_b

    move/from16 v20, v13

    .line 1997
    .local v20, "y":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1999
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v10

    .restart local v10    # "newFocus":Landroid/view/View;
    goto/16 :goto_0

    .line 1976
    .end local v10    # "newFocus":Landroid/view/View;
    .end local v13    # "searchPoint":I
    .end local v19    # "x":I
    .end local v20    # "y":I
    .restart local v18    # "start":I
    :cond_6
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLeft()I

    move-result v16

    goto :goto_4

    .line 1978
    :cond_7
    move/from16 v16, v18

    .restart local v16    # "selectedStart":I
    goto :goto_4

    .line 1983
    .end local v16    # "selectedStart":I
    .end local v18    # "start":I
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v5

    .line 1986
    .local v5, "end":I
    if-eqz v17, :cond_9

    .line 1987
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v15

    .line 1992
    .local v15, "selectedEnd":I
    :goto_8
    invoke-static {v15, v5}, Ljava/lang/Math;->min(II)I

    move-result v13

    .restart local v13    # "searchPoint":I
    goto :goto_5

    .line 1989
    .end local v13    # "searchPoint":I
    .end local v15    # "selectedEnd":I
    :cond_9
    move v15, v5

    .restart local v15    # "selectedEnd":I
    goto :goto_8

    .end local v5    # "end":I
    .end local v15    # "selectedEnd":I
    .restart local v13    # "searchPoint":I
    :cond_a
    move/from16 v19, v13

    .line 1995
    goto :goto_6

    .line 1996
    .restart local v19    # "x":I
    :cond_b
    const/16 v20, 0x0

    goto :goto_7

    .line 2010
    .end local v13    # "searchPoint":I
    .end local v19    # "x":I
    .restart local v10    # "newFocus":Landroid/view/View;
    .restart local v12    # "positionOfNewFocus":I
    .restart local v14    # "selectablePosition":I
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 2011
    .restart local v9    # "movingForward":Z
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 2018
    .end local v9    # "movingForward":Z
    .end local v14    # "selectablePosition":I
    :cond_e
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v10, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->amountToScrollToNewFocus(ILandroid/view/View;I)I

    move-result v6

    .line 2020
    .local v6, "focusScroll":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getMaxScrollAmount()I

    move-result v7

    .line 2021
    .local v7, "maxScrollAmount":I
    if-ge v6, v7, :cond_f

    .line 2023
    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2024
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->populate(II)V

    .line 2025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    goto/16 :goto_3

    .line 2026
    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->distanceToView(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    if-ge v0, v7, :cond_10

    .line 2031
    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->populate(II)V

    .line 2033
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mArrowScrollFocusResult:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    goto/16 :goto_3

    .line 2037
    .end local v6    # "focusScroll":I
    .end local v7    # "maxScrollAmount":I
    .end local v12    # "positionOfNewFocus":I
    :cond_10
    const/16 v21, 0x0

    goto/16 :goto_3
.end method

.method private arrowScrollImpl(I)Z
    .locals 11
    .param p1, "direction"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 2082
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 2084
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    if-gtz v7, :cond_1

    .line 2162
    :cond_0
    :goto_0
    return v9

    .line 2088
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2089
    .local v6, "selectedView":Landroid/view/View;
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 2091
    .local v5, "selectedPos":I
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePositionOnScreen(I)I

    move-result v4

    .line 2092
    .local v4, "nextSelectedPosition":I
    invoke-direct {p0, p1, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->amountToScroll(II)I

    move-result v0

    .line 2095
    .local v0, "amountToScroll":I
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    if-eqz v7, :cond_c

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->arrowScrollFocused(I)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;

    move-result-object v1

    .line 2096
    .local v1, "focusResult":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;
    :goto_1
    if-eqz v1, :cond_2

    .line 2097
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->getSelectedPosition()I

    move-result v4

    .line 2098
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;->getAmountToScroll()I

    move-result v0

    .line 2101
    :cond_2
    if-eqz v1, :cond_d

    move v3, v8

    .line 2102
    .local v3, "needToRedraw":Z
    :goto_2
    if-eq v4, v10, :cond_4

    .line 2103
    if-eqz v1, :cond_e

    move v7, v8

    :goto_3
    invoke-direct {p0, v6, p1, v4, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleNewSelectionChange(Landroid/view/View;IIZ)V

    .line 2105
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 2106
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 2108
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2109
    move v5, v4

    .line 2111
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    if-eqz v7, :cond_3

    if-nez v1, :cond_3

    .line 2114
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 2115
    .local v2, "focused":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 2116
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2120
    .end local v2    # "focused":Landroid/view/View;
    :cond_3
    const/4 v3, 0x1

    .line 2121
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkSelectionChanged()V

    .line 2124
    :cond_4
    if-lez v0, :cond_6

    .line 2125
    const/16 v7, 0x21

    if-eq p1, v7, :cond_5

    const/16 v7, 0x11

    if-ne p1, v7, :cond_f

    .end local v0    # "amountToScroll":I
    :cond_5
    :goto_4
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->scrollListItemsBy(I)Z

    .line 2126
    const/4 v3, 0x1

    .line 2131
    :cond_6
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    if-eqz v7, :cond_8

    if-nez v1, :cond_8

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Landroid/view/View;->hasFocus()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2132
    invoke-virtual {v6}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 2133
    .restart local v2    # "focused":Landroid/view/View;
    invoke-direct {p0, v2, p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->distanceToView(Landroid/view/View;)I

    move-result v7

    if-lez v7, :cond_8

    .line 2134
    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2139
    .end local v2    # "focused":Landroid/view/View;
    :cond_8
    if-ne v4, v10, :cond_9

    if-eqz v6, :cond_9

    invoke-direct {p0, v6, p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2140
    const/4 v6, 0x0

    .line 2141
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hideSelector()V

    .line 2145
    iput v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 2148
    :cond_9
    if-eqz v3, :cond_0

    .line 2149
    if-eqz v6, :cond_a

    .line 2150
    invoke-direct {p0, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    .line 2151
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 2154
    :cond_a
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    move-result v7

    if-nez v7, :cond_b

    .line 2155
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 2158
    :cond_b
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    move v9, v8

    .line 2159
    goto/16 :goto_0

    .line 2095
    .end local v1    # "focusResult":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;
    .end local v3    # "needToRedraw":Z
    .restart local v0    # "amountToScroll":I
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    .restart local v1    # "focusResult":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ArrowScrollFocusResult;
    :cond_d
    move v3, v9

    .line 2101
    goto/16 :goto_2

    .restart local v3    # "needToRedraw":Z
    :cond_e
    move v7, v9

    .line 2103
    goto/16 :goto_3

    .line 2125
    :cond_f
    neg-int v0, v0

    goto :goto_4
.end method

.method private awakenScrollbarsInternal()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    .line 2976
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    invoke-super {p0}, Landroid/widget/AdapterView;->awakenScrollBars()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelCheckForLongPress()V
    .locals 1

    .prologue
    .line 2832
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    if-nez v0, :cond_0

    .line 2837
    :goto_0
    return-void

    .line 2836
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private cancelCheckForTap()V
    .locals 1

    .prologue
    .line 2814
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    if-nez v0, :cond_0

    .line 2819
    :goto_0
    return-void

    .line 2818
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private checkFocus()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5820
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 5821
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    move v1, v3

    .line 5826
    .local v1, "focusable":Z
    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableInTouchModeState:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    invoke-super {p0, v2}, Landroid/widget/AdapterView;->setFocusableInTouchMode(Z)V

    .line 5827
    if-eqz v1, :cond_3

    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableState:Z

    if-eqz v2, :cond_3

    :goto_2
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusable(Z)V

    .line 5829
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 5830
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateEmptyStatus()V

    .line 5832
    :cond_0
    return-void

    .end local v1    # "focusable":Z
    :cond_1
    move v1, v4

    .line 5821
    goto :goto_0

    .restart local v1    # "focusable":Z
    :cond_2
    move v2, v4

    .line 5826
    goto :goto_1

    :cond_3
    move v3, v4

    .line 5827
    goto :goto_2
.end method

.method private checkSelectionChanged()V
    .locals 4

    .prologue
    .line 3225
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedPosition:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedRowId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3226
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->selectionChanged()V

    .line 3227
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedPosition:I

    .line 3228
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedRowId:J

    .line 3230
    :cond_1
    return-void
.end method

.method private cloneCheckStates()Landroid/util/SparseBooleanArray;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 4968
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-nez v2, :cond_1

    .line 4969
    const/4 v0, 0x0

    .line 4984
    :cond_0
    :goto_0
    return-object v0

    .line 4974
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 4975
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .local v0, "checkedStates":Landroid/util/SparseBooleanArray;
    goto :goto_0

    .line 4977
    .end local v0    # "checkedStates":Landroid/util/SparseBooleanArray;
    :cond_2
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 4979
    .restart local v0    # "checkedStates":Landroid/util/SparseBooleanArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 4980
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 4979
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private contentFits()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2790
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v0

    .line 2791
    .local v0, "childCount":I
    if-nez v0, :cond_1

    .line 2802
    :cond_0
    :goto_0
    return v3

    .line 2795
    :cond_1
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-eq v0, v5, :cond_2

    move v3, v4

    .line 2796
    goto :goto_0

    .line 2799
    :cond_2
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2800
    .local v1, "first":Landroid/view/View;
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2802
    .local v2, "last":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v5

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v6

    if-lt v5, v6, :cond_3

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v5

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v6

    if-le v5, v6, :cond_0

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method private correctTooHigh(I)V
    .locals 10
    .param p1, "childCount"    # I

    .prologue
    .line 4834
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v8, p1

    add-int/lit8 v6, v8, -0x1

    .line 4835
    .local v6, "lastPosition":I
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v6, v8, :cond_0

    if-nez p1, :cond_1

    .line 4883
    :cond_0
    :goto_0
    return-void

    .line 4840
    :cond_1
    add-int/lit8 v8, p1, -0x1

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 4844
    .local v4, "lastChild":Landroid/view/View;
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_4

    .line 4845
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v5

    .line 4851
    .local v5, "lastEnd":I
    :goto_1
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v7

    .line 4852
    .local v7, "start":I
    :goto_2
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_6

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    .line 4856
    .local v0, "end":I
    :goto_3
    sub-int v1, v0, v5

    .line 4858
    .local v1, "endOffset":I
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4859
    .local v2, "firstChild":Landroid/view/View;
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_7

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4863
    .local v3, "firstStart":I
    :goto_4
    if-lez v1, :cond_0

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-gtz v8, :cond_2

    if-ge v3, v7, :cond_0

    .line 4864
    :cond_2
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-nez v8, :cond_3

    .line 4866
    sub-int v8, v7, v3

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4870
    :cond_3
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetChildren(I)V

    .line 4872
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-lez v8, :cond_0

    .line 4873
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_8

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4877
    :goto_5
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/lit8 v8, v8, -0x1

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int v9, v3, v9

    invoke-direct {p0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    .line 4880
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    goto :goto_0

    .line 4847
    .end local v0    # "end":I
    .end local v1    # "endOffset":I
    .end local v2    # "firstChild":Landroid/view/View;
    .end local v3    # "firstStart":I
    .end local v5    # "lastEnd":I
    .end local v7    # "start":I
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v5

    .restart local v5    # "lastEnd":I
    goto :goto_1

    .line 4851
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v7

    goto :goto_2

    .line 4852
    .restart local v7    # "start":I
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v9

    sub-int v0, v8, v9

    goto :goto_3

    .line 4859
    .restart local v0    # "end":I
    .restart local v1    # "endOffset":I
    .restart local v2    # "firstChild":Landroid/view/View;
    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_4

    .line 4873
    .restart local v3    # "firstStart":I
    :cond_8
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_5
.end method

.method private correctTooLow(I)V
    .locals 10
    .param p1, "childCount"    # I

    .prologue
    .line 4888
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-nez v8, :cond_0

    if-nez p1, :cond_1

    .line 4940
    :cond_0
    :goto_0
    return-void

    .line 4892
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4893
    .local v1, "first":Landroid/view/View;
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 4895
    .local v2, "firstStart":I
    :goto_1
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v6

    .line 4898
    .local v6, "start":I
    :goto_2
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_6

    .line 4899
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    .line 4906
    .local v0, "end":I
    :goto_3
    sub-int v7, v2, v6

    .line 4908
    .local v7, "startOffset":I
    add-int/lit8 v8, p1, -0x1

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4909
    .local v3, "last":Landroid/view/View;
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 4911
    .local v4, "lastEnd":I
    :goto_4
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v8, p1

    add-int/lit8 v5, v8, -0x1

    .line 4916
    .local v5, "lastPosition":I
    if-lez v7, :cond_0

    .line 4917
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-lt v5, v8, :cond_2

    if-le v4, v0, :cond_9

    .line 4918
    :cond_2
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_3

    .line 4920
    sub-int v8, v4, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 4924
    :cond_3
    neg-int v8, v7

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetChildren(I)V

    .line 4926
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ge v5, v8, :cond_0

    .line 4927
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_8

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 4931
    :goto_5
    add-int/lit8 v8, v5, 0x1

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    add-int/2addr v9, v4

    invoke-direct {p0, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    .line 4934
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    goto :goto_0

    .line 4893
    .end local v0    # "end":I
    .end local v2    # "firstStart":I
    .end local v3    # "last":Landroid/view/View;
    .end local v4    # "lastEnd":I
    .end local v5    # "lastPosition":I
    .end local v6    # "start":I
    .end local v7    # "startOffset":I
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_1

    .line 4895
    .restart local v2    # "firstStart":I
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v6

    goto :goto_2

    .line 4901
    .restart local v6    # "start":I
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v9

    sub-int v0, v8, v9

    .restart local v0    # "end":I
    goto :goto_3

    .line 4909
    .restart local v3    # "last":Landroid/view/View;
    .restart local v7    # "startOffset":I
    :cond_7
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    goto :goto_4

    .line 4927
    .restart local v4    # "lastEnd":I
    .restart local v5    # "lastPosition":I
    :cond_8
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    goto :goto_5

    .line 4936
    :cond_9
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_0

    .line 4937
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    goto/16 :goto_0
.end method

.method private createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 5180
    new-instance v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method private distanceToView(Landroid/view/View;)I
    .locals 6
    .param p1, "descendant"    # Landroid/view/View;

    .prologue
    .line 2308
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v5}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2309
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2311
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v2

    .line 2312
    .local v2, "start":I
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v1

    .line 2314
    .local v1, "end":I
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v4, v5, Landroid/graphics/Rect;->top:I

    .line 2315
    .local v4, "viewStart":I
    :goto_0
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->bottom:I

    .line 2317
    .local v3, "viewEnd":I
    :goto_1
    const/4 v0, 0x0

    .line 2318
    .local v0, "distance":I
    if-ge v3, v2, :cond_3

    .line 2319
    sub-int v0, v2, v3

    .line 2324
    :cond_0
    :goto_2
    return v0

    .line 2314
    .end local v0    # "distance":I
    .end local v3    # "viewEnd":I
    .end local v4    # "viewStart":I
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v4, v5, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 2315
    .restart local v4    # "viewStart":I
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 2320
    .restart local v0    # "distance":I
    .restart local v3    # "viewEnd":I
    :cond_3
    if-le v4, v1, :cond_0

    .line 2321
    sub-int v0, v4, v1

    goto :goto_2
.end method

.method private drawEndEdge(Landroid/graphics/Canvas;)Z
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    .line 3051
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3052
    const/4 v1, 0x0

    .line 3069
    :goto_0
    return v1

    .line 3055
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 3056
    .local v2, "restoreCount":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v3

    .line 3057
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v0

    .line 3059
    .local v0, "height":I
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v4, :cond_1

    .line 3060
    neg-int v4, v3

    int-to-float v4, v4

    int-to-float v5, v0

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3061
    const/high16 v4, 0x43340000    # 180.0f

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 3067
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    .line 3068
    .local v1, "needsInvalidate":Z
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 3063
    .end local v1    # "needsInvalidate":Z
    :cond_1
    int-to-float v4, v3

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3064
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    goto :goto_1
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3073
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3074
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 3075
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3076
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3078
    .end local v0    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method private drawStartEdge(Landroid/graphics/Canvas;)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3031
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3032
    const/4 v1, 0x0

    .line 3047
    :goto_0
    return v1

    .line 3035
    :cond_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 3036
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    goto :goto_0

    .line 3039
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 3040
    .local v2, "restoreCount":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v0

    .line 3042
    .local v0, "height":I
    const/4 v3, 0x0

    int-to-float v4, v0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3043
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3045
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    .line 3046
    .local v1, "needsInvalidate":Z
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private fillAfter(II)Landroid/view/View;
    .locals 7
    .param p1, "pos"    # I
    .param p2, "nextOffset"    # I

    .prologue
    const/4 v4, 0x1

    .line 4659
    const/4 v3, 0x0

    .line 4661
    .local v3, "selectedView":Landroid/view/View;
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v6

    sub-int v1, v5, v6

    .line 4663
    .local v1, "end":I
    :goto_0
    if-ge p2, v1, :cond_4

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-ge p1, v5, :cond_4

    .line 4664
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ne p1, v5, :cond_2

    move v2, v4

    .line 4666
    .local v2, "selected":Z
    :goto_1
    invoke-direct {p0, p1, p2, v4, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 4668
    .local v0, "child":Landroid/view/View;
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_3

    .line 4669
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    add-int p2, v5, v6

    .line 4674
    :goto_2
    if-eqz v2, :cond_0

    .line 4675
    move-object v3, v0

    .line 4678
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 4679
    goto :goto_0

    .line 4661
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "end":I
    .end local v2    # "selected":Z
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v6

    sub-int v1, v5, v6

    goto :goto_0

    .line 4664
    .restart local v1    # "end":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 4671
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "selected":Z
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    add-int p2, v5, v6

    goto :goto_2

    .line 4681
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "selected":Z
    :cond_4
    return-object v3
.end method

.method private fillBefore(II)Landroid/view/View;
    .locals 7
    .param p1, "pos"    # I
    .param p2, "nextOffset"    # I

    .prologue
    const/4 v4, 0x0

    .line 4632
    const/4 v2, 0x0

    .line 4634
    .local v2, "selectedView":Landroid/view/View;
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v3

    .line 4636
    .local v3, "start":I
    :goto_0
    if-le p2, v3, :cond_4

    if-ltz p1, :cond_4

    .line 4637
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ne p1, v5, :cond_2

    const/4 v1, 0x1

    .line 4638
    .local v1, "isSelected":Z
    :goto_1
    invoke-direct {p0, p1, p2, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 4640
    .local v0, "child":Landroid/view/View;
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_3

    .line 4641
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int p2, v5, v6

    .line 4646
    :goto_2
    if-eqz v1, :cond_0

    .line 4647
    move-object v2, v0

    .line 4650
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 4651
    goto :goto_0

    .line 4634
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "isSelected":Z
    .end local v3    # "start":I
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v3

    goto :goto_0

    .restart local v3    # "start":I
    :cond_2
    move v1, v4

    .line 4637
    goto :goto_1

    .line 4643
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "isSelected":Z
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int p2, v5, v6

    goto :goto_2

    .line 4653
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "isSelected":Z
    :cond_4
    add-int/lit8 v4, p1, 0x1

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4655
    return-object v2
.end method

.method private fillBeforeAndAfter(Landroid/view/View;I)V
    .locals 4
    .param p1, "selected"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 4763
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 4766
    .local v0, "itemMargin":I
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_0

    .line 4767
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v2, v3, v0

    .line 4772
    .local v2, "offsetBefore":I
    :goto_0
    add-int/lit8 v3, p2, -0x1

    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    .line 4774
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    .line 4777
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 4778
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    add-int v1, v3, v0

    .line 4783
    .local v1, "offsetAfter":I
    :goto_1
    add-int/lit8 v3, p2, 0x1

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    .line 4784
    return-void

    .line 4769
    .end local v1    # "offsetAfter":I
    .end local v2    # "offsetBefore":I
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v2, v3, v0

    .restart local v2    # "offsetBefore":I
    goto :goto_0

    .line 4780
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    add-int v1, v3, v0

    .restart local v1    # "offsetAfter":I
    goto :goto_1
.end method

.method private fillFromMiddle(II)Landroid/view/View;
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v5, 0x1

    .line 4738
    sub-int v4, p2, p1

    .line 4739
    .local v4, "size":I
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reconcileSelectedPosition()I

    move-result v0

    .line 4741
    .local v0, "position":I
    invoke-direct {p0, v0, p1, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 4742
    .local v1, "selected":Landroid/view/View;
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4744
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_1

    .line 4745
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 4746
    .local v2, "selectedHeight":I
    if-gt v2, v4, :cond_0

    .line 4747
    sub-int v5, v4, v2

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v1, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4756
    .end local v2    # "selectedHeight":I
    :cond_0
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 4757
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->correctTooHigh(I)V

    .line 4759
    return-object v1

    .line 4750
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 4751
    .local v3, "selectedWidth":I
    if-gt v3, v4, :cond_0

    .line 4752
    sub-int v5, v4, v3

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v1, v5}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0
.end method

.method private fillFromOffset(I)Landroid/view/View;
    .locals 2
    .param p1, "nextOffset"    # I

    .prologue
    .line 4727
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4728
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4730
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-gez v0, :cond_0

    .line 4731
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4734
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private fillFromSelection(III)Landroid/view/View;
    .locals 8
    .param p1, "selectedTop"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v7, 0x1

    .line 4787
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 4790
    .local v3, "selectedPosition":I
    invoke-direct {p0, v3, p1, v7, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 4792
    .local v1, "selected":Landroid/view/View;
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    .line 4793
    .local v4, "selectedStart":I
    :goto_0
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v7, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 4796
    .local v2, "selectedEnd":I
    :goto_1
    if-le v2, p3, :cond_3

    .line 4799
    sub-int v5, v4, p2

    .line 4803
    .local v5, "spaceAbove":I
    sub-int v6, v2, p3

    .line 4805
    .local v6, "spaceBelow":I
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4808
    .local v0, "offset":I
    neg-int v7, v0

    invoke-virtual {v1, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4825
    .end local v0    # "offset":I
    .end local v5    # "spaceAbove":I
    .end local v6    # "spaceBelow":I
    :cond_0
    :goto_2
    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 4826
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->correctTooHigh(I)V

    .line 4828
    return-object v1

    .line 4792
    .end local v2    # "selectedEnd":I
    .end local v4    # "selectedStart":I
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_0

    .line 4793
    .restart local v4    # "selectedStart":I
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    goto :goto_1

    .line 4809
    .restart local v2    # "selectedEnd":I
    :cond_3
    if-ge v4, p2, :cond_0

    .line 4812
    sub-int v5, p2, v4

    .line 4816
    .restart local v5    # "spaceAbove":I
    sub-int v6, p3, v2

    .line 4818
    .restart local v6    # "spaceBelow":I
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4821
    .restart local v0    # "offset":I
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_2
.end method

.method private fillSpecific(II)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v8, 0x1

    .line 4685
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ne p1, v9, :cond_1

    move v7, v8

    .line 4686
    .local v7, "tempIsSelected":Z
    :goto_0
    invoke-direct {p0, p1, p2, v8, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v6

    .line 4689
    .local v6, "temp":Landroid/view/View;
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4691
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 4694
    .local v3, "itemMargin":I
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_2

    .line 4695
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int v5, v8, v3

    .line 4699
    .local v5, "offsetBefore":I
    :goto_1
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v8, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    move-result-object v1

    .line 4702
    .local v1, "before":Landroid/view/View;
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    .line 4705
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_3

    .line 4706
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v8

    add-int v4, v8, v3

    .line 4710
    .local v4, "offsetAfter":I
    :goto_2
    add-int/lit8 v8, p1, 0x1

    invoke-direct {p0, v8, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    move-result-object v0

    .line 4712
    .local v0, "after":Landroid/view/View;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v2

    .line 4713
    .local v2, "childCount":I
    if-lez v2, :cond_0

    .line 4714
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->correctTooHigh(I)V

    .line 4717
    :cond_0
    if-eqz v7, :cond_4

    .line 4722
    .end local v6    # "temp":Landroid/view/View;
    :goto_3
    return-object v6

    .line 4685
    .end local v0    # "after":Landroid/view/View;
    .end local v1    # "before":Landroid/view/View;
    .end local v2    # "childCount":I
    .end local v3    # "itemMargin":I
    .end local v4    # "offsetAfter":I
    .end local v5    # "offsetBefore":I
    .end local v7    # "tempIsSelected":Z
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 4697
    .restart local v3    # "itemMargin":I
    .restart local v6    # "temp":Landroid/view/View;
    .restart local v7    # "tempIsSelected":Z
    :cond_2
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int v5, v8, v3

    .restart local v5    # "offsetBefore":I
    goto :goto_1

    .line 4708
    .restart local v1    # "before":Landroid/view/View;
    :cond_3
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v8

    add-int v4, v8, v3

    .restart local v4    # "offsetAfter":I
    goto :goto_2

    .line 4719
    .restart local v0    # "after":Landroid/view/View;
    .restart local v2    # "childCount":I
    :cond_4
    if-eqz v1, :cond_5

    move-object v6, v1

    .line 4720
    goto :goto_3

    :cond_5
    move-object v6, v0

    .line 4722
    goto :goto_3
.end method

.method private findClosestMotionRowOrColumn(I)I
    .locals 3
    .param p1, "motionPos"    # I

    .prologue
    const/4 v2, -0x1

    .line 2747
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v0

    .line 2748
    .local v0, "childCount":I
    if-nez v0, :cond_1

    move v1, v2

    .line 2756
    :cond_0
    :goto_0
    return v1

    .line 2752
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findMotionRowOrColumn(I)I

    move-result v1

    .line 2753
    .local v1, "motionRow":I
    if-ne v1, v2, :cond_0

    .line 2756
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method private findMotionRowOrColumn(I)I
    .locals 5
    .param p1, "motionPos"    # I

    .prologue
    const/4 v3, -0x1

    .line 2730
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v0

    .line 2731
    .local v0, "childCount":I
    if-nez v0, :cond_1

    .line 2743
    :cond_0
    :goto_0
    return v3

    .line 2735
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 2736
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2738
    .local v2, "v":Landroid/view/View;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v4

    if-gt p1, v4, :cond_2

    .line 2739
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v3, v1

    goto :goto_0

    .line 2735
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private findSyncPosition()I
    .locals 20

    .prologue
    .line 4988
    move-object/from16 v0, p0

    iget v10, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 4990
    .local v10, "itemCount":I
    if-nez v10, :cond_1

    .line 4991
    const/4 v13, -0x1

    .line 5064
    :cond_0
    :goto_0
    return v13

    .line 4994
    :cond_1
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 4997
    .local v8, "idToMatch":J
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v16, v8, v16

    if-nez v16, :cond_2

    .line 4998
    const/4 v13, -0x1

    goto :goto_0

    .line 5002
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 5003
    .local v13, "seed":I
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 5004
    add-int/lit8 v16, v10, -0x1

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 5006
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x64

    add-long v4, v16, v18

    .line 5011
    .local v4, "endTime":J
    move v3, v13

    .line 5014
    .local v3, "first":I
    move v11, v13

    .line 5017
    .local v11, "last":I
    const/4 v12, 0x0

    .line 5027
    .local v12, "next":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 5028
    .local v2, "adapter":Landroid/widget/ListAdapter;
    if-nez v2, :cond_5

    .line 5029
    const/4 v13, -0x1

    goto :goto_0

    .line 5047
    .local v6, "hitFirst":Z
    .local v7, "hitLast":Z
    .local v14, "rowId":J
    :cond_3
    if-nez v6, :cond_4

    if-eqz v12, :cond_9

    if-nez v7, :cond_9

    .line 5049
    :cond_4
    add-int/lit8 v11, v11, 0x1

    .line 5050
    move v13, v11

    .line 5053
    const/4 v12, 0x0

    .line 5032
    .end local v6    # "hitFirst":Z
    .end local v7    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    cmp-long v16, v16, v4

    if-gtz v16, :cond_6

    .line 5033
    invoke-interface {v2, v13}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v14

    .line 5034
    .restart local v14    # "rowId":J
    cmp-long v16, v14, v8

    if-eqz v16, :cond_0

    .line 5039
    add-int/lit8 v16, v10, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_7

    const/4 v7, 0x1

    .line 5040
    .restart local v7    # "hitLast":Z
    :goto_2
    if-nez v3, :cond_8

    const/4 v6, 0x1

    .line 5042
    .restart local v6    # "hitFirst":Z
    :goto_3
    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    .line 5064
    .end local v6    # "hitFirst":Z
    .end local v7    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_6
    const/4 v13, -0x1

    goto :goto_0

    .line 5039
    .restart local v14    # "rowId":J
    :cond_7
    const/4 v7, 0x0

    goto :goto_2

    .line 5040
    .restart local v7    # "hitLast":Z
    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .line 5054
    .restart local v6    # "hitFirst":Z
    :cond_9
    if-nez v7, :cond_a

    if-nez v12, :cond_5

    if-nez v6, :cond_5

    .line 5056
    :cond_a
    add-int/lit8 v3, v3, -0x1

    .line 5057
    move v13, v3

    .line 5060
    const/4 v12, 0x1

    goto :goto_1
.end method

.method private finishEdgeGlows()V
    .locals 1

    .prologue
    .line 3021
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v0, :cond_0

    .line 3022
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 3025
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v0, :cond_1

    .line 3026
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 3028
    :cond_1
    return-void
.end method

.method private fireOnSelected()V
    .locals 6

    .prologue
    .line 3255
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 3256
    .local v0, "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    if-nez v0, :cond_0

    .line 3267
    :goto_0
    return-void

    .line 3260
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v3

    .line 3261
    .local v3, "selection":I
    if-ltz v3, :cond_1

    .line 3262
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 3263
    .local v2, "v":Landroid/view/View;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 3265
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-interface {v0, p0}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    goto :goto_0
.end method

.method private forceValidFocusDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1699
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x21

    if-eq p1, v0, :cond_0

    const/16 v0, 0x82

    if-eq p1, v0, :cond_0

    .line 1700
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Focus direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1701
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v0, :cond_1

    const/16 v0, 0x11

    if-eq p1, v0, :cond_1

    const/16 v0, 0x42

    if-eq p1, v0, :cond_1

    .line 1702
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Focus direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1704
    :cond_1
    return-void
.end method

.method private forceValidInnerFocusDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1707
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    .line 1708
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1709
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v0, :cond_1

    const/16 v0, 0x21

    if-eq p1, v0, :cond_1

    const/16 v0, 0x82

    if-eq p1, v0, :cond_1

    .line 1710
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for horizontal orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1712
    :cond_1
    return-void
.end method

.method private getArrowScrollPreviewLength()I
    .locals 3

    .prologue
    .line 2053
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 2055
    .local v0, "fadingEdgeLength":I
    :goto_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    const/16 v2, 0xa

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    return v1

    .line 2053
    .end local v0    # "fadingEdgeLength":I
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHorizontalFadingEdgeLength()I

    move-result v0

    goto :goto_0
.end method

.method private getChildEndEdge(Landroid/view/View;)I
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 2786
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    goto :goto_0
.end method

.method private getChildHeightMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I
    .locals 5
    .param p1, "lp"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 4290
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 4291
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 4296
    :goto_0
    return v1

    .line 4292
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v1, :cond_1

    .line 4293
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v2

    sub-int v0, v1, v2

    .line 4294
    .local v0, "maxHeight":I
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 4296
    .end local v0    # "maxHeight":I
    :cond_1
    iget v1, p1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->height:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0
.end method

.method private getChildStartEdge(Landroid/view/View;)I
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 2782
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method

.method private getChildWidthMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I
    .locals 5
    .param p1, "lp"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 4279
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 4280
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 4285
    :goto_0
    return v1

    .line 4281
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v1, :cond_1

    .line 4282
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v2

    sub-int v0, v1, v2

    .line 4283
    .local v0, "maxWidth":I
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 4285
    .end local v0    # "maxWidth":I
    :cond_1
    iget v1, p1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->width:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0
.end method

.method private final getCurrVelocity()F
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 2967
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 2968
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v0

    .line 2971
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 8
    .param p0, "source"    # Landroid/graphics/Rect;
    .param p1, "dest"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 2682
    sparse-switch p2, :sswitch_data_0

    .line 2720
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2684
    :sswitch_0
    iget v4, p0, Landroid/graphics/Rect;->right:I

    .line 2685
    .local v4, "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2686
    .local v5, "sY":I
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 2687
    .local v0, "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2723
    .local v1, "dY":I
    :goto_0
    sub-int v2, v0, v4

    .line 2724
    .local v2, "deltaX":I
    sub-int v3, v1, v5

    .line 2726
    .local v3, "deltaY":I
    mul-int v6, v3, v3

    mul-int v7, v2, v2

    add-int/2addr v6, v7

    return v6

    .line 2691
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v2    # "deltaX":I
    .end local v3    # "deltaY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_1
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2692
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    .line 2693
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2694
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 2695
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2698
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_2
    iget v4, p0, Landroid/graphics/Rect;->left:I

    .line 2699
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2700
    .restart local v5    # "sY":I
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 2701
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2702
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2705
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_3
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2706
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->top:I

    .line 2707
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2708
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 2709
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2713
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_4
    iget v6, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2714
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2715
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2716
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2717
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2682
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method private getEndEdge()I
    .locals 2

    .prologue
    .line 2774
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    .line 2775
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2777
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private getScaledOverscrollDistance(Landroid/view/ViewConfiguration;)I
    .locals 2
    .param p1, "vc"    # Landroid/view/ViewConfiguration;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 2762
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 2763
    const/4 v0, 0x0

    .line 2766
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v0

    goto :goto_0
.end method

.method private getStartEdge()I
    .locals 1

    .prologue
    .line 2770
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v0

    goto :goto_0
.end method

.method private handleDataChanged()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 4060
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v4, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4061
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->confirmCheckedPositionsById()V

    .line 4064
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->clearTransientStateViews()V

    .line 4066
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 4067
    .local v0, "itemCount":I
    if-lez v0, :cond_9

    .line 4072
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    if-eqz v3, :cond_1

    .line 4074
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 4075
    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 4077
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    packed-switch v3, :pswitch_data_0

    .line 4126
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v3

    if-nez v3, :cond_8

    .line 4128
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v1

    .line 4131
    .local v1, "newPos":I
    if-lt v1, v0, :cond_2

    .line 4132
    add-int/lit8 v1, v0, -0x1

    .line 4134
    :cond_2
    if-gez v1, :cond_3

    .line 4135
    const/4 v1, 0x0

    .line 4139
    :cond_3
    invoke-direct {p0, v1, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4141
    .local v2, "selectablePos":I
    if-ltz v2, :cond_7

    .line 4142
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 4171
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :cond_4
    :goto_0
    return-void

    .line 4079
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4084
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 4085
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    goto :goto_0

    .line 4091
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findSyncPosition()I

    move-result v1

    .line 4092
    .restart local v1    # "newPos":I
    if-ltz v1, :cond_1

    .line 4094
    invoke-direct {p0, v1, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4095
    .restart local v2    # "selectablePos":I
    if-ne v2, v1, :cond_1

    .line 4097
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 4099
    iget-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncHeight:J

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v3

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    .line 4102
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 4110
    :goto_1
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 4106
    :cond_6
    const/4 v3, 0x2

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    goto :goto_1

    .line 4119
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :pswitch_1
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 4120
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    goto :goto_0

    .line 4146
    .restart local v1    # "newPos":I
    .restart local v2    # "selectablePos":I
    :cond_7
    invoke-direct {p0, v1, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4147
    if-ltz v2, :cond_9

    .line 4148
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 4154
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :cond_8
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    if-gez v3, :cond_4

    .line 4161
    :cond_9
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 4162
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 4163
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    .line 4164
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    .line 4165
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    .line 4166
    iput-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 4167
    iput-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 4168
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    .line 4170
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkSelectionChanged()V

    goto :goto_0

    .line 4077
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleDragChange(I)V
    .locals 9
    .param p1, "delta"    # I

    .prologue
    .line 2568
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    .line 2569
    .local v6, "parent":Landroid/view/ViewParent;
    if-eqz v6, :cond_0

    .line 2570
    const/4 v7, 0x1

    invoke-interface {v6, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2574
    :cond_0
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    if-ltz v7, :cond_3

    .line 2575
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v1, v7, v8

    .line 2582
    .local v1, "motionIndex":I
    :goto_0
    const/4 v3, 0x0

    .line 2583
    .local v3, "motionViewPrevStart":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2584
    .local v2, "motionView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 2585
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v7, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 2588
    :cond_1
    :goto_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->scrollListItemsBy(I)Z

    move-result v0

    .line 2590
    .local v0, "atEdge":Z
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2591
    if-eqz v2, :cond_2

    .line 2592
    iget-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v7, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 2594
    .local v4, "motionViewRealStart":I
    :goto_2
    if-eqz v0, :cond_2

    .line 2595
    neg-int v7, p1

    sub-int v8, v4, v3

    sub-int v5, v7, v8

    .line 2596
    .local v5, "overscroll":I
    invoke-direct {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateOverScrollState(II)V

    .line 2599
    .end local v4    # "motionViewRealStart":I
    .end local v5    # "overscroll":I
    :cond_2
    return-void

    .line 2579
    .end local v0    # "atEdge":Z
    .end local v1    # "motionIndex":I
    .end local v2    # "motionView":Landroid/view/View;
    .end local v3    # "motionViewPrevStart":I
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    div-int/lit8 v1, v7, 0x2

    .restart local v1    # "motionIndex":I
    goto :goto_0

    .line 2585
    .restart local v2    # "motionView":Landroid/view/View;
    .restart local v3    # "motionViewPrevStart":I
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_1

    .line 2592
    .restart local v0    # "atEdge":Z
    :cond_5
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_2
.end method

.method private handleFocusWithinItem(I)Z
    .locals 7
    .param p1, "direction"    # I

    .prologue
    .line 1813
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidInnerFocusDirection(I)V

    .line 1815
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v3

    .line 1817
    .local v3, "numChildren":I
    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    if-eqz v5, :cond_1

    if-lez v3, :cond_1

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 1818
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v4

    .line 1820
    .local v4, "selectedView":Landroid/view/View;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v5

    if-eqz v5, :cond_1

    instance-of v5, v4, Landroid/view/ViewGroup;

    if-eqz v5, :cond_1

    .line 1822
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1823
    .local v0, "currentFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v5

    check-cast v4, Landroid/view/ViewGroup;

    .end local v4    # "selectedView":Landroid/view/View;
    invoke-virtual {v5, v4, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 1825
    .local v2, "nextFocus":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1827
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 1828
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1829
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1831
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, v5}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1832
    const/4 v5, 0x1

    .line 1849
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :goto_0
    return v5

    .line 1841
    .restart local v0    # "currentFocus":Landroid/view/View;
    .restart local v2    # "nextFocus":Landroid/view/View;
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getRootView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v6, v5, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1843
    .local v1, "globalNextFocus":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 1844
    invoke-direct {p0, v1, p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v5

    goto :goto_0

    .line 1849
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v1    # "globalNextFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private handleKeyEvent(IILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "count"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v3, 0x42

    const/16 v6, 0x21

    const/16 v7, 0x11

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2349
    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    if-nez v8, :cond_2

    :cond_0
    move v5, v4

    .line 2477
    :cond_1
    :goto_0
    return v5

    .line 2353
    :cond_2
    iget-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-eqz v8, :cond_3

    .line 2354
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 2357
    :cond_3
    const/4 v2, 0x0

    .line 2358
    .local v2, "handled":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 2360
    .local v0, "action":I
    if-eq v0, v5, :cond_4

    .line 2361
    sparse-switch p1, :sswitch_data_0

    .line 2446
    :cond_4
    :goto_1
    if-nez v2, :cond_1

    .line 2450
    packed-switch v0, :pswitch_data_0

    move v5, v4

    .line 2477
    goto :goto_0

    .line 2363
    :sswitch_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_5

    .line 2364
    invoke-direct {p0, p3, p2, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v2

    goto :goto_1

    .line 2365
    :cond_5
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2366
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleFocusWithinItem(I)Z

    move-result v2

    goto :goto_1

    .line 2371
    :sswitch_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_6

    .line 2372
    const/16 v3, 0x82

    invoke-direct {p0, p3, p2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v2

    goto :goto_1

    .line 2373
    :cond_6
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2374
    const/16 v3, 0x82

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleFocusWithinItem(I)Z

    move-result v2

    goto :goto_1

    .line 2380
    :sswitch_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v3, :cond_7

    .line 2381
    invoke-direct {p0, p3, p2, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v2

    goto :goto_1

    .line 2382
    :cond_7
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2383
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleFocusWithinItem(I)Z

    move-result v2

    goto :goto_1

    .line 2388
    :sswitch_3
    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v6, :cond_8

    .line 2389
    invoke-direct {p0, p3, p2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v2

    goto :goto_1

    .line 2390
    :cond_8
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2391
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleFocusWithinItem(I)Z

    move-result v2

    goto :goto_1

    .line 2397
    :sswitch_4
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2398
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v2

    .line 2399
    if-nez v2, :cond_4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_4

    .line 2400
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->keyPressed()V

    .line 2401
    const/4 v2, 0x1

    goto :goto_1

    .line 2407
    :sswitch_5
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2408
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v6

    if-nez v6, :cond_a

    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_9

    const/16 v3, 0x82

    :cond_9
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->pageScroll(I)Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_a
    move v2, v5

    .line 2413
    :cond_b
    :goto_2
    const/4 v2, 0x1

    .line 2414
    goto/16 :goto_1

    :cond_c
    move v2, v4

    .line 2408
    goto :goto_2

    .line 2409
    :cond_d
    invoke-static {p3, v5}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2410
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v3

    if-nez v3, :cond_e

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_f

    move v3, v6

    :goto_3
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_e
    move v2, v5

    :goto_4
    goto :goto_2

    :cond_f
    move v3, v7

    goto :goto_3

    :cond_10
    move v2, v4

    goto :goto_4

    .line 2417
    :sswitch_6
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2418
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v3

    if-nez v3, :cond_11

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_12

    :goto_5
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->pageScroll(I)Z

    move-result v3

    if-eqz v3, :cond_13

    :cond_11
    move v2, v5

    :goto_6
    goto/16 :goto_1

    :cond_12
    move v6, v7

    goto :goto_5

    :cond_13
    move v2, v4

    goto :goto_6

    .line 2419
    :cond_14
    const/4 v3, 0x2

    invoke-static {p3, v3}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2420
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v3

    if-nez v3, :cond_15

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_16

    :goto_7
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v3

    if-eqz v3, :cond_17

    :cond_15
    move v2, v5

    :goto_8
    goto/16 :goto_1

    :cond_16
    move v6, v7

    goto :goto_7

    :cond_17
    move v2, v4

    goto :goto_8

    .line 2425
    :sswitch_7
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 2426
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v6

    if-nez v6, :cond_19

    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_18

    const/16 v3, 0x82

    :cond_18
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->pageScroll(I)Z

    move-result v3

    if-eqz v3, :cond_1a

    :cond_19
    move v2, v5

    :goto_9
    goto/16 :goto_1

    :cond_1a
    move v2, v4

    goto :goto_9

    .line 2427
    :cond_1b
    const/4 v6, 0x2

    invoke-static {p3, v6}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2428
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v6

    if-nez v6, :cond_1d

    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_1c

    const/16 v3, 0x82

    :cond_1c
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v3

    if-eqz v3, :cond_1e

    :cond_1d
    move v2, v5

    :goto_a
    goto/16 :goto_1

    :cond_1e
    move v2, v4

    goto :goto_a

    .line 2433
    :sswitch_8
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2434
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v3

    if-nez v3, :cond_1f

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_20

    :goto_b
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v3

    if-eqz v3, :cond_21

    :cond_1f
    move v2, v5

    :goto_c
    goto/16 :goto_1

    :cond_20
    move v6, v7

    goto :goto_b

    :cond_21
    move v2, v4

    goto :goto_c

    .line 2439
    :sswitch_9
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2440
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v6

    if-nez v6, :cond_23

    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_22

    const/16 v3, 0x82

    :cond_22
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v3

    if-eqz v3, :cond_24

    :cond_23
    move v2, v5

    :goto_d
    goto/16 :goto_1

    :cond_24
    move v2, v4

    goto :goto_d

    .line 2452
    :pswitch_0
    invoke-super {p0, p1, p3}, Landroid/widget/AdapterView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v5

    goto/16 :goto_0

    .line 2455
    :pswitch_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2459
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_26

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_26

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ltz v3, :cond_26

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_26

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    if-ge v3, v6, :cond_26

    .line 2461
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v3, v6

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2462
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_25

    .line 2463
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget-wide v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    invoke-virtual {p0, v1, v3, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performItemClick(Landroid/view/View;IJ)Z

    .line 2464
    invoke-virtual {v1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 2467
    :cond_25
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    goto/16 :goto_0

    .end local v1    # "child":Landroid/view/View;
    :cond_26
    move v5, v4

    .line 2471
    goto/16 :goto_0

    .line 2474
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AdapterView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v5

    goto/16 :goto_0

    .line 2361
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x3e -> :sswitch_5
        0x42 -> :sswitch_4
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_8
        0x7b -> :sswitch_9
    .end sparse-switch

    .line 2450
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleKeyScroll(Landroid/view/KeyEvent;II)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "count"    # I
    .param p3, "direction"    # I

    .prologue
    .line 2328
    const/4 v1, 0x0

    .line 2330
    .local v1, "handled":Z
    invoke-static {p1}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2331
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v1

    .line 2332
    if-nez v1, :cond_2

    move v0, p2

    .line 2333
    .end local p2    # "count":I
    .local v0, "count":I
    :goto_0
    add-int/lit8 p2, v0, -0x1

    .end local v0    # "count":I
    .restart local p2    # "count":I
    if-lez v0, :cond_2

    .line 2334
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->arrowScroll(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2335
    const/4 v1, 0x1

    move v0, p2

    .end local p2    # "count":I
    .restart local v0    # "count":I
    goto :goto_0

    .line 2341
    .end local v0    # "count":I
    .restart local p2    # "count":I
    :cond_0
    const/4 v2, 0x2

    invoke-static {p1, v2}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2342
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelectionIfNeeded()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fullScroll(I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 2345
    :cond_2
    :goto_1
    return v1

    .line 2342
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleNewSelectionChange(Landroid/view/View;IIZ)V
    .locals 10
    .param p1, "selectedView"    # Landroid/view/View;
    .param p2, "direction"    # I
    .param p3, "newSelectedPosition"    # I
    .param p4, "newFocusAssigned"    # Z

    .prologue
    .line 1884
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 1886
    const/4 v8, -0x1

    if-ne p3, v8, :cond_0

    .line 1887
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "newSelectedPosition needs to be valid"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1894
    :cond_0
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v4, v8, v9

    .line 1895
    .local v4, "selectedIndex":I
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v2, p3, v8

    .line 1897
    .local v2, "nextSelectedIndex":I
    const/4 v7, 0x0

    .line 1901
    .local v7, "topSelected":Z
    const/16 v8, 0x21

    if-eq p2, v8, :cond_1

    const/16 v8, 0x11

    if-ne p2, v8, :cond_4

    .line 1902
    :cond_1
    move v6, v2

    .line 1903
    .local v6, "startViewIndex":I
    move v1, v4

    .line 1904
    .local v1, "endViewIndex":I
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1905
    .local v5, "startView":Landroid/view/View;
    move-object v0, p1

    .line 1906
    .local v0, "endView":Landroid/view/View;
    const/4 v7, 0x1

    .line 1914
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v3

    .line 1917
    .local v3, "numChildren":I
    if-eqz v5, :cond_2

    .line 1918
    if-nez p4, :cond_5

    if-eqz v7, :cond_5

    const/4 v8, 0x1

    :goto_1
    invoke-virtual {v5, v8}, Landroid/view/View;->setSelected(Z)V

    .line 1919
    invoke-direct {p0, v5, v6, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureAndAdjustDown(Landroid/view/View;II)V

    .line 1923
    :cond_2
    if-eqz v0, :cond_3

    .line 1924
    if-nez p4, :cond_6

    if-nez v7, :cond_6

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    .line 1925
    invoke-direct {p0, v0, v1, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureAndAdjustDown(Landroid/view/View;II)V

    .line 1927
    :cond_3
    return-void

    .line 1908
    .end local v0    # "endView":Landroid/view/View;
    .end local v1    # "endViewIndex":I
    .end local v3    # "numChildren":I
    .end local v5    # "startView":Landroid/view/View;
    .end local v6    # "startViewIndex":I
    :cond_4
    move v6, v4

    .line 1909
    .restart local v6    # "startViewIndex":I
    move v1, v2

    .line 1910
    .restart local v1    # "endViewIndex":I
    move-object v5, p1

    .line 1911
    .restart local v5    # "startView":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "endView":Landroid/view/View;
    goto :goto_0

    .line 1918
    .restart local v3    # "numChildren":I
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 1924
    :cond_6
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private handleOverScrollChange(I)V
    .locals 4
    .param p1, "delta"    # I

    .prologue
    .line 2638
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 2639
    .local v1, "oldOverScroll":I
    sub-int v0, v1, p1

    .line 2641
    .local v0, "newOverScroll":I
    neg-int v2, p1

    .line 2642
    .local v2, "overScrollDistance":I
    if-gez v0, :cond_0

    if-gez v1, :cond_1

    :cond_0
    if-lez v0, :cond_5

    if-gtz v1, :cond_5

    .line 2643
    :cond_1
    neg-int v2, v1

    .line 2644
    add-int/2addr p1, v2

    .line 2649
    :goto_0
    if-eqz v2, :cond_2

    .line 2650
    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateOverScrollState(II)V

    .line 2653
    :cond_2
    if-eqz p1, :cond_4

    .line 2654
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eqz v3, :cond_3

    .line 2655
    const/4 v3, 0x0

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 2656
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 2659
    :cond_3
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->scrollListItemsBy(I)Z

    .line 2660
    const/4 v3, 0x3

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 2664
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findClosestMotionRowOrColumn(I)I

    move-result v3

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    .line 2665
    const/4 v3, 0x0

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    .line 2667
    :cond_4
    return-void

    .line 2646
    :cond_5
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private hideSelector()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 3106
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-eq v0, v2, :cond_2

    .line 3107
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 3108
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 3111
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-eq v0, v1, :cond_1

    .line 3112
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 3115
    :cond_1
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 3116
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 3118
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 3120
    :cond_2
    return-void
.end method

.method private initOrResetVelocityTracker()V
    .locals 1

    .prologue
    .line 2482
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2483
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2487
    :goto_0
    return-void

    .line 2485
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    .prologue
    .line 2490
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2491
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2493
    :cond_0
    return-void
.end method

.method private invokeOnItemScrollListener()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2506
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2507
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;->onScroll(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;III)V

    .line 2511
    :cond_0
    invoke-virtual {p0, v4, v4, v4, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onScrollChanged(IIII)V

    .line 2512
    return-void
.end method

.method private isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 1689
    if-ne p1, p2, :cond_1

    .line 1695
    :cond_0
    :goto_0
    return v1

    .line 1693
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1695
    .local v0, "theParent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/view/View;

    .end local v0    # "theParent":Landroid/view/ViewParent;
    invoke-direct {p0, v0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keyPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 3172
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isClickable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 3212
    :cond_0
    :goto_0
    return-void

    .line 3176
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 3177
    .local v3, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    .line 3179
    .local v4, "selectorRect":Landroid/graphics/Rect;
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isFocused()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->touchModeDrawsInPressedState()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3181
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3183
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 3184
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3188
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 3191
    :cond_3
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 3193
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isLongClickable()Z

    move-result v2

    .line 3194
    .local v2, "longClickable":Z
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3195
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_4

    instance-of v5, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v5, :cond_4

    .line 3196
    if-eqz v2, :cond_6

    .line 3197
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 3203
    :cond_4
    :goto_1
    if-eqz v2, :cond_0

    iget-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v5, :cond_0

    .line 3204
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForKeyLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

    if-nez v5, :cond_5

    .line 3205
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForKeyLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

    .line 3208
    :cond_5
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForKeyLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->rememberWindowAttachCount()V

    .line 3209
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForKeyLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 3199
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_6
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1
.end method

.method private layoutChildren()V
    .locals 28

    .prologue
    .line 3611
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v2

    if-nez v2, :cond_1

    .line 3869
    :cond_0
    :goto_0
    return-void

    .line 3615
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3616
    .local v8, "blockLayoutRequests":Z
    if-nez v8, :cond_0

    .line 3617
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3623
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 3625
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_2

    .line 3626
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resetState()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3864
    if-nez v8, :cond_0

    .line 3865
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3866
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    goto :goto_0

    .line 3630
    :cond_2
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getStartEdge()I

    move-result v6

    .line 3631
    .local v6, "start":I
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v7

    .line 3633
    .local v7, "end":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v10

    .line 3634
    .local v10, "childCount":I
    const/16 v19, 0x0

    .line 3635
    .local v19, "index":I
    const/4 v5, 0x0

    .line 3637
    .local v5, "delta":I
    const/4 v14, 0x0

    .line 3639
    .local v14, "focusLayoutRestoreView":Landroid/view/View;
    const/16 v25, 0x0

    .line 3640
    .local v25, "selected":Landroid/view/View;
    const/4 v3, 0x0

    .line 3641
    .local v3, "oldSelected":Landroid/view/View;
    const/4 v4, 0x0

    .line 3642
    .local v4, "newSelected":Landroid/view/View;
    const/16 v22, 0x0

    .line 3644
    .local v22, "oldFirstChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_0

    .line 3662
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v26, v0

    sub-int v19, v2, v26

    .line 3663
    if-ltz v19, :cond_3

    move/from16 v0, v19

    if-ge v0, v10, :cond_3

    .line 3664
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3668
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    .line 3670
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    if-ltz v2, :cond_4

    .line 3671
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v26, v0

    sub-int v5, v2, v26

    .line 3675
    :cond_4
    add-int v2, v19, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3678
    :cond_5
    :goto_1
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 3679
    .local v11, "dataChanged":Z
    if-eqz v11, :cond_6

    .line 3680
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleDataChanged()V

    .line 3685
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-nez v2, :cond_7

    .line 3686
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resetState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3864
    if-nez v8, :cond_0

    .line 3865
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3866
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    goto/16 :goto_0

    .line 3646
    .end local v11    # "dataChanged":Z
    :pswitch_1
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v26, v0

    sub-int v19, v2, v26

    .line 3647
    if-ltz v19, :cond_5

    move/from16 v0, v19

    if-ge v0, v10, :cond_5

    .line 3648
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    .line 3688
    .restart local v11    # "dataChanged":Z
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Landroid/widget/ListAdapter;->getCount()I

    move-result v26

    move/from16 v0, v26

    if-eq v2, v0, :cond_9

    .line 3689
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "The content of the adapter has changed but TwoWayView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in TwoWayView("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 3690
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getId()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ") with Adapter("

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v27, v0

    .line 3691
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ")]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3864
    .end local v3    # "oldSelected":Landroid/view/View;
    .end local v4    # "newSelected":Landroid/view/View;
    .end local v5    # "delta":I
    .end local v6    # "start":I
    .end local v7    # "end":I
    .end local v10    # "childCount":I
    .end local v11    # "dataChanged":Z
    .end local v14    # "focusLayoutRestoreView":Landroid/view/View;
    .end local v19    # "index":I
    .end local v22    # "oldFirstChild":Landroid/view/View;
    .end local v25    # "selected":Landroid/view/View;
    :catchall_0
    move-exception v2

    if-nez v8, :cond_8

    .line 3865
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3866
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    :cond_8
    throw v2

    .line 3694
    .restart local v3    # "oldSelected":Landroid/view/View;
    .restart local v4    # "newSelected":Landroid/view/View;
    .restart local v5    # "delta":I
    .restart local v6    # "start":I
    .restart local v7    # "end":I
    .restart local v10    # "childCount":I
    .restart local v11    # "dataChanged":Z
    .restart local v14    # "focusLayoutRestoreView":Landroid/view/View;
    .restart local v19    # "index":I
    .restart local v22    # "oldFirstChild":Landroid/view/View;
    .restart local v25    # "selected":Landroid/view/View;
    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 3697
    const/4 v13, 0x0

    .line 3701
    .local v13, "focusLayoutRestoreDirectChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 3702
    .local v12, "firstPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    move-object/from16 v24, v0

    .line 3704
    .local v24, "recycleBin":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;
    if-eqz v11, :cond_a

    .line 3705
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_2
    move/from16 v0, v18

    if-ge v0, v10, :cond_b

    .line 3706
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    add-int v26, v12, v18

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3705
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 3709
    .end local v18    # "i":I
    :cond_a
    move-object/from16 v0, v24

    invoke-virtual {v0, v10, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->fillActiveViews(II)V

    .line 3716
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFocusedChild()Landroid/view/View;

    move-result-object v17

    .line 3717
    .local v17, "focusedChild":Landroid/view/View;
    if-eqz v17, :cond_d

    .line 3720
    if-nez v11, :cond_c

    .line 3721
    move-object/from16 v13, v17

    .line 3724
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findFocus()Landroid/view/View;

    move-result-object v14

    .line 3725
    if-eqz v14, :cond_c

    .line 3727
    invoke-virtual {v14}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 3731
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestFocus()Z

    .line 3738
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->detachAllViewsFromParent()V

    .line 3740
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_1

    .line 3776
    if-nez v10, :cond_16

    .line 3777
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(I)I

    move-result v23

    .line 3778
    .local v23, "position":I
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 3779
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillFromOffset(I)Landroid/view/View;

    move-result-object v25

    .line 3803
    .end local v23    # "position":I
    :goto_3
    invoke-virtual/range {v24 .. v24}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->scrapActiveViews()V

    .line 3805
    if-eqz v25, :cond_21

    .line 3806
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    if-eqz v2, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_1f

    .line 3807
    move-object/from16 v0, v25

    if-ne v0, v13, :cond_e

    if-eqz v14, :cond_e

    invoke-virtual {v14}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-nez v2, :cond_f

    :cond_e
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-eqz v2, :cond_1d

    :cond_f
    const/4 v15, 0x1

    .line 3809
    .local v15, "focusWasTaken":Z
    :goto_4
    if-nez v15, :cond_1e

    .line 3813
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFocusedChild()Landroid/view/View;

    move-result-object v16

    .line 3814
    .local v16, "focused":Landroid/view/View;
    if-eqz v16, :cond_10

    .line 3815
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->clearFocus()V

    .line 3818
    :cond_10
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    .line 3827
    .end local v15    # "focusWasTaken":Z
    .end local v16    # "focused":Landroid/view/View;
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_20

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getTop()I

    move-result v2

    :goto_6
    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 3849
    :cond_11
    :goto_7
    if-eqz v14, :cond_12

    invoke-virtual {v14}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 3850
    invoke-virtual {v14}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 3853
    :cond_12
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 3854
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 3855
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 3857
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 3858
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lez v2, :cond_13

    .line 3859
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkSelectionChanged()V

    .line 3862
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3864
    if-nez v8, :cond_0

    .line 3865
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 3866
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    goto/16 :goto_0

    .line 3742
    :pswitch_2
    if-eqz v4, :cond_15

    .line 3743
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_14

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v20

    .line 3745
    .local v20, "newSelectedStart":I
    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillFromSelection(III)Landroid/view/View;

    move-result-object v25

    .line 3746
    goto/16 :goto_3

    .line 3743
    .end local v20    # "newSelectedStart":I
    :cond_14
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v20

    goto :goto_8

    .line 3747
    :cond_15
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillFromMiddle(II)Landroid/view/View;

    move-result-object v25

    .line 3750
    goto/16 :goto_3

    .line 3753
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillSpecific(II)Landroid/view/View;

    move-result-object v25

    .line 3754
    goto/16 :goto_3

    .line 3757
    :pswitch_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    move-result-object v25

    .line 3758
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    goto/16 :goto_3

    .line 3762
    :pswitch_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 3763
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillFromOffset(I)Landroid/view/View;

    move-result-object v25

    .line 3764
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    goto/16 :goto_3

    .line 3768
    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reconcileSelectedPosition()I

    move-result v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillSpecific(II)Landroid/view/View;

    move-result-object v25

    .line 3769
    goto/16 :goto_3

    :pswitch_7
    move-object/from16 v2, p0

    .line 3772
    invoke-direct/range {v2 .. v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;

    move-result-object v25

    .line 3773
    goto/16 :goto_3

    .line 3781
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ltz v2, :cond_19

    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v2, v0, :cond_19

    .line 3782
    move/from16 v21, v6

    .line 3783
    .local v21, "offset":I
    if-eqz v3, :cond_17

    .line 3784
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_18

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v21

    .line 3786
    :cond_17
    :goto_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillSpecific(II)Landroid/view/View;

    move-result-object v25

    .line 3787
    goto/16 :goto_3

    .line 3784
    :cond_18
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v21

    goto :goto_9

    .line 3787
    .end local v21    # "offset":I
    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v2, v0, :cond_1c

    .line 3788
    move/from16 v21, v6

    .line 3789
    .restart local v21    # "offset":I
    if-eqz v22, :cond_1a

    .line 3790
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_1b

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getTop()I

    move-result v21

    .line 3793
    :cond_1a
    :goto_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillSpecific(II)Landroid/view/View;

    move-result-object v25

    .line 3794
    goto/16 :goto_3

    .line 3790
    :cond_1b
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLeft()I

    move-result v21

    goto :goto_a

    .line 3795
    .end local v21    # "offset":I
    :cond_1c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillSpecific(II)Landroid/view/View;

    move-result-object v25

    goto/16 :goto_3

    .line 3807
    :cond_1d
    const/4 v15, 0x0

    goto/16 :goto_4

    .line 3820
    .restart local v15    # "focusWasTaken":Z
    :cond_1e
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 3821
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_5

    .line 3824
    .end local v15    # "focusWasTaken":Z
    :cond_1f
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_5

    .line 3827
    :cond_20
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLeft()I

    move-result v2

    goto/16 :goto_6

    .line 3829
    :cond_21
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    if-lez v2, :cond_23

    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v2, v0, :cond_23

    .line 3830
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v26, v0

    sub-int v2, v2, v26

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 3832
    .local v9, "child":Landroid/view/View;
    if-eqz v9, :cond_22

    .line 3833
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    .line 3842
    .end local v9    # "child":Landroid/view/View;
    :cond_22
    :goto_b
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_11

    if-eqz v14, :cond_11

    .line 3843
    invoke-virtual {v14}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_7

    .line 3836
    :cond_23
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 3837
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_b

    .line 3644
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 3740
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method private lookForSelectablePosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3278
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v0

    return v0
.end method

.method private lookForSelectablePosition(IZ)I
    .locals 4
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    const/4 v2, -0x1

    .line 3282
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3283
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3311
    :cond_0
    :goto_0
    return v2

    .line 3287
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 3288
    .local v1, "itemCount":I
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAreAllItemsSelectable:Z

    if-nez v3, :cond_4

    .line 3289
    if-eqz p2, :cond_2

    .line 3290
    const/4 v3, 0x0

    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 3291
    :goto_1
    if-ge p1, v1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3292
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 3295
    :cond_2
    add-int/lit8 v3, v1, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 3296
    :goto_2
    if-ltz p1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3297
    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    .line 3301
    :cond_3
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 3305
    goto :goto_0

    .line 3307
    :cond_4
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 3311
    goto :goto_0
.end method

.method private lookForSelectablePositionOnScreen(I)I
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v6, -0x1

    .line 3321
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 3323
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 3324
    .local v1, "firstPosition":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 3326
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/16 v7, 0x82

    if-eq p1, v7, :cond_0

    const/16 v7, 0x42

    if-ne p1, v7, :cond_6

    .line 3327
    :cond_0
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-eq v7, v6, :cond_2

    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    add-int/lit8 v5, v7, 0x1

    .line 3329
    .local v5, "startPos":I
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_3

    move v4, v6

    .line 3364
    :cond_1
    :goto_1
    return v4

    .end local v5    # "startPos":I
    :cond_2
    move v5, v1

    .line 3327
    goto :goto_0

    .line 3333
    .restart local v5    # "startPos":I
    :cond_3
    if-ge v5, v1, :cond_4

    .line 3334
    move v5, v1

    .line 3337
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getLastVisiblePosition()I

    move-result v3

    .line 3339
    .local v3, "lastVisiblePos":I
    move v4, v5

    .local v4, "pos":I
    :goto_2
    if-gt v4, v3, :cond_c

    .line 3340
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_5

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 3339
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 3345
    .end local v3    # "lastVisiblePos":I
    .end local v4    # "pos":I
    .end local v5    # "startPos":I
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v2, v7, -0x1

    .line 3347
    .local v2, "last":I
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-eq v7, v6, :cond_8

    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    add-int/lit8 v5, v7, -0x1

    .line 3349
    .restart local v5    # "startPos":I
    :goto_3
    if-ltz v5, :cond_7

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_9

    :cond_7
    move v4, v6

    .line 3350
    goto :goto_1

    .line 3347
    .end local v5    # "startPos":I
    :cond_8
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v5, v7, -0x1

    goto :goto_3

    .line 3353
    .restart local v5    # "startPos":I
    :cond_9
    if-le v5, v2, :cond_a

    .line 3354
    move v5, v2

    .line 3357
    :cond_a
    move v4, v5

    .restart local v4    # "pos":I
    :goto_4
    if-lt v4, v1, :cond_c

    .line 3358
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_b

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 3357
    :cond_b
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .end local v2    # "last":I
    :cond_c
    move v4, v6

    .line 3364
    goto :goto_1
.end method

.method private makeAndAddView(IIZZ)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "offset"    # I
    .param p3, "flow"    # Z
    .param p4, "selected"    # Z

    .prologue
    .line 4501
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    .line 4502
    move v3, p2

    .line 4503
    .local v3, "top":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v4

    .line 4509
    .local v4, "left":I
    :goto_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v0, :cond_1

    .line 4511
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->getActiveView(I)Landroid/view/View;

    move-result-object v1

    .line 4512
    .local v1, "activeChild":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 4515
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setupChild(Landroid/view/View;IIIZZZ)V

    .line 4527
    .end local v1    # "activeChild":Landroid/view/View;
    :goto_1
    return-object v1

    .line 4505
    .end local v3    # "top":I
    .end local v4    # "left":I
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v3

    .line 4506
    .restart local v3    # "top":I
    move v4, p2

    .restart local v4    # "left":I
    goto :goto_0

    .line 4522
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v6

    .line 4525
    .local v6, "child":Landroid/view/View;
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    const/4 v2, 0x0

    aget-boolean v12, v0, v2

    move-object v5, p0

    move v7, p1

    move v8, v3

    move v9, v4

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {v5 .. v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setupChild(Landroid/view/View;IIIZZZ)V

    move-object v1, v6

    .line 4527
    goto :goto_1
.end method

.method private maybeScroll(I)V
    .locals 2
    .param p1, "delta"    # I

    .prologue
    .line 2558
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2559
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleDragChange(I)V

    .line 2563
    :cond_0
    :goto_0
    return-void

    .line 2560
    :cond_1
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2561
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleOverScrollChange(I)V

    goto :goto_0
.end method

.method private maybeStartScrolling(I)Z
    .locals 7
    .param p1, "delta"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2526
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eqz v5, :cond_0

    move v0, v3

    .line 2527
    .local v0, "isOverScroll":Z
    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchSlop:I

    if-gt v5, v6, :cond_1

    if-nez v0, :cond_1

    .line 2554
    :goto_1
    return v4

    .end local v0    # "isOverScroll":Z
    :cond_0
    move v0, v4

    .line 2526
    goto :goto_0

    .line 2531
    .restart local v0    # "isOverScroll":Z
    :cond_1
    if-eqz v0, :cond_4

    .line 2532
    const/4 v5, 0x5

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 2539
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 2540
    .local v2, "parent":Landroid/view/ViewParent;
    if-eqz v2, :cond_2

    .line 2541
    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2544
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForLongPress()V

    .line 2546
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 2547
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2548
    .local v1, "motionView":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 2549
    invoke-virtual {v1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 2552
    :cond_3
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    move v4, v3

    .line 2554
    goto :goto_1

    .line 2534
    .end local v1    # "motionView":Landroid/view/View;
    .end local v2    # "parent":Landroid/view/ViewParent;
    :cond_4
    const/4 v5, 0x3

    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    goto :goto_2
.end method

.method private measureAndAdjustDown(Landroid/view/View;II)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "childIndex"    # I
    .param p3, "numChildren"    # I

    .prologue
    .line 1937
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1938
    .local v2, "oldHeight":I
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureChild(Landroid/view/View;)V

    .line 1940
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 1952
    :cond_0
    return-void

    .line 1945
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->relayoutMeasuredChild(Landroid/view/View;)V

    .line 1948
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v0, v3, v2

    .line 1949
    .local v0, "heightDelta":I
    add-int/lit8 v1, p2, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 1950
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1949
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private measureChild(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 4301
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureChild(Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)V

    .line 4302
    return-void
.end method

.method private measureChild(Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "lp"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .prologue
    .line 4305
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildWidthMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I

    move-result v1

    .line 4306
    .local v1, "widthSpec":I
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildHeightMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I

    move-result v0

    .line 4307
    .local v0, "heightSpec":I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 4308
    return-void
.end method

.method private measureHeightOfChildren(IIIII)I
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxHeight"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 4360
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v7

    .line 4361
    .local v7, "paddingTop":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v6

    .line 4363
    .local v6, "paddingBottom":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 4364
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 4365
    add-int v8, v7, v6

    .line 4418
    :cond_0
    :goto_0
    return v8

    .line 4369
    :cond_1
    add-int v10, v7, v6

    .line 4370
    .local v10, "returnedHeight":I
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 4374
    .local v5, "itemMargin":I
    const/4 v8, 0x0

    .line 4379
    .local v8, "prevHeightWithoutPartialChild":I
    const/4 v12, -0x1

    move/from16 v0, p3

    if-ne v0, v12, :cond_2

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    add-int/lit8 p3, v12, -0x1

    .line 4380
    :cond_2
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    .line 4381
    .local v9, "recycleBin":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleOnMeasure()Z

    move-result v11

    .line 4382
    .local v11, "shouldRecycle":Z
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    .line 4384
    .local v4, "isScrap":[Z
    move v3, p2

    .local v3, "i":I
    :goto_1
    move/from16 v0, p3

    if-gt v3, v0, :cond_8

    .line 4385
    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v2

    .line 4387
    .local v2, "child":Landroid/view/View;
    invoke-direct {p0, v2, v3, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureScrapChild(Landroid/view/View;II)V

    .line 4389
    if-lez v3, :cond_3

    .line 4391
    add-int/2addr v10, v5

    .line 4395
    :cond_3
    if-eqz v11, :cond_4

    .line 4396
    const/4 v12, -0x1

    invoke-virtual {v9, v2, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4399
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v10, v12

    .line 4401
    move/from16 v0, p4

    if-lt v10, v0, :cond_6

    .line 4404
    if-ltz p5, :cond_5

    move/from16 v0, p5

    if-le v3, v0, :cond_5

    if-lez v8, :cond_5

    move/from16 v0, p4

    if-ne v10, v0, :cond_0

    :cond_5
    move/from16 v8, p4

    goto :goto_0

    .line 4411
    :cond_6
    if-ltz p5, :cond_7

    move/from16 v0, p5

    if-lt v3, v0, :cond_7

    .line 4412
    move v8, v10

    .line 4384
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v2    # "child":Landroid/view/View;
    :cond_8
    move v8, v10

    .line 4418
    goto :goto_0
.end method

.method private measureScrapChild(Landroid/view/View;II)V
    .locals 4
    .param p1, "scrapChild"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "secondaryMeasureSpec"    # I

    .prologue
    .line 4323
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 4324
    .local v1, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    if-nez v1, :cond_0

    .line 4325
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v1

    .line 4326
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4329
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v3

    iput v3, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->viewType:I

    .line 4330
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->forceAdd:Z

    .line 4334
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 4335
    move v2, p3

    .line 4336
    .local v2, "widthMeasureSpec":I
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildHeightMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I

    move-result v0

    .line 4342
    .local v0, "heightMeasureSpec":I
    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 4343
    return-void

    .line 4338
    .end local v0    # "heightMeasureSpec":I
    .end local v2    # "widthMeasureSpec":I
    :cond_1
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildWidthMeasureSpec(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)I

    move-result v2

    .line 4339
    .restart local v2    # "widthMeasureSpec":I
    move v0, p3

    .restart local v0    # "heightMeasureSpec":I
    goto :goto_0
.end method

.method private measureWidthOfChildren(IIIII)I
    .locals 13
    .param p1, "heightMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxWidth"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 4436
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v6

    .line 4437
    .local v6, "paddingLeft":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v7

    .line 4439
    .local v7, "paddingRight":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 4440
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 4441
    add-int v8, v6, v7

    .line 4494
    :cond_0
    :goto_0
    return v8

    .line 4445
    :cond_1
    add-int v10, v6, v7

    .line 4446
    .local v10, "returnedWidth":I
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 4450
    .local v5, "itemMargin":I
    const/4 v8, 0x0

    .line 4455
    .local v8, "prevWidthWithoutPartialChild":I
    const/4 v12, -0x1

    move/from16 v0, p3

    if-ne v0, v12, :cond_2

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    add-int/lit8 p3, v12, -0x1

    .line 4456
    :cond_2
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    .line 4457
    .local v9, "recycleBin":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleOnMeasure()Z

    move-result v11

    .line 4458
    .local v11, "shouldRecycle":Z
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    .line 4460
    .local v4, "isScrap":[Z
    move v3, p2

    .local v3, "i":I
    :goto_1
    move/from16 v0, p3

    if-gt v3, v0, :cond_8

    .line 4461
    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v2

    .line 4463
    .local v2, "child":Landroid/view/View;
    invoke-direct {p0, v2, v3, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureScrapChild(Landroid/view/View;II)V

    .line 4465
    if-lez v3, :cond_3

    .line 4467
    add-int/2addr v10, v5

    .line 4471
    :cond_3
    if-eqz v11, :cond_4

    .line 4472
    const/4 v12, -0x1

    invoke-virtual {v9, v2, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4475
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v10, v12

    .line 4477
    move/from16 v0, p4

    if-lt v10, v0, :cond_6

    .line 4480
    if-ltz p5, :cond_5

    move/from16 v0, p5

    if-le v3, v0, :cond_5

    if-lez v8, :cond_5

    move/from16 v0, p4

    if-ne v10, v0, :cond_0

    :cond_5
    move/from16 v8, p4

    goto :goto_0

    .line 4487
    :cond_6
    if-ltz p5, :cond_7

    move/from16 v0, p5

    if-lt v3, v0, :cond_7

    .line 4488
    move v8, v10

    .line 4460
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v2    # "child":Landroid/view/View;
    :cond_8
    move v8, v10

    .line 4494
    goto :goto_0
.end method

.method private moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    .locals 20
    .param p1, "oldSelected"    # Landroid/view/View;
    .param p2, "newSelected"    # Landroid/view/View;
    .param p3, "delta"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I

    .prologue
    .line 3890
    move-object/from16 v0, p0

    iget v13, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 3892
    .local v13, "selectedPosition":I
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v10

    .line 3893
    .local v10, "oldSelectedStart":I
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v9

    .line 3895
    .local v9, "oldSelectedEnd":I
    const/4 v11, 0x0

    .line 3897
    .local v11, "selected":Landroid/view/View;
    if-lez p3, :cond_2

    .line 3909
    add-int/lit8 v17, v13, -0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object p1

    .line 3911
    move-object/from16 v0, p0

    iget v5, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 3914
    .local v5, "itemMargin":I
    add-int v17, v9, v5

    const/16 v18, 0x1

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 3916
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v14

    .line 3917
    .local v14, "selectedStart":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v12

    .line 3920
    .local v12, "selectedEnd":I
    move/from16 v0, p5

    if-le v12, v0, :cond_0

    .line 3922
    sub-int v16, v14, p4

    .line 3925
    .local v16, "spaceBefore":I
    sub-int v15, v12, p5

    .line 3928
    .local v15, "spaceAfter":I
    sub-int v17, p5, p4

    div-int/lit8 v4, v17, 0x2

    .line 3929
    .local v4, "halfSpace":I
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 3930
    .local v8, "offset":I
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 3932
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 3933
    neg-int v0, v8

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 3934
    neg-int v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 3942
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x2

    sub-int v18, v14, v5

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    .line 3943
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->adjustViewsStartOrEnd()V

    .line 3944
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    add-int v18, v12, v5

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    .line 4020
    .end local v5    # "itemMargin":I
    :goto_1
    return-object v11

    .line 3936
    .restart local v4    # "halfSpace":I
    .restart local v5    # "itemMargin":I
    .restart local v8    # "offset":I
    .restart local v15    # "spaceAfter":I
    .restart local v16    # "spaceBefore":I
    :cond_1
    neg-int v0, v8

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 3937
    neg-int v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0

    .line 3945
    .end local v4    # "halfSpace":I
    .end local v5    # "itemMargin":I
    .end local v8    # "offset":I
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_2
    if-gez p3, :cond_7

    .line 3956
    if-eqz p2, :cond_5

    .line 3958
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v7

    .line 3959
    .local v7, "newSelectedStart":I
    :goto_2
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v7, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 3966
    .end local v7    # "newSelectedStart":I
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v14

    .line 3967
    .restart local v14    # "selectedStart":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v12

    .line 3970
    .restart local v12    # "selectedEnd":I
    move/from16 v0, p4

    if-ge v14, v0, :cond_3

    .line 3972
    sub-int v16, p4, v14

    .line 3975
    .restart local v16    # "spaceBefore":I
    sub-int v15, p5, v12

    .line 3978
    .restart local v15    # "spaceAfter":I
    sub-int v17, p5, p4

    div-int/lit8 v4, v17, 0x2

    .line 3979
    .restart local v4    # "halfSpace":I
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 3980
    .restart local v8    # "offset":I
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 3982
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 3983
    invoke-virtual {v11, v8}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 3990
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto :goto_1

    .line 3958
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v7

    goto :goto_2

    .line 3963
    :cond_5
    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v10, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    goto :goto_3

    .line 3985
    .restart local v4    # "halfSpace":I
    .restart local v8    # "offset":I
    .restart local v12    # "selectedEnd":I
    .restart local v14    # "selectedStart":I
    .restart local v15    # "spaceAfter":I
    .restart local v16    # "spaceBefore":I
    :cond_6
    invoke-virtual {v11, v8}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_4

    .line 3996
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_7
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v10, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 3998
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v14

    .line 3999
    .restart local v14    # "selectedStart":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v12

    .line 4002
    .restart local v12    # "selectedEnd":I
    move/from16 v0, p4

    if-ge v10, v0, :cond_8

    .line 4005
    move v6, v12

    .line 4006
    .local v6, "newEnd":I
    add-int/lit8 v17, p4, 0x14

    move/from16 v0, v17

    if-ge v6, v0, :cond_8

    .line 4008
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    .line 4009
    sub-int v17, p4, v14

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4017
    .end local v6    # "newEnd":I
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 4011
    .restart local v6    # "newEnd":I
    :cond_9
    sub-int v17, p4, v14

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_5
.end method

.method private obtainView(I[Z)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "isScrap"    # [Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5069
    aput-boolean v4, p2, v4

    .line 5071
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    move-result-object v2

    .line 5072
    .local v2, "scrapView":Landroid/view/View;
    if-eqz v2, :cond_0

    move-object v0, v2

    .line 5115
    :goto_0
    return-object v0

    .line 5076
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v2

    .line 5079
    if-eqz v2, :cond_6

    .line 5080
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 5082
    .local v0, "child":Landroid/view/View;
    if-eq v0, v2, :cond_5

    .line 5083
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v3, v2, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 5091
    :goto_1
    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->getImportantForAccessibility(Landroid/view/View;)I

    move-result v3

    if-nez v3, :cond_1

    .line 5092
    invoke-static {v0, v5}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 5095
    :cond_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mHasStableIds:Z

    if-eqz v3, :cond_3

    .line 5096
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 5098
    .local v1, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    if-nez v1, :cond_7

    .line 5099
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v1

    .line 5104
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->id:J

    .line 5106
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5109
    .end local v1    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAccessibilityDelegate:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;

    if-nez v3, :cond_4

    .line 5110
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;

    invoke-direct {v3, p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAccessibilityDelegate:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;

    .line 5113
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAccessibilityDelegate:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ListItemAccessibilityDelegate;

    invoke-static {v0, v3}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    goto :goto_0

    .line 5085
    :cond_5
    aput-boolean v5, p2, v4

    goto :goto_1

    .line 5088
    .end local v0    # "child":Landroid/view/View;
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "child":Landroid/view/View;
    goto :goto_1

    .line 5100
    .restart local v1    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    :cond_7
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5101
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v1

    goto :goto_2
.end method

.method private offsetChildren(I)V
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 3876
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 3878
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 3879
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3881
    .local v0, "child":Landroid/view/View;
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_0

    .line 3882
    invoke-virtual {v0, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 3878
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3884
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_1

    .line 3887
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private overScrollByInternal(IIIIIIIIZ)Z
    .locals 2
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I
    .param p3, "scrollX"    # I
    .param p4, "scrollY"    # I
    .param p5, "scrollRangeX"    # I
    .param p6, "scrollRangeY"    # I
    .param p7, "maxOverScrollX"    # I
    .param p8, "maxOverScrollY"    # I
    .param p9, "isTouchEvent"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 1029
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 1030
    const/4 v0, 0x0

    .line 1033
    :goto_0
    return v0

    :cond_0
    invoke-super/range {p0 .. p9}, Landroid/widget/AdapterView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    goto :goto_0
.end method

.method private performAccessibilityActionsOnSelected()V
    .locals 2

    .prologue
    .line 3270
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v0

    .line 3271
    .local v0, "position":I
    if-ltz v0, :cond_0

    .line 3273
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->sendAccessibilityEvent(I)V

    .line 3275
    :cond_0
    return-void
.end method

.method private performLongPress(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "longPressPosition"    # I
    .param p3, "longPressId"    # J

    .prologue
    .line 5253
    const/4 v6, 0x0

    .line 5255
    .local v6, "handled":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    .line 5256
    .local v0, "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    .line 5257
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 5260
    :cond_0
    if-nez v6, :cond_1

    .line 5261
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 5262
    invoke-super {p0, p0}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 5265
    :cond_1
    if-eqz v6, :cond_2

    .line 5266
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performHapticFeedback(I)Z

    .line 5269
    :cond_2
    return v6
.end method

.method private positionOfNewFocus(Landroid/view/View;)I
    .locals 5
    .param p1, "newFocus"    # Landroid/view/View;

    .prologue
    .line 2063
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v2

    .line 2065
    .local v2, "numChildren":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2066
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2067
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2068
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v3, v1

    return v3

    .line 2065
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2072
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "newFocus is not a child of any of the children of the list!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private positionSelector(ILandroid/view/View;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "selected"    # Landroid/view/View;

    .prologue
    const/4 v6, -0x1

    .line 3089
    if-eq p1, v6, :cond_0

    .line 3090
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    .line 3093
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3095
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsChildViewEnabled:Z

    .line 3096
    .local v0, "isChildViewEnabled":Z
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eq v1, v0, :cond_1

    .line 3097
    if-nez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsChildViewEnabled:Z

    .line 3099
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v1

    if-eq v1, v6, :cond_1

    .line 3100
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->refreshDrawableState()V

    .line 3103
    :cond_1
    return-void

    .line 3097
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private reconcileSelectedPosition()I
    .locals 2

    .prologue
    .line 4174
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 4175
    .local v0, "position":I
    if-gez v0, :cond_0

    .line 4176
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 4179
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 4180
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4182
    return v0
.end method

.method private recycleVelocityTracker()V
    .locals 1

    .prologue
    .line 2496
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2497
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2500
    :cond_0
    return-void
.end method

.method private relayoutMeasuredChild(Landroid/view/View;)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 4311
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 4312
    .local v5, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 4314
    .local v4, "h":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v1

    .line 4315
    .local v1, "childLeft":I
    add-int v2, v1, v5

    .line 4316
    .local v2, "childRight":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4317
    .local v3, "childTop":I
    add-int v0, v3, v4

    .line 4319
    .local v0, "childBottom":I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 4320
    return-void
.end method

.method private rememberSyncState()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5141
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 5177
    :goto_0
    return-void

    .line 5145
    :cond_0
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 5147
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ltz v2, :cond_3

    .line 5148
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5150
    .local v1, "child":Landroid/view/View;
    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 5151
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 5153
    if-eqz v1, :cond_1

    .line 5154
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    :goto_1
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 5157
    :cond_1
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    goto :goto_0

    .line 5154
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_1

    .line 5160
    .end local v1    # "child":Landroid/view/View;
    :cond_3
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5161
    .restart local v1    # "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 5163
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-ltz v2, :cond_5

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 5164
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 5169
    :goto_2
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 5171
    if-eqz v1, :cond_4

    .line 5172
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 5175
    :cond_4
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    goto :goto_0

    .line 5166
    :cond_5
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    goto :goto_2
.end method

.method private reportScrollStateChange(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 2515
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastScrollState:I

    if-ne p1, v0, :cond_1

    .line 2523
    :cond_0
    :goto_0
    return-void

    .line 2519
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2520
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastScrollState:I

    .line 2521
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;->onScrollStateChanged(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)V

    goto :goto_0
.end method

.method private scrollListItemsBy(I)Z
    .locals 32
    .param p1, "incrementalDelta"    # I

    .prologue
    .line 2840
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    .line 2841
    .local v7, "childCount":I
    if-nez v7, :cond_0

    .line 2842
    const/16 v30, 0x1

    .line 2962
    :goto_0
    return v30

    .line 2845
    :cond_0
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v17

    .line 2846
    .local v17, "firstStart":I
    add-int/lit8 v30, v7, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v20

    .line 2848
    .local v20, "lastEnd":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v25

    .line 2849
    .local v25, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v21

    .line 2850
    .local v21, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v22

    .line 2851
    .local v22, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v23

    .line 2853
    .local v23, "paddingRight":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_2

    move/from16 v24, v25

    .line 2855
    .local v24, "paddingStart":I
    :goto_1
    sub-int v28, v24, v17

    .line 2856
    .local v28, "spaceBefore":I
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getEndEdge()I

    move-result v15

    .line 2857
    .local v15, "end":I
    sub-int v27, v20, v15

    .line 2860
    .local v27, "spaceAfter":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_3

    .line 2861
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v30

    sub-int v30, v30, v21

    sub-int v26, v30, v25

    .line 2866
    .local v26, "size":I
    :goto_2
    if-gez p1, :cond_4

    .line 2867
    add-int/lit8 v30, v26, -0x1

    move/from16 v0, v30

    neg-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2872
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v16, v0

    .line 2874
    .local v16, "firstPosition":I
    if-nez v16, :cond_5

    move/from16 v0, v17

    move/from16 v1, v24

    if-lt v0, v1, :cond_5

    if-ltz p1, :cond_5

    const/4 v4, 0x1

    .line 2875
    .local v4, "cannotScrollDown":Z
    :goto_4
    add-int v30, v16, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    move/from16 v31, v0

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_6

    move/from16 v0, v20

    if-gt v0, v15, :cond_6

    if-gtz p1, :cond_6

    const/4 v5, 0x1

    .line 2877
    .local v5, "cannotScrollUp":Z
    :goto_5
    if-nez v4, :cond_1

    if-eqz v5, :cond_8

    .line 2878
    :cond_1
    if-eqz p1, :cond_7

    const/16 v30, 0x1

    goto/16 :goto_0

    .end local v4    # "cannotScrollDown":Z
    .end local v5    # "cannotScrollUp":Z
    .end local v15    # "end":I
    .end local v16    # "firstPosition":I
    .end local v24    # "paddingStart":I
    .end local v26    # "size":I
    .end local v27    # "spaceAfter":I
    .end local v28    # "spaceBefore":I
    :cond_2
    move/from16 v24, v22

    .line 2853
    goto :goto_1

    .line 2863
    .restart local v15    # "end":I
    .restart local v24    # "paddingStart":I
    .restart local v27    # "spaceAfter":I
    .restart local v28    # "spaceBefore":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v30

    sub-int v30, v30, v23

    sub-int v26, v30, v22

    .restart local v26    # "size":I
    goto :goto_2

    .line 2869
    :cond_4
    add-int/lit8 v30, v26, -0x1

    move/from16 v0, v30

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_3

    .line 2874
    .restart local v16    # "firstPosition":I
    :cond_5
    const/4 v4, 0x0

    goto :goto_4

    .line 2875
    .restart local v4    # "cannotScrollDown":Z
    :cond_6
    const/4 v5, 0x0

    goto :goto_5

    .line 2878
    .restart local v5    # "cannotScrollUp":Z
    :cond_7
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 2881
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v19

    .line 2882
    .local v19, "inTouchMode":Z
    if-eqz v19, :cond_9

    .line 2883
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hideSelector()V

    .line 2886
    :cond_9
    const/16 v29, 0x0

    .line 2887
    .local v29, "start":I
    const/4 v13, 0x0

    .line 2889
    .local v13, "count":I
    if-gez p1, :cond_11

    const/4 v14, 0x1

    .line 2890
    .local v14, "down":Z
    :goto_6
    if-eqz v14, :cond_13

    .line 2891
    move/from16 v0, p1

    neg-int v0, v0

    move/from16 v30, v0

    add-int v12, v30, v24

    .line 2893
    .local v12, "childrenStart":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_7
    move/from16 v0, v18

    if-ge v0, v7, :cond_a

    .line 2894
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2895
    .local v6, "child":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v8

    .line 2897
    .local v8, "childEnd":I
    if-lt v8, v12, :cond_12

    .line 2921
    .end local v6    # "child":Landroid/view/View;
    .end local v8    # "childEnd":I
    .end local v12    # "childrenStart":I
    :cond_a
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 2923
    if-lez v13, :cond_b

    .line 2924
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1, v13}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->detachViewsFromParent(II)V

    .line 2929
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    move-result v30

    if-nez v30, :cond_c

    .line 2930
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 2933
    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetChildren(I)V

    .line 2935
    if-eqz v14, :cond_d

    .line 2936
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v30, v0

    add-int v30, v30, v13

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 2939
    :cond_d
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2940
    .local v3, "absIncrementalDelta":I
    move/from16 v0, v28

    if-lt v0, v3, :cond_e

    move/from16 v0, v27

    if-ge v0, v3, :cond_f

    .line 2941
    :cond_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillGap(Z)V

    .line 2944
    :cond_f
    if-nez v19, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v30, v0

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_14

    .line 2945
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v31, v0

    sub-int v9, v30, v31

    .line 2946
    .local v9, "childIndex":I
    if-ltz v9, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v30

    move/from16 v0, v30

    if-ge v9, v0, :cond_10

    .line 2947
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    .line 2958
    .end local v9    # "childIndex":I
    :cond_10
    :goto_8
    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    .line 2960
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 2962
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 2889
    .end local v3    # "absIncrementalDelta":I
    .end local v14    # "down":Z
    .end local v18    # "i":I
    :cond_11
    const/4 v14, 0x0

    goto/16 :goto_6

    .line 2901
    .restart local v6    # "child":Landroid/view/View;
    .restart local v8    # "childEnd":I
    .restart local v12    # "childrenStart":I
    .restart local v14    # "down":Z
    .restart local v18    # "i":I
    :cond_12
    add-int/lit8 v13, v13, 0x1

    .line 2902
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    move-object/from16 v30, v0

    add-int v31, v16, v18

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v0, v6, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 2893
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_7

    .line 2905
    .end local v6    # "child":Landroid/view/View;
    .end local v8    # "childEnd":I
    .end local v12    # "childrenStart":I
    .end local v18    # "i":I
    :cond_13
    sub-int v11, v15, p1

    .line 2907
    .local v11, "childrenEnd":I
    add-int/lit8 v18, v7, -0x1

    .restart local v18    # "i":I
    :goto_9
    if-ltz v18, :cond_a

    .line 2908
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 2909
    .restart local v6    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v10

    .line 2911
    .local v10, "childStart":I
    if-le v10, v11, :cond_a

    .line 2915
    move/from16 v29, v18

    .line 2916
    add-int/lit8 v13, v13, 0x1

    .line 2917
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    move-object/from16 v30, v0

    add-int v31, v16, v18

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v0, v6, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 2907
    add-int/lit8 v18, v18, -0x1

    goto :goto_9

    .line 2949
    .end local v6    # "child":Landroid/view/View;
    .end local v10    # "childStart":I
    .end local v11    # "childrenEnd":I
    .restart local v3    # "absIncrementalDelta":I
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    move/from16 v30, v0

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_15

    .line 2950
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    move/from16 v31, v0

    sub-int v9, v30, v31

    .line 2951
    .restart local v9    # "childIndex":I
    if-ltz v9, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v30

    move/from16 v0, v30

    if-ge v9, v0, :cond_10

    .line 2952
    const/16 v30, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_8

    .line 2955
    .end local v9    # "childIndex":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_8
.end method

.method private selectionChanged()V
    .locals 3

    .prologue
    .line 3233
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 3234
    .local v0, "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    if-nez v0, :cond_0

    .line 3252
    :goto_0
    return-void

    .line 3238
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    if-eqz v1, :cond_3

    .line 3243
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectionNotifier:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;

    if-nez v1, :cond_2

    .line 3244
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectionNotifier:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;

    .line 3247
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectionNotifier:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SelectionNotifier;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3249
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fireOnSelected()V

    .line 3250
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performAccessibilityActionsOnSelected()V

    goto :goto_0
.end method

.method private setNextSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 3148
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    .line 3149
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    .line 3152
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 3153
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 3154
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 3156
    :cond_0
    return-void
.end method

.method private setSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 3123
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 3124
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedRowId:J

    .line 3125
    return-void
.end method

.method private setSelectionInt(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 3128
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 3129
    const/4 v0, 0x0

    .line 3131
    .local v0, "awakeScrollbars":Z
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    .line 3132
    .local v1, "selectedPosition":I
    if-ltz v1, :cond_0

    .line 3133
    add-int/lit8 v2, v1, -0x1

    if-ne p1, v2, :cond_2

    .line 3134
    const/4 v0, 0x1

    .line 3140
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 3142
    if-eqz v0, :cond_1

    .line 3143
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    .line 3145
    :cond_1
    return-void

    .line 3135
    :cond_2
    add-int/lit8 v2, v1, 0x1

    if-ne p1, v2, :cond_0

    .line 3136
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setupChild(Landroid/view/View;IIIZZZ)V
    .locals 19
    .param p1, "child"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "top"    # I
    .param p4, "left"    # I
    .param p5, "flow"    # Z
    .param p6, "selected"    # Z
    .param p7, "recycled"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 4532
    if-eqz p6, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->shouldShowSelector()Z

    move-result v17

    if-eqz v17, :cond_5

    const/4 v10, 0x1

    .line 4533
    .local v10, "isSelected":Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    move-result v17

    move/from16 v0, v17

    if-eq v10, v0, :cond_6

    const/4 v15, 0x1

    .line 4534
    .local v15, "updateChildSelected":Z
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 4536
    .local v13, "touchMode":I
    if-lez v13, :cond_7

    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v13, v0, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    move/from16 v1, p2

    if-ne v0, v1, :cond_7

    const/4 v9, 0x1

    .line 4538
    .local v9, "isPressed":Z
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isPressed()Z

    move-result v17

    move/from16 v0, v17

    if-eq v9, v0, :cond_8

    const/4 v14, 0x1

    .line 4539
    .local v14, "updateChildPressed":Z
    :goto_3
    if-eqz p7, :cond_0

    if-nez v15, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v17

    if-eqz v17, :cond_9

    :cond_0
    const/4 v12, 0x1

    .line 4542
    .local v12, "needToMeasure":Z
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 4543
    .local v11, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    if-nez v11, :cond_1

    .line 4544
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v11

    .line 4547
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v17

    move/from16 v0, v17

    iput v0, v11, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->viewType:I

    .line 4549
    if-eqz p7, :cond_b

    iget-boolean v0, v11, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->forceAdd:Z

    move/from16 v17, v0

    if-nez v17, :cond_b

    .line 4550
    if-eqz p5, :cond_a

    const/16 v17, -0x1

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4556
    :goto_6
    if-eqz v15, :cond_2

    .line 4557
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setSelected(Z)V

    .line 4560
    :cond_2
    if-eqz v14, :cond_3

    .line 4561
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->setPressed(Z)V

    .line 4564
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    move-object/from16 v17, v0

    sget-object v18, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 4565
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/widget/Checkable;

    move/from16 v17, v0

    if-eqz v17, :cond_d

    move-object/from16 v17, p1

    .line 4566
    check-cast v17, Landroid/widget/Checkable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    invoke-interface/range {v17 .. v18}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 4572
    :cond_4
    :goto_7
    if-eqz v12, :cond_e

    .line 4573
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureChild(Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;)V

    .line 4578
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v16

    .line 4579
    .local v16, "w":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 4581
    .local v8, "h":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    if-nez p5, :cond_f

    sub-int v7, p3, v8

    .line 4582
    .local v7, "childTop":I
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    move/from16 v17, v0

    if-nez v17, :cond_10

    if-nez p5, :cond_10

    sub-int v5, p4, v16

    .line 4584
    .local v5, "childLeft":I
    :goto_a
    if-eqz v12, :cond_11

    .line 4585
    add-int v6, v5, v16

    .line 4586
    .local v6, "childRight":I
    add-int v4, v7, v8

    .line 4588
    .local v4, "childBottom":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v6, v4}, Landroid/view/View;->layout(IIII)V

    .line 4593
    .end local v4    # "childBottom":I
    .end local v6    # "childRight":I
    :goto_b
    return-void

    .line 4532
    .end local v5    # "childLeft":I
    .end local v7    # "childTop":I
    .end local v8    # "h":I
    .end local v9    # "isPressed":Z
    .end local v10    # "isSelected":Z
    .end local v11    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .end local v12    # "needToMeasure":Z
    .end local v13    # "touchMode":I
    .end local v14    # "updateChildPressed":Z
    .end local v15    # "updateChildSelected":Z
    .end local v16    # "w":I
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 4533
    .restart local v10    # "isSelected":Z
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 4536
    .restart local v13    # "touchMode":I
    .restart local v15    # "updateChildSelected":Z
    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 4538
    .restart local v9    # "isPressed":Z
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 4539
    .restart local v14    # "updateChildPressed":Z
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_4

    .line 4550
    .restart local v11    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .restart local v12    # "needToMeasure":Z
    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_5

    .line 4552
    :cond_b
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v11, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->forceAdd:Z

    .line 4553
    if-eqz p5, :cond_c

    const/16 v17, -0x1

    :goto_c
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v11, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto/16 :goto_6

    :cond_c
    const/16 v17, 0x0

    goto :goto_c

    .line 4567
    :cond_d
    sget v17, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_4

    .line 4568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_7

    .line 4575
    :cond_e
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cleanupLayoutState(Landroid/view/View;)V

    goto :goto_8

    .restart local v8    # "h":I
    .restart local v16    # "w":I
    :cond_f
    move/from16 v7, p3

    .line 4581
    goto :goto_9

    .restart local v7    # "childTop":I
    :cond_10
    move/from16 v5, p4

    .line 4582
    goto :goto_a

    .line 4590
    .restart local v5    # "childLeft":I
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v17

    sub-int v17, v5, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 4591
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v17

    sub-int v17, v7, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_b
.end method

.method private shouldShowSelector()Z
    .locals 1

    .prologue
    .line 3085
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->touchModeDrawsInPressedState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private touchModeDrawsInPressedState()Z
    .locals 1

    .prologue
    .line 3159
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    packed-switch v0, :pswitch_data_0

    .line 3164
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 3162
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 3159
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private triggerCheckForLongPress()V
    .locals 4

    .prologue
    .line 2822
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    if-nez v0, :cond_0

    .line 2823
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    .line 2826
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;->rememberWindowAttachCount()V

    .line 2828
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForLongPress:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2829
    return-void
.end method

.method private triggerCheckForTap()V
    .locals 4

    .prologue
    .line 2806
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    if-nez v0, :cond_0

    .line 2807
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    .line 2810
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingCheckForTap:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2811
    return-void
.end method

.method private updateEmptyStatus()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 5835
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 5837
    .local v0, "isEmpty":Z
    :goto_0
    if-eqz v0, :cond_4

    .line 5838
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 5839
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5840
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setVisibility(I)V

    .line 5850
    :goto_1
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-eqz v1, :cond_1

    .line 5851
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getBottom()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layout(IIII)V

    .line 5860
    :cond_1
    :goto_2
    return-void

    .end local v0    # "isEmpty":Z
    :cond_2
    move v0, v1

    .line 5835
    goto :goto_0

    .line 5844
    .restart local v0    # "isEmpty":Z
    :cond_3
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setVisibility(I)V

    goto :goto_1

    .line 5854
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_5

    .line 5855
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 5858
    :cond_5
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateOnScreenCheckedViews()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 5185
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 5186
    .local v2, "firstPos":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 5188
    .local v1, "count":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_1

    const/4 v5, 0x1

    .line 5190
    .local v5, "useActivated":Z
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_3

    .line 5191
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5192
    .local v0, "child":Landroid/view/View;
    add-int v4, v2, v3

    .line 5194
    .local v4, "position":I
    instance-of v6, v0, Landroid/widget/Checkable;

    if-eqz v6, :cond_2

    .line 5195
    check-cast v0, Landroid/widget/Checkable;

    .end local v0    # "child":Landroid/view/View;
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 5190
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 5188
    .end local v3    # "i":I
    .end local v4    # "position":I
    .end local v5    # "useActivated":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 5196
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    .restart local v4    # "position":I
    .restart local v5    # "useActivated":Z
    :cond_2
    if-eqz v5, :cond_0

    .line 5197
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/view/View;->setActivated(Z)V

    goto :goto_2

    .line 5200
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "position":I
    :cond_3
    return-void
.end method

.method private updateOverScrollState(II)V
    .locals 12
    .param p1, "delta"    # I
    .param p2, "overscroll"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 2602
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_4

    move v1, v5

    :goto_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_5

    move v2, p2

    :goto_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_6

    move v3, v5

    :goto_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_7

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    :goto_3
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_8

    move v7, v5

    :goto_4
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_9

    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverscrollDistance:I

    :goto_5
    move-object v0, p0

    move v6, v5

    invoke-direct/range {v0 .. v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->overScrollByInternal(IIIIIIIIZ)Z

    .line 2605
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverscrollDistance:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2607
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2612
    :cond_0
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v10

    .line 2613
    .local v10, "overscrollMode":I
    if-eqz v10, :cond_1

    if-ne v10, v9, :cond_3

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->contentFits()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2614
    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 2616
    int-to-float v1, p2

    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v0

    :goto_6
    int-to-float v0, v0

    div-float v11, v1, v0

    .line 2617
    .local v11, "pull":F
    if-lez p1, :cond_b

    .line 2618
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, v11}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    .line 2620
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2621
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 2631
    :cond_2
    :goto_7
    if-eqz p1, :cond_3

    .line 2632
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 2635
    .end local v11    # "pull":F
    :cond_3
    return-void

    .end local v10    # "overscrollMode":I
    :cond_4
    move v1, p2

    .line 2602
    goto :goto_0

    :cond_5
    move v2, v5

    goto :goto_1

    :cond_6
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    goto :goto_2

    :cond_7
    move v4, v5

    goto :goto_3

    :cond_8
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverscrollDistance:I

    goto :goto_4

    :cond_9
    move v8, v5

    goto :goto_5

    .line 2616
    .restart local v10    # "overscrollMode":I
    :cond_a
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v0

    goto :goto_6

    .line 2623
    .restart local v11    # "pull":F
    :cond_b
    if-gez p1, :cond_2

    .line 2624
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, v11}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    .line 2626
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2627
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    goto :goto_7
.end method

.method private updateSelectorState()V
    .locals 2

    .prologue
    .line 3215
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 3216
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->shouldShowSelector()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3217
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 3222
    :cond_0
    :goto_0
    return-void

    .line 3219
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->STATE_NOTHING:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method private useDefaultSelector()V
    .locals 2

    .prologue
    .line 3081
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 3082
    return-void
.end method


# virtual methods
.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 3408
    invoke-super {p0}, Landroid/widget/AdapterView;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5288
    instance-of v0, p1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    return v0
.end method

.method public clearChoices()V
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v0, :cond_1

    .line 703
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 706
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 707
    return-void
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1110
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v4

    .line 1111
    .local v4, "count":I
    if-nez v4, :cond_1

    move v5, v6

    .line 1133
    :cond_0
    :goto_0
    return v5

    .line 1115
    :cond_1
    mul-int/lit8 v5, v4, 0x64

    .line 1117
    .local v5, "extent":I
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1118
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1120
    .local v1, "childLeft":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1121
    .local v3, "childWidth":I
    if-lez v3, :cond_2

    .line 1122
    mul-int/lit8 v6, v1, 0x64

    div-int/2addr v6, v3

    add-int/2addr v5, v6

    .line 1125
    :cond_2
    add-int/lit8 v6, v4, -0x1

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1126
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1128
    .local v2, "childRight":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1129
    if-lez v3, :cond_0

    .line 1130
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v6

    sub-int v6, v2, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v3

    sub-int/2addr v5, v6

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1158
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 1159
    .local v4, "firstPosition":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 1161
    .local v1, "childCount":I
    if-ltz v4, :cond_0

    if-nez v1, :cond_1

    .line 1173
    :cond_0
    :goto_0
    return v5

    .line 1165
    :cond_1
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1166
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1168
    .local v2, "childLeft":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1169
    .local v3, "childWidth":I
    if-lez v3, :cond_0

    .line 1170
    mul-int/lit8 v6, v4, 0x64

    mul-int/lit8 v7, v2, 0x64

    div-int/2addr v7, v3

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1190
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1192
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eqz v1, :cond_0

    .line 1194
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    :cond_0
    return v0
.end method

.method public computeScroll()V
    .locals 8

    .prologue
    .line 2981
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3018
    :goto_0
    return-void

    .line 2986
    :cond_0
    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_1

    .line 2987
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    .line 2992
    .local v4, "pos":I
    :goto_1
    int-to-float v6, v4

    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    sub-float/2addr v6, v7

    float-to-int v0, v6

    .line 2993
    .local v0, "diff":I
    int-to-float v6, v4

    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    .line 2995
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->scrollListItemsBy(I)Z

    move-result v5

    .line 2997
    .local v5, "stopped":Z
    if-nez v5, :cond_2

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2998
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 2989
    .end local v0    # "diff":I
    .end local v4    # "pos":I
    .end local v5    # "stopped":Z
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v4

    .restart local v4    # "pos":I
    goto :goto_1

    .line 3000
    .restart local v0    # "diff":I
    .restart local v5    # "stopped":Z
    :cond_2
    if-eqz v5, :cond_4

    .line 3001
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v3

    .line 3002
    .local v3, "overScrollMode":I
    const/4 v6, 0x2

    if-eq v3, v6, :cond_3

    .line 3003
    if-lez v0, :cond_5

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 3005
    .local v1, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :goto_2
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getCurrVelocity()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    move-result v2

    .line 3007
    .local v2, "needsInvalidate":Z
    if-eqz v2, :cond_3

    .line 3008
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 3012
    .end local v1    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    .end local v2    # "needsInvalidate":Z
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    .line 3015
    .end local v3    # "overScrollMode":I
    :cond_4
    const/4 v6, -0x1

    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 3016
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    goto :goto_0

    .line 3003
    .restart local v3    # "overScrollMode":I
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_2
.end method

.method protected computeVerticalScrollExtent()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1082
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v4

    .line 1083
    .local v4, "count":I
    if-nez v4, :cond_1

    move v5, v6

    .line 1105
    :cond_0
    :goto_0
    return v5

    .line 1087
    :cond_1
    mul-int/lit8 v5, v4, 0x64

    .line 1089
    .local v5, "extent":I
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1090
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1092
    .local v3, "childTop":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1093
    .local v2, "childHeight":I
    if-lez v2, :cond_2

    .line 1094
    mul-int/lit8 v6, v3, 0x64

    div-int/2addr v6, v2

    add-int/2addr v5, v6

    .line 1097
    :cond_2
    add-int/lit8 v6, v4, -0x1

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1098
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 1100
    .local v1, "childBottom":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1101
    if-lez v2, :cond_0

    .line 1102
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v6

    sub-int v6, v1, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v2

    sub-int/2addr v5, v6

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1138
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 1139
    .local v4, "firstPosition":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 1141
    .local v1, "childCount":I
    if-ltz v4, :cond_0

    if-nez v1, :cond_1

    .line 1153
    :cond_0
    :goto_0
    return v5

    .line 1145
    :cond_1
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1146
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1148
    .local v3, "childTop":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1149
    .local v2, "childHeight":I
    if-lez v2, :cond_0

    .line 1150
    mul-int/lit8 v6, v4, 0x64

    mul-int/lit8 v7, v3, 0x64

    div-int/2addr v7, v2

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_0
.end method

.method protected computeVerticalScrollRange()I
    .locals 3

    .prologue
    .line 1178
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1180
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eqz v1, :cond_0

    .line 1182
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1185
    :cond_0
    return v0
.end method

.method confirmCheckedPositionsById()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 4025
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12}, Landroid/util/SparseBooleanArray;->clear()V

    .line 4027
    const/4 v0, 0x0

    .local v0, "checkedIndex":I
    :goto_0
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v12

    if-ge v0, v12, :cond_4

    .line 4028
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v0}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 4029
    .local v4, "id":J
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v0}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 4031
    .local v3, "lastPos":I
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v6

    .line 4032
    .local v6, "lastPosId":J
    cmp-long v12, v4, v6

    if-eqz v12, :cond_3

    .line 4034
    const/4 v12, 0x0

    add-int/lit8 v13, v3, -0x14

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 4035
    .local v11, "start":I
    add-int/lit8 v12, v3, 0x14

    iget v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4036
    .local v1, "end":I
    const/4 v2, 0x0

    .line 4038
    .local v2, "found":Z
    move v10, v11

    .local v10, "searchPos":I
    :goto_1
    if-ge v10, v1, :cond_0

    .line 4039
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12, v10}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    .line 4040
    .local v8, "searchId":J
    cmp-long v12, v4, v8

    if-nez v12, :cond_2

    .line 4041
    const/4 v2, 0x1

    .line 4042
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12, v10, v14}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 4043
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v0, v13}, Landroid/support/v4/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 4048
    .end local v8    # "searchId":J
    :cond_0
    if-nez v2, :cond_1

    .line 4049
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    .line 4050
    add-int/lit8 v0, v0, -0x1

    .line 4051
    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    add-int/lit8 v12, v12, -0x1

    iput v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 4027
    .end local v1    # "end":I
    .end local v2    # "found":Z
    .end local v10    # "searchPos":I
    .end local v11    # "start":I
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4038
    .restart local v1    # "end":I
    .restart local v2    # "found":Z
    .restart local v8    # "searchId":J
    .restart local v10    # "searchPos":I
    .restart local v11    # "start":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 4054
    .end local v1    # "end":I
    .end local v2    # "found":Z
    .end local v8    # "searchId":J
    .end local v10    # "searchPos":I
    .end local v11    # "start":I
    :cond_3
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12, v3, v14}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_2

    .line 4057
    .end local v3    # "lastPos":I
    .end local v4    # "id":J
    .end local v6    # "lastPosId":J
    :cond_4
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3413
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDrawSelectorOnTop:Z

    .line 3414
    .local v0, "drawSelectorOnTop":Z
    if-nez v0, :cond_0

    .line 3415
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 3418
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 3420
    if-eqz v0, :cond_1

    .line 3421
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 3423
    :cond_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3504
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 3505
    .local v1, "handled":Z
    if-nez v1, :cond_0

    .line 3507
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 3508
    .local v0, "focused":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 3511
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 3515
    .end local v0    # "focused":Landroid/view/View;
    :cond_0
    return v1
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 3522
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3427
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    .line 3429
    const/4 v0, 0x0

    .line 3431
    .local v0, "needsInvalidate":Z
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v1, :cond_0

    .line 3432
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->drawStartEdge(Landroid/graphics/Canvas;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 3435
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v1, :cond_1

    .line 3436
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->drawEndEdge(Landroid/graphics/Canvas;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 3439
    :cond_1
    if-eqz v0, :cond_2

    .line 3440
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 3442
    :cond_2
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 3369
    invoke-super {p0}, Landroid/widget/AdapterView;->drawableStateChanged()V

    .line 3370
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    .line 3371
    return-void
.end method

.method fillGap(Z)V
    .locals 9
    .param p1, "down"    # Z

    .prologue
    const/4 v8, 0x0

    .line 4596
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v0

    .line 4598
    .local v0, "childCount":I
    if-lez v0, :cond_0

    .line 4599
    if-eqz p1, :cond_4

    .line 4600
    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v5

    .line 4603
    .local v5, "paddingStart":I
    :goto_0
    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_2

    .line 4604
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 4609
    .local v3, "lastEnd":I
    :goto_1
    if-lez v0, :cond_3

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    add-int v4, v3, v6

    .line 4610
    .local v4, "offset":I
    :goto_2
    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v6, v0

    invoke-direct {p0, v6, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillAfter(II)Landroid/view/View;

    .line 4611
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->correctTooHigh(I)V

    .line 4629
    .end local v3    # "lastEnd":I
    .end local v4    # "offset":I
    .end local v5    # "paddingStart":I
    :cond_0
    :goto_3
    return-void

    .line 4600
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v5

    goto :goto_0

    .line 4606
    .restart local v5    # "paddingStart":I
    :cond_2
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v3

    .restart local v3    # "lastEnd":I
    goto :goto_1

    :cond_3
    move v4, v5

    .line 4609
    goto :goto_2

    .line 4616
    .end local v3    # "lastEnd":I
    .end local v5    # "paddingStart":I
    :cond_4
    iget-boolean v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_5

    .line 4617
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v7

    sub-int v1, v6, v7

    .line 4618
    .local v1, "end":I
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v2

    .line 4624
    .local v2, "firstStart":I
    :goto_4
    if-lez v0, :cond_6

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    sub-int v4, v2, v6

    .line 4625
    .restart local v4    # "offset":I
    :goto_5
    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/lit8 v6, v6, -0x1

    invoke-direct {p0, v6, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->fillBefore(II)Landroid/view/View;

    .line 4626
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->correctTooLow(I)V

    goto :goto_3

    .line 4620
    .end local v1    # "end":I
    .end local v2    # "firstStart":I
    .end local v4    # "offset":I
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v7

    sub-int v1, v6, v7

    .line 4621
    .restart local v1    # "end":I
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    .restart local v2    # "firstStart":I
    goto :goto_4

    :cond_6
    move v4, v1

    .line 4624
    goto :goto_5
.end method

.method public forceDetachFromWindow()V
    .locals 0

    .prologue
    .line 971
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onDetachedFromWindow()V

    .line 972
    return-void
.end method

.method fullScroll(I)Z
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    .line 1770
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 1772
    const/4 v0, 0x0

    .line 1773
    .local v0, "moved":Z
    const/16 v2, 0x21

    if-eq p1, v2, :cond_0

    const/16 v2, 0x11

    if-ne p1, v2, :cond_4

    .line 1774
    :cond_0
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-eqz v2, :cond_2

    .line 1775
    const/4 v2, 0x0

    invoke-direct {p0, v2, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 1776
    .local v1, "position":I
    if-ltz v1, :cond_1

    .line 1777
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1778
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionInt(I)V

    .line 1779
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 1782
    :cond_1
    const/4 v0, 0x1

    .line 1797
    .end local v1    # "position":I
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1798
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    .line 1799
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 1802
    :cond_3
    return v0

    .line 1784
    :cond_4
    const/16 v2, 0x82

    if-eq p1, v2, :cond_5

    const/16 v2, 0x42

    if-ne p1, v2, :cond_2

    .line 1785
    :cond_5
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 1786
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 1787
    .restart local v1    # "position":I
    if-ltz v1, :cond_6

    .line 1788
    const/4 v2, 0x3

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1789
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionInt(I)V

    .line 1790
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 1793
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, -0x2

    .line 5274
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    .line 5275
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    invoke-direct {v0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;-><init>(II)V

    .line 5277
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 5293
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5283
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCheckedItemCount()I
    .locals 1

    .prologue
    .line 560
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    return v0
.end method

.method public getCheckedItemIds()[J
    .locals 6

    .prologue
    .line 613
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v4, :cond_2

    .line 614
    :cond_0
    const/4 v4, 0x0

    new-array v3, v4, [J

    .line 625
    :cond_1
    return-object v3

    .line 617
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 618
    .local v2, "idStates":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    .line 619
    .local v0, "count":I
    new-array v3, v0, [J

    .line 621
    .local v3, "ids":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 622
    invoke-virtual {v2, v1}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 621
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCheckedItemPosition()I
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    .line 589
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-eq v0, v1, :cond_0

    .line 600
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 603
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChoiceMode()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    return-object v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 5298
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 812
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    return v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 802
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    return v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 842
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 844
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 847
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 848
    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 852
    :goto_0
    return-void

    .line 850
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public getItemMargin()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    return v0
.end method

.method public getItemsCanFocus()Z
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 807
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getMaxScrollAmount()I
    .locals 2

    .prologue
    .line 2044
    const v0, 0x3ea8f5c3    # 0.33f

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getOrientation()Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->VERTICAL:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->HORIZONTAL:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    goto :goto_0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, -0x1

    .line 817
    move-object v0, p1

    .line 820
    .local v0, "child":Landroid/view/View;
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_1

    .line 821
    move-object v0, v4

    goto :goto_0

    .line 823
    .end local v4    # "v":Landroid/view/View;
    :catch_0
    move-exception v2

    .line 837
    :cond_0
    :goto_1
    return v5

    .line 829
    .restart local v4    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 830
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v1, :cond_0

    .line 831
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 832
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v5, v3

    goto :goto_1

    .line 830
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public getSelectedItemId()J
    .locals 2

    .prologue
    .line 545
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedRowId:J

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 537
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNextSelectedPosition:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 3453
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-ltz v0, :cond_0

    .line 3454
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3456
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public isItemChecked(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 575
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 919
    invoke-super {p0}, Landroid/widget/AdapterView;->onAttachedToWindow()V

    .line 921
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 922
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 924
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    if-nez v1, :cond_0

    .line 925
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    .line 926
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 929
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 930
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldItemCount:I

    .line 931
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 934
    :cond_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    .line 935
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 6
    .param p1, "extraSpace"    # I

    .prologue
    .line 3376
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsChildViewEnabled:Z

    if-eqz v4, :cond_1

    .line 3378
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 3403
    :cond_0
    :goto_0
    return-object v3

    .line 3384
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->ENABLED_STATE_SET:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    .line 3389
    .local v1, "enabledState":I
    add-int/lit8 v4, p1, 0x1

    invoke-super {p0, v4}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 3390
    .local v3, "state":[I
    const/4 v0, -0x1

    .line 3391
    .local v0, "enabledPos":I
    array-length v4, v3

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 3392
    aget v4, v3, v2

    if-ne v4, v1, :cond_3

    .line 3393
    move v0, v2

    .line 3399
    :cond_2
    if-ltz v0, :cond_0

    .line 3400
    add-int/lit8 v4, v0, 0x1

    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 3391
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 939
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    if-eqz v1, :cond_3

    .line 940
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 943
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->clear()V

    .line 945
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 946
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 948
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 949
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 950
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    .line 953
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    if-eqz v1, :cond_1

    .line 954
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 957
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 958
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 959
    const/4 v1, -0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 961
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 963
    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 966
    :cond_2
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    .line 968
    .end local v0    # "treeObserver":Landroid/view/ViewTreeObserver;
    :cond_3
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 15
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 856
    invoke-super/range {p0 .. p3}, Landroid/widget/AdapterView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 858
    if-eqz p1, :cond_1

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-gez v12, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v12

    if-nez v12, :cond_1

    .line 859
    iget-boolean v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    if-nez v12, :cond_0

    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v12, :cond_0

    .line 862
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 863
    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    iput v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldItemCount:I

    .line 864
    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    iput v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 867
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelection()Z

    .line 870
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 871
    .local v2, "adapter":Landroid/widget/ListAdapter;
    const/4 v5, -0x1

    .line 872
    .local v5, "closetChildIndex":I
    const/4 v4, 0x0

    .line 874
    .local v4, "closestChildStart":I
    if-eqz v2, :cond_6

    if-eqz p1, :cond_6

    if-eqz p3, :cond_6

    .line 875
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollX()I

    move-result v12

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollY()I

    move-result v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/graphics/Rect;->offset(II)V

    .line 879
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v13

    iget v14, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v13, v14

    if-ge v12, v13, :cond_2

    .line 880
    const/4 v12, 0x0

    iput v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 881
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 886
    :cond_2
    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTempRect:Landroid/graphics/Rect;

    .line 887
    .local v11, "otherRect":Landroid/graphics/Rect;
    const v9, 0x7fffffff

    .line 888
    .local v9, "minDistance":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v3

    .line 889
    .local v3, "childCount":I
    iget v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 891
    .local v7, "firstPosition":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v3, :cond_6

    .line 893
    add-int v12, v7, v8

    invoke-interface {v2, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v12

    if-nez v12, :cond_4

    .line 891
    :cond_3
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 897
    :cond_4
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 898
    .local v10, "other":Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 899
    invoke-virtual {p0, v10, v11}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 900
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-static {v0, v11, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v6

    .line 902
    .local v6, "distance":I
    if-ge v6, v9, :cond_3

    .line 903
    move v9, v6

    .line 904
    move v5, v8

    .line 905
    iget-boolean v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v12, :cond_5

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v4

    :goto_2
    goto :goto_1

    :cond_5
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_2

    .line 910
    .end local v3    # "childCount":I
    .end local v6    # "distance":I
    .end local v7    # "firstPosition":I
    .end local v8    # "i":I
    .end local v9    # "minDistance":I
    .end local v10    # "other":Landroid/view/View;
    .end local v11    # "otherRect":Landroid/graphics/Rect;
    :cond_6
    if-ltz v5, :cond_7

    .line 911
    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v12, v5

    invoke-virtual {p0, v12, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionFromOffset(II)V

    .line 915
    :goto_3
    return-void

    .line 913
    :cond_7
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    goto :goto_3
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1644
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1645
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1646
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 3
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1651
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1652
    const-class v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1654
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 1656
    .local v0, "infoCompat":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1657
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFirstVisiblePosition()I

    move-result v1

    if-lez v1, :cond_0

    .line 1658
    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1661
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 1662
    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1665
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v11, -0x1

    const/4 v9, 0x0

    .line 1235
    iget-boolean v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v10, :cond_2

    :cond_0
    move v8, v9

    .line 1308
    :cond_1
    :goto_0
    return v8

    .line 1239
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    and-int/lit16 v0, v10, 0xff

    .line 1240
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    move v8, v9

    .line 1308
    goto :goto_0

    .line 1242
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->initOrResetVelocityTracker()V

    .line 1243
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1245
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v10}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1247
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 1248
    .local v6, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 1250
    .local v7, "y":F
    iget-boolean v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v10, :cond_4

    .end local v7    # "y":F
    :goto_2
    iput v7, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    .line 1252
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    float-to-int v10, v10

    invoke-direct {p0, v10}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findMotionRowOrColumn(I)I

    move-result v4

    .line 1254
    .local v4, "motionPosition":I
    invoke-static {p1, v9}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v10

    iput v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    .line 1255
    const/4 v10, 0x0

    iput v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    .line 1257
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/4 v11, 0x4

    if-eq v10, v11, :cond_1

    .line 1259
    if-ltz v4, :cond_3

    .line 1260
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    .line 1261
    iput v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    goto :goto_1

    .end local v4    # "motionPosition":I
    .restart local v7    # "y":F
    :cond_4
    move v7, v6

    .line 1250
    goto :goto_2

    .line 1267
    .end local v6    # "x":F
    .end local v7    # "y":F
    :pswitch_1
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    if-nez v10, :cond_3

    .line 1271
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->initVelocityTrackerIfNotExists()V

    .line 1272
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1274
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    invoke-static {p1, v10}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1275
    .local v3, "index":I
    if-gez v3, :cond_5

    .line 1276
    const-string v8, "TwoWayView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - did TwoWayView receive an inconsistent event stream?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v9

    .line 1277
    goto :goto_0

    .line 1281
    :cond_5
    iget-boolean v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v10, :cond_6

    .line 1282
    invoke-static {p1, v3}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 1287
    .local v5, "pos":F
    :goto_3
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    sub-float v10, v5, v10

    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    add-float v2, v10, v11

    .line 1288
    .local v2, "diff":F
    float-to-int v1, v2

    .line 1289
    .local v1, "delta":I
    int-to-float v10, v1

    sub-float v10, v2, v10

    iput v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    .line 1291
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->maybeStartScrolling(I)Z

    move-result v10

    if-eqz v10, :cond_3

    goto/16 :goto_0

    .line 1284
    .end local v1    # "delta":I
    .end local v2    # "diff":F
    .end local v5    # "pos":F
    :cond_6
    invoke-static {p1, v3}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v5

    .restart local v5    # "pos":F
    goto :goto_3

    .line 1300
    .end local v3    # "index":I
    .end local v5    # "pos":F
    :pswitch_2
    iput v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    .line 1301
    iput v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1302
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleVelocityTracker()V

    .line 1303
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    goto/16 :goto_1

    .line 1240
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1608
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "repeatCount"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1613
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1618
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 3581
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    .line 3583
    if-eqz p1, :cond_1

    .line 3584
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v0

    .line 3585
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 3586
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->forceLayout()V

    .line 3585
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3589
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->markChildrenDirty()V

    .line 3592
    .end local v0    # "childCount":I
    .end local v2    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 3594
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    .line 3596
    sub-int v4, p4, p2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v5

    sub-int v3, v4, v5

    .line 3597
    .local v3, "width":I
    sub-int v4, p5, p3

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v5

    sub-int v1, v4, v5

    .line 3599
    .local v1, "height":I
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v4, :cond_2

    .line 3600
    iget-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v4, :cond_3

    .line 3601
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3602
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3608
    :cond_2
    :goto_1
    return-void

    .line 3604
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v1, v3}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3605
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v1, v3}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 19
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 3526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 3527
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->useDefaultSelector()V

    .line 3530
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v18

    .line 3531
    .local v18, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    .line 3532
    .local v16, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 3533
    .local v11, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 3535
    .local v6, "heightSize":I
    const/4 v15, 0x0

    .line 3536
    .local v15, "childWidth":I
    const/4 v14, 0x0

    .line 3538
    .local v14, "childHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 3539
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lez v2, :cond_2

    if-eqz v18, :cond_1

    if-nez v16, :cond_2

    .line 3540
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsScrap:[Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v13

    .line 3542
    .local v13, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_8

    move/from16 v17, p1

    .line 3544
    .local v17, "secondaryMeasureSpec":I
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v13, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureScrapChild(Landroid/view/View;II)V

    .line 3546
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 3547
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    .line 3549
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleOnMeasure()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    const/4 v3, -0x1

    invoke-virtual {v2, v13, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3554
    .end local v13    # "child":Landroid/view/View;
    .end local v17    # "secondaryMeasureSpec":I
    :cond_2
    if-nez v18, :cond_3

    .line 3555
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int v11, v2, v15

    .line 3556
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 3557
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getVerticalScrollbarWidth()I

    move-result v2

    add-int/2addr v11, v2

    .line 3561
    :cond_3
    if-nez v16, :cond_4

    .line 3562
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    add-int v6, v2, v14

    .line 3563
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v2, :cond_4

    .line 3564
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHorizontalScrollbarHeight()I

    move-result v2

    add-int/2addr v6, v2

    .line 3568
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_5

    const/high16 v2, -0x80000000

    move/from16 v0, v16

    if-ne v0, v2, :cond_5

    .line 3569
    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v7, -0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureHeightOfChildren(IIIII)I

    move-result v6

    .line 3572
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v2, :cond_6

    const/high16 v2, -0x80000000

    move/from16 v0, v18

    if-ne v0, v2, :cond_6

    .line 3573
    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v12, -0x1

    move-object/from16 v7, p0

    move/from16 v8, p2

    invoke-direct/range {v7 .. v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->measureWidthOfChildren(IIIII)I

    move-result v11

    .line 3576
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setMeasuredDimension(II)V

    .line 3577
    return-void

    .line 3538
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    goto/16 :goto_0

    .restart local v13    # "child":Landroid/view/View;
    :cond_8
    move/from16 v17, p2

    .line 3542
    goto/16 :goto_1
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 4
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 1007
    const/4 v0, 0x0

    .line 1009
    .local v0, "needsInvalidate":Z
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->AMAZON_DEVICE:Z

    if-nez v1, :cond_0

    .line 1010
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eq v1, p2, :cond_2

    .line 1011
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollX()I

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onScrollChanged(IIII)V

    .line 1012
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 1013
    const/4 v0, 0x1

    .line 1021
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1022
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 1023
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    .line 1025
    :cond_1
    return-void

    .line 1014
    :cond_2
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eq v1, p1, :cond_0

    .line 1015
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getScrollY()I

    move-result v3

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onScrollChanged(IIII)V

    .line 1016
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 1017
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 5376
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 5377
    .local v0, "ss":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 5379
    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 5380
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->height:I

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncHeight:J

    .line 5382
    iget-wide v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->selectedId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_3

    .line 5383
    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 5384
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 5385
    iget-wide v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->selectedId:J

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 5386
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 5387
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 5388
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    .line 5404
    :cond_0
    :goto_0
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_1

    .line 5405
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 5408
    :cond_1
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_2

    .line 5409
    iget-object v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 5412
    :cond_2
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkedItemCount:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 5414
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    .line 5415
    return-void

    .line 5389
    :cond_3
    iget-wide v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    .line 5390
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 5393
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 5395
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    .line 5396
    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 5397
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 5398
    iget-wide v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 5399
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 5400
    iget v1, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 5401
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncMode:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 13

    .prologue
    .line 5303
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v9

    .line 5304
    .local v9, "superState":Landroid/os/Parcelable;
    new-instance v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    invoke-direct {v8, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 5306
    .local v8, "ss":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    if-eqz v10, :cond_0

    .line 5307
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    iget-wide v10, v10, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->selectedId:J

    iput-wide v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->selectedId:J

    .line 5308
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    iget-wide v10, v10, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    iput-wide v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    .line 5309
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    iget v10, v10, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    .line 5310
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    iget v10, v10, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    .line 5311
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    iget v10, v10, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->height:I

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->height:I

    .line 5371
    :goto_0
    return-object v8

    .line 5316
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v10

    if-lez v10, :cond_2

    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lez v10, :cond_2

    const/4 v3, 0x1

    .line 5317
    .local v3, "haveChildren":Z
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemId()J

    move-result-wide v6

    .line 5318
    .local v6, "selectedId":J
    iput-wide v6, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->selectedId:J

    .line 5319
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v10

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->height:I

    .line 5321
    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-ltz v10, :cond_3

    .line 5322
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    .line 5323
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getSelectedItemPosition()I

    move-result v10

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    .line 5324
    const-wide/16 v10, -0x1

    iput-wide v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    .line 5354
    :goto_2
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v10, :cond_1

    .line 5355
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cloneCheckStates()Landroid/util/SparseBooleanArray;

    move-result-object v10

    iput-object v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    .line 5358
    :cond_1
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v10, :cond_8

    .line 5359
    new-instance v5, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v5}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 5361
    .local v5, "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v10}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v1

    .line 5362
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    if-ge v4, v1, :cond_7

    .line 5363
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v10, v4}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    iget-object v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v4}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v5, v10, v11, v12}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5362
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 5316
    .end local v1    # "count":I
    .end local v3    # "haveChildren":Z
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    .end local v6    # "selectedId":J
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 5325
    .restart local v3    # "haveChildren":Z
    .restart local v6    # "selectedId":J
    :cond_3
    if-eqz v3, :cond_6

    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    if-lez v10, :cond_6

    .line 5338
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5339
    .local v0, "child":Landroid/view/View;
    iget-boolean v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v10, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v10

    :goto_4
    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    .line 5341
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 5342
    .local v2, "firstPos":I
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-lt v2, v10, :cond_4

    .line 5343
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v2, v10, -0x1

    .line 5346
    :cond_4
    iput v2, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    .line 5347
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v10, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v10

    iput-wide v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    goto :goto_2

    .line 5339
    .end local v2    # "firstPos":I
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v10

    goto :goto_4

    .line 5349
    .end local v0    # "child":Landroid/view/View;
    :cond_6
    const/4 v10, 0x0

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->viewStart:I

    .line 5350
    const-wide/16 v10, -0x1

    iput-wide v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->firstId:J

    .line 5351
    const/4 v10, 0x0

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->position:I

    goto :goto_2

    .line 5366
    .restart local v1    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_7
    iput-object v5, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    .line 5369
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_8
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    iput v10, v8, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;->checkedItemCount:I

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 26
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1313
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1316
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isClickable()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 1577
    :goto_0
    return v2

    .line 1316
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1319
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsAttached:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_4

    .line 1320
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1323
    :cond_4
    const/16 v20, 0x0

    .line 1325
    .local v20, "needsInvalidate":Z
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->initVelocityTrackerIfNotExists()V

    .line 1326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1328
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v11, v2, 0xff

    .line 1329
    .local v11, "action":I
    packed-switch v11, :pswitch_data_0

    .line 1573
    :cond_5
    :goto_1
    if-eqz v20, :cond_6

    .line 1574
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1577
    :cond_6
    const/4 v2, 0x1

    goto :goto_0

    .line 1331
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v2, :cond_5

    .line 1335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 1336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1338
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 1339
    .local v24, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 1341
    .local v25, "y":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_7

    move/from16 v2, v25

    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    .line 1343
    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->pointToPosition(II)I

    move-result v18

    .line 1345
    .local v18, "motionPosition":I
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    .line 1346
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    .line 1348
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v2, :cond_5

    .line 1352
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    .line 1353
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1354
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    .line 1355
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    float-to-int v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->findMotionRowOrColumn(I)I

    move-result v18

    .line 1356
    const/4 v2, 0x1

    goto/16 :goto_0

    .end local v18    # "motionPosition":I
    :cond_7
    move/from16 v2, v24

    .line 1341
    goto :goto_2

    .line 1357
    .restart local v18    # "motionPosition":I
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    if-ltz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1358
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1359
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->triggerCheckForTap()V

    .line 1362
    :cond_9
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    goto/16 :goto_1

    .line 1368
    .end local v18    # "motionPosition":I
    .end local v24    # "x":F
    .end local v25    # "y":F
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v17

    .line 1369
    .local v17, "index":I
    if-gez v17, :cond_a

    .line 1370
    const-string v2, "TwoWayView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - did TwoWayView receive an inconsistent event stream?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1375
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_c

    .line 1376
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v22

    .line 1381
    .local v22, "pos":F
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-eqz v2, :cond_b

    .line 1384
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 1387
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    sub-float v2, v22, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    add-float v15, v2, v3

    .line 1388
    .local v15, "diff":F
    float-to-int v14, v15

    .line 1389
    .local v14, "delta":I
    int-to-float v2, v14

    sub-float v2, v15, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchRemainderPos:F

    .line 1391
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    goto/16 :goto_1

    .line 1397
    :pswitch_3
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->maybeStartScrolling(I)Z

    goto/16 :goto_1

    .line 1378
    .end local v14    # "delta":I
    .end local v15    # "diff":F
    .end local v22    # "pos":F
    :cond_c
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v22

    .restart local v22    # "pos":F
    goto :goto_3

    .line 1402
    .restart local v14    # "delta":I
    .restart local v15    # "diff":F
    :pswitch_4
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    .line 1403
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->maybeScroll(I)V

    goto/16 :goto_1

    .line 1411
    .end local v14    # "delta":I
    .end local v15    # "diff":F
    .end local v17    # "index":I
    .end local v22    # "pos":F
    :pswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForTap()V

    .line 1412
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1413
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    .line 1415
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 1416
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1417
    .local v19, "motionView":Landroid/view/View;
    if-eqz v19, :cond_d

    .line 1418
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1421
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_e

    .line 1422
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v3

    or-int v20, v2, v3

    .line 1425
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleVelocityTracker()V

    goto/16 :goto_1

    .line 1430
    .end local v19    # "motionView":Landroid/view/View;
    :pswitch_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    packed-switch v2, :pswitch_data_2

    .line 1559
    :goto_4
    :pswitch_7
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForTap()V

    .line 1560
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForLongPress()V

    .line 1561
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 1563
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_f

    .line 1564
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v3

    or-int/2addr v2, v3

    or-int v20, v20, v2

    .line 1567
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleVelocityTracker()V

    goto/16 :goto_1

    .line 1434
    :pswitch_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move/from16 v18, v0

    .line 1435
    .restart local v18    # "motionPosition":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v2, v18, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 1437
    .local v12, "child":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 1438
    .restart local v24    # "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 1441
    .restart local v25    # "y":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_17

    .line 1442
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v24, v2

    if-lez v2, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v24, v2

    if-gez v2, :cond_16

    const/16 v16, 0x1

    .line 1447
    .local v16, "inList":Z
    :goto_5
    if-eqz v12, :cond_15

    invoke-virtual {v12}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-nez v2, :cond_15

    if-eqz v16, :cond_15

    .line 1448
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    if-eqz v2, :cond_10

    .line 1449
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1452
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    if-nez v2, :cond_11

    .line 1453
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    .line 1456
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPerformClick:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;

    move-object/from16 v21, v0

    .line 1457
    .local v21, "performClick":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;
    move/from16 v0, v18

    move-object/from16 v1, v21

    iput v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->mClickMotionPosition:I

    .line 1458
    invoke-virtual/range {v21 .. v21}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->rememberWindowAttachCount()V

    .line 1460
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 1462
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1b

    .line 1463
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    if-nez v2, :cond_19

    .line 1464
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForTap()V

    .line 1469
    :goto_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1471
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1472
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1474
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 1475
    move-object/from16 v0, p0

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->positionSelector(ILandroid/view/View;)V

    .line 1476
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_13

    .line 1479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1480
    .local v13, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_13

    instance-of v2, v13, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v2, :cond_13

    .line 1481
    check-cast v13, Landroid/graphics/drawable/TransitionDrawable;

    .end local v13    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v13}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 1485
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v2, :cond_14

    .line 1486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1489
    :cond_14
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v2, v0, v12, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 1505
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1515
    .end local v21    # "performClick":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;
    :cond_15
    :goto_7
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1516
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    goto/16 :goto_4

    .line 1442
    .end local v16    # "inList":Z
    :cond_16
    const/16 v16, 0x0

    goto/16 :goto_5

    .line 1444
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v25, v2

    if-lez v2, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v25, v2

    if-gez v2, :cond_18

    const/16 v16, 0x1

    .restart local v16    # "inList":Z
    :goto_8
    goto/16 :goto_5

    .end local v16    # "inList":Z
    :cond_18
    const/16 v16, 0x0

    goto :goto_8

    .line 1466
    .restart local v16    # "inList":Z
    .restart local v21    # "performClick":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;
    :cond_19
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->cancelCheckForLongPress()V

    goto/16 :goto_6

    .line 1507
    :cond_1a
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1508
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    goto :goto_7

    .line 1510
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1511
    invoke-virtual/range {v21 .. v21}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->run()V

    goto :goto_7

    .line 1522
    .end local v12    # "child":Landroid/view/View;
    .end local v16    # "inList":Z
    .end local v18    # "motionPosition":I
    .end local v21    # "performClick":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;
    .end local v24    # "x":F
    .end local v25    # "y":F
    :pswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->contentFits()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1523
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1524
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1528
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mMaximumVelocity:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1531
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v2, :cond_1d

    .line 1532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    invoke-static {v2, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v23

    .line 1537
    .local v23, "velocity":F
    :goto_9
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFlingVelocity:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_24

    .line 1538
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1539
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    .line 1541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v5, :cond_1e

    const/4 v5, 0x0

    :goto_a
    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v6, :cond_1f

    .end local v23    # "velocity":F
    :goto_b
    move/from16 v0, v23

    float-to-int v6, v0

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v7, :cond_20

    const/4 v7, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v8, :cond_21

    const/4 v8, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v9, :cond_22

    const/high16 v9, -0x80000000

    :goto_e
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v10, :cond_23

    const v10, 0x7fffffff

    :goto_f
    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1544
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchPos:F

    .line 1545
    const/16 v20, 0x1

    goto/16 :goto_4

    .line 1534
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mActivePointerId:I

    invoke-static {v2, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v23

    .restart local v23    # "velocity":F
    goto :goto_9

    :cond_1e
    move/from16 v5, v23

    .line 1541
    goto :goto_a

    :cond_1f
    const/16 v23, 0x0

    goto :goto_b

    .end local v23    # "velocity":F
    :cond_20
    const/high16 v7, -0x80000000

    goto :goto_c

    :cond_21
    const v8, 0x7fffffff

    goto :goto_d

    :cond_22
    const/4 v9, 0x0

    goto :goto_e

    :cond_23
    const/4 v10, 0x0

    goto :goto_f

    .line 1547
    .restart local v23    # "velocity":F
    :cond_24
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1548
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1554
    .end local v23    # "velocity":F
    :pswitch_a
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1555
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1329
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 1391
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 1430
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
    .end packed-switch
.end method

.method public onTouchModeChanged(Z)V
    .locals 2
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 1582
    if-eqz p1, :cond_2

    .line 1584
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hideSelector()V

    .line 1589
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1590
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    .line 1593
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    .line 1604
    :cond_1
    :goto_0
    return-void

    .line 1595
    :cond_2
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 1596
    .local v0, "touchMode":I
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1597
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    if-eqz v1, :cond_1

    .line 1598
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 1599
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->finishEdgeGlows()V

    .line 1600
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasWindowFocus"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 976
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onWindowFocusChanged(Z)V

    .line 978
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 980
    .local v0, "touchMode":I
    :goto_0
    if-nez p1, :cond_2

    .line 981
    if-ne v0, v2, :cond_0

    .line 983
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 1002
    :cond_0
    :goto_1
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchMode:I

    .line 1003
    return-void

    .end local v0    # "touchMode":I
    :cond_1
    move v0, v2

    .line 978
    goto :goto_0

    .line 987
    .restart local v0    # "touchMode":I
    :cond_2
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchMode:I

    if-eq v0, v3, :cond_0

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastTouchMode:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 989
    if-ne v0, v2, :cond_3

    .line 991
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelection()Z

    goto :goto_1

    .line 995
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->hideSelector()V

    .line 996
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 997
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->layoutChildren()V

    goto :goto_1
.end method

.method pageScroll(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1721
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->forceValidFocusDirection(I)V

    .line 1723
    const/4 v0, 0x0

    .line 1724
    .local v0, "forward":Z
    const/4 v1, -0x1

    .line 1726
    .local v1, "nextPage":I
    const/16 v5, 0x21

    if-eq p1, v5, :cond_0

    const/16 v5, 0x11

    if-ne p1, v5, :cond_3

    .line 1727
    :cond_0
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1733
    :cond_1
    :goto_0
    if-gez v1, :cond_5

    .line 1760
    :cond_2
    :goto_1
    return v3

    .line 1728
    :cond_3
    const/16 v5, 0x82

    if-eq p1, v5, :cond_4

    const/16 v5, 0x42

    if-ne p1, v5, :cond_1

    .line 1729
    :cond_4
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1730
    const/4 v0, 0x1

    goto :goto_0

    .line 1737
    :cond_5
    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 1738
    .local v2, "position":I
    if-ltz v2, :cond_2

    .line 1739
    const/4 v3, 0x4

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1740
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v3

    :goto_2
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 1742
    if-eqz v0, :cond_6

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v5

    sub-int/2addr v3, v5

    if-le v2, v3, :cond_6

    .line 1743
    const/4 v3, 0x3

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1746
    :cond_6
    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 1747
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 1750
    :cond_7
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionInt(I)V

    .line 1751
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 1753
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->awakenScrollbarsInternal()Z

    move-result v3

    if-nez v3, :cond_8

    .line 1754
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    :cond_8
    move v3, v4

    .line 1757
    goto :goto_1

    .line 1740
    :cond_9
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v3

    goto :goto_2
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5204
    const/4 v1, 0x0

    .line 5206
    .local v1, "checkedStateChanged":Z
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-ne v4, v5, :cond_6

    .line 5207
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, p2, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 5208
    .local v0, "checked":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 5210
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5211
    if-eqz v0, :cond_4

    .line 5212
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5218
    :cond_0
    :goto_1
    if-eqz v0, :cond_5

    .line 5219
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 5224
    :goto_2
    const/4 v1, 0x1

    .line 5244
    .end local v0    # "checked":Z
    :cond_1
    :goto_3
    if-eqz v1, :cond_2

    .line 5245
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateOnScreenCheckedViews()V

    .line 5248
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v2

    return v2

    :cond_3
    move v0, v3

    .line 5207
    goto :goto_0

    .line 5214
    .restart local v0    # "checked":Z
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 5221
    :cond_5
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    goto :goto_2

    .line 5225
    .end local v0    # "checked":Z
    :cond_6
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-nez v4, :cond_1

    .line 5226
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, p2, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    if-nez v4, :cond_9

    move v0, v2

    .line 5227
    .restart local v0    # "checked":Z
    :goto_4
    if-eqz v0, :cond_a

    .line 5228
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    .line 5229
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p2, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 5231
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 5232
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 5233
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5236
    :cond_7
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 5241
    :cond_8
    :goto_5
    const/4 v1, 0x1

    goto :goto_3

    .end local v0    # "checked":Z
    :cond_9
    move v0, v3

    .line 5226
    goto :goto_4

    .line 5237
    .restart local v0    # "checked":Z
    :cond_a
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 5238
    :cond_b
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    goto :goto_5
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1059
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1060
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 1061
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1062
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1065
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 1066
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 1067
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1069
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 1070
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1072
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1073
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    add-int/2addr v4, v3

    .line 1077
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 1066
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 1077
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method protected recycleOnMeasure()Z
    .locals 1

    .prologue
    .line 3872
    const/4 v0, 0x1

    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 1226
    if-eqz p1, :cond_0

    .line 1227
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->recycleVelocityTracker()V

    .line 1230
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->requestDisallowInterceptTouchEvent(Z)V

    .line 1231
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 3446
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    if-nez v0, :cond_0

    .line 3447
    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    .line 3449
    :cond_0
    return-void
.end method

.method resetState()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 5119
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->removeAllViewsInLayout()V

    .line 5121
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedStart:I

    .line 5122
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 5123
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 5124
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    .line 5125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mPendingSync:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$SavedState;

    .line 5126
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedPosition:I

    .line 5127
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedRowId:J

    .line 5129
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOverScroll:I

    .line 5131
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 5132
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 5134
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorPosition:I

    .line 5135
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 5137
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invalidate()V

    .line 5138
    return-void
.end method

.method resurrectSelection()Z
    .locals 15

    .prologue
    .line 4186
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildCount()I

    move-result v1

    .line 4187
    .local v1, "childCount":I
    if-gtz v1, :cond_0

    .line 4188
    const/4 v13, 0x0

    .line 4263
    :goto_0
    return v13

    .line 4191
    :cond_0
    const/4 v10, 0x0

    .line 4194
    .local v10, "selectedStart":I
    iget-boolean v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v13, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v11

    .line 4195
    .local v11, "start":I
    :goto_1
    iget-boolean v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v13, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getHeight()I

    move-result v13

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingBottom()I

    move-result v14

    sub-int v5, v13, v14

    .line 4197
    .local v5, "end":I
    :goto_2
    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    .line 4198
    .local v6, "firstPosition":I
    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 4199
    .local v12, "toPosition":I
    const/4 v4, 0x1

    .line 4201
    .local v4, "down":Z
    if-lt v12, v6, :cond_5

    add-int v13, v6, v1

    if-ge v12, v13, :cond_5

    .line 4202
    move v9, v12

    .line 4204
    .local v9, "selectedPosition":I
    iget v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v13, v9, v13

    invoke-virtual {p0, v13}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 4205
    .local v8, "selected":Landroid/view/View;
    iget-boolean v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v13, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v10

    .line 4247
    .end local v8    # "selected":Landroid/view/View;
    :cond_1
    :goto_3
    const/4 v13, -0x1

    iput v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    .line 4248
    const/4 v13, -0x1

    iput v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mTouchMode:I

    .line 4249
    const/4 v13, 0x0

    invoke-direct {p0, v13}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->reportScrollStateChange(I)V

    .line 4251
    iput v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 4253
    invoke-direct {p0, v9, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(IZ)I

    move-result v9

    .line 4254
    if-lt v9, v6, :cond_c

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getLastVisiblePosition()I

    move-result v13

    if-gt v9, v13, :cond_c

    .line 4255
    const/4 v13, 0x4

    iput v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 4256
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    .line 4257
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionInt(I)V

    .line 4258
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 4263
    :goto_4
    if-ltz v9, :cond_d

    const/4 v13, 0x1

    goto :goto_0

    .line 4194
    .end local v4    # "down":Z
    .end local v5    # "end":I
    .end local v6    # "firstPosition":I
    .end local v9    # "selectedPosition":I
    .end local v11    # "start":I
    .end local v12    # "toPosition":I
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v11

    goto :goto_1

    .line 4195
    .restart local v11    # "start":I
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getWidth()I

    move-result v13

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingRight()I

    move-result v14

    sub-int v5, v13, v14

    goto :goto_2

    .line 4205
    .restart local v4    # "down":Z
    .restart local v5    # "end":I
    .restart local v6    # "firstPosition":I
    .restart local v8    # "selected":Landroid/view/View;
    .restart local v9    # "selectedPosition":I
    .restart local v12    # "toPosition":I
    :cond_4
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v10

    goto :goto_3

    .line 4206
    .end local v8    # "selected":Landroid/view/View;
    .end local v9    # "selectedPosition":I
    :cond_5
    if-ge v12, v6, :cond_9

    .line 4208
    move v9, v6

    .line 4210
    .restart local v9    # "selectedPosition":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_5
    if-ge v7, v1, :cond_1

    .line 4211
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4212
    .local v0, "child":Landroid/view/View;
    iget-boolean v13, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v13, :cond_7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4214
    .local v3, "childStart":I
    :goto_6
    if-nez v7, :cond_6

    .line 4216
    move v10, v3

    .line 4219
    :cond_6
    if-lt v3, v11, :cond_8

    .line 4221
    add-int v9, v6, v7

    .line 4222
    move v10, v3

    .line 4223
    goto :goto_3

    .line 4212
    .end local v3    # "childStart":I
    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_6

    .line 4210
    .restart local v3    # "childStart":I
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 4227
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childStart":I
    .end local v7    # "i":I
    .end local v9    # "selectedPosition":I
    :cond_9
    add-int v13, v6, v1

    add-int/lit8 v9, v13, -0x1

    .line 4228
    .restart local v9    # "selectedPosition":I
    const/4 v4, 0x0

    .line 4230
    add-int/lit8 v7, v1, -0x1

    .restart local v7    # "i":I
    :goto_7
    if-ltz v7, :cond_1

    .line 4231
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4232
    .restart local v0    # "child":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildStartEdge(Landroid/view/View;)I

    move-result v3

    .line 4233
    .restart local v3    # "childStart":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildEndEdge(Landroid/view/View;)I

    move-result v2

    .line 4235
    .local v2, "childEnd":I
    add-int/lit8 v13, v1, -0x1

    if-ne v7, v13, :cond_a

    .line 4236
    move v10, v3

    .line 4239
    :cond_a
    if-gt v2, v5, :cond_b

    .line 4240
    add-int v9, v6, v7

    .line 4241
    move v10, v3

    .line 4242
    goto :goto_3

    .line 4230
    :cond_b
    add-int/lit8 v7, v7, -0x1

    goto :goto_7

    .line 4260
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childEnd":I
    .end local v3    # "childStart":I
    .end local v7    # "i":I
    :cond_c
    const/4 v9, -0x1

    goto :goto_4

    .line 4263
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method resurrectSelectionIfNeeded()Z
    .locals 1

    .prologue
    .line 4270
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelectedPosition:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resurrectSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4271
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    .line 4272
    const/4 v0, 0x1

    .line 4275
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollBy(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 3498
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->scrollListItemsBy(I)Z

    .line 3499
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .locals 3
    .param p1, "eventType"    # I

    .prologue
    .line 1626
    const/16 v2, 0x1000

    if-ne p1, v2, :cond_1

    .line 1627
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getFirstVisiblePosition()I

    move-result v0

    .line 1628
    .local v0, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getLastVisiblePosition()I

    move-result v1

    .line 1630
    .local v1, "lastVisiblePosition":I
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastAccessibilityScrollEventFromIndex:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastAccessibilityScrollEventToIndex:I

    if-ne v2, v1, :cond_0

    .line 1639
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :goto_0
    return-void

    .line 1633
    .restart local v0    # "firstVisiblePosition":I
    .restart local v1    # "lastVisiblePosition":I
    :cond_0
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastAccessibilityScrollEventFromIndex:I

    .line 1634
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLastAccessibilityScrollEventToIndex:I

    .line 1638
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 101
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 6
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 744
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 745
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 748
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resetState()V

    .line 749
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->clear()V

    .line 751
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    .line 752
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 754
    const/4 v1, -0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedPosition:I

    .line 755
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldSelectedRowId:J

    .line 757
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_1

    .line 758
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_2

    .line 762
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 765
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_5

    .line 766
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOldItemCount:I

    .line 767
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 769
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    .line 770
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataSetObserver:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 772
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->setViewTypeCount(I)V

    .line 774
    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mHasStableIds:Z

    .line 775
    invoke-interface {p1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAreAllItemsSelectable:Z

    .line 777
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mHasStableIds:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v1, :cond_3

    .line 778
    new-instance v1, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v1}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 781
    :cond_3
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(I)I

    move-result v0

    .line 782
    .local v0, "position":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectedPositionInt(I)V

    .line 783
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 785
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    if-nez v1, :cond_4

    .line 786
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkSelectionChanged()V

    .line 796
    .end local v0    # "position":I
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkFocus()V

    .line 797
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    .line 798
    return-void

    .line 789
    :cond_5
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemCount:I

    .line 790
    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mHasStableIds:Z

    .line 791
    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAreAllItemsSelectable:Z

    .line 793
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->checkSelectionChanged()V

    goto :goto_0
.end method

.method public setChoiceMode(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;)V
    .locals 2
    .param p1, "choiceMode"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    .prologue
    .line 724
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    .line 726
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-eq v0, v1, :cond_1

    .line 727
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_0

    .line 728
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 732
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 735
    :cond_1
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0
    .param p1, "drawSelectorOnTop"    # Z

    .prologue
    .line 490
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDrawSelectorOnTop:Z

    .line 491
    return-void
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 0
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .line 5788
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    .line 5789
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEmptyView:Landroid/view/View;

    .line 5790
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateEmptyStatus()V

    .line 5791
    return-void
.end method

.method public setFocusable(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5795
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 5796
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v1, v3

    .line 5798
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableState:Z

    .line 5799
    if-nez p1, :cond_1

    .line 5800
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableInTouchModeState:Z

    .line 5803
    :cond_1
    if-eqz p1, :cond_3

    if-nez v1, :cond_3

    :goto_1
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusable(Z)V

    .line 5804
    return-void

    .end local v1    # "empty":Z
    :cond_2
    move v1, v2

    .line 5796
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_3
    move v3, v2

    .line 5803
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 5808
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 5809
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v1, v3

    .line 5811
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableInTouchModeState:Z

    .line 5812
    if-eqz p1, :cond_1

    .line 5813
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDesiredFocusableState:Z

    .line 5816
    :cond_1
    if-eqz p1, :cond_3

    if-nez v1, :cond_3

    :goto_1
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusableInTouchMode(Z)V

    .line 5817
    return-void

    .end local v1    # "empty":Z
    :cond_2
    move v1, v2

    .line 5809
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_3
    move v3, v2

    .line 5816
    goto :goto_1
.end method

.method public setItemChecked(IZ)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 635
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-ne v4, v5, :cond_1

    .line 692
    :cond_0
    :goto_0
    return-void

    .line 639
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mChoiceMode:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v5, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    if-ne v4, v5, :cond_6

    .line 640
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 641
    .local v0, "oldValue":Z
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 643
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 644
    if-eqz p2, :cond_4

    .line 645
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 651
    :cond_2
    :goto_1
    if-eq v0, p2, :cond_3

    .line 652
    if-eqz p2, :cond_5

    .line 653
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    .line 687
    .end local v0    # "oldValue":Z
    :cond_3
    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mInLayout:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mBlockLayoutRequests:Z

    if-nez v3, :cond_0

    .line 688
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mDataChanged:Z

    .line 689
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->rememberSyncState()V

    .line 690
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    goto :goto_0

    .line 647
    .restart local v0    # "oldValue":Z
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 655
    :cond_5
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    goto :goto_2

    .line 659
    .end local v0    # "oldValue":Z
    :cond_6
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v4

    if-eqz v4, :cond_a

    move v1, v2

    .line 663
    .local v1, "updateIds":Z
    :goto_3
    if-nez p2, :cond_7

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isItemChecked(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 664
    :cond_7
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->clear()V

    .line 666
    if-eqz v1, :cond_8

    .line 667
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 673
    :cond_8
    if-eqz p2, :cond_b

    .line 674
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 676
    if-eqz v1, :cond_9

    .line 677
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 680
    :cond_9
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    goto :goto_2

    .end local v1    # "updateIds":Z
    :cond_a
    move v1, v3

    .line 659
    goto :goto_3

    .line 681
    .restart local v1    # "updateIds":Z
    :cond_b
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 682
    :cond_c
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mCheckedItemCount:I

    goto :goto_2
.end method

.method public setItemMargin(I)V
    .locals 1
    .param p1, "itemMargin"    # I

    .prologue
    .line 431
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    if-ne v0, p1, :cond_0

    .line 437
    :goto_0
    return-void

    .line 435
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemMargin:I

    .line 436
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    goto :goto_0
.end method

.method public setItemsCanFocus(Z)V
    .locals 1
    .param p1, "itemsCanFocus"    # Z

    .prologue
    .line 449
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mItemsCanFocus:Z

    .line 450
    if-nez p1, :cond_0

    .line 451
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setDescendantFocusability(I)V

    .line 453
    :cond_0
    return-void
.end method

.method public setOnScrollListener(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    .prologue
    .line 468
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mOnScrollListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;

    .line 469
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->invokeOnItemScrollListener()V

    .line 470
    return-void
.end method

.method public setOrientation(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;)V
    .locals 2
    .param p1, "orientation"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    .prologue
    .line 413
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->VERTICAL:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 414
    .local v0, "isVertical":Z
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-ne v1, v0, :cond_1

    .line 424
    :goto_1
    return-void

    .line 413
    .end local v0    # "isVertical":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 418
    .restart local v0    # "isVertical":Z
    :cond_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->resetState()V

    .line 421
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->clear()V

    .line 423
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    goto :goto_1
.end method

.method public setOverScrollMode(I)V
    .locals 4
    .param p1, "mode"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1039
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    .line 1056
    :goto_0
    return-void

    .line 1043
    :cond_0
    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    .line 1044
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-nez v1, :cond_1

    .line 1045
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1047
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1048
    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1055
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setOverScrollMode(I)V

    goto :goto_0

    .line 1051
    :cond_2
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1052
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_1
.end method

.method public setRecyclerListener(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;)V
    .locals 1
    .param p1, "l"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    .prologue
    .line 480
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mRecycler:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->access$102(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    .line 481
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3462
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelectionFromOffset(II)V

    .line 3463
    return-void
.end method

.method public setSelectionFromOffset(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    .line 3466
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 3495
    :cond_0
    :goto_0
    return-void

    .line 3470
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3471
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->lookForSelectablePosition(I)I

    move-result p1

    .line 3472
    if-ltz p1, :cond_2

    .line 3473
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setNextSelectedPositionInt(I)V

    .line 3479
    :cond_2
    :goto_1
    if-ltz p1, :cond_0

    .line 3480
    const/4 v0, 0x4

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mLayoutMode:I

    .line 3482
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mIsVertical:Z

    if-eqz v0, :cond_5

    .line 3483
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    .line 3488
    :goto_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mNeedSync:Z

    if-eqz v0, :cond_3

    .line 3489
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncPosition:I

    .line 3490
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSyncRowId:J

    .line 3493
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->requestLayout()V

    goto :goto_0

    .line 3476
    :cond_4
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mResurrectToPosition:I

    goto :goto_1

    .line 3485
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSpecificStart:I

    goto :goto_2
.end method

.method public setSelector(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 501
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "selector"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 510
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 511
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 512
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 515
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 516
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 517
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 519
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 520
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->updateSelectorState()V

    .line 521
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 7
    .param p1, "originalView"    # Landroid/view/View;

    .prologue
    .line 1202
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 1203
    .local v3, "longPressPosition":I
    if-ltz v3, :cond_2

    .line 1204
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1205
    .local v4, "longPressId":J
    const/4 v6, 0x0

    .line 1207
    .local v6, "handled":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    .line 1208
    .local v0, "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v2, p1

    .line 1209
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 1212
    :cond_0
    if-nez v6, :cond_1

    .line 1213
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mFirstPosition:I

    sub-int v1, v3, v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1215
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 1221
    .end local v0    # "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    .end local v4    # "longPressId":J
    .end local v6    # "handled":Z
    :cond_1
    :goto_0
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method
