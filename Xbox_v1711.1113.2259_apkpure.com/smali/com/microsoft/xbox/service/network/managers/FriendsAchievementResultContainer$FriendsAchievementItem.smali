.class public Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;
.super Ljava/lang/Object;
.source "FriendsAchievementResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FriendsAchievementItem"
.end annotation


# instance fields
.field public achievementIcon:Ljava/lang/String;

.field public achievementId:I

.field public achievementName:Ljava/lang/String;

.field public achievementScid:Ljava/lang/String;

.field public activityItemType:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public date:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;
    .end annotation
.end field

.field public displayName:Ljava/lang/String;

.field public gamerscore:I

.field public isSecret:Z

.field public platform:Ljava/lang/String;

.field public titleId:I

.field public userXuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAchievementIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->achievementIcon:Ljava/lang/String;

    return-object v0
.end method
