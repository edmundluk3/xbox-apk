.class abstract Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;
.super Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
.source "$AutoValue_ActivityHubDataTypes_TrendingTopic.java"


# instance fields
.field private final caption:Ljava/lang/String;

.field private final displayImage:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final sourceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "sourceId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "displayImage"    # Ljava/lang/String;
    .param p4, "caption"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null sourceId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->sourceId:Ljava/lang/String;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->name:Ljava/lang/String;

    .line 27
    if-nez p3, :cond_2

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null displayImage"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->displayImage:Ljava/lang/String;

    .line 31
    if-nez p4, :cond_3

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null caption"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->caption:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public caption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->caption:Ljava/lang/String;

    return-object v0
.end method

.method public displayImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->displayImage:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 72
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 73
    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    .line 74
    .local v0, "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->sourceId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->sourceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->name:Ljava/lang/String;

    .line 75
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->displayImage:Ljava/lang/String;

    .line 76
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->displayImage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->caption:Ljava/lang/String;

    .line 77
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->caption()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    :cond_3
    move v1, v2

    .line 79
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 84
    const/4 v0, 0x1

    .line 85
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->sourceId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 87
    mul-int/2addr v0, v2

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 89
    mul-int/2addr v0, v2

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->displayImage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 91
    mul-int/2addr v0, v2

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->caption:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 93
    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->name:Ljava/lang/String;

    return-object v0
.end method

.method public sourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->sourceId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TrendingTopic{sourceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->sourceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->displayImage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", caption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingTopic;->caption:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
