.class public Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubCardCategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClubCardCategoryViewHolder"
.end annotation


# instance fields
.field private clubList:Landroid/support/v7/widget/RecyclerView;

.field private header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

.field private seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;Landroid/view/View;)V
    .locals 4
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    .line 61
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 63
    const v0, 0x7f0e02c6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 64
    const v0, 0x7f0e02c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 65
    const v0, 0x7f0e02c8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 75
    return-void
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 81
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverSeeAll(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V

    .line 83
    const-string v0, "Clubs - Discover See All Game Suggested Clubs"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
    .param p1, "cardModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    .prologue
    .line 70
    const-string v0, "Clubs - Navigate To Suggested Club"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackNavigateToClub(Ljava/lang/String;J)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 72
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->clear()V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->getClubModels()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->addAll(Ljava/util/Collection;)V

    .line 89
    return-void
.end method
