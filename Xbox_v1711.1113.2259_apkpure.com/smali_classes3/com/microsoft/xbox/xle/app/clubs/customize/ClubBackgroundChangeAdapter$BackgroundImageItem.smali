.class public interface abstract Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;
.super Ljava/lang/Object;
.source "ClubBackgroundChangeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BackgroundImageItem"
.end annotation


# static fields
.field public static final ACHIEVEMENT:I = 0x1

.field public static final SCREENSHOT:I


# virtual methods
.method public abstract getDate()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getGamerScore()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getImageURI()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getItemType()I
.end method

.method public abstract getSubTitle()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getThumbnailImageURI()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getTitle()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
