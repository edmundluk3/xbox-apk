.class public Lcom/microsoft/xbox/smartglass/SessionManager;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "SessionManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/SessionManagerListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _clientInfo:Lcom/microsoft/xbox/smartglass/ClientInformation;

.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

.field private _virtualDeviceId:Ljava/lang/String;


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/smartglass/ClientInformation;)V
    .locals 11
    .param p1, "pPlatform"    # J
    .param p3, "clientInfo"    # Lcom/microsoft/xbox/smartglass/ClientInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 17
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    iget v4, p3, Lcom/microsoft/xbox/smartglass/ClientInformation;->version:I

    iget v5, p3, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMajorVersion:I

    iget v6, p3, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMinorVersion:I

    iget-object v7, p3, Lcom/microsoft/xbox/smartglass/ClientInformation;->displayName:Ljava/lang/String;

    iget-object v1, p3, Lcom/microsoft/xbox/smartglass/ClientInformation;->capabilities:Ljava/util/EnumSet;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->getValue(Ljava/util/EnumSet;)J

    move-result-wide v8

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/smartglass/SessionManager;->initialize(JIIILjava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 18
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_clientInfo:Lcom/microsoft/xbox/smartglass/ClientInformation;

    .line 19
    return-void
.end method

.method private native connect(JSSSSII)V
.end method

.method private native connectToAddress(JSSSSLjava/lang/String;II)V
.end method

.method private native connectToSpecificDevice(JSSSSJII)V
.end method

.method private native disconnect(J)V
.end method

.method private native getActiveTitles(J)[Lcom/microsoft/xbox/smartglass/ActiveTitleState;
.end method

.method private native getClientResolution(J)Lcom/microsoft/xbox/smartglass/ClientResolution;
.end method

.method private native getConnectionState(J)I
.end method

.method private native getMediaStates(J)[Lcom/microsoft/xbox/smartglass/MediaState;
.end method

.method private native getPairedIdentityState(J)I
.end method

.method private native getPrimaryDevice(J)Lcom/microsoft/xbox/smartglass/PrimaryDevice;
.end method

.method private native getPrimaryDeviceState(J)Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;
.end method

.method private native getVirtualDeviceId(J)Ljava/lang/String;
.end method

.method private native initialize(JIIILjava/lang/String;J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native launchTitle(JILjava/lang/String;I)V
.end method

.method private native launchUri(JLjava/lang/String;I)V
.end method

.method private onChannelEstablished(IIII)V
    .locals 4
    .param p1, "titleId"    # I
    .param p2, "service"    # I
    .param p3, "independentError"    # I
    .param p4, "platformError"    # I

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 249
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->fromInt(I)Lcom/microsoft/xbox/smartglass/ServiceChannel;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(ILcom/microsoft/xbox/smartglass/ServiceChannel;)V

    new-instance v3, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v3, p3, p4}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V

    goto :goto_0

    .line 251
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onConnectionStateChanged(III)V
    .locals 4
    .param p1, "connectionState"    # I
    .param p2, "independentError"    # I
    .param p3, "platformError"    # I

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 237
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/ConnectionState;->fromInt(I)Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v3, p2, p3}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V

    goto :goto_0

    .line 239
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 3
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 268
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V

    goto :goto_0

    .line 270
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 3
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 262
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V

    goto :goto_0

    .line 264
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onPairedIdentityStateChanged(III)V
    .locals 4
    .param p1, "pairedIdentityState"    # I
    .param p2, "independentError"    # I
    .param p3, "platformError"    # I

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 243
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->fromInt(I)Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {v3, p2, p3}, Lcom/microsoft/xbox/smartglass/SGResult;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V

    goto :goto_0

    .line 245
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onPrimaryDeviceStateChanged(IIILjava/lang/String;I)V
    .locals 7
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I
    .param p3, "buildNumber"    # I
    .param p4, "locale"    # Ljava/lang/String;
    .param p5, "tvProvider"    # I

    .prologue
    .line 229
    new-instance v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;-><init>(IIILjava/lang/String;I)V

    .line 230
    .local v0, "state":Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 231
    .local v6, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onPrimaryDeviceStateChanged(Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;)V

    goto :goto_0

    .line 233
    .end local v6    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private onTitleChanged(IIZLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "titleId"    # I
    .param p2, "titleLocation"    # I
    .param p3, "hasFocus"    # Z
    .param p4, "productId"    # Ljava/lang/String;
    .param p5, "aumId"    # Ljava/lang/String;

    .prologue
    .line 254
    new-instance v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/ActiveTitleState;-><init>(IIZLjava/lang/String;Ljava/lang/String;)V

    .line 255
    .local v0, "state":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/SessionManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    .line 256
    .local v6, "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;->onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    goto :goto_0

    .line 258
    .end local v6    # "listener":Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    :cond_0
    return-void
.end method

.method private native sendGamePad(JIFFFFFFZ)V
.end method

.method private native sendMediaCommand(JIIJ)V
.end method

.method private native sendTitleMessage(JLjava/lang/String;II)V
.end method

.method private native startChannel(JIII)V
.end method

.method private native stopChannel(JII)V
.end method

.method private native unsnap(J)V
.end method


# virtual methods
.method public connect(Lcom/microsoft/xbox/smartglass/ClientResolution;Lcom/microsoft/xbox/smartglass/ReconnectPolicy;)V
    .locals 10
    .param p1, "resolution"    # Lcom/microsoft/xbox/smartglass/ClientResolution;
    .param p2, "reconnectPolicy"    # Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .prologue
    const/4 v0, 0x0

    .line 136
    if-nez p1, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "resolution cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    if-nez p2, :cond_1

    .line 141
    new-instance p2, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .end local p2    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    invoke-direct {p2, v0, v0}, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;-><init>(II)V

    .line 144
    .restart local p2    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-short v4, p1, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeWidth:S

    iget-short v5, p1, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeHeight:S

    iget-short v6, p1, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiX:S

    iget-short v7, p1, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiY:S

    iget v8, p2, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->maxRetryCount:I

    iget v9, p2, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->retryInterval:I

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/smartglass/SessionManager;->connect(JSSSSII)V

    .line 145
    return-void
.end method

.method public connect(Lcom/microsoft/xbox/smartglass/PrimaryDevice;Lcom/microsoft/xbox/smartglass/ClientResolution;Lcom/microsoft/xbox/smartglass/ReconnectPolicy;)V
    .locals 12
    .param p1, "primaryDevice"    # Lcom/microsoft/xbox/smartglass/PrimaryDevice;
    .param p2, "resolution"    # Lcom/microsoft/xbox/smartglass/ClientResolution;
    .param p3, "reconnectPolicy"    # Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .prologue
    const/4 v0, 0x0

    .line 101
    if-nez p1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "primaryDevice cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    if-nez p3, :cond_1

    .line 106
    new-instance p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .end local p3    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    invoke-direct {p3, v0, v0}, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;-><init>(II)V

    .line 109
    .restart local p3    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-short v4, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeWidth:S

    iget-short v5, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeHeight:S

    iget-short v6, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiX:S

    iget-short v7, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiY:S

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->getPrimaryDevice()Lcom/microsoft/xbox/smartglass/RefTPtr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v8

    iget v10, p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->maxRetryCount:I

    iget v11, p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->retryInterval:I

    move-object v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/microsoft/xbox/smartglass/SessionManager;->connectToSpecificDevice(JSSSSJII)V

    .line 110
    return-void
.end method

.method public connect(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ClientResolution;Lcom/microsoft/xbox/smartglass/ReconnectPolicy;)V
    .locals 11
    .param p1, "serverAddress"    # Ljava/lang/String;
    .param p2, "resolution"    # Lcom/microsoft/xbox/smartglass/ClientResolution;
    .param p3, "reconnectPolicy"    # Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .prologue
    const/4 v0, 0x0

    .line 119
    if-nez p2, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "resolution cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    if-nez p3, :cond_1

    .line 124
    new-instance p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    .end local p3    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    invoke-direct {p3, v0, v0}, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;-><init>(II)V

    .line 127
    .restart local p3    # "reconnectPolicy":Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-short v4, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeWidth:S

    iget-short v5, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeHeight:S

    iget-short v6, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiX:S

    iget-short v7, p2, Lcom/microsoft/xbox/smartglass/ClientResolution;->dpiY:S

    iget v9, p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->maxRetryCount:I

    iget v10, p3, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->retryInterval:I

    move-object v1, p0

    move-object v8, p1

    invoke-direct/range {v1 .. v10}, Lcom/microsoft/xbox/smartglass/SessionManager;->connectToAddress(JSSSSLjava/lang/String;II)V

    .line 128
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->disconnect(J)V

    .line 150
    return-void
.end method

.method public getActiveTitles()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles(J)[Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAuxiliaryStream(I)Lcom/microsoft/xbox/smartglass/AuxiliaryStream;
    .locals 4
    .param p1, "titleId"    # I

    .prologue
    .line 91
    new-instance v0, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p1}, Lcom/microsoft/xbox/smartglass/AuxiliaryStream;-><init>(JI)V

    return-object v0
.end method

.method getClientInfo()Lcom/microsoft/xbox/smartglass/ClientInformation;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_clientInfo:Lcom/microsoft/xbox/smartglass/ClientInformation;

    return-object v0
.end method

.method public getClientResolution()Lcom/microsoft/xbox/smartglass/ClientResolution;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getClientResolution(J)Lcom/microsoft/xbox/smartglass/ClientResolution;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState(J)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/ConnectionState;->fromInt(I)Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v0

    return-object v0
.end method

.method public getMediaStates()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getMediaStates(J)[Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPairedIdentityState(J)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->fromInt(I)Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryDevice()Lcom/microsoft/xbox/smartglass/PrimaryDevice;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPrimaryDevice(J)Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryDeviceState()Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPrimaryDeviceState(J)Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    move-result-object v0

    return-object v0
.end method

.method public getVirtualDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_virtualDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getVirtualDeviceId(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_virtualDeviceId:Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_virtualDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public launch(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    .locals 7
    .param p1, "titleId"    # I
    .param p2, "parameters"    # Ljava/lang/String;
    .param p3, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    invoke-virtual {p3}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v6

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/SessionManager;->launchTitle(JILjava/lang/String;I)V

    .line 196
    return-void
.end method

.method public launch(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v2

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->launchUri(JLjava/lang/String;I)V

    .line 205
    return-void
.end method

.method public sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V
    .locals 12
    .param p1, "gamePad"    # Lcom/microsoft/xbox/smartglass/GamePad;
    .param p2, "simulateButtonPress"    # Z

    .prologue
    .line 175
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/GamePad;->buttons:Ljava/util/EnumSet;

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->getValue(Ljava/util/EnumSet;)I

    move-result v4

    .line 177
    .local v4, "buttonsValue":I
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget v5, p1, Lcom/microsoft/xbox/smartglass/GamePad;->leftTrigger:F

    iget v6, p1, Lcom/microsoft/xbox/smartglass/GamePad;->rightTrigger:F

    iget v7, p1, Lcom/microsoft/xbox/smartglass/GamePad;->leftThumbstickX:F

    iget v8, p1, Lcom/microsoft/xbox/smartglass/GamePad;->leftThumbstickY:F

    iget v9, p1, Lcom/microsoft/xbox/smartglass/GamePad;->rightThumbstickX:F

    iget v10, p1, Lcom/microsoft/xbox/smartglass/GamePad;->rightThumbstickY:F

    move-object v1, p0

    move v11, p2

    invoke-direct/range {v1 .. v11}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendGamePad(JIFFFFFFZ)V

    .line 178
    return-void
.end method

.method public sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V
    .locals 8
    .param p1, "mediaCommand"    # Lcom/microsoft/xbox/smartglass/MediaCommand;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget v4, p1, Lcom/microsoft/xbox/smartglass/MediaCommand;->titleId:I

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MediaCommand;->command:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v5

    iget-wide v6, p1, Lcom/microsoft/xbox/smartglass/MediaCommand;->seekPosition:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(JIIJ)V

    .line 186
    return-void
.end method

.method public sendTitleMessage(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 7
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget v5, p2, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v6

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendTitleMessage(JLjava/lang/String;II)V

    .line 221
    return-void
.end method

.method public startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V
    .locals 7
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "activityId"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget v4, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v5

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/smartglass/SessionManager;->startChannel(JIII)V

    .line 159
    return-void
.end method

.method public stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 4
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    iget v2, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget-object v3, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->getValue()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(JII)V

    .line 167
    return-void
.end method

.method public unsnap()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/SessionManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->unsnap(J)V

    .line 212
    return-void
.end method
