.class Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubChatScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 445
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 422
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 426
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getSelectionEnd()I

    move-result v0

    .line 428
    .local v0, "cursor":I
    :goto_0
    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x40

    if-eq v2, v3, :cond_0

    .line 429
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 432
    :cond_0
    if-lez v0, :cond_1

    .line 433
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getSelectionEnd()I

    move-result v2

    invoke-interface {p1, v0, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 434
    .local v1, "term":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/16 v3, 0xf

    if-gt v2, v3, :cond_1

    .line 435
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Ljava/lang/String;I)V

    .line 441
    .end local v1    # "term":Ljava/lang/CharSequence;
    :goto_1
    return-void

    .line 440
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;Z)V

    goto :goto_1
.end method
