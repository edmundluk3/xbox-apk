.class public final enum Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
.super Ljava/lang/Enum;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TournamentState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

.field public static final enum Canceled:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

.field public static final enum CheckinStarted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

.field public static final enum Completed:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

.field public static final enum Started:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    const-string v1, "CheckinStarted"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->CheckinStarted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    const-string v1, "Started"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Started:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    const-string v1, "Completed"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Completed:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    const-string v1, "Canceled"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Canceled:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->CheckinStarted:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Started:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Completed:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->Canceled:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->$VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->$VALUES:[Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    return-object v0
.end method
