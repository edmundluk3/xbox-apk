.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;
.super Ljava/lang/Object;
.source "LocalizedGamePropertyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalizedValue"
.end annotation


# instance fields
.field public final localized:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Localized"
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Value"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "localized"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "value"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->localized:Ljava/lang/String;

    .line 259
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->value:Ljava/lang/String;

    .line 260
    return-void
.end method
