.class public Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;
.source "RelatedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final A1:F = 1.0f

.field private static final A2:F = 1.369863f

.field private static final B1:F = 35.0f

.field private static final B2:F = 35.0f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public getExtraDp(F)F
    .locals 2
    .param p1, "ar"    # F

    .prologue
    .line 51
    const/4 v0, 0x0

    mul-float/2addr v0, p1

    const v1, -0x4142a150

    div-float/2addr v0, v1

    const/high16 v1, 0x420c0000    # 35.0f

    add-float/2addr v0, v1

    return v0
.end method

.method public bridge synthetic getRelated()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRelated()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public onLoadRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->onLoadRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 39
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelated()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 40
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;

    const/16 v1, 0x21

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->adjustPaneSize(Ljava/lang/Class;I)V

    .line 42
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getRelatedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 30
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->onStartOverride()V

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 23
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getRelatedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 25
    :cond_0
    return-void
.end method
