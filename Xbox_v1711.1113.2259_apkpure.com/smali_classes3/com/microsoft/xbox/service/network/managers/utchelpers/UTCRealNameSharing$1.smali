.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;
.super Ljava/lang/Object;
.source "UTCRealNameSharing.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingSaveFriends(Ljava/util/ArrayList;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$following:Ljava/util/ArrayList;

.field final synthetic val$selected:I

.field final synthetic val$unselected:I


# direct methods
.method constructor <init>(Ljava/util/ArrayList;II)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$following:Ljava/util/ArrayList;

    iput p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$selected:I

    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$unselected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 9

    .prologue
    .line 42
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$following:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 43
    .local v6, "total":I
    const/4 v2, 0x0

    .line 44
    .local v2, "initialSelected":I
    const/4 v3, 0x0

    .line 47
    .local v3, "initialUnselected":I
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$following:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    .line 48
    .local v5, "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    iget-boolean v8, v5, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isIdentityShared:Z

    if-eqz v8, :cond_0

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 55
    .end local v5    # "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    :cond_1
    iget v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$selected:I

    add-int/2addr v7, v2

    iget v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$unselected:I

    sub-int v0, v7, v8

    .line 56
    .local v0, "finalSelected":I
    iget v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$unselected:I

    add-int/2addr v7, v3

    iget v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing$1;->val$selected:I

    sub-int v1, v7, v8

    .line 58
    .local v1, "finalUnselected":I
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 59
    .local v4, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v7, "realNameTotalFriendCount"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    const-string v7, "realNameSelectedFriendCount"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    const-string v7, "realNameUnselectedFriendCount"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    const-string v7, "Real Name Share - Save Friends"

    invoke-static {v7, v4}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 64
    return-void
.end method
