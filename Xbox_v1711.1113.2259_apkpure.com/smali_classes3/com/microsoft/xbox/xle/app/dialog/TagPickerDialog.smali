.class public Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "TagPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;,
        Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
    }
.end annotation


# static fields
.field public static final CLUBS_MAX_TAGS:I = 0xa

.field public static final LFG_MAX_TAGS:I = 0xf

.field private static final TAG_INPUT_FILTER:Landroid/text/InputFilter;


# instance fields
.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private currentTagCountView:Landroid/widget/TextView;

.field private doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private doneHandler:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

.field private maxTags:I

.field private pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private final pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

.field private searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

.field private searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private searchResultListView:Landroid/support/v7/widget/RecyclerView;

.field private searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

.field private selectedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

.field private selectedTagsList:Landroid/support/v7/widget/RecyclerView;

.field private tagSelectionListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

.field private viewModelBases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$$Lambda$4;->lambdaFactory$()Landroid/text/InputFilter;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->TAG_INPUT_FILTER:Landroid/text/InputFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "pivotState"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 91
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 92
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 93
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 94
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    const/16 v0, 0xf

    iput v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    .line 98
    const v0, 0x7f030227

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->setContentView(I)V

    .line 100
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->viewModelBases:Ljava/util/List;

    .line 104
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    .line 106
    const v0, 0x7f0e0aa7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setHint(Ljava/lang/String;)V

    .line 108
    const v0, 0x7f0e0673

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 110
    const v0, 0x7f0e0aa8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    .line 112
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 114
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->tagSelectionListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    const v0, 0x7f0e0aaa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultListView:Landroid/support/v7/widget/RecyclerView;

    .line 141
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->getSelectedTagColor()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultListView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultListView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 166
    const v0, 0x7f0e0aa9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTabLayoutBackgroundColor(I)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->systemTagsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    new-instance v1, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreen;-><init>()V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->appendPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->trendingTagsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    new-instance v1, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->titleId()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->appendPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->achievementTagsEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    new-instance v1, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorScreen;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->titleId()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorScreen;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->appendPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->onCreate()V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderVisibility(Z)V

    .line 183
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->getSelectedTagColor()I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-direct {v0, v4, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(IILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setVerticalPadding(I)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setHasCancel(Z)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 208
    const v0, 0x7f0e0672

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsList:Landroid/support/v7/widget/RecyclerView;

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, v4, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 213
    const v0, 0x7f0e0aa5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    const v0, 0x7f0e0aab

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->doneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-array v1, v5, [Landroid/text/InputFilter;

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->TAG_INPUT_FILTER:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setInputFilters([Landroid/text/InputFilter;)V

    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showSearchBar()V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showTagListView()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showKeyboard(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showTagListView()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->hideSoftKeyBoardFromDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->updateSelectedTags()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->clearSearchResults()V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->viewModelBases:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultListView:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->addCustomTag(Ljava/lang/String;)V

    return-void
.end method

.method private addCustomTag(Ljava/lang/String;)V
    .locals 4
    .param p1, "customTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 410
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 412
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 413
    sget-object v2, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getSystemTags()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .line 414
    .local v0, "systemTag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 417
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 421
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->clearSearchResults()V

    .line 422
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showTagListView()V

    .line 435
    .end local v0    # "systemTag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :goto_0
    return-void

    .line 428
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    move-result-object v1

    .line 429
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 430
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 434
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->clearSearchResults()V

    goto :goto_0
.end method

.method private clearSearchResults()V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultListView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchResultTagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 398
    return-void
.end method

.method private dismissKeyboard()V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showTagListView()V

    .line 402
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->hideSoftKeyBoardFromDialog()V

    .line 403
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 438
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissTagPickerDialog()V

    .line 439
    return-void
.end method

.method private getSelectedTagColor()I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 380
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 382
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    .line 385
    :goto_0
    return v1

    :cond_0
    const v1, -0x777778

    goto :goto_0
.end method

.method private hideSoftKeyBoardFromDialog()V
    .locals 4

    .prologue
    .line 442
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 443
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 445
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 446
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 448
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->clearSearchResults()V

    .line 137
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showSearchBar()V

    .line 138
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->showKeyboard(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    if-eqz v0, :cond_1

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackSearchTag(Ljava/util/List;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->doneHandler:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    .line 223
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 222
    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;->onTagsSelected(Ljava/util/List;)V

    .line 226
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->dismissSelf()V

    .line 227
    return-void
.end method

.method static synthetic lambda$static$0(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 2
    .param p0, "source"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "dest"    # Landroid/text/Spanned;
    .param p4, "dstart"    # I
    .param p5, "dend"    # I

    .prologue
    .line 78
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 79
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    const-string v1, ""

    .line 83
    :goto_1
    return-object v1

    .line 78
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 7
    .param p1, "tag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    if-ge v0, v1, :cond_0

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->updateSelectedTags()V

    .line 279
    :goto_0
    return-void

    .line 273
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0701d4

    .line 274
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0701d3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 276
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 273
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private showKeyboard(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 406
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showKeyboard(Landroid/view/View;I)V

    .line 407
    return-void
.end method

.method private showSearchBar()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clear()V

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setHint(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    return-void
.end method

.method private showTagListView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    return-void
.end method

.method private updateSelectedTags()V
    .locals 5

    .prologue
    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTagsAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 455
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    .line 456
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 455
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 457
    return-void
.end method


# virtual methods
.method public getMaxTags()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    return v0
.end method

.method public getPivotState()Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotState:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->dismissSelf()V

    .line 289
    return-void
.end method

.method protected onStart()V
    .locals 6

    .prologue
    .line 293
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 294
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->onStart()V

    .line 296
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->viewModelBases:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 298
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getTotalPaneCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 299
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getPaneAtIndex(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 301
    .local v1, "pivotPane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    instance-of v4, v1, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    if-eqz v4, :cond_0

    move-object v2, v1

    .line 302
    check-cast v2, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 304
    .local v2, "pivotPaneActivity":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    if-eqz v4, :cond_0

    .line 305
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    .line 306
    .local v3, "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->tagSelectionListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 307
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->viewModelBases:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    .end local v2    # "pivotPaneActivity":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .end local v3    # "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 312
    .end local v1    # "pivotPane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v5, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 359
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v5, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$5;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V

    .line 370
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 374
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 375
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->pivotControl:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->onStop()V

    .line 376
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 284
    return-void
.end method

.method public setMaxTags(I)V
    .locals 5
    .param p1, "maxTags"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 239
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 241
    iput p1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->currentTagCountView:Landroid/widget/TextView;

    const-string v1, "%d/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->selectedTags:Ljava/util/List;

    .line 243
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->maxTags:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 244
    return-void
.end method
