.class public interface abstract Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PeerConnectionEvents"
.end annotation


# virtual methods
.method public abstract onIceCandidate(Lorg/webrtc/IceCandidate;)V
.end method

.method public abstract onIceCandidatesRemoved([Lorg/webrtc/IceCandidate;)V
.end method

.method public abstract onIceConnected()V
.end method

.method public abstract onIceDisconnected()V
.end method

.method public abstract onLocalDescription(Lorg/webrtc/SessionDescription;)V
.end method

.method public abstract onPeerConnectionClosed()V
.end method

.method public abstract onPeerConnectionError(Ljava/lang/String;)V
.end method

.method public abstract onPeerConnectionStatsReady([Lorg/webrtc/StatsReport;)V
.end method
