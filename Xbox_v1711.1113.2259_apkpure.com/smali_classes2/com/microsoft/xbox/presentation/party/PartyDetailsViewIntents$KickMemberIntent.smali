.class public abstract Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;
.super Ljava/lang/Object;
.source "PartyDetailsViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "KickMemberIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(J)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;
    .locals 2
    .param p0, "memberXuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 112
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 113
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewIntents_KickMemberIntent;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewIntents_KickMemberIntent;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public abstract memberXuid()J
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
