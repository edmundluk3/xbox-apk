.class Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "CroppedImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CropBitmapTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private sourceBitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "sourceBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    .line 114
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->TEXTURE:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 115
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->sourceBitmap:Landroid/graphics/Bitmap;

    .line 116
    return-void
.end method


# virtual methods
.method protected doInBackground()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/16 v7, 0x5dc

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->waitForReady(I)V

    .line 127
    const/4 v1, 0x0

    .line 129
    .local v1, "cropped":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->access$000(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->access$000(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 130
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 131
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->sourceBitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->access$100(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->access$000(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;)Landroid/graphics/Rect;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->waitForReady(I)V

    .line 138
    return-object v1

    .line 132
    :catch_0
    move-exception v2

    .line 133
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "CroppedImageView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to crop bitmap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->doInBackground()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 143
    if-eqz p1, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->this$0:Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;->access$200(Lcom/microsoft/xbox/toolkit/ui/CroppedImageView;Landroid/graphics/Bitmap;)V

    .line 146
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 104
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CroppedImageView$CropBitmapTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method
