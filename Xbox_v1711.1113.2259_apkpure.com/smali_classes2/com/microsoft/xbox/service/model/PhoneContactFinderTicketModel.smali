.class public Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "PhoneContactFinderTicketModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final LIVE_DOT_COM_SCOPE:Ljava/lang/String; = "ssl.live.com"

.field private static final POLICY:Ljava/lang/String; = "mbi_ssl"

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;


# instance fields
.field private msaTicket:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$MsaTicketRunnable;-><init>(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 26
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->lifetime:J

    .line 27
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->onGetTicketResult(Ljava/lang/String;Z)V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
    .locals 2

    .prologue
    .line 30
    const-class v1, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->instance:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->instance:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    .line 33
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->instance:Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized onGetTicketResult(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "ticket"    # Ljava/lang/String;
    .param p2, "isFailed"    # Z

    .prologue
    .line 56
    monitor-enter p0

    if-nez p2, :cond_0

    .line 57
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->msaTicket:Ljava/lang/String;

    .line 59
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getMsaTicket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->msaTicket:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loadSync()Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 44
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->msaTicket:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 41
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public loadAsync()V
    .locals 7

    .prologue
    .line 52
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->lastRefreshTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 53
    return-void
.end method

.method public loadSync()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method
