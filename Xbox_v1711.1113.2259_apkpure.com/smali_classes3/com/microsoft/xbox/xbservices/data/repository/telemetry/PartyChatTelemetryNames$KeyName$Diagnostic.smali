.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/PartyChatTelemetryNames$KeyName$Diagnostic;
.super Ljava/lang/Object;
.source "PartyChatTelemetryNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/telemetry/PartyChatTelemetryNames$KeyName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Diagnostic"
.end annotation


# static fields
.field public static final AudioDevice:Ljava/lang/String; = "AudioDevice"

.field public static final AudioDeviceName:Ljava/lang/String; = "AudioDeviceName"

.field public static final AudioInputDevice:Ljava/lang/String; = "AudioInputDevice"

.field public static final AudioOutputDevice:Ljava/lang/String; = "AudioOutputDevice"

.field public static final BluetoothName:Ljava/lang/String; = "BluetoothName"

.field public static final HasMic:Ljava/lang/String; = "HasMic"

.field public static final PingInfo:Ljava/lang/String; = "PingInfo"

.field public static final RtcDataMessage:Ljava/lang/String; = "RtcDataMessage"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
