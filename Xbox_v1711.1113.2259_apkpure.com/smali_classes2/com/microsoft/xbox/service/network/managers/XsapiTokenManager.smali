.class public Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;
.super Ljava/lang/Object;
.source "XsapiTokenManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/IXTokenManager;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;


# instance fields
.field private final lastTokenMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/idp/interop/TokenAndSignature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    .line 31
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    return-object v0
.end method

.method private getLastXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    .locals 3
    .param p1, "audience"    # Ljava/lang/String;

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetching existing XToken for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    monitor-exit v1

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    .locals 1
    .param p1, "audience"    # Ljava/lang/String;

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXToken(Ljava/lang/String;Z)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    return-object v0
.end method

.method private getXToken(Ljava/lang/String;Z)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    .locals 5
    .param p1, "audience"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Z

    .prologue
    .line 113
    if-eqz p2, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v2

    const-string v3, "GET"

    const-string v4, ""

    invoke-virtual {v2, v3, p1, v4}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getNewTokenAndSignatureSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;

    move-result-object v0

    .line 114
    .local v0, "result":Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
    :goto_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getToken()Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v1

    .line 115
    .local v1, "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-nez v1, :cond_0

    .line 118
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XToken fetch failed with status code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getHttpStatusCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", error code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 119
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and error message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 120
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    if-eqz v1, :cond_1

    .line 124
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    monitor-enter v3

    .line 125
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_1
    return-object v1

    .line 113
    .end local v0    # "result":Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
    .end local v1    # "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v2

    const-string v3, "GET"

    const-string v4, ""

    invoke-virtual {v2, v3, p1, v4}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getTokenAndSignatureSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;

    move-result-object v0

    goto :goto_0

    .line 126
    .restart local v0    # "result":Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;
    .restart local v1    # "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private onReset()V
    .locals 2

    .prologue
    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->lastTokenMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 135
    monitor-exit v1

    .line 136
    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public clearAllTokens()V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->TAG:Ljava/lang/String;

    const-string v1, "Clearing all tokens"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->clearTokenCache()V

    .line 90
    return-void
.end method

.method public getAgeGroup(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "audience"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    .line 83
    .local v0, "token":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getAgeGroup()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLastXTokenString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 54
    const-string v1, ""

    .line 56
    .local v1, "tokenStr":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getLastXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    .line 58
    .local v0, "token":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-nez v0, :cond_0

    .line 59
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x3ee

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v2

    .line 61
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getToken()Ljava/lang/String;

    move-result-object v1

    .line 64
    .end local v0    # "token":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    :cond_1
    return-object v1
.end method

.method public getPrivilegesFromToken(Ljava/lang/String;)[I
    .locals 2
    .param p1, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    .line 76
    .local v0, "token":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getPriviliges()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getPriviliges()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->convertPrivileges(Ljava/lang/String;)[I

    move-result-object v1

    goto :goto_0
.end method

.method public getXTokenString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXTokenString(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXTokenString(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .param p1, "audience"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 37
    const-string v1, ""

    .line 38
    .local v1, "tokenStr":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXToken(Ljava/lang/String;Z)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    .line 40
    .local v0, "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getToken()Ljava/lang/String;

    move-result-object v1

    .line 44
    .end local v0    # "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    :cond_0
    return-object v1
.end method

.method public getXuidFromToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getXToken(Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v0

    .line 70
    .local v0, "token":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getXuid()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->onReset()V

    .line 98
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    .line 99
    return-void
.end method
