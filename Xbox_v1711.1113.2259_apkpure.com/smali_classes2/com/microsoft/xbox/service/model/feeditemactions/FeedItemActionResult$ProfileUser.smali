.class public Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
.super Ljava/lang/Object;
.source "FeedItemActionResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfileUser"
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public settings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getSetting(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;
    .locals 5
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "ret":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->settings:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;

    .line 121
    .local v1, "s":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 122
    iget-object v0, v1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 127
    .end local v1    # "s":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    :cond_1
    return-object v0
.end method


# virtual methods
.method public getGamerPicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getSetting(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getSetting(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getSetting(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
