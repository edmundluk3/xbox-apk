.class Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "XLEVirtualizedListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p2, "executorService"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->doInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0xa

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->load(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 165
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 178
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->getFirstVisiblePosition()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->visibleIndexToRealIndex(I)I

    move-result v0

    .line 180
    .local v0, "desiredFirstIndex":I
    const/16 v3, 0xa

    .line 181
    .local v3, "min":I
    const/16 v4, 0x14

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 183
    .local v2, "max":I
    const-string v4, "BUGBUGBUG"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "loadPrev array size %d"

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v4, "BUGBUGBUG"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "loadPrev clear %d %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 188
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$310(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    .line 189
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    move-result v5

    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$302(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;I)I

    .line 191
    const-string v4, "BUGBUGBUG"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "loadPrev, now %d"

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v9, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 196
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onLoadPrevDone()V

    .line 198
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->onDataUpdated()V

    .line 199
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->realIndexToVisibleIndex(I)I

    move-result v1

    .line 200
    .local v1, "desiredFirstVisible":I
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->setSelection(I)V

    .line 201
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method
