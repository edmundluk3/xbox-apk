.class Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;
.super Ljava/lang/Object;
.source "NowPlayingTitleView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->attachToContainer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    .line 105
    .local v0, "companion":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    if-eqz v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->launchCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 108
    :cond_0
    return-void
.end method
