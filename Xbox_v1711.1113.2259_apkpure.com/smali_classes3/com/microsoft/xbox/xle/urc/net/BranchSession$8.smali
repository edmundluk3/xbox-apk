.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onAppChannelLineupsReceived(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$object:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 461
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->val$object:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 468
    .local v0, "error":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->val$object:Ljava/lang/Object;

    instance-of v3, v3, Ljava/lang/Throwable;

    if-eqz v3, :cond_0

    .line 469
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->val$object:Ljava/lang/Object;

    .end local v0    # "error":Ljava/lang/Throwable;
    check-cast v0, Ljava/lang/Throwable;

    .line 471
    .restart local v0    # "error":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x0

    .line 472
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->val$object:Ljava/lang/Object;

    instance-of v3, v3, Ljava/util/List;

    if-eqz v3, :cond_1

    .line 473
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->val$object:Ljava/lang/Object;

    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    check-cast v2, Ljava/util/List;

    .line 475
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$8;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$500(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;

    .line 476
    .local v1, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;
    invoke-interface {v1, v2, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;->onAppChannelLineupsReceived(Ljava/util/List;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 478
    .end local v1    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;
    :cond_2
    return-void
.end method
