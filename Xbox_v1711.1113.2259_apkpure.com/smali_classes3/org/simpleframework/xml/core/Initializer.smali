.class Lorg/simpleframework/xml/core/Initializer;
.super Ljava/lang/Object;
.source "Initializer.java"


# instance fields
.field private final factory:Ljava/lang/reflect/Constructor;

.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/simpleframework/xml/core/Parameter;",
            ">;"
        }
    .end annotation
.end field

.field private final signature:Lorg/simpleframework/xml/core/Signature;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Constructor;Lorg/simpleframework/xml/core/Signature;)V
    .locals 1
    .param p1, "factory"    # Ljava/lang/reflect/Constructor;
    .param p2, "signature"    # Lorg/simpleframework/xml/core/Signature;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p2}, Lorg/simpleframework/xml/core/Signature;->getParameters()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    .line 63
    iput-object p2, p0, Lorg/simpleframework/xml/core/Initializer;->signature:Lorg/simpleframework/xml/core/Signature;

    .line 64
    iput-object p1, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    .line 65
    return-void
.end method

.method private getAdjustment(Lorg/simpleframework/xml/core/Context;D)D
    .locals 6
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "score"    # D

    .prologue
    .line 242
    iget-object v2, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v0, v2, v4

    .line 244
    .local v0, "adjustment":D
    const-wide/16 v2, 0x0

    cmpl-double v2, p2, v2

    if-lez v2, :cond_0

    .line 245
    iget-object v2, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    div-double v2, p2, v2

    add-double/2addr v2, v0

    .line 247
    :goto_0
    return-wide v2

    :cond_0
    iget-object v2, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-double v2, v2

    div-double v2, p2, v2

    goto :goto_0
.end method

.method private getInstance([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "list"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 264
    :cond_0
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private getPercentage(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)D
    .locals 10
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 210
    const-wide/16 v4, 0x0

    .line 212
    .local v4, "score":D
    iget-object v8, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/simpleframework/xml/core/Parameter;

    .line 213
    .local v3, "value":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v3, p1}, Lorg/simpleframework/xml/core/Parameter;->getName(Lorg/simpleframework/xml/core/Context;)Ljava/lang/String;

    move-result-object v2

    .line 214
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p2, v2}, Lorg/simpleframework/xml/core/Criteria;->resolve(Ljava/lang/String;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v1

    .line 216
    .local v1, "label":Lorg/simpleframework/xml/core/Label;
    if-nez v1, :cond_2

    .line 217
    invoke-interface {v3}, Lorg/simpleframework/xml/core/Parameter;->isRequired()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 227
    .end local v1    # "label":Lorg/simpleframework/xml/core/Label;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Lorg/simpleframework/xml/core/Parameter;
    :goto_1
    return-wide v6

    .line 220
    .restart local v1    # "label":Lorg/simpleframework/xml/core/Label;
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v3    # "value":Lorg/simpleframework/xml/core/Parameter;
    :cond_1
    invoke-interface {v3}, Lorg/simpleframework/xml/core/Parameter;->isPrimitive()Z

    move-result v8

    if-eqz v8, :cond_0

    goto :goto_1

    .line 224
    :cond_2
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v8

    goto :goto_0

    .line 227
    .end local v1    # "label":Lorg/simpleframework/xml/core/Label;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "value":Lorg/simpleframework/xml/core/Parameter;
    :cond_3
    invoke-direct {p0, p1, v4, v5}, Lorg/simpleframework/xml/core/Initializer;->getAdjustment(Lorg/simpleframework/xml/core/Context;D)D

    move-result-wide v6

    goto :goto_1
.end method

.method private getVariable(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;I)Ljava/lang/Object;
    .locals 4
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v3, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simpleframework/xml/core/Parameter;

    .line 142
    .local v1, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v1, p1}, Lorg/simpleframework/xml/core/Parameter;->getName(Lorg/simpleframework/xml/core/Context;)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "name":Ljava/lang/String;
    invoke-interface {p2, v0}, Lorg/simpleframework/xml/core/Criteria;->remove(Ljava/lang/String;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v2

    .line 145
    .local v2, "variable":Lorg/simpleframework/xml/core/Variable;
    if-eqz v2, :cond_0

    .line 146
    invoke-virtual {v2}, Lorg/simpleframework/xml/core/Variable;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 148
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getInstance(Lorg/simpleframework/xml/core/Context;)Ljava/lang/Object;
    .locals 2
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 105
    :cond_0
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)Ljava/lang/Object;
    .locals 3
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v2, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 122
    .local v1, "values":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/simpleframework/xml/core/Initializer;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 123
    invoke-direct {p0, p1, p2, v0}, Lorg/simpleframework/xml/core/Initializer;->getVariable(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v0

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    invoke-direct {p0, v1}, Lorg/simpleframework/xml/core/Initializer;->getInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public getParameter(Ljava/lang/String;)Lorg/simpleframework/xml/core/Parameter;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/core/Signature;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/Parameter;

    return-object v0
.end method

.method public getScore(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)D
    .locals 12
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v9, p0, Lorg/simpleframework/xml/core/Initializer;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v9, p1}, Lorg/simpleframework/xml/core/Signature;->getSignature(Lorg/simpleframework/xml/core/Context;)Lorg/simpleframework/xml/core/Signature;

    move-result-object v4

    .line 169
    .local v4, "match":Lorg/simpleframework/xml/core/Signature;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Criteria;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 170
    .local v5, "name":Ljava/lang/String;
    invoke-interface {p2, v5}, Lorg/simpleframework/xml/core/Criteria;->resolve(Ljava/lang/String;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v3

    .line 172
    .local v3, "label":Lorg/simpleframework/xml/core/Label;
    if-eqz v3, :cond_0

    .line 173
    invoke-interface {v3, p1}, Lorg/simpleframework/xml/core/Label;->getUnion(Lorg/simpleframework/xml/core/Context;)Ljava/util/Set;

    move-result-object v7

    .line 174
    .local v7, "options":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v4, v5}, Lorg/simpleframework/xml/core/Signature;->getParameter(Ljava/lang/String;)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v8

    .line 175
    .local v8, "value":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v3}, Lorg/simpleframework/xml/core/Label;->getContact()Lorg/simpleframework/xml/core/Contact;

    move-result-object v0

    .line 177
    .local v0, "contact":Lorg/simpleframework/xml/core/Contact;
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 178
    .local v6, "option":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 179
    invoke-virtual {v4, v6}, Lorg/simpleframework/xml/core/Signature;->getParameter(Ljava/lang/String;)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v8

    goto :goto_0

    .line 182
    .end local v6    # "option":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Lorg/simpleframework/xml/core/Contact;->isReadOnly()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 183
    if-nez v8, :cond_0

    .line 184
    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    .line 189
    .end local v0    # "contact":Lorg/simpleframework/xml/core/Contact;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "label":Lorg/simpleframework/xml/core/Label;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "options":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "value":Lorg/simpleframework/xml/core/Parameter;
    :goto_1
    return-wide v10

    :cond_3
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/Initializer;->getPercentage(Lorg/simpleframework/xml/core/Context;Lorg/simpleframework/xml/core/Criteria;)D

    move-result-wide v10

    goto :goto_1
.end method

.method public isDefault()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Signature;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lorg/simpleframework/xml/core/Initializer;->factory:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
