.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GameProfileClubMembershipViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;


# instance fields
.field private gameName:Ljava/lang/String;

.field private gameTitleId:J

.field private getUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

.field private isLoading:Z

.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private userClubList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->TAG:Ljava/lang/String;

    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 40
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 58
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 59
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 60
    .local v1, "selectedItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameTitleId:J

    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    .line 63
    return-void

    .line 60
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->onGetUserClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private cancelTasks()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->cancel()V

    .line 107
    :cond_0
    return-void
.end method

.method static synthetic lambda$onGetUserClubsCompleted$0(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->associatedTitles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->associatedTitles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameTitleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onGetUserClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 123
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGetUserClubsCompleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 143
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->updateAdapter()V

    .line 144
    return-void

    .line 129
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 130
    .local v0, "userClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 131
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;)Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 138
    .end local v0    # "userClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getClubMembershipList()Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 89
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameTitleId:J

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v1

    .line 91
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    iget-object v2, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameName:Ljava/lang/String;

    .line 96
    .end local v1    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705ca

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->gameName:Ljava/lang/String;

    const-string v7, ""

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "headerText":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->userClubList:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Ljava/lang/String;Ljava/util/List;)V

    .line 99
    .end local v0    # "headerText":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 111
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 115
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->cancelTasks()V

    .line 117
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GetUserClubsAsyncTask;->load(Z)V

    .line 119
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 74
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;->cancelTasks()V

    .line 79
    return-void
.end method
