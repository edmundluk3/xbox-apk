.class public interface abstract Lcom/microsoft/xbox/data/service/beam/BeamService;
.super Ljava/lang/Object;
.source "BeamService.java"


# static fields
.field public static final THUMBS_SMALL_FORMAT:Ljava/lang/String; = "https://thumbs.beam.pro/channel/%s.small.jpg"


# virtual methods
.method public abstract getSpecificChannels(Ljava/lang/String;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation

        .annotation runtime Lretrofit2/http/Query;
            value = "where"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/api/v1/channels?order=online:desc,viewersCurrent:desc,viewersTotal:desc&fields=id,viewersCurrent,name,token,audience"
    .end annotation
.end method

.method public abstract getTrendingChannels(I)Lio/reactivex/Single;
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation

        .annotation runtime Lretrofit2/http/Query;
            value = "page"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/api/v1/channels?order=online:desc,viewersCurrent:desc,viewersTotal:desc&fields=id,viewersCurrent,name,token&where=suspended.eq.0"
    .end annotation
.end method
