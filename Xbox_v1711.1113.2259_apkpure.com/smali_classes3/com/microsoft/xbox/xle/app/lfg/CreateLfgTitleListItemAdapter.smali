.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CreateLfgTitleListItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private titleListener:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    const v0, 0x7f030233

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 25
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->titleListener:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->titleListener:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$OnTitleSelectedListener;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v7, 0x7f020122

    .line 34
    instance-of v5, p2, Landroid/widget/RelativeLayout;

    if-eqz v5, :cond_0

    move-object v1, p2

    .line 35
    check-cast v1, Landroid/widget/RelativeLayout;

    .line 41
    .local v1, "lfgTitleView":Landroid/widget/RelativeLayout;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 42
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    const v5, 0x7f0e0ad7

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 43
    .local v2, "titleImageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const/4 v5, 0x2

    invoke-static {v2, v5}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 44
    const v5, 0x7f0e0ad8

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 46
    .local v3, "titleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getBoxArt()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 49
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getBoxArt()Ljava/lang/String;

    move-result-object v5

    .line 48
    invoke-virtual {v2, v5, v7, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 56
    :goto_1
    new-instance v5, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$1;

    invoke-direct {v5, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-object v1

    .line 37
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    .end local v1    # "lfgTitleView":Landroid/widget/RelativeLayout;
    .end local v2    # "titleImageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .end local v3    # "titleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgTitleListItemAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 38
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f030233

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .restart local v1    # "lfgTitleView":Landroid/widget/RelativeLayout;
    goto :goto_0

    .line 53
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    .restart local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    .restart local v2    # "titleImageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .restart local v3    # "titleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_1
    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    goto :goto_1
.end method
