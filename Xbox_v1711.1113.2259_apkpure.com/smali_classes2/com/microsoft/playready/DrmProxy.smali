.class Lcom/microsoft/playready/DrmProxy;
.super Ljava/lang/Object;
.source "DrmProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/DrmProxy$ChallengeData;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mNativeClass:Lcom/microsoft/playready/Native_Class8;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/playready/DrmProxy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/DrmProxy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 2
    .param p1, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    sget-boolean v0, Lcom/microsoft/playready/DrmProxy;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_0
    sget-object v1, Lcom/microsoft/playready/NativeLoadLibrary;->DrmInitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 75
    :try_start_0
    new-instance v0, Lcom/microsoft/playready/Native_Class8;

    invoke-direct {v0}, Lcom/microsoft/playready/Native_Class8;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    .line 73
    monitor-exit v1

    .line 78
    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static challengeDataFromObfuscatedObj(Lcom/microsoft/playready/Native_Class8$internal_class_1;)Lcom/microsoft/playready/DrmProxy$ChallengeData;
    .locals 3
    .param p0, "obfuscatedObj"    # Lcom/microsoft/playready/Native_Class8$internal_class_1;

    .prologue
    .line 82
    new-instance v0, Lcom/microsoft/playready/DrmProxy$ChallengeData;

    iget-object v1, p0, Lcom/microsoft/playready/Native_Class8$internal_class_1;->mField1:[B

    iget-object v2, p0, Lcom/microsoft/playready/Native_Class8$internal_class_1;->mField2:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/DrmProxy$ChallengeData;-><init>([BLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public deleteLicense(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyID"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class8;->method_7(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public generateDomainJoinChallenge(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)[B
    .locals 2
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p2, "challengeCustomData"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {p0, p1}, Lcom/microsoft/playready/DrmProxy;->obfuscatedObjFromDomainInfo(Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/Native_Class8$internal_class_2;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/playready/Native_Class8;->method_14(Lcom/microsoft/playready/Native_Class8$internal_class_2;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public generateDomainLeaveChallenge(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)[B
    .locals 2
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p2, "challengeCustomData"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {p0, p1}, Lcom/microsoft/playready/DrmProxy;->obfuscatedObjFromDomainInfo(Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/Native_Class8$internal_class_2;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/playready/Native_Class8;->method_15(Lcom/microsoft/playready/Native_Class8$internal_class_2;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public generateLicenseChallenge(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/DrmProxy$ChallengeData;
    .locals 2
    .param p1, "keyId"    # Ljava/lang/String;
    .param p2, "customData"    # Ljava/lang/String;
    .param p3, "currentDomainInfo"    # Lcom/microsoft/playready/DomainInfo;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    .line 113
    invoke-virtual {p0, p3}, Lcom/microsoft/playready/DrmProxy;->obfuscatedObjFromDomainInfo(Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/Native_Class8$internal_class_2;

    move-result-object v1

    .line 110
    invoke-virtual {v0, p1, p2, v1}, Lcom/microsoft/playready/Native_Class8;->method_5(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/Native_Class8$internal_class_2;)Lcom/microsoft/playready/Native_Class8$internal_class_1;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/microsoft/playready/DrmProxy;->challengeDataFromObfuscatedObj(Lcom/microsoft/playready/Native_Class8$internal_class_1;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v0

    return-object v0
.end method

.method public generateMeteringChallenge([BLjava/lang/String;)Lcom/microsoft/playready/DrmProxy$ChallengeData;
    .locals 1
    .param p1, "meteringCert"    # [B
    .param p2, "customData"    # Ljava/lang/String;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/playready/Native_Class8;->method_8([BLjava/lang/String;)Lcom/microsoft/playready/Native_Class8$internal_class_1;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/playready/DrmProxy;->challengeDataFromObfuscatedObj(Lcom/microsoft/playready/Native_Class8$internal_class_1;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v0

    return-object v0
.end method

.method protected obfuscatedObjFromDomainInfo(Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/Native_Class8$internal_class_2;
    .locals 2
    .param p1, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "obfObj":Lcom/microsoft/playready/Native_Class8$internal_class_2;
    if-eqz p1, :cond_0

    .line 90
    new-instance v0, Lcom/microsoft/playready/Native_Class8$internal_class_2;

    .end local v0    # "obfObj":Lcom/microsoft/playready/Native_Class8$internal_class_2;
    invoke-direct {v0}, Lcom/microsoft/playready/Native_Class8$internal_class_2;-><init>()V

    .line 91
    .restart local v0    # "obfObj":Lcom/microsoft/playready/Native_Class8$internal_class_2;
    invoke-virtual {p1}, Lcom/microsoft/playready/DomainInfo;->getServiceID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/playready/Native_Class8$internal_class_2;->mField1:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lcom/microsoft/playready/DomainInfo;->getAccountID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/playready/Native_Class8$internal_class_2;->mField2:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Lcom/microsoft/playready/DomainInfo;->getFriendlyName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/playready/Native_Class8$internal_class_2;->mField3:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Lcom/microsoft/playready/DomainInfo;->getRevision()I

    move-result v1

    iput v1, v0, Lcom/microsoft/playready/Native_Class8$internal_class_2;->mField4:I

    .line 97
    :cond_0
    return-object v0
.end method

.method public processDomainJoinResponse([B)Ljava/lang/String;
    .locals 1
    .param p1, "domainResponse"    # [B

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class8;->method_16([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public processDomainLeaveResponse([B)Ljava/lang/String;
    .locals 1
    .param p1, "domainResponse"    # [B

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class8;->method_17([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public processLicenseReponse([B)Ljava/lang/String;
    .locals 1
    .param p1, "response"    # [B

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class8;->method_6([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public processMeteringResponse([B)Ljava/lang/String;
    .locals 1
    .param p1, "response"    # [B

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy;->mNativeClass:Lcom/microsoft/playready/Native_Class8;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class8;->method_9([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
