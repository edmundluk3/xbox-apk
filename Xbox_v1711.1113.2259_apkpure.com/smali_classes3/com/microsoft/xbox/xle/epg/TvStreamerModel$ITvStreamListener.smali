.class public interface abstract Lcom/microsoft/xbox/xle/epg/TvStreamerModel$ITvStreamListener;
.super Ljava/lang/Object;
.source "TvStreamerModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ITvStreamListener"
.end annotation


# virtual methods
.method public abstract onChannelChanged()V
.end method

.method public abstract onChannelTypeChanged(Ljava/lang/String;)V
.end method

.method public abstract onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
.end method

.method public abstract onLoadingChanged(Z)V
.end method

.method public abstract onStreamError(Ljava/lang/String;Z)V
.end method

.method public abstract onStreamFormatChanged()V
.end method

.method public abstract onTvStreamClosed()V
.end method

.method public abstract onTvStreamClosing()V
.end method
