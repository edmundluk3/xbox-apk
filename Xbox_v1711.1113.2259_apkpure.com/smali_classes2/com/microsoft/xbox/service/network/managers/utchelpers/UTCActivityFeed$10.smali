.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;
.super Ljava/lang/Object;
.source "UTCActivityFeed.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackSocial(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;->val$actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;->val$xuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 209
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;->val$actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 211
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 212
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Filter"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;->val$actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 213
    const-string v1, "TargetXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed$10;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 215
    const-string v1, "Activity Social Bar"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 216
    return-void
.end method
