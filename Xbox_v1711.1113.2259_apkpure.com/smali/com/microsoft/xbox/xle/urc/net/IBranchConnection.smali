.class public interface abstract Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;
.super Ljava/lang/Object;
.source "IBranchConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;,
        Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract getLastError()Ljava/lang/String;
.end method

.method public abstract getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
.end method

.method public abstract open()Z
.end method

.method public abstract sendMessage(Ljava/lang/String;)Z
.end method

.method public abstract setListener(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;)V
.end method
