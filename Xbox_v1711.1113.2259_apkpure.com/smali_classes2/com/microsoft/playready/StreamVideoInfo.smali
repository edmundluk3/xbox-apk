.class public final Lcom/microsoft/playready/StreamVideoInfo;
.super Ljava/lang/Object;
.source "StreamVideoInfo.java"

# interfaces
.implements Lcom/microsoft/playready/VideoInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;I)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 51
    iput p2, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    .line 52
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_45(I)I

    move-result v0

    return v0
.end method

.method public getRotation()I
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_46(I)I

    move-result v0

    return v0
.end method

.method public getSARHeight()I
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_48(I)I

    move-result v0

    return v0
.end method

.method public getSARWidth()I
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_47(I)I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/StreamVideoInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/StreamVideoInfo;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_44(I)I

    move-result v0

    return v0
.end method
