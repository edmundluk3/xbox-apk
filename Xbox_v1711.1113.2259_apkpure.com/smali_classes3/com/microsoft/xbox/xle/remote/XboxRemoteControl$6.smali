.class Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;
.super Ljava/lang/Object;
.source "XboxRemoteControl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->captureTouchFrames()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 25
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 328
    const/4 v15, 0x0

    .line 330
    .local v15, "processed":Z
    new-instance v16, Lcom/microsoft/xbox/smartglass/TouchFrame;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/TouchFrame;-><init>(I)V

    .line 332
    .local v16, "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v19

    packed-switch v19, :pswitch_data_0

    .line 419
    :cond_0
    :goto_0
    :pswitch_0
    return v15

    .line 336
    :pswitch_1
    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v10

    .line 337
    .local v10, "id":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->clear()V

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 339
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v21

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v22

    sget-object v23, Lcom/microsoft/xbox/smartglass/TouchAction;->Down:Lcom/microsoft/xbox/smartglass/TouchAction;

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v10, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 341
    const/4 v15, 0x1

    .line 342
    goto :goto_0

    .line 348
    .end local v10    # "id":I
    :pswitch_2
    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v10

    .line 349
    .restart local v10    # "id":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 350
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v21, v0

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v22

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v19

    const/16 v24, 0x1

    move/from16 v0, v19

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    sget-object v19, Lcom/microsoft/xbox/smartglass/TouchAction;->Up:Lcom/microsoft/xbox/smartglass/TouchAction;

    :goto_1
    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v19

    invoke-static {v0, v1, v2, v10, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v19

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 352
    const/4 v15, 0x1

    .line 354
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->clear()V

    goto/16 :goto_0

    .line 350
    :cond_2
    sget-object v19, Lcom/microsoft/xbox/smartglass/TouchAction;->Cancel:Lcom/microsoft/xbox/smartglass/TouchAction;

    goto :goto_1

    .line 360
    .end local v10    # "id":I
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v19

    const v20, 0xff00

    and-int v19, v19, v20

    shr-int/lit8 v7, v19, 0x8

    .line 361
    .local v7, "downIdx":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    .line 364
    .local v6, "downId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 366
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v21

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v22

    sget-object v23, Lcom/microsoft/xbox/smartglass/TouchAction;->Down:Lcom/microsoft/xbox/smartglass/TouchAction;

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v6, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 368
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 373
    .end local v6    # "downId":I
    .end local v7    # "downIdx":I
    :pswitch_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v19

    const v20, 0xff00

    and-int v19, v19, v20

    shr-int/lit8 v18, v19, 0x8

    .line 374
    .local v18, "upIdx":I
    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v17

    .line 377
    .local v17, "upId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v14

    .line 379
    .local v14, "pos":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v14, v0, :cond_3

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 382
    :cond_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v21

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v22

    sget-object v23, Lcom/microsoft/xbox/smartglass/TouchAction;->Up:Lcom/microsoft/xbox/smartglass/TouchAction;

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v18

    move-object/from16 v4, v23

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 385
    const/4 v15, 0x1

    .line 386
    goto/16 :goto_0

    .line 390
    .end local v14    # "pos":I
    .end local v17    # "upId":I
    .end local v18    # "upIdx":I
    :pswitch_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v9

    .line 391
    .local v9, "historySize":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    .line 392
    .local v12, "pointerCount":I
    const/4 v8, 0x0

    .local v8, "h":I
    :goto_2
    if-ge v8, v9, :cond_6

    .line 393
    new-instance v16, Lcom/microsoft/xbox/smartglass/TouchFrame;

    .end local v16    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/TouchFrame;-><init>(I)V

    .line 394
    .restart local v16    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    const/4 v11, 0x0

    .local v11, "p":I
    :goto_3
    if-ge v11, v12, :cond_5

    .line 395
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v13

    .line 396
    .local v13, "pointerId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 397
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v8}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v21

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v8}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v22

    sget-object v23, Lcom/microsoft/xbox/smartglass/TouchAction;->Move:Lcom/microsoft/xbox/smartglass/TouchAction;

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v13, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 400
    .end local v13    # "pointerId":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 392
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 404
    .end local v11    # "p":I
    :cond_6
    new-instance v16, Lcom/microsoft/xbox/smartglass/TouchFrame;

    .end local v16    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/TouchFrame;-><init>(I)V

    .line 405
    .restart local v16    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    const/4 v11, 0x0

    .restart local v11    # "p":I
    :goto_4
    if-ge v11, v12, :cond_8

    .line 406
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v13

    .line 407
    .restart local v13    # "pointerId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$300(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Ljava/util/Vector;

    move-result-object v19

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 408
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v21

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v22

    sget-object v23, Lcom/microsoft/xbox/smartglass/TouchAction;->Move:Lcom/microsoft/xbox/smartglass/TouchAction;

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v13, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$400(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;FFILcom/microsoft/xbox/smartglass/TouchAction;)Lcom/microsoft/xbox/smartglass/TouchPoint;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 411
    .end local v13    # "pointerId":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$500(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;Lcom/microsoft/xbox/smartglass/TouchFrame;)V

    .line 412
    const/4 v15, 0x1

    .line 413
    goto/16 :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
