.class public Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
.super Ljava/lang/Object;
.source "JavaScriptBridge.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SetJavaScriptEnabled"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;,
        Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ChromeClient;,
        Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;,
        Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$Bridge;,
        Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;
    }
.end annotation


# static fields
.field private static CompleteRequestScript:Ljava/lang/String;

.field private static EventScript:Ljava/lang/String;

.field private static InitializeScript:Ljava/lang/String;

.field private static _invokeScriptLock:Ljava/lang/Object;


# instance fields
.field private final _adapter:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

.field private final _executor:Ljava/util/concurrent/Executor;

.field private final _listener:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;

.field private _scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

.field private final _webView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "SGInitializeBridge"

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->InitializeScript:Ljava/lang/String;

    .line 29
    const-string v0, "SGCompleteRequest"

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->CompleteRequestScript:Ljava/lang/String;

    .line 30
    const-string v0, "SGEvent"

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->EventScript:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_invokeScriptLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/webkit/WebView;)V
    .locals 6
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    const/4 v5, 0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;-><init>(Landroid/webkit/WebView;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .line 43
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$1;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_listener:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;

    .line 45
    new-instance v2, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    invoke-direct {v2}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_adapter:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    .line 46
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_adapter:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_listener:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$AdapterListener;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->addListener(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_executor:Ljava/util/concurrent/Executor;

    .line 50
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ViewClient;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 51
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ChromeClient;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$ChromeClient;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 52
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 53
    .local v1, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 54
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 55
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "database"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "databasePath":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    new-instance v3, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$Bridge;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$Bridge;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)V

    const-string v4, "smartglass"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_adapter:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->EventScript:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->invokeScript(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->onScriptNotify(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->InitializeScript:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;)Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    return-object v0
.end method

.method private varargs invokeScript(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1, "scriptName"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 86
    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_invokeScriptLock:Ljava/lang/Object;

    monitor-enter v4

    .line 87
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    if-eqz v3, :cond_2

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "javascript:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 94
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    aget-object v3, p2, v0

    const-string v5, "\\"

    const-string v6, "\\\\"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "\""

    const-string v6, "\\\""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    const-string v3, "\","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_0
    array-length v3, p2

    if-lez v3, :cond_1

    .line 101
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    array-length v3, p2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, p2, v3

    const-string v5, "\\"

    const-string v6, "\\\\"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "\""

    const-string v6, "\\\""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    const-string v3, ");"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;-><init>(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 111
    .local v2, "script":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->run(Lcom/microsoft/xbox/smartglass/controls/RunnableScript;)V

    .line 113
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "script":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    :cond_2
    monitor-exit v4

    .line 114
    return-void

    .line 113
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private onScriptNotify(Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 63
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Ljava/lang/String;)V

    .line 64
    .local v0, "command":Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->_executor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$1;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public completeRequest(ILjava/lang/String;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "resultJson"    # Ljava/lang/String;

    .prologue
    .line 76
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 77
    .local v1, "result":Lorg/json/JSONObject;
    const-string v2, "id"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 78
    const-string v2, "data"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    sget-object v2, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->CompleteRequestScript:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;->invokeScript(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .end local v1    # "result":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "ex":Lorg/json/JSONException;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse JavaScript command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    goto :goto_0
.end method
