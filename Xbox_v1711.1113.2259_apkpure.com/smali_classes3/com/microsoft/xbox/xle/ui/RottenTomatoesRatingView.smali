.class public Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;
.super Landroid/widget/LinearLayout;
.source "RottenTomatoesRatingView.java"


# static fields
.field private static final ROTTEN_TOMATOES_RATING_BAR:F = 60.0f


# instance fields
.field private ratingIconView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

.field private ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private ratingValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingValue:I

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 27
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f0301ec

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 28
    const v1, 0x7f0e09c6

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 29
    const v1, 0x7f0e09c5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingIconView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    .line 30
    return-void
.end method


# virtual methods
.method public setRating(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 33
    const/4 v0, 0x0

    const/16 v1, 0x64

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingValue:I

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingValue:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingIconView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    if-eqz v0, :cond_1

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingIconView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/RottenTomatoesRatingView;->ratingValue:I

    int-to-float v0, v0

    const/high16 v2, 0x42700000    # 60.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_2

    const v0, 0x7f0201d4

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;->setImageResource(I)V

    .line 42
    :cond_1
    return-void

    .line 40
    :cond_2
    const v0, 0x7f0201d5

    goto :goto_0
.end method
