.class public abstract Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
.super Ljava/lang/Object;
.source "ClubRosterDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubRosterBatchUpdateRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static delete(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "userIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 62
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;

    const-string v1, "DELETE"

    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method

.method public static put(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "userIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;

    const-string v1, "PUT"

    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public abstract method()Ljava/lang/String;
.end method

.method public abstract userIds()Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
