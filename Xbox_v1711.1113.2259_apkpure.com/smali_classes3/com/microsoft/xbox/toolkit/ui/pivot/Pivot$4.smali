.class synthetic Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;
.super Ljava/lang/Object;
.source "Pivot.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PaneAnimationType:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 507
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->values()[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PaneAnimationType:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PaneAnimationType:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PaneAnimationType:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->EASE_OUT_EXPO_7:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 325
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->values()[Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I

    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_LEFT:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_RIGHT:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_NONE:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    .line 507
    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method
