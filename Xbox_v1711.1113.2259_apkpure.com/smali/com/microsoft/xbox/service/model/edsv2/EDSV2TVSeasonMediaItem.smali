.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2TVSeasonMediaItem.java"


# instance fields
.field public EpisodeCount:I

.field public IsSeasonComplete:Z

.field public SeasonNumber:I

.field public ZuneId:Ljava/lang/String;

.field private episodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private formattedName:Ljava/lang/String;

.field private parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ParentSeries"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 29
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 30
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->EpisodeCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->EpisodeCount:I

    move-object v0, p1

    .line 31
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->IsSeasonComplete:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->IsSeasonComplete:Z

    move-object v0, p1

    .line 32
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->SeasonNumber:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->SeasonNumber:I

    .line 33
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ZuneId:Ljava/lang/String;

    .line 35
    :cond_0
    const/16 v0, 0x3ed

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->setMediaItemTypeFromInt(I)V

    .line 37
    return-void
.end method


# virtual methods
.method public getDisplayTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getSeriesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEpisodes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->episodes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    return-object v0
.end method

.method public getSeasonName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->formattedName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 41
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0703d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->SeasonNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->formattedName:Ljava/lang/String;

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->formattedName:Ljava/lang/String;

    return-object v0
.end method

.method public getSeasonNumber()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->SeasonNumber:I

    return v0
.end method

.method public getSeriesCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ID:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->parentSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method

.method public setEpisodes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "episodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->episodes:Ljava/util/ArrayList;

    .line 52
    return-void
.end method
