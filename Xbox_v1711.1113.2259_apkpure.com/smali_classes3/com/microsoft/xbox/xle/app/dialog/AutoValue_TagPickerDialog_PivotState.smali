.class final Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;
.super Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
.source "AutoValue_TagPickerDialog_PivotState.java"


# instance fields
.field private final achievementTagsEnabled:Z

.field private final systemTagsEnabled:Z

.field private final titleId:J

.field private final trendingTagsEnabled:Z


# direct methods
.method constructor <init>(ZZZJ)V
    .locals 0
    .param p1, "systemTagsEnabled"    # Z
    .param p2, "trendingTagsEnabled"    # Z
    .param p3, "achievementTagsEnabled"    # Z
    .param p4, "titleId"    # J

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;-><init>()V

    .line 19
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->systemTagsEnabled:Z

    .line 20
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->trendingTagsEnabled:Z

    .line 21
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->achievementTagsEnabled:Z

    .line 22
    iput-wide p4, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    .line 23
    return-void
.end method


# virtual methods
.method public achievementTagsEnabled()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->achievementTagsEnabled:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 61
    check-cast v0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    .line 62
    .local v0, "that":Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->systemTagsEnabled:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->systemTagsEnabled()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->trendingTagsEnabled:Z

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->trendingTagsEnabled()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->achievementTagsEnabled:Z

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->achievementTagsEnabled()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->titleId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
    :cond_3
    move v1, v2

    .line 67
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const v4, 0xf4243

    .line 72
    const/4 v0, 0x1

    .line 73
    .local v0, "h":I
    mul-int/2addr v0, v4

    .line 74
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->systemTagsEnabled:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 75
    mul-int/2addr v0, v4

    .line 76
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->trendingTagsEnabled:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 77
    mul-int/2addr v0, v4

    .line 78
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->achievementTagsEnabled:Z

    if-eqz v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 79
    mul-int/2addr v0, v4

    .line 80
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 81
    return v0

    :cond_0
    move v1, v3

    .line 74
    goto :goto_0

    :cond_1
    move v1, v3

    .line 76
    goto :goto_1

    :cond_2
    move v2, v3

    .line 78
    goto :goto_2
.end method

.method public systemTagsEnabled()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->systemTagsEnabled:Z

    return v0
.end method

.method public titleId()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PivotState{systemTagsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->systemTagsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", trendingTagsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->trendingTagsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementTagsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->achievementTagsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->titleId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trendingTagsEnabled()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/dialog/AutoValue_TagPickerDialog_PivotState;->trendingTagsEnabled:Z

    return v0
.end method
