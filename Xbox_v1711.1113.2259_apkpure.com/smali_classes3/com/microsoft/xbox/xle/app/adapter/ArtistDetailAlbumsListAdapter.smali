.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ArtistDetailAlbumsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;

    .line 28
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 33
    move-object v3, p2

    .line 35
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_3

    .line 36
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 37
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f03003c

    invoke-virtual {v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 39
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;

    invoke-direct {v1, v7}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$1;)V

    .line 40
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;
    const v5, 0x7f0e01da

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->tileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 41
    const v5, 0x7f0e01db

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->albumNameView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    const v5, 0x7f0e01dc

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->artistReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .line 50
    .local v2, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    if-eqz v2, :cond_2

    .line 51
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->tileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v5, :cond_0

    .line 52
    const/16 v5, 0x3ee

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 53
    .local v0, "defaultImage":I
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->tileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 56
    .end local v0    # "defaultImage":I
    :cond_0
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->albumNameView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v5, :cond_1

    .line 57
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->albumNameView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :cond_1
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->artistReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v5, :cond_2

    .line 61
    iget-object v6, v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;->artistReleaseData:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_2
    return-object v3

    .line 46
    .end local v1    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsListAdapter$ViewHolder;
    goto :goto_0

    .line 61
    .restart local v2    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    :cond_4
    const-string v5, ""

    goto :goto_1
.end method
