.class Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ExperimentModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ExperimentModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetExperiementDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ExperimentModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ExperimentModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ExperimentModel;Lcom/microsoft/xbox/service/model/ExperimentModel;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ExperimentModel;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;->this$0:Lcom/microsoft/xbox/service/model/ExperimentModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 162
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;->caller:Lcom/microsoft/xbox/service/model/ExperimentModel;

    .line 163
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v1, 0x0

    .line 174
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;->caller:Lcom/microsoft/xbox/service/model/ExperimentModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ExperimentModel;->getXuid()J

    move-result-wide v2

    .line 176
    .local v2, "lxuid":J
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getExperimentManager()Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;->GetSettings(J)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 183
    .end local v2    # "lxuid":J
    :goto_0
    return-object v1

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ExperimentModel;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Could not get experiment settings"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .end local v1    # "result":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;-><init>()V

    .restart local v1    # "result":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 194
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ExperimentModel$GetExperiementDataRunner;->caller:Lcom/microsoft/xbox/service/model/ExperimentModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ExperimentModel;->update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 190
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method
