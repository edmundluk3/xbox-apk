.class Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;
.super Ljava/lang/Object;
.source "Accelerometer.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/Accelerometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/Accelerometer;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/Accelerometer;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Accelerometer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/Accelerometer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/Accelerometer;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/Accelerometer$1;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;-><init>(Lcom/microsoft/xbox/smartglass/Accelerometer;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 63
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 69
    const v0, 0x411ce80a

    .line 70
    .local v0, "g":F
    new-instance v1, Lcom/microsoft/xbox/smartglass/AccelerometerReading;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    div-float/2addr v2, v0

    neg-float v2, v2

    float-to-double v2, v2

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    div-float/2addr v4, v0

    neg-float v4, v4

    float-to-double v4, v4

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    div-float/2addr v6, v0

    neg-float v6, v6

    float-to-double v6, v6

    iget-wide v8, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/smartglass/AccelerometerReading;-><init>(DDDJ)V

    .line 72
    .local v1, "reading":Lcom/microsoft/xbox/smartglass/AccelerometerReading;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Accelerometer;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/Accelerometer;->cloneListeners()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/smartglass/AccelerometerListener;

    .line 73
    .local v10, "listener":Lcom/microsoft/xbox/smartglass/AccelerometerListener;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/Accelerometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Accelerometer;

    invoke-virtual {v10, v3, v1}, Lcom/microsoft/xbox/smartglass/AccelerometerListener;->onReadingChanged(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/AccelerometerReading;)V

    goto :goto_0

    .line 75
    .end local v10    # "listener":Lcom/microsoft/xbox/smartglass/AccelerometerListener;
    :cond_0
    return-void
.end method
