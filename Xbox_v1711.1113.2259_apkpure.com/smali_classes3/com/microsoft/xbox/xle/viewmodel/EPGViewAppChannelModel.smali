.class public Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;
.source "EPGViewAppChannelModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;


# instance fields
.field private final mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)V
    .locals 0
    .param p1, "model"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 12
    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->hasListeners()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 78
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 79
    return-void
.end method

.method public getChannelGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColors()[I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getColors()[I

    move-result-object v0

    return-object v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    return-object v0
.end method

.method public getHDEquivalent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeadendId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;->AppChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Type;

    return-object v0
.end method

.method public isAdult()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isFavorite()Z

    move-result v0

    return v0
.end method

.method public isHD()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
    .locals 3
    .param p1, "obj"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 18
    const/4 v1, 0x0

    .line 21
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 20
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;

    .line 21
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isTheSameChannel(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPropertyChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v0, "favorite"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->notifyIsFavoriteChanged()V

    .line 108
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->hasListeners()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 87
    :cond_0
    return-void
.end method

.method public toggleFavorite()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;->mModel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->toggleFavorite()V

    .line 101
    return-void
.end method

.method public tuneToChannel()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
