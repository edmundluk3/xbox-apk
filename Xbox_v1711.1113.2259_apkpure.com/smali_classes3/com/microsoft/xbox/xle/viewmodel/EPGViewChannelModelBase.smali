.class public abstract Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;
.super Ljava/lang/Object;
.source "EPGViewChannelModelBase.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;


# instance fields
.field private mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->mListeners:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public getColors()[I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method protected hasListeners()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
    .locals 2
    .param p1, "obj"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 13
    if-eq p1, p0, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyIsFavoriteChanged()V
    .locals 3

    .prologue
    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .line 47
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;->isFavoriteChanged()V

    goto :goto_0

    .line 49
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel$Listener;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewChannelModelBase;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method
