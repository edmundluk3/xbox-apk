.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileCapturesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCaptureAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

.field private filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V
    .locals 3
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 323
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 326
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    .line 327
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 328
    return-void
.end method

.method private collectXuids(Ljava/util/Set;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 434
    .local p1, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p2, "captures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 435
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ICapture;

    .line 436
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getXuid()Ljava/lang/String;

    move-result-object v1

    .line 437
    .local v1, "xuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 438
    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 442
    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/ICapture;
    .end local v1    # "xuid":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 332
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 333
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProfileCapturesFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 341
    :cond_0
    :goto_0
    return v0

    .line 335
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 337
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z

    move-result v0

    goto :goto_0

    .line 339
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z

    move-result v0

    goto :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 13

    .prologue
    .line 369
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 370
    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 372
    .local v5, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    const/4 v0, 0x0

    .line 373
    .local v0, "captureData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 374
    .local v9, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v10, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProfileCapturesFilter:[I

    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 410
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Ljava/util/Hashtable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Hashtable;->clear()V

    .line 411
    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 412
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    .line 415
    .local v2, "mgr":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    :try_start_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-interface {v2, v9, v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileUsers(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;

    move-result-object v3

    .line 416
    .local v3, "profileUsers":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    if-eqz v3, :cond_2

    .line 417
    iget-object v10, v3, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;

    .line 418
    .local v7, "user":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    iget-object v8, v7, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->id:Ljava/lang/String;

    .line 419
    .local v8, "uuid":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;->getRealName()Ljava/lang/String;

    move-result-object v4

    .line 420
    .local v4, "realName":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 421
    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Ljava/util/Hashtable;

    move-result-object v11

    invoke-virtual {v11, v8, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 425
    .end local v3    # "profileUsers":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    .end local v4    # "realName":Ljava/lang/String;
    .end local v7    # "user":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUser;
    .end local v8    # "uuid":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 426
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v10, "GameProfileCapturesScreenViewModel"

    const-string v11, "Failed getting profile setting"

    invoke-static {v10, v11, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 430
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v2    # "mgr":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    :cond_2
    return-object v5

    .line 376
    :pswitch_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    iget-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->forceLoad:Z

    sget-object v12, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    .line 377
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    iget-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->forceLoad:Z

    sget-object v12, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v6

    .line 378
    .local v6, "status1":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 379
    move-object v5, v6

    .line 381
    :cond_3
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 382
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 383
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v0, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 384
    invoke-direct {p0, v9, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->collectXuids(Ljava/util/Set;Ljava/util/ArrayList;)V

    .line 386
    :cond_4
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 387
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 388
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v0, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 389
    invoke-direct {p0, v9, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->collectXuids(Ljava/util/Set;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 393
    .end local v6    # "status1":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :pswitch_1
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    iget-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->forceLoad:Z

    sget-object v12, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    .line 394
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 395
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 396
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v0, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 397
    invoke-direct {p0, v9, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->collectXuids(Ljava/util/Set;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 401
    :pswitch_2
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    iget-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->forceLoad:Z

    sget-object v12, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11, v12}, Lcom/microsoft/xbox/service/model/TitleModel;->loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    .line 402
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;

    move-result-object v0

    .line 403
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 404
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityScreenshotsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v10, v0, v11}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 405
    invoke-direct {p0, v9, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->collectXuids(Ljava/util/Set;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 364
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 346
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V

    .line 348
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V

    .line 360
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 320
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 352
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;Z)Z

    .line 354
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$LoadCaptureAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->updateAdapter()V

    .line 355
    return-void
.end method
