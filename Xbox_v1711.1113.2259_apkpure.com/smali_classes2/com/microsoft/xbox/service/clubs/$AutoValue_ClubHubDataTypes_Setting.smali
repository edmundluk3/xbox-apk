.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
.source "$AutoValue_ClubHubDataTypes_Setting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final allowedValues:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final canViewerAct:Z

.field private final canViewerChangeSetting:Z

.field private final value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lcom/google/common/collect/ImmutableList;ZZ)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "canViewerAct"    # Z
    .param p4, "canViewerChangeSetting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    .local p2, "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    .line 24
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerAct:Z

    .line 25
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerChangeSetting:Z

    .line 26
    return-void
.end method


# virtual methods
.method public allowedValues()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public canViewerAct()Z
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerAct:Z

    return v0
.end method

.method public canViewerChangeSetting()Z
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerChangeSetting:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 67
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<*>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_4

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerAct:Z

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerChangeSetting:Z

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerChangeSetting()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 67
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 68
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<*>;"
    :cond_5
    move v1, v2

    .line 72
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    const v5, 0xf4243

    .line 77
    const/4 v0, 0x1

    .line 78
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 80
    mul-int/2addr v0, v5

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 82
    mul-int/2addr v0, v5

    .line 83
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerAct:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    xor-int/2addr v0, v1

    .line 84
    mul-int/2addr v0, v5

    .line 85
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerChangeSetting:Z

    if-eqz v1, :cond_3

    :goto_3
    xor-int/2addr v0, v3

    .line 86
    return v0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    move v1, v4

    .line 83
    goto :goto_2

    :cond_3
    move v3, v4

    .line 85
    goto :goto_3
.end method

.method public toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting$Builder;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Setting{value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowedValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->allowedValues:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canViewerAct="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerAct:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canViewerChangeSetting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->canViewerChangeSetting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;->value:Ljava/lang/Object;

    return-object v0
.end method
