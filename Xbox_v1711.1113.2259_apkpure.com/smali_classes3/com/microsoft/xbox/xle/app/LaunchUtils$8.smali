.class final Lcom/microsoft/xbox/xle/app/LaunchUtils$8;
.super Ljava/lang/Object;
.source "LaunchUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/LaunchUtils;->getLaunchPostActionRunnable(Ljava/lang/String;IIJZ)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$contentId:Ljava/lang/String;

.field final synthetic val$launchRemoteControl:Z

.field final synthetic val$mediaGroup:I

.field final synthetic val$mediaTypeString:Ljava/lang/String;

.field final synthetic val$providerTitleId:J


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 441
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$providerTitleId:J

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$contentId:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaTypeString:Ljava/lang/String;

    iput p5, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaGroup:I

    iput-boolean p6, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$launchRemoteControl:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 446
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$providerTitleId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$contentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaTypeString:Ljava/lang/String;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaGroup:I

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    .line 448
    .local v1, "activityModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 449
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 450
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Found default companion, launch companion"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    .line 452
    .local v0, "activity":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    new-instance v2, Lcom/microsoft/xbox/xle/app/LaunchUtils$8$1;

    invoke-direct {v2, p0, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$8$1;-><init>(Lcom/microsoft/xbox/xle/app/LaunchUtils$8;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 469
    .end local v0    # "activity":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_0
    :goto_1
    return-void

    .line 446
    .end local v1    # "activityModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$contentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaTypeString:Ljava/lang/String;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$mediaGroup:I

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$providerTitleId:J

    .line 447
    invoke-static {v2, v3, v4, v6, v7}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    goto :goto_0

    .line 459
    .restart local v1    # "activityModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    :cond_2
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;->val$launchRemoteControl:Z

    if-eqz v2, :cond_0

    .line 460
    invoke-static {}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Did not find default companion, launch remote"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    new-instance v2, Lcom/microsoft/xbox/xle/app/LaunchUtils$8$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$8$2;-><init>(Lcom/microsoft/xbox/xle/app/LaunchUtils$8;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
