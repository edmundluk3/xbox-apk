.class public Lcom/microsoft/xbox/service/model/recents/Recent;
.super Ljava/lang/Object;
.source "Recent.java"


# instance fields
.field public DateAdded:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public DateModified:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public HydrationResult:Ljava/lang/String;

.field public Index:I

.field public Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

.field private transient hashCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    if-ne p1, p0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v1

    .line 34
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/recents/Recent;

    if-nez v3, :cond_2

    move v1, v2

    .line 35
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 38
    .local v0, "that":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->Index:I

    iget v4, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Index:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    if-nez v0, :cond_0

    .line 45
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    .line 46
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    .line 47
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->Index:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    .line 50
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/model/recents/Recent;->hashCode:I

    return v0
.end method
