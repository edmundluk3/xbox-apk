.class public final enum Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;
.super Ljava/lang/Enum;
.source "PeopleHubSocialListFilter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum BLOCKED:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum CLUBS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum FOLLOWERS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum FRIENDS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

.field public static final enum GAMES:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "ALL"

    const v2, 0x7f070b5a

    const-string v3, "All"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "CLUBS"

    const v2, 0x7f07032c

    const-string v3, "Clubs"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->CLUBS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "FRIENDS"

    const v2, 0x7f070ab4

    const-string v3, "Friends"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FRIENDS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "FOLLOWERS"

    const v2, 0x7f070511

    const-string v3, "Followers"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FOLLOWERS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "BLOCKED"

    const v2, 0x7f070ac6

    const-string v3, "Block"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->BLOCKED:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const-string v1, "GAMES"

    const/4 v2, 0x5

    const v3, 0x7f070ab6

    const-string v4, "Games"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->GAMES:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 16
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->CLUBS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FRIENDS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FOLLOWERS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->BLOCKED:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->GAMES:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 39
    iput p3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->resId:I

    .line 40
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->telemetryName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public static getMeProfileFilters()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->CLUBS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FRIENDS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FOLLOWERS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->BLOCKED:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->GAMES:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getYouProfileFilters()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->ALL:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->CLUBS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->FRIENDS:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->GAMES:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
