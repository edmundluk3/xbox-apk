.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;
.super Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;
.source "PeopleHubSocialListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GameViewHolder"
.end annotation


# instance fields
.field image:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0933
    .end annotation
.end field

.field name:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0934
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    .line 253
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;Landroid/view/View;)V

    .line 254
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 255
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "itemAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "itemAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    const v3, 0x7f0201fa

    .line 259
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 260
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 262
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 263
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;

    .line 265
    .local v0, "gameListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->getTitleImage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;->getTitleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter$GameViewHolder;Lcom/microsoft/xbox/toolkit/generics/Action;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    .end local v0    # "gameListItem":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$GameListItem;
    :goto_0
    return-void

    .line 270
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t bind GameViewHolder to non-GameListItem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method
