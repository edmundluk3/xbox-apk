.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;
.source "AutoValue_ClubModerationDataTypes_ClubReportedItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;Ljava/util/Date;IJLcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "contentType"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    .param p3, "lastReported"    # Ljava/util/Date;
    .param p4, "reportCount"    # I
    .param p5, "reportId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;",
            "Ljava/util/Date;",
            "IJ",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p7, "reports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    invoke-direct/range {p0 .. p7}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubReportedItem;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;Ljava/util/Date;IJLcom/google/common/collect/ImmutableList;)V

    .line 22
    return-void
.end method
