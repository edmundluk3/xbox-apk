.class public Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PartyMemberIconListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final SQUAWKER_RING_SIZE_SILENT:I

.field private static final SQUAWKER_RING_SIZE_TALKING:I


# instance fields
.field partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f090424

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->SQUAWKER_RING_SIZE_SILENT:I

    .line 36
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f090425

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->SQUAWKER_RING_SIZE_TALKING:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 42
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;)V

    .line 43
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->SQUAWKER_RING_SIZE_TALKING:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->SQUAWKER_RING_SIZE_SILENT:I

    return v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 73
    const v0, 0x7f0301a5

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 53
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;

    .line 54
    .local v0, "memberIconViewHolder":Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V

    .line 55
    return-void
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;ILjava/util/List;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p3, "payloads":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 69
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p1

    .line 62
    check-cast v1, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;

    .line 64
    .local v1, "memberIconViewHolder":Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 65
    .local v0, "latestPayload":Ljava/lang/Object;
    instance-of v2, v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    if-eqz v2, :cond_0

    .line 66
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .end local v0    # "latestPayload":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;-><init>(Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;Landroid/view/View;)V

    return-object v1
.end method
