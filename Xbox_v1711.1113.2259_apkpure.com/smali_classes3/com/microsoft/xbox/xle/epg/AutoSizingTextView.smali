.class public Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;
.super Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.source "AutoSizingTextView.java"


# instance fields
.field private defaultFontSize:F

.field private isAutoSizingEnabled:Z

.field private measurePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->measurePaint:Landroid/graphics/Paint;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->defaultFontSize:F

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->measurePaint:Landroid/graphics/Paint;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getTextSize()F

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->defaultFontSize:F

    .line 43
    return-void
.end method

.method private adjustTextSize(Ljava/lang/String;)V
    .locals 7
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 47
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v1, v4

    .line 49
    .local v1, "availableWidth":F
    const/4 v4, 0x0

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    .line 68
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->measurePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 54
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->measurePaint:Landroid/graphics/Paint;

    iget v5, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->defaultFontSize:F

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 56
    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->measurePaint:Landroid/graphics/Paint;

    invoke-virtual {v4, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 58
    .local v2, "measuredSize":F
    div-float v0, v1, v2

    .line 60
    .local v0, "availableToMeasuredRatio":F
    iget v3, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->defaultFontSize:F

    .line 61
    .local v3, "resultingSize":F
    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v0, v4

    if-gez v4, :cond_1

    .line 62
    mul-float/2addr v3, v0

    .line 63
    cmpl-float v4, v3, v6

    if-lez v4, :cond_1

    .line 64
    sub-float/2addr v3, v6

    .line 67
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v3}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setTextSize(IF)V

    goto :goto_0
.end method


# virtual methods
.method public getIsAutoSizingEnabled()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    return v0
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onMeasure(II)V

    .line 73
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->adjustTextSize(Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 88
    invoke-super {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onSizeChanged(IIII)V

    .line 89
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    if-eqz v0, :cond_0

    if-eq p1, p3, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->adjustTextSize(Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "lengthBefore"    # I
    .param p4, "lengthAfter"    # I

    .prologue
    .line 80
    invoke-super {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->adjustTextSize(Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method

.method public setIsAutoSizingEnabled(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->isAutoSizingEnabled:Z

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/epg/AutoSizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    return-void
.end method
