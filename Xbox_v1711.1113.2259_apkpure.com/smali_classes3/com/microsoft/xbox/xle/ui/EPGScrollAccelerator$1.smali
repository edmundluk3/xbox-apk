.class Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;
.super Ljava/lang/Object;
.source "EPGScrollAccelerator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->tweenIn(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

.field final synthetic val$desiredWidth:F


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;F)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->val$desiredWidth:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 228
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 229
    .local v0, "value":F
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->val$desiredWidth:F

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 230
    .local v1, "width":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$100(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 231
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$200(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 232
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$1;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->requestLayout()V

    .line 234
    return-void
.end method
