.class public Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;
.super Ljava/lang/Object;
.source "PurchaseWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MessageCaptureInterface"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MessageCaptureInterface"


# instance fields
.field mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;->mContext:Landroid/content/Context;

    .line 411
    return-void
.end method


# virtual methods
.method public onCaptureEvent(Ljava/lang/String;)V
    .locals 8
    .param p1, "data"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 415
    const-string v5, "MessageCaptureInterface"

    invoke-static {v5, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 419
    .local v2, "event":Lorg/json/JSONObject;
    const-string v5, "message"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 420
    const-string v5, "message"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v5, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 476
    .end local v2    # "event":Lorg/json/JSONObject;
    :cond_1
    :goto_1
    return-void

    .line 420
    .restart local v2    # "event":Lorg/json/JSONObject;
    :sswitch_0
    const-string v7, "ready"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :sswitch_1
    const-string v7, "done"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v0

    goto :goto_0

    :sswitch_2
    const-string v7, "error"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x2

    goto :goto_0

    :sswitch_3
    const-string v7, "openUrl"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x3

    goto :goto_0

    .line 422
    :pswitch_0
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 473
    .end local v2    # "event":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 474
    .local v1, "e":Lorg/json/JSONException;
    const-string v5, "MessageCaptureInterface"

    const-string v6, "parse json error"

    invoke-static {v5, v6, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 430
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v2    # "event":Lorg/json/JSONObject;
    :pswitch_1
    :try_start_1
    const-string/jumbo v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 431
    const-string/jumbo v5, "status"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 432
    .local v3, "status":Ljava/lang/String;
    const-string/jumbo v5, "success"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 433
    const-string v5, "MessageCaptureInterface"

    const-string v6, "purchase success"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$2;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 440
    :cond_2
    const-string v5, "cancel"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 441
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$3;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 451
    .end local v3    # "status":Ljava/lang/String;
    :pswitch_2
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$4;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 459
    :pswitch_3
    const-string/jumbo v5, "targetUrl"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string/jumbo v5, "targetUrl"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 460
    .local v4, "targetUrl":Ljava/lang/String;
    :goto_2
    const-string v5, "closeBlend"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "closeBlend"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 461
    .local v0, "closeBlend":Z
    :cond_3
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$5;

    invoke-direct {v5, p0, v4, v0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$MessageCaptureInterface;Ljava/lang/String;Z)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    .line 459
    .end local v0    # "closeBlend":Z
    .end local v4    # "targetUrl":Ljava/lang/String;
    :cond_4
    const-string v4, ""
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 420
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4b4af53b -> :sswitch_3
        0x2f2382 -> :sswitch_1
        0x5c4d208 -> :sswitch_2
        0x675d9a3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
