.class public interface abstract Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
.super Ljava/lang/Object;
.source "LanguageSettingsRepository.java"


# virtual methods
.method public abstract getCurrentPrefs()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLanguageInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
.end method

.method public abstract getLocationInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
.end method

.method public abstract prefs()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setUserDefinedLanguage(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setUserDefinedLocation(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation
.end method
