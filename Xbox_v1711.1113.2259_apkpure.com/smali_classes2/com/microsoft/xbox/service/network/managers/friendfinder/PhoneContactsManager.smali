.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
.super Ljava/lang/Object;
.source "PhoneContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
    }
.end annotation


# static fields
.field private static final MAX_UPLOAD_NUM_PER_REQUEST:I = 0x64

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;


# instance fields
.field private currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

.field private currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

.field private dialog:Landroid/app/Dialog;

.field private getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

.field private meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

.field private optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

.field private optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

.field private phoneVerificationToken:Ljava/lang/String;

.field private privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field private recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

.field private shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

.field private targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->TAG:Ljava/lang/String;

    .line 82
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->NotSet:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 76
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 90
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$1;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 95
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->phoneVerificationToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->needUpdatePrivacy()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method private cancelGetPeopleHubRecommendationsAsyncTask()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;->cancel()V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    .line 353
    :cond_0
    return-void
.end method

.method private cancelShortCircuitProfileAsyncTask()V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->cancel()V

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    .line 407
    :cond_0
    return-void
.end method

.method private dismissCurrentDialog()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 394
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 395
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    return-object v0
.end method

.method private loadPeopleHubRecommendationsAsyncTask()V
    .locals 2

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->cancelGetPeopleHubRecommendationsAsyncTask()V

    .line 357
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 358
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$GetPeopleHubRecommendationsAsyncTask;->load(Z)V

    .line 360
    return-void
.end method

.method private needUpdatePrivacy()Z
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->NotSet:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;)V
    .locals 0
    .param p1, "task"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    .prologue
    .line 231
    if-eqz p1, :cond_0

    .line 232
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->cancel()V

    .line 234
    :cond_0
    return-void
.end method

.method public getPhoneContactsList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v1, 0x0

    .line 101
    .local v1, "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->recommendationSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;

    iget v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;->phoneContact:I

    if-lez v5, :cond_0

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .restart local v1    # "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->recommendationSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;

    iget v4, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;->facebookFriend:I

    .line 104
    .local v4, "startIndex":I
    move v0, v4

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->recommendationSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;

    iget v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;->phoneContact:I

    if-ge v0, v5, :cond_0

    .line 105
    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 106
    .local v2, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-direct {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 107
    .local v3, "personSelector":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    iget-boolean v5, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isIdentityShared:Z

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 108
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "i":I
    .end local v2    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v3    # "personSelector":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .end local v4    # "startIndex":I
    :cond_0
    return-object v1
.end method

.method public getPhoneState()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    .locals 4

    .prologue
    .line 208
    const/4 v2, 0x0

    .line 209
    .local v2, "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    if-eqz v3, :cond_0

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "phoneNumber":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->isVerified(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;

    move-result-object v2

    .line 212
    if-nez v2, :cond_0

    .line 213
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getFullPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "fullPhoneNumber":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->myPhoneProfile:Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->isVerified(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;

    move-result-object v2

    .line 217
    .end local v0    # "fullPhoneNumber":Ljava/lang/String;
    .end local v1    # "phoneNumber":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method public linkUnlink()V
    .locals 6

    .prologue
    .line 188
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v1

    .line 189
    .local v1, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    .line 191
    .local v2, "status":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_1

    .line 192
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOutDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 193
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOut:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 194
    new-instance v3, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 195
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    check-cast v3, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 205
    .end local v2    # "status":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_0
    :goto_0
    return-void

    .line 196
    .restart local v2    # "status":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_0

    .line 197
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsOptInView()V

    .line 198
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptInDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 199
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 200
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;-><init>(Landroid/content/Context;)V

    .line 201
    .local v0, "dialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 202
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    goto :goto_0
.end method

.method public loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V
    .locals 2
    .param p1, "task"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->cancelShortCircuitProfileAsyncTask()V

    .line 226
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->shortCircuitProfileAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileAsyncTask;->load(Z)V

    .line 228
    return-void
.end method

.method public loadUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 5
    .param p1, "nextAction"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    const/4 v4, 0x1

    .line 237
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$2;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 257
    :goto_0
    return-void

    .line 239
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->cancelUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;)V

    .line 240
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    .line 241
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 244
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->cancelUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;)V

    .line 245
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    .line 246
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 249
    :pswitch_2
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 250
    .local v1, "resetTask":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;
    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 253
    .end local v1    # "resetTask":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 254
    .local v0, "dontShow":Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$UpdatePhoneOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public notInterested()V
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 222
    return-void
.end method

.method protected onGetPeopleHubRecommendationsAsyncTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 363
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 367
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 368
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v1, v2, :cond_0

    .line 369
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubRecommendationsRawData()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    .line 370
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 376
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 377
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-class v1, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;->PHONE_CONTACTS:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onShortCircuitProfileAsyncTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "task"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .prologue
    .line 311
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 315
    :pswitch_0
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne p2, v3, :cond_2

    .line 316
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v3, v4, :cond_1

    .line 317
    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 318
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->ServerOptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 320
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPhoneContactsUploadTimestamp()V

    goto :goto_0

    .line 321
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    if-ne p2, v3, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getPhoneState()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;

    move-result-object v2

    .line 324
    .local v2, "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    if-eqz v2, :cond_0

    iget-boolean v3, v2, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;->isVerified:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;->hasXLEApplication:Z

    if-eqz v3, :cond_0

    .line 325
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 326
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;-><init>(Landroid/content/Context;)V

    .line 327
    .local v1, "phoneContactDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 328
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->UploadPhoneContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 330
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 331
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    goto :goto_0

    .line 337
    .end local v1    # "phoneContactDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .end local v2    # "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->targetState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptIn:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v3, v4, :cond_0

    .line 338
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 339
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;-><init>(Landroid/content/Context;)V

    .line 340
    .local v0, "dialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 341
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 342
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    goto :goto_0

    .line 311
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onUpdatePhoneOptInStatusCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "optInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    const/4 v3, 0x1

    .line 264
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 308
    :goto_0
    return-void

    .line 273
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq p2, v1, :cond_0

    .line 274
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 275
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadPeopleHubRecommendationsAsyncTask()V

    .line 277
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->needUpdatePrivacy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279
    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 281
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne p2, v1, :cond_3

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 296
    :cond_2
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    goto :goto_0

    .line 283
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne p2, v1, :cond_2

    .line 284
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    if-nez v1, :cond_4

    .line 285
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 288
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    if-nez v1, :cond_5

    .line 289
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 292
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    check-cast v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 293
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadPeopleHubRecommendationsAsyncTask()V

    goto :goto_1

    .line 300
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 302
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;-><init>(Landroid/content/Context;)V

    .line 303
    .local v0, "dialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 304
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 305
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public periodcallyUploadPhoneContacts()V
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->shoulddUploadPhoneContacts()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 123
    .local v0, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getPhoneAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v1

    .line 125
    .local v1, "status":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v1, v2, :cond_0

    .line 126
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->UploadPhoneContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 127
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 131
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .end local v1    # "status":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_0
    return-void
.end method

.method public varargs returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V
    .locals 6
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
    .param p2, "isConfirmed"    # Z
    .param p3, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 134
    if-nez p2, :cond_1

    .line 135
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 136
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->VerifyPhoneTokenDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v2, v3, :cond_0

    .line 137
    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->show()V

    .line 139
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->AddPhoneNumberDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 185
    :goto_0
    return-void

    .line 141
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Unknown:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    goto :goto_0

    .line 147
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$2;->$SwitchMap$com$microsoft$xbox$service$network$managers$friendfinder$PhoneContactsManager$State:[I

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 149
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    check-cast v2, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 150
    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    goto :goto_0

    .line 153
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 154
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 155
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->AddPhoneNumberDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 156
    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactAddPhoneNumberDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 157
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->show()V

    goto :goto_0

    .line 160
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->dismissCurrentDialog()V

    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;-><init>(Landroid/content/Context;)V

    .line 162
    .local v0, "phoneContactVerifyPhoneDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentDialog:Lcom/microsoft/xbox/toolkit/XLEManagedDialog;

    .line 163
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->VerifyPhoneTokenDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getPhoneState()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;

    move-result-object v1

    .line 167
    .local v1, "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    if-eqz v1, :cond_2

    iget-boolean v2, v1, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;->isVerified:Z

    if-eqz v2, :cond_2

    iget-boolean v2, v1, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;->hasXLEApplication:Z

    if-eqz v2, :cond_2

    .line 168
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 169
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v0, v2, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->currentState:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v0, v2, v5}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V

    .line 172
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    goto :goto_0

    .line 176
    .end local v0    # "phoneContactVerifyPhoneDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    .end local v1    # "phoneState":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$PhoneState;
    :pswitch_3
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty([Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 177
    aget-object v2, p3, v5

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->phoneVerificationToken:Ljava/lang/String;

    move-object v0, p1

    .line 178
    check-cast v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;

    .line 179
    .restart local v0    # "phoneContactVerifyPhoneDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;->setBusy(Z)V

    .line 180
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 183
    .end local v0    # "phoneContactVerifyPhoneDialog":Lcom/microsoft/xbox/xle/app/dialog/PhoneContactVerifyPhoneDialog;
    :cond_3
    :pswitch_4
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    goto/16 :goto_0

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setPhoneVerificationToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->phoneVerificationToken:Ljava/lang/String;

    .line 116
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->cancelShortCircuitProfileAsyncTask()V

    .line 117
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 118
    return-void
.end method

.method public updatePrivacyValue(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 0
    .param p1, "privacyValue"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 261
    return-void
.end method
