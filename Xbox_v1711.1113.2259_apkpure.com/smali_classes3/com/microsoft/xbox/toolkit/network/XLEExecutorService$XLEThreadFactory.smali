.class Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;
.super Ljava/lang/Object;
.source "XLEExecutorService.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "XLEThreadFactory"
.end annotation


# instance fields
.field private final name:Ljava/lang/String;

.field private final priority:I


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "priority"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;->name:Ljava/lang/String;

    .line 25
    iput p2, p0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;->priority:I

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$1;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEThread;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEThread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 33
    .local v0, "thread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 34
    iget v1, p0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService$XLEThreadFactory;->priority:I

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 36
    return-object v0
.end method
