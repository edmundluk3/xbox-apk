.class public Lorg/webrtc/EglRenderer;
.super Ljava/lang/Object;
.source "EglRenderer.java"

# interfaces
.implements Lorg/webrtc/VideoRenderer$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/webrtc/EglRenderer$EglSurfaceCreation;,
        Lorg/webrtc/EglRenderer$FrameListenerAndParams;,
        Lorg/webrtc/EglRenderer$FrameListener;
    }
.end annotation


# static fields
.field private static final LOG_INTERVAL_SEC:J = 0x4L

.field private static final MAX_SURFACE_CLEAR_COUNT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "EglRenderer"


# instance fields
.field private bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

.field private drawer:Lorg/webrtc/RendererCommon$GlDrawer;

.field private eglBase:Lorg/webrtc/EglBase;

.field private final eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

.field private final fpsReductionLock:Ljava/lang/Object;

.field private final frameListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/webrtc/EglRenderer$FrameListenerAndParams;",
            ">;"
        }
    .end annotation
.end field

.field private final frameLock:Ljava/lang/Object;

.field private framesDropped:I

.field private framesReceived:I

.field private framesRendered:I

.field private final handlerLock:Ljava/lang/Object;

.field private layoutAspectRatio:F

.field private final layoutLock:Ljava/lang/Object;

.field private final logStatisticsRunnable:Ljava/lang/Runnable;

.field private minRenderPeriodNs:J

.field private mirror:Z

.field private final name:Ljava/lang/String;

.field private nextFrameTimeNs:J

.field private pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

.field private final renderFrameRunnable:Ljava/lang/Runnable;

.field private renderSwapBufferTimeNs:J

.field private renderThreadHandler:Landroid/os/Handler;

.field private renderTimeNs:J

.field private final statisticsLock:Ljava/lang/Object;

.field private statisticsStartTimeNs:J

.field private final yuvUploader:Lorg/webrtc/RendererCommon$YuvUploader;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->fpsReductionLock:Ljava/lang/Object;

    .line 96
    new-instance v0, Lorg/webrtc/RendererCommon$YuvUploader;

    invoke-direct {v0}, Lorg/webrtc/RendererCommon$YuvUploader;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->yuvUploader:Lorg/webrtc/RendererCommon$YuvUploader;

    .line 100
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->frameLock:Ljava/lang/Object;

    .line 104
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->layoutLock:Ljava/lang/Object;

    .line 110
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    .line 129
    new-instance v0, Lorg/webrtc/EglRenderer$1;

    invoke-direct {v0, p0}, Lorg/webrtc/EglRenderer$1;-><init>(Lorg/webrtc/EglRenderer;)V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->renderFrameRunnable:Ljava/lang/Runnable;

    .line 136
    new-instance v0, Lorg/webrtc/EglRenderer$2;

    invoke-direct {v0, p0}, Lorg/webrtc/EglRenderer$2;-><init>(Lorg/webrtc/EglRenderer;)V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->logStatisticsRunnable:Ljava/lang/Runnable;

    .line 150
    new-instance v0, Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/webrtc/EglRenderer$EglSurfaceCreation;-><init>(Lorg/webrtc/EglRenderer;Lorg/webrtc/EglRenderer$1;)V

    iput-object v0, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    .line 157
    iput-object p1, p0, Lorg/webrtc/EglRenderer;->name:Ljava/lang/String;

    .line 158
    return-void
.end method

.method static synthetic access$000(Lorg/webrtc/EglRenderer;)Lorg/webrtc/EglBase;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    return-object v0
.end method

.method static synthetic access$002(Lorg/webrtc/EglRenderer;Lorg/webrtc/EglBase;)Lorg/webrtc/EglBase;
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;
    .param p1, "x1"    # Lorg/webrtc/EglBase;

    .prologue
    .line 32
    iput-object p1, p0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    return-object p1
.end method

.method static synthetic access$100(Lorg/webrtc/EglRenderer;)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/webrtc/EglRenderer;->renderFrameOnRenderThread()V

    return-void
.end method

.method static synthetic access$1000(Lorg/webrtc/EglRenderer;)Lorg/webrtc/GlTextureFrameBuffer;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    return-object v0
.end method

.method static synthetic access$1002(Lorg/webrtc/EglRenderer;Lorg/webrtc/GlTextureFrameBuffer;)Lorg/webrtc/GlTextureFrameBuffer;
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;
    .param p1, "x1"    # Lorg/webrtc/GlTextureFrameBuffer;

    .prologue
    .line 32
    iput-object p1, p0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    return-object p1
.end method

.method static synthetic access$1100(Lorg/webrtc/EglRenderer;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lorg/webrtc/EglRenderer;)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/webrtc/EglRenderer;->clearSurfaceOnRenderThread()V

    return-void
.end method

.method static synthetic access$200(Lorg/webrtc/EglRenderer;)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/webrtc/EglRenderer;->logStatistics()V

    return-void
.end method

.method static synthetic access$300(Lorg/webrtc/EglRenderer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lorg/webrtc/EglRenderer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lorg/webrtc/EglRenderer;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->logStatisticsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lorg/webrtc/EglRenderer;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lorg/webrtc/EglRenderer;)Lorg/webrtc/RendererCommon$GlDrawer;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    return-object v0
.end method

.method static synthetic access$802(Lorg/webrtc/EglRenderer;Lorg/webrtc/RendererCommon$GlDrawer;)Lorg/webrtc/RendererCommon$GlDrawer;
    .locals 0
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;
    .param p1, "x1"    # Lorg/webrtc/RendererCommon$GlDrawer;

    .prologue
    .line 32
    iput-object p1, p0, Lorg/webrtc/EglRenderer;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    return-object p1
.end method

.method static synthetic access$900(Lorg/webrtc/EglRenderer;)Lorg/webrtc/RendererCommon$YuvUploader;
    .locals 1
    .param p0, "x0"    # Lorg/webrtc/EglRenderer;

    .prologue
    .line 32
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->yuvUploader:Lorg/webrtc/RendererCommon$YuvUploader;

    return-object v0
.end method

.method private averageTimeAsString(JI)Ljava/lang/String;
    .locals 5
    .param p1, "sumTimeNs"    # J
    .param p3, "count"    # I

    .prologue
    .line 680
    if-gtz p3, :cond_0

    const-string v0, "NA"

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v2, p3

    div-long v2, p1, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u03bcs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private clearSurfaceOnRenderThread()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 485
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v0}, Lorg/webrtc/EglBase;->hasSurface()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    const-string v0, "clearSurface"

    invoke-direct {p0, v0}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 487
    invoke-static {v1, v1, v1, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 488
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 489
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v0}, Lorg/webrtc/EglBase;->swapBuffers()V

    .line 491
    :cond_0
    return-void
.end method

.method private createEglSurfaceInternal(Ljava/lang/Object;)V
    .locals 1
    .param p1, "surface"    # Ljava/lang/Object;

    .prologue
    .line 213
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    invoke-virtual {v0, p1}, Lorg/webrtc/EglRenderer$EglSurfaceCreation;->setSurface(Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    invoke-direct {p0, v0}, Lorg/webrtc/EglRenderer;->postToRenderThread(Ljava/lang/Runnable;)V

    .line 215
    return-void
.end method

.method private logD(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 704
    const-string v0, "EglRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/webrtc/EglRenderer;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/webrtc/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    return-void
.end method

.method private logStatistics()V
    .locals 12

    .prologue
    .line 684
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 685
    .local v0, "currentTimeNs":J
    iget-object v6, p0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    monitor-enter v6

    .line 686
    :try_start_0
    iget-wide v8, p0, Lorg/webrtc/EglRenderer;->statisticsStartTimeNs:J

    sub-long v2, v0, v8

    .line 687
    .local v2, "elapsedTimeNs":J
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-gtz v5, :cond_0

    .line 688
    monitor-exit v6

    .line 701
    :goto_0
    return-void

    .line 690
    :cond_0
    iget v5, p0, Lorg/webrtc/EglRenderer;->framesRendered:I

    int-to-long v8, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x1

    invoke-virtual {v5, v10, v11}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v10

    mul-long/2addr v8, v10

    long-to-float v5, v8

    long-to-float v7, v2

    div-float v4, v5, v7

    .line 691
    .local v4, "renderFps":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Duration: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ms. Frames received: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p0, Lorg/webrtc/EglRenderer;->framesReceived:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". Dropped: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p0, Lorg/webrtc/EglRenderer;->framesDropped:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". Rendered: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p0, Lorg/webrtc/EglRenderer;->framesRendered:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". Render fps: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%.1f"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 695
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". Average render time: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v8, p0, Lorg/webrtc/EglRenderer;->renderTimeNs:J

    iget v7, p0, Lorg/webrtc/EglRenderer;->framesRendered:I

    .line 696
    invoke-direct {p0, v8, v9, v7}, Lorg/webrtc/EglRenderer;->averageTimeAsString(JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ". Average swapBuffer time: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v8, p0, Lorg/webrtc/EglRenderer;->renderSwapBufferTimeNs:J

    iget v7, p0, Lorg/webrtc/EglRenderer;->framesRendered:I

    .line 698
    invoke-direct {p0, v8, v9, v7}, Lorg/webrtc/EglRenderer;->averageTimeAsString(JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 691
    invoke-direct {p0, v5}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 699
    invoke-direct {p0, v0, v1}, Lorg/webrtc/EglRenderer;->resetStatistics(J)V

    .line 700
    monitor-exit v6

    goto/16 :goto_0

    .end local v2    # "elapsedTimeNs":J
    .end local v4    # "renderFps":F
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method private notifyCallbacks(Lorg/webrtc/VideoRenderer$I420Frame;[I[F)V
    .locals 16
    .param p1, "frame"    # Lorg/webrtc/VideoRenderer$I420Frame;
    .param p2, "yuvTextures"    # [I
    .param p3, "texMatrix"    # [F

    .prologue
    .line 628
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    :goto_0
    return-void

    .line 631
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lorg/webrtc/EglRenderer;->mirror:Z

    if-eqz v1, :cond_2

    .line 633
    invoke-static {}, Lorg/webrtc/RendererCommon;->horizontalFlipMatrix()[F

    move-result-object v1

    .line 632
    :goto_1
    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lorg/webrtc/RendererCommon;->multiplyMatrices([F[F)[F

    move-result-object v1

    .line 634
    invoke-static {}, Lorg/webrtc/RendererCommon;->verticalFlipMatrix()[F

    move-result-object v2

    .line 631
    invoke-static {v1, v2}, Lorg/webrtc/RendererCommon;->multiplyMatrices([F[F)[F

    move-result-object v3

    .line 636
    .local v3, "bitmapMatrix":[F
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;

    .line 637
    .local v14, "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    iget v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->scale:F

    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v8, v1

    .line 638
    .local v8, "scaledWidth":I
    iget v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->scale:F

    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v9, v1

    .line 640
    .local v9, "scaledHeight":I
    if-eqz v8, :cond_1

    if-nez v9, :cond_3

    .line 641
    :cond_1
    iget-object v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->listener:Lorg/webrtc/EglRenderer$FrameListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lorg/webrtc/EglRenderer$FrameListener;->onFrame(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 633
    .end local v3    # "bitmapMatrix":[F
    .end local v8    # "scaledWidth":I
    .end local v9    # "scaledHeight":I
    .end local v14    # "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    :cond_2
    invoke-static {}, Lorg/webrtc/RendererCommon;->identityMatrix()[F

    move-result-object v1

    goto :goto_1

    .line 645
    .restart local v3    # "bitmapMatrix":[F
    .restart local v8    # "scaledWidth":I
    .restart local v9    # "scaledHeight":I
    .restart local v14    # "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    if-nez v1, :cond_4

    .line 646
    new-instance v1, Lorg/webrtc/GlTextureFrameBuffer;

    const/16 v2, 0x1908

    invoke-direct {v1, v2}, Lorg/webrtc/GlTextureFrameBuffer;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    .line 648
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    invoke-virtual {v1, v8, v9}, Lorg/webrtc/GlTextureFrameBuffer;->setSize(II)V

    .line 650
    const v1, 0x8d40

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    invoke-virtual {v2}, Lorg/webrtc/GlTextureFrameBuffer;->getFrameBufferId()I

    move-result v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 651
    const v1, 0x8d40

    const v2, 0x8ce0

    const/16 v4, 0xde1

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/webrtc/EglRenderer;->bitmapTextureFramebuffer:Lorg/webrtc/GlTextureFrameBuffer;

    .line 652
    invoke-virtual {v5}, Lorg/webrtc/GlTextureFrameBuffer;->getTextureId()I

    move-result v5

    const/4 v6, 0x0

    .line 651
    invoke-static {v1, v2, v4, v5, v6}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 654
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 655
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 656
    move-object/from16 v0, p1

    iget-boolean v1, v0, Lorg/webrtc/VideoRenderer$I420Frame;->yuvFrame:Z

    if-eqz v1, :cond_5

    .line 657
    iget-object v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v4

    .line 658
    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p2

    .line 657
    invoke-interface/range {v1 .. v9}, Lorg/webrtc/RendererCommon$GlDrawer;->drawYuv([I[FIIIIII)V

    .line 664
    :goto_3
    mul-int v1, v8, v9

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 665
    .local v12, "bitmapBuffer":Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v8, v9}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 666
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v10, 0x1908

    const/16 v11, 0x1401

    invoke-static/range {v6 .. v12}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 669
    const v1, 0x8d40

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 670
    const-string v1, "EglRenderer.notifyCallbacks"

    invoke-static {v1}, Lorg/webrtc/GlUtil;->checkNoGLES2Error(Ljava/lang/String;)V

    .line 672
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 673
    .local v13, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v13, v12}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 674
    iget-object v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->listener:Lorg/webrtc/EglRenderer$FrameListener;

    invoke-interface {v1, v13}, Lorg/webrtc/EglRenderer$FrameListener;->onFrame(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 660
    .end local v12    # "bitmapBuffer":Ljava/nio/ByteBuffer;
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v1, v14, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    move-object/from16 v0, p1

    iget v2, v0, Lorg/webrtc/VideoRenderer$I420Frame;->textureId:I

    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v4

    .line 661
    invoke-virtual/range {p1 .. p1}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 660
    invoke-interface/range {v1 .. v9}, Lorg/webrtc/RendererCommon$GlDrawer;->drawOes(I[FIIIIII)V

    goto :goto_3

    .line 676
    .end local v8    # "scaledWidth":I
    .end local v9    # "scaledHeight":I
    .end local v14    # "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0
.end method

.method private postToRenderThread(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 477
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 478
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 481
    :cond_0
    monitor-exit v1

    .line 482
    return-void

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private renderFrameOnRenderThread()V
    .locals 31

    .prologue
    .line 516
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/webrtc/EglRenderer;->frameLock:Ljava/lang/Object;

    monitor-enter v9

    .line 517
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    if-nez v4, :cond_0

    .line 518
    monitor-exit v9

    .line 624
    :goto_0
    return-void

    .line 520
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    move-object/from16 v20, v0

    .line 521
    .local v20, "frame":Lorg/webrtc/VideoRenderer$I420Frame;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    .line 522
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 523
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v4}, Lorg/webrtc/EglBase;->hasSurface()Z

    move-result v4

    if-nez v4, :cond_2

    .line 524
    :cond_1
    const-string v4, "Dropping frame - No surface"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 525
    invoke-static/range {v20 .. v20}, Lorg/webrtc/VideoRenderer;->renderFrameDone(Lorg/webrtc/VideoRenderer$I420Frame;)V

    goto :goto_0

    .line 522
    .end local v20    # "frame":Lorg/webrtc/VideoRenderer$I420Frame;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 530
    .restart local v20    # "frame":Lorg/webrtc/VideoRenderer$I420Frame;
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/webrtc/EglRenderer;->fpsReductionLock:Ljava/lang/Object;

    monitor-enter v9

    .line 531
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    const-wide v12, 0x7fffffffffffffffL

    cmp-long v4, v10, v12

    if-nez v4, :cond_6

    .line 533
    const/16 v24, 0x0

    .line 549
    .local v24, "shouldRenderFrame":Z
    :goto_1
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 551
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v26

    .line 552
    .local v26, "startTimeNs":J
    move-object/from16 v0, v20

    iget-object v4, v0, Lorg/webrtc/VideoRenderer$I420Frame;->samplingMatrix:[F

    move-object/from16 v0, v20

    iget v9, v0, Lorg/webrtc/VideoRenderer$I420Frame;->rotationDegree:I

    int-to-float v9, v9

    .line 553
    invoke-static {v4, v9}, Lorg/webrtc/RendererCommon;->rotateTextureMatrix([FF)[F

    move-result-object v30

    .line 561
    .local v30, "texMatrix":[F
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/webrtc/EglRenderer;->layoutLock:Ljava/lang/Object;

    monitor-enter v9

    .line 563
    :try_start_3
    move-object/from16 v0, p0

    iget v4, v0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    const/4 v10, 0x0

    cmpl-float v4, v4, v10

    if-lez v4, :cond_a

    .line 564
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v10

    int-to-float v10, v10

    div-float v21, v4, v10

    .line 565
    .local v21, "frameAspectRatio":F
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/webrtc/EglRenderer;->mirror:Z

    move-object/from16 v0, p0

    iget v10, v0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    move/from16 v0, v21

    invoke-static {v4, v0, v10}, Lorg/webrtc/RendererCommon;->getLayoutMatrix(ZFF)[F

    move-result-object v22

    .line 566
    .local v22, "layoutMatrix":[F
    move-object/from16 v0, p0

    iget v4, v0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    cmpl-float v4, v21, v4

    if-lez v4, :cond_9

    .line 567
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v10, v0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    mul-float/2addr v4, v10

    float-to-int v7, v4

    .line 568
    .local v7, "drawnFrameWidth":I
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v8

    .line 579
    .end local v21    # "frameAspectRatio":F
    .local v8, "drawnFrameHeight":I
    :goto_2
    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/webrtc/RendererCommon;->multiplyMatrices([F[F)[F

    move-result-object v6

    .line 580
    .local v6, "drawMatrix":[F
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 582
    const/16 v25, 0x0

    .line 583
    .local v25, "shouldUploadYuvTextures":Z
    move-object/from16 v0, v20

    iget-boolean v4, v0, Lorg/webrtc/VideoRenderer$I420Frame;->yuvFrame:Z

    if-eqz v4, :cond_4

    .line 584
    move/from16 v25, v24

    .line 587
    if-nez v25, :cond_4

    .line 588
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->frameListeners:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/webrtc/EglRenderer$FrameListenerAndParams;

    .line 589
    .local v23, "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    move-object/from16 v0, v23

    iget v9, v0, Lorg/webrtc/EglRenderer$FrameListenerAndParams;->scale:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_3

    .line 590
    const/16 v25, 0x1

    .line 596
    .end local v23    # "listenerAndParams":Lorg/webrtc/EglRenderer$FrameListenerAndParams;
    :cond_4
    if-eqz v25, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->yuvUploader:Lorg/webrtc/RendererCommon$YuvUploader;

    move-object/from16 v0, v20

    iget v9, v0, Lorg/webrtc/VideoRenderer$I420Frame;->width:I

    move-object/from16 v0, v20

    iget v10, v0, Lorg/webrtc/VideoRenderer$I420Frame;->height:I

    move-object/from16 v0, v20

    iget-object v11, v0, Lorg/webrtc/VideoRenderer$I420Frame;->yuvStrides:[I

    move-object/from16 v0, v20

    iget-object v12, v0, Lorg/webrtc/VideoRenderer$I420Frame;->yuvPlanes:[Ljava/nio/ByteBuffer;

    .line 597
    invoke-virtual {v4, v9, v10, v11, v12}, Lorg/webrtc/RendererCommon$YuvUploader;->uploadYuvData(II[I[Ljava/nio/ByteBuffer;)[I

    move-result-object v5

    .line 600
    .local v5, "yuvTextures":[I
    :goto_3
    if-eqz v24, :cond_5

    .line 601
    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v4, v9, v10, v11}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 602
    const/16 v4, 0x4000

    invoke-static {v4}, Landroid/opengl/GLES20;->glClear(I)V

    .line 603
    move-object/from16 v0, v20

    iget-boolean v4, v0, Lorg/webrtc/VideoRenderer$I420Frame;->yuvFrame:Z

    if-eqz v4, :cond_d

    .line 604
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    .line 605
    invoke-virtual {v11}, Lorg/webrtc/EglBase;->surfaceWidth()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v12}, Lorg/webrtc/EglBase;->surfaceHeight()I

    move-result v12

    .line 604
    invoke-interface/range {v4 .. v12}, Lorg/webrtc/RendererCommon$GlDrawer;->drawYuv([I[FIIIIII)V

    .line 611
    :goto_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v28

    .line 612
    .local v28, "swapBuffersStartTimeNs":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v4}, Lorg/webrtc/EglBase;->swapBuffers()V

    .line 614
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    .line 615
    .local v18, "currentTimeNs":J
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    monitor-enter v9

    .line 616
    :try_start_4
    move-object/from16 v0, p0

    iget v4, v0, Lorg/webrtc/EglRenderer;->framesRendered:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lorg/webrtc/EglRenderer;->framesRendered:I

    .line 617
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->renderTimeNs:J

    sub-long v12, v18, v26

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/webrtc/EglRenderer;->renderTimeNs:J

    .line 618
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->renderSwapBufferTimeNs:J

    sub-long v12, v18, v28

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/webrtc/EglRenderer;->renderSwapBufferTimeNs:J

    .line 619
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 622
    .end local v18    # "currentTimeNs":J
    .end local v28    # "swapBuffersStartTimeNs":J
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v5, v2}, Lorg/webrtc/EglRenderer;->notifyCallbacks(Lorg/webrtc/VideoRenderer$I420Frame;[I[F)V

    .line 623
    invoke-static/range {v20 .. v20}, Lorg/webrtc/VideoRenderer;->renderFrameDone(Lorg/webrtc/VideoRenderer$I420Frame;)V

    goto/16 :goto_0

    .line 534
    .end local v5    # "yuvTextures":[I
    .end local v6    # "drawMatrix":[F
    .end local v7    # "drawnFrameWidth":I
    .end local v8    # "drawnFrameHeight":I
    .end local v22    # "layoutMatrix":[F
    .end local v24    # "shouldRenderFrame":Z
    .end local v25    # "shouldUploadYuvTextures":Z
    .end local v26    # "startTimeNs":J
    .end local v30    # "texMatrix":[F
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    const-wide/16 v12, 0x0

    cmp-long v4, v10, v12

    if-gtz v4, :cond_7

    .line 536
    const/16 v24, 0x1

    .restart local v24    # "shouldRenderFrame":Z
    goto/16 :goto_1

    .line 538
    .end local v24    # "shouldRenderFrame":Z
    :cond_7
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    .line 539
    .restart local v18    # "currentTimeNs":J
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    cmp-long v4, v18, v10

    if-gez v4, :cond_8

    .line 540
    const-string v4, "Skipping frame rendering - fps reduction is active."

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 541
    const/16 v24, 0x0

    .restart local v24    # "shouldRenderFrame":Z
    goto/16 :goto_1

    .line 543
    .end local v24    # "shouldRenderFrame":Z
    :cond_8
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    .line 545
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    move-wide/from16 v0, v18

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    .line 546
    const/16 v24, 0x1

    .restart local v24    # "shouldRenderFrame":Z
    goto/16 :goto_1

    .line 549
    .end local v18    # "currentTimeNs":J
    .end local v24    # "shouldRenderFrame":Z
    :catchall_1
    move-exception v4

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    .line 570
    .restart local v21    # "frameAspectRatio":F
    .restart local v22    # "layoutMatrix":[F
    .restart local v24    # "shouldRenderFrame":Z
    .restart local v26    # "startTimeNs":J
    .restart local v30    # "texMatrix":[F
    :cond_9
    :try_start_6
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v7

    .line 571
    .restart local v7    # "drawnFrameWidth":I
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v10, v0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    div-float/2addr v4, v10

    float-to-int v8, v4

    .restart local v8    # "drawnFrameHeight":I
    goto/16 :goto_2

    .line 574
    .end local v7    # "drawnFrameWidth":I
    .end local v8    # "drawnFrameHeight":I
    .end local v21    # "frameAspectRatio":F
    .end local v22    # "layoutMatrix":[F
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/webrtc/EglRenderer;->mirror:Z

    if-eqz v4, :cond_b

    .line 575
    invoke-static {}, Lorg/webrtc/RendererCommon;->horizontalFlipMatrix()[F

    move-result-object v22

    .line 576
    .restart local v22    # "layoutMatrix":[F
    :goto_5
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedWidth()I

    move-result v7

    .line 577
    .restart local v7    # "drawnFrameWidth":I
    invoke-virtual/range {v20 .. v20}, Lorg/webrtc/VideoRenderer$I420Frame;->rotatedHeight()I

    move-result v8

    .restart local v8    # "drawnFrameHeight":I
    goto/16 :goto_2

    .line 575
    .end local v7    # "drawnFrameWidth":I
    .end local v8    # "drawnFrameHeight":I
    .end local v22    # "layoutMatrix":[F
    :cond_b
    invoke-static {}, Lorg/webrtc/RendererCommon;->identityMatrix()[F

    move-result-object v22

    goto :goto_5

    .line 580
    :catchall_2
    move-exception v4

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v4

    .line 597
    .restart local v6    # "drawMatrix":[F
    .restart local v7    # "drawnFrameWidth":I
    .restart local v8    # "drawnFrameHeight":I
    .restart local v22    # "layoutMatrix":[F
    .restart local v25    # "shouldUploadYuvTextures":Z
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 607
    .restart local v5    # "yuvTextures":[I
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/webrtc/EglRenderer;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    move-object/from16 v0, v20

    iget v10, v0, Lorg/webrtc/VideoRenderer$I420Frame;->textureId:I

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    .line 608
    invoke-virtual {v4}, Lorg/webrtc/EglBase;->surfaceWidth()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/webrtc/EglRenderer;->eglBase:Lorg/webrtc/EglBase;

    invoke-virtual {v4}, Lorg/webrtc/EglBase;->surfaceHeight()I

    move-result v17

    move-object v11, v6

    move v12, v7

    move v13, v8

    .line 607
    invoke-interface/range {v9 .. v17}, Lorg/webrtc/RendererCommon$GlDrawer;->drawOes(I[FIIIIII)V

    goto/16 :goto_4

    .line 619
    .restart local v18    # "currentTimeNs":J
    .restart local v28    # "swapBuffersStartTimeNs":J
    :catchall_3
    move-exception v4

    :try_start_7
    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v4
.end method

.method private resetStatistics(J)V
    .locals 5
    .param p1, "currentTimeNs"    # J

    .prologue
    .line 281
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 282
    :try_start_0
    iput-wide p1, p0, Lorg/webrtc/EglRenderer;->statisticsStartTimeNs:J

    .line 283
    const/4 v0, 0x0

    iput v0, p0, Lorg/webrtc/EglRenderer;->framesReceived:I

    .line 284
    const/4 v0, 0x0

    iput v0, p0, Lorg/webrtc/EglRenderer;->framesDropped:I

    .line 285
    const/4 v0, 0x0

    iput v0, p0, Lorg/webrtc/EglRenderer;->framesRendered:I

    .line 286
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/webrtc/EglRenderer;->renderTimeNs:J

    .line 287
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/webrtc/EglRenderer;->renderSwapBufferTimeNs:J

    .line 288
    monitor-exit v1

    .line 289
    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public addFrameListener(Lorg/webrtc/EglRenderer$FrameListener;F)V
    .locals 1
    .param p1, "listener"    # Lorg/webrtc/EglRenderer$FrameListener;
    .param p2, "scale"    # F

    .prologue
    .line 368
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/webrtc/EglRenderer;->addFrameListener(Lorg/webrtc/EglRenderer$FrameListener;FLorg/webrtc/RendererCommon$GlDrawer;)V

    .line 369
    return-void
.end method

.method public addFrameListener(Lorg/webrtc/EglRenderer$FrameListener;FLorg/webrtc/RendererCommon$GlDrawer;)V
    .locals 1
    .param p1, "listener"    # Lorg/webrtc/EglRenderer$FrameListener;
    .param p2, "scale"    # F
    .param p3, "drawerParam"    # Lorg/webrtc/RendererCommon$GlDrawer;

    .prologue
    .line 382
    new-instance v0, Lorg/webrtc/EglRenderer$6;

    invoke-direct {v0, p0, p3, p1, p2}, Lorg/webrtc/EglRenderer$6;-><init>(Lorg/webrtc/EglRenderer;Lorg/webrtc/RendererCommon$GlDrawer;Lorg/webrtc/EglRenderer$FrameListener;F)V

    invoke-direct {p0, v0}, Lorg/webrtc/EglRenderer;->postToRenderThread(Ljava/lang/Runnable;)V

    .line 389
    return-void
.end method

.method public clearImage()V
    .locals 3

    .prologue
    .line 497
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 498
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 499
    monitor-exit v1

    .line 508
    :goto_0
    return-void

    .line 501
    :cond_0
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    new-instance v2, Lorg/webrtc/EglRenderer$9;

    invoke-direct {v2, p0}, Lorg/webrtc/EglRenderer$9;-><init>(Lorg/webrtc/EglRenderer;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 507
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public createEglSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lorg/webrtc/EglRenderer;->createEglSurfaceInternal(Ljava/lang/Object;)V

    .line 210
    return-void
.end method

.method public createEglSurface(Landroid/view/Surface;)V
    .locals 0
    .param p1, "surface"    # Landroid/view/Surface;

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lorg/webrtc/EglRenderer;->createEglSurfaceInternal(Ljava/lang/Object;)V

    .line 206
    return-void
.end method

.method public disableFpsReduction()V
    .locals 1

    .prologue
    .line 351
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    invoke-virtual {p0, v0}, Lorg/webrtc/EglRenderer;->setFpsReduction(F)V

    .line 352
    return-void
.end method

.method public init(Lorg/webrtc/EglBase$Context;[ILorg/webrtc/RendererCommon$GlDrawer;)V
    .locals 10
    .param p1, "sharedContext"    # Lorg/webrtc/EglBase$Context;
    .param p2, "configAttributes"    # [I
    .param p3, "drawer"    # Lorg/webrtc/RendererCommon$GlDrawer;

    .prologue
    .line 168
    iget-object v4, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 169
    :try_start_0
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 170
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lorg/webrtc/EglRenderer;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Already initialized"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 201
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 172
    :cond_0
    :try_start_1
    const-string v3, "Initializing EglRenderer"

    invoke-direct {p0, v3}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 173
    iput-object p3, p0, Lorg/webrtc/EglRenderer;->drawer:Lorg/webrtc/RendererCommon$GlDrawer;

    .line 175
    new-instance v2, Landroid/os/HandlerThread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lorg/webrtc/EglRenderer;->name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "EglRenderer"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 176
    .local v2, "renderThread":Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 177
    new-instance v3, Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    .line 181
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    new-instance v5, Lorg/webrtc/EglRenderer$3;

    invoke-direct {v5, p0, p1, p2}, Lorg/webrtc/EglRenderer$3;-><init>(Lorg/webrtc/EglRenderer;Lorg/webrtc/EglBase$Context;[I)V

    invoke-static {v3, v5}, Lorg/webrtc/ThreadUtils;->invokeAtFrontUninterruptibly(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 196
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    iget-object v5, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 197
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 198
    .local v0, "currentTimeNs":J
    invoke-direct {p0, v0, v1}, Lorg/webrtc/EglRenderer;->resetStatistics(J)V

    .line 199
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    iget-object v5, p0, Lorg/webrtc/EglRenderer;->logStatisticsRunnable:Ljava/lang/Runnable;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x4

    .line 200
    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 199
    invoke-virtual {v3, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 201
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 202
    return-void
.end method

.method public pauseVideo()V
    .locals 1

    .prologue
    .line 355
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/webrtc/EglRenderer;->setFpsReduction(F)V

    .line 356
    return-void
.end method

.method public printStackTrace()V
    .locals 7

    .prologue
    .line 292
    iget-object v4, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 293
    :try_start_0
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-nez v3, :cond_0

    const/4 v1, 0x0

    .line 295
    .local v1, "renderThread":Ljava/lang/Thread;
    :goto_0
    if-eqz v1, :cond_1

    .line 296
    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    .line 297
    .local v0, "renderStackTrace":[Ljava/lang/StackTraceElement;
    array-length v3, v0

    if-lez v3, :cond_1

    .line 298
    const-string v3, "EglRenderer stack trace:"

    invoke-direct {p0, v3}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 299
    array-length v5, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v2, v0, v3

    .line 300
    .local v2, "traceElem":Ljava/lang/StackTraceElement;
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 299
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 293
    .end local v0    # "renderStackTrace":[Ljava/lang/StackTraceElement;
    .end local v1    # "renderThread":Ljava/lang/Thread;
    .end local v2    # "traceElem":Ljava/lang/StackTraceElement;
    :cond_0
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    .line 294
    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    goto :goto_0

    .line 304
    .restart local v1    # "renderThread":Ljava/lang/Thread;
    :cond_1
    monitor-exit v4

    .line 305
    return-void

    .line 304
    .end local v1    # "renderThread":Ljava/lang/Thread;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public release()V
    .locals 5

    .prologue
    .line 224
    const-string v2, "Releasing."

    invoke-direct {p0, v2}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 225
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 226
    .local v0, "eglCleanupBarrier":Ljava/util/concurrent/CountDownLatch;
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 227
    :try_start_0
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 228
    const-string v2, "Already released"

    invoke-direct {p0, v2}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 229
    monitor-exit v3

    .line 275
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    iget-object v4, p0, Lorg/webrtc/EglRenderer;->logStatisticsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 233
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    new-instance v4, Lorg/webrtc/EglRenderer$4;

    invoke-direct {v4, p0, v0}, Lorg/webrtc/EglRenderer$4;-><init>(Lorg/webrtc/EglRenderer;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 254
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 256
    .local v1, "renderLooper":Landroid/os/Looper;
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    new-instance v4, Lorg/webrtc/EglRenderer$5;

    invoke-direct {v4, p0, v1}, Lorg/webrtc/EglRenderer$5;-><init>(Lorg/webrtc/EglRenderer;Landroid/os/Looper;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 264
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    .line 265
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-static {v0}, Lorg/webrtc/ThreadUtils;->awaitUninterruptibly(Ljava/util/concurrent/CountDownLatch;)V

    .line 268
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->frameLock:Ljava/lang/Object;

    monitor-enter v3

    .line 269
    :try_start_1
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    if-eqz v2, :cond_1

    .line 270
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    invoke-static {v2}, Lorg/webrtc/VideoRenderer;->renderFrameDone(Lorg/webrtc/VideoRenderer$I420Frame;)V

    .line 271
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    .line 273
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 274
    const-string v2, "Releasing done."

    invoke-direct {p0, v2}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    goto :goto_0

    .line 265
    .end local v1    # "renderLooper":Landroid/os/Looper;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 273
    .restart local v1    # "renderLooper":Landroid/os/Looper;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method public releaseEglSurface(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "completionCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 453
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/webrtc/EglRenderer$EglSurfaceCreation;->setSurface(Ljava/lang/Object;)V

    .line 454
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 455
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    iget-object v2, p0, Lorg/webrtc/EglRenderer;->eglSurfaceCreationRunnable:Lorg/webrtc/EglRenderer$EglSurfaceCreation;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 457
    iget-object v0, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    new-instance v2, Lorg/webrtc/EglRenderer$8;

    invoke-direct {v2, p0, p1}, Lorg/webrtc/EglRenderer$8;-><init>(Lorg/webrtc/EglRenderer;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 467
    monitor-exit v1

    .line 471
    :goto_0
    return-void

    .line 469
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public removeFrameListener(Lorg/webrtc/EglRenderer$FrameListener;)V
    .locals 3
    .param p1, "listener"    # Lorg/webrtc/EglRenderer$FrameListener;

    .prologue
    .line 399
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 400
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "removeFrameListener must not be called on the render thread."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 402
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 403
    .local v0, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v1, Lorg/webrtc/EglRenderer$7;

    invoke-direct {v1, p0, v0, p1}, Lorg/webrtc/EglRenderer$7;-><init>(Lorg/webrtc/EglRenderer;Ljava/util/concurrent/CountDownLatch;Lorg/webrtc/EglRenderer$FrameListener;)V

    invoke-direct {p0, v1}, Lorg/webrtc/EglRenderer;->postToRenderThread(Ljava/lang/Runnable;)V

    .line 415
    invoke-static {v0}, Lorg/webrtc/ThreadUtils;->awaitUninterruptibly(Ljava/util/concurrent/CountDownLatch;)V

    .line 416
    return-void
.end method

.method public renderFrame(Lorg/webrtc/VideoRenderer$I420Frame;)V
    .locals 5
    .param p1, "frame"    # Lorg/webrtc/VideoRenderer$I420Frame;

    .prologue
    .line 421
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 422
    :try_start_0
    iget v1, p0, Lorg/webrtc/EglRenderer;->framesReceived:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/webrtc/EglRenderer;->framesReceived:I

    .line 423
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->handlerLock:Ljava/lang/Object;

    monitor-enter v2

    .line 426
    :try_start_1
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 427
    const-string v1, "Dropping frame - Not initialized or already released."

    invoke-direct {p0, v1}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 428
    invoke-static {p1}, Lorg/webrtc/VideoRenderer;->renderFrameDone(Lorg/webrtc/VideoRenderer$I420Frame;)V

    .line 429
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 423
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 431
    :cond_1
    :try_start_3
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->frameLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 432
    :try_start_4
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    .line 433
    .local v0, "dropOldFrame":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 434
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    invoke-static {v1}, Lorg/webrtc/VideoRenderer;->renderFrameDone(Lorg/webrtc/VideoRenderer$I420Frame;)V

    .line 436
    :cond_2
    iput-object p1, p0, Lorg/webrtc/EglRenderer;->pendingFrame:Lorg/webrtc/VideoRenderer$I420Frame;

    .line 437
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->renderThreadHandler:Landroid/os/Handler;

    iget-object v4, p0, Lorg/webrtc/EglRenderer;->renderFrameRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 438
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 439
    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 440
    if-eqz v0, :cond_0

    .line 441
    iget-object v2, p0, Lorg/webrtc/EglRenderer;->statisticsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 442
    :try_start_6
    iget v1, p0, Lorg/webrtc/EglRenderer;->framesDropped:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/webrtc/EglRenderer;->framesDropped:I

    .line 443
    monitor-exit v2

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1

    .line 432
    .end local v0    # "dropOldFrame":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 438
    :catchall_2
    move-exception v1

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v1

    .line 439
    :catchall_3
    move-exception v1

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v1
.end method

.method public setFpsReduction(F)V
    .locals 6
    .param p1, "fps"    # F

    .prologue
    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFpsReduction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 336
    iget-object v3, p0, Lorg/webrtc/EglRenderer;->fpsReductionLock:Ljava/lang/Object;

    monitor-enter v3

    .line 337
    :try_start_0
    iget-wide v0, p0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    .line 338
    .local v0, "previousRenderPeriodNs":J
    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_1

    .line 339
    const-wide v4, 0x7fffffffffffffffL

    iput-wide v4, p0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    .line 343
    :goto_0
    iget-wide v4, p0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    cmp-long v2, v4, v0

    if-eqz v2, :cond_0

    .line 345
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/webrtc/EglRenderer;->nextFrameTimeNs:J

    .line 347
    :cond_0
    monitor-exit v3

    .line 348
    return-void

    .line 341
    :cond_1
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    long-to-float v2, v4

    div-float/2addr v2, p1

    float-to-long v4, v2

    iput-wide v4, p0, Lorg/webrtc/EglRenderer;->minRenderPeriodNs:J

    goto :goto_0

    .line 347
    .end local v0    # "previousRenderPeriodNs":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setLayoutAspectRatio(F)V
    .locals 2
    .param p1, "layoutAspectRatio"    # F

    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setLayoutAspectRatio: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->layoutLock:Ljava/lang/Object;

    monitor-enter v1

    .line 324
    :try_start_0
    iput p1, p0, Lorg/webrtc/EglRenderer;->layoutAspectRatio:F

    .line 325
    monitor-exit v1

    .line 326
    return-void

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setMirror(Z)V
    .locals 2
    .param p1, "mirror"    # Z

    .prologue
    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setMirror: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/webrtc/EglRenderer;->logD(Ljava/lang/String;)V

    .line 312
    iget-object v1, p0, Lorg/webrtc/EglRenderer;->layoutLock:Ljava/lang/Object;

    monitor-enter v1

    .line 313
    :try_start_0
    iput-boolean p1, p0, Lorg/webrtc/EglRenderer;->mirror:Z

    .line 314
    monitor-exit v1

    .line 315
    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
