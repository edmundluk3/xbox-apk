.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_CreateResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final contentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentUploadUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContentId:Ljava/lang/String;

.field private defaultContentUploadUri:Ljava/lang/String;

.field private defaultPublishUri:Ljava/lang/String;

.field private final publishUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultPublishUri:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentUploadUri:Ljava/lang/String;

    .line 26
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->publishUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentUploadUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 62
    const/4 v4, 0x0

    .line 93
    :goto_0
    return-object v4

    .line 64
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 66
    .local v1, "contentId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultPublishUri:Ljava/lang/String;

    .line 67
    .local v3, "publishUri":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentUploadUri:Ljava/lang/String;

    .line 68
    .local v2, "contentUploadUri":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 74
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 88
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 74
    :sswitch_0
    const-string v5, "contentId"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "publishUri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "contentUploadUri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 76
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "contentId":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 77
    .restart local v1    # "contentId":Ljava/lang/String;
    goto :goto_1

    .line 80
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->publishUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "publishUri":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 81
    .restart local v3    # "publishUri":Ljava/lang/String;
    goto :goto_1

    .line 84
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentUploadUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "contentUploadUri":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 85
    .restart local v2    # "contentUploadUri":Ljava/lang/String;
    goto :goto_1

    .line 92
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 93
    new-instance v4, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse;

    invoke-direct {v4, v1, v3, v2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        -0x37d928ce -> :sswitch_2
        -0x1843fc8c -> :sswitch_0
        0x726e61dd -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultContentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentId"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public setDefaultContentUploadUri(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentUploadUri"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultContentUploadUri:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public setDefaultPublishUri(Ljava/lang/String;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPublishUri"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->defaultPublishUri:Ljava/lang/String;

    .line 36
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    if-nez p2, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 57
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 50
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->contentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 52
    const-string v0, "publishUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->publishUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->publishUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 54
    const-string v0, "contentUploadUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->contentUploadUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;->contentUploadUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CreateResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CreateResponse;)V

    return-void
.end method
