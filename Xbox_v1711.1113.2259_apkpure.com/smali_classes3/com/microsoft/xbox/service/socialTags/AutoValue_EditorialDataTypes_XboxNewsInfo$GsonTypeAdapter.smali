.class public final Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_EditorialDataTypes_XboxNewsInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final bgColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final bgImageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultBgColor:Ljava/lang/String;

.field private defaultBgImage:Ljava/lang/String;

.field private defaultDescription:Ljava/lang/String;

.field private defaultFeaturedURL:Ljava/lang/String;

.field private defaultIconImage:Ljava/lang/String;

.field private defaultProfileName:Ljava/lang/String;

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final featuredURLAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final iconImageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final profileNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgColor:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgImage:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultFeaturedURL:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultIconImage:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultProfileName:Ljava/lang/String;

    .line 32
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgImageAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->featuredURLAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->iconImageAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->profileNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    .locals 9
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_0

    .line 88
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 89
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgColor:Ljava/lang/String;

    .line 93
    .local v1, "bgColor":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgImage:Ljava/lang/String;

    .line 94
    .local v2, "bgImage":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 95
    .local v3, "description":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultFeaturedURL:Ljava/lang/String;

    .line 96
    .local v4, "featuredURL":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultIconImage:Ljava/lang/String;

    .line 97
    .local v5, "iconImage":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultProfileName:Ljava/lang/String;

    .line 98
    .local v6, "profileName":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 100
    .local v7, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_1

    .line 101
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 104
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 130
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 104
    :sswitch_0
    const-string v8, "BgColor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "BgImage"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "Description"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "FeaturedURL"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "IconImage"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "ProfileName"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "bgColor":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 107
    .restart local v1    # "bgColor":Ljava/lang/String;
    goto :goto_1

    .line 110
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "bgImage":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 111
    .restart local v2    # "bgImage":Ljava/lang/String;
    goto :goto_1

    .line 114
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "description":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 115
    .restart local v3    # "description":Ljava/lang/String;
    goto :goto_1

    .line 118
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->featuredURLAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "featuredURL":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 119
    .restart local v4    # "featuredURL":Ljava/lang/String;
    goto/16 :goto_1

    .line 122
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->iconImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "iconImage":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 123
    .restart local v5    # "iconImage":Ljava/lang/String;
    goto/16 :goto_1

    .line 126
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->profileNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "profileName":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 127
    .restart local v6    # "profileName":Ljava/lang/String;
    goto/16 :goto_1

    .line 134
    .end local v7    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 135
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 104
    :sswitch_data_0
    .sparse-switch
        -0x360d424 -> :sswitch_2
        0x23588042 -> :sswitch_4
        0x4cf87641 -> :sswitch_3
        0x5702adfe -> :sswitch_0
        0x575627f6 -> :sswitch_1
        0x6dfe23b4 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultBgColor(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultBgColor"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgColor:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public setDefaultBgImage(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultBgImage"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultBgImage:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public setDefaultDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDescription"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public setDefaultFeaturedURL(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFeaturedURL"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultFeaturedURL:Ljava/lang/String;

    .line 53
    return-object p0
.end method

.method public setDefaultIconImage(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIconImage"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultIconImage:Ljava/lang/String;

    .line 57
    return-object p0
.end method

.method public setDefaultProfileName(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultProfileName"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->defaultProfileName:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    if-nez p2, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 84
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 71
    const-string v0, "BgColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 73
    const-string v0, "BgImage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->bgImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 75
    const-string v0, "Description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 77
    const-string v0, "FeaturedURL"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->featuredURLAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->featuredURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 79
    const-string v0, "IconImage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->iconImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->iconImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 81
    const-string v0, "ProfileName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->profileNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->profileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;)V

    return-void
.end method
