.class Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen$AchievementsAdapterProvider;
.super Ljava/lang/Object;
.source "GameProfileAchievementsScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AchievementsAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen$AchievementsAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen$AchievementsAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 66
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .line 67
    .local v0, "achievementsViewModel":Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameProfileAchievementsAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
