.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleHubData"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final titles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "titles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    if-nez p1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->titles:Ljava/util/List;

    .line 64
    return-void

    .line 63
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 73
    if-ne p1, p0, :cond_0

    .line 74
    const/4 v1, 0x1

    .line 79
    :goto_0
    return v1

    .line 75
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    if-nez v1, :cond_1

    .line 76
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 78
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;

    .line 79
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->titles:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->titles:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getTitles()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->titles:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->hashCode:I

    if-nez v0, :cond_0

    .line 86
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->hashCode:I

    .line 87
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->titles:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->hashCode:I

    .line 90
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHubData;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
