.class public interface abstract Lcom/microsoft/xbox/xle/ui/EPGView$OnTapListener;
.super Ljava/lang/Object;
.source "EPGView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTapListener"
.end annotation


# virtual methods
.method public abstract onChannelTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z
.end method

.method public abstract onProgramTap(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z
.end method
