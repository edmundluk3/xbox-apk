.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;
.super Ljava/lang/Object;
.source "UTCNotifications.java"


# static fields
.field private static final HOST_HAS_CONFIRMED_MEMBER_WITHDRAW:Ljava/lang/String; = "hostHasConfirmedMemberWithdraw"

.field private static final HOST_HAS_NEW_APPLICANT:Ljava/lang/String; = "hostHasNewApplicant"

.field private static final IMMINENT_LFG_APPLICANT_CONFIRMED:Ljava/lang/String; = "imminentLfgApplicantConfirmed"

.field private static final IMMINENT_LFG_FILLED_WITHOUT_APPLICANT:Ljava/lang/String; = "imminentLfgFilledWithoutApplicant"

.field private static final SCHEDULED_LFG_APPLICANT_CONFIRMED:Ljava/lang/String; = "scheduledLfgApplicantConfirmed"

.field private static final SCHEDULED_LFG_CANCELED:Ljava/lang/String; = "scheduledLfgCanceled"

.field private static final SCHEDULED_LFG_EXPIRED:Ljava/lang/String; = "scheduledLfgExpired"

.field private static final SCHEDULED_LFG_FILLED_WITHOUT_APPLICANT:Ljava/lang/String; = "scheduledLfgFilledWithoutApplicant"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;->getTournamentNotificationAction(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getTournamentNotificationAction(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "notifyType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "messageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 197
    .local v0, "pageAction":Ljava/lang/String;
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 199
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const-string v3, "string"

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 201
    .local v1, "resourceId":I
    if-lez v1, :cond_1

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne p0, v2, :cond_1

    .line 202
    packed-switch v1, :pswitch_data_0

    .line 243
    .end local v1    # "resourceId":I
    :cond_0
    :goto_0
    return-object v0

    .line 204
    .restart local v1    # "resourceId":I
    :pswitch_0
    const-string v0, "Push Notification - Tournament Canceled"

    .line 205
    goto :goto_0

    .line 207
    :pswitch_1
    const-string v0, "Push Notification - Tournament Completed"

    .line 208
    goto :goto_0

    .line 210
    :pswitch_2
    const-string v0, "Push Notification - Tournament Started"

    .line 211
    goto :goto_0

    .line 213
    :pswitch_3
    const-string v0, "Push Notification - Tournament Check In"

    goto :goto_0

    .line 216
    :cond_1
    if-lez v1, :cond_0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne p0, v2, :cond_0

    .line 217
    packed-switch v1, :pswitch_data_1

    :pswitch_4
    goto :goto_0

    .line 231
    :pswitch_5
    const-string v0, "Push Notification - Tournament Team Check In"

    .line 232
    goto :goto_0

    .line 219
    :pswitch_6
    const-string v0, "Push Notification - Tournament Team Registered"

    .line 220
    goto :goto_0

    .line 222
    :pswitch_7
    const-string v0, "Push Notification - Tournament Team Waitlisted"

    .line 223
    goto :goto_0

    .line 225
    :pswitch_8
    const-string v0, "Push Notification - Tournament Team Standby"

    .line 226
    goto :goto_0

    .line 228
    :pswitch_9
    const-string v0, "Push Notification - Tournament Team Playing"

    .line 229
    goto :goto_0

    .line 234
    :pswitch_a
    const-string v0, "Push Notification - Tournament Team Eliminated"

    .line 235
    goto :goto_0

    .line 237
    :pswitch_b
    const-string v0, "Push Notification - Tournament Team Unregistered"

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x7f070dc3
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 217
    :pswitch_data_1
    .packed-switch 0x7f070dc7
        :pswitch_5
        :pswitch_a
        :pswitch_4
        :pswitch_9
        :pswitch_6
        :pswitch_8
        :pswitch_b
        :pswitch_7
    .end packed-switch
.end method

.method public static trackPushNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/content/Intent;)V
    .locals 1
    .param p0, "notifyType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;-><init>(Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 184
    return-void
.end method
