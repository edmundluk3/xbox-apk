.class public final Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes;
.super Ljava/lang/Object;
.source "CommentsServiceDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;,
        Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;,
        Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;,
        Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsBatchRequest;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
