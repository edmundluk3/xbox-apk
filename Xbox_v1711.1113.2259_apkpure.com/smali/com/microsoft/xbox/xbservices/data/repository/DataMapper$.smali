.class public Lcom/microsoft/xbox/xbservices/data/repository/DataMapper$;
.super Ljava/lang/Object;
.source "DataMapper.java"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SOURCE_TYPE:",
        "Ljava/lang/Object;",
        "TARGET_TYPE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function",
        "<TSOURCE_TYPE;",
        "Lio/reactivex/Observable",
        "<TTARGET_TYPE;>;>;"
    }
.end annotation

.annotation runtime Ljava/lang/FunctionalInterface;
.end annotation


# direct methods
.method public static bridge synthetic apply(Lcom/microsoft/xbox/xbservices/data/repository/DataMapper;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 8
    .local p0, "this":Lcom/microsoft/xbox/xbservices/data/repository/DataMapper;, "Lcom/microsoft/xbox/xbservices/data/repository/DataMapper<TSOURCE_TYPE;TTARGET_TYPE;>;"
    invoke-interface {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/DataMapper;->apply(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
