.class public Lcom/microsoft/xbox/service/model/SocialTagModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "SocialTagModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;


# instance fields
.field editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

.field private final systemTagGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final tagMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 39
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/model/SocialTagModel;)V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->tagMap:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->systemTagGroups:Ljava/util/List;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/SocialTagModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SocialTagModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->onGetSystemTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGetSystemTagsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;>;"
    const/4 v3, 0x1

    .line 96
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 99
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->processSystemTagsResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 100
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 101
    return-void
.end method

.method private declared-synchronized processSystemTagsResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v3, v4, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 107
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;

    .line 109
    .local v0, "systemTags":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->tagMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->systemTagGroups:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 112
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;->systemTagGroups()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    .line 113
    .local v2, "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->systemTagGroups:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    .line 115
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->id()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 122
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->tagMap:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->id()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 104
    .end local v0    # "systemTags":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v2    # "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 127
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public getSystemTagGroups()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->systemTagGroups:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSystemTags()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->tagMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->tagMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    return-object v0
.end method

.method public getTags(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 61
    .local v2, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v1

    .line 63
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v1, :cond_0

    .line 64
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    :cond_1
    return-object v2
.end method

.method public loadSystemTagsAsync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->isLoading:Z

    if-nez v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;-><init>(Lcom/microsoft/xbox/service/model/SocialTagModel;Lcom/microsoft/xbox/service/model/SocialTagModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    .line 82
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 84
    :cond_1
    return-void
.end method

.method public loadSystemTagsSync(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 87
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    if-nez v1, :cond_0

    .line 88
    new-instance v1, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    invoke-direct {v1, p0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;-><init>(Lcom/microsoft/xbox/service/model/SocialTagModel;Lcom/microsoft/xbox/service/model/SocialTagModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel;->runner:Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    invoke-virtual {p0, p1, v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 92
    .local v0, "tagsResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;>;"
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->processSystemTagsResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 93
    return-void
.end method
