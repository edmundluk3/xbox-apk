.class Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ChangeGamertagDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GamertagSuggestionsListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I

    .prologue
    .line 234
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .line 235
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 236
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 240
    if-nez p2, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 242
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f030052

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 245
    .end local v1    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0e0258

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 247
    .local v0, "text":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$GamertagSuggestionsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 249
    return-object p2
.end method
