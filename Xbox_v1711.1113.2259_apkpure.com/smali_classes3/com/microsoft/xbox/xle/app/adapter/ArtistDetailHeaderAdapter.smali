.class public Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ArtistDetailHeaderAdapter.java"


# instance fields
.field private artistTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private artistTitleSmallTextView:Landroid/widget/TextView;

.field private artistTitleView:Landroid/widget/TextView;

.field private extraTextView:Landroid/widget/TextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    .line 22
    const v0, 0x7f0e01d4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->screenBody:Landroid/view/View;

    .line 23
    const v0, 0x7f0e01d5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 24
    const v0, 0x7f0e01d6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTitleView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0e01d8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->extraTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0e01d9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTitleSmallTextView:Landroid/widget/TextView;

    .line 27
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 3

    .prologue
    const v2, 0x7f020189

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTitleSmallTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->extraTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->artistTileView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 41
    :cond_1
    return-void
.end method
