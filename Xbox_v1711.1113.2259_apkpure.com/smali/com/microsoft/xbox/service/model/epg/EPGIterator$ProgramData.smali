.class public Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
.super Ljava/lang/Object;
.source "EPGIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProgramData"
.end annotation


# static fields
.field private static randomGenerator:Ljava/util/Random;

.field private static showIndex:I

.field private static showList:[Ljava/lang/String;


# instance fields
.field private contentType:Ljava/lang/String;

.field private duration:I

.field private id:Ljava/lang/String;

.field private isEmpty:Z

.field private isRepeat:Z

.field private nativePointer:I

.field private showDescription:Ljava/lang/String;

.field private showGenres:Ljava/lang/String;

.field private showGuid:Ljava/lang/String;

.field private showImageUrl:Ljava/lang/String;

.field private showParentSeriesTitle:Ljava/lang/String;

.field private showParentalRating:Ljava/lang/String;

.field private showParentalRatingSystem:Ljava/lang/String;

.field private showTitle:Ljava/lang/String;

.field private startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 140
    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showList:[Ljava/lang/String;

    .line 142
    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->randomGenerator:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    .line 16
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->duration:I

    return v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->duration:I

    return p1
.end method

.method public static getEmptyProgramData()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 3

    .prologue
    .line 129
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;-><init>()V

    .line 130
    .local v0, "program":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->isEmpty:Z

    .line 131
    const-string v1, ""

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showGuid:Ljava/lang/String;

    .line 132
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showTitle:Ljava/lang/String;

    .line 135
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->id:Ljava/lang/String;

    .line 136
    return-object v0
.end method

.method public static getMockProgramData(I)Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 1
    .param p0, "offset"    # I

    .prologue
    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method private native nativePopulateAll(I)V
.end method

.method private native nativeRelease(I)V
.end method

.method private populateAllIfNeeded()V
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    if-eqz v0, :cond_0

    .line 90
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePopulateAll(I)V

    .line 91
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativeRelease(I)V

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 81
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    if-eqz v0, :cond_0

    .line 82
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativeRelease(I)V

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->nativePointer:I

    .line 85
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 86
    return-void
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->duration:I

    return v0
.end method

.method public final getEndTime()I
    .locals 2

    .prologue
    .line 26
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->startTime:I

    iget v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->duration:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowGenres()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showGenres:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showGuid:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowParentSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showParentSeriesTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showParentalRating:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowParentalRatingSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->populateAllIfNeeded()V

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showParentalRatingSystem:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->showTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getStartTime()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->startTime:I

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->isEmpty:Z

    return v0
.end method

.method public final isRepeat()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->isRepeat:Z

    return v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->id:Ljava/lang/String;

    .line 120
    return-void
.end method
