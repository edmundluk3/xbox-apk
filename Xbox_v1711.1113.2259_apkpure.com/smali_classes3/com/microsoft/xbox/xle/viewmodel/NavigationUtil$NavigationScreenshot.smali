.class public final enum Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;
.super Ljava/lang/Enum;
.source "NavigationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NavigationScreenshot"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

.field public static final enum ITEM_CONTENT:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

.field public static final enum ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 417
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    const-string v1, "ITEM_DETAIL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    const-string v1, "ITEM_CONTENT"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_CONTENT:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    .line 416
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->ITEM_CONTENT:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 416
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 416
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationScreenshot;

    return-object v0
.end method
