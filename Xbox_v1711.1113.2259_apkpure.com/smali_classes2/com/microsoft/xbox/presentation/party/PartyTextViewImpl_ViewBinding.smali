.class public Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_ViewBinding;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.source "PartyTextViewImpl_ViewBinding.java"


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Landroid/view/View;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;

    .line 30
    const v0, 0x7f0e086b

    const-string v1, "field \'statusText\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->statusText:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f0e086c

    const-string v1, "field \'memberList\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberList:Landroid/support/v7/widget/RecyclerView;

    .line 32
    const v0, 0x7f0e086d

    const-string v1, "field \'messageList\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageList:Landroid/support/v7/widget/RecyclerView;

    .line 33
    const v0, 0x7f0e086f

    const-string v1, "field \'messageInput\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageInput:Landroid/widget/EditText;

    .line 34
    const v0, 0x7f0e0870

    const-string v1, "field \'sendButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->sendButton:Landroid/widget/Button;

    .line 35
    const v0, 0x7f0e0871

    const-string v1, "field \'characterCountText\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->characterCountText:Landroid/widget/TextView;

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;

    .line 41
    .local v0, "target":Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->statusText:Landroid/widget/TextView;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberList:Landroid/support/v7/widget/RecyclerView;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageList:Landroid/support/v7/widget/RecyclerView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageInput:Landroid/widget/EditText;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->sendButton:Landroid/widget/Button;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->characterCountText:Landroid/widget/TextView;

    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->unbind()V

    .line 52
    return-void
.end method
