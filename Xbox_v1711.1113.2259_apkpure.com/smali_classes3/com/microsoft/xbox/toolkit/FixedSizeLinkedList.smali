.class public Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;
.super Ljava/lang/Object;
.source "FixedSizeLinkedList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private data:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;, "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList<TT;>;"
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->size:I

    .line 7
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    .line 10
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 11
    iput p1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->size:I

    .line 12
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;, "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 40
    return-void
.end method

.method public pop()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;, "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    .line 28
    .local v0, "rv":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->size:I

    if-gt v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 30
    return-object v0

    .line 28
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public push(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;, "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList<TT;>;"
    .local p1, "val":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 18
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->size:I

    if-le v0, v1, :cond_0

    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->size:I

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 23
    return-void

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;, "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList<TT;>;"
    .local p1, "name":[Ljava/lang/Object;, "[TT;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;->data:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method
