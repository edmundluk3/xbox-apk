.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
.source "$AutoValue_ClubHubDataTypes_ClubRoleItem.java"


# instance fields
.field private final actorXuid:J

.field private final createdDate:Ljava/lang/String;

.field private final role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;JLjava/lang/String;)V
    .locals 2
    .param p1, "role"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .param p2, "actorXuid"    # J
    .param p4, "createdDate"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null role"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 22
    iput-wide p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    .line 23
    if-nez p4, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null createdDate"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->createdDate:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public actorXuid()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    return-wide v0
.end method

.method public createdDate()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->createdDate:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    .line 61
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    .line 62
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->actorXuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->createdDate:Ljava/lang/String;

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    :cond_3
    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 70
    const/4 v0, 0x1

    .line 71
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 73
    mul-int/2addr v0, v8

    .line 74
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 75
    mul-int/2addr v0, v8

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->createdDate:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 77
    return v0
.end method

.method public role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubRoleItem{role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->role:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actorXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->actorXuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", createdDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubRoleItem;->createdDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
