.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;
.super Ljava/lang/Object;
.source "UTCSettings.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$enabled:Z

.field final synthetic val$settingName:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;


# direct methods
.method constructor <init>(ZLcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;->val$enabled:Z

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;->val$settingName:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 43
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Enabled"

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;->val$enabled:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;->val$settingName:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 46
    return-void
.end method
