.class final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
.source "$AutoValue_MultiplayerDataTypes_MultiplayerHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

.field private id:Ljava/lang/String;

.field private invitedXuid:Ljava/lang/String;

.field private relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

.field private roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

.field private searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

.field private sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

.field private type:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;-><init>()V

    .line 155
    return-void
.end method


# virtual methods
.method public activityInfo(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "activityInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    .line 177
    return-object p0
.end method

.method public build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
    .locals 10

    .prologue
    .line 201
    const-string v9, ""

    .line 202
    .local v9, "missing":Ljava/lang/String;
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->type:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 205
    :cond_0
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing required properties:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerHandle;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    iget-object v8, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->invitedXuid:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_MultiplayerHandle;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 158
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->id:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public invitedXuid(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "invitedXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 196
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->invitedXuid:Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method public relatedInfo(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "relatedInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    .line 187
    return-object p0
.end method

.method public roleInfo(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "roleInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 191
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    .line 192
    return-object p0
.end method

.method public searchAttributes(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "searchAttributes"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 181
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    .line 182
    return-object p0
.end method

.method public sessionRef(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 0
    .param p1, "sessionRef"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    .line 172
    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle$Builder;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 163
    if-nez p1, :cond_0

    .line 164
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;->type:Ljava/lang/String;

    .line 167
    return-object p0
.end method
