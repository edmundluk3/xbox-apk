.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubBackgroundChangeViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
    }
.end annotation


# static fields
.field private static final CUSTOM_PIC_HEIGHT:I = 0x438

.field private static final CUSTOM_PIC_WIDTH:I = 0x780

.field private static final NUM_SCREENSHOTS:I = 0x1e

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final achievementsItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation
.end field

.field private final clubId:J

.field private isLoadingScreenshots:Z

.field private final loadAchievementsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;

.field private loadScreenshotsCall:Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/Call",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final removeBackgroundEnabled:Z

.field private final screenshotMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;>;"
        }
    .end annotation
.end field

.field private final selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 6
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 74
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getBackgroundImageAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 75
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    .line 76
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->achievementsItemList:Ljava/util/List;

    .line 77
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadAchievementsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forUser(Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v2

    .line 80
    .local v2, "screenshotsRequest":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadScreenshotsCall:Lretrofit2/Call;

    .line 82
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 83
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    instance-of v3, v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 84
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    .line 85
    .local v0, "clubBackgroundChangeScreenParameters":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->access$100(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->clubId:J

    .line 86
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->removeBackgroundEnabled:Z

    .line 87
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->access$300(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 91
    return-void

    .line 89
    .end local v0    # "clubBackgroundChangeScreenParameters":Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "ClubBackgroundChangeViewModel no selection completed action"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onScreenshotsLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onAchievementsLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onUploadPicSuccess(Landroid/net/Uri;)V

    return-void
.end method

.method private onAchievementsLoaded(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    if-eqz p1, :cond_1

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->achievementsItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->achievementsItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 241
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 244
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->updateAdapter()V

    .line 245
    return-void

    .line 239
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method private onScreenshotsLoaded(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->isLoadingScreenshots:Z

    .line 217
    if-eqz p1, :cond_1

    .line 218
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 219
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .line 220
    .local v0, "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    .end local v0    # "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 228
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->isBusy()Z

    move-result v1

    if-nez v1, :cond_3

    .line 229
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 231
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->updateAdapter()V

    .line 232
    return-void
.end method

.method private onUploadPicSuccess(Landroid/net/Uri;)V
    .locals 1
    .param p1, "customPic"    # Landroid/net/Uri;

    .prologue
    .line 157
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onImageSelected(Ljava/lang/String;)V

    .line 158
    return-void
.end method


# virtual methods
.method public browseAllAchievements()V
    .locals 5

    .prologue
    .line 108
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 109
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 110
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 112
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;

    const/4 v4, 0x1

    invoke-static {v2, v3, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    const v2, 0x7f070b6d

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->showError(I)V

    goto :goto_0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 206
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->goBack()V

    .line 207
    return-void
.end method

.method public getAchievements()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->achievementsItemList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getScreenshotsByTitle()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->screenshotMap:Ljava/util/Map;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadAchievementsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->isLoadingScreenshots:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 180
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadAchievementsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->load(Z)V

    .line 183
    if-eqz p1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadScreenshotsCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->clone()Lretrofit2/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadScreenshotsCall:Lretrofit2/Call;

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadScreenshotsCall:Lretrofit2/Call;

    invoke-interface {v0}, Lretrofit2/Call;->isExecuted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->isLoadingScreenshots:Z

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->loadScreenshotsCall:Lretrofit2/Call;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 203
    :cond_1
    return-void
.end method

.method public navigateToUploadPicScreen()V
    .locals 8

    .prologue
    .line 127
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->getPrivResult()Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;

    move-result-object v0

    .line 129
    .local v0, "customPicPrivResult":Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$uploadCustomPic$CustomPicPrivCheck$PrivResult:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck$PrivResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected priv result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 131
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackUseCustomBackground()V

    .line 132
    const-class v1, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->clubId:J

    sget-object v3, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;->ClubBackground:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    const/16 v6, 0x780

    .line 138
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/16 v7, 0x438

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v7

    .line 135
    invoke-static {v4, v5, v3, v6, v7}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->getRectangleInstance(JLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;Landroid/util/Pair;Lcom/microsoft/xbox/toolkit/generics/Action;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    move-result-object v3

    .line 132
    invoke-virtual {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 143
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showPrivFailureDialog()V

    goto :goto_0

    .line 147
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/CustomPicPrivCheck;->showChildFailureDialog()V

    goto :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onImageSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "selectedImage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 211
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->goBack()V

    .line 212
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getBackgroundImageAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 167
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public removeBackground()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onImageSelected(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public removeBackgroundEnabled()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->removeBackgroundEnabled:Z

    return v0
.end method
