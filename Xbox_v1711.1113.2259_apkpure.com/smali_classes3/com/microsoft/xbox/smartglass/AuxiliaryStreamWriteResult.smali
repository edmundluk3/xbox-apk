.class public Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;
.super Ljava/lang/Object;
.source "AuxiliaryStreamWriteResult.java"


# instance fields
.field public final result:Z

.field public final writeStats:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;


# direct methods
.method constructor <init>(ZJJ)V
    .locals 2
    .param p1, "result"    # Z
    .param p2, "sentBytes"    # J
    .param p4, "queuedBytes"    # J

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p1, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;->result:Z

    .line 17
    new-instance v0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;-><init>(JJ)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteResult;->writeStats:Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;

    .line 18
    return-void
.end method
