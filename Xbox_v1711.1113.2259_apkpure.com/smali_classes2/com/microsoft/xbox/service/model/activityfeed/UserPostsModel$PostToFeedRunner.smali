.class Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "UserPostsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostToFeedRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubId:J

.field private final linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

.field private final message:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;JLcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V
    .locals 1
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "clubId"    # J
    .param p5, "previewData"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 85
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->message:Ljava/lang/String;

    .line 86
    iput-wide p3, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->clubId:J

    .line 87
    if-nez p5, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    .line 88
    return-void

    .line 87
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    invoke-direct {v0, p5}, Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;-><init>(Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V

    goto :goto_0
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->clubId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->message:Ljava/lang/String;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->clubId:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->postToClub(Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/WebLinkPostData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->message:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->linkPostData:Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->postNonClub(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->buildData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 112
    const-wide/16 v0, 0x232a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->StatusPosted:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;->this$0:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 108
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
