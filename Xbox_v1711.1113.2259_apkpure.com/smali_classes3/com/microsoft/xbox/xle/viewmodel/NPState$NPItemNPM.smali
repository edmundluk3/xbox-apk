.class public Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;
.super Ljava/lang/Object;
.source "NPState.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NPState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NPItemNPM"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

.field private final itemId:Ljava/lang/String;

.field private final loading:Z

.field private final npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

.field private final providerTitleId:J

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 6
    .param p1, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 374
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 375
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    .line 376
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->title:Ljava/lang/String;

    .line 377
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getItemId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->itemId:Ljava/lang/String;

    .line 378
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getIsLoading()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->loading:Z

    .line 381
    new-instance v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/recents/RecentItem;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    .line 382
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getAppMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 383
    .local v0, "ami":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    .line 384
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    .line 385
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    .line 386
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getBoxArtUri()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 387
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Provider:Ljava/lang/String;

    .line 390
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 458
    if-ne p1, p0, :cond_1

    .line 464
    :cond_0
    :goto_0
    return v1

    .line 460
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;

    if-nez v3, :cond_2

    move v1, v2

    .line 461
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 463
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;

    .line 464
    .local v0, "that":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->title:Ljava/lang/String;

    .line 465
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->itemId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->itemId:Ljava/lang/String;

    .line 466
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->loading:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->loading:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 406
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public getLi()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    return-object v0
.end method

.method public getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    return-object v0
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 400
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    return-wide v0
.end method

.method public getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 445
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    if-nez v0, :cond_0

    .line 446
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    .line 447
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->providerTitleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    .line 448
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    .line 449
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->itemId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    .line 450
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->loading:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    .line 453
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->hashCode:I

    return v0
.end method

.method public isNPM()Z
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method public isRecent()Z
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ContentType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toRecent()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 438
    new-instance v0, Lcom/microsoft/xbox/service/model/recents/Recent;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/recents/Recent;-><init>()V

    .line 439
    .local v0, "r":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemNPM;->item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    .line 440
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;-><init>(Lcom/microsoft/xbox/service/model/recents/Recent;)V

    return-object v1
.end method
