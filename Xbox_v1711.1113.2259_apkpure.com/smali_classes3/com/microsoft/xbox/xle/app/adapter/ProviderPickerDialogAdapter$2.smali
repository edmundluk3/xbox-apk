.class Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$2;
.super Ljava/lang/Object;
.source "ProviderPickerDialogAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 152
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-eqz v0, :cond_0

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->providerTapped(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    .line 155
    :cond_0
    return-void
.end method
