.class public final Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;
.super Ljava/lang/Object;
.source "OOBEPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final authStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final interactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private final navigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final oOBEPresenterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final profileProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/MyProfileProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/MyProfileProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "oOBEPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;>;"
    .local p2, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;>;"
    .local p3, "navigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;>;"
    .local p4, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;>;"
    .local p5, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p6, "profileProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/MyProfileProvider;>;"
    .local p7, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->oOBEPresenterMembersInjector:Ldagger/MembersInjector;

    .line 43
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 45
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 47
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 49
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 51
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->profileProvider:Ljavax/inject/Provider;

    .line 53
    sget-boolean v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_6
    iput-object p7, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 55
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/MyProfileProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    .local p0, "oOBEPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;>;"
    .local p1, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;>;"
    .local p2, "navigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;>;"
    .local p3, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;>;"
    .local p4, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p5, "profileProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/MyProfileProvider;>;"
    .local p6, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;
    .locals 8

    .prologue
    .line 59
    iget-object v7, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->oOBEPresenterMembersInjector:Ldagger/MembersInjector;

    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 62
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 63
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 64
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 65
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->profileProvider:Ljavax/inject/Provider;

    .line 66
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 67
    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    .line 59
    invoke-static {v7, v0}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->get()Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    move-result-object v0

    return-object v0
.end method
