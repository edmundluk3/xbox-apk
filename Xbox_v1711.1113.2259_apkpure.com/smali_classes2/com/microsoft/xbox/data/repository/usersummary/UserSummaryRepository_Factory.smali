.class public final Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;
.super Ljava/lang/Object;
.source "UserSummaryRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final peopleHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final userSummaryRepositoryMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "userSummaryRepositoryMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p2, "peopleHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;>;"
    .local p3, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;>;"
    .local p4, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->userSummaryRepositoryMembersInjector:Ldagger/MembersInjector;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->peopleHubServiceProvider:Ljavax/inject/Provider;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 37
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "userSummaryRepositoryMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;>;"
    .local p1, "peopleHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;>;"
    .local p2, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;>;"
    .local p3, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .locals 5

    .prologue
    .line 41
    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->userSummaryRepositoryMembersInjector:Ldagger/MembersInjector;

    new-instance v4, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->peopleHubServiceProvider:Ljavax/inject/Provider;

    .line 44
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->dataMapperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct {v4, v0, v1, v2}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;-><init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 41
    invoke-static {v3, v4}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository_Factory;->get()Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    move-result-object v0

    return-object v0
.end method
