.class public Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "SettingsNotificationsPageViewModel.java"


# static fields
.field private static final GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

.field private static final PLAY_SERVICES_RESOLUTION_REQUEST:I = 0x63

.field private static final TAG:Ljava/lang/String;


# instance fields
.field chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation
.end field

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->TAG:Ljava/lang/String;

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 36
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)Lcom/microsoft/xbox/toolkit/XLEObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)V

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsNotificationsPageAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 48
    return-void
.end method

.method private varargs alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 9
    .param p1, "enabled"    # Z
    .param p2, "notifTypes"    # [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 268
    sget-object v6, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v0

    .line 269
    .local v0, "flags":I
    move v1, v0

    .line 270
    .local v1, "newFlags":I
    const/4 v3, 0x0

    .line 272
    .local v3, "skypeNotification":Z
    array-length v7, p2

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_2

    aget-object v2, p2, v6

    .line 273
    .local v2, "notifType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    sget-object v8, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne v2, v8, :cond_0

    .line 274
    const/4 v3, 0x1

    .line 277
    :cond_0
    if-eqz p1, :cond_1

    .line 278
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v8

    or-int/2addr v1, v8

    .line 272
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 280
    :cond_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v8

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v1, v8

    goto :goto_1

    .line 284
    .end local v2    # "notifType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_2
    if-eq v0, v1, :cond_3

    .line 285
    if-eqz v3, :cond_5

    .line 286
    const-string v6, "Only Skype notifications OR regular notifications can be modified at once"

    array-length v7, p2

    if-ne v7, v4, :cond_4

    :goto_2
    invoke-static {v6, v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 287
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setMessagingEnabledInternal(Z)V

    .line 292
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v4, v5

    .line 286
    goto :goto_2

    .line 289
    :cond_5
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->setNotificationsFlags(I)V

    goto :goto_3
.end method

.method private isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z
    .locals 2
    .param p1, "notifType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 263
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v0

    .line 264
    .local v0, "flags":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;
    .param p1, "asyncResult"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 38
    const v0, 0x7f0705cb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->showError(I)V

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->updateAdapter()V

    .line 42
    return-void
.end method

.method private onPlayServiceUnavailable(I)V
    .locals 2
    .param p1, "serviceCode"    # I

    .prologue
    .line 318
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceErrorRecoverable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/16 v1, 0x63

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->launchServiceErrorRecovery(II)V

    .line 326
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->updateAdapter()V

    .line 327
    return-void

    .line 321
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Device does not support push notifications"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setMessagingEnabledInternal(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 305
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getServiceCode()I

    move-result v0

    .line 306
    .local v0, "serviceCode":I
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceAvailable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    if-eqz p1, :cond_0

    .line 308
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerPushNotificationWithWithSkype()V

    .line 315
    :goto_0
    return-void

    .line 310
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterPushNotificationWithWithSkype()V

    goto :goto_0

    .line 313
    :cond_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->onPlayServiceUnavailable(I)V

    goto :goto_0
.end method

.method private setNotificationsFlags(I)V
    .locals 2
    .param p1, "flags"    # I

    .prologue
    .line 295
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getServiceCode()I

    move-result v0

    .line 296
    .local v0, "serviceCode":I
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceAvailable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->setUserFlags(I)V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->onPlayServiceUnavailable(I)V

    goto :goto_0
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public isGlobalHoverChatEnabled()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledGlobally()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsMessageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMessageHoverChatEnabled()Z
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->genericInstance()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->isEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isNotificationsMessageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotificationPartyInviteVisible()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v0

    return v0
.end method

.method public isNotificationTournamentMatchEnabled()Z
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationTournamentStatusEnabled()Z
    .locals 1

    .prologue
    .line 253
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 254
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 253
    :goto_0
    return v0

    .line 254
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotificationTournamentTeamEnabled()Z
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_JOIN:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsAchievementsEnabled()Z
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubDemotionEnabled()Z
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubInviteRequestEnabled()Z
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubInvitedEnabled()Z
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubJoinedEnabled()Z
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubNewMemberEnabled()Z
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubPromotionEnabled()Z
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubRecommendationEnabled()Z
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubReportEnabled()Z
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsClubTransferEnabled()Z
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsFavoriteBroadcastEnabled()Z
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsFavoriteOnlineEnabled()Z
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsLfgEnabled()Z
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsMessageEnabled()Z
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isNotificationsPartyInviteEnabled()Z
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isPushNotificationsBusy()Z
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isBusy()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 72
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getSettingsNotificationsPageAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 58
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 53
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->GCM_MODEL:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 63
    return-void
.end method

.method public setGlobalHoverChat(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->setEnabledGlobally(Z)V

    .line 84
    return-void
.end method

.method public setMessageHoverChatEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->genericInstance()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->setEnabledFromNotificationForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Z)V

    .line 232
    return-void
.end method

.method public setNotificationsAchievementsEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 132
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 133
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 134
    return-void
.end method

.method public setNotificationsClubDemotionEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 159
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubDemotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 160
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 161
    return-void
.end method

.method public setNotificationsClubInviteRequestEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 168
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInviteRequest:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 169
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 170
    return-void
.end method

.method public setNotificationsClubInvitedEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 195
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubInvited:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 196
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 197
    return-void
.end method

.method public setNotificationsClubJoinedEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 213
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubJoined:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 214
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 215
    return-void
.end method

.method public setNotificationsClubNewMemberEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 141
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubNewMember:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 142
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 143
    return-void
.end method

.method public setNotificationsClubPromotionEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 150
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubPromotion:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 151
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 152
    return-void
.end method

.method public setNotificationsClubRecommendationEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 177
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubRecommendation:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 178
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 179
    return-void
.end method

.method public setNotificationsClubReportEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 186
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubReport:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 187
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 188
    return-void
.end method

.method public setNotificationsClubTransferEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 204
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->ClubTransfer:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 205
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 206
    return-void
.end method

.method public setNotificationsFavoriteBroadcastEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 100
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 101
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 102
    return-void
.end method

.method public setNotificationsFavoriteOnlineEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 91
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->FavoriteOnline:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 93
    return-void
.end method

.method public setNotificationsLfgEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 123
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->LFGEnabled:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 124
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 125
    return-void
.end method

.method public setNotificationsMessageEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 222
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->Message:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 223
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 224
    return-void
.end method

.method public setNotificationsPartyInviteEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 115
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 116
    return-void
.end method

.method public setNotificationsTournamentMatchEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 243
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentMatchReady:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 244
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 245
    return-void
.end method

.method public setNotificationsTournamentStatusEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 258
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentStatus:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 259
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 260
    return-void
.end method

.method public setNotificationsTournamentTeamEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 248
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;->TournamentTeam:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;->trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V

    .line 249
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_JOIN:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;->alterFlags(Z[Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 250
    return-void
.end method
