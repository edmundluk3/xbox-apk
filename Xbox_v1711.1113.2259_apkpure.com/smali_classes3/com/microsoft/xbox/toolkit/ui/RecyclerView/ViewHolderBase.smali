.class public abstract Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ViewHolderBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "itemView"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 14
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<TT;>;"
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 16
    return-void
.end method


# virtual methods
.method public abstract onBind(Ljava/lang/Object;)V
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
