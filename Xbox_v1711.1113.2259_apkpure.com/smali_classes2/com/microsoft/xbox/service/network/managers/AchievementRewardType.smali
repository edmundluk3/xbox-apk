.class public final enum Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;
.super Ljava/lang/Enum;
.source "AchievementRewardType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

.field public static final enum Art:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

.field public static final enum Gamerscore:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

.field public static final enum InApp:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Unknown:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    const-string v1, "Gamerscore"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Gamerscore:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    const-string v1, "InApp"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->InApp:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    const-string v1, "Art"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Art:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Unknown:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Gamerscore:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->InApp:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Art:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    return-object v0
.end method
