.class Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;
.super Ljava/lang/Object;
.source "ScriptRunner.java"

# interfaces
.implements Lcom/microsoft/xbox/smartglass/controls/HandlesIme;


# instance fields
.field private final SCRIPT_QUEUE_MEMORY_THRESHOLD:I

.field private final SCRIPT_QUEUE_THRESHOLD:I

.field private final SCRIPT_QUEUE_TIMEOUT:I

.field private _keyboardVisible:Z

.field private _memoryUsed:I

.field private _oldestScriptInsertionTime:J

.field private _scripts:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private _timer:Ljava/util/Timer;

.field private _webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;)V
    .locals 6
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    const-wide/16 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/16 v0, 0x20

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->SCRIPT_QUEUE_THRESHOLD:I

    .line 20
    const/16 v0, 0x7530

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->SCRIPT_QUEUE_TIMEOUT:I

    .line 23
    const/high16 v0, 0x1000000

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->SCRIPT_QUEUE_MEMORY_THRESHOLD:I

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_webView:Landroid/webkit/WebView;

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_memoryUsed:I

    .line 36
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_oldestScriptInsertionTime:J

    .line 37
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_timer:Ljava/util/Timer;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_timer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_oldestScriptInsertionTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_keyboardVisible:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 15
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_memoryUsed:I

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->runAllScripts()V

    return-void
.end method

.method private runAllScripts()V
    .locals 6

    .prologue
    .line 82
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    monitor-enter v2

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 84
    .local v0, "script":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 89
    .end local v0    # "script":Ljava/lang/Runnable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 86
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 87
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_oldestScriptInsertionTime:J

    .line 88
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_memoryUsed:I

    .line 89
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    return-void
.end method


# virtual methods
.method public onKeyboardDismissed()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_keyboardVisible:Z

    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->runAllScripts()V

    .line 98
    return-void
.end method

.method public onKeyboardShown()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_keyboardVisible:Z

    .line 103
    return-void
.end method

.method public run(Lcom/microsoft/xbox/smartglass/controls/RunnableScript;)V
    .locals 6
    .param p1, "script"    # Lcom/microsoft/xbox/smartglass/controls/RunnableScript;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_keyboardVisible:Z

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_webView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    .line 70
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 64
    iget v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_memoryUsed:I

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;->getScript()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_memoryUsed:I

    .line 65
    iget-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_oldestScriptInsertionTime:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 66
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_oldestScriptInsertionTime:J

    .line 68
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 74
    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_scripts:Ljava/util/Queue;

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 76
    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_timer:Ljava/util/Timer;

    .line 78
    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->_webView:Landroid/webkit/WebView;

    .line 79
    return-void
.end method
