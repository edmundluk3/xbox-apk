.class Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApproveLfgRequestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final handleId:Ljava/lang/String;

.field private final memberIndex:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "memberIndex"    # Ljava/lang/String;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 404
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 405
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 407
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->handleId:Ljava/lang/String;

    .line 408
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->memberIndex:Ljava/lang/String;

    .line 409
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 427
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->memberIndex:Ljava/lang/String;

    .line 428
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->withLfgApproval(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v2

    .line 429
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    move-result-object v1

    .line 432
    .local v1, "sessionUpdate":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->handleId:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->updateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v2

    .line 433
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to approve member"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 438
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 422
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 418
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->updatedSession:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->ApproveToLFG:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->access$100(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/microsoft/xbox/service/model/UpdateType;)V

    .line 449
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 398
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 444
    return-void
.end method
