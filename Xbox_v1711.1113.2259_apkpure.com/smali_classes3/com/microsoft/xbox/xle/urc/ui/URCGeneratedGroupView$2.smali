.class Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$2;
.super Ljava/lang/Object;
.source "URCGeneratedGroupView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateView;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;->updateButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$2;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    .local v0, "digit_btn":Z
    if-eqz p2, :cond_0

    .line 107
    const-string v2, "btn.digit_"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 110
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    if-eqz v0, :cond_1

    sget v2, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_numpad_button:I

    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 111
    .local v1, "v":Landroid/view/View;
    invoke-static {p1}, Lcom/microsoft/xbox/xle/urc/ui/URCGeneratedGroupView$CreateViewImpl;->createLayoutParams(Landroid/content/Context;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    return-object v1

    .line 110
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    sget v2, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_numpad_small_button:I

    goto :goto_0
.end method
