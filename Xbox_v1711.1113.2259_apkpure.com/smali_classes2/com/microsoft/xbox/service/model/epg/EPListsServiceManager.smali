.class public Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;
.super Ljava/lang/Object;
.source "EPListsServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
    }
.end annotation


# static fields
.field private static final AppChannelFavoritesListName:Ljava/lang/String; = "ChannelFavs_v2_Apps"

.field private static final COMMON_HEADERS:[Lorg/apache/http/message/BasicHeader;

.field private static final EPListContainsItems:Ljava/lang/String; = "/ContainsItems"

.field private static final EPListMetadata:Ljava/lang/String; = "/metadata"

.field private static final EPListsMultipleListBaseUri:Ljava/lang/String; = "https://eplists%s.xboxlive.com/users/xuid(%s)/lists/%s/MultipleLists?listNames=%s&filterDeviceType=XboxOne"

.field private static final EPListsRemoveItems:Ljava/lang/String; = "/RemoveItems"

.field private static final EPListsSingleListBaseUri:Ljava/lang/String; = "https://eplists%s.xboxlive.com/users/xuid(%s)/lists/%s/%s%s?filterDeviceType=XboxOne"

.field private static final PinsListName:Ljava/lang/String; = "XBLPins"

.field private static final PinsListType:Ljava/lang/String; = "PINS"

.field private static final QueueListType:Ljava/lang/String; = "QUEUE"

.field private static final RecentAppsListName:Ljava/lang/String; = "AppsRecents"

.field private static final RecentGamesListName:Ljava/lang/String; = "GamesRecents"

.field private static final RecentMoviesListName:Ljava/lang/String; = "MovieRecents"

.field private static final RecentMusicListName:Ljava/lang/String; = "MusicRecents"

.field private static final RecentTVListName:Ljava/lang/String; = "TVRecents"

.field private static final RecentsListType:Ljava/lang/String; = "RECN"

.field private static final UnifiedRecentListNames:Ljava/lang/String; = "GamesRecents,AppsRecents"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 264
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/http/message/BasicHeader;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-type"

    const-string v4, "application/json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v3, "x-xbl-contract-version"

    const-string v4, "2"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->COMMON_HEADERS:[Lorg/apache/http/message/BasicHeader;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static addAppChannelFavorite(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "providerId"    # Ljava/lang/String;
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getListBaseUri(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "endPoint":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/service/model/epg/EPItem;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/epg/EPItem;-><init>()V

    .line 86
    .local v2, "item":Lcom/microsoft/xbox/service/model/epg/EPItem;
    const-string v6, "TvChannel"

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->ContentType:Ljava/lang/String;

    .line 87
    const-string v6, "XboxOne"

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->DeviceType:Ljava/lang/String;

    .line 88
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->Locale:Ljava/lang/String;

    .line 90
    iput-object p0, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    .line 91
    iput-object p1, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->ProviderId:Ljava/lang/String;

    .line 93
    new-array v3, v4, [Lcom/microsoft/xbox/service/model/epg/EPItem;

    aput-object v2, v3, v5

    .line 95
    .local v3, "items":[Lcom/microsoft/xbox/service/model/epg/EPItem;
    :try_start_0
    invoke-static {v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->addRemoveItems(Ljava/lang/String;[Lcom/microsoft/xbox/service/model/epg/EPItem;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return v4

    .line 97
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    move v4, v5

    .line 99
    goto :goto_0
.end method

.method private static addCommonHeaders(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 267
    .local p0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->COMMON_HEADERS:[Lorg/apache/http/message/BasicHeader;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 268
    .local v0, "h":Lorg/apache/http/message/BasicHeader;
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "h":Lorg/apache/http/message/BasicHeader;
    :cond_0
    return-void
.end method

.method private static addRemoveItems(Ljava/lang/String;[Lcom/microsoft/xbox/service/model/epg/EPItem;)V
    .locals 7
    .param p0, "endPoint"    # Ljava/lang/String;
    .param p1, "items"    # [Lcom/microsoft/xbox/service/model/epg/EPItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 196
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->addCommonHeaders(Ljava/util/ArrayList;)V

    .line 200
    const/4 v2, 0x0

    .line 203
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    new-instance v3, Lcom/microsoft/xbox/service/model/epg/AddRemoveItemBody;

    invoke-direct {v3, p1}, Lcom/microsoft/xbox/service/model/epg/AddRemoveItemBody;-><init>([Lcom/microsoft/xbox/service/model/epg/EPItem;)V

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 204
    const-string v3, "EPListService"

    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget v3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_2

    .line 206
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xfc1

    iget-object v6, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "EPListService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to add/remove items "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    instance-of v3, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v3, :cond_4

    .line 211
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_0

    .line 217
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_0
    throw v3

    .line 196
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 216
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_2
    if-eqz v2, :cond_3

    .line 217
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 221
    :cond_3
    sget-object v3, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {p0, v3}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 222
    return-void

    .line 213
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xfc1

    invoke-direct {v3, v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private static appendQueryParams(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2
    .param p0, "baseUri"    # Ljava/lang/String;
    .param p1, "skipItems"    # I
    .param p2, "maxItems"    # I

    .prologue
    .line 273
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 293
    .end local p0    # "baseUri":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 277
    .restart local p0    # "baseUri":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 279
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "?"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 280
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_2
    if-lez p1, :cond_3

    .line 284
    const-string v1, "&skipItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 288
    :cond_3
    if-lez p2, :cond_4

    .line 289
    const-string v1, "&maxItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 293
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getAppChannelFavoritesList()[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .locals 3

    .prologue
    .line 79
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const/4 v1, 0x0

    const/16 v2, 0xc8

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getList(Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;II)[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    move-result-object v0

    return-object v0
.end method

.method public static getList(Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;II)[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .locals 4
    .param p0, "profileListType"    # Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
    .param p1, "skipItems"    # I
    .param p2, "maxItems"    # I

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getListBaseUri(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "endPoint":Ljava/lang/String;
    invoke-static {v1, p1, p2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->appendQueryParams(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    .line 62
    :try_start_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getList(Ljava/lang/String;)[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 68
    :goto_0
    return-object v2

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getList(Ljava/lang/String;)[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .locals 8
    .param p0, "endPoint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 225
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->addCommonHeaders(Ljava/util/ArrayList;)V

    .line 229
    const/4 v2, 0x0

    .line 237
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {p0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 238
    const-string v4, "EPListServiceGet"

    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_2

    .line 241
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfc0

    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusLine:Ljava/lang/String;

    invoke-direct {v4, v6, v7, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :catch_0
    move-exception v0

    .line 251
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "EPListService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to gete items "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_5

    .line 253
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_1

    .line 259
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_1
    throw v4

    .line 244
    :cond_2
    :try_start_2
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/service/model/epg/EPListResponse;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/epg/EPListResponse;

    .line 245
    .local v3, "tempResponse":Lcom/microsoft/xbox/service/model/epg/EPListResponse;
    if-eqz v3, :cond_4

    .line 246
    iget-object v4, v3, Lcom/microsoft/xbox/service/model/epg/EPListResponse;->ListItems:Ljava/util/ArrayList;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/microsoft/xbox/service/model/epg/EPListItem;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/microsoft/xbox/service/model/epg/EPListItem;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 258
    if-eqz v2, :cond_3

    .line 259
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 249
    :cond_3
    :goto_0
    return-object v4

    :cond_4
    const/4 v4, 0x0

    .line 258
    if-eqz v2, :cond_3

    .line 259
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    goto :goto_0

    .line 255
    .end local v3    # "tempResponse":Lcom/microsoft/xbox/service/model/epg/EPListResponse;
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_3
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfc0

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private static getListBaseUri(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "profileListType"    # Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;
    .param p2, "verb"    # Ljava/lang/String;

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    .local v0, "isMultipleLists":Z
    const/4 v2, 0x0

    .line 128
    .local v2, "listType":Ljava/lang/String;
    const/4 v1, 0x0

    .line 130
    .local v1, "listName":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$1;->$SwitchMap$com$microsoft$xbox$service$model$epg$EPListsServiceManager$ProfileListType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 173
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unknown profile list type"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 132
    :pswitch_0
    const-string v2, "RECN"

    .line 133
    const-string v1, "AppsRecents"

    .line 176
    :goto_0
    if-eqz v0, :cond_0

    .line 177
    invoke-static {p0, v2, v1}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getMultipleListBaseUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 179
    :goto_1
    return-object v3

    .line 137
    :pswitch_1
    const-string v2, "RECN"

    .line 138
    const-string v1, "GamesRecents"

    .line 139
    goto :goto_0

    .line 142
    :pswitch_2
    const-string v2, "RECN"

    .line 143
    const-string v1, "MovieRecents"

    .line 144
    goto :goto_0

    .line 147
    :pswitch_3
    const-string v2, "RECN"

    .line 148
    const-string v1, "MusicRecents"

    .line 149
    goto :goto_0

    .line 152
    :pswitch_4
    const-string v2, "RECN"

    .line 153
    const-string v1, "TVRecents"

    .line 154
    goto :goto_0

    .line 157
    :pswitch_5
    const-string v2, "RECN"

    .line 158
    const-string v1, "GamesRecents,AppsRecents"

    .line 159
    const/4 v0, 0x1

    .line 160
    goto :goto_0

    .line 163
    :pswitch_6
    const-string v2, "PINS"

    .line 164
    const-string v1, "XBLPins"

    .line 165
    goto :goto_0

    .line 168
    :pswitch_7
    const-string v2, "QUEUE"

    .line 169
    const-string v1, "ChannelFavs_v2_Apps"

    .line 170
    goto :goto_0

    .line 179
    :cond_0
    invoke-static {p0, v2, v1, p2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getSingleListBaseUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 130
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getListVersion(Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;)V
    .locals 0
    .param p0, "profileListType"    # Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    .prologue
    .line 76
    return-void
.end method

.method private static getMultipleListBaseUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "listType"    # Ljava/lang/String;
    .param p2, "listNames"    # Ljava/lang/String;

    .prologue
    .line 184
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://eplists%s.xboxlive.com/users/xuid(%s)/lists/%s/MultipleLists?listNames=%s&filterDeviceType=XboxOne"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getURLPrefix()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSingleListBaseUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "listType"    # Ljava/lang/String;
    .param p2, "listName"    # Ljava/lang/String;
    .param p3, "verb"    # Ljava/lang/String;

    .prologue
    .line 188
    if-nez p3, :cond_0

    .line 189
    const-string p3, ""

    .line 192
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "https://eplists%s.xboxlive.com/users/xuid(%s)/lists/%s/%s%s?filterDeviceType=XboxOne"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getURLPrefix()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    aput-object p3, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static removeAppChannelFavorite(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "providerId"    # Ljava/lang/String;
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 104
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;->PLT_AppChannelFavorites:Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;

    const-string v8, "/RemoveItems"

    invoke-static {v6, v7, v8}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getListBaseUri(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager$ProfileListType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "endPoint":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/service/model/epg/EPItem;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/epg/EPItem;-><init>()V

    .line 107
    .local v2, "item":Lcom/microsoft/xbox/service/model/epg/EPItem;
    const-string v6, "TvChannel"

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->ContentType:Ljava/lang/String;

    .line 108
    const-string v6, "XboxOne"

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->DeviceType:Ljava/lang/String;

    .line 109
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->Locale:Ljava/lang/String;

    .line 111
    iput-object p0, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    .line 112
    iput-object p1, v2, Lcom/microsoft/xbox/service/model/epg/EPItem;->ProviderId:Ljava/lang/String;

    .line 114
    new-array v3, v4, [Lcom/microsoft/xbox/service/model/epg/EPItem;

    aput-object v2, v3, v5

    .line 117
    .local v3, "items":[Lcom/microsoft/xbox/service/model/epg/EPItem;
    :try_start_0
    invoke-static {v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->addRemoveItems(Ljava/lang/String;[Lcom/microsoft/xbox/service/model/epg/EPItem;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return v4

    .line 119
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    move v4, v5

    .line 121
    goto :goto_0
.end method
