.class public final enum Lcom/microsoft/xbox/smartglass/SurfaceType;
.super Ljava/lang/Enum;
.source "SurfaceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/SurfaceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/SurfaceType;

.field public static final enum Blank:Lcom/microsoft/xbox/smartglass/SurfaceType;

.field public static final enum Direct:Lcom/microsoft/xbox/smartglass/SurfaceType;

.field public static final enum Html:Lcom/microsoft/xbox/smartglass/SurfaceType;

.field public static final enum TitleTextInput:Lcom/microsoft/xbox/smartglass/SurfaceType;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/SurfaceType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/SurfaceType;

    const-string v3, "Blank"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/SurfaceType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->Blank:Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/SurfaceType;

    const-string v3, "Direct"

    invoke-direct {v2, v3, v4, v4}, Lcom/microsoft/xbox/smartglass/SurfaceType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->Direct:Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/SurfaceType;

    const-string v3, "Html"

    invoke-direct {v2, v3, v5, v5}, Lcom/microsoft/xbox/smartglass/SurfaceType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->Html:Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/SurfaceType;

    const-string v3, "TitleTextInput"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/SurfaceType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->TitleTextInput:Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 10
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/SurfaceType;

    sget-object v3, Lcom/microsoft/xbox/smartglass/SurfaceType;->Blank:Lcom/microsoft/xbox/smartglass/SurfaceType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/SurfaceType;->Direct:Lcom/microsoft/xbox/smartglass/SurfaceType;

    aput-object v3, v2, v4

    sget-object v3, Lcom/microsoft/xbox/smartglass/SurfaceType;->Html:Lcom/microsoft/xbox/smartglass/SurfaceType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/smartglass/SurfaceType;->TitleTextInput:Lcom/microsoft/xbox/smartglass/SurfaceType;

    aput-object v3, v2, v6

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->$VALUES:[Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 21
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/SurfaceType;->_lookup:Landroid/util/SparseArray;

    .line 24
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SurfaceType;->values()[Lcom/microsoft/xbox/smartglass/SurfaceType;

    move-result-object v2

    array-length v3, v2

    .local v0, "val":Lcom/microsoft/xbox/smartglass/SurfaceType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 25
    sget-object v4, Lcom/microsoft/xbox/smartglass/SurfaceType;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/SurfaceType;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 27
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/microsoft/xbox/smartglass/SurfaceType;->_value:I

    .line 30
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/SurfaceType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 47
    sget-object v1, Lcom/microsoft/xbox/smartglass/SurfaceType;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 48
    .local v0, "val":Lcom/microsoft/xbox/smartglass/SurfaceType;
    if-nez v0, :cond_0

    .line 49
    sget-object v0, Lcom/microsoft/xbox/smartglass/SurfaceType;->Blank:Lcom/microsoft/xbox/smartglass/SurfaceType;

    .line 52
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/SurfaceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/SurfaceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/SurfaceType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/SurfaceType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/SurfaceType;->$VALUES:[Lcom/microsoft/xbox/smartglass/SurfaceType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/SurfaceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/SurfaceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/smartglass/SurfaceType;->_value:I

    return v0
.end method
