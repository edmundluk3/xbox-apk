.class Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "StoreBrowseModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/StoreBrowseModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGoldLoungeDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final storeService:Lcom/microsoft/xbox/service/store/IStoreService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;)V
    .locals 1

    .prologue
    .line 301
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 302
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/StoreBrowseModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;-><init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 306
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v8, v11, v9, v9}, Lcom/microsoft/xbox/service/store/IStoreService;->getGoldGameList(III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v1

    .line 307
    .local v1, "goldGameList":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v8, v11, v9, v9}, Lcom/microsoft/xbox/service/store/IStoreService;->getGoldDealList(III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 309
    .local v0, "goldDealList":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    const/4 v7, 0x0

    .line 311
    .local v7, "totalItemSize":I
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 312
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    .line 315
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 316
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    .line 319
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 321
    .local v2, "goldItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;"
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 322
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v8, v1}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v6

    .line 324
    .local v6, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 326
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    .line 327
    .local v5, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 328
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 329
    .local v4, "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    sget-object v8, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->GamesWithGold:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->setGoldResultItemSection(Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)V

    .line 330
    if-nez v3, :cond_2

    .line 331
    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->setGamesWithGoldHeaderText(Z)V

    .line 333
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    .end local v3    # "i":I
    .end local v4    # "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    .end local v6    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 339
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v8, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v6

    .line 341
    .restart local v6    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 343
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    .line 344
    .restart local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_5

    .line 345
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 346
    .restart local v4    # "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    sget-object v8, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->DealsWithGold:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->setGoldResultItemSection(Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)V

    .line 347
    if-nez v3, :cond_4

    .line 348
    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->setDealsWithGoldHeaderText(Z)V

    .line 350
    :cond_4
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 355
    .end local v3    # "i":I
    .end local v4    # "item":Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    .end local v6    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_5
    return-object v2
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 369
    const-wide/16 v0, 0x2332

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetGoldLoungeDataRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->access$100(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 365
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method
