.class public Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "TrendingBeamPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/beam/TrendingBeamView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
    .param p3, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p4, "systemSettingsModel"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 52
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 54
    invoke-static {p0, p2, p3}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 84
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$7(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p3, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 55
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;
    .locals 1
    .param p0, "loadMoreIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;
    .locals 1
    .param p0, "refreshIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;)V
    .locals 1
    .param p0, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p1, "itemClickedIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;->item()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;->navigateToChannel(Lcom/microsoft/xbox/domain/beam/BeamChannel;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
    .param p1, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p2, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x6

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    const/4 v1, 0x0

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 58
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 59
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->initialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$LoadMoreIntent;

    .line 62
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 63
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->loadMoreTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    .line 66
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 67
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->refreshTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;

    .line 70
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;

    .line 73
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 74
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent;

    .line 75
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 76
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 77
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;

    .line 79
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 80
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 57
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
    .param p2, "navigator"    # Lcom/microsoft/xbox/presentation/beam/BeamNavigator;
    .param p3, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 56
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 9
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 91
    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    if-eqz v5, :cond_3

    move-object v2, p2

    .line 92
    check-cast v2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    .line 94
    .local v2, "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isSuccess()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 95
    iget-object v5, v2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    check-cast v5, Lcom/google/common/collect/ImmutableList;

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    .line 140
    .end local v2    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    :cond_0
    :goto_0
    return-object p1

    .line 96
    .restart local v2    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    :cond_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->isFailure()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    iget-object v5, v2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->error:Ljava/lang/Throwable;

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 101
    .end local v2    # "loadResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_3
    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;

    if-eqz v5, :cond_6

    move-object v1, p2

    .line 102
    check-cast v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;

    .line 104
    .local v1, "loadMoreResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    iget-boolean v5, v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->inFlight:Z

    if-eqz v5, :cond_4

    .line 105
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingMoreInstance(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->isSuccess()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 110
    iget-object v5, v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->content:Ljava/lang/Object;

    check-cast v5, Ljava/util/Collection;

    invoke-static {v5}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 112
    :cond_5
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->isFailure()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 113
    iget-object v5, v1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->error:Ljava/lang/Throwable;

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 115
    .end local v1    # "loadMoreResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_6
    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    if-eqz v5, :cond_9

    move-object v4, p2

    .line 116
    check-cast v4, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    .line 118
    .local v4, "refreshResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->isSuccess()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 119
    iget-object v5, v4, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->content:Ljava/lang/Object;

    check-cast v5, Lcom/google/common/collect/ImmutableList;

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_7
    invoke-virtual {v4}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->isFailure()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 121
    iget-object v5, v4, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->error:Ljava/lang/Throwable;

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 123
    :cond_8
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto :goto_0

    .line 125
    .end local v4    # "refreshResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_9
    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;

    if-eqz v5, :cond_a

    move-object v3, p2

    .line 126
    check-cast v3, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;

    .line 128
    .local v3, "mustConfirmResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .line 129
    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;->toConfirm()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;->with(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ConfirmIntent;

    move-result-object v5

    .line 130
    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;->toConfirm()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;->with(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .line 131
    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->beamAppDeeplinkEnabled()Z

    move-result v7

    invoke-direct {v0, v5, v6, v7}, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;-><init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Z)V

    .line 133
    .local v0, "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    invoke-static {p1, v0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto/16 :goto_0

    .line 135
    .end local v0    # "dialogViewState":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    .end local v3    # "mustConfirmResult":Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult;, "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$MustConfirmIntentResult<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$ItemClickedIntent<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;>;"
    :cond_a
    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$CancelledIntentResult;

    if-nez v5, :cond_b

    instance-of v5, p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$ConfirmedIntentResult;

    if-eqz v5, :cond_c

    .line 136
    :cond_b
    invoke-static {p1}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->withoutDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    move-result-object p1

    goto/16 :goto_0

    .line 139
    :cond_c
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Couldn\'t explicitly reduce state. previousState: %s. result: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 145
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 146
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 147
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 148
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 149
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 151
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 152
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 154
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 155
    return-void
.end method
