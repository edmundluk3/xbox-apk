.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
.super Ljava/lang/Object;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AutoSystemUpdatesChangedAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Z)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;
    .locals 1
    .param p0, "enabled"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 323
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_AutoSystemUpdatesChangedAction;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_AutoSystemUpdatesChangedAction;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public abstract enabled()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
