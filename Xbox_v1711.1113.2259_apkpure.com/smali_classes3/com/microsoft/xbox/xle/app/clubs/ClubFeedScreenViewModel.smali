.class public Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "ClubFeedScreenViewModel.java"


# instance fields
.field private final clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

.field private final clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    .line 45
    .local v0, "parameters":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    sget-object v1, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    iget-wide v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;->clubId:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    iget-wide v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;->clubId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->getInstance(J)Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    .line 49
    return-void
.end method


# virtual methods
.method public canPinPosts()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method protected getActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->getFeedItems()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0700da

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0701e7

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    return-object v0
.end method

.method public getTimelineId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public isStatusPostEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    if-eqz v3, :cond_0

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 139
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 140
    .local v1, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->canPostStatus()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v3, v4, :cond_0

    const/4 v2, 0x1

    .line 146
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_0
    return v2

    .line 139
    .restart local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public launchStatusPost()V
    .locals 4

    .prologue
    .line 151
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 152
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedClubId(Ljava/lang/Long;)V

    .line 154
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTimelineId(Ljava/lang/String;)V

    .line 156
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedStatusPostScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 157
    return-void
.end method

.method public launchUnsharedFeed()V
    .locals 4

    .prologue
    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->ClubFeed:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;-><init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;J)V

    .line 162
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 163
    return-void
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->loadSync(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onSetActive()V

    .line 110
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Feed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 112
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 99
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 104
    return-void
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel;->clubActivityFeedModel:Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubActivityFeedModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 122
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 125
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubFeedScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    .line 132
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 133
    return-void

    .line 121
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
