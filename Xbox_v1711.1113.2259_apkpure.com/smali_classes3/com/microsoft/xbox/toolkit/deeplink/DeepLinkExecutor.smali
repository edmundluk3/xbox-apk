.class public final Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;
.super Ljava/lang/Object;
.source "DeepLinkExecutor.java"


# static fields
.field private static final AMAZON_STORE_URI:Ljava/lang/String; = "amzn://apps/android?p="

.field public static final AMAZON_TABLET_STORE_PACKAGE:Ljava/lang/String; = "com.amazon.venezia"

.field public static final AMAZON_UNDERGROUND_PACKAGE:Ljava/lang/String; = "com.amazon.mShop.android"

.field public static final PLAY_STORE_PACKAGE:Ljava/lang/String; = "com.android.vending"

.field private static final PLAY_STORE_URI:Ljava/lang/String; = "market://details?id="

.field private static final PLAY_STORE_WEB_URI:Ljava/lang/String; = "https://play.google.com/store/apps/details?id="

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final context:Landroid/content/Context;

.field private final fallbackToPlayStoreWebView:Z

.field private final packageManager:Landroid/content/pm/PackageManager;

.field private final tryFallbackToAmazonStore:Z

.field private final tryFallbackToPlayStore:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "tryFallbackToPlayStore"    # Z
    .param p3, "tryFallbackToAmazonStore"    # Z
    .param p4, "fallbackToPlayStoreWebView"    # Z

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->context:Landroid/content/Context;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->packageManager:Landroid/content/pm/PackageManager;

    .line 58
    iput-boolean p2, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryFallbackToPlayStore:Z

    .line 59
    iput-boolean p3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryFallbackToAmazonStore:Z

    .line 60
    iput-boolean p4, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->fallbackToPlayStoreWebView:Z

    .line 61
    return-void
.end method

.method private canLaunchIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static defaultExecutor()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 65
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;-><init>(Landroid/content/Context;ZZZ)V

    return-object v0
.end method

.method private getStoreIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p1, "storeUri"    # Ljava/lang/String;
    .param p2, "expectedStorePackage"    # Ljava/lang/String;
    .param p3, "packageInStore"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 178
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 179
    .local v4, "storeIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->packageManager:Landroid/content/pm/PackageManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 181
    .local v1, "appsAbleToHandleIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 182
    .local v0, "app":Landroid/content/pm/ResolveInfo;
    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 184
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 186
    .local v3, "otherAppActivity":Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v5, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .local v2, "componentName":Landroid/content/ComponentName;
    const/high16 v5, 0x10200000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    invoke-virtual {v4, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 198
    .end local v0    # "app":Landroid/content/pm/ResolveInfo;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    .end local v3    # "otherAppActivity":Landroid/content/pm/ActivityInfo;
    .end local v4    # "storeIntent":Landroid/content/Intent;
    :goto_0
    return-object v4

    .restart local v4    # "storeIntent":Landroid/content/Intent;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static intentOnlyExecutor()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 70
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;-><init>(Landroid/content/Context;ZZZ)V

    return-object v0
.end method

.method private launchIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    sget-object v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "launching intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 139
    return-void
.end method

.method private tryLaunchAmazonStore(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "packgeInStore"    # Ljava/lang/String;

    .prologue
    .line 159
    const-string v0, "amzn://apps/android?p="

    const-string v1, "com.amazon.mShop.android"

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchStore(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private tryLaunchIntent(Ljava/util/List;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 123
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->canLaunchIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->launchIntent(Landroid/content/Intent;)V

    .line 129
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryLaunchLegacyAmazonStore(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "packgeInStore"    # Ljava/lang/String;

    .prologue
    .line 163
    const-string v0, "amzn://apps/android?p="

    const-string v1, "com.amazon.venezia"

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchStore(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private tryLaunchPackage(Ljava/util/List;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "launchPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 143
    .local v1, "launchPackage":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 145
    .local v0, "launchIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->canLaunchIntent(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->launchIntent(Landroid/content/Intent;)V

    .line 151
    .end local v0    # "launchIntent":Landroid/content/Intent;
    .end local v1    # "launchPackage":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryLaunchPlayStore(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "packgeInStore"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string v0, "market://details?id="

    const-string v1, "com.android.vending"

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchStore(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private tryLaunchStore(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1, "storeUri"    # Ljava/lang/String;
    .param p2, "expectedStorePackage"    # Ljava/lang/String;
    .param p3, "packageInStore"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->getStoreIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 169
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->launchIntent(Landroid/content/Intent;)V

    .line 173
    :cond_0
    return-object v0
.end method


# virtual methods
.method public execute(Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;)Landroid/content/Intent;
    .locals 5
    .param p1, "request"    # Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->getIntents()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchIntent(Ljava/util/List;)Landroid/content/Intent;

    move-result-object v0

    .line 78
    .local v0, "launchedIntent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    move-object v2, v0

    .line 118
    :cond_0
    :goto_0
    return-object v2

    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->getLaunchPackages()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchPackage(Ljava/util/List;)Landroid/content/Intent;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_2

    move-object v2, v0

    .line 84
    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->getPackageInStore()Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "packageInStore":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 93
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryFallbackToPlayStore:Z

    if-eqz v3, :cond_3

    .line 94
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchPlayStore(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_3

    move-object v2, v0

    .line 96
    goto :goto_0

    .line 100
    :cond_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryFallbackToAmazonStore:Z

    if-eqz v3, :cond_5

    .line 101
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchAmazonStore(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_4

    move-object v2, v0

    .line 103
    goto :goto_0

    .line 106
    :cond_4
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->tryLaunchLegacyAmazonStore(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_5

    move-object v2, v0

    .line 108
    goto :goto_0

    .line 112
    :cond_5
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->fallbackToPlayStoreWebView:Z

    if-eqz v3, :cond_0

    .line 113
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "launchedIntent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 114
    .restart local v0    # "launchedIntent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->launchIntent(Landroid/content/Intent;)V

    move-object v2, v0

    .line 115
    goto :goto_0
.end method
