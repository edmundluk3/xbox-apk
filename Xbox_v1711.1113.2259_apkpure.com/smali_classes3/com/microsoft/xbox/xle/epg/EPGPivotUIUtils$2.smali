.class final Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;
.super Ljava/lang/Object;
.source "EPGPivotUIUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils;->setupPivotArrowButtons(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$currentPaneIndex:I

.field final synthetic val$pivot:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;I)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;->val$pivot:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;

    iput p2, p0, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;->val$currentPaneIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;->val$pivot:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;->val$currentPaneIndex:I

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->EASE_OUT_EXPO_7:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateToCurrentPaneIndex(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 50
    return-void
.end method
