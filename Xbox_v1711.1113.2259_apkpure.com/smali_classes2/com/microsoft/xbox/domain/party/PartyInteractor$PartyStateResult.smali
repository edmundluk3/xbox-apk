.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PartyStateResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
    .locals 1
    .param p0, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;)",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;"
        }
    .end annotation

    .prologue
    .line 281
    .local p1, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 282
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 283
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public abstract partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    const-string v0, "PartyState"

    return-object v0
.end method

.method public abstract userSummaries()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end method
