.class Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;
.super Ljava/lang/Object;
.source "HoverChatHeadImageDownload.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClubHoverChatHeadImageDownloader"
.end annotation


# instance fields
.field private final key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)V
    .locals 0
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .line 91
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
    .param p2, "x1"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$1;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;-><init>(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)V

    return-void
.end method

.method static synthetic lambda$getImagesUrls$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/google/common/collect/ImmutableList;
    .locals 2
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 101
    :goto_0
    return-object v1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "ex":Ljava/lang/NullPointerException;
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public getImagesUrls()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;->key:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Detail:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->rxLoad(ZLjava/lang/Long;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 95
    return-object v0
.end method
