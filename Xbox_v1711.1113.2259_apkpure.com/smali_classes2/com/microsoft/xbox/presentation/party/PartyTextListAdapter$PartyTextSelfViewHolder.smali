.class public Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyTextListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PartyTextSelfViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
        ">;"
    }
.end annotation


# instance fields
.field arrow:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04a3
    .end annotation
.end field

.field messageContainer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04a4
    .end annotation
.end field

.field text:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04a5
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

.field timeStamp:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e049a
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    .line 101
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 102
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 103
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;)V
    .locals 5
    .param p1, "partyMessage"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 107
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->getAdapterPosition()I

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->access$000(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;I)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "timeStamp":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 109
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->timeStamp:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->message()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    iget-object v2, v2, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->myProfileProvider:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/MyProfileProvider;->getMyProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 118
    .local v0, "profilePreferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-nez v0, :cond_0

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->defaultProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 122
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->messageContainer:Landroid/view/View;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 126
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->invalidate()V

    .line 127
    return-void

    .line 111
    .end local v0    # "profilePreferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->timeStamp:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->timeStamp:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 86
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;->onBind(Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;)V

    return-void
.end method
