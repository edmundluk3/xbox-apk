.class Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;
.super Ljava/lang/Object;
.source "DetailsMoreOrLessView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEllipsizeChange(Z)V
    .locals 3
    .param p1, "isEllipsized"    # Z

    .prologue
    const/4 v2, 0x0

    .line 87
    if-eqz p1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$100(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$100(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setClickable(Z)V

    .line 96
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->access$100(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;->this$0:Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setClickable(Z)V

    goto :goto_0
.end method
