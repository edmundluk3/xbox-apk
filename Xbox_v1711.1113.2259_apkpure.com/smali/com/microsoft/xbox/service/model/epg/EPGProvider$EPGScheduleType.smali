.class public final enum Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;
.super Ljava/lang/Enum;
.source "EPGProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EPGScheduleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

.field public static final enum favorites:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

.field public static final enum full:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

.field public static final enum hdAndUnmatchedSd:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

.field public static final enum hdOnly:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 336
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    const-string v1, "full"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->full:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    const-string v1, "favorites"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->favorites:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    const-string v1, "hdOnly"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdOnly:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    const-string v1, "hdAndUnmatchedSd"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdAndUnmatchedSd:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    .line 335
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->full:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->favorites:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdOnly:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdAndUnmatchedSd:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 335
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static fromFilterPreference(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;)Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;
    .locals 1
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .prologue
    .line 339
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->all:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    if-ne p0, v0, :cond_0

    .line 340
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->full:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    .line 344
    :goto_0
    return-object v0

    .line 341
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->hd:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    if-ne p0, v0, :cond_1

    .line 342
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdOnly:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    goto :goto_0

    .line 344
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->hdAndUnmatchedSd:Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 335
    const-class v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;
    .locals 1

    .prologue
    .line 335
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    return-object v0
.end method
