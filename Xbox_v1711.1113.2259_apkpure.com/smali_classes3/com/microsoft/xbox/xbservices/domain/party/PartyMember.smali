.class public abstract Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
.super Ljava/lang/Object;
.source "PartyMember.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;,
        Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    }
.end annotation


# static fields
.field private static final transient TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    .locals 7

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getCurrentPartyConnectionState()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;

    move-result-object v0

    .line 56
    .local v0, "currentPartyState":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Unknown:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 58
    .local v1, "currentState":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    sget-object v4, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$1;->$SwitchMap$com$microsoft$xbox$xbservices$data$service$multiplayer$MultiplayerDataTypes$PartySimpleConnectionState:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$PartySimpleConnectionState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 90
    :goto_0
    return-object v1

    .line 60
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Connecting:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 61
    goto :goto_0

    .line 63
    :pswitch_1
    const/4 v2, 0x0

    .line 65
    .local v2, "isMuted":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->isAllowed()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 67
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isTalking()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Talking:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 78
    :cond_0
    :goto_1
    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isSelfMuted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 79
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Muted:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    goto :goto_0

    .line 67
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Silent:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    goto :goto_1

    .line 68
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->reasons()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->reasons()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->reasons()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;

    .line 70
    .local v3, "reason":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;->reason()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->MuteListRestrictsTarget:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    if-ne v5, v6, :cond_4

    .line 71
    const/4 v2, 0x1

    .line 72
    goto :goto_1

    .line 81
    .end local v3    # "reason":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isTalking()Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Talking:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 84
    :goto_2
    goto :goto_0

    .line 81
    :cond_6
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Silent:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    goto :goto_2

    .line 86
    .end local v2    # "isMuted":Z
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->Disconnected:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    goto/16 :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public gamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->gamertag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAllowedInBroadcast()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->allowedInBroadcast()Z

    move-result v0

    return v0
.end method

.method public isBroadcasting()Z
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->isBroadcasting()Z

    move-result v0

    return v0
.end method

.method public abstract isHost()Z
.end method

.method public abstract isSelfMuted()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract isTalking()Z
.end method

.method public abstract multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract rosterKey()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public xuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->multiplayerMember()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
