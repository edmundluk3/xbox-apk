.class public Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "ExtrasScreenAdapter.java"


# instance fields
.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 25
    const v0, 0x7f0e0793

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->screenBody:Landroid/view/View;

    .line 26
    const v0, 0x7f0e0794

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .line 29
    const v0, 0x7f0e0795

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getShouldHideScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ExtrasScreen;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 53
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->updateLoadingIndicator(Z)V

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->items:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->items:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030167

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->items:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ExtrasListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->restoreListPosition()V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 51
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto :goto_0

    .line 47
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->notifyDataSetChanged()V

    goto :goto_1
.end method
