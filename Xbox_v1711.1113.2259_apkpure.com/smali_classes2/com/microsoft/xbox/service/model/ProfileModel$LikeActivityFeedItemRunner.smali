.class Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeActivityFeedItemRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
        ">;"
    }
.end annotation


# instance fields
.field itemPath:Ljava/lang/String;

.field private newLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "newLikeState"    # Z
    .param p3, "itemPath"    # Ljava/lang/String;
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 4234
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4235
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->newLikeState:Z

    .line 4236
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->itemPath:Ljava/lang/String;

    .line 4237
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->xuid:Ljava/lang/String;

    .line 4238
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4246
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSendLikeActivityUrlFormat()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->itemPath:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->xuid:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4247
    .local v0, "likeItemUrlPath":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->newLikeState:Z

    invoke-interface {v1, v0, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendMeLike(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4229
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$LikeActivityFeedItemRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4256
    const-wide/16 v0, 0xd1d

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4252
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4242
    return-void
.end method
