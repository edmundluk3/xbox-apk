.class abstract Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;
.super Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
.source "$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle$Builder;
    }
.end annotation


# instance fields
.field private final activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

.field private final id:Ljava/lang/String;

.field private final relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

.field private final roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

.field private final searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

.field private final sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "sessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "activityInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "searchAttributes"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "relatedInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "roleInfo"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 29
    iput-object p4, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    .line 30
    iput-object p5, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    .line 31
    iput-object p6, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    .line 32
    iput-object p7, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    .line 33
    return-void
.end method


# virtual methods
.method public activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p1, p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 96
    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 97
    .local v0, "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->type()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    if-nez v3, :cond_5

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    if-nez v3, :cond_6

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    if-nez v3, :cond_7

    .line 101
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    if-nez v3, :cond_8

    .line 102
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    if-nez v3, :cond_9

    .line 103
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 97
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 98
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 99
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 100
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 101
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 102
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 103
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_a
    move v1, v2

    .line 105
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 110
    const/4 v0, 0x1

    .line 111
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 113
    mul-int/2addr v0, v3

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 115
    mul-int/2addr v0, v3

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 117
    mul-int/2addr v0, v3

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 119
    mul-int/2addr v0, v3

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 121
    mul-int/2addr v0, v3

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 123
    mul-int/2addr v0, v3

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v2

    .line 125
    return v0

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    .line 120
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    .line 122
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    .line 124
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    return-object v0
.end method

.method public relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    return-object v0
.end method

.method public roleInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    return-object v0
.end method

.method public searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    return-object v0
.end method

.method public sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MultiplayerHandle{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionRef="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activityInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchAttributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relatedInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roleInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    return-object v0
.end method
