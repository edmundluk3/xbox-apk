.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;
.super Ljava/lang/Object;
.source "GameProfileClubMembershipViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GameProfileClubMembershipListItem"
.end annotation


# instance fields
.field public final clubList:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p3, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 191
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->description:Ljava/lang/String;

    .line 192
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->clubList:Ljava/util/List;

    .line 193
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    if-ne p1, p0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v1

    .line 199
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 200
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 202
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    .line 203
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->description:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->clubList:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->clubList:Ljava/util/List;

    .line 204
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 210
    const/16 v0, 0x11

    .line 211
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->description:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 212
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->clubList:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 213
    return v0
.end method
