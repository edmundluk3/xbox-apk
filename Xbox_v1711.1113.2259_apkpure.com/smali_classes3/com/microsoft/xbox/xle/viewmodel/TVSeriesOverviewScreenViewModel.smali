.class public Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TVSeriesOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;",
        ">;"
    }
.end annotation


# instance fields
.field private activitiesData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private activitiesModelLoaded:Z

.field private activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesModelLoaded:Z

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 29
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesModelLoaded:Z

    return p1
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesData:Ljava/util/ArrayList;

    return-object p1
.end method


# virtual methods
.method public getActivityData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f070683

    return v0
.end method

.method public getLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMetacriticReviewScore()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMetacriticReviewScore()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getNetworkName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeasonCount()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getSeasonCount()I

    move-result v0

    return v0
.end method

.method public getTVSeriesExtraData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHasTvShowtimeInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "TODO: Hookup to live data"

    .line 113
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTVSeriesReleaseData()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getSeasonCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->getParentalRating()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f071084

    .line 88
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v0, v1, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->navigateToActivityDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 102
    return-void
.end method

.method public navigateToLatestEpisode()V
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getLatestEpisodeItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getLatestEpisodeItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    move-result-object v0

    .line 94
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 98
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    :goto_0
    return-void

    .line 96
    :cond_0
    const-string v1, "TVSeriesDetailpage"

    const-string v2, "The latest episode is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 54
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesModelLoaded:Z

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->load(Z)V

    .line 58
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVSeriesOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 39
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStopOverride()V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->activitiesTask:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->cancel()V

    .line 66
    :cond_0
    return-void
.end method
