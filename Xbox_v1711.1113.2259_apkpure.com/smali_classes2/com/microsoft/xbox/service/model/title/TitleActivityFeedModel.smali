.class public Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "TitleActivityFeedModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final ARG_TITLE_ID:Ljava/lang/String; = "TITLE_ID"

.field private static final CACHE_SIZE:I = 0x80

.field private static final NINETY_DAYS_IN_MILLISECONDS:J = 0x1cf7c5800L

.field private static final map:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private continuationToken:Ljava/lang/String;

.field private feeds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingMoreFeed:Z

.field private final titleId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3
    .param p1, "titleId"    # J

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->isLoadingMoreFeed:Z

    .line 38
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->titleId:J

    .line 39
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->lifetime:J

    .line 40
    return-void
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->isLoadingMoreFeed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->titleId:J

    return-wide v0
.end method

.method public static getInstance(J)Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    .locals 4
    .param p0, "titleId"    # J

    .prologue
    .line 84
    sget-object v2, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    monitor-enter v2

    .line 85
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .line 86
    .local v0, "entry":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .end local v0    # "entry":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;-><init>(J)V

    .line 88
    .restart local v0    # "entry":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    :cond_0
    monitor-exit v2

    return-object v0

    .line 91
    .end local v0    # "entry":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifyOnChange(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 108
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "TITLE_ID"

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->titleId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 109
    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    .line 110
    .local v2, "updateData":Lcom/microsoft/xbox/service/model/UpdateData;
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    invoke-direct {v1, v2, p0, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 111
    .local v1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 112
    return-void
.end method

.method public static reset()V
    .locals 4

    .prologue
    .line 95
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 97
    sget-object v3, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    monitor-enter v3

    .line 98
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    .line 99
    .local v0, "copy":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;>;"
    sget-object v2, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->map:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->evictAll()V

    .line 100
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    .line 102
    .local v1, "m":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->clearObservers()V

    goto :goto_0

    .line 100
    .end local v1    # "m":Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public getContinuationToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->continuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getFeedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->titleId:J

    return-wide v0
.end method

.method public loadSync(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;-><init>(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/ModelBase;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 67
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    .line 68
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->isLoadingMoreFeed:Z

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    .line 77
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->isLoadingMoreFeed:Z

    .line 78
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->contToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->continuationToken:Ljava/lang/String;

    .line 80
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->notifyOnChange(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 81
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    if-nez v0, :cond_2

    .line 72
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    goto :goto_0

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->feeds:Ljava/util/List;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
