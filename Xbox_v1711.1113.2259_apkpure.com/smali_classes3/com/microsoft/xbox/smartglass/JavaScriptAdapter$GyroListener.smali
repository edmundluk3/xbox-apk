.class Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;
.super Lcom/microsoft/xbox/smartglass/GyrometerListener;
.source "JavaScriptAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GyroListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/GyrometerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReadingChanged(Lcom/microsoft/xbox/smartglass/Gyrometer;Lcom/microsoft/xbox/smartglass/GyrometerReading;)V
    .locals 11
    .param p1, "gyrometer"    # Lcom/microsoft/xbox/smartglass/Gyrometer;
    .param p2, "reading"    # Lcom/microsoft/xbox/smartglass/GyrometerReading;

    .prologue
    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter$GyroListener;->this$0:Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->access$000(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;)Lcom/microsoft/xbox/smartglass/RefTPtr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v2

    iget-wide v4, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->x:D

    iget-wide v6, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->y:D

    iget-wide v8, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->z:D

    const-string v10, "gyrometerReading"

    invoke-static/range {v1 .. v10}, Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;->access$100(Lcom/microsoft/xbox/smartglass/JavaScriptAdapter;JDDDLjava/lang/String;)V

    .line 111
    return-void
.end method
