.class Lcom/microsoft/xbox/smartglass/controls/LogV2;
.super Lcom/microsoft/xbox/smartglass/controls/Log;
.source "LogV2.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 0
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Log;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected write(ILjava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 17
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;-><init>(Ljava/lang/String;)V

    .line 18
    .local v1, "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;
    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;->message:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/LogV2;->writeInternal(ILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .end local v1    # "parameters":Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/LogV2;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid JSON. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
