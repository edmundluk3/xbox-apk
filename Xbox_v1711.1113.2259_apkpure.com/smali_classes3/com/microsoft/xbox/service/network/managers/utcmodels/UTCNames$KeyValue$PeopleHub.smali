.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$PeopleHub;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PeopleHub"
.end annotation


# static fields
.field public static final All:Ljava/lang/String; = "All"

.field public static final Block:Ljava/lang/String; = "Block"

.field public static final Clubs:Ljava/lang/String; = "Clubs"

.field public static final Favorites:Ljava/lang/String; = "Favorites"

.field public static final Followers:Ljava/lang/String; = "Followers"

.field public static final Friends:Ljava/lang/String; = "Friends"

.field public static final FriendsGames:Ljava/lang/String; = "FriendsAndGames"

.field public static final Games:Ljava/lang/String; = "Games"

.field public static final MostRecent:Ljava/lang/String; = "MostRecent"

.field public static final Muted:Ljava/lang/String; = "Muted"

.field public static final Other:Ljava/lang/String; = "Other"

.field public static final Xbox360:Ljava/lang/String; = "Xbox360"

.field public static final XboxOne:Ljava/lang/String; = "XboxOne"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
