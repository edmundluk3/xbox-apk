.class Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;
.super Ljava/lang/Object;
.source "VerticalScrollDockLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setScrollContainerView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "scrollView"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 259
    const/4 v2, 0x0

    .line 260
    .local v2, "handled":Z
    check-cast p1, Landroid/widget/ScrollView;

    .end local p1    # "scrollView":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v4

    .line 261
    .local v4, "scrollY":I
    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 262
    .local v0, "action":I
    const/4 v5, 0x2

    if-ne v0, v5, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$100(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-ne v5, v6, :cond_0

    .line 263
    if-nez v4, :cond_0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I

    move-result v5

    if-lez v5, :cond_0

    .line 265
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 266
    .local v1, "cancelTouch":Landroid/view/MotionEvent;
    const/4 v5, 0x3

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 267
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 268
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 269
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 270
    .local v3, "newTouchDown":Landroid/view/MotionEvent;
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 271
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 272
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 273
    const/4 v2, 0x1

    .line 276
    .end local v1    # "cancelTouch":Landroid/view/MotionEvent;
    .end local v3    # "newTouchDown":Landroid/view/MotionEvent;
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$402(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;I)I

    .line 277
    return v2
.end method
